﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class AdvanceAdjustment_VM
    {
        public long AssesseeID { get; set; }
        public long AdvPaymentID { get; set; }
        public int MunicipalityID { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingNo { get; set; }
        public string ReceiptNo { get; set; }
        public string AdvanceDate { get; set; }
        public decimal AdvAmt { get; set; }
        public decimal UnadjustAdvAmt { get; set; }
        public decimal AdjustAdvAmt { get; set; }
    }
}