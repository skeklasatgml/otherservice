﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class RegOfficeNIC_VM
    {
        public int? ROcode { get; set; }
        public string RO_name { get; set; }
    }
}