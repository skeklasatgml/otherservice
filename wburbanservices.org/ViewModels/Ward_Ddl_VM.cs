﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Ward_Ddl_VM
    {
        public string WardName { get; set; }
        public int WardID { get; set; }
    }
}