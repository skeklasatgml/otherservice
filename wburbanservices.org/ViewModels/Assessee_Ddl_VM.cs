﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Assessee_Ddl_VM
    {
        public long AssesseeID { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingNo { get; set; }
    }
}