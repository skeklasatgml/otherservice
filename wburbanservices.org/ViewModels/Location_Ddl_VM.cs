﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Location_Ddl_VM
    {
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
}