﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Dashboard_vm
    {
        public Dashboard_vm()
        {
            Municipalities = new List<Municipality>();
        }
        public long TotalAssesseesCount { get; set; }
        public int TotalMunicipalityCount { get; set; }
        public decimal TotalCollection { get; set; }
        public List<Municipality> Municipalities { get; set; }
        public class AssesseesByDistrict
        {
            public int DistrictId { get; set; }
            public string DistrictName { get; set; }
            public int NewAssesseeCount { get; set; }
            public int  TotalAssesseeCount { get; set; }
        }
        public class AssesseesByMuncipaity
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public int NewAssesseeCount { get; set; }
            public int TotalAssesseeCount { get; set; }
        }
        public class AssesseesByWardMonthWise
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public int WardId { get; set; }
            public string Ward { get; set; }
            public string MonthYear { get; set; }
            public int NewAssesseeCount { get; set; }
        }
        
        public class AssesseesByMuncipaityWard
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public string Ward { get; set; }
            public int WardId { get; set; }
            public int NewAssesseeCount { get; set; }
            public int TotalAssesseeCount { get; set; }
        }
        public class Assessee
        {
            public long AssesseeId { get; set; }
            public string Name { get; set; }
            public string HoldingNo { get; set; }
            public string AssesseeNo { get; set; }
            public string Ward { get; set; }
            public int? WardId { get; set; }
            public int?  LocationId { get; set; }
            public string Location { get; set; }
            public string PhoneNo { get; set; }
            public string EmailID { get; set; }
            public int? MunicipalityId { get; set; }
            public string AssesseeAddress { get; set; }
            public decimal PropertyTax   { get; set; }
            public decimal Surcharge { get; set; }
        }
       public class MunicipalityValuation
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public long TotalAssesseeCount { get; set; }
            public decimal TotalValuation { get; set; }
        }
        public class Municipality
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public DateTime? InsertedOn { get; set; }
        }
        public class MuncipalityCollectionSummary
        {
            public decimal TotalPropertyTax { get; set; }
            public decimal TotalSurcharge { get; set; }
            public decimal TotalRebate { get; set; }
            public decimal TotalInterest { get; set; }
            public string AppOrigine { get; set; }
            public decimal NetPaidAmount { get; set; }
        }
        public class Collection
        {
            public decimal CurPropTax { get; set; }
            public decimal CurSurcharge { get; set; }
            public decimal ArrPropTax { get; set; }
            public decimal ArrSurcharge { get; set; }
            public decimal Rebate { get; set; }
            public decimal Interest { get; set; }
            public decimal OtherAdjustment { get; set; }
            public string AppOrigine { get; set; }
            public decimal NetPaidAmount { get; set; }
        }
        public class MunicipalityWiseCollection: MuncipalityCollectionSummary
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            
        }
        public class MunicipalityWiseCollection2 : Collection
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }

        }
        public class WardWiseCollection : MunicipalityWiseCollection2
        {
            public int WardId { get; set; }
            public string WardName { get; set; }
        }
        public class CollectorWiseCollection : MunicipalityWiseCollection2
        {
            public long CollectorId { get; set; }
            public string CollectorName { get; set; }
        }
        public class CollectionFlowByTimeSpan:Collection
        {
            public string FinYear { get; set; }
            public string DtSpanBase { get; set; }
        }

        public class DemandVsCollectionTotalSummary
        {
            public string WardID { get; set; }
            public decimal? TotalDemand { get; set; }
            public decimal? TotalCollection { get; set; }
            public decimal? ArrearDemand { get; set; }
            public decimal? ArrearCollection { get; set; }
            public decimal? CurrentDemand { get; set; }
            public decimal? CurrentCollection { get; set; }
        }
    }
}