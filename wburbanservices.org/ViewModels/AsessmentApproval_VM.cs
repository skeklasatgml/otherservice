﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class AsessmentApproval_VM
    {
        public int? MunicipalityID { get; set; }
        public int? v_year { get; set; }
        public int? v_id_no { get; set; }
        public int? v_srl { get; set; }
        public int? v_ward_no { get; set; }
        public string WardName { get; set; }
        public int? v_street_code { get; set; }
        public string LocationName { get; set; }
        public string v_holding_no { get; set; }
        public string v_name { get; set; }
        public string v_address { get; set; }
        public string v_hold_type { get; set; }
        public decimal? v_yr_proptax { get; set; }
        public decimal? v_qtr_proptax { get; set; }
        public decimal? v_prv_qtr_proptax { get; set; }
        public string v_srchg_tag { get; set; }
        public decimal? v_qtr_srchg { get; set; }
        public int? v_val_year { get; set; }
        public int? v_val_qtr { get; set; }
        public int? v_val_srl { get; set; }
        public string ApprovedFlag { get; set; }
        public string ApprovedStatus { get; set; }
        public string ApprovedOn { get; set; }
        public string v_entry_time { get; set; }
    }
}