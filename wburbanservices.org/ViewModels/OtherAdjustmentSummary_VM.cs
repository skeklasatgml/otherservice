﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class OtherAdjustmentSummary_VM
    {
        public long MstAdjID { get; set; }
        public string AdjustmentText { get; set; }
        public string WellKnownName { get; set; }
        public decimal AdjustedAmount { get; set; }
        public decimal RateValue { get; set; }
        public string RateValueType { get; set; }
        public string PayHeadType{get;set;}
        public int MstPayHeadID { get; set; }
        /// <summary>
        /// Tergate amount like [Gross Amount, Line Total OF Current Demand]
        /// </summary>
        //public decimal AdjAppliedAmountOn { get; set; }
    }
}