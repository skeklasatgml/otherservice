﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Valuation_Board.Models.Application.ScheduleThree;
using static Valuation_Board.ViewModels.Assessment_VM.AssessmentForm;

namespace Valuation_Board.ViewModels
{
    public class Assessment_VM
    {
        public class AssessmentType
        {
            public string ValTag { get; set; }
            public string ValDesc { get; set; }
            public string ValGroup { get; set; }
        }
        public abstract class AssessmentFormBase
        {
            public string AssessmentValTag { get; set; }
            public string SubAction { get; set; }
            public string FinYear { get; set; }
            public int Qtr { get; set; }
        }
        public class AssessmentForm : AssessmentFormBase
        {
            
            public int _YearID { get; set; }
            public HoldingDetails _HoldingDetails { get; set; }
            //public ValuationDetails _ValuationDetails { get; set; }
            public List<ValuationDetails> _ValuationDetails { get; set; }
            public ValuationDetails _ValuationDetail { get; set; }
            public OtherDetails _OtherDetails { get; set; }
            public OtherTabDetails _OtherTabDetails { get; set; }
            public AdditionalHoldingDetails _AdditionalHoldingDetails { get; set; }
            public List<HoldingTypeGroup> _HoldingTypeGroupes { get; set; }
            public List<Score> _Zones { get; set; }
            public List<Score> _Constructions { get; set; }
            public List<Score> _NatureOfUse { get; set; }
            public List<DeedType> _DeedTypes { get; set; }
            public List<HoldingType> _HoldingTypes { get; set; }
            public AssmtTransactionModel ActualAssmtData { get; set; }

            public List<OtherInfo> OtherInfos { get; set; }
            public MasterData MasterData { get; set; }

            public class CalculatedModel
            {
                public decimal AnnualValuation { get; set; }
                public decimal Qtr_Ptax { get; set; }
                public decimal Qtr_Schrg { get; set; }
                public decimal Waitage { get; set; }
                public decimal LandCost { get; set; }
                public decimal Factor { get; set; }
                public decimal DisPer { get; set; }
                public decimal DiscAmt { get; set; }
                public decimal RevAnnualVal { get; set; }
                public decimal PtaxPer { get; set; }
                public decimal Ytax { get; set; }
                public decimal EduCessAmt { get; set; }
            }
            public class Block
            {
                public int BlockCode { get; set; }
                public string BlockName { get; set; }
            }
            public class Mouza
            {
                public int MouzaCode { get; set; }
                public string MouzaName { get; set; }
            }
            public class DeedType
            {
                public int DeedTypeId { get; set; }
                public string DeedTypeName { get; set; }
            }
            public class HoldingType
            {
                public int HoldingTypeId { get; set; }
                public string HoldingTypeDesc { get; set; }
            }
            public class HoldingTypeGroup
            {
                public int HoldingTypeGrpId { get; set; }
                public string HoldingTypeGrpDesc { get; set; }
                public string HoldingTypeGrpIdFor { get; set; }
            }
            public class Score
            {
                /// <summary>
                /// Score Id or Score sheet
                /// </summary>
                public long ScoreId { get; set; }
                public string ScoreCode { get; set; }
                public decimal ScoreValue { get; set; }
            }
            public class Owner
            {
                public long TabId { get; set; }
                public long TabIdAssmtOccupancy { get; set; }
                public string AssesseeName { get; set; }
                public string AssesseeAddress { get; set; }

                public int MunicipalityID { get; set; }
            }
            public class HoldingDetails : AssessmentFormBase
            {
                public int WardId { get; set; }
                public string WardName { get; set; }
                public int LocationId { get; set; }
                public string LocationName { get; set; }
                public string HoldingNo { get; set; }
                public int HoldingTypeId { get; set; }
                public int HoldingTypeGroupId { get; set; }
                public List<Owner> Owners { get; set; }
                public string Address { get; set; }

                public HoldingDetails()
                {
                    Owners = new List<Owner>();
                }

            }
            //public class ValuationDetails : AssessmentFormBase
            //{
            //    /// <summary>
            //    /// score code
            //    /// </summary>
            //    public string ZoneId { get; set; }
            //    /// <summary>
            //    /// score code
            //    /// </summary>
            //    public string NatureOfUseId { get; set; }
            //    /// <summary>
            //    /// score code
            //    /// </summary>
            //    public string ConstructionId { get; set; }
            //    public decimal LandArea_KT { get; set; }
            //    public decimal LandArea_CH { get; set; }
            //    public decimal LandArea_SFT { get; set; }
            //    public decimal BldgArea_SFT { get; set; }
            //    public decimal Plinth_SFT { get; set; }
            //    public decimal LandCost { get; set; }
            //    public decimal Age { get; set; }
            //    public decimal AnnualValuation { get; set; }
            //    public decimal Qtr_Ptax { get; set; }
            //    public decimal Qtr_Schrg { get; set; }
            //    public decimal Qtr_EduCess { get; set; }
            //    public bool SrchgTag { get; set; }
            //    public bool EduCessTag { get; set; }
            //    public int HoldingTypeGroupId { get; set; }
            //}
            public class ValuationDetails : AssessmentFormBase
            {
                /// <summary>
                /// score code
                /// </summary>
                public int HoldingTypeId { get; set; }
                public string ZoneId { get; set; }
                public decimal ZoneVal { get; set; }
                /// <summary>
                /// score code
                /// </summary>
                public string NatureOfUseId { get; set; }
                public decimal NatureOfUseVal { get; set; }
                /// <summary>
                /// score code
                /// </summary>
                public string ConstructionId { get; set; }
                public decimal ConstructionVal { get; set; }
                public decimal LandArea_KT { get; set; }
                public decimal LandArea_CH { get; set; }
                public decimal LandArea_SFT { get; set; }
                public decimal BldgArea_SFT { get; set; }
                public decimal Plinth_SFT { get; set; }
                public decimal LandCost { get; set; }
                public decimal Age { get; set; }
                public decimal AnnualValuation { get; set; }
                public decimal Qtr_Ptax { get; set; }
                public decimal Qtr_Schrg { get; set; }
                public decimal Qtr_EduCess { get; set; }
                public bool SrchgTag { get; set; }
                public bool EduCessTag { get; set; }
                public int HoldingTypeGroupId { get; set; }

                public decimal Waitage { get; set; }
                public decimal Factor { get; set; }
                public decimal DisPer { get; set; }
                public decimal DiscAmt { get; set; }
                public decimal RevAnnualVal { get; set; }
                public decimal PtaxPer { get; set; }
                public decimal Ytax { get; set; }
                public decimal EduCessAmt { get; set; }
                public long AssmtId { get; set; }
                public long AnulValDtlTabId { get; set; }
                public string EditMode { get; set; }
            }
            public class OtherDetails : AssessmentFormBase
            {
                public string BuildingDtl { get; set; }
            }
            public class OtherTabDetails : AssessmentFormBase
            {
                public string PrimaryPhNo { get; set; }
                public string PrimaryEmail { get; set; }
                public decimal ProCosingFees { get; set; }
                public decimal OtherFees { get; set; }
                public DateTime? ApplictnDt { get; set; }
                public int? ApplictnCaseNo { get; set; }
                public DateTime? DtOfOrder { get; set; }
                public string Remarks { get; set; }

            }
            public class AdditionalHoldingDetails : AssessmentFormBase
            {
                public string MaujaName { get; set; }
                public string KhatianNo_RS { get; set; }
                public string KhatianNo_LR { get; set; }
                public string DaagNo_RS { get; set; }
                public string DaagNo_LR { get; set; }
                public string DeedNo { get; set; }
                public DateTime DeedDate { get; set; }
                public long DeedTypeId { get; set; }
                public string HoldingAreaCode { get; set; }
                public bool Tag_Lift { get; set; }
                public bool Tag_Drainage { get; set; }
                public bool Tag_Toilets { get; set; }
                public bool Tag_Electricity { get; set; }
                public bool Tag_Water { get; set; }
                public string Sanction_Bldng_Plan { get; set; }
                public string Adsr_Office { get; set; }
                public decimal Current_Market_Value { get; set; }
                public string BoroughNo { get; set; }
            }

            public void setYear(int yearId)
            {
                this._YearID = yearId;
                if (this.ActualAssmtData != null)
                {
                    this.ActualAssmtData.YearId = yearId;
                }
            }
            public void setValTag(string valTag)
            {
                this.AssessmentValTag = valTag;
                //try
                //{
                //    this._ValuationDetails.AssessmentValTag = valTag;
                //}
                //finally { }
                try
                {
                    this._HoldingDetails.AssessmentValTag = valTag;
                }
                finally { }
                try
                {
                    this._AdditionalHoldingDetails.AssessmentValTag = valTag;
                }
                finally { }
                try
                {
                    this._OtherDetails.AssessmentValTag = valTag;
                }
                finally { }
                if (this.ActualAssmtData != null)
                {
                    this.ActualAssmtData.ValTag = valTag;
                }
            }
            public void setFinYear(string finYear)
            {
                this.FinYear = finYear;
                //try
                //{
                //    this._ValuationDetails.FinYear = finYear;
                //}
                //finally { }
                try
                {
                    this._HoldingDetails.FinYear = finYear;
                }
                finally { }
                try
                {
                    this._AdditionalHoldingDetails.FinYear = finYear;
                }
                finally { }
                try
                {
                    this._OtherDetails.FinYear = finYear;
                }
                finally { }
                if (this.ActualAssmtData != null)
                {
                    this.ActualAssmtData.ValYear = finYear;
                }
            }
            public void setQtr(int qtr)
            {
                this.Qtr = qtr;
                //try
                //{
                //    this._ValuationDetails.Qtr = qtr;
                //}
                //finally { }
                try
                {
                    this._HoldingDetails.Qtr = qtr;
                }
                finally { }
                try
                {
                    this._AdditionalHoldingDetails.Qtr = qtr;
                }
                finally { }
                try
                {
                    this._OtherDetails.Qtr = qtr;
                }
                finally { }
                if (this.ActualAssmtData != null)
                {
                    this.ActualAssmtData.ValQtr = qtr;
                }
            }
            public AssessmentForm()
            {
                _DeedTypes = new List<DeedType>();
                _NatureOfUse = new List<Score>();
                _Constructions = new List<Score>();
                _Zones = new List<Score>();
                _HoldingTypeGroupes = new List<HoldingTypeGroup>();
                _HoldingTypes = new List<HoldingType>();
                 OtherInfos = new List<OtherInfo>();
                MasterData = new MasterData();
            }
        }
        public class MasterData
        {
            public List<OtherInfoType> OtherInfoTypeConfigs { get; set; }
            public MasterData()
            {
                OtherInfoTypeConfigs = new List<OtherInfoType>();
            }
        }
        public class AssmtTransactionModel
        {
            public long TabId { get; set; }
            public int YearId { get; set; }
            public long Idno { get; set; }
            public int Idnosrl { get; set; }
            public int WardId { get; set; }
            public string WardName { get; set; }
            public int LocationID { get; set; }
            public string LocationName { get; set; }
            public string HoldingNo { get; set; }
            public string AssesseeNo { get; set; }
            public string AssesseeName { get; set; }
            public string AssesseeAddress { get; set; }
            public int? ApplNo { get; set; }
            public DateTime? ApplDt { get; set; }
            public DateTime? DtOfOrder { get; set; }
            public decimal? ProcessingFees { get; set; }
            public decimal? OtherFees { get; set; }
            public string PrimaryPhNo { get; set; }
            public string PrimaryEmail { get; set; }
            public string BoroughNo { get; set; }
            public string NameCAC { get; set; }
            public int? HoldingTypeGrp { get; set; }
            public int? HoldingTypeGrpId { get; set; }
            public string HoldArea { get; set; }
            /// <summary>
            /// score code /zone code
            /// </summary>
            public string  ScoreZoneID { get; set; }
            /// <summary>
            /// score code /Use code
            /// </summary>
            public string ScoreUseID { get; set; }
            /// <summary>
            /// score code /cost code
            /// </summary>
            public string ScoreCostID { get; set; }
            public decimal? TotWt { get; set; }
            public Int16? HoldingAge { get; set; }
            public string LandType { get; set; }
            public int? VacLandAreaDc { get; set; }
            public int? VacLandAreaSft { get; set; }
            public int? LandAreaDc { get; set; }
            public int? LandAreaKt { get; set; }
            public int? LandAreaCh { get; set; }
            public int? LandAreaSft { get; set; }
            public int? CoveredArea { get; set; }
            public int? PlinthArea { get; set; }
            public decimal? LandCost { get; set; }
            public decimal? AnnualVal { get; set; }
            public decimal? DiscPer { get; set; }
            public decimal? DiscAmt { get; set; }
            public decimal? RevAnnualVal { get; set; }
            public decimal? PtaxPer { get; set; }
            public decimal? YrProptax { get; set; }
            public decimal? QtrProptax { get; set; }
            public decimal? PrvQtrProptax { get; set; }
            public string SrchgTag { get; set; }
            public decimal? QtrSrchg { get; set; }
            public string EduTag { get; set; }
            public decimal? QtrEduCess { get; set; }
            public string MouzaName { get; set; }
            public string Sanction_Bldng_Plan { get; set; }
            public string Adsr_Office { get; set; }
            public decimal Current_Market_Value { get; set; }
            public string JlNo { get; set; }
            public string PSName { get; set; }
            public string KhatianNoRS { get; set; }
            public string KhatianNoLR { get; set; }
            public string DagNoRS { get; set; }
            public string DagNoLR { get; set; }
            public string LandNatureROR { get; set; }
            public string DeedNo { get; set; }
            public DateTime? DeedDt { get; set; }
            public int? DeedTypeId { get; set; }
            public string WhetherAssessed { get; set; }
            public int? OldYearId { get; set; }
            public decimal? OldAnnualValue { get; set; }
            public decimal? OldPTax { get; set; }
            public string WhetherOwnChange { get; set; }
            public string OldAssesseeName { get; set; }
            public string WhetherHoldingChange { get; set; }
            public int? OldHoldingType { get; set; }
            public string AADRHolding { get; set; }
            public int? ApptTotAreaSft { get; set; }
            public int? OthAreaSft { get; set; }
            public string BuildingDtl { get; set; }
            public string LiftTag { get; set; }
            public string DranageTag { get; set; }
            public string ToiletsTag { get; set; }
            public string ElectricityTag { get; set; }
            public string WaterTag { get; set; }
            public decimal? FerruleSize { get; set; }
            public string ValTag { get; set; }
            public string ValYear { get; set; }
            public int? ValQtr { get; set; }
            public int? ValSrl { get; set; }
            public byte? ValSubSrl { get; set; }
            public string ValFromTo { get; set; }
            public int? FromWardNo { get; set; }
            public int? FromStreetCode { get; set; }
            public string FromHoldingNo { get; set; }
            public int? OldWardNo { get; set; }
            public int? OldStreetCode { get; set; }
            public string OldHoldingNo { get; set; }
            public decimal? Mfactor { get; set; }
            public string MacId { get; set; }
            public string ApprovedFlag { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public int? ApprovedBy { get; set; }
            public string ActiveFlag { get; set; }
            public long? RefTabId { get; set; }
            public int? MunicipalityID { get; set; }
            public int? InsertedBy { get; set; }
            public DateTime? InsertedOn { get; set; }
            public int? UpdatedBy { get; set; }
            public DateTime? UpdatedOn { get; set; }
            public long? MotherHoldingId { get; set; }
            public AssessmentForm ConvertToAssesmentForm()
            {
                var form = new AssessmentForm();
                form.AssessmentValTag = this.ValTag;
                form.FinYear = this.ValYear;
                form.Qtr = this.ValQtr??0;
                form._YearID = this.YearId;
                form._AdditionalHoldingDetails = new AssessmentForm.AdditionalHoldingDetails()
                {
                    AssessmentValTag = this.ValTag,
                    FinYear = this.ValYear,
                    Qtr = this.ValQtr ?? 0,
                    BoroughNo = this.BoroughNo,
                    DaagNo_LR = this.DagNoLR,
                    DaagNo_RS = this.DagNoRS,
                    DeedDate = this.DeedDt ?? new DateTime(),
                    DeedNo = this.DeedNo,
                    DeedTypeId = this.DeedTypeId ?? 0,
                    HoldingAreaCode = this.HoldArea,
                    KhatianNo_LR = this.KhatianNoLR,
                    KhatianNo_RS = this.KhatianNoRS,
                    MaujaName = this.MouzaName,
                    Tag_Drainage = string.Equals(this.DranageTag, "Y") ? true : false,
                    Tag_Electricity = string.Equals(this.ElectricityTag, "Y") ? true : false,
                    Tag_Lift = string.Equals(this.LiftTag, "Y") ? true : false,
                    Tag_Toilets = string.Equals(this.ToiletsTag, "Y") ? true : false,
                    Tag_Water = string.Equals(this.WaterTag, "Y") ? true : false
                };
                form._HoldingDetails = new AssessmentForm.HoldingDetails()
                {
                    AssessmentValTag = this.ValTag,
                    FinYear = this.ValYear,
                    Qtr = this.ValQtr ?? 0,
                    Address = this.AssesseeAddress,
                    HoldingNo = this.HoldingNo,
                    HoldingTypeGroupId = this.HoldingTypeGrp ?? 0,
                    //HoldingTypeId = this.HoldingTypeGrpId ?? 0,
                    LocationId = this.LocationID,
                    LocationName = this.LocationName,
                    WardId = this.WardId,
                    WardName = this.WardId.ToString(),
                    Owners =(string.IsNullOrEmpty(this.AssesseeName)?new List<AssessmentForm.Owner>(): this.AssesseeName.Split(',').Select(t => {
                        AssessmentForm.Owner obj = new AssessmentForm.Owner();
                        obj.AssesseeName = t;
                        obj.AssesseeAddress = this.AssesseeAddress;
                        obj.MunicipalityID = this.MunicipalityID ?? 0;
                        return obj;
                    }).ToList())
                };
                form._ValuationDetail = new AssessmentForm.ValuationDetails() {
                    Age=this.HoldingAge??0,
                    AnnualValuation=this.AnnualVal??0,
                    AssessmentValTag = this.ValTag,
                    FinYear = this.ValYear,
                    Qtr = this.ValQtr ?? 0,
                    BldgArea_SFT=this.CoveredArea ?? 0,
                    ConstructionId=this.ScoreCostID ,
                    HoldingTypeGroupId= this.HoldingTypeGrp ?? 0,
                    LandArea_CH=this.LandAreaCh ?? 0,
                    LandArea_KT=this.LandAreaKt ?? 0,
                    LandArea_SFT=this.LandAreaSft ?? 0,
                    LandCost=this.LandCost ?? 0,
                    NatureOfUseId=this.ScoreUseID ,
                    Plinth_SFT=this.PlinthArea ?? 0,
                    Qtr_Ptax=this.QtrProptax ?? 0,
                    Qtr_Schrg=this.QtrSrchg??0,
                    ZoneId=this.ScoreZoneID,
                    HoldingTypeId=this.HoldingTypeGrpId??0,
                    EduCessTag =string.Equals( this.EduTag,"y",StringComparison.OrdinalIgnoreCase)?true:false,
                    Qtr_EduCess=this.QtrEduCess??0,
                    SrchgTag= string.Equals(this.SrchgTag,"y", StringComparison.OrdinalIgnoreCase) ?true:false,
                    
                };
                form._OtherDetails = new AssessmentForm.OtherDetails()
                {
                    AssessmentValTag = this.ValTag,
                    FinYear = this.ValYear,
                    Qtr = this.ValQtr ?? 0,
                    BuildingDtl=this.BuildingDtl,
                };
                form._OtherTabDetails = new AssessmentForm.OtherTabDetails()
                {
                    OtherFees = this.OtherFees ?? 0,
                    PrimaryEmail = this.PrimaryEmail,
                    PrimaryPhNo = this.PrimaryPhNo,
                    ProCosingFees = this.ProcessingFees ?? 0,
                    ApplictnCaseNo = this.ApplNo,
                    ApplictnDt = (this.ApplDt == null ?  null :this.ApplDt),
                    DtOfOrder = (this.DtOfOrder == null ? null : this.ApplDt ),
                };
                return form;
            }
            public AssessmentForm ConvertToAssesmentFormOtherTabData()
            {
                var form = new AssessmentForm();
                form._OtherTabDetails = new AssessmentForm.OtherTabDetails()
                {
                    OtherFees = this.OtherFees ?? 0,
                    PrimaryEmail = this.PrimaryEmail,
                    PrimaryPhNo = this.PrimaryPhNo,
                    ProCosingFees = this.ProcessingFees ?? 0,
                    ApplictnCaseNo = this.ApplNo,
                    ApplictnDt = (this.ApplDt == null ? null : this.ApplDt),
                    DtOfOrder = (this.DtOfOrder == null ? null:this.ApplDt ),
                };
                return form;
            }
            public void SetValuationCalculationData(CalculatedModel CalculatedModel)
            {
                this.TotWt = CalculatedModel.Waitage;
                this.LandCost = CalculatedModel.LandCost;
                this.AnnualVal = CalculatedModel.AnnualValuation;
                this.DiscPer = CalculatedModel.DisPer;
                this.DiscAmt = CalculatedModel.DiscAmt;
                this.RevAnnualVal = CalculatedModel.RevAnnualVal;
                this.PtaxPer = CalculatedModel.PtaxPer;
                this.YrProptax = CalculatedModel.Ytax;
                this.QtrProptax = CalculatedModel.Qtr_Ptax;
                this.QtrSrchg = CalculatedModel.Qtr_Schrg;
                this.QtrEduCess = CalculatedModel.EduCessAmt;
            }
        }
        public class AssmtTransactionWithValuationList : AssmtTransactionModel
        {
            public List<ValuationDetails> _ValuationDetails { get; set; }
        }
        public class AssmtApprovalBase
        {
            public int YearId { get; set; }
            public List<AssessmentType> AssessmentTypes { get; set; }
            public AssmtApprovalBase()
            {
                AssessmentTypes = new List<AssessmentType>();
            }
        }
        public class AssmtApprovalForm
        {
            public string ValTag { get; set; }
            public List<AssmtTransactionModel> AssmtTransactions { get; set; }
            public AssmtApprovalForm()
            {
                AssmtTransactions = new List<AssmtTransactionModel>();
            }
        }
        public class ApprovalPayload
        {
            public long TabId { get; set; }
            public string  ValYear { get; set; }
            public int ValQtr { get; set; }
        }
    }
}