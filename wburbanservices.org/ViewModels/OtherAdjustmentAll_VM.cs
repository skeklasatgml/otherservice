﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class OtherAdjustmentAll_VM
    {
        public long? MstOthrAdjID { get; set; }
        public int? MunicipalityID { get; set; }

        //public string FinYear { get; set; }
        public int? PayHeadID { get; set; }
        public string PayHeadDesc { get; set; }
        public string RateValueType { get; set; }
        public string RateValue { get; set; }
        public decimal? RateValueWithoutPercent { get; set; }
        public string ApplicableTaxRangeType { get; set;}
        public int? GracePeriodInMonth { get; set; }

        //public string IsCurrentDemand { get; set; }
        // public bool? IsCurrentDemandFlag { get; set; }
        public bool IsPropertyTax { get; set; }
        public bool IsSurcharge { get; set; }        
        public bool IsPropertyTaxInt { get; set; }
        public bool IsSurchargeInt { get; set; }
       
        public string EffectFromDT { get; set; }
        public string EffectToDT { get; set; }
        public bool IsApprovedFlag { get; set; }
        public string IsApproved { get; set; }
        public string ApprovedOn { get; set; }
        public string FromFinYear { get; set; }
        public string FromFinYearQtr { get; set; }
        public string ToFinYear { get; set; }
        public string ToFinYearQtr { get; set; }
        public string Remarks { get; set; }
    }
}