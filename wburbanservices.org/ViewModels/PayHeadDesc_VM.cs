﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class PayHeadDesc_VM
    {
        public int? PayHeadID { get; set; }
        public string PayHeadDesc { get; set; }
    }
}