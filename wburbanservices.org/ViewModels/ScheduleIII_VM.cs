﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class ScheduleIII_VM
    {
        public string AppliWindowId { get; set; }
        public string MunicipalityID { get; set; }
        public string ScheIIIStartDt { get; set; }
        public string ScheIIIDt { get; set; }
        public string MMunicipalityName { get; set; }
        public string Message { get; set; }
        public ResponseStatus Status { get; set; }
        public string ErrorCode { get; set; }
        public List<MMunicipality> MMunicipalityList { get; set; }
        public List<ScheduleIII_VM> ScheduleIIIList { get; set; }
    }
    public class MMunicipality
        {
            public string MunicipalityID { get; set; }
            public string MMunicipalityName { get; set; }
    }
}