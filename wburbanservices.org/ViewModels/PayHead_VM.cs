﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
   
    public class PayHead_VM
    {
        public int? PayHeadID { get; set; }
        public string PayHeadDesc { get; set; }
        public string PayHeadType { get; set; }
        public decimal? PayRate { get; set; }
        public decimal? MinLimit { get; set; }
        public decimal? MaxLimit { get; set; }
        public string ApplicableType { get; set; }
        public string ApprovalLevel { get; set; }
        public int? MunicipalityID { get; set; }
        public string PayHeadBehave { get; set; }
        public int GracePeriodInMonth { get; set; }

    }
}