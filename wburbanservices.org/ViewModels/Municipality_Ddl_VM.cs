﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Municipality_Ddl_VM
    {
        public int MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
    }
}