﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class District_Ddl_VM
    {
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
    }
}