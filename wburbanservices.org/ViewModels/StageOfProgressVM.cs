﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Valuation_Board.Models.Application.StageOfProgress;

namespace Valuation_Board.ViewModels
{
    public class StageOfProgressVM
    {
        public List<StageGroup> StageGroups { get; set; }
        public StageOfProgressVM()
        {
            StageGroups = new List<StageGroup>();
        }
    }
}