﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class ReviewData_VM
    {
        public long ReviewID { get; set; }
        public int MunicipalityID { get; set; }
        public long AssesseeID { get; set; }
        public string FromFinYear { get; set; }
        public string FromQtr { get; set; }
        public string ToFinYear { get; set; }
        public string ToQtr { get; set; }
        public decimal? RPtax { get; set; }
        public decimal? RSch { get; set; }
        public string ApprovedFlag { get; set; }
        public string ReferanceNo { get; set; }
        public string ReferanceText { get; set; }
        public string Status { get; set; }
    }
}