﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class OtherAdjustment
    {
        public long MstOthrAdjID { get; set; }
        public string FinYear { get; set; }
        public string PayHeadDesc { get; set; }
        public string EffectiveFromDt { get; set; }
        public string EffectiveToDt { get; set; }
        public string RateValue { get; set; }
        public string EffectOnCurrentDemand { get; set; }
        public int gracePeriodInMonth { get; set; }
    }
}