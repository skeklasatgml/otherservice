﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class Block_VM
    {
        public int? Bcode  { get; set; }
        public string uni_bname { get; set; }
    }
}