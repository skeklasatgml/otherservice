﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class ReviewApproval_VM
    {
        public long? ReviewID { get; set; }
        public long? AssesseeID { get; set; }
        public int? MunicipalityID { get; set; }
        public string WardName { get; set; }
        public string LocationName { get; set; }
        public long? OldAssesseeNo { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public string AssesseeAddress { get; set; }
        public decimal? PtaxRValue { get; set; }
        public decimal? SurchargeRValue { get; set; }
        public string FromFinYear { get; set; }
        public int FromQtrNo { get; set; }
        public string ToFinYear { get; set; }
        public int ToQtrNo { get; set; }
        public string ApprovedFlag { get; set; }
        public string ApprovedStatus { get; set; }
        public string ApprovedOn { get; set; }
        public string InsertedOn { get; set; }
    }
}