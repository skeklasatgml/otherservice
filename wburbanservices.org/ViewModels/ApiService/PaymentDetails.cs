﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class PaymentDetails
    {
        public int MunicipalityID { get; set; }
        public int WardID { get; set; }
        public int LocationID { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public string AssesseeNo { get; set; }
        public string PaymentMode { get; set; }
        public string ReceiptNo { get; set; }
        public string PaymentCollectionDate { get; set; }
        public decimal PaidAmount { get; set; }
    }
}