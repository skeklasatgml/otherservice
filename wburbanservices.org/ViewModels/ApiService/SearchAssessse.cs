﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class SearchAssessse
    {
        public long ProfileId { get; set; }
        public long AssesseeID { get; set; }
        public int? DistrictID { get; set; }
        public int? MunicipalityID { get; set; }
        public int? WardID { get; set; }
        public int? LocationID { get; set; }
        public int? HoldingTypeID { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public decimal NetAmount { get; set; }
        public string MunicipalityName { get; set; }
        public string WardName { get; set; }
        public string LocationName { get; set; }
    }
}