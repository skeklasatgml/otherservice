﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.ViewModels.ApiService
{
    public class MakePaymentsCollector
    {
        public string Mobile { get; set; }
        public string Email { get; set; }
        public long userID { get; set; }
        public int MunicipalityID { get; set; }
        public int WardID { get; set; }
        public int LocationID { get; set; }
        public long AssesseeID { get; set; }
        public string PaymentDate { get; set; }
        public int? EffectQtrFrom { get; set; }
        public string BillReciptNo { get; set; }
        public DateTime? NoticeServedOn { get; set; }
        public List<PaymentMode> PaymentModes { get; set; }
        public List<TaxPaymentCheckParameter> TaxPaymentCheckParameters { get; set; }
    }
}