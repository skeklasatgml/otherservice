﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class NG_TrackingPermission
    {
        public long TrackedProfileID { get; set; }
        public long TrackerProfileID { get; set; }
        public string TrackingFlag { get; set; }
        public string TrackedProfileName { get; set; }
        public DateTime InsertedOn { get; set; }
    }
}