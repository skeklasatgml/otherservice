﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class NG_OnlineCitizenProfile
    {
        public long ProfileId { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string AuthToken { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}