﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class ComplainNarayangang
    {
        public long ComID { get; set; }
        public int ComType { get; set; }
        public long ProfileID { get; set; }
        public string Com_Header { get; set; }
        public string Com_Body { get; set; }
        public int Com_Status { get; set; }
        public string InsertedOn { get; set; }
        public string UpdatedOn { get; set; }
    }
}