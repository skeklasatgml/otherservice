﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class NG_UserLocation
    {
        public long ProfileID { get; set; }
        public string LocationCoordinates { get; set; }
        public DateTime InsertedOn { get; set; }
    }
}