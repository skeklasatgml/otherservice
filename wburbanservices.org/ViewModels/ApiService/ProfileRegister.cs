﻿namespace Valuation_Board.ViewModels.ApiService
{
    public class ProfileRegister
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
        public string FireBaseID { get; set; }        
    }
}