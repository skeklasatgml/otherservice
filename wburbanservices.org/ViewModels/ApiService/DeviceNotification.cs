﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class DeviceNotification
    {
        public long ProfileID { get; set; }
        public string MobileNo { get; set; }
        public string PaidAmount { get; set; }
        public string NotificationText { get; set; }
    }
}