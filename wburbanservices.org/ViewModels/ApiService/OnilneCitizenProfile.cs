﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.ApiService
{
    public class OnilneCitizenProfile
    {
        public long ProfileId { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string AuthToken { get; set; }
        public DateTime? TokenExpiredOn { get; set; }
        public string TimeZone { get; set; }
        public string OTP { get; set; }
        public bool IsPhoneNoVerified { get; set; }
    }
}