﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.ViewModels.ApiService
{
    public class TaxOutstanding
    {
        public long ProfileId { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int MunicipalityID { get; set; }
        public int WardID { get; set; }
        public int LocationID { get; set; }
        public long AssesseeID { get; set; }
        public string PaymentDate { get; set; }
        public string PG { get; set; }      

        public List<TaxPaymentCheckParameter> TaxPaymentCheckParameters { get; set; }
    }
}