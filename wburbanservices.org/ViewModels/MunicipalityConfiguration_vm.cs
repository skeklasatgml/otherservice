﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.ViewModels
{
    public class MunicipalityConfiguration_vm
    {
        public MunicipalityConfiguration MunicipalityConfiguration { get; set; }
        public List<District> Districts { get; set; }
        public MunicipalityConfiguration_vm()
        {
            Districts = new List<District>();
        }
    }
}