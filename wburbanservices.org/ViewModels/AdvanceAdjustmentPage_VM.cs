﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class AdvanceAdjustmentPage_VM
    {
        //WardId=3&LocationId=501&AssesseeId=68276&AdvanceDate=31/01/2018
        public int? WardId { get; set; }
        public long? AssesseeId { get; set; }
        public int? LocationId { get; set; }
        public DateTime? AdvanceDate { get; set; }

        public string WardName { get; set; }
        public string  LocationName { get; set; }
        public string  AssesseeName { get; set; }

    }
}