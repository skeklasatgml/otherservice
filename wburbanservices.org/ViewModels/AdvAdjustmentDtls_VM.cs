﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class AdvAdjustmentDtls_VM
    {
        public string FinYear { get; set; }
        public int QtrNo { get; set; }
        public decimal TaxValue { get; set; }
        public decimal PaidAmt { get; set; }
        public decimal SurchargeValue { get; set; }
        public decimal PaidSurchargeValue { get; set; }
        public string AdjustedOn { get; set; }

    }
}