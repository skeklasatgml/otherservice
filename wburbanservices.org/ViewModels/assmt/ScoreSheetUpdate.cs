﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.assmt
{
    public class ScoreSheetUpdate
    {
        public long ScoreSheetID { get; set; }
        public decimal ScoreValue { get; set; }
    }
}