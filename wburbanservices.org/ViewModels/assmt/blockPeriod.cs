﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.assmt
{
    public class blockPeriod
    {
        public int? MunicipalityID { get; set; }
        public int? YearId { get; set; }
        public int FinYear { get; set; }
        public int Qtr { get; set; }
        public string Period { get; set; }
        public DateTime EffectiveDateForm { get; set; }
        public DateTime EffectiveDateTo { get; set; }
        public decimal DiscPer { get; set; }
        public decimal Factor { get; set; }
        public decimal MinVal { get; set; }
        public decimal MaxPtaxPer { get; set; }
        public decimal AddPtaxPer1 { get; set; }
        public decimal AddPtaxPer2 { get; set; }

        public long CurrentUserId { get; set; }
        
    }
}