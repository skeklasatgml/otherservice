﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels.assmt
{
    public class BeltSheetUpdate
    {
        public long BeltSheetID { get; set; }
        public decimal From { get; set; }
        public decimal To { get; set; }
        public string BeltValue { get; set; }
    }
}