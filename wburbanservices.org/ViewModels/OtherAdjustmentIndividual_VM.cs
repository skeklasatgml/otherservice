﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.ViewModels
{
    public class OtherAdjustmentIndividual_VM : OtherAdjustmentAll_VM
    {
        public long? DtlID { get; set; }
        public long? AssesseeID { get; set; }
        public string AssesseeName { get; set; }
        public long? fk_MstOthrAdjID { get; set; }
        public bool? IsPayAdjustedFlag { get; set; }
        public string IsPayAdjusted { get; set; }
        public string PayAdjustedOn { get; set; }
        
    }
}