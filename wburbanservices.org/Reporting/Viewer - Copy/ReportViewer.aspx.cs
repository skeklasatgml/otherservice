﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.Reporting.Viewer
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        #region
        //url format to avail this page=>http://localhost/Reporting/Viewer/ReportViewer.aspx?ReportCode=2&Params=Encrypted({AssesseeID:1})
        private IEncryptDecrypt chiperAlgorithm;

        #endregion
        
        public ReportViewer()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ReadQueryString();
        }
        private void ReadQueryString()
        {
            Dictionary<string, object> ReportParams = new Dictionary<string, object>();
            int ReportCode = 0;
            string pramJSON = null;
            try
            {
                ReportCode = Convert.ToInt32(Request.QueryString["ReportCode"]);
                string v = Request.Url.ToString();
                if (Request.QueryString["Params"] != null)
                {
                    string htmlDecodedChiperText = HttpUtility.UrlDecode(Convert.ToString(Request.QueryString["Params"])).Replace(" ", "+");//plush need to replaced after decode string to get actuall encoded value
                                                                                                                                            // string htmlDecodedChiperText = Convert.ToString(Request.QueryString["Params"]);

                    pramJSON = chiperAlgorithm.Decrypt(htmlDecodedChiperText);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ReportParams = js.Deserialize<Dictionary<string, object>>(pramJSON);
                }
                InitilizeReportData(ReportCode, ReportParams);
            }
            catch (FormatException)
            {
                ShowError("Parameters Malformed: Report paramater UnReadable");
            }
            catch (CrystalReportsException ex)
            {
                ShowError("Crystal Report Error: <br/>" + ex.Message);
            }
            catch (SecurityException ex)
            {
                ShowError("Secutirty Exception : <br/>" + ex.Message);
            }
            catch (FileNotFoundException ex)
            {
                ShowError("File not found Exception : <br/>" + ex.Message);
            }
            catch (SqlException)
            {
                ShowError("Database Exception: Error not exposable");
            }
            catch (Exception ex)
            {
                ShowError("Unknown Error: Please contact with vendor. <br/>"+ex.Message);
            }
        }
        void ShowError(string message)
        {
            Response.Write(string.Format("<center><b><span style='color:red;font-weight:bold'>{0}</span></b></center>", message));
        }
        private void InitilizeReportData(int ReportCode, Dictionary<string, object> ReportParams)
        {
            try
            {
                //init 
                Report objRpt = new Reporting_BLL().GetReport(ReportCode);
                if (objRpt == null)
                {
                    throw new FileNotFoundException("Report not Registerd");
                }

                //validation
                //CheckAuthorisation(ReportCode, GetUserID());
                //showreport call

                ShowReport(objRpt, ReportParams);
            }
            catch
            {
                throw;
            }
            


        }
        //[obsolete]
        private void CheckAuthorisation(int ReportCode, long UserID)
        {
            Reporting_BLL rptBll = new Reporting_BLL();
            if (!rptBll.CheckUserAuthorisation(ReportCode, UserID))
            {
                throw new SecurityException("User not Authorised for this report");
            }
        }
        private void ShowReport(Report _reportObj, Dictionary<string, object> _params)
        {
            ReportDocument report = new ReportDocument();
            try
            {
                //ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/" + _reportObj.ReportFilePath), _reportObj.ReportName));
                foreach (var item in _params)
                {
                    report.SetParameterValue("@" + item.Key, item.Value);
                }

                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["constring"].ConnectionString);

                string server = builder.DataSource;
                string database = builder.InitialCatalog;

                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                //crConnectionInfo.ServerName = @"DESKTOP-3U91EIO\MSSQLSERVER_2014";
                crConnectionInfo.ServerName = builder.DataSource;
                crConnectionInfo.DatabaseName = builder.InitialCatalog;
                crConnectionInfo.UserID = builder.UserID;
                crConnectionInfo.Password = builder.Password;
                TableLogOnInfo crTableLogoninfo = new TableLogOnInfo();

                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in report.Database.Tables)
                {
                    crTableLogoninfo = CrTable.LogOnInfo;
                    crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crTableLogoninfo);
                }
                foreach (ReportDocument subreport in report.Subreports)
                {
                    foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in subreport.Database.Tables)
                    {
                        crTableLogoninfo = CrTable.LogOnInfo;
                        crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                        CrTable.ApplyLogOnInfo(crTableLogoninfo);
                    }
                }
                report.Refresh();
                //CrystalReportViewer1.ReportSource = report;

                //CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                ExportFormatType formatType = ExportFormatType.NoFormat;
                formatType = ExportFormatType.PortableDocFormat;
                //switch (rbFormat.SelectedItem.Value)
                //{
                //    case "Excel":
                //        formatType = ExportFormatType.Excel;
                //        break;
                //    case "PDF":

                //        break;
                //}
                report.ExportToHttpResponse(formatType, Response, false, _reportObj.ReportName.Replace(".rpt",""));

                //report.Close();
                //report.Dispose();
                //Response.End();
            }
            catch (FileNotFoundException)
            {
                throw new FileNotFoundException("Report Physical File not Found");
            }
            catch (CrystalReportsException)
            {
                throw;
            }
            catch
            {
                throw;
            }
            finally
            {
                report.Close();
                report.Dispose();
                Response.End();
            }

        }
        //[obsolete]
        long GetUserID()
        {
            try
            {
                MunicipalityUser user = (MunicipalityUser)SessionContext.CurrentUser;
                return user.UserID;
            }
            catch (NullReferenceException)
            {
                throw new SecurityException("Authentication Required");
            }
        }
    }
}