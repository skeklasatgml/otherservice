﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using Valuation_Board.Models.Report;
using Valuation_Board.App_Codes.BLL;
using ClosedXML.Excel;
//using Valuation_Board.Models.Report;
using Excel = Microsoft.Office.Interop.Excel;

namespace Valuation_Board.Reporting.Viewer
{
    public class FileCreator
    {
        public string GetChallanExcellFile(Dictionary<string, object> Params)
        {
            CollectionChallanBll cc = new CollectionChallanBll();
            List<Collection> CLst = cc.GetCollectionDtls(DtF: Convert.ToString(Params["DateFrom"]), DtT: Convert.ToString(Params["DateTo"]),
                WID: Convert.ToInt32(Params["WardID"]), LID: Convert.ToInt32(Params["LocationID"]), PType: Convert.ToString(Params["PaymentTyp"]),
                Counter: Convert.ToString(Params["Counter"]), Muid: Convert.ToInt32(Params["MunicipalityID"]));

            //string FileName = "Challan_" + Params["MunicipalityID"] + ".xlsx";
            string FileName = "Challan_" + Params["MunicipalityID"] + ".xls";
            //string FileName = HttpContext.Current.Server.MapPath("~/Municipality/MnReports/" + FileName);
            string sName = "Challan";
            //return CreateXMLExcelFile(ConvertToDTable(CLst),FileName, sName);
            return CreateGeneralExcelFile(CLst, FileName, sName);
        }
        private string CreateXMLExcelFile(DataTable dt, string fname,string sheetName)
        {
            string folderPath = AppDomain.CurrentDomain.BaseDirectory ;
            string fPath = "Municipality\\ExportFiles\\";
            if (!Directory.Exists(folderPath + fPath))
            {
                Directory.CreateDirectory(folderPath + fPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, sheetName);
                wb.SaveAs(folderPath + fPath + fname);
            }
            return ("\\" + fPath + fname);
        }
        private DataTable ConvertToDTable(List<Collection> lst)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Receipt Date", typeof(System.String));
            dt.Columns.Add("Receipt No.", typeof(System.String));
            dt.Columns.Add("Old Receipt No.", typeof(System.String));
            dt.Columns.Add("Name Of Assessee", typeof(System.String));
            dt.Columns.Add("Ward Name", typeof(System.String));
            dt.Columns.Add("Holding No.", typeof(System.String));
            dt.Columns.Add("Location", typeof(System.String));
            dt.Columns.Add("Arrear Prop Tax", typeof(System.String));
            dt.Columns.Add("Arrear Surcharge", typeof(System.String));
            dt.Columns.Add("1st Qtr Prop Tax", typeof(System.String));
            dt.Columns.Add("1st Qtr Surcharge", typeof(System.String));
            dt.Columns.Add("2nd Qtr Prop Tax", typeof(System.String));
            dt.Columns.Add("2nd Qtr Surcharge", typeof(System.String));
            dt.Columns.Add("3rd Qtr Prop Tax", typeof(System.String));
            dt.Columns.Add("3rd Qtr Surcharge", typeof(System.String));
            dt.Columns.Add("4th Qtr Prop Tax", typeof(System.String));
            dt.Columns.Add("4th Qtr Surcharge", typeof(System.String));
            dt.Columns.Add("Current Total Tax", typeof(System.String));
            dt.Columns.Add("Current Total Surcharge", typeof(System.String));
            dt.Columns.Add("Rebate", typeof(System.String));
            dt.Columns.Add("Interest", typeof(System.String));
            dt.Columns.Add("Round Off Adjusted", typeof(System.String));
            dt.Columns.Add("Adjusted Amount", typeof(System.String));
            dt.Columns.Add("Collector Adj. Amount", typeof(System.String));
            dt.Columns.Add("Advance Amount", typeof(System.String));
            dt.Columns.Add("Net Amount", typeof(System.String));

            foreach (Collection c in lst)
            {
                DataRow dr = dt.NewRow();
                dr[0] = c.PaymentReceiveDate;
                dr[1] = c.receiptNo;
                dr[2] = c.BillReceiptNo;
                dr[3] = c.AssesseeName;
                dr[4] = c.WardName;
                dr[5] = c.HoldingNo;
                dr[6] = c.LocationName;
                dr[7] = c.Arr_tax;
                dr[8] = c.Arr_sur;
                dr[9] = c.Qtr1_Ptax;
                dr[10] = c.Qtr1_sur;
                dr[11] = c.Qtr2_Ptax;
                dr[12] = c.Qtr2_sur;
                dr[13] = c.Qtr3_Ptax;
                dr[14] = c.Qtr3_sur;
                dr[15] = c.Qtr4_Ptax;
                dr[16] = c.Qtr4_sur;
                dr[17] = c.TotalTax;
                dr[18] = c.TotalSur;
                dr[19] = c.Rebate;
                dr[20] = c.Interest;
                dr[21] = c.RoundOffAdjust;
                dr[22] = c.AdjustedAmount;
                dr[23] = c.CollectorAdjustedAmount;
                dr[24] = c.AdvanceAmount;
                dr[25] = c.NetAmount;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private string CreateGeneralExcelFile(List<Collection> lst, string fname, string sheetName)
        {
            string fPath = "Municipality\\ExportFiles\\";
            try
            {
                string folderPath = AppDomain.CurrentDomain.BaseDirectory;
                if (!Directory.Exists(folderPath + fPath))
                {
                    Directory.CreateDirectory(folderPath + fPath);
                }

                int i = 1;
                int j = i;

                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                
                Excel.Range rgB = xlWorkSheet.Cells[1];
                rgB.EntireRow.Font.Bold = true;
                
                if (i == 1 && j == 1)
                {
                    Excel.Range rg = xlWorkSheet.Cells[i, j];
                    rg.EntireColumn.NumberFormat = "mm/dd/yyyy";
                    xlWorkSheet.Cells[i, j] = "Receipt Date";
                    xlWorkSheet.Cells[i, ++j] = "Receipt No.";
                    xlWorkSheet.Cells[i, ++j] = "Old Receipt No.";
                    xlWorkSheet.Cells[i, ++j] = "Name Of Assessee";
                    xlWorkSheet.Cells[i, ++j] = "Ward Name";
                    xlWorkSheet.Cells[i, ++j] = "Holding No.";
                    Excel.Range rgH = xlWorkSheet.Cells[i, j];
                    rgH.EntireColumn.NumberFormat = "@";
                    xlWorkSheet.Cells[i, ++j] = "Location";
                    xlWorkSheet.Cells[i, ++j] = "Arrear Prop Tax";
                    xlWorkSheet.Cells[i, ++j] = "Arrear Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "1st Qtr Prop Tax";
                    xlWorkSheet.Cells[i, ++j] = "1st Qtr Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "2nd Qtr Prop Tax";
                    xlWorkSheet.Cells[i, ++j] = "2nd Qtr Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "3rd Qtr Prop Tax";
                    xlWorkSheet.Cells[i, ++j] = "3rd Qtr Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "4th Qtr Prop Tax";
                    xlWorkSheet.Cells[i, ++j] = "4th Qtr Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "Current Total Tax";
                    xlWorkSheet.Cells[i, ++j] = "Current Total Surcharge";
                    xlWorkSheet.Cells[i, ++j] = "Rebate";
                    xlWorkSheet.Cells[i, ++j] = "Interest";
                    xlWorkSheet.Cells[i, ++j] = "Round Off Adjusted";
                    xlWorkSheet.Cells[i, ++j] = "Adjusted Amount";
                    xlWorkSheet.Cells[i, ++j] = "Collector Adj. Amount";
                    xlWorkSheet.Cells[i, ++j] = "Advance Amount";
                    xlWorkSheet.Cells[i, ++j] = "Net Amount";
                }

                xlWorkSheet.Cells[i, j].Style.Font.Italic = true;
                j = i;
                foreach (Collection row in lst)
                {
                    i++;
                    xlWorkSheet.Cells[i, j] = row.PaymentReceiveDate.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.receiptNo.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.BillReceiptNo.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.AssesseeName.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.WardName.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.HoldingNo.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.LocationName.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Arr_tax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Arr_sur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr1_Ptax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr1_sur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr2_Ptax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr2_sur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr3_Ptax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr3_sur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr4_Ptax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Qtr4_sur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.TotalTax.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.TotalSur.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Rebate.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.Interest.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.RoundOffAdjust.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.AdjustedAmount.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.CollectorAdjustedAmount.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.AdvanceAmount.ToString();
                    xlWorkSheet.Cells[i, ++j] = row.NetAmount.ToString();
                    j = 1;
                }

                if (!fname.Equals(""))
                {
                    string fileName = folderPath + fPath + fname;
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                    xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlNoChange, misValue, misValue, misValue, misValue, misValue);

                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();
                }
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ("\\" + fPath + fname); ;
        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                throw new Exception(ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}