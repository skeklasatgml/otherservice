﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes.Events
{
    public class LogInLogOut_EventArgs:EventArgs
    {
        public string SessionID { get; set; }
        public object User { get; set; }
    }
}