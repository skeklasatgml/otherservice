﻿using System;
using System.Security;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
namespace Valuation_Board.App_Codes.Authenticator
{

    public class MunicipalityAuthenticator : IAuthentication<MunicipalityUser>
    {
        private MunicipalityLogin _userLogin;
        MunicipalityUserBll mnAuth = new MunicipalityUserBll();
        public MunicipalityAuthenticator(MunicipalityLogin userLogin)
        {
            this._userLogin = userLogin;
        }

        public MunicipalityUser Authenticate()
        {
            if (_userLogin == null || string.IsNullOrWhiteSpace(_userLogin.UserName) || string.IsNullOrWhiteSpace(_userLogin.Password) || _userLogin.MunicipalityID==0)
            {
                throw new SecurityException("User Name, password or Municipality ID Can not be Empty");
            }
            MunicipalityUser muObj = mnAuth.GetUser(_userLogin.UserName, _userLogin.Password,_userLogin.MunicipalityID);
            if (muObj == null)
            {
                throw new SecurityException("Invalid User Name or password");
            }
            if(muObj.IsAccountLocked==true)
            {
                throw new SecurityException("User Account Locked! Please Contact to Admnistrator");
            }
            return muObj;
        }
    }
   
}