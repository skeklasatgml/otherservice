﻿using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using System;
namespace Valuation_Board.App_Codes.Authenticator
{
    public class OtherCitizenAuthenticator : IAuthentication<OtherCitizenUser>
    {
        private UserLogin _userLogin;
        OtherCitizenUserBll vuBll = new OtherCitizenUserBll();
        public OtherCitizenAuthenticator(UserLogin userLogin)
        {
            this._userLogin = userLogin;
        }

        public OtherCitizenUser Authenticate()
        {
            if (_userLogin == null)
            {
                throw new ArgumentException("User Name and password Can not be Empty");
            }
            OtherCitizenUser vuObj = vuBll.GetUser(_userLogin.UserName, _userLogin.Password);
            if (_userLogin == null)
            {
                throw new ArgumentException("Invalid User Name or password");
            }
            return vuObj;
        }
    }

}