﻿using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using System;
namespace Valuation_Board.App_Codes.Authenticator
{
    public class AdministrativedAuthenticator : IAuthentication<AdministrativeUser>
    {
        private AdministrativeLogin _userLogin;
        AdministrativeUserBll vuBll = new AdministrativeUserBll();
        public AdministrativedAuthenticator(AdministrativeLogin userLogin)
        {
            this._userLogin = userLogin;
        }

        public AdministrativeUser Authenticate()
        {
            if (_userLogin == null)
            {
                throw new ArgumentException("User Name and password Can not be Empty");
            }
            AdministrativeUser vuObj = vuBll.GetUser(_userLogin.UserName, _userLogin.Password, _userLogin.AdminType);
            if (_userLogin == null)
            {
                throw new ArgumentException("Invalid User Name or password");
            }
            return vuObj;
        }
    }

}