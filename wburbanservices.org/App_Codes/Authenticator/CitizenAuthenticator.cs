﻿using System;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.Authenticator
{
    public class CitizenAuthenticator : IAuthentication<CitizenUser>
    {
        CitizenLogin _userLogin;
        AssesseeBll ctAuth = new AssesseeBll();
        public CitizenAuthenticator(CitizenLogin loginObject)
        {
            _userLogin = loginObject;
        }

        public CitizenUser Authenticate()
        {
            if (_userLogin == null)
            {
                throw new ArgumentException("Please input all mandatory field [Municipality,Ward,Location,HoldingNo]");
            }
            Assessee muObj = ctAuth.GetAssessee(_userLogin.MunicipalityID,_userLogin.WardID,_userLogin.LocationID,_userLogin.HoldingNo);
            if (muObj == null)
            {
                throw new ArgumentException("No Assessee Found");
            }
            return muObj.Convert();
        }
    }
}