﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation_Board
{
    public static  class AppSettings
    {
        public static string FileUploadDir { get { return getValue("FileUploadDir"); } }
        public static string ChiperKey { get { return getValue("chiperKey"); } }
        public static string SrvcPwdCourtCase { get { return getValue("password.service.court-case",true); } }
        static string getValue(string key, bool handleException=false)
        {
                if(handleException && ConfigurationManager.AppSettings.AllKeys.Contains(key)==false)
                {
                    throw new Exception(string.Format("Configuration Key [{0}] not found", key));
                }
                return ConfigurationManager.AppSettings[key] as string;
        }
    }
    
}
