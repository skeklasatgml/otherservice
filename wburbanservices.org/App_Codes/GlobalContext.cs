﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Valuation_Board.Models.AppResource;

namespace Valuation_Board.App_Codes
{
    public static class GlobalContext
    {
        private static Dictionary<string, object> _loggedInUsers;
        private static ApplicationInfo _applicationInfo;
        static GlobalContext()
        {
            _loggedInUsers = HttpContext.Current.Application[KeysLibrary.GlobalKeys.LoggedInUsers_Key] as Dictionary<string, object>;
            if (_loggedInUsers == null)
            {
                _loggedInUsers = new Dictionary<string, object>();
                HttpContext.Current.Application[KeysLibrary.GlobalKeys.LoggedInUsers_Key] = _loggedInUsers;
            }

            
        }
        public static void Init()
        {
            //load application infos
            _applicationInfo = HttpContext.Current.Application[KeysLibrary.GlobalKeys.ApplicationInfo_Key] as ApplicationInfo;
            if (_applicationInfo == null)
            {
                _applicationInfo = new ApplicationInfo();
                _applicationInfo.AssemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                HttpContext.Current.Application[KeysLibrary.GlobalKeys.ApplicationInfo_Key] = _applicationInfo;
            }
        }
        public static Dictionary<string, object> LoggedInUsers
        {
            get
            {
                return _loggedInUsers;

            }
           private set {
                _loggedInUsers = value;
            }
        }
        public static ApplicationInfo applicationInfo
        {
            get
            {
                return _applicationInfo;
            }
        }
    }
}