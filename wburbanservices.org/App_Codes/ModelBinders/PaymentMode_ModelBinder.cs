﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.ModelBinders
{
    public class PaymentMode_ModelBinder:DefaultModelBinder
    {
      
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(List<PaymentMode>))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;

                var x = bindingContext.ValueProvider.GetValue("ModeType");
                //as json request, data will be in inputStream object
                Stream req = request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();
                Dictionary<string, object> dicMainInput = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(json);
                List<Dictionary<string, object>> lstPaymentModes = new JavaScriptSerializer().ConvertToType<List<Dictionary<string, object>>>(dicMainInput["PaymentModes"]);

                List<PaymentMode> lstTypedPaymentModes = new List<PaymentMode>();
                foreach (var item in lstPaymentModes)
                {
                    int modeType = Convert.ToInt32(item["ModeType"]);
                    bool HasInstrument = Convert.ToBoolean(item["HasInstrument"]);
                    string PaymentModeCode = item["PaymentModeCode"].ToString();
                    string PaymentModeDescription = item["PaymentModeDescription"].ToString();
                    decimal TotalAmount = Convert.ToDecimal(item["TotalAmount"]);
                    List<Dictionary<string, object>> PaymentInstruments = new JavaScriptSerializer().ConvertToType<List<Dictionary<string, object>>>(item["PaymentInstruments"]);

                    List<PaymentInstrumentBase> lstTypeInstruments = new List<PaymentInstrumentBase>();

                    switch (modeType)
                    {
                        case 1:
                        case 3:
                            {
                                PaymentInstruments.ForEach(t =>
                                {
                                    decimal InstrumentAmount = Convert.ToDecimal(t["InstrumentAmount"]);
                                    int ModeType = Convert.ToInt32(t["ModeType"]);
                                    PaymentInstrumentBase instBase = new PaymentInstrumentBase() { InstrumentAmount = InstrumentAmount, ModeType = ModeType };
                                    lstTypeInstruments.Add(instBase);
                                });
                                break;
                            }
                        case 2:
                            {
                                PaymentInstruments.ForEach(t =>
                                {
                                    decimal InstrumentAmount = Convert.ToDecimal(t["InstrumentAmount"]);
                                    int ModeType = Convert.ToInt32(t["ModeType"]);
                                    string BankName = t["BankName"].ToString();
                                    DateTime InstrumentDate = Convert.ToDateTime(t["InstrumentDate"]);
                                    string InstrumentNo = t["InstrumentNo"] as string;
                                    PaymentInstrument instBase = new PaymentInstrument() { InstrumentAmount = InstrumentAmount, ModeType = ModeType, BankName = BankName, InstrumentDate = InstrumentDate, InstrumentNo = BankName = InstrumentNo };
                                    lstTypeInstruments.Add(instBase);
                                });
                                break;
                            }

                        case 4:
                            {
                                PaymentInstruments.ForEach(t =>
                                {
                                    decimal InstrumentAmount = Convert.ToDecimal(t["InstrumentAmount"]);
                                    int ModeType = Convert.ToInt32(t["ModeType"]);
                                    string BankName = t["BankName"].ToString();
                                    DateTime InstrumentDate = Convert.ToDateTime(t["InstrumentDate"]);
                                    string InstrumentNo = t["InstrumentNo"] as string;
                                    string FromAccountNo = t["FromAccountNo"] as string;
                                    PaymentInsturmentType4 instBase = new PaymentInsturmentType4() { InstrumentAmount = InstrumentAmount, ModeType = ModeType, BankName = BankName, InstrumentDate = InstrumentDate, InstrumentNo = BankName = InstrumentNo, FromAccountNo = FromAccountNo };
                                    lstTypeInstruments.Add(instBase);
                                });
                                break;
                            }
                        case 5:
                            {
                                PaymentInstruments.ForEach(t =>
                                {
                                    decimal InstrumentAmount = Convert.ToDecimal(t["InstrumentAmount"]);
                                    int ModeType = Convert.ToInt32(t["ModeType"]);
                                    string BankName = t["BankName"].ToString();
                                    DateTime InstrumentDate = Convert.ToDateTime(t["InstrumentDate"]);
                                    string InstrumentNo = t["InstrumentNo"] as string;
                                    string CardNo = t["CardNo"] as string;
                                    PaymentInsturmentType5 instBase = new PaymentInsturmentType5() { InstrumentAmount = InstrumentAmount, ModeType = ModeType, BankName = BankName, InstrumentDate = InstrumentDate, InstrumentNo = BankName = InstrumentNo, CardNo = CardNo };
                                    lstTypeInstruments.Add(instBase);
                                });
                                break;
                            }
                        default:
                            {
                                //throw new ArgumentException("Error Location: Custom ModelBinder(PaymentMode_ModelBinder).Payment Instrument Type not supported. Please contact with vendor. ");
                                break;
                            }
                    }
                    PaymentMode p = new PaymentMode() { HasInstrument = HasInstrument, ModeType = modeType, PaymentModeCode = PaymentModeCode, PaymentModeDescription = PaymentModeDescription, TotalAmount = TotalAmount, PaymentInstruments = lstTypeInstruments };
                    lstTypedPaymentModes.Add(p);
                }
                return lstTypedPaymentModes;
            }
            else
            {
               return base.BindModel(controllerContext, bindingContext);
            }
        }
    }
}