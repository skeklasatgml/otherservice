﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.ModelBinders
{
    public class CustomModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            if(modelType.IsAssignableFrom(typeof(List<PaymentMode>)))
            {
                return new PaymentMode_ModelBinder();
            }
            return null;
        }
    }
}