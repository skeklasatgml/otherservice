﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valuation_Board.App_Codes
{
   public interface IAuthentication<T>
    {
       T Authenticate();
    }
}
