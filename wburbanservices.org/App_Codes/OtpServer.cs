﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.BLL;
using static Valuation_Board.Models.Application.ScheduleThree;

namespace Valuation_Board.App_Codes
{
    public class OtpServer
    {
        DbHandler _db = null;
        SqlConnection cn;
        public string _userIdentifire { get; set; }
        public OtpServer(string userIdentifire)
        {
            this._userIdentifire = userIdentifire;
            _db = new DbHandler();
            cn = new SqlConnection(_db.ConnectionString);
            
        }
        public RequestOtp RequerstOTP(string mobileNo)
        {
            try
            {
                int OtpExpireAfterMinutes = 0;
                int otpNextSendAfterMinutes = 0;
                OtpExpireAfterMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["OtpExpireAfterMinutes"]);
                otpNextSendAfterMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["otpNextSendAfterMinutes"]);
                var cmd = _db.NewCommand("Insert_SendOtp", cn);
                cmd.Parameters.AddWithValue("@identifire", this._userIdentifire);
                cmd.Parameters.AddWithValue("@mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("@expireAfterMinites", OtpExpireAfterMinutes);
                cmd.Parameters.AddWithValue("@nextSendAfterMinutes", otpNextSendAfterMinutes);
                cmd.Parameters.AddWithValue("@otpVal", new Random().Next(10000000, 99999999));//8 digit random number
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                if (dt.Rows.Count > 0)
                {
                    return dt.AsEnumerable().Select(t => {
                        return new RequestOtp() {
                            ExpiredOn=t.Field<DateTime?>("ExpiredOn"),
                            NextOtpSendOn= t.Field<DateTime?>("NextSendAfter"),
                            MobileNo = t.Field<string>("MobileNo")
                        };
                    }).ToList()[0];
                }
                throw new InvalidProgramException("OTP can not be send, unknown error");

            }
            catch (FormatException)
            {
                throw new InvalidOperationException("Otp expiry timestamp or next send timestamp is not configured on Configuration");
            }
            finally
            {
                cn.Close();
            }
        }
        public bool Verify(string otpValue,string mobile)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_verify_OtpOnServer", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@otpVal", otpValue);
                cmd.Parameters.AddWithValue("@identifire", this._userIdentifire);
                cmd.Parameters.AddWithValue("@mobileNo", mobile);
                var val = _db.GetScalar<bool>(cmd);
                if (val == true)
                {
                    return val;
                }
                else
                {
                    throw new InvalidOperationException("OTP not verified");
                }
            }catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}