﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using static Valuation_Board.ViewModels.Assessment_VM;

namespace Valuation_Board.App_Codes.BLL
{
    public class MovieBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        DbHandler _db;
        SqlConnection cn;
        public MovieBll()
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public List<MovieMD.Surface> GetTypeofSurface(int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SURFACE_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Surface(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.RoadType> GetTypeofRoad(string SurfaceID, int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SurfaceID", SurfaceID);
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_ROAD_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_TypeofRoad(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public string GetApplicantName(long UserID, int UserType, int? ApplicationId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (UserType == 4)
            {
                cmd.Parameters.AddWithValue("@ApplicantID", UserID);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS");
            }
            else if (UserType == 2)
            {
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS_MUNICIPALITY");
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string NAME = dt.Rows[0]["NAME"].ToString();
                    string DESIGNATION = dt.Rows[0]["DESIGNATION"].ToString();
                    string COMPANYNAME = dt.Rows[0]["COMPANYNAME"].ToString();
                    string MOBILENO = dt.Rows[0]["MOBILENO"].ToString();

                    return NAME + ", " + DESIGNATION + ", " + COMPANYNAME + ", " + MOBILENO;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Municipality> GetMunicipality(string Desc, int subModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_MUNICIPALITY");
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SubModuleID", subModuleId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Municipality(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Purposes> GetPurpose(int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_PURPOSE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Purposes(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Specification> GetSpecification(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATION");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Specification> GetSpecificationbyID(string SpecificationID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SpecificationID", SpecificationID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATIONBYID");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public Boolean? GetModuleDetail(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SUBMODULE_DETAILS");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Boolean ISDetailMultiple = Convert.ToBoolean(dt.Rows[0]["ISDetailMultiple"].ToString());
                    return ISDetailMultiple;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.UploadDetail> GetUploadDetail(int SubModuleId, int? ApplicationId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_UPLOAD_DETAILS");
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_UploadDetail(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Ward> GetWard(string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Ward(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Location> GetLocationByWard(string WardID, string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_LOCATION_BY_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Location(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Action> GetDesignation(int? ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Desig(dt.Tables[0]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Action> GetDesignationByAction(string ActionID, string ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID == "" ? DBNull.Value : (object)ApplicationmID);
            cmd.Parameters.AddWithValue("@ActionType", ActionID == "" ? DBNull.Value : (object)ActionID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Desig(dt.Tables[1]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Action> GetAction(int? ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Action(dt.Tables[0]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Inspector> GetInspectorName(string MunicipalityID, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("UDServices.InspectorSchedule_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LOAD_INSPECTOR");
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            //cmd.Parameters.AddWithValue("@InspectionOnFrom", FromDate);
            //cmd.Parameters.AddWithValue("@InspectionOnTo", ToDate);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Inspector(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MovieMD.Holding> GetHoldingNo(string WardID, string MunicipalityID, string LocationID, string Desc)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@LocationID", LocationID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_HOLDING");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Holding(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converters
        public List<MovieMD.Surface> Convert_Surface(DataTable dt)
        {
            List<MovieMD.Surface> lst = new List<MovieMD.Surface>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Surface();
                obj.SurfaceID = t.Field<int?>("SurfaceID");
                obj.SurfaceName = t.Field<string>("SurfaceName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.RoadType> Convert_TypeofRoad(DataTable dt)
        {
            List<MovieMD.RoadType> lst = new List<MovieMD.RoadType>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.RoadType();
                obj.RoadTypeID = t.Field<int?>("RoadTypeID");
                obj.RoadTypeName = t.Field<string>("RoadTypeName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Municipality> Convert_Municipality(DataTable dt)
        {
            List<MovieMD.Municipality> lst = new List<MovieMD.Municipality>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Municipality();
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.MunicipalityName = t.Field<string>("MunicipalityName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Purposes> Convert_Purposes(DataTable dt)
        {
            List<MovieMD.Purposes> lst = new List<MovieMD.Purposes>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Purposes();
                obj.TypeID = t.Field<int?>("TypeID");
                obj.Purpose = t.Field<string>("Purpose");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Specification> Convert_Specification(DataTable dt)
        {
            List<MovieMD.Specification> lst = new List<MovieMD.Specification>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Specification();
                obj.ID = t.Field<long?>("ID");
                obj.Description = t.Field<string>("Description");
                obj.SpecificationRWidth = t.Field<Boolean?>("SpecificationRWidth");
                obj.SpecificationRDepth = t.Field<Boolean?>("SpecificationRDepth");
                obj.SpecificationPNo = t.Field<Boolean?>("SpecificationPNo");
                obj.SpecificationPLength = t.Field<Boolean?>("SpecificationPLength");
                obj.SpecificationPWidth = t.Field<Boolean?>("SpecificationPWidth");
                obj.SpecificationPDepth = t.Field<Boolean?>("SpecificationPDepth");
                obj.SpecificationTDiameter = t.Field<Boolean?>("SpecificationTDiameter");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.UploadDetail> Convert_UploadDetail(DataTable dt)
        {
            List<MovieMD.UploadDetail> lst = new List<MovieMD.UploadDetail>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.UploadDetail();
                obj.DocTypeID = t.Field<int?>("DocTypeID");
                obj.DocName = t.Field<string>("DocName");
                obj.ExtensionType = t.Field<string>("ExtensionType");
                obj.maxQuantity = t.Field<int?>("maxQuantity");
                obj.isMandatory = t.Field<int?>("isMandatory");
                obj.MaxSize = t.Field<decimal?>("MaxSize");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Ward> Convert_Ward(DataTable dt)
        {
            List<MovieMD.Ward> lst = new List<MovieMD.Ward>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Ward();
                obj.WardID = t.Field<int?>("WardID");
                obj.WardName = t.Field<string>("WardName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Location> Convert_Location(DataTable dt)
        {
            List<MovieMD.Location> lst = new List<MovieMD.Location>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Location();
                obj.LocationID = t.Field<int?>("LocationID");
                obj.LocationName = t.Field<string>("LocationName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Action> Convert_Desig(DataTable dt)
        {
            List<MovieMD.Action> lst = new List<MovieMD.Action>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Action();
                obj.DesignationID = t.Field<int?>("DesignationID");
                obj.DesignationName = t.Field<string>("DesignationName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Action> Convert_Action(DataTable dt)
        {
            List<MovieMD.Action> lst = new List<MovieMD.Action>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Action();
                obj.ActionType = t.Field<string>("ActionType");
                obj.ActionName = t.Field<string>("ActionName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Inspector> Convert_Inspector(DataTable dt)
        {
            List<MovieMD.Inspector> lst = new List<MovieMD.Inspector>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Inspector();
                obj.InspectorID = t.Field<int?>("InspectorID");
                obj.InspectorName = t.Field<string>("InspectorName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<MovieMD.Holding> Convert_Holding(DataTable dt)
        {
            List<MovieMD.Holding> lst = new List<MovieMD.Holding>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MovieMD.Holding();
                obj.AssesseeID = t.Field<long?>("AssesseeID");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}