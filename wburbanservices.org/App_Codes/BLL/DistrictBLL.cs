﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class DistrictBLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public DistrictBLL()
        {
            cn = new SqlConnection(constring);
        }
        public List<District> GetDistrictRange(params int []ids)
        {
            if(ids==null || ids.Length==0)
            {
                return new List<District>();
            }
            try
            {
                string commaSeparatedNums = ids.Select(b => b.ToString()).Aggregate<string>((items, item) => items + "," + item);

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "Get_AllDistrictByIdRange";
                cmd.Parameters.AddWithValue("@IdRange", commaSeparatedNums);
                cmd.Parameters.AddWithValue("@Delimiter", ",");
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    sda.Fill(dt);
                    return Convert(dt);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }catch(SqlException)
            {
                throw new Exception("Database Error");
            }
        }
        public List<District> GetAllDistricts()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_AllDistrict";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Convert(dt);
            }
            catch(SqlException )
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        #region
        List<District> Convert(DataTable dt)
        {
            List<District> dis_lst = new List<District>();
            dis_lst = dt.AsEnumerable().Select(t =>
            {
                var objDis = new District();
                objDis.DistrictID = t.Field<int?>("DistrictID");
                objDis.DistrictName = t.Field<string>("DistrictName");
                objDis.DistrictCode = t.Field<int?>("DistrictCode");
                objDis.InsertedBy = t.Field<int?>("InsertedBy");
                objDis.InsertedOn = t.Field<DateTime?>("InsertedOn");
                objDis.UpdatedBy = t.Field<int?>("UpdatedBy");
                objDis.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                return objDis;
            }).ToList();

            return dis_lst;
        }
        #endregion
    }
}