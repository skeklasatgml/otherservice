﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using static Valuation_Board.Models.Application.ScheduleThree;
using static Valuation_Board.ViewModels.Assessment_VM;

namespace Valuation_Board.App_Codes.BLL
{
    public class ScheduleThree_Bll:AssessmentBase
    {
        
        public ScheduleThree_Bll()
        {
        }
        public List<AssmtMaster> GetSubmittedForms(long userId)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("GetMstAssmt_byfilter", cn, CommandType.StoredProcedure);
                //cmd.Parameters.AddWithValue("@municipalityId", municipalityId.ReplaceDbNull()); 
                cmd.Parameters.AddWithValue("@valtag", 3);
                if((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.CitizenUser)
                {
                    var user = (SessionContext.CurrentUser as CitizenUser);
                    cmd.Parameters.AddWithValue("@municipalityId", user.MunicipalityID);
                    cmd.Parameters.AddWithValue("@wardId", user.WardID.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@locationId", user.LocationID.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@holdingNo", user.HoldingNo.ReplaceDbNull());
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@submittedBy", userId.ReplaceDbNull());
                }
                
                var dt = _db.GetResult(cmd);

                var lst= dt.AsEnumerable().Select(t =>
                {
                    AssmtMaster m = new AssmtMaster();
                    m.AssmtId = t.Field<long>("TabId");
                    m.AssesseName = t.Field<string>("AssesseeName");
                    m.HoldingNo = t.Field<string>("HoldingNo");
                    m.MunicipalityId = t.Field<int?>("MunicipalityID");
                    m.SubmittedOn = t.Field<DateTime?>("InsertedOn");
                    m.WardID = t.Field<int?>("WardId");
                    return m;
                }).ToList();

                WardBLL b = new WardBLL(); MunicipalityBLL mbl = new MunicipalityBLL();
                lst.GroupBy(t => t.MunicipalityId).Select(t => t.Key).ToList().ForEach(f => {
                var wards = b.GetWards(lst.Where(t => t.WardID != null && t.MunicipalityId == f).Select(q => q.WardID ?? 0).ToList(), f ?? 0);
                lst.Where(t => t.WardID != null && t.MunicipalityId == f).ToList().ForEach(w=>{
                    var ward = wards.FirstOrDefault(s => s.WardID == w.WardID);
                    if (ward != null)
                    {
                        w.Ward = ward.WardName;
                    }
                });
                 
                });
                var municipalities = mbl.GetMunicipalitiesByIds(lst.GroupBy(t => t.MunicipalityId).Select(t => t.Key??0).ToArray());
                lst.ForEach(t => {
                    var m = municipalities.FirstOrDefault(s => s.MunicipalityID == t.MunicipalityId);
                    if (m != null)
                    {
                        t.Municipalityname = m.MunicipalityName;
                    }
                });
                return lst.OrderByDescending(t=>t.SubmittedOn).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public ScheduleThree.Form GetForm(long userId, int municipalityID)
        {
            try
            {
                Form f = new Form();
                f.Address = string.Empty;
                f.Name = string.Empty;
                f.LocationTab = new LocationTab();
                f.UsageTab = new UsageTab();
                f.OtherTab = new OtherTab();
                f.ImageTab = new ImagesTab();
                f.MasterData = this.GetMasterDatas(userId);
                return f;
            }
            catch
            {
                throw;
            }

        }
        public ScheduleThree.MasterData GetMasterDatas(long userId)
        {
            try
            {
                var permission = GetSChdl3ValuationPermission(userId);
                if (permission == null)
                {
                    throw new NullReferenceException("Permission Access Denied");
                }
                AssessmentBll bll = new AssessmentBll();
                MunicipalityBLL mBll = new MunicipalityBLL();

                ScheduleThree.MasterData obj = new ScheduleThree.MasterData();
                obj.Floors = GetFloors();
                obj.FloorTypes = GetFloorTypes();//
                obj.HoldingTypeGroups = bll.GetHoldingTypeGroupes('V').Select(t => new KeyVal<int, string> { Key = t.HoldingTypeGrpId, Value = t.HoldingTypeGrpDesc }).ToList();
                obj.Images = GetOtherInfoTypes('I',null).Where(t=>t.TypeCode!="SIG").ToList();
                obj.Location_otherInfos = GetOtherInfoTypes('L',null);
                var lstAssmtYear = GetOtherInfoTypes('O', "YOA");
                obj.AsseesmentYear = lstAssmtYear.Count > 0 ? lstAssmtYear[0] : null;
                obj.RoofTypes = GetRoofTypes();//
                obj.WallTypes = GetWallTypes();//
                obj.RoomTypes = GetRoomTypes();
                obj.AssmtYears = GetAssmtYears(permission.MunicipalityId);
                obj.Municipalities = mBll.GetAllMunicipalities();
                obj.DeedTypes = bll.GetDeedTypes();
                obj.LandTypesForVacantLand = GetLandTypesForVacantLand();

                var landTypeConfigs = GetOtherInfoTypes('V', "LTV");
                if (landTypeConfigs.Count > 0)
                {
                    obj.VacantLandLandTypeConfig = landTypeConfigs[0];
                }

                return obj;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        public long InsertScheduleIIIData(ScheduleThree.Form form,long userID)
        {
            SqlTransaction tr=null;
            try
            {

                var authType = "O";//O=>otp, S=>signature
                if (form.UserVerificationData.VerificationType == VerificationType.Otp)
                {
                    authType = "O";
                }else if(form.UserVerificationData.VerificationType == VerificationType.SignatureUpload)
                {
                    authType = "S";
                }
                form.ApprovalFlag = authType;

                cn.Open();
                tr = cn.BeginTransaction();
                int municipalityID = form.MunicipalityId;
                
                //master data
                var cmd = _db.NewCommand("InsertMstAssmt_AllParam", cn, tr, CommandType.StoredProcedure);
                form.BindAssmtMaster(cmd,userID,municipalityID,true);
                var assmtId = _db.GetScalar<object>(cmd);

                //location.other info save
                if(form.LocationTab!=null)
                form.LocationTab.OtherInfos.ForEach(t => {
                    cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                    t.Bind(cmd, assmtId, municipalityID, userID);
                    cmd.ExecuteNonQuery();
                });

                //usage bind data to cmd
                if (form.UsageTab != null)
                {
                    if (form.UsageTab.HoldingTypeGroupId != 4)
                    {
                        form.UsageTab.Floors.ForEach(t => {
                            //add floor info
                            cmd = _db.NewCommand("Insert_assmt_Occupancy", cn, tr, CommandType.StoredProcedure);
                            t.Bind(cmd, assmtId,municipalityID, userID);
                            var floorId = _db.GetScalar<object>(cmd);

                            //add room info
                            t.Rooms.ForEach(q => {
                                cmd = _db.NewCommand("Insert_Assmt_Room", cn, tr, CommandType.StoredProcedure);
                                q.Bind(cmd, floorId,municipalityID, userID);
                                cmd.ExecuteNonQuery();
                            });
                        });
                    }
                    else if(form.UsageTab.HoldingTypeGroupId==4)
                    {
                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                        form.UsageTab.VacantLand.LandType.Bind(cmd, assmtId, municipalityID, userID);
                        cmd.ExecuteNonQuery();
                    }
                }

                if (form.OtherTab != null)
                {
                    //location.other info save
                    if (form.OtherTab.AssessmentYear != null)
                    {
                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                        form.OtherTab.AssessmentYear.Bind(cmd, assmtId, municipalityID, userID);
                        cmd.ExecuteNonQuery();
                    }
                }

                if (form.ImageTab != null)
                {
                    cmd = _db.NewCommand("Insert_Assmt_GeoLocation", cn, tr, CommandType.StoredProcedure);
                    form.ImageTab.GeoLocation.Bind(cmd, assmtId,municipalityID, userID);
                    cmd.ExecuteNonQuery();

                    form.ImageTab.Images.ForEach(t => {
                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                        t.Bind(cmd,assmtId,municipalityID, userID);
                        cmd.ExecuteNonQuery();
                    });
                }
                DeleteTempData(municipalityID, userID,cn,tr);

                

                if (form.UserVerificationData.VerificationType == VerificationType.Otp)
                {
                    //user verification
                    OtpServer otp = null;
                    var otpIdentifire = "";
                    if ((SessionContext.CurrentUser as IUserType).UserType == UserType.CitizenUser)
                    {
                        otpIdentifire = (SessionContext.CurrentUser as CitizenUser).AssesseeNo;
                        otp = new OtpServer(otpIdentifire);
                    }
                    else
                    {
                        otpIdentifire = (SessionContext.CurrentUser as IInternalUser).UserID.ToString();

                    }
                    otp = new OtpServer(otpIdentifire);
                    otp.Verify(form.UserVerificationData.VerificationData["OTP"] as string,form.Phone);
                }
                else if (form.UserVerificationData.VerificationType == VerificationType.SignatureUpload)
                {
                    if (form.UserVerificationData.VerificationData["SIG"] == null)
                    {
                        throw new InvalidOperationException("Signature photo Required");
                    }

                    string filePath = ((HttpPostedFileBase)form.UserVerificationData.VerificationData["SIG"]).SaveFile();
                    var cmdSig = _db.NewCommand("Insert_Assmt_Dat_SaveSignature", cn, tr, CommandType.StoredProcedure);
                    cmdSig.Parameters.AddWithValue("@_tabIdAssmtId", assmtId);
                    cmdSig.Parameters.AddWithValue("@_signatureSnapDir", filePath);
                    cmdSig.Parameters.AddWithValue("@_MunicipalityID", municipalityID);
                    cmdSig.Parameters.AddWithValue("@_userId", userID);
                    cmdSig.ExecuteNonQuery();
                }
                else
                {
                    throw new InvalidOperationException("Please select valid authentication type");
                }

                tr.Commit();
                return Convert.ToInt64( assmtId);
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public long UpdateScheduleIIIData(ScheduleThree.Form form, long userID)
        {
            SqlTransaction tr = null;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                int municipalityID = form.MunicipalityId;

                //image data will be not be deleted
                var cmd= _db.NewCommand("delete_Assmt_AllRelativDataOfAssmt_OnUpdateSchdleIII", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@assmtId", form.AssmtId);
                cmd.Parameters.AddWithValue("@valFor", 3);
                cmd.ExecuteNonQuery();

                //delete image requested images
                form.DelImgRequests.ForEach(t => {
                     cmd = _db.NewCommand("delete_Assmt_SchedlIII_Image", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@assmtId", form.AssmtId);
                    cmd.Parameters.AddWithValue("@ImageCode", t.TypeCode);
                    cmd.ExecuteNonQuery();
                });
                //master data
                 cmd = _db.NewCommand("updateMstAssmt_AllParam", cn, tr, CommandType.StoredProcedure);
                form.BindAssmtMaster(cmd, userID, municipalityID,false);
                cmd.ExecuteNonQuery();

                //location.other info save
                if (form.LocationTab != null)
                    form.LocationTab.OtherInfos.ForEach(t => {
                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                        t.Bind(cmd, form.AssmtId, municipalityID, userID);
                        cmd.ExecuteNonQuery();
                    });

                //usage bind data to cmd
                if (form.UsageTab != null)
                {
                    if (form.UsageTab.HoldingTypeGroupId != 4)
                    {
                        form.UsageTab.Floors.ForEach(t => {
                            //add floor info
                            cmd = _db.NewCommand("Insert_assmt_Occupancy", cn, tr, CommandType.StoredProcedure);
                            t.Bind(cmd, form.AssmtId, municipalityID, userID);
                            var floorId = _db.GetScalar<object>(cmd);

                            //add room info
                            t.Rooms.ForEach(q => {
                                cmd = _db.NewCommand("Insert_Assmt_Room", cn, tr, CommandType.StoredProcedure);
                                q.Bind(cmd, floorId, municipalityID, userID);
                                cmd.ExecuteNonQuery();
                            });
                        });
                    }
                }

                if (form.OtherTab != null)
                {
                    //location.other info save
                    if (form.OtherTab != null)

                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                    form.OtherTab.AssessmentYear.Bind(cmd, form.AssmtId, municipalityID, userID);
                    cmd.ExecuteNonQuery();
                }
                if (form.ImageTab != null)
                {
                    cmd = _db.NewCommand("Insert_Assmt_GeoLocation", cn, tr, CommandType.StoredProcedure);
                    form.ImageTab.GeoLocation.Bind(cmd, form.AssmtId, municipalityID, userID);
                    cmd.ExecuteNonQuery();

                    form.ImageTab.Images.ForEach(t => {
                        cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr, CommandType.StoredProcedure);
                        t.Bind(cmd, form.AssmtId, municipalityID, userID);
                        cmd.ExecuteNonQuery();
                    });
                }
                DeleteTempData(municipalityID, userID,cn,tr);
               
                    //delete images physically
                    form.DelImgRequests.ForEach(t =>
                    {
                        CommonUtils.DeleteFile("", t.Value);
                    });
               
                tr.Commit();
                return Convert.ToInt64(form.AssmtId);
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public ScheduleThree.Form FindScheduledIIIData(long tabId,long userID)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("GetMstAssmt_byfilter", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@TabId", tabId);
                var dt = _db.GetResult(cmd);
                cn.Close();
                if (dt.Rows.Count > 0)
                {
                    var ligacyObj = dt.AsEnumerable().Select(t =>
                    {
                        AssmtTransactionModel o = new AssmtTransactionModel();
                        o.TabId = t.Field<long>("TabId");
                        o.YearId = t.Field<int>("YearId");
                        o.Idno = t.Field<long>("Idno");
                        o.Idnosrl = t.Field<int>("Idnosrl");
                        o.WardId = t.Field<int>("WardId");
                        o.WardName = t.Field<string>("WardName");
                        o.LocationID = t.Field<int>("LocationID");
                        o.LocationName = t.Field<string>("LocationName");
                        o.HoldingNo = t.Field<string>("HoldingNo");
                        o.AssesseeNo = t.Field<string>("AssesseeNo");
                        o.AssesseeName = t.Field<string>("AssesseeName");
                        o.AssesseeAddress = t.Field<string>("AssesseeAddress");
                        o.ApplNo = t.Field<int?>("ApplNo");
                        o.ApplDt = t.Field<DateTime?>("ApplDt");
                        o.ProcessingFees = t.Field<decimal?>("ProcessingFees");
                        o.OtherFees = t.Field<decimal?>("OtherFees");
                        o.PrimaryPhNo = t.Field<string>("PrimaryPhNo");
                        o.PrimaryEmail = t.Field<string>("PrimaryEmail");
                        o.BoroughNo = t.Field<string>("BoroughNo");
                        o.NameCAC = t.Field<string>("NameCAC");
                        o.HoldingTypeGrp = t.Field<int?>("HoldingTypeGrp");
                        o.HoldingTypeGrpId = t.Field<int?>("HoldingTypeGrpId");
                        o.HoldArea = t.Field<string>("HoldArea");
                        o.ScoreZoneID = t.Field<string>("ScoreZoneID");
                        o.ScoreUseID = t.Field<string>("ScoreUseID");
                        o.ScoreCostID = t.Field<string>("ScoreCostID");
                        o.TotWt = t.Field<decimal?>("TotWt");
                        o.HoldingAge = t.Field<short?>("HoldingAge");
                        o.LandType = t.Field<string>("LandType");
                        o.VacLandAreaDc = t.Field<int?>("VacLandAreaDc");
                        o.VacLandAreaSft = t.Field<int?>("VacLandAreaSft");
                        o.LandAreaDc = t.Field<int?>("LandAreaDc");
                        o.LandAreaKt = t.Field<int?>("LandAreaKt");
                        o.LandAreaCh = t.Field<int?>("LandAreaCh");
                        o.LandAreaSft = t.Field<int?>("LandAreaSft");
                        o.CoveredArea = t.Field<int?>("CoveredArea");
                        o.PlinthArea = t.Field<int?>("PlinthArea");
                        o.LandCost = t.Field<decimal?>("LandCost");
                        o.AnnualVal = t.Field<decimal?>("AnnualVal");
                        o.DiscPer = t.Field<decimal?>("DiscPer");
                        o.DiscAmt = t.Field<decimal?>("DiscAmt");
                        o.RevAnnualVal = t.Field<decimal?>("RevAnnualVal");
                        o.PtaxPer = t.Field<decimal?>("PtaxPer");
                        o.YrProptax = t.Field<decimal?>("YrProptax");
                        o.QtrProptax = t.Field<decimal?>("QtrProptax");
                        o.PrvQtrProptax = t.Field<decimal?>("PrvQtrProptax");
                        o.SrchgTag = t.Field<string>("SrchgTag");
                        o.QtrSrchg = t.Field<decimal?>("QtrSrchg");
                        o.EduTag = t.Field<string>("EduTag");
                        o.QtrEduCess = t.Field<decimal?>("QtrEduCess");
                        o.MouzaName = t.Field<string>("MouzaName");
                        o.JlNo = t.Field<string>("JlNo");
                        o.PSName = t.Field<string>("PSName");
                        o.KhatianNoRS = t.Field<string>("KhatianNoRS");
                        o.KhatianNoLR = t.Field<string>("KhatianNoLR");
                        o.DagNoRS = t.Field<string>("DagNoRS");
                        o.DagNoLR = t.Field<string>("DagNoLR");
                        o.LandNatureROR = t.Field<string>("LandNatureROR");
                        o.DeedNo = t.Field<string>("DeedNo");
                        o.DeedDt = t.Field<DateTime?>("DeedDt");
                        o.DeedTypeId = t.Field<int?>("DeedTypeId");
                        o.WhetherAssessed = t.Field<string>("WhetherAssessed");
                        o.OldYearId = t.Field<int?>("OldYearId");
                        o.OldAnnualValue = t.Field<decimal?>("OldAnnualValue");
                        o.OldPTax = t.Field<decimal?>("OldPTax");
                        o.WhetherOwnChange = t.Field<string>("WhetherOwnChange");
                        o.OldAssesseeName = t.Field<string>("OldAssesseeName");
                        o.WhetherHoldingChange = t.Field<string>("WhetherHoldingChange");
                        o.OldHoldingType = t.Field<int?>("OldHoldingType");
                        o.AADRHolding = t.Field<string>("AADRHolding");
                        o.ApptTotAreaSft = t.Field<int?>("ApptTotAreaSft");
                        o.OthAreaSft = t.Field<int?>("OthAreaSft");
                        o.BuildingDtl = t.Field<string>("BuildingDtl");
                        o.LiftTag = t.Field<string>("LiftTag");
                        o.DranageTag = t.Field<string>("DranageTag");
                        o.ToiletsTag = t.Field<string>("ToiletsTag");
                        o.ElectricityTag = t.Field<string>("ElectricityTag");
                        o.WaterTag = t.Field<string>("WaterTag");
                        o.FerruleSize = t.Field<decimal?>("FerruleSize");
                        o.ValTag = t.Field<string>("ValTag");
                        o.ValYear = t.Field<string>("ValYear");
                        o.ValQtr = t.Field<int?>("ValQtr");
                        o.ValSrl = t.Field<int?>("ValSrl");
                        o.ValSubSrl = t.Field<byte?>("ValSubSrl");
                        o.ValFromTo = t.Field<string>("ValFromTo");
                        o.FromWardNo = t.Field<int?>("FromWardNo");
                        o.FromStreetCode = t.Field<int?>("FromStreetCode");
                        o.FromHoldingNo = t.Field<string>("FromHoldingNo");
                        o.OldWardNo = t.Field<int?>("OldWardNo");
                        o.OldStreetCode = t.Field<int?>("OldStreetCode");
                        o.OldHoldingNo = t.Field<string>("OldHoldingNo");
                        o.Mfactor = t.Field<decimal?>("Mfactor");
                        o.MacId = t.Field<string>("MacId");
                        o.ApprovedFlag = t.Field<string>("ApprovedFlag");
                        o.ApprovedOn = t.Field<DateTime?>("ApprovedOn");
                        o.ApprovedBy = t.Field<int?>("ApprovedBy");
                        o.ActiveFlag = t.Field<string>("ActiveFlag");
                        o.RefTabId = t.Field<long?>("RefTabId");
                        o.MunicipalityID = t.Field<int?>("MunicipalityID");
                        o.InsertedBy = t.Field<int?>("InsertedBy");
                        o.InsertedOn = t.Field<DateTime?>("InsertedOn");
                        o.UpdatedBy = t.Field<int?>("UpdatedBy");
                        o.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                        return o;
                    }).ToList();
                    var form = ligacyObj[0].Convert();
                    form.MasterData = GetMasterDatas(userID);
                    form.LocationTab.OtherInfos = Dat_GetOtherInfos(form.AssmtId??0, form.MasterData.Location_otherInfos.Select(t => t.TypeCode).ToList());
                    form.UsageTab.Floors = Dat_GetFloorsWithRooms(form.AssmtId ?? 0);
                    form.ImageTab.GeoLocation = Dat_getGeoLoation(form.AssmtId??0);
                    form.ImageTab.Images = Dat_GetOtherInfos(form.AssmtId ?? 0, form.MasterData.Images.Select(t => t.TypeCode).ToList());
                    var lstAssmtYears = Dat_GetOtherInfos(form.AssmtId ?? 0, new List<string>() { "YOA" });
                    form.OtherTab.AssessmentYear = lstAssmtYears.Count > 0 ? lstAssmtYears[0] : null;
                    if (form.UsageTab.HoldingTypeGroupId == 4)
                    {
                        var lst = Dat_GetOtherInfos(form.AssmtId ?? 0, new List<string>() { "LTV" });
                        if (lst.Count > 0) {
                            form.UsageTab.VacantLand.LandType = lst[0];
                        }
                    }
                    WardBLL wbll = new WardBLL();
                    var wards = wbll.GetWards(new List<int> { form.LocationTab.WardID }, form.MunicipalityId);
                    if (wards.Count > 0)
                    {
                        form.LocationTab.Ward = wards[0].WardName;
                    }
                    return form;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(cn.State==ConnectionState.Open)
                cn.Close();
            }
        }
        #region private methods
        
        private List<Floor> Dat_GetFloorsWithRooms(long assmtId)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_Occupancy", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@assmtId", assmtId);
                var dts = _db.GetResults(cmd);
                return dts.Tables[0].AsEnumerable().Select(t =>
                {
                    Floor o = new Floor();
                    o.TabId = t.Field<long>("TabId");
                    o.FloorID = t.Field<int?>("FloorID");
                    o.ConstYear = t.Field<string>("ConstYear");
                    o.UseId = t.Field<int?>("UseId");
                    o.NameOfOccupant = t.Field<string>("NameOfOccupant");
                    o.OccuDetails = t.Field<string>("OccuDetails");
                    o.TotCoverdAreaSft = t.Field<int?>("TotCoverdAreaSft");
                    o.MonRent = t.Field<decimal?>("MonRent") ?? 0;
                    o.MonSc = t.Field<decimal?>("MonSc") ?? 0;
                    o.MunicipalityID = t.Field<int?>("MunicipalityID") ?? 0;
                    o.Rooms = dts.Tables[1].AsEnumerable().Where(q => q.Field<long>("TabIdOccu") == o.TabId).Select(r =>
                    {
                        Room w = new Room();
                        w.TabId = r.Field<long>("TabId");
                        w.TabIdOccu = r.Field<long>("TabIdOccu");
                        w.RoomTypeId = r.Field<int?>("RoomTypeId") ?? 0;
                        w.WSft = r.Field<decimal?>("WSft") ?? 0;
                        w.LSft = r.Field<decimal?>("LSft") ?? 0;
                        w.HSft = r.Field<decimal?>("HSft") ?? 0;
                        w.CoverdAreaSft = r.Field<int?>("CoverdAreaSft") ?? 0;
                        w.RoomDetails = r.Field<string>("RoomDetails");
                        w.RoofType = r.Field<int?>("RoofType") ?? 0;
                        w.WallType = r.Field<int?>("WallType") ?? 0;
                        w.FloorType = r.Field<int?>("FloorType") ?? 0;
                        w.MunicipalityID = r.Field<int?>("MunicipalityID") ?? 0;
                        return w;
                    }).ToList(); ;
                    return o;
                }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        private GeoLocation Dat_getGeoLoation(long assmtId)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("Get_assmt_geoLocation", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@assmtId", assmtId);
                var dt = _db.GetResult(cmd);
                if (dt.Rows.Count == 0)
                    return null;
                else
                return dt.AsEnumerable().Select(t => {
                    GeoLocation o = new GeoLocation();
                    o.Geo_Lat = t.Field<decimal?>("GeoLocationLatitude")??0;
                    o.Geo_Long = t.Field<decimal?>("GeoLocationlongitude")??0;
                    o.TabId = t.Field<long>("TabId");
                    return o;
                }).ToList()[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">L=Location,I=Image</param>
        /// <returns></returns>
        private void DeleteTempData(int municipalityId,long userId,SqlConnection cn,SqlTransaction tr)
        {
            try
            {
                
                var cmd = _db.NewCommand("Delete_Assmt_SchedulIIITemp_data", cn, tr,CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@municipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            
        }
        
        public List<KeyVal<int, string>> GetHoldingTypes(int? hodlingTypeGroupID)
        {
            try
            {
                var cmd = _db.NewCommand("Get_assmt_HoldingTypes_valuation", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HoldingTypeGrpId", hodlingTypeGroupID ?? DBNull.Value as object);
                cn.Open();
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string>
                {
                    Key = t.Field<int>("HoldingTypeId"),
                    Value = t.Field<string>("HoldingTypeDesc")
                }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }

        }
        public List<KeyVal<int, string>> GetRoomTypes(int? usageId=null)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_Room_Types", cn);
                cmd.Parameters.AddWithValue("@UsageId", usageId.ReplaceDbNull());
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string> { Key = t.Field<int>("RoomTypeId"), Value = t.Field<string>("RoomDesc") }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
       private List<KeyVal<int,string>> GetLandTypesForVacantLand(int? typeId=null)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_LandtypesForVacandLand", cn);
                cmd.Parameters.AddWithValue("@TypeId", typeId.ReplaceDbNull());
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string> { Key = t.Field<int>("TypeId"), Value = t.Field<string>("TypeName") }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        private List<KeyVal<int, string>> GetFloors()
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_floors", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string> { Key = t.Field<int>("FloorId"), Value = t.Field<string>("FloorNo") }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        private List<KeyVal<int, string>> GetRoofTypes()
        {
            return GetRoofWallFloor_Types('R');
        }
        private List<KeyVal<int, string>> GetWallTypes()
        {
            return GetRoofWallFloor_Types('W');
        }
        private List<KeyVal<int, string>> GetFloorTypes()
        {
            return GetRoofWallFloor_Types('F');
        }
        private List<KeyVal<int, string>> GetRoofWallFloor_Types(char type)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_RoofWallFloor_Type", cn);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string> { Key = t.Field<int>("RWFTypeId"), Value = t.Field<string>("RWFTypeDesc") }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        private List<KeyVal<int, string>> GetAssmtYears(int? municipalityId)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_assmt_years", cn);
                cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => new KeyVal<int, string> { Key = t.Field<int>("YearId"), Value = t.Field<string>("YearDesc") }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        private ValudationUserPermissionScheduleIII GetSChdl3ValuationPermission(long userID)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("get_ValuationUserPermissionScheduleIII", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userId", userID);
                var dt = _db.GetResult(cmd);
                var lst = dt.AsEnumerable().Select(t =>
                {// MunicipalityID,1 as WardID,1 as WardNo
                    return new ValudationUserPermissionScheduleIII { MunicipalityId = t.Field<int>("MunicipalityID"), WardId = t.Field<int>("WardID"), WardNo = t.Field<string>("WardNo") };
                }).ToList();
                if (lst.Count > 0)
                {
                    return lst[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        public class TempData
        {
            DbHandler _db = null;
            SqlConnection cn;
            public TempData()
            {
                _db = new DbHandler();
                cn = new SqlConnection(_db.ConnectionString);
            }
            
            public bool SaveDataTemp(long userId,int MunicipalityID ,ScheduleThree.Form form)
            {
                try
                {
                    cn.Open();
                    var cmd = _db.NewCommand("Insert_assmt_ScheduledIII_TempData", cn, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@municipalityId", MunicipalityID);
                    cmd.Parameters.AddWithValue("@tempData",JsonConvert.SerializeObject(form));
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            public ScheduleThree.Form  GetTempData(long userID, int municipalityID)
            {
                try
                {
                    cn.Open();
                    var cmd = _db.NewCommand("Get_Assmt_SchedulIIITemp_data", cn, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@userId", userID);
                    cmd.Parameters.AddWithValue("@municipalityId", municipalityID);
                    DataTable dt = _db.GetResult(cmd);
                    if (dt.Rows.Count > 0)
                    {
                        var x = dt.Rows[0];
                        var data = x.Field<string>("tempData");
                        return JsonConvert.DeserializeObject<ScheduleThree.Form>(data);
                    }
                    return null;
                    
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            public bool DeleteTempData(long userID, int municipalityID)
            {
                try
                {
                    cn.Open();
                    var cmd = _db.NewCommand("Delete_Assmt_SchedulIIITemp_data", cn, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@userId", userID);
                    cmd.Parameters.AddWithValue("@municipalityId", municipalityID);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        }
    }
    internal static class ExtensionMethod
    {
        
        public static void BindAssmtMaster(this ScheduleThree.Form form, SqlCommand cmd,long userId,int Municipalityid,bool isInsert)
        {
            AssmtTransactionModel obj = new AssmtTransactionModel();
            obj.ValTag = "3";
            obj.Idno = 0;
            obj.Idnosrl = 0;
            obj.MunicipalityID = Municipalityid;
            obj.TabId = form.AssmtId??0;
            if (form.UsageTab != null)
            {
                obj.HoldingTypeGrpId = form.UsageTab.HoldingTypeGroupId;
                if (form.UsageTab.HoldingTypeGroupId == 4)
                {
                    obj.VacLandAreaDc = (int)form.UsageTab.VacantLand.Area_dec;
                    obj.VacLandAreaSft = (int)form.UsageTab.VacantLand.Area_sft;
                }
            }


            obj.AssesseeName = form.Name;
            obj.AssesseeAddress = form.Address;
            obj.ApprovedFlag = form.ApprovalFlag;

            if (form.LocationTab != null)
            {
                var loc = form.LocationTab;
                obj.PrimaryEmail = form.Email;
                obj.PrimaryPhNo = form.Phone;
                obj.ApplDt = form.ApplicationDate;
                obj.BoroughNo = loc.Borough;
                obj.WardId = loc.WardID;
                obj.HoldingNo = loc.Holding;
                obj.LocationName = loc.RoadLane.ToString();
                obj.LocationID = loc.RoadLaneId;
                obj.NameCAC=loc.Complex;
                obj.LandType = loc.LandType;
                obj.LandAreaDc = (int)loc.LandArea_dec;
                obj.LandAreaSft = (int)loc.LandArea_sft;
                obj.MouzaName = loc.MouzaName;
                obj.JlNo = loc.JlNo;
                obj.PSName = loc.PS;
                obj.DagNoRS = loc.DagNo_Rs;
                obj.DagNoLR = loc.DagNo_Lr;
                obj.KhatianNoLR = loc.Khatian_Lr;
                obj.KhatianNoRS = loc.Khatian_Rs;
                obj.LandNatureROR = loc.RorNature;
                obj.DeedNo = loc.DeedNo;
                obj.DeedDt = loc.DeedDate;
                obj.DeedTypeId = loc.DeetType;

                if (form.OtherTab != null)
                {
                    var ot = form.OtherTab;
                    obj.WhetherAssessed = ot.WheatherAssessed ? "Y" : "N";
                    if (ot.WheatherAssessed)
                    {
                        obj.OldAnnualValue = ot.AnnualValuation;
                        obj.OldYearId = ot.YearOfAssessment;
                        obj.OldPTax = ot.QtrlyPropTax;
                    }
                    obj.WhetherOwnChange= ot.IsOwnershpChanged ? "Y" : "N";
                    obj.OldAssesseeName = ot.OwnershpChanged_Desc;
                    obj.WhetherHoldingChange=ot.IsNatureOfUsedChanged ? "Y" : "N";
                    //obj.OldHoldingType = ot.NatureOfUsedChanged_Desc;//[OldHoldingNo]
                    obj.AADRHolding = ot.IsAddAltrModed ? "Y" : "N";///[AADRHolding]
                    //obj.AddAltrModed_Desc = ot.AddAltrModed_Desc;
                    obj.ApptTotAreaSft = (int)ot.TotalAreaApartment_sft;
                    obj.OthAreaSft = (int)ot.OtherArea_sft;
                }

            }
            AssessmentBll.Bindder.BindParams(cmd, obj,userId);
            if (isInsert)
            {
                cmd.Parameters["@InsertedBy"].Value = userId;
                cmd.Parameters["@InsertedOn"].Value = DateTime.Now.ToString("yyyy'-'MM'-'dd HH:mm:ss").ReplaceDbNull();
                cmd.Parameters["@UpdatedBy"].Value = "".ReplaceDbNull();
                cmd.Parameters["@UpdatedOn"].Value = "".ReplaceDbNull();
            }
        }
        public static void Bind(this ScheduleThree.OtherInfo form, SqlCommand cmd,object assmtId,int municipalityID,long useID)
        {
            cmd.Parameters.AddWithValue("@TabIdAssmtId", assmtId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@InfoTypeId", form.InfoTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@InfoTypeIdVal", form.Value.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@userId", useID.ReplaceDbNull());
            
        }
        public static void Bind(this ScheduleThree.Floor floor,SqlCommand cmd, object assmtId, int municipalityID, long useID)
        {
            cmd.Parameters.AddWithValue("@TabIdAssmt", assmtId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@FloorID", floor.FloorID.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@ConstYear", floor.ConstYear.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@UseId", floor.UseId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@NameOfOccupant", floor.NameOfOccupant.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@OccuDetails", floor.OccuDetails.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@TotCoverdAreaSft", floor.TotCoverdAreaSft.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MonRent", floor.MonRent.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MonSc", floor.MonSc.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@userId", useID.ReplaceDbNull());
        }
        public static void Bind(this ScheduleThree.Room room, SqlCommand cmd, object floorId, int municipalityID, long useID)
        {
            cmd.Parameters.AddWithValue("@TabIdOccu", floorId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@RoomTypeId", room.RoomTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@WSft", room.WSft.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@LSft", room.LSft.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@HSft", room.HSft.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@CoverdAreaSft", room.CoverdAreaSft.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@RoomDetails", room.RoomDetails.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@RoofType", room.RoofType.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@WallType", room.WallType.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@FloorType", room.FloorType.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@userId", useID.ReplaceDbNull());
        }
        public static void Bind(this ScheduleThree.GeoLocation geo, SqlCommand cmd, object assmtId, int municipalityID, long useID)
        {
            cmd.Parameters.AddWithValue("@TabIdAssmtId", assmtId);
            cmd.Parameters.AddWithValue("@GeoLocationLatitude", geo.Geo_Lat.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@GeoLocationlongitude", geo.Geo_Long.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@userId", useID);
        }
        public static Form Convert(this AssmtTransactionModel obj)
        {
            Form f = new Form();
            f.AssmtId = obj.TabId;
            f.Name = obj.AssesseeName;
            f.Address = obj.AssesseeAddress;
            f.MunicipalityId = obj.MunicipalityID??0;
            f.Email = obj.PrimaryEmail;
            f.ApplicationDate = obj.ApplDt;
            f.Phone = obj.PrimaryPhNo;
            f.UsageTab = new UsageTab();
            f.UsageTab.HoldingTypeGroupId = obj.HoldingTypeGrpId;
            if (obj.HoldingTypeGrpId == 4 ) {
                f.UsageTab.VacantLand = new VacantLand() { Area_dec = obj.VacLandAreaDc ?? 0, Area_sft = obj.VacLandAreaSft ?? 0 };
            }


            f.LocationTab = new LocationTab();
            f.LocationTab.Borough = obj.BoroughNo;
            f.LocationTab.WardID = obj.WardId;
            f.LocationTab.Ward = obj.WardName;
            f.LocationTab.RoadLane = obj.LocationName;
            f.LocationTab.RoadLaneId = obj.LocationID;
            f.LocationTab.Holding = obj.HoldingNo;
            f.LocationTab.RoadLane = obj.LocationName;
            f.LocationTab.Complex = obj.NameCAC;
            f.LocationTab.LandType = obj.LandType;
            f.LocationTab.LandArea_dec = obj.LandAreaDc??0;
            f.LocationTab.LandArea_sft = obj.LandAreaSft??0;
            f.LocationTab.MouzaName = obj.MouzaName;
            f.LocationTab.JlNo = obj.JlNo;
            f.LocationTab.PS = obj.PSName;
            f.LocationTab.DagNo_Rs = obj.DagNoRS;
            f.LocationTab.DagNo_Lr = obj.DagNoLR;
            f.LocationTab.Khatian_Lr = obj.KhatianNoLR;
            f.LocationTab.Khatian_Rs = obj.KhatianNoRS;
            f.LocationTab.RorNature = obj.LandNatureROR;
            f.LocationTab.DeedNo = obj.DeedNo;
            f.LocationTab.DeetType = obj.DeedTypeId;
            f.LocationTab.DeedDate = obj.DeedDt;

            f.OtherTab = new OtherTab();
            f.OtherTab.WheatherAssessed = string.Equals(obj.WhetherAssessed, "Y", StringComparison.OrdinalIgnoreCase);
            f.OtherTab.AnnualValuation = obj.OldAnnualValue??0;
            f.OtherTab.YearOfAssessment = obj.OldYearId;
            f.OtherTab.QtrlyPropTax = obj.OldPTax??0;
            f.OtherTab.IsOwnershpChanged=string.Equals(obj.WhetherOwnChange, "Y", StringComparison.OrdinalIgnoreCase);
            f.OtherTab.OwnershpChanged_Desc = obj.OldAssesseeName;
            f.OtherTab.IsNatureOfUsedChanged=string.Equals(obj.WhetherHoldingChange, "Y", StringComparison.OrdinalIgnoreCase);
            f.OtherTab.IsAddAltrModed= string.Equals(obj.AADRHolding, "Y", StringComparison.OrdinalIgnoreCase);
            f.OtherTab.TotalAreaApartment_sft = obj.ApptTotAreaSft??0;
            f.OtherTab.OtherArea_sft = obj.OthAreaSft??0;

            f.ImageTab = new ImagesTab();

            return f;

        }
    }
}