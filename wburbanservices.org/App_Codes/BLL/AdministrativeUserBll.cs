﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class AdministrativeUserBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AdministrativeUserBll()
        {
            cn = new SqlConnection(conString);
        }
        public void GetUser(int id)
        {
            throw new NotImplementedException();
        }
        
        public string GetUrl_LandingPage(AdministrativeUser user)
        {
            return new BllBase().GetLandingUrlByUserId(user.UserID);
        }
        public string GetUrl_LandingPage(AdministrativeUser user,int moduleId)
        {
            return new BllBase().GetLandingUrlByUserId(user.UserID,moduleId);
        }

        public AdministrativeUser GetUser(string userName, string password,AdminType adminType)
        {
            cn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "Get_AdministrativeUser";
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.Parameters.AddWithValue("@AdminType", adminType);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs = Convert(dt);
                if (objs.Count > 0)
                {
                    return objs[0];
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        #region
        public List<AdministrativeUser> Convert(DataTable dt)
        {
            List<AdministrativeUser> lst = new List<AdministrativeUser>();
            foreach (DataRow dr in dt.Rows)
            {
                AdministrativeUser vu = new AdministrativeUser()
                {
                    UserID = dr.Field<long>("UserID"),
                    RoleId = dr.Field<int>("RoleId"),
                    Email = dr.Field<string>("Email"),
                    FirstName = dr.Field<string>("FirstName"),
                    LastName = dr.Field<string>("LastName"),
                    PhoneNo = dr.Field<string>("PhoneNo"),
                    IsAccountVerified = dr.Field<byte>("IsAccountVerified") == 1 ? true : false,
                    IsAccountLocked = dr.Field<byte>("IsAccountLocked") == 1 ? true : false,
                    UserName = dt.Rows[0].Field<string>("UserName"),
                    UserType = dt.Rows[0].Field<UserType>("UserType"),
                    AdminType=dt.Rows[0].Field<AdminType>("AdminType")
                };
                lst.Add(vu);
            }
            return lst;

        }
        #endregion
    }
}