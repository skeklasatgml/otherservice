﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using System.Globalization;
namespace Valuation_Board.App_Codes.BLL
{
    public class OtherAdjustmentBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public OtherAdjustmentBLL()
        {
            cn = new SqlConnection(conString);
        }
        /// <summary>
        ///  Return addjustments if available that is applicable on all assessee on pertucular municipality
        /// </summary>
        /// <returns>List<OtherAdjustmentMaster></returns>
        public List<OtherAdjustmentMaster> GetLevelAllAdjustments(int MunicipalityID,DateTime CurDate)
        {
            SqlCommand cmd = new SqlCommand("Get_Details_OtherAdjustMentLvlOverAll", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@CurDate", CurDate);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                    return ConvertToMasterLvl(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        ///  Return addjustments if available that is applicable on perticular assessee on pertucular municipality
        /// </summary>
        /// <returns>List<OtherAdjustmentMaster></returns>
        public List<OtherAdjustmentLevelAssessee> GetAssesseeLevelAdjustments(long AssesseeID,  DateTime CurDate,int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_Detail_OtherAdjustMentLvlAssessee", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@CurDate", CurDate);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertToAssesseeLvl(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        List<OtherAdjustmentLevelAssessee> ConvertToAssesseeLvl(DataTable dt)
        {
            List<OtherAdjustmentLevelAssessee> lst = new List<OtherAdjustmentLevelAssessee>();
            foreach (DataRow dr in dt.Rows)
            {
                OtherAdjustmentLevelAssessee a = new OtherAdjustmentLevelAssessee();
                a.MstAdjID = dr.Field<long>("MstAdjID");
                a.MunicipalityID = dr.Field<int>("MunicipalityID");
                a.MstPayHeadID = dr.Field<int>("MstPayHeadID");
                a.PayHeadBehave = dr.Field<string>("PayHeadBehave");
                a.PayHeadDesc = dr.Field<string>("PayHeadDesc");
                a.PayHeadType = dr.Field<string>("PayHeadType");
                a.EffectType = dr.Field<string>("EffectType");
                a.EffectiveFromDt = dr.Field<DateTime>("EffectiveFromDt");
                a.EffectiveToDt = dr.Field<DateTime>("EffectiveToDt");
                a.RateValueType = dr.Field<string>("RateValueType");
                a.RateValue = dr.Field<decimal>("RateValue");
                a.IsApproved = dr.Field<bool>("IsApproved");
                a.ApprovedOn = dr.Field<DateTime?>("ApprovedOn");
                a.MstDocumentsID = dr.Field<long?>("MstDocumentsID");
                a.ApplicableTaxRangeType = dr.Field<string>("ApplicableTaxRangeType");
                a.IsPropertyTax = dr.Field<bool>("IsPropertyTax");
                a.IsSurcharge = dr.Field<bool>("IsSurcharge");
                a.IsPropertyTaxInt = dr.Field<bool>("IsPropertyTaxInt");
                a.IsSurchargeInt = dr.Field<bool>("IsSurchargeInt");
                a.IsApproved = dr.Field<bool>("IsApproved");
                a.ApprovedOn = dr.Field<DateTime?>("ApprovedOn");
                a.FromFinYear = dr.Field<string>("FromFinYear");
                a.FromFinYearQtr = dr.Field<int?>("FromFinYearQtr");
                a.ToFinYear = dr.Field<string>("ToFinYear");
                a.ToFinYearQtr = dr.Field<int?>("ToFinYearQtr");
                a.Remarks = dr.Field<string>("Remarks");

                a.RowID= dr.Field<long>("RowID");
                a.PayAdjustedBy = dr.Field<long?>("PayAdjustedBy");
                a.IsPayAdjusted = dr.Field<bool>("IsPayAdjusted");
                a.PayAdjustedOn = dr.Field<DateTime?>("PayAdjustedOn");
                a.AssesseID = dr.Field<long>("AssesseeID");
                lst.Add(a);
            }
            return lst;
        }
        List<OtherAdjustmentMaster> ConvertToMasterLvl(DataTable dt)
        {
            List<OtherAdjustmentMaster> lst = new List<OtherAdjustmentMaster>();
            foreach (DataRow dr in dt.Rows)
            {
                OtherAdjustmentMaster a = new OtherAdjustmentMaster();
                a.MstAdjID = dr.Field<long>("MstAdjID");
                a.MunicipalityID = dr.Field<int>("MunicipalityID");
                a.MstPayHeadID = dr.Field<int>("MstPayHeadID");
                a.PayHeadBehave = dr.Field<string>("PayHeadBehave");
                a.PayHeadDesc = dr.Field<string>("PayHeadDesc");
                a.PayHeadType = dr.Field<string>("PayHeadType");
                a.EffectType = dr.Field<string>("EffectType");
                a.EffectiveFromDt = dr.Field<DateTime>("EffectiveFromDt");
                a.EffectiveToDt = dr.Field<DateTime>("EffectiveToDt");
                a.RateValueType = dr.Field<string>("RateValueType");
                a.RateValue = dr.Field<decimal>("RateValue");
                a.IsApproved = dr.Field<bool>("IsApproved");
                a.ApprovedOn = dr.Field<DateTime?>("ApprovedOn");
                a.MstDocumentsID = dr.Field<long?>("MstDocumentsID");
                a.ApplicableTaxRangeType= dr.Field<string>("ApplicableTaxRangeType");
                a.IsPropertyTax= dr.Field<bool>("IsPropertyTax");
                a.IsSurcharge= dr.Field<bool>("IsSurcharge");
                a.IsPropertyTaxInt= dr.Field<bool>("IsPropertyTaxInt");
                a.IsSurchargeInt= dr.Field<bool>("IsSurchargeInt");
                a.IsApproved= dr.Field<bool>("IsApproved");
                a.ApprovedOn= dr.Field<DateTime?>("ApprovedOn");
                a.FromFinYear= dr.Field<string>("FromFinYear");
                a.FromFinYearQtr= dr.Field<int?>("FromFinYearQtr");
                a.ToFinYear= dr.Field<string>("ToFinYear");
                a.ToFinYearQtr= dr.Field<int?>("ToFinYearQtr");
                a.Remarks = dr.Field<string>("Remarks");
                lst.Add(a);
            }
            return lst;
        }

        public List<OtherAdjustmentAll_VM> Get_AllOtherAdjustment(int municipalityID, string FromDate, string ToDate)
        {
            DateTime dtFrom = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //System.Diagnostics.Debug.WriteLine(dtFrom.ToString("yyyy'-'MM'-'dd"));
            //DateTime dtFrom = Convert.ToDateTime(FromDate);
            //DateTime dtTo = Convert.ToDateTime(ToDate);

            SqlCommand cmd = new SqlCommand("Get_OtherAdjustmentForAll", cn);
            cmd.Parameters.AddWithValue("@FromDate", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@ToDate", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertAllAdjEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }

        public List<OtherAdjustmentIndividual_VM> Get_IndOtherAdjustment(int municipalityID, string EffectFromDT, string EffectToDT, string AssesseeID)
        {
            DateTime dtFrom = DateTime.ParseExact(EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //System.Diagnostics.Debug.WriteLine(dtFrom.ToString("yyyy'-'MM'-'dd"));
            //DateTime dtFrom = Convert.ToDateTime(FromDate);
            //DateTime dtTo = Convert.ToDateTime(ToDate);

            SqlCommand cmd = new SqlCommand("Get_OtherAdjustmentForIndividual", cn);
            cmd.Parameters.AddWithValue("@FromDate", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@ToDate", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@AssesseeId", AssesseeID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertIndAdjEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }


        public void Update_AllOtherAdjustment(OtherAdjustmentAll_VM objOtherAdj, long UserID)
        {
            DateTime dtFrom = DateTime.ParseExact(objOtherAdj.EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(objOtherAdj.EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            SqlCommand cmd = new SqlCommand("Update_OtherAdjustmentAll", cn);
            cmd.Parameters.AddWithValue("@MstOthrAdjID", objOtherAdj.MstOthrAdjID);
            cmd.Parameters.AddWithValue("@MunicipalityID", objOtherAdj.MunicipalityID);
            cmd.Parameters.AddWithValue("@ApplicableTaxRangeType", (objOtherAdj.ApplicableTaxRangeType));
            cmd.Parameters.AddWithValue("@FromFinYear", (objOtherAdj.FromFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.FromFinYear));
            cmd.Parameters.AddWithValue("@FromFinYearQtr", (objOtherAdj.FromFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.FromFinYearQtr)));
            cmd.Parameters.AddWithValue("@ToFinYear", (objOtherAdj.ToFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.ToFinYear));
            cmd.Parameters.AddWithValue("@ToFinYearQtr", (objOtherAdj.ToFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.ToFinYearQtr)));

            cmd.Parameters.AddWithValue("@RateValueType", objOtherAdj.RateValueType);
            cmd.Parameters.AddWithValue("@RateValue", objOtherAdj.RateValueWithoutPercent);

            cmd.Parameters.AddWithValue("@isPropertyTax", (objOtherAdj.IsPropertyTax));
            cmd.Parameters.AddWithValue("@isSurcharge", (objOtherAdj.IsSurcharge));
            cmd.Parameters.AddWithValue("@isPropertyTaxInt", (objOtherAdj.IsPropertyTaxInt));
            cmd.Parameters.AddWithValue("@isSurchargeInt", (objOtherAdj.IsSurchargeInt));

            cmd.Parameters.AddWithValue("@gracePeriodInMonth", objOtherAdj.GracePeriodInMonth);
            cmd.Parameters.AddWithValue("@EffectiveFrom", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@EffectiveTo", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@UserID", UserID);

            cmd.Parameters.AddWithValue("@Remarks", objOtherAdj.Remarks);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);

            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("This is already approved"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    throw new Exception("Database Error", ex);
                }

            }
            finally
            {
                cn.Close();
            }

        }

        public void Update_IndividualOtherAdjustment(OtherAdjustmentIndividual_VM objOtherAdj, long UserID)
        {
            DateTime dtFrom = DateTime.ParseExact(objOtherAdj.EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(objOtherAdj.EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            SqlCommand cmd = new SqlCommand("Update_OtherAdjustmentIndividual", cn);
            cmd.Parameters.AddWithValue("@Dtild", objOtherAdj.DtlID);
            cmd.Parameters.AddWithValue("@AssesseeID", objOtherAdj.AssesseeID);
            cmd.Parameters.AddWithValue("@MstOthrAdjID", objOtherAdj.MstOthrAdjID);
            cmd.Parameters.AddWithValue("@MunicipalityID", objOtherAdj.MunicipalityID);
            cmd.Parameters.AddWithValue("@ApplicableTaxRangeType", (objOtherAdj.ApplicableTaxRangeType));
            cmd.Parameters.AddWithValue("@FromFinYear", (objOtherAdj.FromFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.FromFinYear));
            cmd.Parameters.AddWithValue("@FromFinYearQtr", (objOtherAdj.FromFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.FromFinYearQtr)));
            cmd.Parameters.AddWithValue("@ToFinYear", (objOtherAdj.ToFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.ToFinYear));
            cmd.Parameters.AddWithValue("@ToFinYearQtr", (objOtherAdj.ToFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.ToFinYearQtr)));

            cmd.Parameters.AddWithValue("@RateValueType", objOtherAdj.RateValueType);
            cmd.Parameters.AddWithValue("@RateValue", objOtherAdj.RateValueWithoutPercent);

            cmd.Parameters.AddWithValue("@isPropertyTax", (objOtherAdj.IsPropertyTax));
            cmd.Parameters.AddWithValue("@isSurcharge", (objOtherAdj.IsSurcharge));
            cmd.Parameters.AddWithValue("@isPropertyTaxInt", (objOtherAdj.IsPropertyTaxInt));
            cmd.Parameters.AddWithValue("@isSurchargeInt", (objOtherAdj.IsSurchargeInt));

            cmd.Parameters.AddWithValue("@gracePeriodInMonth", objOtherAdj.GracePeriodInMonth);
            cmd.Parameters.AddWithValue("@EffectiveFrom", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@EffectiveTo", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@UserID", UserID);

            cmd.Parameters.AddWithValue("@Remarks", objOtherAdj.Remarks);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);

            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("This is already approved"))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    throw new Exception("Database Error", ex);
                }

            }
            finally
            {
                cn.Close();
            }

        }



        public void Insert_AllOtherAdjustment(OtherAdjustmentAll_VM objOtherAdj, int MunicipalityID, long UserID)
        {
            DateTime dtFrom = DateTime.ParseExact(objOtherAdj.EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(objOtherAdj.EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            SqlCommand cmd = new SqlCommand("Insert_OtherAdjustment", cn);
            cmd.Parameters.AddWithValue("@fk_MstPayHeadID", objOtherAdj.PayHeadID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@ApplicableTaxRangeType", (objOtherAdj.ApplicableTaxRangeType));
            cmd.Parameters.AddWithValue("@FromFinYear", (objOtherAdj.FromFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.FromFinYear));
            cmd.Parameters.AddWithValue("@FromFinYearQtr", (objOtherAdj.FromFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.FromFinYearQtr)));
            cmd.Parameters.AddWithValue("@ToFinYear", (objOtherAdj.ToFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.ToFinYear));
            cmd.Parameters.AddWithValue("@ToFinYearQtr", (objOtherAdj.ToFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.ToFinYearQtr)));

            cmd.Parameters.AddWithValue("@RateValueType", objOtherAdj.RateValueType);
            cmd.Parameters.AddWithValue("@RateValue", objOtherAdj.RateValueWithoutPercent);

            cmd.Parameters.AddWithValue("@isPropertyTax", (objOtherAdj.IsPropertyTax));
            cmd.Parameters.AddWithValue("@isSurcharge", (objOtherAdj.IsSurcharge));
            cmd.Parameters.AddWithValue("@isPropertyTaxInt", (objOtherAdj.IsPropertyTaxInt));
            cmd.Parameters.AddWithValue("@isSurchargeInt", (objOtherAdj.IsSurchargeInt));

            cmd.Parameters.AddWithValue("@gracePeriodInMonth", objOtherAdj.GracePeriodInMonth);
            cmd.Parameters.AddWithValue("@EffectiveFromDt", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@EffectiveToDt", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@UserID", UserID);

            cmd.Parameters.AddWithValue("@Remarks", objOtherAdj.Remarks);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);

            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("This PayHead already asigned for given period..."))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    throw new Exception("Database Error1", ex);
                }

            }
            finally
            {
                cn.Close();
            }

        }

        public void Insert_IndOtherAdjustment(OtherAdjustmentIndividual_VM objOtherAdj, int MunicipalityID, long UserID)
        {
            DateTime dtFrom = DateTime.ParseExact(objOtherAdj.EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(objOtherAdj.EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            SqlCommand cmd = new SqlCommand("Insert_OtherAdjustmentDtl", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", objOtherAdj.AssesseeID);
            cmd.Parameters.AddWithValue("@fk_MstPayHeadID", objOtherAdj.PayHeadID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@ApplicableTaxRangeType", (objOtherAdj.ApplicableTaxRangeType));
            cmd.Parameters.AddWithValue("@FromFinYear", (objOtherAdj.FromFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.FromFinYear));
            cmd.Parameters.AddWithValue("@FromFinYearQtr", (objOtherAdj.FromFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.FromFinYearQtr)));
            cmd.Parameters.AddWithValue("@ToFinYear", (objOtherAdj.ToFinYear.Equals(" ") ? DBNull.Value : (object)objOtherAdj.ToFinYear));
            cmd.Parameters.AddWithValue("@ToFinYearQtr", (objOtherAdj.ToFinYearQtr.Equals(" ") ? DBNull.Value : (object)Convert.ToInt16(objOtherAdj.ToFinYearQtr)));

            cmd.Parameters.AddWithValue("@RateValueType", objOtherAdj.RateValueType);
            cmd.Parameters.AddWithValue("@RateValue", objOtherAdj.RateValueWithoutPercent);

            cmd.Parameters.AddWithValue("@isPropertyTax", (objOtherAdj.IsPropertyTax));
            cmd.Parameters.AddWithValue("@isSurcharge", (objOtherAdj.IsSurcharge));
            cmd.Parameters.AddWithValue("@isPropertyTaxInt", (objOtherAdj.IsPropertyTaxInt));
            cmd.Parameters.AddWithValue("@isSurchargeInt", (objOtherAdj.IsSurchargeInt));

            cmd.Parameters.AddWithValue("@gracePeriodInMonth", objOtherAdj.GracePeriodInMonth);
            cmd.Parameters.AddWithValue("@EffectiveFromDt", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@EffectiveToDt", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@UserID", UserID);

            cmd.Parameters.AddWithValue("@Remarks", objOtherAdj.Remarks);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);

            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Error inserting in OAM."))
                {
                    throw new Exception(ex.Message);
                }
                else if (ex.Message.Contains("Error inserting in OAD."))
                {
                    throw new Exception(ex.Message);
                }
                else if (ex.Message.Contains("This PayHead already asigned for given period..."))
                {
                    throw new Exception(ex.Message);
                }
                else
                {
                    throw new Exception("Database Error1", ex);
                }

            }
            finally
            {
                cn.Close();
            }

        }


        List<OtherAdjustmentAll_VM> ConvertAllAdjEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                OtherAdjustmentAll_VM p = new OtherAdjustmentAll_VM();
                p.MstOthrAdjID = t.Field<long?>("MstOthrAdjID");
                p.MunicipalityID = t.Field<int?>("MunicipalityID");
                p.ApplicableTaxRangeType = t.Field<string>("ApplicableTaxRangeType");
                p.FromFinYear = t.Field<string>("FromFinYear");
                p.FromFinYearQtr = t.Field<int?>("FromFinYearQtr").ToString();
                p.ToFinYear = t.Field<string>("ToFinYear");
                p.ToFinYearQtr = t.Field<int?>("ToFinYearQtr").ToString();

                p.IsPropertyTax = t.Field<bool>("isPropertyTax");
                p.IsSurcharge = t.Field<bool>("isSurcharge");
                p.IsPropertyTaxInt = t.Field<bool>("isPropertyTaxInt");
                p.IsSurchargeInt = t.Field<bool>("isSurchargeInt");

                p.PayHeadID = t.Field<int?>("fk_MstPayHeadID");
                p.PayHeadDesc = t.Field<string>("PayHeadDesc");
                p.RateValueType = t.Field<string>("RateValueType");
                p.RateValue = t.Field<string>("RateValue");
                p.RateValueWithoutPercent = t.Field<decimal?>("RateVlaWithoutPer");
                p.GracePeriodInMonth = t.Field<int?>("gracePeriodInMonth");
                p.EffectFromDT = t.Field<string>("EffectiveFromDt");
                p.EffectToDT = t.Field<string>("EffectiveToDt");
                p.IsApprovedFlag = t.Field<bool>("IsApprovedFlag");
                p.IsApproved = t.Field<string>("IsApproved");
                p.ApprovedOn = t.Field<string>("ApprovedOn");

                p.Remarks = t.Field<string>("Remarks");
                return p;
            }).ToList();
        }

        List<OtherAdjustmentIndividual_VM> ConvertIndAdjEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                OtherAdjustmentIndividual_VM p = new OtherAdjustmentIndividual_VM();
                p.DtlID = t.Field<long?>("DtlID");
                p.AssesseeID = t.Field<long?>("AssesseeID");
                p.AssesseeName = t.Field<string>("AssesseeName");
                p.fk_MstOthrAdjID = t.Field<long?>("fk_MstOthrAdjID");
                p.IsPayAdjustedFlag = t.Field<bool?>("IsPayAdjustedFlag");
                p.IsPayAdjusted = t.Field<string>("IsPayAdjusted");
                p.PayAdjustedOn = t.Field<string>("PayAdjustedOn");

                p.MstOthrAdjID = t.Field<long?>("fk_MstOthrAdjID");
                p.MunicipalityID = t.Field<int?>("MunicipalityID");
                p.ApplicableTaxRangeType = t.Field<string>("ApplicableTaxRangeType");
                p.FromFinYear = t.Field<string>("FromFinYear");
                p.FromFinYearQtr = t.Field<int?>("FromFinYearQtr").ToString();
                p.ToFinYear = t.Field<string>("ToFinYear");
                p.ToFinYearQtr = t.Field<int?>("ToFinYearQtr").ToString();

                p.IsPropertyTax = t.Field<bool>("isPropertyTax");
                p.IsSurcharge = t.Field<bool>("isSurcharge");
                p.IsPropertyTaxInt = t.Field<bool>("isPropertyTaxInt");
                p.IsSurchargeInt = t.Field<bool>("isSurchargeInt");

                p.PayHeadID = t.Field<int?>("fk_MstPayHeadID");
                p.PayHeadDesc = t.Field<string>("PayHeadDesc");
                p.RateValueType = t.Field<string>("RateValueType");
                p.RateValue = t.Field<string>("RateValue");
                p.RateValueWithoutPercent = t.Field<decimal?>("RateVlaWithoutPer");
                p.GracePeriodInMonth = t.Field<int?>("gracePeriodInMonth");
                p.EffectFromDT = t.Field<string>("EffectiveFromDt");
                p.EffectToDT = t.Field<string>("EffectiveToDt");
                p.IsApprovedFlag = t.Field<bool>("IsApprovedFlag");
                p.IsApproved = t.Field<string>("IsApproved");
                p.ApprovedOn = t.Field<string>("ApprovedOn");

                p.Remarks = t.Field<string>("Remarks");

                return p;
            }).ToList();
        }

        public List<PayHeadDesc_VM> GetPayHeadDesc(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadDesc", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertPayHead(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<PayHeadDesc_VM> ConvertPayHead(DataTable dt)
        {
            List<PayHeadDesc_VM> lst = new List<PayHeadDesc_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new PayHeadDesc_VM();
                obj.PayHeadID = t.Field<int?>("PayHeadID");
                obj.PayHeadDesc = t.Field<string>("PayHeadDesc");
                return obj;
            }).ToList();
            return lst;
        }
    }
}