﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Valuation_Board.ViewModels;
using System.Globalization;

namespace Valuation_Board.App_Codes.BLL
{
    public class ApprovedAdjustBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ApprovedAdjustBLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<ApproveOtherAdjustment> GetAdjustmentData(int municipalityID, string effectType)
        {
            SqlCommand cmd = new SqlCommand("GET_OtherAdjustment", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@EffectType", effectType);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertApproval(dt);

            }
            catch (SqlException se)
            {
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApproveOtherAdjustment> ConvertApproval(DataTable dt)
        {
            List<ApproveOtherAdjustment> lst = new List<ApproveOtherAdjustment>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApproveOtherAdjustment();
                obj.MstOthrAdjID = t.Field<long>("MstOthrAdjID");
                //obj.FinYear = t.Field<string>("FinYear");
                obj.PayHeadDesc = t.Field<string>("PayHeadDesc");
                obj.EffectiveFromDt = t.Field<string>("EffectiveFromDt");
                obj.EffectiveToDt = t.Field<string>("EffectiveToDt");
                obj.RateValue = t.Field<string>("RateValue");
                //obj.EffectOnCurrentDemand = t.Field<string>("EffectOnCurrentDemand");
                obj.gracePeriodInMonth = t.Field<int>("gracePeriodInMonth");
                obj.AssesseeID = t.Field<string>("AssesseeID");
                obj.AssesseeName = t.Field<string>("AssesseeName");
                obj.WardName = t.Field<string>("WardName");
                obj.LocationName = t.Field<string>("LocationName");
                obj.HoldingNo = t.Field<string>("HoldingNo");

                return obj;
            }).ToList();
            return lst;
        }
        public void UpdateApproved(long UserID, int MunicipalityID, long othAdjID)
        {
            SqlCommand cmd = new SqlCommand("Update_ApprovedOtherAdjustment", cn);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@MstOthrAdjID", othAdjID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error...!" + ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
    }
}