﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class PayHeadSubmitBll
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public PayHeadSubmitBll()
        {
            cn = new SqlConnection(constring);
        }

        public List<PayHeadDetails> Get_PayHeadsDetailsByMunicipltyID(int municipalityID, string FinYear, string ApplicableType, string PayHeadBehave)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadDetailsByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@ApplicableType", ApplicableType);
            cmd.Parameters.AddWithValue("@PayHeadBehave", PayHeadBehave);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadDetailsEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public List<PayHeadDetails> Get_PayHeadsDetailsByMunicipltyID(int municipalityID, string FinYear, string ApplicableType, string PayHeadBehave, long? GroupId)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadDetailsByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@ApplicableType", ApplicableType);
            cmd.Parameters.AddWithValue("@PayHeadBehave", PayHeadBehave);
            cmd.Parameters.AddWithValue("@GroupID", (GroupId??DBNull.Value as object));
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadDetailsEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }

        public List<PayHeadDetails> Save_PayHeadsDetailsByMunicipltyID(int municipalityID, string FinYear, string ApplicableType, string PayHeadBehave, List<PayHeadDetails> tableDataRebate, long? GroupId)
        {
            string PayHeadID = "";
            SqlTransaction transaction=null;
            
            try
            {
                cn.Open();
                transaction = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand("Insert_PayHeadMaster", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@ApplicableType", ApplicableType);
                cmd.Parameters.AddWithValue("@PayHeadBehave", PayHeadBehave);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.CommandType = CommandType.StoredProcedure;
               // cmd.Connection = cn;
                cmd.Transaction = transaction;
                
               
                SqlDataAdapter daMST = new SqlDataAdapter(cmd);
                DataTable dtMST = new DataTable();
                daMST.Fill(dtMST);
                PayHeadID = dtMST.Rows[0]["PayHead_ID"].ToString();
                //reader = cmd.ExecuteReader();
                //while (reader.Read())
                //{
                //    PayHeadID = reader["PayHead_ID"].ToString();
                //}
                //reader.Close();

                tableDataRebate.ForEach(t => {
                    cmd = new SqlCommand("Insert_PayHeadDetails", cn);
                    cmd.Parameters.AddWithValue("@PayHeadID", PayHeadID);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                    cmd.Parameters.AddWithValue("@ApplicableType", ApplicableType);
                    cmd.Parameters.AddWithValue("@PayHeadBehave", PayHeadBehave);
                    cmd.Parameters.AddWithValue("@FinYear", FinYear);
                    cmd.Parameters.AddWithValue("@QtrNo", t.QtrNo);
                    cmd.Parameters.AddWithValue("@GracePeriodInMonth", t.GracePeriodInMonth);
                    cmd.Parameters.AddWithValue("@PayRate", t.PayRate);
                    cmd.Parameters.AddWithValue("@GroupID", GroupId ?? 0);
                    cmd.Parameters.AddWithValue("@PeriodOffset", t.PeriodOffset);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                });

                cmd = new SqlCommand("Get_PayHeadDetailsByMunicipalityID", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@ApplicableType", ApplicableType);
                cmd.Parameters.AddWithValue("@PayHeadBehave", PayHeadBehave);
                cmd.Parameters.AddWithValue("@GroupID", (GroupId ?? 0));
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = transaction;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();


                da.Fill(dt);

                transaction.Commit();
                return ConvertToPayheadDetailsEntity(dt);


            }
            catch 
            {
                transaction.Rollback();
                cn.Close();
                throw;
            }
            finally
            {
                cn.Close();
            }

        }

        List<PayHeadDetails> ConvertToPayheadDetailsEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                PayHeadDetails p = new PayHeadDetails();
                p.EffectiveDateForm = t.Field<string>("EffectiveDateForm");
                p.EffectiveDateTo = t.Field<string>("EffectiveDateTo");
                p.GracePeriodInMonth = t.Field<int?>("GracePeriodInMonth");
                p.PayRate = t.Field<decimal?>("PayRate");
                p.QtrNo = t.Field<byte?>("QtrNo");
                p.FinYear = t.Field<string>("FinYear");
                p.PayHeadID = t.Field<int?>("PayHeadID");
                p.IsDeleted = t.Field<bool>("IsDeleted");
                p.PeriodOffset = t.Field<int>("PeriodOffSet");
                return p;
            }).ToList();
        }
    }
}