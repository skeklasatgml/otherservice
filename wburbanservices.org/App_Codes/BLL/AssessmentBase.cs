﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using  Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public abstract class AssessmentBase
    {
        protected SqlTransaction tr = null;
        protected DbHandler _db;
        protected SqlConnection cn;
       public bool IsExceptionOcurred { get; set; }
        public AssessmentBase()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();

            }
            tr = cn.BeginTransaction();
        }
        protected List<ScheduleThree.OtherInfoType> GetOtherInfoTypes(char type, string typeCode,char valfor='3')
        {
            try
            {
                //cn.Open();
                var cmd = _db.NewCommand("get_assmt_OtherInfoTypes", cn,tr);
                cmd.Parameters.AddWithValue("@typeGroup", type.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@valFor", valfor);
                cmd.Parameters.AddWithValue("@typeCode", typeCode.ReplaceDbNull());
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new ScheduleThree.OtherInfoType()
                    {
                        DataType = t.Field<string>("ValType"),
                        InfoType = t.Field<string>("InfoTypeGroup"),
                        InfoTypeId = t.Field<int>("InfoTypeId"),
                        TypeCode = t.Field<string>("InfoTypeCode"),
                        TypeDesc = t.Field<string>("InfoTypeDesc")
                    };
                }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                //cn.Close();
            }
        }
        protected List<ScheduleThree.OtherInfoType> GetOtherInfoTypes(List<char> types, List<string> typeCodes, char valfor = '3')
        {
            try
            {
                var strTypes = string.Join(",", (types ?? new List<char>()));
                strTypes= string.IsNullOrEmpty(strTypes) ? null : strTypes;

                var strtypeCodes = string.Join(",", (typeCodes ?? new List<string>()));
                strtypeCodes = string.IsNullOrEmpty(strtypeCodes) ? null : strtypeCodes;

                //cn.Open();
                var cmd = _db.NewCommand("get_assmt_OtherInfoTypesDynamic", cn,tr);
                cmd.Parameters.AddWithValue("@typeGroup", strTypes.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@valFor", valfor);
                cmd.Parameters.AddWithValue("@typeCode", strtypeCodes.ReplaceDbNull());
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new ScheduleThree.OtherInfoType()
                    {
                        DataType = t.Field<string>("ValType"),
                        InfoType = t.Field<string>("InfoTypeGroup"),
                        InfoTypeId = t.Field<int>("InfoTypeId"),
                        TypeCode = t.Field<string>("InfoTypeCode"),
                        TypeDesc = t.Field<string>("InfoTypeDesc")
                    };
                }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
               // cn.Close();
            }
        }
        protected List<ScheduleThree.OtherInfo> Dat_GetOtherInfos(long assmtId, List<string> typeCodes)
        {
            try
            {
                //cn.Open();
                var cmd = _db.NewCommand("Get_Assmt_OthInfo", cn,tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@assmtId", assmtId);
                cmd.Parameters.AddWithValue("@typeCodes", string.Join(",", typeCodes));
                var dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    ScheduleThree.OtherInfo o = new ScheduleThree.OtherInfo();
                    o.DatId = t.Field<long>("TabId");
                    o.InfoTypeId = t.Field<int?>("InfoTypeId");
                    o.Value = t.Field<string>("InfoTypeIdVal");
                    o.TypeCode = t.Field<string>("InfoTypeCode");
                    o.InfoType = t.Field<string>("InfoTypeGroup");
                    o.DataType = t.Field<string>("ValType");

                    return o;
                }).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                //cn.Close();
            }
        }
        public void Dispose()
        {

            if (tr != null)
            {
                if (IsExceptionOcurred)
                {
                    tr.Rollback();
                }
                else
                {
                    tr.Commit();
                }
            }
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }
        }
    }

}