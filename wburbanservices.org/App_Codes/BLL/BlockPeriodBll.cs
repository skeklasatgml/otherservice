﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.ViewModels.assmt;

namespace Valuation_Board.App_Codes.BLL
{
    public class BlockPeriodBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;

        public BlockPeriodBll()
        {
            cn = new SqlConnection(conString);
        }

        public object EffctFromTo(blockPeriod blockPeriod) {
            int month;
            if (blockPeriod.Qtr == 1)
            {
                month = 1;
            } else if (blockPeriod.Qtr == 2) {
                month = 4;
            } else if (blockPeriod.Qtr == 3) {
                month = 7;
            }
            else {
                month = 10;
            }
            
            DateTime from = new DateTime(blockPeriod.FinYear, month, 1);
            DateTime to;
            string[] yearmonth;
            if (blockPeriod.Period.IndexOf(".") != -1)
            {
                yearmonth = blockPeriod.Period.Split('.');
                to = from.AddMonths((Convert.ToInt32(yearmonth[0]) * 12) + Convert.ToInt32(yearmonth[1])).AddDays(-1);
            }
            else {
                to = from.AddMonths(Convert.ToInt32(blockPeriod.Period) *12).AddDays(-1);
            }
             return  new { From = from, To = to };
        }
        

        public void InsertBlock(blockPeriod blockPeriod) {
            dynamic fromto = EffctFromTo(blockPeriod);
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Insert_BlockPeriod";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@MunicipalityID", blockPeriod.MunicipalityID);
                cmd.Parameters.AddWithValue("@YearId", blockPeriod.YearId);
                cmd.Parameters.AddWithValue("@FinYear", blockPeriod.FinYear);
                cmd.Parameters.AddWithValue("@Qtr",blockPeriod.Qtr);
                cmd.Parameters.AddWithValue("@Period",blockPeriod.Period);
                cmd.Parameters.AddWithValue("@EffectiveDateForm", fromto.From);
                cmd.Parameters.AddWithValue("@EffectiveDateTo", fromto.To);
                cmd.Parameters.AddWithValue("@DiscPer", blockPeriod.DiscPer);
                cmd.Parameters.AddWithValue("@Factor", blockPeriod.Factor);
                cmd.Parameters.AddWithValue("@MinVal", blockPeriod.MinVal);
                cmd.Parameters.AddWithValue("@MaxPtaxPer", blockPeriod.MaxPtaxPer);
                cmd.Parameters.AddWithValue("@AddPtaxPer1", blockPeriod.AddPtaxPer1);
                cmd.Parameters.AddWithValue("AddPtaxPer2", blockPeriod.AddPtaxPer2);
                cmd.Parameters.AddWithValue("@CurrentUserId", blockPeriod.CurrentUserId);
                //comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                //comd.Parameters.AddWithValue("@LastName", obj.LastName);
                //comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                //comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                //comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                //comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                //comd.Parameters.AddWithValue("@UserType", obj.UserType);
                //comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                //comd.Parameters.AddWithValue("@UserName", obj.UserName);
                //comd.Parameters.AddWithValue("@Password", Password);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch 
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object GetAllBlockPeriod()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "GetAllBlockPeriod";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
                var lst = dt.AsEnumerable().Select(t => new {
                    MunicipalityID = t.Field<int>("MunicipalityID"),
                    YearId = t.Field<int?>("YearId"),
                    YearDesc = t.Field<string>("YearDesc"),
                    QtrDesc = t.Field<int?>("QtrDesc"),
                    EffectiveDateForm = t.Field<DateTime?>("EffectiveDateForm"),
                    EffectiveDateTo = t.Field<DateTime?>("EffectiveDateTo"),
                    BlockPeriod = t.Field<decimal?>("BlockPeriod"),
                    DiscPer = t.Field<decimal?>("DiscPer"),
                    MinVal = t.Field<decimal?>("MinVal"),
                    MaxPtaxPer = t.Field<decimal?>("MaxPtaxPer"),
                    Factor = t.Field<decimal?>("Factor"),
                    AddPtaxPer1 = t.Field<decimal?>("AddPtaxPer1"),
                    AddPtaxPer2 = t.Field<decimal?>("AddPtaxPer2")

                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public object checkFinYear(int MunicipalityID, int finyear) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_checkFinYear";
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@finyear", finyear);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
                var lst = dt.AsEnumerable().Select(t => new {
                    MunicipalityID = t.Field<int>("MunicipalityID"),
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
    }
}