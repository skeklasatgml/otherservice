﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class AuthorityBll : IDisposable
    {
        protected DbHandler _db;
        protected SqlConnection cn;
        public AuthorityBll()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }
        public List<Authority> GetAuthorities(int? authId=null, bool? isActive=null)
        {
            SqlCommand cmd = _db.NewCommand("udma.Get_Authorities",cn,CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@authId", authId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@isActive", isActive.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<Authority>();
        }
        public void Dispose()
        {
            cn.Close();
        }
    }
}
