﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.ViewModels;
using static Valuation_Board.ViewModels.Assessment_VM;
using static Valuation_Board.ViewModels.Assessment_VM.AssessmentForm;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssessmentBll:AssessmentBase, IDisposable
    {
        
        public AssessmentBll()
        {
            
        }
        #region save assessment data
        public bool AddNewHolding1(AssessmentForm assessmentForm,int municipalityId,long userId)
        {
            return true;
            //try
            //{
            //    var CalculatedModel = CalculateValuation(assessmentForm._ValuationDetails, municipalityId);
            //    if (CalculatedModel == null)
            //    {
            //        throw new InvalidOperationException("Error on valuation calculation");
            //    }
            //    
            //    SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn);
            //    cmd.Parameters.AddWithValue("@TabId", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@YearId", assessmentForm._YearID);
            //    cmd.Parameters.AddWithValue("@Idno", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@Idnosrl", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@WardId", assessmentForm._HoldingDetails.WardId);
            //    cmd.Parameters.AddWithValue("@LocationID", assessmentForm._HoldingDetails.LocationId);
            //    cmd.Parameters.AddWithValue("@HoldingNo", assessmentForm._HoldingDetails.HoldingNo);
                
            //    cmd.Parameters.AddWithValue("@AssesseeNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@AssesseeName",string.Join(",", assessmentForm._HoldingDetails.Owners.Select(t=>t.AssesseeName).ToList()));
            //    cmd.Parameters.AddWithValue("@AssesseeAddress", assessmentForm._HoldingDetails.Address);
            //    cmd.Parameters.AddWithValue("@ApplNo", assessmentForm._OtherTabDetails.ApplictnCaseNo == null ? 0 : assessmentForm._OtherTabDetails.ApplictnCaseNo);
            //    cmd.Parameters.AddWithValue("@ApplDt", assessmentForm._OtherTabDetails.ApplictnDt == null ? null: assessmentForm._OtherTabDetails.ApplictnDt);
            //    cmd.Parameters.AddWithValue("@OrderDt", assessmentForm._OtherTabDetails.DtOfOrder == null ? null : assessmentForm._OtherTabDetails.DtOfOrder);

            //    cmd.Parameters.AddWithValue("@ProcessingFees", assessmentForm._OtherTabDetails.ProCosingFees);
            //    cmd.Parameters.AddWithValue("@OtherFees", assessmentForm._OtherTabDetails.OtherFees);
            //    cmd.Parameters.AddWithValue("@PrimaryPhNo", string.IsNullOrEmpty(assessmentForm._OtherTabDetails.PrimaryPhNo) ? DBNull.Value : assessmentForm._OtherTabDetails.PrimaryPhNo as object);
            //    cmd.Parameters.AddWithValue("@PrimaryEmail", string.IsNullOrEmpty(assessmentForm._OtherTabDetails.PrimaryEmail) ? DBNull.Value : assessmentForm._OtherTabDetails.PrimaryEmail as object);
            //    cmd.Parameters.AddWithValue("@BoroughNo", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.BoroughNo) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.BoroughNo as object);

            //    cmd.Parameters.AddWithValue("@NameCAC", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@HoldingTypeGrp", assessmentForm._HoldingDetails.HoldingTypeGroupId==0?DBNull.Value: assessmentForm._HoldingDetails.HoldingTypeGroupId as object);
            //    cmd.Parameters.AddWithValue("@HoldingTypeGrpId", assessmentForm._HoldingDetails.HoldingTypeId == 0 ? DBNull.Value : assessmentForm._HoldingDetails.HoldingTypeId as object);
            //    cmd.Parameters.AddWithValue("@HoldArea",string.IsNullOrEmpty( assessmentForm._AdditionalHoldingDetails.HoldingAreaCode ) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.HoldingAreaCode as object);
            //    cmd.Parameters.AddWithValue("@ScoreZoneID", Bindder.ReplaceDbBull( assessmentForm._ValuationDetails.ZoneId));
            //    cmd.Parameters.AddWithValue("@ScoreUseID", Bindder.ReplaceDbBull(assessmentForm._ValuationDetails.NatureOfUseId));
            //    cmd.Parameters.AddWithValue("@ScoreCostID", Bindder.ReplaceDbBull(assessmentForm._ValuationDetails.ConstructionId ));


            //    cmd.Parameters.AddWithValue("@TotWt", CalculatedModel.Waitage);
            //    cmd.Parameters.AddWithValue("@HoldingAge", assessmentForm._ValuationDetails.Age);
            //    cmd.Parameters.AddWithValue("@LandType", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@VacLandAreaDc", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@VacLandAreaSft", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@LandAreaDc", DBNull.Value);

            //    cmd.Parameters.AddWithValue("@LandAreaKt", assessmentForm._ValuationDetails.LandArea_KT);
            //    cmd.Parameters.AddWithValue("@LandAreaCh", assessmentForm._ValuationDetails.LandArea_CH);
            //    cmd.Parameters.AddWithValue("@LandAreaSft", assessmentForm._ValuationDetails.LandArea_SFT);
            //    cmd.Parameters.AddWithValue("@CoveredArea", assessmentForm._ValuationDetails.BldgArea_SFT);
            //    cmd.Parameters.AddWithValue("@PlinthArea", assessmentForm._ValuationDetails.Plinth_SFT);
            //    cmd.Parameters.AddWithValue("@LandCost", CalculatedModel.LandCost);

            //    cmd.Parameters.AddWithValue("@AnnualVal", CalculatedModel.AnnualValuation);
            //    cmd.Parameters.AddWithValue("@DiscPer", CalculatedModel.DisPer);
            //    cmd.Parameters.AddWithValue("@DiscAmt", CalculatedModel.DiscAmt);
            //    cmd.Parameters.AddWithValue("@RevAnnualVal", CalculatedModel.RevAnnualVal);
            //    cmd.Parameters.AddWithValue("@PtaxPer", CalculatedModel.PtaxPer);
            //    cmd.Parameters.AddWithValue("@YrProptax", CalculatedModel.Ytax);
            //    cmd.Parameters.AddWithValue("@QtrProptax", CalculatedModel.Qtr_Ptax);
            //    cmd.Parameters.AddWithValue("@PrvQtrProptax", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@SrchgTag", assessmentForm._ValuationDetails.SrchgTag ? "Y" : "N");

            //    cmd.Parameters.AddWithValue("@QtrSrchg", CalculatedModel.Qtr_Schrg);
            //    cmd.Parameters.AddWithValue("@EduTag", assessmentForm._ValuationDetails.EduCessTag?"Y":"N");
            //    cmd.Parameters.AddWithValue("@QtrEduCess", CalculatedModel.EduCessAmt);
            //    cmd.Parameters.AddWithValue("@MouzaName",string.IsNullOrEmpty( assessmentForm._AdditionalHoldingDetails.MaujaName)?DBNull.Value : assessmentForm._AdditionalHoldingDetails.MaujaName as object);
            //    cmd.Parameters.AddWithValue("@JlNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@PSName", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@KhatianNoRS", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.KhatianNo_RS) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.KhatianNo_RS as object);
            //    cmd.Parameters.AddWithValue("@KhatianNoLR", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.KhatianNo_LR) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.KhatianNo_LR as object);

            //    cmd.Parameters.AddWithValue("@DagNoRS", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DaagNo_RS) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DaagNo_RS as object);
            //    cmd.Parameters.AddWithValue("@DagNoLR", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DaagNo_LR) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DaagNo_LR as object);
            //    cmd.Parameters.AddWithValue("@LandNatureROR", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@DeedNo", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DeedNo) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DeedNo as object);
            //    cmd.Parameters.AddWithValue("@DeedDt", assessmentForm._AdditionalHoldingDetails.DeedTypeId == 0 ?DBNull.Value: assessmentForm._AdditionalHoldingDetails.DeedDate.ToString("yyyy'-'MM'-'dd") as object);
            //    cmd.Parameters.AddWithValue("@DeedTypeId", assessmentForm._AdditionalHoldingDetails.DeedTypeId == 0 ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DeedTypeId as object);
            //    cmd.Parameters.AddWithValue("@WhetherAssessed", DBNull.Value);

            //    cmd.Parameters.AddWithValue("@OldYearId", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldAnnualValue", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldPTax", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@WhetherOwnChange", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldAssesseeName", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@WhetherHoldingChange", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldHoldingType", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@AADRHolding", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@ApptTotAreaSft", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OthAreaSft", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@BuildingDtl",  assessmentForm._OtherDetails.BuildingDtl.ReplaceDbNull());
            //    cmd.Parameters.AddWithValue("@LiftTag", assessmentForm._AdditionalHoldingDetails.Tag_Lift?"Y":"N");
            //    cmd.Parameters.AddWithValue("@DranageTag", assessmentForm._AdditionalHoldingDetails.Tag_Drainage ? "Y" : "N");
            //    cmd.Parameters.AddWithValue("@ToiletsTag", assessmentForm._AdditionalHoldingDetails.Tag_Toilets ? "Y" : "N");
            //    cmd.Parameters.AddWithValue("@ElectricityTag", assessmentForm._AdditionalHoldingDetails.Tag_Electricity ? "Y" : "N");
            //    cmd.Parameters.AddWithValue("@WaterTag", assessmentForm._AdditionalHoldingDetails.Tag_Water ? "Y" : "N");
            //    cmd.Parameters.AddWithValue("@FerruleSize", DBNull.Value);

            //    cmd.Parameters.AddWithValue("@ValTag", assessmentForm.AssessmentValTag);
            //    cmd.Parameters.AddWithValue("@ValYear", assessmentForm.FinYear);
            //    cmd.Parameters.AddWithValue("@ValQtr", assessmentForm.Qtr);
            //    cmd.Parameters.AddWithValue("@ValSrl", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@ValSubSrl", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@ValFromTo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@FromWardNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@FromStreetCode", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@FromHoldingNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldWardNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldStreetCode", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@OldHoldingNo", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@Mfactor", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@MacId", DBNull.Value);//put value if possible

            //    cmd.Parameters.AddWithValue("@ApprovedFlag", 'N');
            //    cmd.Parameters.AddWithValue("@ApprovedOn", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@ApprovedBy", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@ActiveFlag", 'Y');
            //    cmd.Parameters.AddWithValue("@RefTabId", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
            //    cmd.Parameters.AddWithValue("@InsertedBy", userId);

            //    cmd.Parameters.AddWithValue("@InsertedOn", DateTime.Now.ToString("yyyy'-'MM'-'dd"));
            //    cmd.Parameters.AddWithValue("@UpdatedBy", DBNull.Value);
            //    cmd.Parameters.AddWithValue("@UpdatedOn", DBNull.Value);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    var dt = _db.GetResult(cmd);
            //    if (dt.Rows.Count > 0)
            //    {
            //        if (dt.Rows[0].Field<int>("result") > 0)
            //        {
            //            return true;
            //        }
            //    }
            //    return false;
            //}
            //finally
            //{
            //    
            //}
        }
        public bool AddNewHolding(AssessmentForm assessmentForm, int municipalityId, long userId)
        {
            try
            {
                var CalculatedModel = new ValuationDetails();
                decimal SummeryLandCost = 0;
                decimal SummeryAnnualValuation = 0;
                decimal SummeryQtr_Ptax = 0;
                decimal SummeryQtr_Schrg = 0;
                decimal SummeryEduCessAmt = 0;
                //var CalculatedModel = CalculateValuation(assessmentForm._ValuationDetails, municipalityId);
                for (int i = 0; i < assessmentForm._ValuationDetails.Count; i++)
                {
                    CalculatedModel = CalculateValuation1(assessmentForm._ValuationDetails[i], municipalityId);
                    SummeryLandCost = SummeryLandCost + CalculatedModel.LandCost;
                    SummeryAnnualValuation = SummeryAnnualValuation + CalculatedModel.AnnualValuation;
                    SummeryQtr_Ptax = SummeryQtr_Ptax + CalculatedModel.Qtr_Ptax;
                    SummeryQtr_Schrg = SummeryQtr_Schrg + CalculatedModel.Qtr_Schrg;
                    SummeryEduCessAmt = SummeryEduCessAmt + CalculatedModel.EduCessAmt;
                }
                if (CalculatedModel == null)
                {
                    throw new InvalidOperationException("Error on valuation calculation");
                }
                
                SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn,tr);
                cmd.Parameters.AddWithValue("@TabId", DBNull.Value);
                cmd.Parameters.AddWithValue("@YearId", assessmentForm._YearID);
                cmd.Parameters.AddWithValue("@Idno", DBNull.Value);
                cmd.Parameters.AddWithValue("@Idnosrl", DBNull.Value);
                cmd.Parameters.AddWithValue("@WardId", assessmentForm._HoldingDetails.WardId);
                cmd.Parameters.AddWithValue("@LocationID", assessmentForm._HoldingDetails.LocationId);
                cmd.Parameters.AddWithValue("@HoldingNo", assessmentForm._HoldingDetails.HoldingNo);

                cmd.Parameters.AddWithValue("@AssesseeNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@AssesseeName", string.Join(",", assessmentForm._HoldingDetails.Owners.Select(t => t.AssesseeName).ToList()));
                cmd.Parameters.AddWithValue("@AssesseeAddress", assessmentForm._HoldingDetails.Address);
                cmd.Parameters.AddWithValue("@ApplNo", assessmentForm._OtherTabDetails.ApplictnCaseNo == null ? 0 : assessmentForm._OtherTabDetails.ApplictnCaseNo);
                cmd.Parameters.AddWithValue("@ApplDt", assessmentForm._OtherTabDetails.ApplictnDt == null ? null : assessmentForm._OtherTabDetails.ApplictnDt);
                cmd.Parameters.AddWithValue("@OrderDt", assessmentForm._OtherTabDetails.DtOfOrder == null ? null : assessmentForm._OtherTabDetails.DtOfOrder);

                cmd.Parameters.AddWithValue("@ProcessingFees", assessmentForm._OtherTabDetails.ProCosingFees);
                cmd.Parameters.AddWithValue("@OtherFees", assessmentForm._OtherTabDetails.OtherFees);
                cmd.Parameters.AddWithValue("@PrimaryPhNo", string.IsNullOrEmpty(assessmentForm._OtherTabDetails.PrimaryPhNo) ? DBNull.Value : assessmentForm._OtherTabDetails.PrimaryPhNo as object);
                cmd.Parameters.AddWithValue("@PrimaryEmail", string.IsNullOrEmpty(assessmentForm._OtherTabDetails.PrimaryEmail) ? DBNull.Value : assessmentForm._OtherTabDetails.PrimaryEmail as object);
                cmd.Parameters.AddWithValue("@BoroughNo", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.BoroughNo) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.BoroughNo as object);

                cmd.Parameters.AddWithValue("@NameCAC", DBNull.Value);
                cmd.Parameters.AddWithValue("@HoldingTypeGrp", assessmentForm._HoldingDetails.HoldingTypeGroupId == 0 ? DBNull.Value : assessmentForm._HoldingDetails.HoldingTypeGroupId as object);
                //cmd.Parameters.AddWithValue("@HoldingTypeGrpId", assessmentForm._HoldingDetails.HoldingTypeId == 0 ? DBNull.Value : assessmentForm._HoldingDetails.HoldingTypeId as object);
                cmd.Parameters.AddWithValue("@HoldArea", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.HoldingAreaCode) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.HoldingAreaCode as object);
                //cmd.Parameters.AddWithValue("@ScoreZoneID", Bindder.ReplaceDbBull( assessmentForm._ValuationDetails.ZoneId));
                //cmd.Parameters.AddWithValue("@ScoreUseID", Bindder.ReplaceDbBull(assessmentForm._ValuationDetails.NatureOfUseId));
                //cmd.Parameters.AddWithValue("@ScoreCostID", Bindder.ReplaceDbBull(assessmentForm._ValuationDetails.ConstructionId ));
                cmd.Parameters.AddWithValue("@ScoreZoneID", DBNull.Value);
                cmd.Parameters.AddWithValue("@ScoreUseID", DBNull.Value);
                cmd.Parameters.AddWithValue("@ScoreCostID", DBNull.Value);


                cmd.Parameters.AddWithValue("@TotWt", CalculatedModel.Waitage);
                //cmd.Parameters.AddWithValue("@HoldingAge", assessmentForm._ValuationDetails.Age);
                cmd.Parameters.AddWithValue("@HoldingAge", DBNull.Value);
                cmd.Parameters.AddWithValue("@LandType", DBNull.Value);
                cmd.Parameters.AddWithValue("@VacLandAreaDc", DBNull.Value);
                cmd.Parameters.AddWithValue("@VacLandAreaSft", DBNull.Value);
                cmd.Parameters.AddWithValue("@LandAreaDc", DBNull.Value);

                //cmd.Parameters.AddWithValue("@LandAreaKt", assessmentForm._ValuationDetails.LandArea_KT);
                //cmd.Parameters.AddWithValue("@LandAreaCh", assessmentForm._ValuationDetails.LandArea_CH);
                //cmd.Parameters.AddWithValue("@LandAreaSft", assessmentForm._ValuationDetails.LandArea_SFT);
                //cmd.Parameters.AddWithValue("@CoveredArea", assessmentForm._ValuationDetails.BldgArea_SFT);
                //cmd.Parameters.AddWithValue("@PlinthArea", assessmentForm._ValuationDetails.Plinth_SFT);
                cmd.Parameters.AddWithValue("@LandAreaKt", DBNull.Value);
                cmd.Parameters.AddWithValue("@LandAreaCh", DBNull.Value);
                cmd.Parameters.AddWithValue("@LandAreaSft", DBNull.Value);
                cmd.Parameters.AddWithValue("@CoveredArea", DBNull.Value);
                cmd.Parameters.AddWithValue("@PlinthArea", DBNull.Value);
                cmd.Parameters.AddWithValue("@LandCost", SummeryLandCost);

                cmd.Parameters.AddWithValue("@AnnualVal", SummeryAnnualValuation);
                cmd.Parameters.AddWithValue("@DiscPer", CalculatedModel.DisPer);
                cmd.Parameters.AddWithValue("@DiscAmt", CalculatedModel.DiscAmt);
                cmd.Parameters.AddWithValue("@RevAnnualVal", CalculatedModel.RevAnnualVal);
                cmd.Parameters.AddWithValue("@PtaxPer", CalculatedModel.PtaxPer);
                cmd.Parameters.AddWithValue("@YrProptax", CalculatedModel.Ytax);
                cmd.Parameters.AddWithValue("@QtrProptax", SummeryQtr_Ptax);
                cmd.Parameters.AddWithValue("@PrvQtrProptax", DBNull.Value);
                //cmd.Parameters.AddWithValue("@SrchgTag", assessmentForm._ValuationDetails.SrchgTag ? "Y" : "N");
                cmd.Parameters.AddWithValue("@SrchgTag", DBNull.Value);

                cmd.Parameters.AddWithValue("@QtrSrchg", SummeryQtr_Schrg);
                //cmd.Parameters.AddWithValue("@EduTag", assessmentForm._ValuationDetails.EduCessTag?"Y":"N");
                cmd.Parameters.AddWithValue("@EduTag", DBNull.Value);
                cmd.Parameters.AddWithValue("@QtrEduCess", SummeryEduCessAmt);
                cmd.Parameters.AddWithValue("@MouzaName", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.MaujaName) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.MaujaName as object);
                cmd.Parameters.AddWithValue("@JlNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@PSName", DBNull.Value);
                cmd.Parameters.AddWithValue("@KhatianNoRS", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.KhatianNo_RS) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.KhatianNo_RS as object);
                cmd.Parameters.AddWithValue("@KhatianNoLR", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.KhatianNo_LR) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.KhatianNo_LR as object);

                cmd.Parameters.AddWithValue("@DagNoRS", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DaagNo_RS) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DaagNo_RS as object);
                cmd.Parameters.AddWithValue("@DagNoLR", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DaagNo_LR) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DaagNo_LR as object);
                cmd.Parameters.AddWithValue("@LandNatureROR", DBNull.Value);
                cmd.Parameters.AddWithValue("@DeedNo", string.IsNullOrEmpty(assessmentForm._AdditionalHoldingDetails.DeedNo) ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DeedNo as object);
                cmd.Parameters.AddWithValue("@DeedDt", assessmentForm._AdditionalHoldingDetails.DeedTypeId == 0 ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DeedDate.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DeedTypeId", assessmentForm._AdditionalHoldingDetails.DeedTypeId == 0 ? DBNull.Value : assessmentForm._AdditionalHoldingDetails.DeedTypeId as object);
                cmd.Parameters.AddWithValue("@WhetherAssessed", DBNull.Value);

                cmd.Parameters.AddWithValue("@OldYearId", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldAnnualValue", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldPTax", DBNull.Value);
                cmd.Parameters.AddWithValue("@WhetherOwnChange", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldAssesseeName", DBNull.Value);
                cmd.Parameters.AddWithValue("@WhetherHoldingChange", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldHoldingType", DBNull.Value);
                cmd.Parameters.AddWithValue("@AADRHolding", DBNull.Value);
                cmd.Parameters.AddWithValue("@ApptTotAreaSft", DBNull.Value);
                cmd.Parameters.AddWithValue("@OthAreaSft", DBNull.Value);
                cmd.Parameters.AddWithValue("@BuildingDtl", assessmentForm._OtherDetails.BuildingDtl.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@LiftTag", assessmentForm._AdditionalHoldingDetails.Tag_Lift ? "Y" : "N");
                cmd.Parameters.AddWithValue("@DranageTag", assessmentForm._AdditionalHoldingDetails.Tag_Drainage ? "Y" : "N");
                cmd.Parameters.AddWithValue("@ToiletsTag", assessmentForm._AdditionalHoldingDetails.Tag_Toilets ? "Y" : "N");
                cmd.Parameters.AddWithValue("@ElectricityTag", assessmentForm._AdditionalHoldingDetails.Tag_Electricity ? "Y" : "N");
                cmd.Parameters.AddWithValue("@WaterTag", assessmentForm._AdditionalHoldingDetails.Tag_Water ? "Y" : "N");
                cmd.Parameters.AddWithValue("@FerruleSize", DBNull.Value);

                cmd.Parameters.AddWithValue("@ValTag", assessmentForm.AssessmentValTag);
                cmd.Parameters.AddWithValue("@ValYear", assessmentForm.FinYear);
                cmd.Parameters.AddWithValue("@ValQtr", assessmentForm.Qtr);
                cmd.Parameters.AddWithValue("@ValSrl", DBNull.Value);
                cmd.Parameters.AddWithValue("@ValSubSrl", DBNull.Value);
                cmd.Parameters.AddWithValue("@ValFromTo", DBNull.Value);
                cmd.Parameters.AddWithValue("@FromWardNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@FromStreetCode", DBNull.Value);
                cmd.Parameters.AddWithValue("@FromHoldingNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldWardNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldStreetCode", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldHoldingNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@Mfactor", DBNull.Value);
                cmd.Parameters.AddWithValue("@MacId", DBNull.Value);//put value if possible

                cmd.Parameters.AddWithValue("@ApprovedFlag", 'N');
                cmd.Parameters.AddWithValue("@ApprovedOn", DBNull.Value);
                cmd.Parameters.AddWithValue("@ApprovedBy", DBNull.Value);
                cmd.Parameters.AddWithValue("@ActiveFlag", 'Y');
                cmd.Parameters.AddWithValue("@RefTabId", DBNull.Value);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@InsertedBy", userId);

                cmd.Parameters.AddWithValue("@InsertedOn", DateTime.Now.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@UpdatedBy", DBNull.Value);
                cmd.Parameters.AddWithValue("@UpdatedOn", DBNull.Value);
                cmd.CommandType = CommandType.StoredProcedure;
                var dt = _db.GetResult(cmd);
                if (!(dt.Rows.Count > 0))
                {
                    return false;
                    
                }
                var assmtTransId= Convert.ToInt64(dt.Rows[0].Field<long>("TabId"));
                //insert additional details
                if (assessmentForm._AdditionalHoldingDetails.Sanction_Bldng_Plan != null)
                {
                    cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                    cmd.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                    cmd.Parameters.AddWithValue("@InfoTypeId", 9);
                    cmd.Parameters.AddWithValue("@InfoTypeIdVal", assessmentForm._AdditionalHoldingDetails.Sanction_Bldng_Plan);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                    cmd.CommandType = CommandType.StoredProcedure;
                    _db.GetScalar<object>(cmd);
                }
                if (assessmentForm._AdditionalHoldingDetails.Adsr_Office != null)
                {
                    cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                    cmd.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                    cmd.Parameters.AddWithValue("@InfoTypeId", 10);
                    cmd.Parameters.AddWithValue("@InfoTypeIdVal", assessmentForm._AdditionalHoldingDetails.Adsr_Office);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                    cmd.CommandType = CommandType.StoredProcedure;
                    _db.GetScalar<object>(cmd);
                }
                if (assessmentForm._AdditionalHoldingDetails.Current_Market_Value != 0)
                {
                    cmd = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                    cmd.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                    cmd.Parameters.AddWithValue("@InfoTypeId", 11);
                    cmd.Parameters.AddWithValue("@InfoTypeIdVal", assessmentForm._AdditionalHoldingDetails.Current_Market_Value);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                    cmd.CommandType = CommandType.StoredProcedure;
                    _db.GetScalar<object>(cmd);
                }
                for (int i = 0; i < assessmentForm._ValuationDetails.Count; i++)
                {
                    cmd = _db.NewCommand("Insert_Annual_Valuation_Details", cn, tr);
                    cmd.Parameters.AddWithValue("@HoldingTypeGrpId", assessmentForm._ValuationDetails[i].HoldingTypeId);
                    cmd.Parameters.AddWithValue("@ScoreZoneID", assessmentForm._ValuationDetails[i].ZoneId);
                    cmd.Parameters.AddWithValue("@ScoreZoneValue", assessmentForm._ValuationDetails[i].ZoneVal);
                    cmd.Parameters.AddWithValue("@ScoreUseID", assessmentForm._ValuationDetails[i].NatureOfUseId);
                    cmd.Parameters.AddWithValue("@ScoreUseValue", assessmentForm._ValuationDetails[i].NatureOfUseVal);
                    cmd.Parameters.AddWithValue("@ConstructionID", assessmentForm._ValuationDetails[i].ConstructionId);
                    cmd.Parameters.AddWithValue("@ConstructionValue", assessmentForm._ValuationDetails[i].ConstructionVal);
                    cmd.Parameters.AddWithValue("@LandAreaKt", assessmentForm._ValuationDetails[i].LandArea_KT);
                    cmd.Parameters.AddWithValue("@LandAreaCh", assessmentForm._ValuationDetails[i].LandArea_CH);
                    cmd.Parameters.AddWithValue("@LandAreaSft", assessmentForm._ValuationDetails[i].LandArea_SFT);
                    cmd.Parameters.AddWithValue("@BuildingAreaSft", assessmentForm._ValuationDetails[i].BldgArea_SFT);
                    cmd.Parameters.AddWithValue("@PlinthSft", assessmentForm._ValuationDetails[i].Plinth_SFT);
                    cmd.Parameters.AddWithValue("@Age", assessmentForm._ValuationDetails[i].Age);
                    cmd.Parameters.AddWithValue("@SurchApplicableTag", assessmentForm._ValuationDetails[i].SrchgTag);
                    cmd.Parameters.AddWithValue("@AnnualValue", assessmentForm._ValuationDetails[i].AnnualValuation);
                    cmd.Parameters.AddWithValue("@QtrPTax", assessmentForm._ValuationDetails[i].Qtr_Ptax);
                    cmd.Parameters.AddWithValue("@QtrSrchg", assessmentForm._ValuationDetails[i].Qtr_Schrg);
                    cmd.Parameters.AddWithValue("@LandCost", assessmentForm._ValuationDetails[i].LandCost);
                    cmd.Parameters.AddWithValue("@EduCess", assessmentForm._ValuationDetails[i].Qtr_EduCess);
                    cmd.Parameters.AddWithValue("@AssmtId", assmtTransId);
                    cmd.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                    cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                    cmd.Parameters.AddWithValue("@InsertedBy", userId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    _db.GetScalar<object>(cmd);
                }
                if (assmtTransId != 0)
                {
                    return true;
                }
                return false;
            }
            finally
            {
                
            }
        }
        public void Approve(List<ApprovalPayload> Approvals,int municipalityId,long userId)
        {
            
            try
            {
                //actual db object container
                List<AssmtTransactionModel> lst = new List<AssmtTransactionModel>();
                foreach (var item in Approvals)
                {
                    var curObj = GetUnApproved_Object(item.TabId, municipalityId);
                    if (curObj == null)
                    {
                        throw new NullReferenceException(string.Format("Requested Data not found, tabId={0},ValYear={1},ValQtr={2}", item.TabId, item.ValYear, item.ValQtr));
                    }
                    curObj.ValYear = item.ValYear;
                    curObj.ValQtr = item.ValQtr;
                    curObj.ApprovedFlag = "Y";
                    curObj.ApprovedOn = DateTime.Now;
                    lst.Add(curObj);
                }
                
                //
                foreach (var item in lst)
                {
                    var cmd = _db.NewCommand("UpdateMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                    cmd = Bindder.BindParams(cmd, item, userId);
                    cmd.ExecuteNonQuery();
                }
                
            }
            catch
            {
               
                throw;
            }
            finally
            {
                
            }
            
        }
        public int SaveReviewOrImprovement1(AssessmentForm assessmentForm,int municipalityId, long userId)
        {
            return 1;
            //try
            //{
            //    var lst = GetAssmtActiveList(null, null, assessmentForm._YearID, municipalityId, false, assessmentForm._HoldingDetails.WardId, assessmentForm._HoldingDetails.LocationId, assessmentForm._HoldingDetails.HoldingNo);
            //    if (lst.Count() > 0)
            //    {
            //        if (new List<string> { "R", "I" }.Contains(assessmentForm.AssessmentValTag.ToUpper().Trim()))
            //        {
            //            ValuationDetails valuationDetails = new ValuationDetails();
            //            valuationDetails.ZoneId = assessmentForm._ValuationDetails.ZoneId;
            //            valuationDetails.NatureOfUseId = assessmentForm._ValuationDetails.NatureOfUseId;
            //            valuationDetails.ConstructionId =assessmentForm._ValuationDetails.ConstructionId;
            //            valuationDetails.HoldingTypeGroupId = (int)assessmentForm._ValuationDetails.HoldingTypeGroupId;
            //            valuationDetails.LandArea_KT = assessmentForm._ValuationDetails.LandArea_KT;
            //            valuationDetails.LandArea_CH = assessmentForm._ValuationDetails.LandArea_CH;
            //            valuationDetails.LandArea_SFT = assessmentForm._ValuationDetails.LandArea_SFT;
            //            valuationDetails.BldgArea_SFT = assessmentForm._ValuationDetails.BldgArea_SFT;
            //            valuationDetails.Age = (int)assessmentForm._ValuationDetails.Age;
            //            valuationDetails.SrchgTag = assessmentForm._ValuationDetails.SrchgTag;
            //            valuationDetails.EduCessTag = assessmentForm._ValuationDetails.EduCessTag;

            //            CalculatedModel mdl = CalculateValuation(valuationDetails, municipalityId);
            //            if (mdl == null)
            //            {
            //                throw new NullReferenceException("Valuation details faild to calculate again. Error Unknown");
            //            }

            //            var tObj = lst[0];
            //            tObj.HoldingAge = (short)assessmentForm._ValuationDetails.Age;
            //            tObj.ValTag = assessmentForm.AssessmentValTag;
            //            tObj.CoveredArea = (int)assessmentForm._ValuationDetails.BldgArea_SFT;
            //            tObj.HoldingTypeGrp = (int)assessmentForm._ValuationDetails.HoldingTypeGroupId;
            //            tObj.LandAreaCh = (int)assessmentForm._ValuationDetails.LandArea_CH;
            //            tObj.LandAreaKt = (int)assessmentForm._ValuationDetails.LandArea_KT;
            //            tObj.LandAreaSft = (int)assessmentForm._ValuationDetails.LandArea_SFT;
            //            tObj.PlinthArea = (int)assessmentForm._ValuationDetails.Plinth_SFT;
            //            tObj.QtrSrchg = assessmentForm._ValuationDetails.Qtr_Schrg;
            //            tObj.ScoreZoneID = assessmentForm._ValuationDetails.ZoneId;
            //            tObj.ScoreCostID = assessmentForm._ValuationDetails.ConstructionId;
            //            tObj.ScoreUseID = assessmentForm._ValuationDetails.NatureOfUseId;
            //            tObj.EduTag = assessmentForm._ValuationDetails.EduCessTag?"Y":"N";
            //            tObj.SrchgTag = assessmentForm._ValuationDetails.SrchgTag ? "Y" : "N";
            //            tObj.ValYear = assessmentForm.FinYear;
            //            tObj.ValQtr = assessmentForm.Qtr;
            //            tObj.BuildingDtl = assessmentForm._OtherDetails.BuildingDtl;

            //            tObj.TotWt = mdl.Waitage;
            //            tObj.LandCost=mdl.LandCost;
            //            tObj.AnnualVal=mdl.AnnualValuation;
            //            tObj.DiscPer=mdl.DisPer;
            //            tObj.DiscAmt=mdl.DiscAmt;
            //            tObj.RevAnnualVal=mdl.RevAnnualVal;
            //            tObj.PtaxPer=mdl.PtaxPer;
            //            tObj.YrProptax=mdl.Ytax;
            //            tObj.QtrProptax=mdl.Qtr_Ptax;
            //            tObj.QtrEduCess = mdl.EduCessAmt;
            //            tObj.QtrSrchg = mdl.Qtr_Schrg;
                        

            //            
            //            SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn);
            //            cmd.CommandType = CommandType.StoredProcedure;
            //            Bindder.BindParams(cmd, tObj, userId);
            //            DataTable dt=_db.GetResult(cmd);
            //            if (dt.Rows.Count > 0)
            //            {
            //                return dt.Rows[0].Field<int>("result");
            //            }
            //            return 0;
            //        }
            //        else
            //        {
            //            throw new InvalidOperationException("Invalid operation. Assessment val-tag should be in (R,P) for review or improvement");
            //        }
            //    }
            //    else
            //    {
            //        throw new NullReferenceException(string.Format("Selected Holding {0} not found", assessmentForm._HoldingDetails.HoldingNo));
            //    }
            //}
            //finally
            //{
            //    
            //}
        }
        public int SaveReviewOrImprovement(AssessmentForm assessmentForm, int municipalityId, long userId)
        {
            try
            {
                var lst = GetAssmtActiveList(null, null, assessmentForm._YearID, municipalityId, false, assessmentForm._HoldingDetails.WardId, assessmentForm._HoldingDetails.LocationId, assessmentForm._HoldingDetails.HoldingNo);
                if (lst.Count() > 0)
                {
                    if (new List<string> { "R", "I" }.Contains(assessmentForm.AssessmentValTag.ToUpper().Trim()))
                    {
                        ValuationDetails valuationDetails = new ValuationDetails();
                        var CalculatedModel = new ValuationDetails();
                        //added by Eklas 01/10/2018
                        decimal SummeryLandCost = 0;
                        decimal SummeryAnnualValuation = 0;
                        decimal SummeryQtr_Ptax = 0;
                        decimal SummeryQtr_Schrg = 0;
                        decimal SummeryEduCessAmt = 0;
                        for (int i = 0; i < assessmentForm._ValuationDetails.Count; i++)
                        {
                            CalculatedModel = CalculateValuation1(assessmentForm._ValuationDetails[i], municipalityId);
                            SummeryLandCost = SummeryLandCost + CalculatedModel.LandCost;
                            SummeryAnnualValuation = SummeryAnnualValuation + CalculatedModel.AnnualValuation;
                            SummeryQtr_Ptax = SummeryQtr_Ptax + CalculatedModel.Qtr_Ptax;
                            SummeryQtr_Schrg = SummeryQtr_Schrg + CalculatedModel.Qtr_Schrg;
                            SummeryEduCessAmt = SummeryEduCessAmt + CalculatedModel.EduCessAmt;
                        }
                        // 

                        var tObj = lst[0];
                        tObj.ValTag = assessmentForm.AssessmentValTag;
                        tObj.ValYear = assessmentForm.FinYear;
                        tObj.ValQtr = assessmentForm.Qtr;
                        tObj.BuildingDtl = assessmentForm._OtherDetails.BuildingDtl;
                        tObj.TotWt = CalculatedModel.Waitage;
                        tObj.LandCost = SummeryLandCost;
                        tObj.AnnualVal = SummeryAnnualValuation;
                        tObj.DiscPer = CalculatedModel.DisPer;
                        tObj.DiscAmt = CalculatedModel.DiscAmt;
                        tObj.RevAnnualVal = CalculatedModel.RevAnnualVal;
                        tObj.PtaxPer = CalculatedModel.PtaxPer;
                        tObj.YrProptax = CalculatedModel.Ytax;
                        tObj.QtrProptax = SummeryQtr_Ptax;
                        tObj.QtrEduCess = SummeryEduCessAmt;
                        tObj.QtrSrchg = SummeryQtr_Schrg;
                        
                        SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn, tr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        Bindder.BindParams(cmd, tObj, userId);
                        var dt = _db.GetResult(cmd);
                        if (!(dt.Rows.Count > 0))
                        {
                            return 0;

                        }
                        var assmtTransId = Convert.ToInt64(dt.Rows[0].Field<long>("TabId"));
                        //var assmtTransRowId = _db.GetScalar<int>(cmd);
                        //cmd = _db.NewCommand("GetAssmtIdUsingRowNumber", cn);
                        //cmd.Parameters.AddWithValue("@rowNumber", assmtTransRowId);
                        //cmd.CommandType = CommandType.StoredProcedure;
                        //DataTable dt = _db.GetResult(cmd);
                        //var assmtTransId = dt.Rows[0].Field<long>("TabId");

                        for (int i = 0; i < assessmentForm._ValuationDetails.Count; i++)
                        {
                           
                                cmd = _db.NewCommand("Insert_Annual_Valuation_Details", cn, tr);
                                cmd.Parameters.AddWithValue("@HoldingTypeGrpId", assessmentForm._ValuationDetails[i].HoldingTypeId);
                                cmd.Parameters.AddWithValue("@ScoreZoneID", assessmentForm._ValuationDetails[i].ZoneId);
                                cmd.Parameters.AddWithValue("@ScoreZoneValue", assessmentForm._ValuationDetails[i].ZoneVal);
                                cmd.Parameters.AddWithValue("@ScoreUseID", assessmentForm._ValuationDetails[i].NatureOfUseId);
                                cmd.Parameters.AddWithValue("@ScoreUseValue", assessmentForm._ValuationDetails[i].NatureOfUseVal);
                                cmd.Parameters.AddWithValue("@ConstructionID", assessmentForm._ValuationDetails[i].ConstructionId);
                                cmd.Parameters.AddWithValue("@ConstructionValue", assessmentForm._ValuationDetails[i].ConstructionVal);
                                cmd.Parameters.AddWithValue("@LandAreaKt", assessmentForm._ValuationDetails[i].LandArea_KT);
                                cmd.Parameters.AddWithValue("@LandAreaCh", assessmentForm._ValuationDetails[i].LandArea_CH);
                                cmd.Parameters.AddWithValue("@LandAreaSft", assessmentForm._ValuationDetails[i].LandArea_SFT);
                                cmd.Parameters.AddWithValue("@BuildingAreaSft", assessmentForm._ValuationDetails[i].BldgArea_SFT);
                                cmd.Parameters.AddWithValue("@PlinthSft", assessmentForm._ValuationDetails[i].Plinth_SFT);
                                cmd.Parameters.AddWithValue("@Age", assessmentForm._ValuationDetails[i].Age);
                                cmd.Parameters.AddWithValue("@SurchApplicableTag", assessmentForm._ValuationDetails[i].SrchgTag);
                                cmd.Parameters.AddWithValue("@AnnualValue", assessmentForm._ValuationDetails[i].AnnualValuation);
                                cmd.Parameters.AddWithValue("@QtrPTax", assessmentForm._ValuationDetails[i].Qtr_Ptax);
                                cmd.Parameters.AddWithValue("@QtrSrchg", assessmentForm._ValuationDetails[i].Qtr_Schrg);
                                cmd.Parameters.AddWithValue("@LandCost", assessmentForm._ValuationDetails[i].LandCost);
                                cmd.Parameters.AddWithValue("@EduCess", assessmentForm._ValuationDetails[i].Qtr_EduCess);
                                cmd.Parameters.AddWithValue("@AssmtId", assmtTransId);
                                cmd.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                                cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                                cmd.Parameters.AddWithValue("@InsertedBy", userId);
                                cmd.CommandType = CommandType.StoredProcedure;
                                _db.GetScalar<object>(cmd);
                            //}
                            //if (assessmentForm._ValuationDetails[i].AnulValDtlTabId > 0 && assessmentForm._ValuationDetails[i].EditMode == "U")
                            //{
                            //    cmd = _db.NewCommand("UpdateValuationDetailsData", cn, tr);
                            //    cmd.Parameters.AddWithValue("@tabId", assessmentForm._ValuationDetails[i].AnulValDtlTabId);
                            //    cmd.Parameters.AddWithValue("@ScoreZoneID", assessmentForm._ValuationDetails[i].ZoneId);
                            //    cmd.Parameters.AddWithValue("@ScoreZoneValue", assessmentForm._ValuationDetails[i].ZoneVal);
                            //    cmd.Parameters.AddWithValue("@ScoreUseID", assessmentForm._ValuationDetails[i].NatureOfUseId);
                            //    cmd.Parameters.AddWithValue("@ScoreUseValue", assessmentForm._ValuationDetails[i].NatureOfUseVal);
                            //    cmd.Parameters.AddWithValue("@ConstructionID", assessmentForm._ValuationDetails[i].ConstructionId);
                            //    cmd.Parameters.AddWithValue("@ConstructionValue", assessmentForm._ValuationDetails[i].ConstructionVal);
                            //    cmd.Parameters.AddWithValue("@LandAreaKt", assessmentForm._ValuationDetails[i].LandArea_KT);
                            //    cmd.Parameters.AddWithValue("@LandAreaCh", assessmentForm._ValuationDetails[i].LandArea_CH);
                            //    cmd.Parameters.AddWithValue("@LandAreaSft", assessmentForm._ValuationDetails[i].LandArea_SFT);
                            //    cmd.Parameters.AddWithValue("@BuildingAreaSft", assessmentForm._ValuationDetails[i].BldgArea_SFT);
                            //    cmd.Parameters.AddWithValue("@PlinthSft", assessmentForm._ValuationDetails[i].Plinth_SFT);
                            //    cmd.Parameters.AddWithValue("@Age", assessmentForm._ValuationDetails[i].Age);
                            //    cmd.Parameters.AddWithValue("@SurchApplicableTag", assessmentForm._ValuationDetails[i].SrchgTag);
                            //    cmd.Parameters.AddWithValue("@AnnualValue", assessmentForm._ValuationDetails[i].AnnualValuation);
                            //    cmd.Parameters.AddWithValue("@QtrPTax", assessmentForm._ValuationDetails[i].Qtr_Ptax);
                            //    cmd.Parameters.AddWithValue("@QtrSrchg", assessmentForm._ValuationDetails[i].Qtr_Schrg);
                            //    cmd.Parameters.AddWithValue("@LandCost", assessmentForm._ValuationDetails[i].LandCost);
                            //    cmd.Parameters.AddWithValue("@EduCess", assessmentForm._ValuationDetails[i].Qtr_EduCess);
                            //    cmd.Parameters.AddWithValue("@AssmtId", assessmentForm._ValuationDetails[i].AssmtId);
                            //    cmd.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                            //    cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                            //    cmd.Parameters.AddWithValue("@UpdatedBy", userId);
                            //    cmd.CommandType = CommandType.StoredProcedure;
                            //    _db.GetScalar<object>(cmd);
                            //}
                            //if (assessmentForm._ValuationDetails[i].AnulValDtlTabId > 0 && assessmentForm._ValuationDetails[i].EditMode == "D")
                            //{
                            //    cmd = _db.NewCommand("DeleteValuationDetailsData", cn, tr);
                            //    cmd.Parameters.AddWithValue("@tabId", assessmentForm._ValuationDetails[i].AnulValDtlTabId);
                            //    cmd.Parameters.AddWithValue("@deletedBy", userId);
                            //    cmd.CommandType = CommandType.StoredProcedure;
                            //    _db.GetScalar<object>(cmd);
                            //}
                        }
                        if (assmtTransId > 0)
                        {
                            return 1;
                        }
                        return 0;
                    }
                    else
                    {
                        throw new InvalidOperationException("Invalid operation. Assessment val-tag should be in (R,P) for review or improvement");
                    }
                }
                else
                {
                    throw new NullReferenceException(string.Format("Selected Holding {0} not found", assessmentForm._HoldingDetails.HoldingNo));
                }
            }
            finally
            {
                
            }
        }
        public int SaveSeparation1(AssmtTransactionModel existing,List<AssmtTransactionModel> newHoldings,int municpalityId,long userId)
        {
            ////SqlTransaction tr = null;
            try
            {
                existing.MunicipalityID = municpalityId;
                existing.ValTag = "M";
                existing.ValFromTo = "F";
                existing.ApprovedFlag = "N";
                existing.ActiveFlag = "Y";
                existing.ApprovedOn = null;
                existing.InsertedBy =(int) userId;
                existing.InsertedOn = DateTime.Now;
                existing.UpdatedOn = null;
                existing.UpdatedBy = null;
                existing.RefTabId = existing.TabId;

                newHoldings.ForEach(t =>
                {
                    t.ValFromTo = "T";
                    t.TabId = 0;
                    t.MunicipalityID = municpalityId;
                    t.ValTag = "M";
                    t.ActiveFlag = "Y";
                    existing.ApprovedFlag = "N";
                    t.ApprovedOn = null;
                    t.InsertedBy = (int)userId;
                    t.InsertedOn = DateTime.Now;
                    t.UpdatedOn = null;
                    t.UpdatedBy = null;

                    t.RefTabId = existing.RefTabId ;
                    t.FromHoldingNo = existing.HoldingNo;
                    t.FromStreetCode = existing.LocationID;
                    t.FromWardNo = existing.WardId;

                    t.OldStreetCode= existing.LocationID;
                    t.OldWardNo= existing.WardId;
                    t.OldHoldingNo= existing.HoldingNo;
                    
                });

                Action calculateValuationData = () =>
                {
                    newHoldings.ForEach(t =>
                    {
                        var newValuationDetails = new ValuationDetails()
                        {
                            Age = t.HoldingAge ?? 0,
                            ConstructionId = t.ScoreCostID ,
                            ZoneId = t.ScoreZoneID ,
                            NatureOfUseId = t.ScoreUseID,
                            HoldingTypeGroupId = t.HoldingTypeGrp ?? 0,

                            LandArea_CH = t.LandAreaCh ?? 0,
                            LandArea_KT = t.LandAreaKt ?? 0,
                            LandArea_SFT = t.LandAreaSft ?? 0,
                            LandCost = t.LandCost ?? 0,
                            BldgArea_SFT = t.CoveredArea ?? 0,
                            EduCessTag = (t.EduTag == "Y" ? true : false),
                            SrchgTag = (t.SrchgTag == "Y" ? true : false)
                        };
                        var CalculatedModel = CalculateValuation(newValuationDetails, municpalityId);
                        if (CalculatedModel == null)
                        {
                            throw new InvalidOperationException("Can not calculate valuation details");
                        }
                        t.SetValuationCalculationData(CalculatedModel);
                    });
                };
                Action validateValuation = () =>
                {
                    //validate land area
                    if (Utils.Converter.Area.ConvertToSFT(existing.LandAreaKt ?? 0, existing.LandAreaCh ?? 0, existing.LandAreaSft ?? 0) < newHoldings.Sum(t => Utils.Converter.Area.ConvertToSFT(t.LandAreaKt ?? 0, t.LandAreaCh ?? 0, t.LandAreaSft ?? 0)))
                    {
                        throw new InvalidOperationException("Sum of new holding's land-area can not be greater than existing holding");
                    }

                    //validate building area
                    if ((existing.CoveredArea ?? 0) < newHoldings.Sum(t => t.CoveredArea ?? 0))
                    {
                        throw new InvalidOperationException("Sum of new holding's building area can not be greater than existing holding");
                    }
                };

                //validate existing and new holdings, all should be unique
                Action validateHodingNos=()=>{
                    var holdings = newHoldings.Select(t => t.HoldingNo).ToList();
                    //push existing holding to array
                    holdings.Add(existing.HoldingNo);

                    //check empty
                    if (holdings.Where(t => string.IsNullOrEmpty(t)).Count() > 0)
                    {
                        throw new InvalidOperationException("Holing no can not be empty");
                    }

                    if (holdings.Distinct().Count() < holdings.Count)
                    {
                        throw new InvalidOperationException("Duplicate holding no not allowed");
                    }
                };
                
                //step 1
                calculateValuationData();
                //step 2
                validateHodingNos();
                //step 3
                validateValuation();

                
                 
                SqlCommand cmdExisting = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                Bindder.BindParams(cmdExisting, existing, userId);
                DataTable dt = _db.GetResult(cmdExisting);
                if (dt.Rows.Count > 0)
                {
                    var valsrl = dt.Rows[0].Field<int>("result");
                    newHoldings.ForEach(t =>
                    {
                        //apply pending values
                        t.ValSrl = valsrl;//returned valsrl
                        SqlCommand cmdNew = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                        Bindder.BindParams(cmdNew, t, userId);
                        cmdNew.ExecuteNonQuery();
                    });

                    //change valuation of existing holding
                    cmdExisting = _db.NewCommand("UpdateMstAssmtTransValuation", cn, tr, CommandType.StoredProcedure);
                    cmdExisting.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                    cmdExisting.Parameters.AddWithValue("@ValSrl", valsrl);
                    cmdExisting.Parameters.AddWithValue("@ValTag", existing.ValTag);
                    cmdExisting.ExecuteNonQuery();
                }

                
                
            }
            catch
            {
                if (tr != null)
                {
                   
                }
                throw;
            }
            finally
            {
                
            }
            return 1;
        }
        public int SaveSeparation(AssmtTransactionWithValuationList existing, List<AssmtTransactionWithValuationList> newHoldings, int municpalityId, long userId)
        {            
            try
            {
                existing.MunicipalityID = municpalityId;
                existing.ValTag = "M";
                existing.ValFromTo = "F";
                existing.ApprovedFlag = "N";
                existing.ActiveFlag = "Y";
                existing.ApprovedOn = null;
                existing.InsertedBy = (int)userId;
                existing.InsertedOn = DateTime.Now;
                existing.UpdatedOn = null;
                existing.UpdatedBy = null;
                existing.RefTabId = existing.TabId;

                newHoldings.ForEach(t =>
                {
                    t.ValFromTo = "T";
                    t.TabId = 0;
                    t.MunicipalityID = municpalityId;
                    t.ValTag = "M";
                    t.ActiveFlag = "Y";
                    existing.ApprovedFlag = "N";
                    t.ApprovedOn = null;
                    t.InsertedBy = (int)userId;
                    t.InsertedOn = DateTime.Now;
                    t.UpdatedOn = null;
                    t.UpdatedBy = null;

                    t.RefTabId = existing.RefTabId;
                    t.FromHoldingNo = existing.HoldingNo;
                    t.FromStreetCode = existing.LocationID;
                    t.FromWardNo = existing.WardId;

                    t.OldStreetCode = existing.LocationID;
                    t.OldWardNo = existing.WardId;
                    t.OldHoldingNo = existing.HoldingNo;

                });
                decimal SummeryLandAreaKTOfAllTab = 0;
                decimal SummeryLandAreaCHOfAllTab = 0;
                decimal SummeryLandAreaSFTOfAllTab = 0;
                decimal SummeryBldngAreaSFTOfAllTab = 0;
                Action calculateValuationData = () =>
                {
                    newHoldings.ForEach(t =>
                    {
                        var CalculatedModel = new ValuationDetails();
                        decimal SummeryLandCost = 0;
                        decimal SummeryAnnualValuation = 0;
                        decimal SummeryQtr_Ptax = 0;
                        decimal SummeryQtr_Schrg = 0;
                        decimal SummeryEduCessAmt = 0;

                        decimal SummeryLandAreaKT = 0;
                        decimal SummeryLandAreaCH = 0;
                        decimal SummeryLandAreaSFT = 0;
                        decimal SummeryBldngAreaSFT = 0;
                        for (int i = 0; i < t._ValuationDetails.Count; i++)
                        {
                            CalculatedModel = CalculateValuation1(t._ValuationDetails[i], municpalityId);
                            SummeryLandCost = SummeryLandCost + CalculatedModel.LandCost;
                            SummeryAnnualValuation = SummeryAnnualValuation + CalculatedModel.AnnualValuation;
                            SummeryQtr_Ptax = SummeryQtr_Ptax + CalculatedModel.Qtr_Ptax;
                            SummeryQtr_Schrg = SummeryQtr_Schrg + CalculatedModel.Qtr_Schrg;
                            SummeryEduCessAmt = SummeryEduCessAmt + CalculatedModel.EduCessAmt;

                            SummeryLandAreaKT = SummeryLandAreaKT + CalculatedModel.LandArea_KT;
                            SummeryLandAreaCH = SummeryLandAreaCH + CalculatedModel.LandArea_CH;
                            SummeryLandAreaSFT = SummeryLandAreaSFT + CalculatedModel.LandArea_SFT;
                            SummeryBldngAreaSFT = SummeryBldngAreaSFT + CalculatedModel.BldgArea_SFT;
                        }
                        t.TotWt = CalculatedModel.Waitage;
                        t.LandCost = SummeryLandCost;
                        t.AnnualVal = SummeryAnnualValuation;
                        t.DiscPer = CalculatedModel.DisPer;
                        t.DiscAmt = CalculatedModel.DiscAmt;
                        t.RevAnnualVal = CalculatedModel.RevAnnualVal;
                        t.PtaxPer = CalculatedModel.PtaxPer;
                        t.YrProptax = CalculatedModel.Ytax;
                        t.QtrProptax = SummeryQtr_Ptax;
                        t.QtrSrchg = SummeryQtr_Schrg;
                        t.QtrEduCess = SummeryEduCessAmt;

                        SummeryLandAreaKTOfAllTab = SummeryLandAreaKTOfAllTab + SummeryLandAreaKT;
                        SummeryLandAreaCHOfAllTab = SummeryLandAreaCHOfAllTab + SummeryLandAreaCH;
                        SummeryLandAreaSFTOfAllTab = SummeryLandAreaSFTOfAllTab + SummeryLandAreaSFT;
                        SummeryBldngAreaSFTOfAllTab = SummeryBldngAreaSFTOfAllTab + SummeryBldngAreaSFT;

                        //for (int i = 0; i < t._ValuationDetails.Count; i++)
                        //{
                        //    var newValuationDetails = new ValuationDetails()
                        //    {
                        //        Age = t._ValuationDetails[i].Age > 0 ? t._ValuationDetails[i].Age : 0,
                        //        ConstructionId = t._ValuationDetails[i].ConstructionId,
                        //        ZoneId = t._ValuationDetails[i].ZoneId,
                        //        NatureOfUseId = t._ValuationDetails[i].NatureOfUseId,
                        //        HoldingTypeGroupId = t.HoldingTypeGrp ?? 0,

                        //        LandArea_CH = t._ValuationDetails[i].LandArea_CH > 0 ? t._ValuationDetails[i].LandArea_CH : 0,
                        //        LandArea_KT = t._ValuationDetails[i].LandArea_KT > 0 ? t._ValuationDetails[i].LandArea_KT : 0,
                        //        LandArea_SFT = t._ValuationDetails[i].LandArea_SFT > 0 ? t._ValuationDetails[i].LandArea_SFT : 0,
                        //        LandCost = t._ValuationDetails[i].LandCost > 0 ? t._ValuationDetails[i].LandCost : 0,
                        //        BldgArea_SFT = t._ValuationDetails[i].BldgArea_SFT > 0 ? t._ValuationDetails[i].BldgArea_SFT : 0,
                        //        EduCessTag = (t.EduTag == "Y" ? true : false),
                        //        SrchgTag = (t._ValuationDetails[i].SrchgTag == true ? true : false)
                        //    };
                        //    var CalculatedModel = CalculateValuation1(newValuationDetails, municpalityId);
                        //    if (CalculatedModel == null)
                        //    {
                        //        throw new InvalidOperationException("Can not calculate valuation details");
                        //    }
                        //    //  t.SetValuationCalculationData(CalculatedModel);
                        //    t.SetValuationCalculationData1(CalculatedModel);
                        //}

                    });
                };
                Action validateValuation = () =>
                {
                    //validate land area
                    if (Utils.Converter.Area.ConvertToSFT(existing.LandAreaKt ?? 0, existing.LandAreaCh ?? 0, existing.LandAreaSft ?? 0) < Utils.Converter.Area.ConvertToSFT(SummeryLandAreaKTOfAllTab, SummeryLandAreaCHOfAllTab, SummeryLandAreaSFTOfAllTab) /*newHoldings.Sum(t => Utils.Converter.Area.ConvertToSFT(t.LandAreaKt ?? 0, t.LandAreaCh ?? 0, t.LandAreaSft ?? 0))*/)
                    {
                        throw new InvalidOperationException("Sum of new holding's land-area can not be greater than existing holding");
                    }

                    //validate building area
                    if ((existing.CoveredArea ?? 0) < SummeryBldngAreaSFTOfAllTab /*newHoldings.Sum(t => t.CoveredArea ?? 0)*/)
                    {
                        throw new InvalidOperationException("Sum of new holding's building area can not be greater than existing holding");
                    }
                };

                //validate existing and new holdings, all should be unique
                Action validateHodingNos = () =>
                {
                    var holdings = newHoldings.Select(t => t.HoldingNo).ToList();
                    //push existing holding to array
                    holdings.Add(existing.HoldingNo);

                    //check empty
                    if (holdings.Where(t => string.IsNullOrEmpty(t)).Count() > 0)
                    {
                        throw new InvalidOperationException("Holing no can not be empty");
                    }

                    if (holdings.Distinct().Count() < holdings.Count)
                    {
                        throw new InvalidOperationException("Duplicate holding no not allowed");
                    }
                };

                //step 1
                calculateValuationData();
                //step 2
                validateHodingNos();
                //step 3
                validateValuation();

                
                //
                SqlCommand cmdExisting = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                Bindder.BindParams(cmdExisting, existing, userId);
                DataTable dt = _db.GetResult(cmdExisting);
                var assmtTransId = dt.Rows[0].Field<long>("TabId");
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < existing._ValuationDetails.Count; i++)
                    {
                        var valuationObject = existing._ValuationDetails[i];
                        var CalculatedModel = new ValuationDetails();
                        decimal SummeryLandCost = 0;
                        decimal SummeryAnnualValuation = 0;
                        decimal SummeryQtr_Ptax = 0;
                        decimal SummeryQtr_Schrg = 0;
                        decimal SummeryEduCessAmt = 0;

                        CalculatedModel = CalculateValuation1(valuationObject, municpalityId);
                        SummeryLandCost = CalculatedModel.LandCost;
                        SummeryAnnualValuation = CalculatedModel.AnnualValuation;
                        SummeryQtr_Ptax = CalculatedModel.Qtr_Ptax;
                        SummeryQtr_Schrg = CalculatedModel.Qtr_Schrg;
                        SummeryEduCessAmt = CalculatedModel.EduCessAmt;

                        var cmdNew = _db.NewCommand("Insert_Annual_Valuation_Details", cn, tr);
                        cmdNew.Parameters.AddWithValue("@HoldingTypeGrpId", valuationObject.HoldingTypeId);
                        cmdNew.Parameters.AddWithValue("@ScoreZoneID", valuationObject.ZoneId);
                        cmdNew.Parameters.AddWithValue("@ScoreZoneValue", valuationObject.ZoneVal);
                        cmdNew.Parameters.AddWithValue("@ScoreUseID", valuationObject.NatureOfUseId);
                        cmdNew.Parameters.AddWithValue("@ScoreUseValue", valuationObject.NatureOfUseVal);
                        cmdNew.Parameters.AddWithValue("@ConstructionID", valuationObject.ConstructionId);
                        cmdNew.Parameters.AddWithValue("@ConstructionValue", valuationObject.ConstructionVal);
                        cmdNew.Parameters.AddWithValue("@LandAreaKt", valuationObject.LandArea_KT);
                        cmdNew.Parameters.AddWithValue("@LandAreaCh", valuationObject.LandArea_CH);
                        cmdNew.Parameters.AddWithValue("@LandAreaSft", valuationObject.LandArea_SFT);
                        cmdNew.Parameters.AddWithValue("@BuildingAreaSft", valuationObject.BldgArea_SFT);
                        cmdNew.Parameters.AddWithValue("@PlinthSft", valuationObject.Plinth_SFT);
                        cmdNew.Parameters.AddWithValue("@Age", valuationObject.Age);
                        cmdNew.Parameters.AddWithValue("@SurchApplicableTag", valuationObject.SrchgTag);
                        cmdNew.Parameters.AddWithValue("@AnnualValue", SummeryAnnualValuation);
                        cmdNew.Parameters.AddWithValue("@QtrPTax", SummeryQtr_Ptax);
                        cmdNew.Parameters.AddWithValue("@QtrSrchg", SummeryQtr_Schrg);
                        cmdNew.Parameters.AddWithValue("@LandCost", SummeryLandCost);
                        cmdNew.Parameters.AddWithValue("@EduCess", SummeryEduCessAmt);
                        cmdNew.Parameters.AddWithValue("@AssmtId", assmtTransId);
                        cmdNew.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                        cmdNew.Parameters.AddWithValue("@Remarks", DBNull.Value);
                        cmdNew.Parameters.AddWithValue("@InsertedBy", userId);
                        cmdNew.CommandType = CommandType.StoredProcedure;
                        _db.GetScalar<object>(cmdNew);
                    }
                    var valsrl = dt.Rows[0].Field<int>("result");
                    newHoldings.ForEach(t =>
                    {
                        //apply pending values
                        t.ValSrl = valsrl;//returned valsrl
                        SqlCommand cmdNew = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                        Bindder.BindParams(cmdNew, t, userId);
                        DataTable dtt = _db.GetResult(cmdNew);
                        var t_assmtTransId = dtt.Rows[0].Field<long>("TabId");

                        if (t.Sanction_Bldng_Plan != null)
                        {
                            cmdNew = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                            cmdNew.Parameters.AddWithValue("@InfoTypeId", 9);
                            cmdNew.Parameters.AddWithValue("@InfoTypeIdVal", t.Sanction_Bldng_Plan);
                            cmdNew.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                            cmdNew.Parameters.AddWithValue("@userId", userId);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                            cmdNew.CommandType = CommandType.StoredProcedure;
                            _db.GetScalar<object>(cmdNew);
                        }
                        if (t.Adsr_Office != null)
                        {
                            cmdNew = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                            cmdNew.Parameters.AddWithValue("@InfoTypeId", 10);
                            cmdNew.Parameters.AddWithValue("@InfoTypeIdVal", t.Adsr_Office);
                            cmdNew.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                            cmdNew.Parameters.AddWithValue("@userId", userId);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                            cmdNew.CommandType = CommandType.StoredProcedure;
                            _db.GetScalar<object>(cmdNew);
                        }
                        if (t.Current_Market_Value != 0)
                        {
                            cmdNew = _db.NewCommand("Insert_assmt_OtherInfo", cn, tr);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtId", assmtTransId);
                            cmdNew.Parameters.AddWithValue("@InfoTypeId", 11);
                            cmdNew.Parameters.AddWithValue("@InfoTypeIdVal", t.Current_Market_Value);
                            cmdNew.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                            cmdNew.Parameters.AddWithValue("@userId", userId);
                            cmdNew.Parameters.AddWithValue("@TabIdAssmtIdFalg", "AT");
                            cmdNew.CommandType = CommandType.StoredProcedure;
                            _db.GetScalar<object>(cmdNew);
                        }

                        
                        
                        for (int i = 0; i < t._ValuationDetails.Count; i++)
                        {
                            var CalculatedModel = new ValuationDetails();
                            decimal SummeryLandCost = 0;
                            decimal SummeryAnnualValuation = 0;
                            decimal SummeryQtr_Ptax = 0;
                            decimal SummeryQtr_Schrg = 0;
                            decimal SummeryEduCessAmt = 0;

                            CalculatedModel = CalculateValuation1(t._ValuationDetails[i], municpalityId);
                            SummeryLandCost =  CalculatedModel.LandCost;
                            SummeryAnnualValuation =  CalculatedModel.AnnualValuation;
                            SummeryQtr_Ptax =  CalculatedModel.Qtr_Ptax;
                            SummeryQtr_Schrg =  CalculatedModel.Qtr_Schrg;
                            SummeryEduCessAmt =  CalculatedModel.EduCessAmt;

                            cmdNew = _db.NewCommand("Insert_Annual_Valuation_Details", cn, tr);
                            cmdNew.Parameters.AddWithValue("@HoldingTypeGrpId", t._ValuationDetails[i].HoldingTypeId);
                            cmdNew.Parameters.AddWithValue("@ScoreZoneID", t._ValuationDetails[i].ZoneId);
                            cmdNew.Parameters.AddWithValue("@ScoreZoneValue", t._ValuationDetails[i].ZoneVal);
                            cmdNew.Parameters.AddWithValue("@ScoreUseID", t._ValuationDetails[i].NatureOfUseId);
                            cmdNew.Parameters.AddWithValue("@ScoreUseValue", t._ValuationDetails[i].NatureOfUseVal);
                            cmdNew.Parameters.AddWithValue("@ConstructionID", t._ValuationDetails[i].ConstructionId);
                            cmdNew.Parameters.AddWithValue("@ConstructionValue", t._ValuationDetails[i].ConstructionVal);
                            cmdNew.Parameters.AddWithValue("@LandAreaKt", t._ValuationDetails[i].LandArea_KT);
                            cmdNew.Parameters.AddWithValue("@LandAreaCh", t._ValuationDetails[i].LandArea_CH);
                            cmdNew.Parameters.AddWithValue("@LandAreaSft", t._ValuationDetails[i].LandArea_SFT);
                            cmdNew.Parameters.AddWithValue("@BuildingAreaSft", t._ValuationDetails[i].BldgArea_SFT);
                            cmdNew.Parameters.AddWithValue("@PlinthSft", t._ValuationDetails[i].Plinth_SFT);
                            cmdNew.Parameters.AddWithValue("@Age", t._ValuationDetails[i].Age);
                            cmdNew.Parameters.AddWithValue("@SurchApplicableTag", t._ValuationDetails[i].SrchgTag);
                            cmdNew.Parameters.AddWithValue("@AnnualValue", SummeryAnnualValuation);
                            cmdNew.Parameters.AddWithValue("@QtrPTax", SummeryQtr_Ptax);
                            cmdNew.Parameters.AddWithValue("@QtrSrchg", SummeryQtr_Schrg);
                            cmdNew.Parameters.AddWithValue("@LandCost", SummeryLandCost);
                            cmdNew.Parameters.AddWithValue("@EduCess", SummeryEduCessAmt);
                            cmdNew.Parameters.AddWithValue("@AssmtId", t_assmtTransId);
                            cmdNew.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                            cmdNew.Parameters.AddWithValue("@Remarks", DBNull.Value);
                            cmdNew.Parameters.AddWithValue("@InsertedBy", userId);
                            cmdNew.CommandType = CommandType.StoredProcedure;
                            _db.GetScalar<object>(cmdNew);
                        }
                    });

                    //change valuation of existing holding
                    cmdExisting = _db.NewCommand("UpdateMstAssmtTransValuation", cn, tr, CommandType.StoredProcedure);
                    cmdExisting.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                    cmdExisting.Parameters.AddWithValue("@ValSrl", valsrl);
                    cmdExisting.Parameters.AddWithValue("@ValTag", existing.ValTag);
                    cmdExisting.ExecuteNonQuery();
                }
            }
            catch
            {
                if (tr != null)
                {
                   
                }
                throw;
            }
            finally
            {
                
            }
            return 1;
        }
        public void SaveAmalgamationData(List<AssmtTransactionModel> ChildHoldings, AssmtTransactionModel MohtherHolding, List<ValuationDetails> MotherHoldingValuationDetails, int municpalityId, long userId)
        {
            // //SqlTransaction tr = null;
            // //SqlTransaction tr = null;
            try
            {
                if (ChildHoldings == null || ChildHoldings.Count == 0)
                {
                    throw new NullReferenceException("child holdings required");
                }
                var reLoadChildHoldings = new List<AssmtTransactionModel>();
                ChildHoldings.ForEach(t => {
                    var relodHoldingTemp = GetAssessmtFormData(municpalityId, t.WardId, t.LocationID, t.HoldingNo);
                    if (relodHoldingTemp == null || relodHoldingTemp.ActualAssmtData == null)
                    {
                        throw new NullReferenceException("Holding not found");
                    }
                    relodHoldingTemp.ActualAssmtData.BuildingDtl = MohtherHolding.BuildingDtl;
                    reLoadChildHoldings.Add(relodHoldingTemp.ActualAssmtData);
                });

                //reassign datas
                reLoadChildHoldings.ForEach(t => {
                    t.MunicipalityID = municpalityId;
                    t.ValTag = "A";
                    t.ValFromTo = "F";
                    t.ApprovedFlag = "N";
                    t.ActiveFlag = "Y";
                    t.ApprovedOn = null;
                    t.InsertedBy = (int)userId;
                    t.InsertedOn = DateTime.Now;
                    t.UpdatedOn = null;
                    t.UpdatedBy = null;
                    t.RefTabId = t.TabId;
                });

                if (MohtherHolding == null)
                {
                    throw new NullReferenceException("Please provide mother holding");
                }
                var reloadedMohtherHolding_temp = GetAssessmtFormData(municpalityId, MohtherHolding.WardId, MohtherHolding.LocationID, MohtherHolding.HoldingNo);
                if (reloadedMohtherHolding_temp == null || reloadedMohtherHolding_temp.ActualAssmtData == null)
                {
                    throw new NullReferenceException("Mother holding not found");
                }

                var CalculatedModel = new ValuationDetails();
                decimal SummeryLandCost = 0;
                decimal SummeryAnnualValuation = 0;
                decimal SummeryQtr_Ptax = 0;
                decimal SummeryQtr_Schrg = 0;
                decimal SummeryEduCessAmt = 0;
                //var CalculatedModel = CalculateValuation(assessmentForm._ValuationDetails, municipalityId);
                for (int i = 0; i < MotherHoldingValuationDetails.Count; i++)
                {
                    CalculatedModel = CalculateValuation1(MotherHoldingValuationDetails[i], municpalityId);
                    SummeryLandCost = SummeryLandCost + CalculatedModel.LandCost;
                    SummeryAnnualValuation = SummeryAnnualValuation + CalculatedModel.AnnualValuation;
                    SummeryQtr_Ptax = SummeryQtr_Ptax + CalculatedModel.Qtr_Ptax;
                    SummeryQtr_Schrg = SummeryQtr_Schrg + CalculatedModel.Qtr_Schrg;
                    SummeryEduCessAmt = SummeryEduCessAmt + CalculatedModel.EduCessAmt;
                }
                if (CalculatedModel == null)
                {
                    throw new InvalidOperationException("Error on valuation calculation");
                }

                var reloadedMohtherHolding = reloadedMohtherHolding_temp.ActualAssmtData;

                reloadedMohtherHolding.LandCost = SummeryLandCost;
                reloadedMohtherHolding.AnnualVal = SummeryAnnualValuation;
                reloadedMohtherHolding.QtrProptax = SummeryQtr_Ptax;
                reloadedMohtherHolding.QtrSrchg = SummeryQtr_Schrg;
                reloadedMohtherHolding.QtrEduCess = SummeryEduCessAmt;

                reloadedMohtherHolding.MunicipalityID = municpalityId;
                reloadedMohtherHolding.ValTag = "A";
                reloadedMohtherHolding.ValFromTo = "T";
                reloadedMohtherHolding.ApprovedFlag = "N";
                reloadedMohtherHolding.ActiveFlag = "Y";
                reloadedMohtherHolding.ApprovedOn = null;
                reloadedMohtherHolding.InsertedBy = (int)userId;
                reloadedMohtherHolding.InsertedOn = DateTime.Now;
                reloadedMohtherHolding.UpdatedOn = null;
                reloadedMohtherHolding.UpdatedBy = null;
                reloadedMohtherHolding.RefTabId = reloadedMohtherHolding.TabId;
                reloadedMohtherHolding.BuildingDtl = MohtherHolding.BuildingDtl;

                SqlCommand cmdMother = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                Bindder.BindParams(cmdMother, reloadedMohtherHolding, userId);
                DataTable dt = _db.GetResult(cmdMother);
                var assmtTransId = Convert.ToInt64(dt.Rows[0].Field<long>("TabId"));
                if (dt.Rows.Count > 0)
                {
                    var valsrl = dt.Rows[0].Field<int>("result");
                    reLoadChildHoldings.ForEach(t =>
                    {
                        if (reloadedMohtherHolding.TabId != t.TabId)
                        {
                            //apply pending values
                            t.ValSrl = valsrl;//returned valsrl
                            SqlCommand cmdNew = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                            cmdNew.Parameters.AddWithValue("@MotherHoldingTabId", Bindder.ReplaceDbBull(assmtTransId));
                            Bindder.BindParams(cmdNew, t, userId);
                            cmdNew.ExecuteNonQuery();
                        }
                    });

                    //change valuation of existing holding
                    //var cmdExisting = _db.NewCommand("UpdateMstAssmtTransValuation", cn, tr, CommandType.StoredProcedure);
                    //cmdExisting.Parameters.AddWithValue("@MunicipalityID", municpalityId);
                    //cmdExisting.Parameters.AddWithValue("@ValSrl", valsrl);
                    //cmdExisting.Parameters.AddWithValue("@ValTag", reloadedMohtherHolding.ValTag);
                    //cmdExisting.ExecuteNonQuery();
                }

                
                for (int i = 0; i < MotherHoldingValuationDetails.Count; i++)
                {
                    var cmd = _db.NewCommand("Insert_Annual_Valuation_Details", cn, tr);
                    cmd.Parameters.AddWithValue("@HoldingTypeGrpId", MotherHoldingValuationDetails[i].HoldingTypeId);
                    cmd.Parameters.AddWithValue("@ScoreZoneID", MotherHoldingValuationDetails[i].ZoneId);
                    cmd.Parameters.AddWithValue("@ScoreZoneValue", MotherHoldingValuationDetails[i].ZoneVal);
                    cmd.Parameters.AddWithValue("@ScoreUseID", MotherHoldingValuationDetails[i].NatureOfUseId);
                    cmd.Parameters.AddWithValue("@ScoreUseValue", MotherHoldingValuationDetails[i].NatureOfUseVal);
                    cmd.Parameters.AddWithValue("@ConstructionID", MotherHoldingValuationDetails[i].ConstructionId);
                    cmd.Parameters.AddWithValue("@ConstructionValue", MotherHoldingValuationDetails[i].ConstructionVal);
                    cmd.Parameters.AddWithValue("@LandAreaKt", MotherHoldingValuationDetails[i].LandArea_KT);
                    cmd.Parameters.AddWithValue("@LandAreaCh", MotherHoldingValuationDetails[i].LandArea_CH);
                    cmd.Parameters.AddWithValue("@LandAreaSft", MotherHoldingValuationDetails[i].LandArea_SFT);
                    cmd.Parameters.AddWithValue("@BuildingAreaSft", MotherHoldingValuationDetails[i].BldgArea_SFT);
                    cmd.Parameters.AddWithValue("@PlinthSft", MotherHoldingValuationDetails[i].Plinth_SFT);
                    cmd.Parameters.AddWithValue("@Age", MotherHoldingValuationDetails[i].Age);
                    cmd.Parameters.AddWithValue("@SurchApplicableTag", MotherHoldingValuationDetails[i].SrchgTag);
                    cmd.Parameters.AddWithValue("@AnnualValue", MotherHoldingValuationDetails[i].AnnualValuation);
                    cmd.Parameters.AddWithValue("@QtrPTax", MotherHoldingValuationDetails[i].Qtr_Ptax);
                    cmd.Parameters.AddWithValue("@QtrSrchg", MotherHoldingValuationDetails[i].Qtr_Schrg);
                    cmd.Parameters.AddWithValue("@LandCost", MotherHoldingValuationDetails[i].LandCost);
                    cmd.Parameters.AddWithValue("@EduCess", MotherHoldingValuationDetails[i].Qtr_EduCess);
                    cmd.Parameters.AddWithValue("@AssmtId", assmtTransId);
                    cmd.Parameters.AddWithValue("@AssmtIdFlag", "AT");
                    cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                    cmd.Parameters.AddWithValue("@InsertedBy", userId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    _db.GetScalar<object>(cmd);
                }
            }
            catch
            {

                throw;
            }
            finally
            {

            }
        }
        public void SaveNameChange(AssmtTransactionModel ExistingHolding, List<Owner> Names, int municipalityId,long userId)
        {
            try
            {
                if (Names.Count == 0)
                {
                    throw new ArgumentException("Owner list can not be empty");
                }

                if (ExistingHolding == null)
                {
                    throw new NullReferenceException("Tergate Holding can not be null");
                }
                var tergateHoldingTemp = GetAssessmtFormData(municipalityId, ExistingHolding.WardId, ExistingHolding.LocationID, ExistingHolding.HoldingNo);
                if (tergateHoldingTemp == null)
                {
                    throw new NullReferenceException("Tergate Holding can not be null");
                }
                var description = ExistingHolding.BuildingDtl;
                ExistingHolding = tergateHoldingTemp.ActualAssmtData;
                ExistingHolding.MunicipalityID = municipalityId;
                ExistingHolding.ValTag = "N";
                ExistingHolding.ValFromTo = "T";
                ExistingHolding.ApprovedFlag = "N";
                ExistingHolding.ActiveFlag = "Y";
                ExistingHolding.ApprovedOn = null;
                ExistingHolding.InsertedBy = (int)userId;
                ExistingHolding.InsertedOn = DateTime.Now;
                ExistingHolding.UpdatedOn = null;
                ExistingHolding.UpdatedBy = null;
                ExistingHolding.RefTabId = ExistingHolding.TabId;
                ExistingHolding.AssesseeName = string.Join(",", Names.Select(t=>t.AssesseeName).ToList());
                ExistingHolding.BuildingDtl = description;

                SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn, tr, CommandType.StoredProcedure);
                
                Bindder.BindParams(cmd, ExistingHolding, userId);
                DataTable dt = _db.GetResult(cmd);
                //migrate all valuation and other data to temp of mother holding
                if (dt.Rows.Count > 0)
                {
                    var TabId = dt.Rows[0].Field<long>("TabId");
                   var cmd1 = _db.NewCommand("taxation.Assmt_migrateValuationNOtherInfoDataONTempInsert", cn, tr, CommandType.StoredProcedure);
                    cmd1.Parameters.AddWithValue("@oldAssmtId", ExistingHolding.TabId);
                    cmd1.Parameters.AddWithValue("@newTempAssmtId", TabId);
                    cmd1.Parameters.AddWithValue("@userId", userId);
                    cmd1.ExecuteNonQuery();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }
        }
        public void SaveStrikeOffData(AssmtTransactionModel tergateHolding,int municipalityId,long userId)
        {
            //SqlTransaction tr = null;
            try
            {
                

                if (tergateHolding == null)
                {
                    throw new NullReferenceException("Tergate Holding can not be null");
                }
                var tergateHoldingTemp = GetAssessmtFormData(municipalityId, tergateHolding.WardId, tergateHolding.LocationID, tergateHolding.HoldingNo);
                if (tergateHoldingTemp == null)
                {
                    throw new NullReferenceException("Tergate Holding can not be null");
                }
                var description = tergateHolding.BuildingDtl;
                tergateHolding = tergateHoldingTemp.ActualAssmtData;
                tergateHolding.MunicipalityID = municipalityId;
                tergateHolding.ValTag = "S";
                tergateHolding.ValFromTo = "F";
                tergateHolding.ApprovedFlag = "N";
                tergateHolding.ActiveFlag = "Y";
                tergateHolding.ApprovedOn = null;
                tergateHolding.InsertedBy = (int)userId;
                tergateHolding.InsertedOn = DateTime.Now;
                tergateHolding.UpdatedOn = null;
                tergateHolding.UpdatedBy = null;
                tergateHolding.RefTabId = tergateHolding.TabId;
                tergateHolding.BuildingDtl = description;

                
                
                SqlCommand cmd = _db.NewCommand("InsertMstAssmtTrans", cn,tr, CommandType.StoredProcedure);
                
                Bindder.BindParams(cmd, tergateHolding, userId);
                DataTable dt = _db.GetResult(cmd);
                if (dt.Rows.Count > 0)
                {
                    var TabId = dt.Rows[0].Field<long>("TabId");
                    cmd = _db.NewCommand("taxation.Assmt_migrateValuationNOtherInfoDataONTempInsert", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@oldAssmtId", tergateHolding.TabId);
                    cmd.Parameters.AddWithValue("@newTempAssmtId", TabId);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.ExecuteNonQuery();
                }

            }
            catch
            {
                
                throw;
                
            }
            finally
            {
                
            }
        }
        #endregion

        public List<AssessmentType> GetAssessmentTypes(string ValGroup)
        {
            try
            {
                
                SqlCommand cmd = _db.NewCommand("get_AssessmentTypes", cn, tr);
                cmd.Parameters.AddWithValue("@ValGroup", ValGroup);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = _db.GetResult(cmd);

                return dt.AsEnumerable().Select(t => new Assessment_VM.AssessmentType {
                    ValDesc=t.Field<string>("ValDesc"),
                    ValGroup=t.Field<string>("ValGroup"),
                    ValTag=t.Field<string>("ValTag")
                }).ToList();

            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                
            }
        }
        public AssessmentForm GetNewForm(int municipalityId,string AssessmentValTag, string SubAction)
        {
            var assessmnetForm = new AssessmentForm();
            assessmnetForm.AssessmentValTag = AssessmentValTag;
            assessmnetForm.SubAction = SubAction;
            assessmnetForm._Constructions = GetScoreList(3, municipalityId);
            assessmnetForm._NatureOfUse = GetScoreList(2, municipalityId);
            assessmnetForm._Zones = GetScoreList(1, municipalityId);
            assessmnetForm._HoldingTypeGroupes = GetHoldingTypeGroupes('M');
            assessmnetForm._DeedTypes = GetDeedTypes();
            assessmnetForm._AdditionalHoldingDetails = new AdditionalHoldingDetails ();
            assessmnetForm._HoldingDetails = new HoldingDetails();
            //assessmnetForm._ValuationDetails = new ValuationDetails();
            assessmnetForm._ValuationDetails = new List<ValuationDetails>();
            assessmnetForm._OtherDetails = new OtherDetails();
            assessmnetForm._OtherTabDetails = new OtherTabDetails();
            assessmnetForm._YearID = GetMaxYear(municipalityId);
            assessmnetForm._HoldingTypes = GetHoldingTypes(null,municipalityId);
            assessmnetForm.MasterData.OtherInfoTypeConfigs = GetOtherInfoTypes(new List<char>() {'O','D','M'}, new List<string>() {"REM","DOO","ARO","CMV", "SBP" }, 'A');
            return assessmnetForm;
        }
        public List<ValuationDetails> GetValuationDetailsData(int municipalityId, int wardId, int locationId, string holdingNo, bool IsTemp=true)
        {
            try
            {
                var cmd = _db.NewCommand("GetValuationDetailsData", cn, tr);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@WardID", wardId);
                cmd.Parameters.AddWithValue("@LocationID", locationId);
                cmd.Parameters.AddWithValue("@HoldingNo", holdingNo);
                cmd.Parameters.AddWithValue("@Flag", IsTemp==true?'T':'P');
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataTable dt = _db.GetResult(cmd);
                var res = dt.AsEnumerable().Select(t =>
                {
                    var valDet = new ValuationDetails();
                    valDet.AnulValDtlTabId = t.Field<long>("TabId"); ;
                    valDet.HoldingTypeId = t.Field<int>("HoldingTypeGrpId");
                    valDet.ZoneId = t.Field<string>("ScoreZoneID");
                    valDet.ZoneVal = t.Field<decimal>("ScoreZoneValue");
                    valDet.NatureOfUseId = t.Field<string>("ScoreUseID");
                    valDet.NatureOfUseVal = t.Field<decimal>("ScoreUseValue");
                    valDet.ConstructionId = t.Field<string>("ConstructionID");
                    valDet.ConstructionVal = t.Field<decimal>("ConstructionValue");
                    valDet.LandArea_KT = t.Field<decimal>("LandAreaKt");
                    valDet.LandArea_CH = t.Field<decimal>("LandAreaCh");
                    valDet.LandArea_SFT = t.Field<decimal>("LandAreaSft");
                    valDet.BldgArea_SFT = t.Field<decimal>("BuildingAreaSft");
                    valDet.Plinth_SFT = t.Field<decimal>("PlinthSft");
                    valDet.Age = t.Field<int>("Age");
                    valDet.SrchgTag = t.Field<bool>("SurchApplicableTag");
                    valDet.AnnualValuation = t.Field<decimal>("AnnualValue");
                    valDet.Qtr_Ptax = t.Field<decimal>("QtrPTax");
                    valDet.Qtr_Schrg = t.Field<decimal>("QtrSrchg");
                    valDet.LandCost = t.Field<decimal>("LandCost");
                    valDet.EduCessAmt = t.Field<decimal>("EduCess");
                    valDet.AssmtId = t.Field<long>("AssmtId");
                    return valDet;
                }).ToList();
                if (res.Count > 0)
                {
                    return res;
                }
                return null;
            }
            finally
            {
                
            }
        }
        
        public AssessmentForm GetAssessmtFormOtherTabData(int municipalityId, int wardId, int locationId, string holdingNo)
        {
            var lst = GetAssmtActiveList(null, null, null, municipalityId, false, wardId, locationId, holdingNo);
            if (lst.Count() > 0)
            {
                var obj = lst[0].ConvertToAssesmentFormOtherTabData();
                return obj;
            }
            return null;
        }
        public AssessmentForm GetAssessmtFormData(int municipalityId,int wardId,int locationId,string holdingNo)
        {
            var lst = GetAssmtActiveList(null,null, null, municipalityId,false,wardId,locationId,holdingNo);
            if (lst.Count() > 0)
            {
                var obj= lst[0].ConvertToAssesmentForm();
                obj.ActualAssmtData = lst[0];
                obj._Constructions = GetScoreList(3, municipalityId);
                obj._NatureOfUse = GetScoreList(2, municipalityId);
                obj._Zones = GetScoreList(1, municipalityId);
                obj._HoldingTypeGroupes = GetHoldingTypeGroupes('M');
                obj._DeedTypes = GetDeedTypes();
                obj._HoldingTypes = GetHoldingTypes(null,municipalityId);
                obj.MasterData.OtherInfoTypeConfigs = GetOtherInfoTypes(new List<char>() { 'O', 'D', 'M' }, new List<string>() { "REM", "DOO", "ARO", "CMV", "SBP", "INH","DCN","DOD" }, 'A');
                return obj;
            }
            return null;
        }

        public CalculatedModel CalculateValuation(ValuationDetails valuationDetails,int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("CalcAssmtAnnualValueTax", cn, tr);
                cmd.Parameters.AddWithValue("@ScoreZoneID", valuationDetails.ZoneId);
                cmd.Parameters.AddWithValue("@ScoreUseID", valuationDetails.NatureOfUseId);
                cmd.Parameters.AddWithValue("@ScoreCostID", valuationDetails.ConstructionId);
                cmd.Parameters.AddWithValue("@HoldingTypeGrpId", valuationDetails.HoldingTypeGroupId);
                cmd.Parameters.AddWithValue("@LandAreaKt", valuationDetails.LandArea_KT);
                cmd.Parameters.AddWithValue("@LandAreaCh", valuationDetails.LandArea_CH);
                cmd.Parameters.AddWithValue("@LandAreaSft", valuationDetails.LandArea_SFT);
                cmd.Parameters.AddWithValue("@CoveredArea", valuationDetails.BldgArea_SFT);
                cmd.Parameters.AddWithValue("@HoldingAge", valuationDetails.Age);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@SrchgTag", (valuationDetails.SrchgTag?"Y": "N"));
                cmd.Parameters.AddWithValue("@EduCessTag", (valuationDetails.EduCessTag ? "Y" : "N"));
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataTable dt = _db.GetResult(cmd);
                var res = dt.AsEnumerable().Select(t =>
                {
                    var valDet = new CalculatedModel();
                    valDet.AnnualValuation = t.Field<decimal>("AnnualValue");
                    valDet.Qtr_Ptax = t.Field<decimal>("Qtax");
                    valDet.LandCost = t.Field<decimal>("LandValue");

                    valDet.Waitage = t.Field<decimal>("Waitage");
                    valDet.Factor = t.Field<decimal>("Factor");
                    valDet.DisPer = t.Field<decimal>("DisPer");
                    valDet.DiscAmt = t.Field<decimal>("DiscAmt");
                    valDet.RevAnnualVal = t.Field<decimal>("RevAnnualVal");
                    valDet.PtaxPer = t.Field<decimal>("PtaxPer");
                    valDet.Ytax = t.Field<decimal>("Ytax");
                    valDet.EduCessAmt = t.Field<decimal>("EduCessAmt");
                    valDet.Qtr_Schrg = t.Field<decimal>("SrchgAmt");
                    return valDet;
                }).ToList();
                if (res.Count > 0)
                {
                    return res[0];
                }
                return null;
            }
            finally
            {
                
            }
        }
        public ValuationDetails CalculateValuation1(ValuationDetails valuationDetails, int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("CalcAssmtAnnualValueTax", cn,tr);
                cmd.Parameters.AddWithValue("@ScoreZoneID", valuationDetails.ZoneId);
                cmd.Parameters.AddWithValue("@ScoreUseID", valuationDetails.NatureOfUseId);
                cmd.Parameters.AddWithValue("@ScoreCostID", valuationDetails.ConstructionId);
                cmd.Parameters.AddWithValue("@HoldingTypeGrpId", valuationDetails.HoldingTypeGroupId);
                cmd.Parameters.AddWithValue("@LandAreaKt", valuationDetails.LandArea_KT);
                cmd.Parameters.AddWithValue("@LandAreaCh", valuationDetails.LandArea_CH);
                cmd.Parameters.AddWithValue("@LandAreaSft", valuationDetails.LandArea_SFT);
                cmd.Parameters.AddWithValue("@CoveredArea", valuationDetails.BldgArea_SFT);
                cmd.Parameters.AddWithValue("@HoldingAge", valuationDetails.Age);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@SrchgTag", (valuationDetails.SrchgTag ? "Y" : "N"));
                cmd.Parameters.AddWithValue("@EduCessTag", (valuationDetails.EduCessTag ? "Y" : "N"));
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataTable dt = _db.GetResult(cmd);
                var res = dt.AsEnumerable().Select(t =>
                {
                    var valDet = new ValuationDetails();
                    valDet.AnnualValuation = t.Field<decimal>("AnnualValue");
                    valDet.Qtr_Ptax = t.Field<decimal>("Qtax");
                    valDet.LandCost = t.Field<decimal>("LandValue");
                    valDet.Waitage = t.Field<decimal>("Waitage");
                    valDet.Factor = t.Field<decimal>("Factor");
                    valDet.DisPer = t.Field<decimal>("DisPer");
                    valDet.DiscAmt = t.Field<decimal>("DiscAmt");
                    valDet.RevAnnualVal = t.Field<decimal>("RevAnnualVal");
                    valDet.PtaxPer = t.Field<decimal>("PtaxPer");
                    valDet.Ytax = t.Field<decimal>("Ytax");
                    valDet.EduCessAmt = t.Field<decimal>("EduCessAmt");
                    valDet.Qtr_Schrg = t.Field<decimal>("SrchgAmt");
                    return valDet;
                }).ToList();
                if (res.Count > 0)
                {
                    return res[0];
                }
                return null;
            }
            finally
            {
                
            }
        }
        public List<HoldingType> GetHoldingTypes(int? groupId,int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("Get_assmt_HoldingTypes", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HoldingTypeId", groupId??DBNull.Value as object);
                cmd.Parameters.AddWithValue("@municipalityID", municipalityId);
                
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var group = new HoldingType();
                    group.HoldingTypeId = t.Field<int>("HoldingTypeId");
                    group.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                    return group;
                }).ToList();
            }
            finally
            {
                
            }
        }
        public List<AssmtTransactionModel> GetUnApprovedList(string valTag,int yearId,int municipalityId)
        {
            var lst= GetAssmtActiveList(null, valTag, yearId, municipalityId, true, null, null, null);
            if (string.Equals(valTag, "A", StringComparison.OrdinalIgnoreCase))
            {
                return lst.Where(t => t.TabId == t.MotherHoldingId).ToList();
            }
            return lst;
            
        }
        private List<AssmtTransactionModel> GetAssmtActiveList(int?tabId,string valTag, int? yearId, int municipalityId,bool isTempData,int?wardId,int?locationId,string holdingNO)
        {
            try
            {
                SqlCommand cmd = _db.NewCommand("GetMstAssmt", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TabId", tabId??DBNull.Value as object);
                cmd.Parameters.AddWithValue("@ValTag", valTag??DBNull.Value as object);
                cmd.Parameters.AddWithValue("@YearId", yearId??DBNull.Value as object);
                cmd.Parameters.AddWithValue("@ActiveFlag", 'Y');
                cmd.Parameters.AddWithValue("@WardId", wardId?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@LocationID",locationId?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@HoldingNo",holdingNO?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@AssesseeName", DBNull.Value);
                cmd.Parameters.AddWithValue("@TabFlag", isTempData?"T":DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                
                DataTable dt = _db.GetResult(cmd);
                return Converter.ConvertToAssmtTransactionModel(dt);
            }
            finally
            {
                
            }
        }
        public AssmtTransactionModel GetUnApproved_Object(long tabId,int municipalityId)
        {
            try
            {
                SqlCommand cmd = _db.NewCommand("GetMstAssmt", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TabId", tabId);
                cmd.Parameters.AddWithValue("@ValTag", DBNull.Value);
                cmd.Parameters.AddWithValue("@YearId", DBNull.Value);
                cmd.Parameters.AddWithValue("@ActiveFlag", 'Y');
                cmd.Parameters.AddWithValue("@WardId", DBNull.Value);
                cmd.Parameters.AddWithValue("@LocationID", DBNull.Value);
                cmd.Parameters.AddWithValue("@HoldingNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@AssesseeNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@AssesseeName", DBNull.Value);
                cmd.Parameters.AddWithValue("@TabFlag", "T");
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                
                DataTable dt = _db.GetResult(cmd);
                var lst= Converter.ConvertToAssmtTransactionModel(dt);
                if (lst.Count > 0)
                {
                    return lst[0];
                }
                return null;
            }
            finally
            {
                
            }
        }
        public int GetMaxYear(int municipalityId)
        {
            //Get_assmt_MaxYear
            try
            {
                
                SqlCommand cmd = _db.NewCommand("Get_assmt_MaxYear", cn, tr);
                cmd.Parameters.AddWithValue("@municipalityId",municipalityId);
                cmd.CommandType = CommandType.StoredProcedure;
                var dt=_db.GetResult(cmd);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0].Field<int>("YearId");
                }
                return 0;
            }
            finally
            {
                
            }
        }
        public  List<HoldingTypeGroup> GetHoldingTypeGroupes(char valGroup)
        {
            try
            {
                var cmd = _db.NewCommand("Get_assmt_HoldingTypeGroupes", cn, tr);
                cmd.Parameters.AddWithValue("@valGroup", valGroup);
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var group = new HoldingTypeGroup();
                    group.HoldingTypeGrpDesc = t.Field<string>("HoldingTypeGrpDesc");
                    group.HoldingTypeGrpIdFor = t.Field<string>("HoldingTypeGrpIdFor");
                    group.HoldingTypeGrpId = t.Field<int>("HoldingTypeGrpId");
                    return group;
                }).ToList();
            }
            finally
            {
                
            }
        }
        internal void SaveMutationForm(Valuation_Board.Models.Application.MutationForm data, int MuniId)
        {
            string fileName = "";
            if (data.IdProofData != null)
            {
                fileName = Path.GetFileNameWithoutExtension(data.IdProofData.FileName);
                string extension = Path.GetExtension(data.IdProofData.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                data.IdProofPath = fileName;
            }
            if (data.RorData != null)
            {
                fileName = Path.GetFileNameWithoutExtension(data.RorData.FileName);
                string extension = Path.GetExtension(data.RorData.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                data.RorPath = fileName;
            }
            if (data.DeedData != null)
            {
                fileName = Path.GetFileNameWithoutExtension(data.DeedData.FileName);
                string extension = Path.GetExtension(data.DeedData.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                data.DeedPath = fileName;
            }
            if (data.DevolvedData != null)
            {
                fileName = Path.GetFileNameWithoutExtension(data.DevolvedData.FileName);
                string extension = Path.GetExtension(data.DevolvedData.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                data.DevolvedPath = fileName;
            }
            if (data.AppliSigData != null)
            {
                fileName = Path.GetFileNameWithoutExtension(data.AppliSigData.FileName);
                string extension = Path.GetExtension(data.AppliSigData.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
                data.AppliSigPath = fileName;
            }
            try
            {
                // SqlCommand cmd = new SqlCommand("Insert_MutationForm", cn,tr);
                var cmd = _db.NewCommand("UDServices.Insert_MutationForm", cn, tr, CommandType.StoredProcedure);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MutationTypeId", data.MutationTypeId);
                cmd.Parameters.AddWithValue("@ApplicantsName", data.ApplicantsName);
                cmd.Parameters.AddWithValue("@ApplicantsFatherHusband", data.ApplicantsFatherHusband);
                cmd.Parameters.AddWithValue("@AddressNo", data.AddressNo);
                cmd.Parameters.AddWithValue("@AddressRdLn", data.AddressRdLn);
                cmd.Parameters.AddWithValue("@AddressViTn", data.AddressViTn);
                cmd.Parameters.AddWithValue("@AddressPs", data.AddressPs);
                cmd.Parameters.AddWithValue("@AddressPO", data.AddressPO);
                cmd.Parameters.AddWithValue("@AddressDt", data.AddressDt);
                cmd.Parameters.AddWithValue("@AddressPin", data.AddressPin);
                cmd.Parameters.AddWithValue("@AddressSt", data.AddressSt);
                cmd.Parameters.AddWithValue("@AddressLandMark", data.AddressLandMark);
                cmd.Parameters.AddWithValue("@CommunicationDetailsPh", data.CommunicationDetailsPh);
                cmd.Parameters.AddWithValue("@CommunicationDetailsMn", data.CommunicationDetailsMn);
                cmd.Parameters.AddWithValue("@CommunicationDetailsEmailIdn", data.CommunicationDetailsEmailIdn);
                cmd.Parameters.AddWithValue("@IdProofType", data.IdProofType);
                cmd.Parameters.AddWithValue("@IdProofPath", data.IdProofPath);
                cmd.Parameters.AddWithValue("@WardNo", data.WardNo);
                cmd.Parameters.AddWithValue("@HoldingDtlsRoad", data.HoldingDtlsRoad);
                cmd.Parameters.AddWithValue("@HoldingDtlsLoMo", data.HoldingDtlsLoMo);
                cmd.Parameters.AddWithValue("@HoldingDtlsHoldingNo", data.HoldingDtlsHoldingNo);
                cmd.Parameters.AddWithValue("@HoldingType", data.HoldingTypeId);
                cmd.Parameters.AddWithValue("@LandRecBlock", data.LandRecBlock);
                cmd.Parameters.AddWithValue("@LandRecMouza", data.LandRecMouza);
                cmd.Parameters.AddWithValue("@JlNo", data.JlNo);
                cmd.Parameters.AddWithValue("@PlotNoRS", data.PlotNoRs);
                cmd.Parameters.AddWithValue("@PlotNoLR", data.PlotNoLs);
                cmd.Parameters.AddWithValue("@KhatianNoRs", data.KhatianNoRs);
                cmd.Parameters.AddWithValue("@KhatianNoLR", data.KhatianNoLs);
                cmd.Parameters.AddWithValue("@QuanTumLandDc", data.QuanTumLandDc);
                cmd.Parameters.AddWithValue("@QuanTumLandKt", data.QuanTumLandKt);
                cmd.Parameters.AddWithValue("@LandTypeRor", data.LandTypeRor);
                cmd.Parameters.AddWithValue("@LandOwOcRor", data.LandOwOcRor);
                cmd.Parameters.AddWithValue("@RorPath", data.RorPath);
                cmd.Parameters.AddWithValue("@Assessed", data.Assessed);
                cmd.Parameters.AddWithValue("@nameTaxPayer", data.nameTaxPayer);
                cmd.Parameters.AddWithValue("@NameTrandVend", data.NameTrandVend);
                cmd.Parameters.AddWithValue("@presentOwnerName", data.presentOwnerName);
                cmd.Parameters.AddWithValue("@presentOwnerAddress", data.presentOwnerAddress);
                cmd.Parameters.AddWithValue("@lastQuaterPaymentReceipt", data.lastQuaterPaymentReceipt);
                cmd.Parameters.AddWithValue("@TrandVendAssessee", data.TrandVendAssessee);
                cmd.Parameters.AddWithValue("@TrandVendTaxPaidUpto", data.TrandVendTaxPaidUpto);
                cmd.Parameters.AddWithValue("@TrandVendNoRemarks", data.TrandVendNoRemarks);
                cmd.Parameters.AddWithValue("@DeedTypeId", data.DeedTypeId);
                cmd.Parameters.AddWithValue("@DeedTypeNo", data.DeedTypeNo);
                cmd.Parameters.AddWithValue("@DeedTypeDate", data.DeedTypeDate);
                cmd.Parameters.AddWithValue("@DeedTypeRegOff", data.DeedTypeRegOff);
                cmd.Parameters.AddWithValue("@DeedTypeVal", data.DeedTypeVal);
                cmd.Parameters.AddWithValue("@DeedPath", data.DeedPath);
                cmd.Parameters.AddWithValue("@DevolvedType", data.DevolvedType);
                cmd.Parameters.AddWithValue("@DevolvedPath", data.DevolvedPath);
                cmd.Parameters.AddWithValue("@AreaDtlsTla", data.AreaDtlsTla);
                cmd.Parameters.AddWithValue("@AreaDtlsTvla", data.AreaDtlsTvla);
                cmd.Parameters.AddWithValue("@AreaDtlsTp", data.AreaDtlsTp);
                cmd.Parameters.AddWithValue("@AreaDtlsLabf", data.AreaDtlsLabf);
                cmd.Parameters.AddWithValue("@AreaDtlsFa", data.AreaDtlsFa);
                cmd.Parameters.AddWithValue("@parkingspace", data.parkingspace);
                cmd.Parameters.AddWithValue("@exclusivegarden", data.exclusivegarden);
                cmd.Parameters.AddWithValue("@RoofTypeId", data.RoofTypeId);
                cmd.Parameters.AddWithValue("@CompleCert", data.CompleCert);
                cmd.Parameters.AddWithValue("@CompleCertNo", data.CompleCertNo);
                cmd.Parameters.AddWithValue("@CompleCertDt", data.CompleCertDt);
                cmd.Parameters.AddWithValue("@AppliSigPath", data.AppliSigPath);
                cmd.Parameters.AddWithValue("@MuniId", MuniId);
                cmd.ExecuteNonQuery();
                if (data.IdProofData != null)
                {
                    data.IdProofData.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), fileName));
                }
                if (data.RorData != null)
                {
                    data.RorData.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), fileName));
                }
                if (data.DeedData != null)
                {
                    data.DeedData.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), fileName));
                }
                if (data.DevolvedData != null)
                {
                    data.DevolvedData.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), fileName));
                }
                if (data.AppliSigData != null)
                {
                    data.AppliSigData.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), fileName));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public List<Valuation_Board.Models.Application.MutationType> GetMutationType()
        {
            try
            {
                var cmd = _db.NewCommand("UDServices.Get_MutationType", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var deedType = new Valuation_Board.Models.Application.MutationType();
                    deedType.MutationTypeName = t.Field<string>("MutationTypeName");
                    deedType.MutationTypeId = t.Field<int>("MutationTypeId");
                    return deedType;
                }).ToList();
            }
            finally
            {

            }
        }
        public List<Valuation_Board.Models.Application.IdProofType> GetIdProofType()
        {
            try
            {
                var cmd = _db.NewCommand("UDServices.Get_IdProofType", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var deedType = new Valuation_Board.Models.Application.IdProofType();
                    deedType.IdProofTypeName = t.Field<string>("IdProofTypeName");
                    deedType.IdProofTypeId = t.Field<int>("IdProofTypeId");
                    return deedType;
                }).ToList();
            }
            finally
            {

            }
        }
        public List<Valuation_Board.Models.Application.DevolvedType> GetDevolvedType()
        {
            try
            {
                var cmd = _db.NewCommand("UDServices.Get_DevolvedType", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var deedType = new Valuation_Board.Models.Application.DevolvedType();
                    deedType.DevolvedTypeName = t.Field<string>("DevolvedTypeName");
                    deedType.DevolvedTypeId = t.Field<int>("DevolvedTypeId");
                    return deedType;
                }).ToList();
            }
            finally
            {

            }
        }
        public List<Block> GetBlocks(int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("taxation.Get_BlockByAssesseeID", cn, tr);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var deedType = new Block();
                    deedType.BlockName = t.Field<string>("uni_bname");
                    deedType.BlockCode = t.Field<int>("Bcode");
                    return deedType;
                }).ToList();
            }
            finally
            {

            }
        }
        public List<Mouza> GetMouzaByBcode(int? Bcode, int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("taxation.Get_MaujaByBlockID", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlockID", Bcode ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);

                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var group = new Mouza();
                    group.MouzaCode = t.Field<int>("Moucode");
                    group.MouzaName = t.Field<string>("eng_mouname");
                    return group;
                }).ToList();
            }
            finally
            {

            }
        }

        internal List<Valuation_Board.Models.Application.MutationForm> GetMutationList(int? TabId = null)
        {
            try
            {
                var cmd = _db.NewCommand("UDServices.Get_MutationList", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TabId", TabId);
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    var deedType = new Valuation_Board.Models.Application.MutationForm();
                    deedType.TabId = t.Field<long?>("TabId");
                    deedType.MutationTypeId = t.Field<int?>("MutationTypeId");
                    deedType.MutationType = t.Field<string>("MutationType");
                    deedType.ApplicantsName = t.Field<string>("ApplicantsName");
                    deedType.ApplicantsFatherHusband = t.Field<string>("ApplicantsFatherHusband");
                    deedType.AddressNo = t.Field<string>("AddressNo");
                    deedType.AddressRdLn = t.Field<string>("AddressRdLn");
                    deedType.AddressViTn = t.Field<string>("AddressViTn");
                    deedType.AddressPs = t.Field<string>("AddressPs");
                    deedType.AddressPO = t.Field<string>("AddressPO");
                    deedType.AddressDt = t.Field<string>("AddressDt");
                    deedType.AddressPin = t.Field<int?>("AddressPin");
                    deedType.AddressSt = t.Field<string>("AddressSt");
                    deedType.AddressLandMark = t.Field<string>("AddressLandMark");
                    deedType.CommunicationDetailsPh = t.Field<string>("CommunicationDetailsPh");
                    deedType.CommunicationDetailsMn = t.Field<string>("CommunicationDetailsMn");
                    deedType.CommunicationDetailsEmailIdn = t.Field<string>("CommunicationDetailsEmailIdn");
                    deedType.IdProofType = t.Field<int?>("IdProofType");
                    deedType.IdProofPath = t.Field<string>("IdProofPath");
                    deedType.WardNo = t.Field<string>("HoldingDtlsWardNo");
                    deedType.HoldingDtlsRoad = t.Field<string>("HoldingDtlsRoad");
                    deedType.HoldingDtlsLoMo = t.Field<string>("HoldingDtlsLoMo");
                    deedType.HoldingDtlsHoldingNo = t.Field<string>("HoldingDtlsHoldingNo");
                    deedType.HoldingTypeId = t.Field<int?>("HoldingType");
                    deedType.LandRecBlock = t.Field<int?>("LandRecBlock");
                    deedType.LandRecMouza = t.Field<string>("LandRecMouza");
                    deedType.JlNo = t.Field<string>("JlNo");
                    deedType.PlotNoRs = t.Field<string>("PlotNoRs");
                    deedType.PlotNoLs = t.Field<string>("PlotNoLs");
                    deedType.KhatianNoRs = t.Field<string>("KhatianNoRs");
                    deedType.KhatianNoLs = t.Field<string>("KhatianNoLs");
                    deedType.QuanTumLandDc = t.Field<string>("QuanTumLandDc");
                    deedType.QuanTumLandKt = t.Field<string>("QuanTumLandKt");
                    deedType.LandTypeRor = t.Field<string>("LandTypeRor");
                    deedType.LandOwOcRor = t.Field<string>("LandOwOcRor");
                    deedType.RorPath = t.Field<string>("RorPath");
                    deedType.Assessed = t.Field<bool?>("Assessed");
                    deedType.nameTaxPayer = t.Field<string>("nameTaxPayer");
                    deedType.presentOwnerName = t.Field<string>("presentOwnerName");
                    deedType.presentOwnerAddress = t.Field<string>("presentOwnerAddress");
                    deedType.lastQuaterPaymentReceipt = t.Field<string>("lastQuaterPaymentReceipt");
                    deedType.NameTrandVend = t.Field<string>("NameTrandVend");
                    deedType.TrandVendAssessee = t.Field<bool?>("TrandVendAssessee");
                    deedType.TrandVendTaxPaidUpto = t.Field<string>("TrandVendTaxPaidUpto");
                    deedType.TrandVendNoRemarks = t.Field<string>("TrandVendNoRemarks");
                    deedType.DeedTypeId = t.Field<int?>("DeedTypeId");
                    deedType.DeedTypeNo = t.Field<string>("DeedTypeNo");
                    deedType.DeedTypeDate = t.Field<DateTime>("DeedTypeDate");
                    deedType.DeedTypeRegOff = t.Field<string>("DeedTypeRegOff");
                    deedType.DeedTypeVal = t.Field<decimal?>("DeedTypeVal");
                    deedType.DeedPath = t.Field<string>("DeedPath");
                    deedType.DevolvedType = t.Field<int?>("DevolvedType");
                    deedType.DevolvedPath = t.Field<string>("DevolvedPath");
                    deedType.AreaDtlsTla = t.Field<string>("AreaDtlsTla");
                    deedType.AreaDtlsTvla = t.Field<string>("AreaDtlsTvla");
                    deedType.AreaDtlsTp = t.Field<string>("AreaDtlsTp");
                    deedType.AreaDtlsLabf = t.Field<string>("AreaDtlsLabf");
                    deedType.AreaDtlsFa = t.Field<string>("AreaDtlsFabf");
                    deedType.parkingspace = t.Field<string>("AreaDtlsPg");
                    deedType.exclusivegarden = t.Field<string>("AreaDtlsEg");
                    deedType.RoofTypeId = t.Field<int?>("RoofTypeId");
                    deedType.CompleCert = t.Field<bool?>("CompleCert");
                    deedType.CompleCertNo = t.Field<string>("CompleCertNo");
                    deedType.CompleCertDt = t.Field<DateTime>("CompleCertDt");
                    deedType.ApplicationDate = t.Field<DateTime?>("ApplicationDate");
                    deedType.ApplicationNo = t.Field<string>("ApplicationNo");
                    deedType.AppliSigPath = t.Field<string>("AppliSigPath");
                    deedType.OtpVerification = t.Field<string>("OtpVerification");
                    deedType.MunicipalityId = t.Field<int?>("MunicipalityId");
                    deedType.ApplicationSource = t.Field<string>("ApplicationSource");
                    deedType.Status = t.Field<string>("Status");
                    return deedType;
                }).ToList();
            }
            finally
            {

            }
        }
        internal void UpdateStatus(int? TabId, string Status, int MuniId)
        {
            try
            {
                var cmd = _db.NewCommand("UDServices.Update_MutationFormStatus", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@TabId", TabId);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@MuniId", MuniId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public List<DeedType> GetDeedTypes()
        {
            try
            {
                var cmd = _db.NewCommand("Get_assmt_DeedTypes", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t => {
                    var deedType = new DeedType();
                    deedType.DeedTypeName = t.Field<string>("DeedTypeDesc");
                    deedType.DeedTypeId = t.Field<int>("DeedTypeId");
                    return deedType;
                }).ToList();
            }
            finally
            {
                
            }
        }
        List<Score> GetScoreList(int scorebeltTypeId,int municipalityId)
        {
            try
            {
                var cmd = _db.NewCommand("Get_assmt_scores", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@scorebeltTypeId", scorebeltTypeId);
                cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
                
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t=> {
                    var score = new Score();
                    score.ScoreId = t.Field<long>("ScoreID");
                    score.ScoreValue= t.Field<decimal>("ScoreValue");
                    score.ScoreCode= t.Field<string>("ScoreCode");
                    return score;
                }).ToList();
            }
            finally
            {
                
            }

        }
        private class Converter
        {
            public static List<AssmtTransactionModel> ConvertToAssmtTransactionModel(DataTable dt)
            {
                return dt.AsEnumerable().Select(t=> {
                    AssmtTransactionModel o = new AssmtTransactionModel();
                    o.TabId = t.Field<long >("TabId");
                    o.YearId = t.Field<int>("YearId");
                    o.Idno = t.Field<long>("Idno");
                    o.Idnosrl = t.Field<int>("Idnosrl");
                    o.WardId = t.Field<int>("WardId");
                    o.LocationID = t.Field<int>("LocationID");
                    o.HoldingNo = t.Field<string>("HoldingNo");
                    o.AssesseeNo = t.Field<string>("AssesseeNo");
                    o.AssesseeName = t.Field<string>("AssesseeName");
                    o.AssesseeAddress = t.Field<string>("AssesseeAddress");
                    o.ApplNo = t.Field<int?>("ApplNo");
                    o.ApplDt = t.Field<DateTime?>("ApplDt");
                    o.ProcessingFees = t.Field<decimal?>("ProcessingFees");
                    o.OtherFees = t.Field<decimal?>("OtherFees");
                    o.PrimaryPhNo = t.Field<string>("PrimaryPhNo");
                    o.PrimaryEmail = t.Field<string>("PrimaryEmail");
                    o.BoroughNo = t.Field<string>("BoroughNo");
                    o.NameCAC = t.Field<string>("NameCAC");
                    o.HoldingTypeGrp = t.Field<int?>("HoldingTypeGrp");
                    o.HoldingTypeGrpId = t.Field<int?>("HoldingTypeGrpId");
                    o.HoldArea = t.Field<string>("HoldArea");
                    o.ScoreZoneID = t.Field<string>("ScoreZoneID");
                    o.ScoreUseID = t.Field<string>("ScoreUseID");
                    o.ScoreCostID = t.Field<string>("ScoreCostID");
                    o.TotWt = t.Field<decimal?>("TotWt");
                    o.HoldingAge = t.Field<short?>("HoldingAge");
                    o.LandType = t.Field<string>("LandType");
                    o.VacLandAreaDc = t.Field<int?>("VacLandAreaDc");
                    o.VacLandAreaSft = t.Field<int?>("VacLandAreaSft");
                    o.LandAreaDc = t.Field<int?>("LandAreaDc");
                    o.LandAreaKt = t.Field<int?>("LandAreaKt");
                    o.LandAreaCh = t.Field<int?>("LandAreaCh");
                    o.LandAreaSft = t.Field<int?>("LandAreaSft");
                    o.CoveredArea = t.Field<int?>("CoveredArea");
                    o.PlinthArea = t.Field<int?>("PlinthArea");
                    o.LandCost = t.Field<decimal?>("LandCost");
                    o.AnnualVal = t.Field<decimal?>("AnnualVal");
                    o.DiscPer = t.Field<decimal?>("DiscPer");
                    o.DiscAmt = t.Field<decimal?>("DiscAmt");
                    o.RevAnnualVal = t.Field<decimal?>("RevAnnualVal");
                    o.PtaxPer = t.Field<decimal?>("PtaxPer");
                    o.YrProptax = t.Field<decimal?>("YrProptax");
                    o.QtrProptax = t.Field<decimal?>("QtrProptax");
                    o.PrvQtrProptax = t.Field<decimal?>("PrvQtrProptax");
                    o.SrchgTag = t.Field<string>("SrchgTag");
                    o.QtrSrchg = t.Field<decimal?>("QtrSrchg");
                    o.EduTag = t.Field<string>("EduTag");
                    o.QtrEduCess = t.Field<decimal?>("QtrEduCess");
                    o.MouzaName = t.Field<string>("MouzaName");
                    o.JlNo = t.Field<string>("JlNo");
                    o.PSName = t.Field<string>("PSName");
                    o.KhatianNoRS = t.Field<string>("KhatianNoRS");
                    o.KhatianNoLR = t.Field<string>("KhatianNoLR");
                    o.DagNoRS = t.Field<string>("DagNoRS");
                    o.DagNoLR = t.Field<string>("DagNoLR");
                    o.LandNatureROR = t.Field<string>("LandNatureROR");
                    o.DeedNo = t.Field<string>("DeedNo");
                    o.DeedDt = t.Field<DateTime?>("DeedDt");
                    o.DeedTypeId = t.Field<int?>("DeedTypeId");
                    o.WhetherAssessed = t.Field<string>("WhetherAssessed");
                    o.OldYearId = t.Field<int?>("OldYearId");
                    o.OldAnnualValue = t.Field<decimal?>("OldAnnualValue");
                    o.OldPTax = t.Field<decimal?>("OldPTax");
                    o.WhetherOwnChange = t.Field<string>("WhetherOwnChange");
                    o.OldAssesseeName = t.Field<string>("OldAssesseeName");
                    o.WhetherHoldingChange = t.Field<string>("WhetherHoldingChange");
                    o.OldHoldingType = t.Field<int?>("OldHoldingType");
                    o.AADRHolding = t.Field<string>("AADRHolding");
                    o.ApptTotAreaSft = t.Field<int?>("ApptTotAreaSft");
                    o.OthAreaSft = t.Field<int?>("OthAreaSft");
                    o.BuildingDtl = t.Field<string>("BuildingDtl");
                    o.LiftTag = t.Field<string>("LiftTag");
                    o.DranageTag = t.Field<string>("DranageTag");
                    o.ToiletsTag = t.Field<string>("ToiletsTag");
                    o.ElectricityTag = t.Field<string>("ElectricityTag");
                    o.WaterTag = t.Field<string>("WaterTag");
                    o.FerruleSize = t.Field<decimal?>("FerruleSize");
                    o.ValTag = t.Field<string>("ValTag");
                    o.ValYear = t.Field<string>("ValYear");
                    o.ValQtr = t.Field<int?>("ValQtr");
                    o.ValSrl = t.Field<int?>("ValSrl");
                    o.ValSubSrl = t.Field<byte?>("ValSubSrl");
                    o.ValFromTo = t.Field<string>("ValFromTo");
                    o.FromWardNo = t.Field<int?>("FromWardNo");
                    o.FromStreetCode = t.Field<int?>("FromStreetCode");
                    o.FromHoldingNo = t.Field<string>("FromHoldingNo");
                    o.OldWardNo = t.Field<int?>("OldWardNo");
                    o.OldStreetCode = t.Field<int?>("OldStreetCode");
                    o.OldHoldingNo = t.Field<string>("OldHoldingNo");
                    o.Mfactor = t.Field<decimal?>("Mfactor");
                    o.MacId = t.Field<string>("MacId");
                    o.ApprovedFlag = t.Field<string>("ApprovedFlag");
                    o.ApprovedOn = t.Field<DateTime?>("ApprovedOn");
                    o.ApprovedBy = t.Field<int?>("ApprovedBy");
                    o.ActiveFlag = t.Field<string>("ActiveFlag");
                    o.RefTabId = t.Field<long?>("RefTabId");
                    o.MunicipalityID = t.Field<int?>("MunicipalityID");
                    o.InsertedBy = t.Field<int?>("InsertedBy");
                    o.InsertedOn = t.Field<DateTime?>("InsertedOn");
                    o.UpdatedBy = t.Field<int?>("UpdatedBy");
                    o.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                    o.MotherHoldingId = t.Field<long?>("MotherHoldingTabId");
                    return o;
                }).ToList();
            }
        }
        public class Bindder
        {
            public static SqlCommand BindParams(SqlCommand cmd, AssmtTransactionModel obj,long userid)
            {
                cmd.Parameters.AddWithValue("@TabId",Bindder.ReplaceDbBull( obj.TabId));
                cmd.Parameters.AddWithValue("@YearId", Bindder.ReplaceDbBull(obj.YearId));
                cmd.Parameters.AddWithValue("@Idno", Bindder.ReplaceDbBull(obj.Idno));
                cmd.Parameters.AddWithValue("@Idnosrl", Bindder.ReplaceDbBull(obj.Idnosrl));
                cmd.Parameters.AddWithValue("@WardId", Bindder.ReplaceDbBull(obj.WardId));
                cmd.Parameters.AddWithValue("@LocationID", Bindder.ReplaceDbBull(obj.LocationID));
                cmd.Parameters.AddWithValue("@HoldingNo", Bindder.ReplaceDbBull(obj.HoldingNo));

                cmd.Parameters.AddWithValue("@AssesseeNo", Bindder.ReplaceDbBull(obj.AssesseeNo));
                cmd.Parameters.AddWithValue("@AssesseeName", Bindder.ReplaceDbBull(obj.AssesseeName));
                cmd.Parameters.AddWithValue("@AssesseeAddress", Bindder.ReplaceDbBull(obj.AssesseeAddress));
                cmd.Parameters.AddWithValue("@ApplNo", Bindder.ReplaceDbBull(obj.ApplNo));
                cmd.Parameters.AddWithValue("@ApplDt", Bindder.ReplaceDbBull(obj.ApplDt==null?null:obj.ApplDt.Value.ToString("yyyy'-'MM'-'dd") as object));

                cmd.Parameters.AddWithValue("@ProcessingFees", Bindder.ReplaceDbBull(obj.ProcessingFees));
                cmd.Parameters.AddWithValue("@OtherFees", Bindder.ReplaceDbBull(obj.OtherFees));
                cmd.Parameters.AddWithValue("@PrimaryPhNo", Bindder.ReplaceDbBull(obj.PrimaryPhNo));
                cmd.Parameters.AddWithValue("@PrimaryEmail", Bindder.ReplaceDbBull(obj.PrimaryEmail));
                cmd.Parameters.AddWithValue("@BoroughNo", Bindder.ReplaceDbBull(obj.BoroughNo));

                cmd.Parameters.AddWithValue("@NameCAC", Bindder.ReplaceDbBull(obj.NameCAC));
                cmd.Parameters.AddWithValue("@HoldingTypeGrp", Bindder.ReplaceDbBull(obj.HoldingTypeGrp));
                cmd.Parameters.AddWithValue("@HoldingTypeGrpId", Bindder.ReplaceDbBull(obj.HoldingTypeGrpId));
                cmd.Parameters.AddWithValue("@HoldArea", Bindder.ReplaceDbBull(obj.HoldArea));
                cmd.Parameters.AddWithValue("@ScoreZoneID", Bindder.ReplaceDbBull(obj.ScoreZoneID));
                cmd.Parameters.AddWithValue("@ScoreUseID", Bindder.ReplaceDbBull(obj.ScoreUseID));
                cmd.Parameters.AddWithValue("@ScoreCostID", Bindder.ReplaceDbBull(obj.ScoreCostID));


                cmd.Parameters.AddWithValue("@TotWt", Bindder.ReplaceDbBull(obj.TotWt));
                cmd.Parameters.AddWithValue("@HoldingAge", Bindder.ReplaceDbBull(obj.HoldingAge));
                cmd.Parameters.AddWithValue("@LandType", Bindder.ReplaceDbBull(obj.LandType));
                cmd.Parameters.AddWithValue("@VacLandAreaDc", Bindder.ReplaceDbBull(obj.VacLandAreaDc));
                cmd.Parameters.AddWithValue("@VacLandAreaSft", Bindder.ReplaceDbBull(obj.VacLandAreaSft));
                cmd.Parameters.AddWithValue("@LandAreaDc", Bindder.ReplaceDbBull(obj.LandAreaDc));

                cmd.Parameters.AddWithValue("@LandAreaKt", Bindder.ReplaceDbBull(obj.LandAreaKt));
                cmd.Parameters.AddWithValue("@LandAreaCh", Bindder.ReplaceDbBull(obj.LandAreaCh));
                cmd.Parameters.AddWithValue("@LandAreaSft", Bindder.ReplaceDbBull(obj.LandAreaSft));
                cmd.Parameters.AddWithValue("@CoveredArea", Bindder.ReplaceDbBull(obj.CoveredArea));
                cmd.Parameters.AddWithValue("@PlinthArea", Bindder.ReplaceDbBull(obj.PlinthArea));
                cmd.Parameters.AddWithValue("@LandCost", Bindder.ReplaceDbBull(obj.LandCost));

                cmd.Parameters.AddWithValue("@AnnualVal", Bindder.ReplaceDbBull(obj.AnnualVal));
                cmd.Parameters.AddWithValue("@DiscPer", Bindder.ReplaceDbBull(obj.DiscPer));
                cmd.Parameters.AddWithValue("@DiscAmt", Bindder.ReplaceDbBull(obj.DiscAmt));
                cmd.Parameters.AddWithValue("@RevAnnualVal", Bindder.ReplaceDbBull(obj.RevAnnualVal));
                cmd.Parameters.AddWithValue("@PtaxPer", Bindder.ReplaceDbBull(obj.PtaxPer));
                cmd.Parameters.AddWithValue("@YrProptax", Bindder.ReplaceDbBull(obj.YrProptax));
                cmd.Parameters.AddWithValue("@QtrProptax", Bindder.ReplaceDbBull(obj.QtrProptax));
                cmd.Parameters.AddWithValue("@PrvQtrProptax", Bindder.ReplaceDbBull(obj.PrvQtrProptax));
                cmd.Parameters.AddWithValue("@SrchgTag", Bindder.ReplaceDbBull(obj.SrchgTag));

                cmd.Parameters.AddWithValue("@QtrSrchg", Bindder.ReplaceDbBull(obj.QtrSrchg));
                cmd.Parameters.AddWithValue("@EduTag", Bindder.ReplaceDbBull(obj.EduTag));
                cmd.Parameters.AddWithValue("@QtrEduCess", Bindder.ReplaceDbBull(obj.QtrEduCess));
                cmd.Parameters.AddWithValue("@MouzaName", Bindder.ReplaceDbBull(obj.MouzaName));
                cmd.Parameters.AddWithValue("@JlNo", Bindder.ReplaceDbBull(obj.JlNo));
                cmd.Parameters.AddWithValue("@PSName", Bindder.ReplaceDbBull(obj.PSName));
                cmd.Parameters.AddWithValue("@KhatianNoRS", Bindder.ReplaceDbBull(obj.KhatianNoRS));
                cmd.Parameters.AddWithValue("@KhatianNoLR", Bindder.ReplaceDbBull(obj.KhatianNoLR));

                cmd.Parameters.AddWithValue("@DagNoRS", Bindder.ReplaceDbBull(obj.DagNoRS));
                cmd.Parameters.AddWithValue("@DagNoLR", Bindder.ReplaceDbBull(obj.DagNoLR));
                cmd.Parameters.AddWithValue("@LandNatureROR", Bindder.ReplaceDbBull(obj.LandNatureROR));
                cmd.Parameters.AddWithValue("@DeedNo", Bindder.ReplaceDbBull(obj.DeedNo));
                cmd.Parameters.AddWithValue("@DeedDt", Bindder.ReplaceDbBull(obj.DeedDt));
                cmd.Parameters.AddWithValue("@DeedTypeId", Bindder.ReplaceDbBull(obj.DeedTypeId));
                cmd.Parameters.AddWithValue("@WhetherAssessed", Bindder.ReplaceDbBull(obj.WhetherAssessed));

                cmd.Parameters.AddWithValue("@OldYearId", Bindder.ReplaceDbBull(obj.OldYearId));
                cmd.Parameters.AddWithValue("@OldAnnualValue", Bindder.ReplaceDbBull(obj.OldAnnualValue));
                cmd.Parameters.AddWithValue("@OldPTax", Bindder.ReplaceDbBull(obj.OldPTax));
                cmd.Parameters.AddWithValue("@WhetherOwnChange", Bindder.ReplaceDbBull(obj.WhetherOwnChange));
                cmd.Parameters.AddWithValue("@OldAssesseeName", Bindder.ReplaceDbBull(obj.OldAssesseeName));
                cmd.Parameters.AddWithValue("@WhetherHoldingChange", Bindder.ReplaceDbBull(obj.WhetherHoldingChange));
                cmd.Parameters.AddWithValue("@OldHoldingType", Bindder.ReplaceDbBull(obj.OldHoldingType));
                cmd.Parameters.AddWithValue("@AADRHolding", Bindder.ReplaceDbBull(obj.AADRHolding));
                cmd.Parameters.AddWithValue("@ApptTotAreaSft", Bindder.ReplaceDbBull(obj.ApptTotAreaSft));
                cmd.Parameters.AddWithValue("@OthAreaSft", Bindder.ReplaceDbBull(obj.OthAreaSft));
                cmd.Parameters.AddWithValue("@BuildingDtl", Bindder.ReplaceDbBull(obj.BuildingDtl));
                cmd.Parameters.AddWithValue("@LiftTag", Bindder.ReplaceDbBull(obj.LiftTag));
                cmd.Parameters.AddWithValue("@DranageTag", Bindder.ReplaceDbBull(obj.DranageTag));
                cmd.Parameters.AddWithValue("@ToiletsTag", Bindder.ReplaceDbBull(obj.ToiletsTag));
                cmd.Parameters.AddWithValue("@ElectricityTag", Bindder.ReplaceDbBull(obj.ElectricityTag));
                cmd.Parameters.AddWithValue("@WaterTag", Bindder.ReplaceDbBull(obj.WaterTag));
                cmd.Parameters.AddWithValue("@FerruleSize", Bindder.ReplaceDbBull(obj.FerruleSize));

                cmd.Parameters.AddWithValue("@ValTag", Bindder.ReplaceDbBull(obj.ValTag));
                cmd.Parameters.AddWithValue("@ValYear", Bindder.ReplaceDbBull(obj.ValYear));
                cmd.Parameters.AddWithValue("@ValQtr", Bindder.ReplaceDbBull(obj.ValQtr));
                cmd.Parameters.AddWithValue("@ValSrl", Bindder.ReplaceDbBull(obj.ValSrl));
                cmd.Parameters.AddWithValue("@ValSubSrl", Bindder.ReplaceDbBull(obj.ValSubSrl));
                cmd.Parameters.AddWithValue("@ValFromTo", Bindder.ReplaceDbBull(obj.ValFromTo));
                cmd.Parameters.AddWithValue("@FromWardNo", Bindder.ReplaceDbBull(obj.FromWardNo));
                cmd.Parameters.AddWithValue("@FromStreetCode", Bindder.ReplaceDbBull(obj.FromStreetCode));
                cmd.Parameters.AddWithValue("@FromHoldingNo", Bindder.ReplaceDbBull(obj.FromHoldingNo));
                cmd.Parameters.AddWithValue("@OldWardNo", Bindder.ReplaceDbBull(obj.OldWardNo));
                cmd.Parameters.AddWithValue("@OldStreetCode", Bindder.ReplaceDbBull(obj.OldStreetCode));
                cmd.Parameters.AddWithValue("@OldHoldingNo", Bindder.ReplaceDbBull(obj.OldHoldingNo));
                cmd.Parameters.AddWithValue("@Mfactor", Bindder.ReplaceDbBull(obj.Mfactor));
                cmd.Parameters.AddWithValue("@MacId", Bindder.ReplaceDbBull(obj.MacId));//put value if possible

                cmd.Parameters.AddWithValue("@ApprovedFlag", Bindder.ReplaceDbBull(obj.ApprovedFlag));
                cmd.Parameters.AddWithValue("@ApprovedOn", Bindder.ReplaceDbBull(obj.ApprovedOn));
                cmd.Parameters.AddWithValue("@ApprovedBy", Bindder.ReplaceDbBull(obj.ApprovedBy));
                cmd.Parameters.AddWithValue("@ActiveFlag", Bindder.ReplaceDbBull(obj.ActiveFlag));
                cmd.Parameters.AddWithValue("@RefTabId", Bindder.ReplaceDbBull(obj.RefTabId));
                cmd.Parameters.AddWithValue("@MunicipalityID", Bindder.ReplaceDbBull(obj.MunicipalityID));
                cmd.Parameters.AddWithValue("@InsertedBy", Bindder.ReplaceDbBull(obj.InsertedBy));

                cmd.Parameters.AddWithValue("@InsertedOn", Bindder.ReplaceDbBull(obj.InsertedOn));
                cmd.Parameters.AddWithValue("@UpdatedBy", Bindder.ReplaceDbBull(userid));
                cmd.Parameters.AddWithValue("@UpdatedOn", Bindder.ReplaceDbBull(DateTime.Now.ToString("yyyy'-'MM'-'dd HH:mm:ss")));
                return cmd;
            }
            public static object ReplaceDbBull(object val)
            {
                if((val == null))
                {
                    return DBNull.Value;
                }
                else if (val.GetType() == typeof(string))
                {
                    if(string.IsNullOrEmpty(val as string))
                    {
                        return DBNull.Value;
                    }
                }
                
                return val;

            }
        }
        
    }
}