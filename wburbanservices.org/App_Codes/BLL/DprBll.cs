﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;
using static Valuation_Board.Models.Application.DprExtension;
using static Valuation_Board.Models.Application.DprModel;
using static Valuation_Board.Models.Application.DprModel.DprDashboard;
using static Valuation_Board.Models.Application.DprModel.DprFundRelease;

namespace Valuation_Board.App_Codes.BLL
{
    public class DprBll : IDisposable
    {
        protected DbHandler _db;
        protected SqlConnection cn;
        public DprBll()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }
        public void Dispose()
        {
            cn.Close();
        }
        public List<SchemeCategory> GetFundTypeForBudget()
        {
            var cmd = _db.NewCommand("Udma.get_SchemeType", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.SchemeCategory
                {
                    Name = t.Field<string>("schemeName"),
                    Id = t.Field<int>("schemeTypeId"),
                };
            }).ToList();
        }
        public List<UlbDesig> GetDesignation()
        {
            var cmd = _db.NewCommand("Udma.get_UlbDesignation", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.UlbDesig
                {
                    DesignationName = t.Field<string>("DesigNm"),
                    DesignationId = t.Field<int>("DesigId"),
                };
            }).ToList();
        }

        public List<StageGrp> getStageGroups(int? Id = null)
        {
            var cmd = _db.NewCommand("Udma.get_StageGroups", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@Id", Id.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.StageGrp
                {
                    Value = t.Field<int>("Id"),
                    Text = t.Field<string>("Value"),
                };
            }).ToList();
        }
        public List<SubProjectType> getSubProjType(int? Id = null)
        {
            var cmd = _db.NewCommand("Udma.get_SubprojType", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@Id", Id.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.SubProjectType
                {
                    Id = t.Field<int>("Id"),
                    Desc = t.Field<string>("Desc"),
                };
            }).ToList();
        }
        public bool DelSubProjType(int Id)
        {
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                var cmd = _db.NewCommand("Udma.Del_SubprojType", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@Id", Id);
                _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch
            {
                throw new Exception("Error in Delete Sub Project Type");
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddOrEditSpt(SubProjectType data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertOrUpdateSpt", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@StgGrpId", data.StgGrpId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Id", data.Id.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Desc", data.Desc);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<BudgetCode> GetBudgetCodes()
        {
            List<BudgetCode> TypeList = new List<BudgetCode>();
            TypeList = GetBCodes(null);
            return TypeList;
        }
        public Budget GetBudgetDetails(int? Mnid, string BCode, string FinYr)
        {
            Budget data = new Budget();
            data = GetBDetails(Mnid, BCode, FinYr);
            return data;
        }
        public List<Budget> GetAllBudgets(int mnId, string finYr)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_AllBudgets", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mnId", mnId);
                cmd.Parameters.AddWithValue("@finYr", finYr);
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new Budget
                    {
                        BId= t.Field<int>("BId"),
                        BCode = t.Field<string>("BCode"),
                        MnId = t.Field<int>("MnId"),
                        MnName= GetMunicipalityNameByMnId(t.Field<int>("MnId")),
                        BDesc = t.Field<string>("BDesc"),
                        FinYr = t.Field<string>("FinYr"),
                        BAmt = t.Field<decimal>("BAmt"),
                        EffDt = t.Field<DateTime>("EffDt"),
                        FundType = t.Field<int>("FundType"),
                        Remarks = t.Field<string>("Remarks"),
                        SumBamt = t.Field<decimal>("SumBamt"),
                        SumUlbAmt = t.Field<decimal>("SumUlbAmt"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public List<UlbWiseContact> GetDesigList(int mnId)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_DesigList", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mnId", mnId);
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new UlbWiseContact
                    {
                        TabId = t.Field<int>("TabId"),
                        ContactPersonName = t.Field<string>("ContactPersonName"),
                        DesignationId = t.Field<int>("DesignationId"),
                        EmailId = t.Field<string>("EmailId"),
                        MobNo = t.Field<string>("MobNo"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public List<FundSourceMaster> GetAllFundSource()
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_AllFundSource", cn, CommandType.StoredProcedure);
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new FundSourceMaster
                    {
                        FundSource = t.Field<string>("FundSource"),
                        FundSourceID = t.Field<int?>("FundSourceID"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SchemeCategoryMaster> GetAllFundSrcAndSchmCat()
        {
            var cmd = _db.NewCommand("Udma.get_AllFundSrcAndSchemeCategories", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.SchemeCategoryMaster
                {
                    SchemeCategory = t.Field<string>("categoryName"),
                    SchemeCategoryID = t.Field<int>("CategoryId"),
                    FundSource = t.Field<string>("FundSource"),
                    FundSourceID = t.Field<int>("FundSourceId"),
                };
            }).ToList();
        }
        public string GetMunicipalityNameByMnId(int mnId)
        {
            var cmd = _db.NewCommand("Udma.get_MnNameByMnId", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MnId",mnId);
            DataTable dt = _db.GetResult(cmd);
            if (dt.Rows.Count > 0)
                return dt.Rows[0].Field<string>("MnName");
            else
                return "";
        }
        public List<WorkStatus> GetWorkStatus(int mnId, int vendorId, int workCatId, string wrkorderNo)
        {
            List<WorkStatus> WorkStatus = new List<WorkStatus>();
            WorkStatus = GetWorkStatusAll(mnId,vendorId, workCatId, wrkorderNo);
            return WorkStatus;
        }
        public List<Tender> GetAllTendersData(int mnId)
        {
            var cmd = _db.NewCommand("Udma.get_AllTendersData", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mnId", mnId);
            DataTable dt = _db.GetResult(cmd);
            var lst = dt.AsEnumerable().Select(t =>
            {
                return new Tender
                {
                    ProjectNo = t.Field<string>("ProjectNo"),
                    SubProjName= t.Field<string>("SubProjectNm"),
                    TenderNo = t.Field<string>("TenderNo"),
                    TenderCallNo = t.Field<string>("TenderCallNo"),
                    TndrOpenDt = t.Field<DateTime>("TndrOpenDt"),
                    TndrCloseDt = t.Field<DateTime>("TndrCloseDt"),
                    AwardedTo = t.Field<string>("AwardedTo"),
                    WrkOrdrNo = t.Field<string>("WrkOrdrNo"),
                    WrkOrdrDt = t.Field<DateTime?>("WrkOrdrDt"),
                    WrkStrtDt = t.Field<DateTime?>("WorkStartDt"),
                    WrkCmpltnDt = t.Field<DateTime?>("WorkCompletionDt"),
                    EngName = t.Field<string>("EngineerName"),
                    EngDesig = t.Field<string>("EngineerDesignation"),
                    EngEmail = t.Field<string>("Email"),
                    EngPhone = t.Field<string>("Mno"),
                };
            }).ToList();
            return lst;
        }
        public List<VendorDropDwn> GetVendors()
        {
            List<VendorDropDwn> TypeList = new List<VendorDropDwn>();
            var cmd = _db.NewCommand("Udma.get_Vendors", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new VendorDropDwn
                {
                    VId = t.Field<int>("VId"),
                    VName = t.Field<string>("VName"),
                };
            }).ToList();

        }
        public List<WorkTypDropDwn> GetWTypes()
        {
            List<WorkTypDropDwn> TypeList = new List<WorkTypDropDwn>();
            var cmd = _db.NewCommand("Udma.get_WorkTypes", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new WorkTypDropDwn
                {
                    WTypName = t.Field<string>("WTypName"),
                    WTypId = t.Field<int>("WTypId"),
                };
            }).ToList();

        }
        public List<WorkStatus> GetWorkStatusAll(int mnId, int vendorId, int workCatId, string wrkorderNo)
        {
            var cmd = _db.NewCommand("Udma.get_WorkStatus", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mnId", mnId);
            cmd.Parameters.AddWithValue("@vendorId", vendorId);
            cmd.Parameters.AddWithValue("@workCatId", workCatId);
            cmd.Parameters.AddWithValue("@wrkorderNo", wrkorderNo.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            var lst = dt.AsEnumerable().Select(t =>
            {
                return new WorkStatus
                {
                    TenderNo = t.Field<string>("TenderNo"),
                    VendorName = t.Field<string>("VendorName"),
                    WorkOrderNo = t.Field<string>("WorkOrderNo"),
                    WrkOrdrDt = t.Field<DateTime>("WrkOrdrDt"),
                    PaidOn = t.Field<DateTime?>("PaidOn"),
                    AllocatedWrkStrtDt = t.Field<DateTime>("AllocatedWrkStrtDt"),
                    WrkStrtDt = t.Field<DateTime?>("WrkStrtDt"),
                    WrkCmpltnDt = t.Field<DateTime>("WrkCmpltnDt"),
                    CummiAmt = t.Field<decimal>("CummiAmt"),
                    Remarks = t.Field<string>("Remarks"),
                    WMilestoneDesc= t.Field<string>("WMilestoneDesc"),
                    Percentage= t.Field<decimal?>("Percentage"),
                    ImagePath = string.IsNullOrEmpty(t.Field<string>("ImagePath")) ? null : t.Field<string>("ImagePath").BindHost(),
                };
            }).ToList();
            return lst;
        }
        public bool AddWorkStatus(WorkStatus WrkStatusData, HttpFileCollectionBase files)
        {
            try
            {
                string ImagePath1 = "";
                string ImagePath2 = "";
                string ImagePath3 = "";
                if (files != null)
                {
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[0];
                        if (i == 0)
                            ImagePath1 = file.SaveFile();
                        if (i == 1)
                            ImagePath2 = file.SaveFile();
                        if (i == 2)
                            ImagePath3 = file.SaveFile();
                    }
                }
                var hdshg = WrkStatusData.PaidOn.ToDbDate();
                var gdsh = WrkStatusData.WrkStrtDt.ToDbDate();
                var cmd = _db.NewCommand("Udma.InsertWrkStatusData", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@VId", WrkStatusData.VId);
                cmd.Parameters.AddWithValue("@WTypId", WrkStatusData.WTypId);
                cmd.Parameters.AddWithValue("@WorkOrderNo", WrkStatusData.WorkOrderNo);
                cmd.Parameters.AddWithValue("@Remarks", WrkStatusData.Remarks.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@DelayRemarks", WrkStatusData.DelayRemarks.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@CummiAmt", WrkStatusData.CummiAmt);
                cmd.Parameters.AddWithValue("@PaidOn", WrkStatusData.PaidOn.ToDbDate());
                cmd.Parameters.AddWithValue("@WrkStrtDt", WrkStatusData.WrkStrtDt.ToDbDate());
                cmd.Parameters.AddWithValue("@WMilestone", WrkStatusData.WMilestone);
                cmd.Parameters.AddWithValue("@ImagePath1", ImagePath1.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Img1AsOn", WrkStatusData.Pic2AsOn.ReplaceDbNull()); //Image1 date st into image2
                cmd.Parameters.AddWithValue("@ImagePath2", ImagePath2.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Img2AsOn", WrkStatusData.Pic2AsOn.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ImagePath3", ImagePath3.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Img3AsOn", WrkStatusData.Pic3AsOn.ReplaceDbNull());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddOrEditFundSource(FundSourceMaster data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertOrUpdateFundSource", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@FundSource", data.FundSource.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@FundSourceID", data.FundSourceID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddOrEditSchemeCat(SchemeCategoryMaster data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertOrUpdateSchemeCat", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@FundSourceID", data.FundSourceID);
                cmd.Parameters.AddWithValue("@SchemeCategory", data.SchemeCategory.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@SchemeCategoryID", data.SchemeCategoryID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddBudgetData(DprModel.Budget BudgetDetails)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertBudgetData", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@BCode", BudgetDetails.BCode.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Bdesc", BudgetDetails.BDesc.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@FinYr", BudgetDetails.FinYr.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Ulbid", BudgetDetails.MnId);
                cmd.Parameters.AddWithValue("@Bamt", BudgetDetails.BAmt);
                cmd.Parameters.AddWithValue("@EffDate", BudgetDetails.EffDt == null ? DBNull.Value : BudgetDetails.EffDt.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@FundType", BudgetDetails.FundType);
                cmd.Parameters.AddWithValue("@Remarks", BudgetDetails.Remarks);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddOrUpdateUlbContact(UlbWiseContact data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.AddOrUpdateUlbContact", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@MunicipalityID", data.MunicipalityID.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@DesignationId", data.DesignationId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@TabId", data.TabId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ContactPersonName", data.ContactPersonName);
                cmd.Parameters.AddWithValue("@EmailId", data.EmailId);
                cmd.Parameters.AddWithValue("@MobNo", data.MobNo);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddVendor(VendorData data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertVendorData", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@VendorName", data.VendorName.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VendorReg", data.VendorReg.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VendorAdd", data.VendorAdd.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VendorPAN", data.VendorPAN.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VendorGST", data.VendorGST.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@PrimaryCntctNo", data.PrimaryCntctNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@PrimaryCntctNm", data.PrimaryCntctNm.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@PrimaryEmail", data.PrimaryEmail.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@OtherCntctNo", data.OtherCntctNo.ReplaceDbNull());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<VendorData> GetAllVendors()
        {
            var cmd = _db.NewCommand("Udma.get_AllVendors", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new VendorData
                {
                    VendorName = t.Field<string>("VendorName"),
                    VendorAdd = t.Field<string>("VendorAdd"),
                    VendorReg = t.Field<string>("VendorReg"),
                    VendorPAN = t.Field<string>("VendorPAN"),
                    VendorGST = t.Field<string>("VendorGST"),
                    PrimaryCntctNo = t.Field<string>("PrimaryCntctNo"),
                    PrimaryCntctNm = t.Field<string>("PrimaryCntctNm"),
                    PrimaryEmail = t.Field<string>("PrimaryEmail"),
                    OtherCntctNo = t.Field<string>("OtherCntctNo"),
                };
            }).ToList();
        }
        public List<SurrenderInsAmt> GetAllInstAmt(SurrenderInsAmt data)
        {
            var cmd = _db.NewCommand("Udma.get_AllInstDetails", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@ProjectNo", data.ProjectNo.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new SurrenderInsAmt
                {
                    SubProjName = t.Field<string>("SubProjName"),
                    InstAmt = t.Field<decimal>("InstAmt"),
                    SurrenderAmt= t.Field<decimal?>("SurrenderAmt"),
                    InstNo = t.Field<string>("InstNo"),
                    InstallmentSubProjectTabId = t.Field<long>("InstallmentSubProjectTabId"),
                    FundReleasedTabId = t.Field<long>("FundReleasedTabId"),
                };
            }).ToList();
        }
        public bool SaveSurrenderAmt(SurrenderInsAmt data)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.SaveSurrenderAmt", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@InstallmentSubProjectTabId", data.InstallmentSubProjectTabId);
                cmd.Parameters.AddWithValue("@FundReleasedTabId", data.FundReleasedTabId);
                cmd.Parameters.AddWithValue("@SurrenderAmt", data.SurrenderAmt);
                cmd.Parameters.AddWithValue("@StageId", data.ScOrTCdoc.StageId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool SaveReClameAmt(SurrenderInsAmt data, long? userId)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.SaveReClameAmt", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@InstallmentSubProjectTabId", data.InstallmentSubProjectTabId);
                cmd.Parameters.AddWithValue("@FundReleasedTabId", data.FundReleasedTabId);
                cmd.Parameters.AddWithValue("@ReClameAmt", data.SurrenderAmt);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@StageId", data.ScOrTCdoc.StageId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<WrkMilestone> GetAllMilestone(int vendorId, string wrkorderNo)
        {
            var cmd = _db.NewCommand("Udma.get_AllMilestones", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@wrkorderNo", wrkorderNo.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@vendorId", vendorId);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new WrkMilestone
                {
                    percentage = t.Field<decimal>("Percentage"),
                    MileStoneId = t.Field<int>("WsTabId"),
                    WmDesc = t.Field<string>("WmDesc")
                };
            }).ToList();
        }
        public List<VendorDropDwn> GetVendors(string VendorName,int? mnId=null)
        {
            List<VendorDropDwn> TypeList = new List<VendorDropDwn>();
            var cmd = _db.NewCommand("Udma.get_Vendors", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@VendorName", VendorName.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MnID", mnId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new VendorDropDwn
                {
                    VId = t.Field<int>("VId"),
                    VName = t.Field<string>("VName"),
                };
            }).ToList();

        }
        public List<ProjectNoDropDwn> GetAllProjectNos(string ProjNo,int? MnId=null)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_AllProjectNos", cn, CommandType.StoredProcedure); 
                cmd.Parameters.AddWithValue("@ProjNo", ProjNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MnId", MnId.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new ProjectNoDropDwn
                    {
                        ProjectNoTxt = t.Field<string>("ProjectNo"),
                        ProjectNoVal = t.Field<string>("ProjectNo"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SubProjectNoDropDwn> GetAllSubProjectNos(string SubProjNo, string ProjNo, int? MnId = null)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_AllSubProjectNos", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@SubProjNo", SubProjNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ProjNo", ProjNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MnId", MnId.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new SubProjectNoDropDwn
                    {
                        SubProjNoVal = t.Field<long>("SubProjId"),
                        SubProjNoTxt = t.Field<string>("SubProjectNo"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public List<WrkOrdrNoDropDwn> GetAllWrkOrdr(string VendorName, string WrkOrdrNo)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.get_AllWrkOrdrNos", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@WrkOrdrNo", WrkOrdrNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VendorName", VendorName.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                return dt.AsEnumerable().Select(t =>
                {
                    return new WrkOrdrNoDropDwn
                    {
                        WrkOrdrNoTxt = t.Field<string>("WrkOrdrNo"),
                        WrkOrdrNoVal = t.Field<string>("WrkOrdrNo"),
                    };
                }).ToList();
            }
            finally
            {
                cn.Close();
            }
        }
        public bool AddTenderData(Tender TenderDetails)
        {
            try
            {
                var cmd = _db.NewCommand("Udma.InsertTenderData", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@Ulbid", TenderDetails.MnId);
                cmd.Parameters.AddWithValue("@ProjectNo", TenderDetails.ProjectNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@TenderNo", TenderDetails.TenderNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@TenderCallNo", TenderDetails.TenderCallNo);
                cmd.Parameters.AddWithValue("@TndrOpenDt", TenderDetails.TndrOpenDt /*== null ? DBNull.Value : TenderDetails.TndrOpenDt.ToString("yyyy'-'MM'-'dd") as object*/);
                cmd.Parameters.AddWithValue("@TndrCloseDt", TenderDetails.TndrCloseDt /*== null ? DBNull.Value : TenderDetails.TndrCloseDt.ToString("yyyy'-'MM'-'dd") as object*/);
                cmd.Parameters.AddWithValue("@AwardedTo", TenderDetails.AwardedTo);
                cmd.Parameters.AddWithValue("@WrkOrdrNo", TenderDetails.WrkOrdrNo);
                cmd.Parameters.AddWithValue("@WrkOrdrDt", TenderDetails.WrkOrdrDt /*== null ? DBNull.Value : TenderDetails.WrkOrdrDt.ToString("yyyy'-'MM'-'dd") as object*/);
                cmd.Parameters.AddWithValue("@WrkStrtDt", TenderDetails.WrkStrtDt /*== null ? DBNull.Value : TenderDetails.WrkStrtDt.ToString("yyyy'-'MM'-'dd") as object*/);
                cmd.Parameters.AddWithValue("@WrkCmpltnDt", TenderDetails.WrkCmpltnDt /*== null ? DBNull.Value : TenderDetails.WrkCmpltnDt.ToString("yyyy'-'MM'-'dd") as object*/);
                cmd.Parameters.AddWithValue("@EngName", TenderDetails.EngName);
                cmd.Parameters.AddWithValue("@EngEmail", TenderDetails.EngEmail.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@EngDesig", TenderDetails.EngDesig.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@EngPhone", TenderDetails.EngPhone.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@SubProj", TenderDetails.SubProjName.ReplaceDbNull());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return true;
            }
            finally
            {
                cn.Close();
            }
        }
        public bool IsWrkStatusDtGtrThnWrkCompltnDt(string WrkStatusDt, int VId, string WorkOrderNo)
        {
            try
            {
                cn.Open();
                var cmd = _db.NewCommand("Udma.IsWrkStatusDtGtrThnWrkCompltnDt", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@WrkStatusDt", WrkStatusDt.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VId", VId);
                cmd.Parameters.AddWithValue("@WorkOrderNo", WorkOrderNo.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                return dt.Rows[0].Field<bool>("result");
            }
            catch(Exception e)
            {
                throw new EvaluateException("Error in DB for date comparision");
            }
            finally
            {
                cn.Close();
            }
        }
        public List<BudgetCode> GetBCodes(int? schemeId)
        {
            var cmd = _db.NewCommand("Udma.get_BudgetCodes", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new BudgetCode
                {
                    BCode = t.Field<string>("Code"),
                    BCodeId = t.Field<string>("CodeId"),
                };
            }).ToList();
        }
        public Budget GetBDetails(int? MnId, string Bcode, string FinYr)
        {
            var cmd = _db.NewCommand("Udma.get_BudgetDetails", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityId", MnId);
            cmd.Parameters.AddWithValue("@Bcode", Bcode.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@FinYr", FinYr.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new Budget
                {
                    BCode = t.Field<string>("BCode"),
                    MnId = t.Field<int>("MnId"),
                    BDesc = t.Field<string>("BDesc"),
                    FinYr = t.Field<string>("FinYr"),
                    BAmt = t.Field<decimal>("BAmt"),
                    EffDt = t.Field<DateTime>("EffDt"),
                    FundType = t.Field<int>("FundType"),
                    Remarks = t.Field<string>("Remarks"),
                };
            }).ToList().FirstOrDefault();
        }
        public DprModel.Master GetEmptyForm(int municipalityId, long userId)
        {
            var mstr = new DprModel.Master()
            {
                Part1 = new DprModel.Part()
                ,
                Part2 = new DprModel.Part()
            };
            mstr.Part1.CheckList = GetCheckList("I");
            mstr.Part2.CheckList = GetCheckList("II");
            mstr.Part2.Notes = GetNotes("II");
            mstr.Part1.Notes = GetNotes("I");
            var municipality = GetDevAuthority(municipalityId);
            if (municipality.Equals(default(KeyValuePair<int, string>)))
            {
                throw new Exception("Dev authority not found");
            }
            mstr.MunicipalityId = municipality.Key;
            mstr.MunicipalityName = municipality.Value;
            mstr.IdentityStamp = DateTime.Now.Ticks + "" + userId;
            mstr.SchemeCategories = GetSchemeCategories(null);
            mstr.SchemeTypes = GetSchemeTypes();
            mstr.Units = GetUnits();
            return mstr;
        }
        public List<UnitMaster> GetUnits(int? unitId = null, bool isActive = true)
        {
            var cmd = _db.NewCommand("Udma.get_units", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@unitId", unitId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@isActive", isActive);
            return _db.GetResult(cmd).Convert<UnitMaster>();
        }
        public List<InstmentMaster> GetInst()
        {
            var cmd = _db.NewCommand("Udma.get_instmaster", cn, CommandType.StoredProcedure);
            return _db.GetResult(cmd).Convert<InstmentMaster>();
        }
        public List<DprModel.Dpr> GetDprList(int? municipalityId, long? userId, DateTime? dateFrom, DateTime? dateTo, string status, long? mstDprId = null)
        {
            var cmd = _db.NewCommand("Udma.Get_dprSubmittedList", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateForm", dateFrom == null ? DBNull.Value : dateFrom.Value.ToString("yyyy'-'MM'-'dd") as object);
            cmd.Parameters.AddWithValue("@dateTo", dateTo == null ? DBNull.Value : dateTo.Value.ToString("yyyy'-'MM'-'dd") as object);
            cmd.Parameters.AddWithValue("@status", status.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());

            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                var mster = new DprModel.Dpr()
                {
                    ApproChkBy = t.Field<int?>("ApproChkBy"),
                    DPChkSlId = t.Field<long>("DPChkSlId"),
                    ProjectDate = t.Field<DateTime?>("ProjectDate"),
                    ProjectNo = t.Field<string>("projectNo"),
                    DeptType = t.Field<string>("DeptType"),
                    MunicipalityDevAuthoID = t.Field<int>("MunicipalityDevAuthoID"),
                    MunicipalityDevAuthoName = t.Field<string>("MunicipalityName"),
                    NameOfWork = t.Field<string>("NameOfWork"),
                    NameOfEngineer = t.Field<string>("NameOfEngineer"),
                    ImplementingAgency = t.Field<string>("ImplementingAgency"),
                    EstimatedCost = t.Field<decimal>("EstimatedCost"),
                    Designation = t.Field<string>("Designation"),
                    ContactNumber = t.Field<string>("ContactNumber"),
                    MemoDate = t.Field<DateTime>("MemoDate"),
                    MemoNumber = t.Field<string>("MemoNumber"),
                    DPChkStatus = t.Field<string>("DPChkStatus"),
                    ApproChkOn = t.Field<DateTime?>("ApproChkOn"),
                    DocFilePath = t.Field<string>("DocFilePath"),
                    InsertedBy = t.Field<int>("InsertedBy"),
                    InsertedOn = t.Field<DateTime>("InsertedOn"),
                    UpdatedBy = t.Field<int?>("UpdatedBy"),
                    UpdatedOn = t.Field<DateTime?>("UpdatedOn"),
                    existInDprAllocation = t.Field<bool>("existInDprAllocation"),
                    isFinallAprovalPossible = t.Field<bool>("isFinallAprovalPossible"),
                    EmailID = t.Field<string>("emailId"),
                    dprProgStat = t.Field<string>("dprProgStat"),
                    TotalApprovedAmount = t.Field<decimal>("TotalApprovedAmount"),
                    TotalReleasedAmnt = t.Field<decimal>("TotalReleasedAmnt"),
                    schemeCategoryId = t.Field<int>("schemeCategoryId"),
                    schemeTypeId = t.Field<int>("schemeTypeId"),
                    HasSubProj = t.Field<bool>("HasSubProj"),
                    CheckListDocUrl = string.IsNullOrEmpty(t.Field<string>("chkListFile")) ? null : t.Field<string>("chkListFile").BindHost(),
                    AbsPgDocUrl = string.IsNullOrEmpty(t.Field<string>("abstractPgFile")) ? null : t.Field<string>("abstractPgFile").BindHost(),
                };
                return mster;
            }).ToList();
        }
        public List<int> GetMnIdForLoginDistrictByUserId(long? UserId)
        {
            var cmd = _db.NewCommand("Udma.Get_MnIdForLoginDistrictByUserId", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", UserId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return t.Field<int>("MunicipalityID");
            }).ToList();
        }
        public List<DprModel.Dpr> GetDprList(long? userId, DprMinDashPayloadStat payload)
        {
            var cmd = _db.NewCommand("Udma.Get_dprSubmittedListNew", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityIdList", string.Join(",", payload.MunicipalityIds ?? new List<int>()));
            cmd.Parameters.AddWithValue("@dateForm", payload.FromDate.ToDbDate());
            cmd.Parameters.AddWithValue("@dateTo", payload.ToDate.ToDbDate());
            cmd.Parameters.AddWithValue("@Status", payload.Status.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemeTypeId", payload.SchemeTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemeCategoryId", payload.SchemeCategoryId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dprType", payload.DprType.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MyListFlag", payload.MyList.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                var mster = new DprModel.Dpr()
                {
                    ApproChkBy = t.Field<int?>("ApproChkBy"),
                    DPChkSlId = t.Field<long>("DPChkSlId"),
                    ProjectDate = t.Field<DateTime?>("ProjectDate"),
                    ProjectNo = t.Field<string>("projectNo"),
                    DeptType = t.Field<string>("DeptType"),
                    MunicipalityDevAuthoID = t.Field<int>("MunicipalityDevAuthoID"),
                    MunicipalityDevAuthoName = t.Field<string>("MunicipalityName"),
                    NameOfWork = t.Field<string>("NameOfWork"),
                    NameOfEngineer = t.Field<string>("NameOfEngineer"),
                    ImplementingAgency = t.Field<string>("ImplementingAgency"),
                    EstimatedCost = t.Field<decimal>("EstimatedCost"),
                    Designation = t.Field<string>("Designation"),
                    ContactNumber = t.Field<string>("ContactNumber"),
                    MemoDate = t.Field<DateTime>("MemoDate"),
                    MemoNumber = t.Field<string>("MemoNumber"),
                    DPChkStatus = t.Field<string>("DPChkStatus"),
                    ApproChkOn = t.Field<DateTime?>("ApproChkOn"),
                    DocFilePath = t.Field<string>("DocFilePath"),
                    InsertedBy = t.Field<int>("InsertedBy"),
                    InsertedOn = t.Field<DateTime>("InsertedOn"),
                    InstDate = t.Field<DateTime?>("InstDate"),
                    UpdatedBy = t.Field<int?>("UpdatedBy"),
                    UpdatedOn = t.Field<DateTime?>("UpdatedOn"),
                    existInDprAllocation = t.Field<bool>("existInDprAllocation"),
                    isFinallAprovalPossible = t.Field<bool>("isFinallAprovalPossible"),
                    EmailID = t.Field<string>("emailId"),
                    dprProgStat = t.Field<string>("dprProgStat"),
                    TotalApprovedAmount = t.Field<decimal>("TotalApprovedAmount"),
                    TotalReleasedAmnt = t.Field<decimal>("TotalReleasedAmnt"),
                    CheckListDocUrl = string.IsNullOrEmpty(t.Field<string>("chkListFile")) ? null : t.Field<string>("chkListFile").BindHost(),
                    AAandFSDocUrl = string.IsNullOrEmpty(t.Field<string>("AAFSFile")) ? null : t.Field<string>("AAFSFile").BindHost()

                };
                return mster;
            }).ToList();
        }
        public DprModel.SubmittedDpr SubmitDpr(DprModel.Master form, int devAuthId, long userId)
        {
            SqlTransaction tr = null;
            try
            {
                //insert master and fetch id
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                var VettedFilePath = "";
                var AbsPgFilePath = "";
                if (form.VettedDoc != null)
                    VettedFilePath = form.VettedDoc.SaveString64File();
                if (form.AbsPgDoc == null)
                    throw new Exception("Please Upload Abstract page with Engineer's Signature");
                tr = cn.BeginTransaction();
                var cmd = _db.NewCommand("Udma.Insert_DprMasterForm", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@SchemeDate", form.SchemeDate == null ? DBNull.Value : form.SchemeDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DeptType", DBNull.Value);
                cmd.Parameters.AddWithValue("@MunicipalityDevAuthoID", devAuthId);
                cmd.Parameters.AddWithValue("@NameOfWork", form.NameOfWork.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ImplementingAgency", form.AgencyName.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@EstimatedCost", form.EstimatedCost);
                cmd.Parameters.AddWithValue("@NameOfEngineer", form.NameOfEngineer.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Designation", form.Designation.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ContactNumber", form.ContactNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoNumber", form.MemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoDate", form.MemoDate == null ? DBNull.Value : form.MemoDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DPChkStatus", "D");
                cmd.Parameters.AddWithValue("@DocFilePath", DBNull.Value);
                cmd.Parameters.AddWithValue("@SchemeTypeId", form.SchemeTypeId);
                cmd.Parameters.AddWithValue("@SchemeCategoryId", form.SchemeCategoryId);
                cmd.Parameters.AddWithValue("@EmailId", form.EmailId); 
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@HasSubProj", form.HasSubProj);
                cmd.Parameters.AddWithValue("@CEOName", form.CEOName.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@CEOEmail", form.CEOEmail.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@CEOMob", form.CEOMob.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VettedFile", VettedFilePath.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@AbsPgFile", AbsPgFilePath.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@StageId", form.AbsPgDoc==null? form.AbsPgDoc.ReplaceDbNull() : form.AbsPgDoc.StageId.ReplaceDbNull());

                DataTable dt = _db.GetResult(cmd);
                if (dt.Rows.Count <= 0)
                {
                    throw new Exception("DPR not submitted successfully");
                }
                var submitttedDpr = dt.AsEnumerable().Select(t => new DprModel.SubmittedDpr
                {
                    MstDprId = t.Field<long>("tabId")
                }).ToList()[0];
                var mstTabId = submitttedDpr.MstDprId;
                //Insert Dpr Executive Summary
                cmd = _db.NewCommand("udma.insert_dprExecutiveSummary", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@ExecutiveSummary", form.ExecutiveSummary.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                cmd.Parameters.AddWithValue("@CommencementDate", form.CommencementDate == null ? DBNull.Value : form.CommencementDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@CompletionDate", form.CompletionDate == null ? DBNull.Value : form.CompletionDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                _db.ExecuteNonQuery(cmd);
                //insert part 1 check list
                if (form.Part1 != null)
                    form.Part1.CheckList.ForEach(t =>
                    {
                        cmd = _db.NewCommand("Udma.Insert_DprDatCheckList", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                        cmd.Parameters.AddWithValue("@partId", t.Id);
                        cmd.Parameters.AddWithValue("@partVal", t.Val.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@pageNo", t.PageNo.ReplaceDbNull()); 
                        cmd.Parameters.AddWithValue("@remarks", t.Remarks.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@userId", userId);
                        _db.ExecuteNonQuery(cmd);
                    });
                //insert part 2 check list
                if (form.Part2 != null)
                    form.Part2.CheckList.ForEach(t =>
                    {
                        cmd = _db.NewCommand("Udma.Insert_DprDatCheckList", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                        cmd.Parameters.AddWithValue("@partId", t.Id);
                        cmd.Parameters.AddWithValue("@partVal", t.Val.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@pageNo", t.PageNo.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@remarks", t.Remarks.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@userId", userId);
                        _db.ExecuteNonQuery(cmd);
                    });
                if (form.PhasingOfExpenditures.Count == 0) throw new Exception("Please provide phasing of expenditures");
                form.PhasingOfExpenditures.ForEach(t =>
                {
                    t.Validate();
                    cmd = _db.NewCommand("udma.insert_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                    cmd.Parameters.AddWithValue("@finYear", t.FinYear);
                    cmd.Parameters.AddWithValue("@expenditureAmount", t.ExpenditureAmount);
                    _db.ExecuteNonQuery(cmd);
                });
                if(form.HasSubProj==true)
                {
                    if (form.SubProjects.Count == 0)
                    {
                        throw new Exception("Please enter sub project details.");
                    }
                    int i = 1;
                    long StageId = 0;
                    form.SubProjects.ForEach(t =>
                    {
                        var SubProjFilePath = "";
                        form.SubpProjFiles.ForEach(t1 =>
                        {
                            if(t.RandId==t1.RandId)
                            {
                                //if(t1.file!=null)
                                //    SubProjFilePath = t1.file.SaveString64File();
                                StageId = t1.StageId;
                            }
                        });
                        t.Validate();
                        if (StageId > 0)
                        {
                            cmd = _db.NewCommand("udma.dpr_Insert_subprojects", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                            cmd.Parameters.AddWithValue("@subProjectName", t.Name.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@estimatedCost", t.Cost);
                            cmd.Parameters.AddWithValue("@qty", t.Qty);
                            cmd.Parameters.AddWithValue("@unitid", t.UnitId);
                            cmd.Parameters.AddWithValue("@SubProjFile", SubProjFilePath.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@StageId", StageId);
                            cmd.Parameters.AddWithValue("@SubProjSlNo", i);
                            _db.ExecuteNonQuery(cmd);
                            i++;
                        }
                        else
                            throw new Exception("Please Upload Sub Project file");
                        StageId = 0;
                    });
                }
                deleteDraft(form.IdentityStamp, userId, tr);
                tr.Commit();
                return submitttedDpr;
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public long SaveFileToStage(Base64File File)
        {
            if (File != null)
            {
                var FilePath = File.SaveString64File();
                var cmd = _db.NewCommand("Udma.Insert_FileToStage", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@FilePath", FilePath.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                var StageId = dt.Rows[0].Field<long>("StageId");
                return StageId;
            }
            return 0;
        }
        public string[] SaveFileToStage(HttpPostedFileBase File)
        {
            string[] stringArray = new string[3];
            if (File != null)
            {
                var FilePath = File.SaveFile();
                var cmd = _db.NewCommand("Udma.Insert_FileToStage", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@FilePath", FilePath.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                var StageId = dt.Rows[0].Field<long>("StageId");
                stringArray[0] = Convert.ToString(StageId);
                stringArray[1] = FilePath.BindHost();
                return stringArray;
            }
            stringArray[0] = Convert.ToString(0);
            stringArray[1] = "";
            return stringArray;
        }
        public SubmittedDpr DprFinalSubmit(long mstDprID, Base64File file, int devAuthId, long userId)
        {
            var filePath = "";
            //if (file!=null)
            //    filePath = file.SaveString64File();
            if (cn.State != ConnectionState.Open)
                cn.Open();
            SqlTransaction tr;
            tr=cn.BeginTransaction();
            try
            {
                var cmd = _db.NewCommand("udma.DprFinalSubmit" ,cn,tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprID", mstDprID);
                cmd.Parameters.AddWithValue("@devAuthId", devAuthId);
                cmd.Parameters.AddWithValue("@chkListFile", filePath.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@StageId", file.StageId);
                var dt = _db.GetResult(cmd);
                var submitttedDpr = dt.AsEnumerable().Select(t => new DprModel.SubmittedDpr
                {
                    DocVerificationDate = t.Field<DateTime>("DprVerificationDate"),
                    MstDprId = t.Field<long>("tabId")
                }).ToList()[0];
                tr.Commit();
                return submitttedDpr;
            }
            catch
            {
                if(tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public void SaveAsDraft(string form, int devAuthId, long userId, string identityStamp, bool IsOldDpr=false)
        {
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            var cmd = _db.NewCommand("Udma.Save_DprDrafts", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityId", devAuthId);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@draftData", form);
            cmd.Parameters.AddWithValue("@IdentityStamp", identityStamp);
            cmd.Parameters.AddWithValue("@IsOldDpr", IsOldDpr);
            _db.ExecuteNonQuery(cmd);

        }

       

        public List<DprModel.DprDraft> GetDraftList(int? devAuthId, long? userId, string idenityStamp)
        {
            var cmd = _db.NewCommand("Udma.get_DprDrafts", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@IdentityStamp", idenityStamp.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@municipalityId", devAuthId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            var lst= dt.AsEnumerable().Select(t =>
            {
               if(t.Field<bool?>("IsOldDpr")==true)
                {
                    var mstr = JsonConvert.DeserializeObject<OldDpr>(t.Field<string>("DraftData"));
                    MunicipalityBLL mnBll = new MunicipalityBLL();
                    mstr.SchemeCategories = GetSchemeCategories(null);
                    mstr.SchemeTypes = GetSchemeTypes();
                    mstr.instlmntDrpDwn = GetInst();
                    mstr.Municipalities = mnBll.GetAllMunicipalities().Select(t1 => new KeyValue<int, string> { Key = t1.MunicipalityID ?? 0, Value = t1.MunicipalityName }).ToList();
                    mstr.IsOldDpr = true;
                    return new DprModel.DprDraft { DprMaster = mstr, DraftOn = t.Field<DateTime>("draftOn") };
                }
               else
                {
                    var mstr = JsonConvert.DeserializeObject<DprModel.Master>(t.Field<string>("DraftData"));
                    mstr.IsOldDpr = false;
                    return new DprModel.DprDraft { DprMaster = mstr, DraftOn = t.Field<DateTime>("draftOn") };
                }
            }).ToList();
            lst[0].DprMaster.Units= GetUnits();
            return lst;
        }
        public List<DprModel.SchemeType> GetSchemeTypes()
        {
            var cmd = _db.NewCommand("Udma.get_SchemeTypes", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.SchemeType
                {
                    Description = t.Field<string>("description"),
                    Id = t.Field<int>("schemeTypeId"),
                    Name = t.Field<string>("schemeName")
                };
            }).ToList();
        }
        public List<DprModel.SchemeCategory> GetSchemeCategories(int? schemeId)
        {
            var cmd = _db.NewCommand("Udma.get_SchemeCategories", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@schemeTypeId", schemeId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                return new DprModel.SchemeCategory
                {
                    Description = t.Field<string>("description"),
                    Name = t.Field<string>("categoryName"),
                    SchemeTypeId = t.Field<int>("schemTypeId"),
                    Id = t.Field<int>("categoryId"),
                };
            }).ToList();
        }
        public List<BudgetMaster> GetBudets(int ulbId,string finYear,int schemeCategoryId)
        {
            var cmd = _db.NewCommand("Udma.Get_mstBudget", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@ulbId", ulbId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@finyear", finYear.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemCategoryId", schemeCategoryId.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<BudgetMaster>();
        }
        public List<SubProject> GetSubProjects(long mstDprId)
        {
            var cmd = _db.NewCommand("Udma.get_Subprojects", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<SubProject>();
        }
        #region dpr approval related methods
       public List<AppliedBudgetHead> GetAppliedBudgets(long mstDprID)
        {
            var cmd = _db.NewCommand("udma.get_dpr_AppliedBudgetAmount", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprID);
            var data = _db.GetResult(cmd).Convert<AppliedBudgetHead>();
            return data;
        }
        public FinalApprovalExt GetAAFSData(long mstDprId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_AAFsData", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            var data = _db.GetResult(cmd).Convert<FinalApprovalExt>();
            if (data.Count >0)
                return data[0];
            return null;
        }
        public DprModel.DprApproval GetDprDetailsForApproval(long mstDprId, long userId)
        {
            var outPut = new DprModel.DprApproval();
            var dprs = GetDprList(null, null, null, null, "AL", mstDprId);
            if (dprs.Count == 0)
                throw new Exception("DPR not found");
            if (dprs[0].existInDprAllocation == false)
                throw new Exception("DPR never gone through apporval process");

            var dpr = dprs[0];
            outPut.DPChkSlId = dpr.DPChkSlId;
            outPut.ProjectNo = dpr.ProjectNo;
            outPut.ProjectDate = dpr.ProjectDate;
            outPut.DeptType = dpr.DeptType;
            outPut.MunicipalityDevAuthoID = dpr.MunicipalityDevAuthoID;
            outPut.MunicipalityDevAuthoName = dpr.MunicipalityDevAuthoName;
            outPut.NameOfWork = dpr.NameOfWork;
            outPut.ImplementingAgency = dpr.ImplementingAgency;
            outPut.EstimatedCost = dpr.EstimatedCost;
            outPut.NameOfEngineer = dpr.NameOfEngineer;
            outPut.Designation = dpr.Designation;
            outPut.ContactNumber = dpr.ContactNumber;
            outPut.MemoNumber = dpr.MemoNumber;
            outPut.MemoDate = dpr.MemoDate;
            outPut.DPChkStatus = dpr.DPChkStatus;
            outPut.ApproChkBy = dpr.ApproChkBy;
            outPut.ApproChkOn = dpr.ApproChkOn;
            outPut.DocFilePath = dpr.DocFilePath;
            outPut.InsertedBy = dpr.InsertedBy;
            outPut.InsertedOn = dpr.InsertedOn;
            outPut.UpdatedBy = dpr.UpdatedBy;
            outPut.UpdatedOn = dpr.UpdatedOn;
            outPut.EmailID = dpr.EmailID;
            outPut.DocVerificationDate = dpr.DocVerificationDate;
            outPut.existInDprAllocation = dpr.existInDprAllocation;
            outPut.dprProgStat = dpr.dprProgStat;
            outPut.BudgetHeadsApplied = GetAppliedBudgets(outPut.DPChkSlId);
            var dprOtherAttributes = GetOtherAttributesOfDpr(mstDprId, userId);
            outPut.canApprovalAmtChangble = dprOtherAttributes == null ? false : dprOtherAttributes.IsApporvalAmountChangable;
            outPut.LastApprovedAmount = dprOtherAttributes == null ? 0 : dprOtherAttributes.LastApprovedAmount;
            outPut.CanGenerateAAFS = dprOtherAttributes.CanGenerateAAFS;
            outPut.isFinallAprovalPossible = dpr.isFinallAprovalPossible;
            outPut.Hierarcheys = GetHierarchies();
            outPut.Designations = GetDesignations(userId,mstDprId);
            outPut.DprAllocationSteps = GetDprAllocationSteps(mstDprId);
            outPut.OpenDesignationId = GetOpenDesignationId(mstDprId);
            outPut.CanChangeInternalStatus = GetInternalStatusChangeBit(mstDprId);
            outPut.AaFs = GetAAFSData(mstDprId);
            if(outPut.AaFs!=null)
                outPut.AaFs.AAFSFilePath= (outPut.AaFs == null || string.IsNullOrEmpty(outPut.AaFs.AAFSFilePath)) ? null : outPut.AaFs.AAFSFilePath.BindHost();
            outPut.ApprovalActions = GetApprovalActionList(userId, outPut.LastApprovedAmount, dpr.DPChkSlId,"DA");
            outPut.HasSubProj = dpr.HasSubProj;
            outPut.SubProjects = GetSubProjects(outPut.DPChkSlId);
            outPut.SubProjects.ForEach(t => {
                t.SubProjFile = string.IsNullOrEmpty(t.SubProjFile) ? null : t.SubProjFile.BindHost();
            });
            outPut.Budgets = GetBudets(outPut.MunicipalityDevAuthoID, CommonUtils.GetFinYear(outPut.ApproChkOn), dpr.schemeTypeId);
            outPut.AbsPgDocUrl = dpr.AbsPgDocUrl;
            outPut.CheckListDocUrl = dpr.CheckListDocUrl;
            return outPut;
        }
        public void ChangeStatus(ChangeDprApproval payload,long userId, bool isGenAAFS = false)
        {
            SqlTransaction tr = null;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                tr = cn.BeginTransaction();
                if (payload.SobProjNotSelected != null)
                {
                    for (int i = 0; i < payload.SobProjNotSelected.Count; i++)
                    {
                        var cmd1 = _db.NewCommand("udma.update_DprSubProj", cn, tr, CommandType.StoredProcedure);
                        cmd1.Parameters.AddWithValue("@sbpId", payload.SobProjNotSelected[i].sbpId);
                        cmd1.Parameters.AddWithValue("@DprId", payload.SobProjNotSelected[i].DprId);
                        _db.ExecuteNonQuery(cmd1);
                    }
                }
                var cmd = _db.NewCommand("udma.update_Dpr_ChangeApprovalStatusNew", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                cmd.Parameters.AddWithValue("@targetDesignationId", payload.targetDesignationId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@status", payload.status.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@remarks", payload.remarks.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@approvedAmount", payload.approvedProjectAmount);
                var lst = _db.ExecuteNonQuery(cmd);
                if (isGenAAFS)
                {
                    var AAFSFilePath = "";
                    //if (payload.AAFSFile != null)
                    //    AAFSFilePath=payload.AAFSFile.SaveString64File();
                    cmd = _db.NewCommand("Udma.Insert_GenerateApprovalUOnoDataset", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                    cmd.Parameters.AddWithValue("@sanctionDate", payload.SanctionDate.ToDbDate());
                    cmd.Parameters.AddWithValue("@uoNo", payload.UoNo.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@uoDate", payload.UoDate.ToDbDate());
                    cmd.Parameters.AddWithValue("@accountHead", payload.AccountHead.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@sanctionNo", payload.SanctionNo.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@budgetId", payload.BudgetId);
                    cmd.Parameters.AddWithValue("@AccordedBy", payload.AccordedBy);
                    cmd.Parameters.AddWithValue("@AAFSFilePath", AAFSFilePath.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@StageId", payload.AAFSFile.StageId.ReplaceDbNull());
                    _db.GetResult(cmd);
                    payload.BudgetHeads.ForEach(t => {
                        cmd = _db.NewCommand("udma.Insert_budgetAmountToDpr", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                        cmd.Parameters.AddWithValue("@amount", t.DeductedAmount);
                        cmd.Parameters.AddWithValue("@budgetId", t.Id);
                        cmd.Parameters.AddWithValue("@userId", userId);
                        _db.GetResult(cmd);
                    });
                }
                
                
                payload.Subprojects.ForEach(t => {
                    cmd = _db.NewCommand("udma.update_subprojectAmt", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                    cmd.Parameters.AddWithValue("@cost", t.Cost);
                    cmd.Parameters.AddWithValue("@sbpId", t.Id);
                    _db.GetResult(cmd);
                });
                if(string.Equals(payload.status, "A", StringComparison.OrdinalIgnoreCase))
                {
                    //cmd = _db.NewCommand("udma.vld_budgetAmnt", cn, tr, CommandType.StoredProcedure);
                    //cmd.Parameters.AddWithValue("@budgetId", payload.BudgetId.ReplaceDbNull());
                    //cmd.Parameters.AddWithValue("@chkAmnt", payload.approvedProjectAmount.ReplaceDbNull());
                    //_db.ExecuteNonQuery(cmd);

                    cmd = _db.NewCommand("udma.update_DprDoFinallApprove", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                    cmd.Parameters.AddWithValue("@approvalDate", payload.ApprovalDate.ToDbDate());
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@approvedAmount", payload.approvedProjectAmount);
                    _db.ExecuteNonQuery(cmd);
                }
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public void SaveInternalMessage(InterMessage payload, long userId)
        {
            SqlTransaction tr = null;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                tr = cn.BeginTransaction();

                var cmd = _db.NewCommand("udma.Save_InternalStatus", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", payload.mstDprId);
                cmd.Parameters.AddWithValue("@Message", payload.InternalMessage.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@userId", userId);
                var lst = _db.ExecuteNonQuery(cmd);
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public DprModel.DprOtherAttributes GetOtherAttributesOfDpr(long mstDprId, long userId)
        {
            var cmd = _db.NewCommand("udma.get_dpr_dprOtherAttributes", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            cmd.Parameters.AddWithValue("@userId", userId);
            DataTable dt = _db.GetResult(cmd);
            var lst = dt.AsEnumerable()
                .Select(t =>
                {
                    var v = new DprModel.DprOtherAttributes()
                    {
                        IsApporvalAmountChangable = t.Field<bool>("canApprovalAmtChangble"),
                        LastApprovedAmount = t.Field<decimal>("lastApprovedAmount"),
                        CanGenerateAAFS = t.Field<bool>("CanGenerateAAFS")
                    };
                    return v;
                }).ToList();
            if (lst.Count > 0)
            {
                return lst[0];
            }
            else
            {
                return null;
            }
        }
        private List<DprModel.DprAllocationStep> GetDprAllocationSteps(long mstDprId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_DprAllocationSteps", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            return _db.GetResult(cmd).AsEnumerable().Select(t =>
            {
                var allocationStep = new DprModel.DprAllocationStep();
                allocationStep.AssignedById = t.Field<long?>("AssignedById");
                allocationStep.AssignedByName = t.Field<string>("AssignedByName");
                allocationStep.AssignedToId = t.Field<long?>("AssignedToId");
                allocationStep.AssignedOn = t.Field<DateTime?>("AssignedOn");
                allocationStep.AssignedToName = t.Field<string>("AssignedToName");
                allocationStep.TDesignationId = t.Field<int?>("TDesignationId");
                allocationStep.TDesignationName = t.Field<string>("TDesignationName");
                allocationStep.HiarcheyId = t.Field<int>("HiarcheyId");
                allocationStep.Id = t.Field<long>("Id");
                allocationStep.Remarks = t.Field<string>("Remarks");
                allocationStep.Status = t.Field<string>("Status");
                allocationStep.RepliedOn = t.Field<DateTime?>("RepliedOn");
                allocationStep.AprovedProjectAmount = t.Field<decimal>("AprovedProjectAmount");
                return allocationStep;
            }).ToList();
        }
        private long GetOpenDesignationId(long mstDprId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_DprAllocationCurrDesig", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            DataTable dt = _db.GetResult(cmd);
            return dt.Rows[0].Field<long>("DesignationId");
        }
        public long GetOpenDesignationIdForFundRelease(long fundReleaseID)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_DprAllocationCurrDesigForFundRelease", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@fundReleaseID", fundReleaseID);
            DataTable dt = _db.GetResult(cmd);
            return dt.Rows[0].Field<long>("DesignationId");
        }
        private bool GetInternalStatusChangeBit(long mstDprId, long? FundreleaseId=null)
        {
            var cmd = _db.NewCommand("udma.Get_InternalStatusChangeBit", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            cmd.Parameters.AddWithValue("@FundreleaseId", FundreleaseId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.Rows[0].Field<bool>("CanChangeBit");
        }
        private List<DprModel.Designation> GetDesignations()
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_Designations", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable()
                .Select(t =>
                {
                    var designation = new DprModel.Designation()
                    {
                        Id = t.Field<int>("Id"),
                        Name = t.Field<string>("Name"),
                        HierarcheyId = t.Field<int>("HierarcheyId")
                    };
                    return designation;
                }).ToList();
        }
        
        private List<DprModel.Designation> GetDesignationsInstallment(long userId, long fundreleaseId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_availableDesignationsInstallment", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@fundreleaseId", fundreleaseId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable()
                .Select(t =>
                {
                    var designation = new DprModel.Designation()
                    {
                        Id = t.Field<int>("Id"),
                        Name = t.Field<string>("Name"),
                        HierarcheyId = t.Field<int>("HierarcheyId")
                    };
                    return designation;
                }).ToList();
        }
        /// <summary>
        /// get available designations
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private List<DprModel.Designation> GetDesignations(long userId,long mstDprId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_availableDesignations", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable()
                .Select(t =>
                {
                    var designation = new DprModel.Designation()
                    {
                        Id = t.Field<int>("Id"),
                        Name = t.Field<string>("Name"),
                        HierarcheyId = t.Field<int>("HierarcheyId")
                    };
                    return designation;
                }).ToList();
        }

        public long appNewInstallment(DprModel.DprFundRelease.FundRelease fundRelease, long userId, int municipalityId)
        {
            if (fundRelease == null)
                throw new Exception("Invalid data");
            if (fundRelease.InstallmentAmount <= 0) throw new Exception("Installment amount can not be 0 or less");
            SqlTransaction tr = null;

            try
            {
                if (cn.State != ConnectionState.Open)
                    cn.Open();
                tr = cn.BeginTransaction();
                var LetterOrUcFilepath = "";
                var NewsPaprAbsFilePath = "";
                //if (fundRelease.LetterOrUc !=null)
                //    LetterOrUcFilepath=fundRelease.LetterOrUc.SaveString64File();
                if (fundRelease.NewsPaprAbs != null)
                    NewsPaprAbsFilePath = fundRelease.NewsPaprAbs.SaveString64File();
                var mstCmd = _db.NewCommand("udma.Insert_DprNewInstallment", cn, tr, CommandType.StoredProcedure);
                mstCmd.Parameters.AddWithValue("@mstDprId", fundRelease.MstDprId);
                mstCmd.Parameters.AddWithValue("@installmentAmount", fundRelease.InstallmentAmount);
                mstCmd.Parameters.AddWithValue("@memoNo", fundRelease.MemoNo.ReplaceDbNull());
                mstCmd.Parameters.AddWithValue("@memoDate", fundRelease.MemoDate.ToDbDate());
                mstCmd.Parameters.AddWithValue("@installmentId", fundRelease.InstallmentId);
                mstCmd.Parameters.AddWithValue("@projetStartDate", fundRelease.StartDate.ToDbDate());
                mstCmd.Parameters.AddWithValue("@projectEndDate", fundRelease.EndDate.ToDbDate());
                mstCmd.Parameters.AddWithValue("@userId", userId);
                mstCmd.Parameters.AddWithValue("@municipalityId", municipalityId);
                mstCmd.Parameters.AddWithValue("@LetterOrUcFilepath", LetterOrUcFilepath.ReplaceDbNull());
                mstCmd.Parameters.AddWithValue("@NewsPaprAbsFilepath", NewsPaprAbsFilePath.ReplaceDbNull());
                mstCmd.Parameters.AddWithValue("@StageId", fundRelease.LetterOrUc.StageId);

                var mstFundRlsId = _db.GetScalar<long>(mstCmd);
                fundRelease.ApplyCheckList.ForEach(t =>
                {
                    var filepath = "";
                    long StageId = 0;
                    fundRelease.ApplyCheckListFiles.ForEach(t1 => {
                        if(t1.id == t.ChkId)
                        {
                            //if(t1.file!=null)
                            //{
                            //    //filepath= t1.file.SaveString64File();
                            //}
                            StageId = t1.StageId;
                        }
                    });
                    var cmd = _db.NewCommand("udma.Insert_DprApplyNewInsallCheckList", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@fundReleaseId", mstFundRlsId);
                    cmd.Parameters.AddWithValue("@chkId", t.ChkId);
                    cmd.Parameters.AddWithValue("@IsChecked", t.IsChecked);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@filepath", filepath.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@StageId", StageId);
                    _db.ExecuteNonQuery(cmd);
                });
                fundRelease.SubProjects.ForEach(t => {
                    var cmd = _db.NewCommand("udma.Insert_SubProjectOnFundRelease", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", fundRelease.MstDprId);
                    cmd.Parameters.AddWithValue("@sbpid", t.Id); 
                    cmd.Parameters.AddWithValue("@InsAmt", t.InstAmt);
                    cmd.Parameters.AddWithValue("@instlmntId", mstFundRlsId);
                    _db.GetResult(cmd);
                });
                fundRelease.SubpProjFiles.ForEach(t =>
                {
                    if(t.file !=null)
                    {
                        var filePath = t.file.SaveString64File();
                        var cmd = _db.NewCommand("udma.Insert_SubProjectFiles", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@mstDprId", fundRelease.MstDprId);
                        cmd.Parameters.AddWithValue("@sbpid", t.id);
                        cmd.Parameters.AddWithValue("@filePath", filePath.ReplaceDbNull());
                        _db.GetResult(cmd);
                    }
                });

                tr.Commit();
                return mstFundRlsId;
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
            finally
            {
                cn.Close();
            }


        }
        private List<DprModel.Hierarchey> GetHierarchies()
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_Hierarchies", cn, CommandType.StoredProcedure);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable()
                .Select(t =>
                {
                    var designation = new DprModel.Hierarchey()
                    {
                        Id = t.Field<int>("Id"),
                        Name = t.Field<string>("Name"),
                    };
                    return designation;
                }).ToList();
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="amount"></param>
        /// <param name="activityId">DprId or FundreleaseId</param>
        /// <param name="activityCode">DA for Dpr Approval or FR Dpr fund release</param>
        /// <returns></returns>
        public List<ApprovalAction> GetApprovalActionList(long userId,decimal? amount,long activityId,string activityCode)
        {
            var cmd = _db.NewCommand("udma.get_ApprovalAvailableActionPermissions", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId",userId);
            cmd.Parameters.AddWithValue("@amount",amount.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@activityId", activityId); 
            cmd.Parameters.AddWithValue("@ActivityCode", activityCode);
            return _db.GetResult(cmd).Convert<ApprovalAction>();
        }

        #region Eklas, Dated: 27/10/2018. Work: Dpr approve or return
        public DprModel.Master GetParticularListByDPChkSlId(long DPChkSlId)
        {
            var mstr = new DprModel.Master()
            {
                Part1 = new DprModel.Part(),
                Part2 = new DprModel.Part()
            };
            try
            {
                var cmd = _db.NewCommand("Udma.Get_dprChecklistByDPChkSlId", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@DPChkSlId", DPChkSlId);
                cmd.Parameters.AddWithValue("@PartNo", 'I');
                DataTable dt = _db.GetResult(cmd);
                mstr.Part1.CheckList = dt.AsEnumerable().Select(t =>
                {
                    DprModel.Check2 c = new DprModel.Check2();
                    c.Id = t.Field<int>("PartId");
                    c.PartSlNo = t.Field<int>("PartSlNo");
                    c.Perticular = t.Field<string>("Particulars");
                    c.Val = t.Field<string>("PartVal");
                    c.PageNo = t.Field<string>("PageNo");
                    return c;
                }).ToList();

                cmd = _db.NewCommand("Udma.Get_dprChecklistByDPChkSlId", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@DPChkSlId", DPChkSlId);
                cmd.Parameters.AddWithValue("@PartNo", "II");
                dt = _db.GetResult(cmd);
                mstr.Part2.CheckList = dt.AsEnumerable().Select(t =>
                {
                    DprModel.Check2 c = new DprModel.Check2();
                    c.Id = t.Field<int>("PartId");
                    c.PartSlNo = t.Field<int>("PartSlNo");
                    c.Perticular = t.Field<string>("Particulars");
                    c.Val = t.Field<string>("PartVal");
                    c.PageNo = t.Field<string>("PageNo");
                    return c;
                }).ToList();

                cmd = _db.NewCommand("Udma.Get_dprMstDataByDPChkSlId", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@DPChkSlId", DPChkSlId);
                dt = _db.GetResult(cmd);
                if (dt.Rows.Count > 0)
                {
                    mstr.MunicipalityName = dt.Rows[0].Field<string>("MunicipalityName");
                    mstr.NameOfEngineer = dt.Rows[0].Field<string>("NameOfEngineer");
                    mstr.Designation = dt.Rows[0].Field<string>("Designation");
                    mstr.AgencyName = dt.Rows[0].Field<string>("ImplementingAgency");
                    mstr.NameOfWork = dt.Rows[0].Field<string>("NameOfWork");
                    mstr.MemoNo = dt.Rows[0].Field<string>("MemoNumber");
                    mstr.MemoDate = dt.Rows[0].Field<DateTime>("MemoDate");
                    mstr.EstimatedCost = dt.Rows[0].Field<decimal>("EstimatedCost");                   
                    mstr.CheckListDocUrl = string.IsNullOrEmpty(dt.Rows[0].Field<string>("chkListFile")) ? null : dt.Rows[0].Field<string>("chkListFile").BindHost();
                    mstr.AbsPgDocUrl = string.IsNullOrEmpty(dt.Rows[0].Field<string>("chkListFile")) ? null : dt.Rows[0].Field<string>("abstractPgFile").BindHost();
                }
                cmd = _db.NewCommand("Udma.get_dpr_SubPorjects", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", DPChkSlId);
                dt = _db.GetResult(cmd);
                mstr.SubProjects = dt.AsEnumerable().Select(t =>
                {
                    DprModel.SubProject c = new DprModel.SubProject();
                    c.Name = t.Field<string>("Name");
                    c.Cost = t.Field<decimal>("Cost");
                    c.Qty = t.Field<decimal>("Qty");
                    c.SubProjFile = string.IsNullOrEmpty(t.Field<string>("SubProjFile")) ? null : t.Field<string>("SubProjFile").BindHost();
                    return c;
                }).ToList();
                return mstr;
            }
            catch
            {
                throw;
            }
        }
        public void verifyDpr(DprModel.Master form, long userId)
        {
            SqlTransaction tr = null;
            try
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                tr = cn.BeginTransaction();
                form.Part1.CheckList.ForEach(t =>
                {
                    var cmd = _db.NewCommand("Udma.Update_DprChecklist", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@DPChkSlId", form.DPChkSlId);
                    cmd.Parameters.AddWithValue("@partId", t.Id);
                    cmd.Parameters.AddWithValue("@approve", t.Approve);
                    cmd.Parameters.AddWithValue("@remarks", t.Remarks.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@partVal", t.Val.ReplaceDbNull());
                    _db.ExecuteNonQuery(cmd);
                });
                var cm = _db.NewCommand("Udma.dpr_requestApprovalByDocVerify", cn, tr, CommandType.StoredProcedure);
                cm.Parameters.AddWithValue("@mstDprId", form.DPChkSlId);
                cm.Parameters.AddWithValue("@userId", userId);
                _db.ExecuteNonQuery(cm);
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public List<DprModel.Dpr> GetFilteredDprList(long? userId, DateTime? dateFrom, DateTime? dateTo, string memoNo)
        {
            var cmd = _db.NewCommand("Udma.Get_FilteredDprSubmitted", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateForm", dateFrom == null ? DBNull.Value : dateFrom.Value.ToString("yyyy'-'MM'-'dd") as object);
            cmd.Parameters.AddWithValue("@dateTo", dateTo == null ? DBNull.Value : dateTo.Value.ToString("yyyy'-'MM'-'dd") as object);
            cmd.Parameters.AddWithValue("@memoNo", (memoNo == null || memoNo == "") ? DBNull.Value : memoNo as object);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                var mster = new DprModel.Dpr()
                {
                    DPChkSlId = t.Field<long>("DPChkSlId"),
                    MunicipalityDevAuthoID = t.Field<int>("MunicipalityDevAuthoID"),
                    MunicipalityDevAuthoName = t.Field<string>("MunicipalityName"),
                    NameOfWork = t.Field<string>("NameOfWork"),
                    EstimatedCost = t.Field<decimal>("EstimatedCost"),
                    MemoDate = t.Field<DateTime>("MemoDate"),
                    MemoNumber = t.Field<string>("MemoNumber"),
                    DocVerificationDate = t.Field<DateTime?>("docVerificationDate"),
                };
                return mster;
            }).ToList();
        }

        public int updateVieificationDateByNewDate(long mstDprId, DateTime? uvDate, long? userId)
        {
            try
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                var cmd = _db.NewCommand("Udma.UpdateVerificationDate", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@userId", userId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
                cmd.Parameters.AddWithValue("@uvDate", uvDate);
                return _db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region helper methods
        void deleteDraft(string idenitityStamp, long userId, SqlTransaction tr)
        {
            SqlCommand cmd = null;
            if (tr != null)
            {
                cmd = _db.NewCommand("Udma.Delete_DprDraft", cn, tr, CommandType.StoredProcedure);
            }
            else
            {
                cmd = _db.NewCommand("Udma.Delete_DprDraft", cn, CommandType.StoredProcedure);
            }
            cmd.Parameters.AddWithValue("@IdentityStamp", idenitityStamp);
            cmd.Parameters.AddWithValue("@userId", userId);
            _db.ExecuteNonQuery(cmd);
        }
        List<DprModel.Check2> GetCheckList(string partNo)
        {
            partNo = partNo.ToUpper();
            if (!new List<string> { "I", "II" }.Contains(partNo))
                throw new Exception(string.Format("Invalid part no", partNo));

            var cmd = _db.NewCommand("Udma.Get_dprChecklist", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@partNo", partNo.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                DprModel.Check2 c = new DprModel.Check2();
                c.Id = t.Field<int>("PartId");
                c.Perticular = t.Field<string>("Particulars");
                c.DefaultValue = t.Field<string>("defVal");
                c.IsReadOnly = t.Field<bool>("isValReadOnly");
                c.IsMandatory = t.Field<bool?>("IsMandatory");
                c.IsNAconsiderable = t.Field<bool?>("IsNAconsiderable");
                return c;
            }).ToList();
        }
        List<DprModel.Note> GetNotes(string partNo)
        {
            partNo = partNo.ToUpper();
            if (!new List<string> { "I", "II" }.Contains(partNo))
                throw new Exception(string.Format("Invalid part no", partNo));

            var cmd = _db.NewCommand("Udma.Get_dprNotes", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@partNo", partNo.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                DprModel.Note c = new DprModel.Note();
                c.Description = t.Field<string>("NoteDesc");
                return c;
            }).ToList();
        }
        KeyValuePair<int, string> GetDevAuthority(int id)
        {
            var cmd = _db.NewCommand("Get_MunicipalityByIDType", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MunicipalityID", id);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
             {
                 return new KeyValuePair<int, string>(t.Field<int>("MunicipalityId"), t.Field<string>("MunicipalityName"));
             }).FirstOrDefault();
        }
        #endregion


        #region Fund release
        public List<SubProjectInstallment> get_InstallmntSubprojects(long mstDpId,long?installmentId)
        {
            var cmd = _db.NewCommand("Udma.get_InstallmntSubprojects", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", mstDpId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@InstlmntId", installmentId.ReplaceDbNull());
            return _db.GetResult(cmd).Convert< SubProjectInstallment>();
        }
        public DprModel.DprFundRelease.InstallmentMaster GetRequestForNewInstallMent(long mstDprID, long installmentId)
        {
            //check installment applied => throw exception
            var appliedInstallments = getAlreadyAppliedInstallments(mstDprID);
            if (appliedInstallments.Count > 0)
            {
                //check last installment approved
                var appInstallments = appliedInstallments.OrderByDescending(t => t.InstallmentMaster.Rank);
                var lastInstallment = appInstallments.FirstOrDefault();
                if ((lastInstallment.Status == "A" || lastInstallment.Status == "R" || lastInstallment.Status == "N") == false)
                {
                    throw new Exception(string.Format("Request installment can not be processed! An previous installment named- [{0}] still in 'pending approval'", lastInstallment.InstallmentMaster.Name, lastInstallment.StatusDscr));
                }
                else if (appInstallments.Where(t => new List<string>() { "R", "N" }.Contains(t.Status) == false && t.InstallmentId == installmentId).Count() > 0)
                {
                    throw new Exception("Requested installment already applied");
                }
            }

            //get installment master
            var installMentMasters = getInstallmentMasters(installmentId);
            var instMstr = installMentMasters.Count > 0 ? installMentMasters[0] : throw new Exception("Invalid installment requested");

            //get related check list master and convert to applycheck
            instMstr.CheckList = getInstallmentCheckListMaster(instMstr.Id);
            instMstr.SubProjects = get_InstallmntSubprojects(mstDprID, null);
            return instMstr;
        }
        
        public InitiationModel getDprForFundRelease(long mstDprId, int? municipalityId = null)
        {
            try
            {
                //those dpr with 'dprstatus=>A'

                //get dpr summary for fund release
                //get isntallment master
                //get all installment check list master
                //installments already applied
                var cmd = _db.NewCommand("udma.get_dprMasterForFundRelease", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
                cmd.Parameters.AddWithValue("@municipalityId", municipalityId.ReplaceDbNull());
                var rlsts = _db.GetResult(cmd).Convert<DprModel.DprFundRelease.InitiationModel>();
                var outPut = rlsts.Count > 0 ? rlsts[0] : throw new Exception("Tergate DPR not found or not approved");

                outPut.InstallmentMasters = getInstallmentMasters();
                outPut.AppliedInstallments = getAlreadyAppliedInstallments(mstDprId);
                return outPut;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        
        public List<DprModel.DprFundRelease.InstallmentMaster> getInstallmentMasters(long? id = null, long? FrId=null)
        {
            try
            {
                var cmd = _db.NewCommand("udma.get_dprInstallmentMaster", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@Id", id.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@FrId", FrId.ReplaceDbNull());
                return _db.GetResult(cmd).Convert<DprModel.DprFundRelease.InstallmentMaster>();
            }
            catch { throw; }
            finally { cn.Close(); }
        }
        public List<DprModel.DprFundRelease.InstallmentCheckListMasterObject> getInstallmentCheckListMaster(int? installmentId = null, int? chkListId = null)
        {
            try
            {
                var cmd = _db.NewCommand("udma.get_dprInstallmentChecklistMaster", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@installmentId", installmentId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@chklstId", chkListId.ReplaceDbNull());
                return _db.GetResult(cmd).Convert<DprModel.DprFundRelease.InstallmentCheckListMasterObject>();
            }
            catch { throw; }
            finally { cn.Close(); }
        }
        /// <summary>
        /// Fund release history
        /// </summary>
        /// <param name="mstDprId"></param>
        /// <returns></returns>
        public List<DprModel.DprFundRelease.FundReleaseAplied> getAlreadyAppliedInstallments(long mstDprId, long? fundReleaseId = null, int? installmentId = null)
        {
            try
            {
                var cmd = _db.NewCommand("udma.get_DprAlreadyAppliedInstallments", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@installmentId", installmentId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<DprModel.DprFundRelease.FundReleaseAplied>();
                rslt.ForEach(t =>
                {
                    t.NewsPaprAbsDocUrl = string.IsNullOrEmpty(t.NewsPaprAbsDocUrl) ? null : t.NewsPaprAbsDocUrl.BindHost();
                    t.LetterOrUcDocUrl = string.IsNullOrEmpty(t.LetterOrUcDocUrl) ? null : t.LetterOrUcDocUrl.BindHost();
                    t.ReleaseFilePath = string.IsNullOrEmpty(t.ReleaseFilePath) ? null : t.ReleaseFilePath.BindHost();
                    t.ApplyCheckList = getAppliedCheckListOnInstallment(mstDprId, fundReleaseId: t.FundReleaseId);
                    t.ApplyCheckList.ForEach(t1 => {
                        t1.ApplyCheckListDocUrl = string.IsNullOrEmpty(t1.ApplyCheckListDocUrl) ? null : t1.ApplyCheckListDocUrl.BindHost();
                    });
                    var instMasters = getInstallmentMasters(t.InstallmentId,t.FundReleaseId);
                    t.InstallmentMaster = instMasters.Count > 0 ? instMasters[0] : null;
                    t.SubProjects = get_InstallmntSubprojects(mstDprId, t.FundReleaseId);
                    t.SubProjects.ForEach(t2 =>
                    {
                        t2.SubpProjFilesDocUrl = string.IsNullOrEmpty(t2.SubpProjFilesDocUrl) ? null : t2.SubpProjFilesDocUrl.BindHost();
                    });
                });
                return rslt;
            }
            catch { throw; }
            finally { cn.Close(); }
        }
        public List<T> getAlreadyAppliedInstallments<T>(long mstDprId, long? fundReleaseId = null, int? installmentId = null) where T: FundReleaseAplied
        {
            try
            {
                var cmd = _db.NewCommand("udma.get_DprAlreadyAppliedInstallments", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@installmentId", installmentId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<T>();
                rslt.ForEach(t =>
                {
                    t.ApplyCheckList = getAppliedCheckListOnInstallment(mstDprId, fundReleaseId: t.FundReleaseId);
                    var instMasters = getInstallmentMasters(t.InstallmentId);
                    t.InstallmentMaster = instMasters.Count > 0 ? instMasters[0] : null;
                });
                return rslt;
            }
            catch { throw; }
            finally { cn.Close(); }
        }

        public List<ApplyCheck> getAppliedCheckListOnInstallment(long? mstDprId, long? ChkId = null, long? fundReleaseId = null, int? installmentId = null)
        {
            try
            {
                var cmd = _db.NewCommand("udma.dpr_getAppliedInstChecklist", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstDprId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ChkId", ChkId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@installmentId", installmentId.ReplaceDbNull());
                return _db.GetResult(cmd).Convert<DprModel.DprFundRelease.ApplyCheck>();
            }
            catch { throw; }
            finally { cn.Close(); }
        }
        /// <summary>
        /// return "R" or "V"; R=> Returned, V=> Verified
        /// </summary>
        /// <param name="mstDprId"></param>
        /// <param name="fundReleaseId"></param>
        /// <param name="userId"></param>
        /// <param name="checks"></param>
        /// <returns></returns>
        public string verifyForFundRelease(long mstDprId, long fundReleaseId, long userId, List<DprModel.DprFundRelease.ApplyCheck> checks)
        {
            checks = (checks == null || checks.Count == 0 ? throw new Exception("No Verification Check list supplied") : checks);
            SqlTransaction tr = null;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                    tr = cn.BeginTransaction();
                }

                checks.ForEach(t =>
                {
                    var cmd = _db.NewCommand("udma.dpr_updateAppliedInstVerifyCheck", cn, tr,
                        CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@ChkId", t.ChkId);
                    cmd.Parameters.AddWithValue("@isVerified", t.isVerified);
                    cmd.Parameters.AddWithValue("@VerifyRemarks", t.VerifyRemarks.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.ExecuteNonQuery();
                });
                var cmdFinal = _db.NewCommand("udma.dpr_updateLookBackVerifiedCheckListNTryToChangeVerified", cn, tr,
                    CommandType.StoredProcedure);
                cmdFinal.Parameters.AddWithValue("@mstDprId", mstDprId);
                cmdFinal.Parameters.AddWithValue("@fundReleaseId", fundReleaseId);
                cmdFinal.Parameters.AddWithValue("@userId", userId);

                var result = _db.GetScalar<string>(cmdFinal);
                tr.Commit();
                return result;
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
            finally { cn.Close(); }
        }

        public FundReleaseAppliedApproval GetInstallmentObjectForApproval(long mstDprID, long fundReleaseID, long userID)
        {
            var mainObjects = getAlreadyAppliedInstallments(mstDprID, fundReleaseID);
            var mainObject = mainObjects.Count > 0 ? mainObjects[0] : throw new Exception("Installment not found");

            var outPutObject = new DprModel.DprFundRelease.FundReleaseAppliedApproval();
            outPutObject.DprAllocationSteps = GetDprAllocationStepsFundRelease(fundReleaseID);
            var otherAttr = GetOtherAttributesOfDprInstallment(fundReleaseID, userID, mstDprID);
            outPutObject.LastApprovedAmount = otherAttr.LastApprovedAmount;
            outPutObject.IsFinallAprovalPossible = mainObject.IsFinallAprovalPossible;
            outPutObject.canApprovalAmtChangble = otherAttr.IsApporvalAmountChangable;
            outPutObject.CanGenerateAAFS = otherAttr.CanGenerateAAFS;
            outPutObject.isReleaseFileUploded = GetReleaseOrderFile(fundReleaseID);
            outPutObject.AaFs = GetReleaseOrderDetails(fundReleaseID);
            outPutObject.Designations = GetDesignationsInstallment(userID, fundReleaseID);

            outPutObject.Status = mainObject.Status;
            outPutObject.StatusDscr = mainObject.StatusDscr;
            outPutObject.LastStatusChangedOn = mainObject.LastStatusChangedOn;
            outPutObject.FundReleaseId = mainObject.FundReleaseId;
            outPutObject.MstDprId = mainObject.MstDprId;
            outPutObject.InstallmentId = mainObject.InstallmentId;
            outPutObject.InstallmentAmount = mainObject.InstallmentAmount;
            outPutObject.StartDate = mainObject.StartDate;
            outPutObject.EndDate = mainObject.EndDate;
            outPutObject.MemoNo = mainObject.MemoNo;
            outPutObject.MemoDate = mainObject.MemoDate;
            outPutObject.AppliedOn = mainObject.AppliedOn;
            outPutObject.ApplyCheckList = mainObject.ApplyCheckList;
            outPutObject.InstallmentMaster = mainObject.InstallmentMaster;
            outPutObject.DprAllocationSteps = GetDprAllocationStepsFundRelease(fundReleaseID);
            outPutObject.ApprovalActions = GetApprovalActionList(userID, outPutObject.LastApprovedAmount, outPutObject.FundReleaseId, "FR");
            outPutObject.SubProjects = mainObject.SubProjects;
            outPutObject.LetterOrUcDocUrl = mainObject.LetterOrUcDocUrl;
            outPutObject.ApplyCheckList = mainObject.ApplyCheckList;
            outPutObject.AbstractPgDocUrl = mainObject.AbstractPgDocUrl;
            outPutObject.AAFSFileDocUrl = mainObject.AAFSFileDocUrl;
            outPutObject.IsOldDprEntry = mainObject.IsOldDprEntry;
            outPutObject.OpenDesignationId = GetOpenDesignationIdForFundRelease(fundReleaseID);
            outPutObject.CanChangeInternalStatus = GetInternalStatusChangeBit(mstDprID, fundReleaseID);
            return outPutObject;
        }
        public DprModel.DprOtherAttributes GetOtherAttributesOfDprInstallment(long fundReleaseID, long userId, long mstDprId)
        {
            var cmd = _db.NewCommand("udma.get_dpr_InstallmentOtherAttributes", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@fundReleaseID", fundReleaseID);
            cmd.Parameters.AddWithValue("@userId", userId);
            //cmd.Parameters.AddWithValue("@mstDprId", mstDprId);
            DataTable dt = _db.GetResult(cmd);
            var lst = dt.AsEnumerable()
                .Select(t =>
                {
                    var v = new DprModel.DprOtherAttributes()
                    {
                        IsApporvalAmountChangable = t.Field<bool>("canApprovalAmtChangble"),
                        LastApprovedAmount = t.Field<decimal>("lastApprovedAmount"),
                        CanGenerateAAFS= t.Field<bool>("CanGenerateAAFS"),
                    };
                    return v;
                }).ToList();
            if (lst.Count > 0)
            {
                return lst[0];
            }
            else
            {
                return null;
            }
        }
        private List<DprModel.DprAllocationStep> GetDprAllocationStepsFundRelease(long fundReleaseId)
        {
            var cmd = _db.NewCommand("udma.Get_Dpr_DprAllocationStepsFundRelease", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@fundReleaseID", fundReleaseId);
            return _db.GetResult(cmd).AsEnumerable().Select(t =>
            {
                var allocationStep = new DprModel.DprAllocationStep();
                allocationStep.AssignedById = t.Field<long?>("AssignedById");
                allocationStep.AssignedByName = t.Field<string>("AssignedByName");
                allocationStep.AssignedToId = t.Field<long?>("AssignedToId");
                allocationStep.AssignedOn = t.Field<DateTime?>("AssignedOn");
                allocationStep.AssignedToName = t.Field<string>("AssignedToName");
                allocationStep.TDesignationId = t.Field<int?>("TDesignationId");
                allocationStep.TDesignationName = t.Field<string>("TDesignationName");
                allocationStep.HiarcheyId = t.Field<int>("HiarcheyId");
                allocationStep.Id = t.Field<long>("Id");
                allocationStep.Remarks = t.Field<string>("Remarks");
                allocationStep.Status = t.Field<string>("Status");
                allocationStep.RepliedOn = t.Field<DateTime?>("RepliedOn");
                allocationStep.AprovedProjectAmount = t.Field<decimal>("AprovedProjectAmount");
                return allocationStep;
            }).ToList();
        }
        internal void ChangeInstallmentApporvalStatus(ChangeDprInstallmentApproval payload, long userID)
        {
            SqlTransaction tr = null;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                tr = cn.BeginTransaction();
                if(payload.SobProjNotSelected !=null)
                {
                    for (int i = 0; i < payload.SobProjNotSelected.Count; i++)
                    {
                        var cmd1 = _db.NewCommand("udma.update_Dpr_InstallmentSubProj", cn, tr, CommandType.StoredProcedure);
                        cmd1.Parameters.AddWithValue("@sbpId", payload.SobProjNotSelected[i].sbpId);
                        cmd1.Parameters.AddWithValue("@DprId", payload.SobProjNotSelected[i].DprId);
                        cmd1.Parameters.AddWithValue("@instId", payload.SobProjNotSelected[i].instId);
                        _db.ExecuteNonQuery(cmd1);
                    }
                }
                var cmd = _db.NewCommand("udma.update_Dpr_ChangeInstallmentApprovalStatusNew", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@fundReleaseId", payload.fundReleaseId);
                cmd.Parameters.AddWithValue("@targetDesignationId", payload.targetDesignationId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@status", payload.status.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@userId", userID);
                cmd.Parameters.AddWithValue("@remarks", payload.remarks.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@approvedAmount", payload.approvedAmount);

                cmd.Parameters.AddWithValue("@approvalDate", payload.ApprovalDate.ToDbDate());
                cmd.Parameters.AddWithValue("@uoNo", payload.UoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@uoDate", payload.UoDate.ToDbDate());
                cmd.Parameters.AddWithValue("@accountHead", payload.AccountHead.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@sanctionNo", payload.SanctionNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ReleaseOrderNo", payload.ReleaseOrderNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ReleaseOrderDt", payload.ReleaseOrderDt.ToDbDate());
                cmd.Parameters.AddWithValue("@StageId", payload.ReleaseOrderFile==null? payload.ReleaseOrderFile.ReplaceDbNull(): payload.ReleaseOrderFile.StageId.ReplaceDbNull());
                var lst = _db.ExecuteNonQuery(cmd);
                if (payload.Subprojects != null)
                {
                    for (int i = 0; i < payload.Subprojects.Count; i++) 
                    {
                        var cmd1 = _db.NewCommand("udma.update_Dpr_InstallmentSubProj", cn, tr, CommandType.StoredProcedure);
                        cmd1.Parameters.AddWithValue("@sbpId", payload.Subprojects[i].Id);
                        cmd1.Parameters.AddWithValue("@fundReleaseId", payload.fundReleaseId);
                        cmd1.Parameters.AddWithValue("@instId", payload.fundReleaseId);
                        cmd1.Parameters.AddWithValue("@Cost", payload.Subprojects[i].Cost);
                        _db.ExecuteNonQuery(cmd1);
                    }
                }
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public string GetReleaseOrderFile(long fundReleaseId)
        {
            var cmd = _db.NewCommand("udma.Get_ReleaseOrderFile", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId);
            DataTable dt = _db.GetResult(cmd);
            return dt.Rows[0].Field<string>("ReleaseFile");
        }
        public FinalApprovalExt GetReleaseOrderDetails(long fundReleaseId)
        {
            var cmd = _db.NewCommand("udma.Get_ReleaseOrderDetail", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@fundReleaseId", fundReleaseId);
            DataTable dt = _db.GetResult(cmd);
            var lst = dt.AsEnumerable()
                .Select(t =>
                {
                    var v = new FinalApprovalExt()
                    {
                        SanctionNo = t.Field<string>("SanctionNo"),
                        UoNo = t.Field<string>("UoNo"),
                        UoDate = t.Field<DateTime?>("UoDate"),
                        ApprovalDate = t.Field<DateTime?>("ApprovalDate"),
                        ReleaseOrderNo = t.Field<string>("ReleaseOrderNo"),
                        ReleaseOrderDate = t.Field<DateTime?>("ReleaseOrderDate"),
                        ReleaseFilePath = t.Field<string>("ReleaseFile").BindHost(),
                    };
                    return v;
                }).ToList();
            if (lst.Count > 0)
            {
                return lst[0];
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region old fund release
        public DprModel.DprFundRelease.OldFundRelease GetOldInstallMent(long mstDprID, long fundReleaseId)
        {
            //check installment applied => throw exception
            var appliedInstallments = getAlreadyAppliedInstallments<OldFundRelease>(mstDprID, fundReleaseId);
            if (appliedInstallments.Count == 0)
            {
                throw new Exception("Installment not found");
            }
            var appliedInstallment = appliedInstallments[0];
            //get installment master
            var installMentMasters = getInstallmentMasters(appliedInstallment.InstallmentId);
            var instMstr = installMentMasters.Count > 0 ? installMentMasters[0] : throw new Exception("Invalid installment requested");

            //get related check list master and convert to applycheck
            instMstr.CheckList = getInstallmentCheckListMaster(instMstr.Id);
            appliedInstallment.InstallmentMaster = instMstr;
            return appliedInstallment;
        }
        public void UpdateOldFundRelase(OldFundRelease payload,long userId, int? municipalityId)
        {
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            var tr = cn.BeginTransaction();
            try
            {
                var cmd = _db.NewCommand("udma.Update_dprOldFundRelease", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@MstDprId", payload.MstDprId);
                cmd.Parameters.AddWithValue("@FundReleaseId", payload.FundReleaseId);
                cmd.Parameters.AddWithValue("@MemoNo", payload.MemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoDate", payload.MemoDate.ToDbDate());
                cmd.Parameters.AddWithValue("@InstallmentAmount", payload.InstallmentAmount);
                cmd.Parameters.AddWithValue("@StartDate", payload.StartDate.ToDbDate());
                cmd.Parameters.AddWithValue("@EndDate", payload.EndDate.ToDbDate());
                cmd.Parameters.AddWithValue("@ApprovedAmount", payload.ApprovedAmount);
                cmd.Parameters.AddWithValue("@ApprovalDate", payload.ApprovalDate.ToDbDate());
                cmd.Parameters.AddWithValue("@UoNo", payload.UoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UoDate", payload.UoDate.ToDbDate());
                cmd.Parameters.AddWithValue("@SanctionMemoNo", payload.SanctionMemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@VerificationDate", payload.VerificationDate.ToDbDate());
                cmd.Parameters.AddWithValue("@AccountHeadNo", payload.AccountHeadNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MunicipalityId", municipalityId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@userId", userId);
                _db.ExecuteNonQuery(cmd);

                cmd = _db.NewCommand("udma.DeletePreviousCheckList", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@fundReleaseId", payload.FundReleaseId);
                _db.ExecuteNonQuery(cmd);

                payload.ApplyCheckList.ForEach(t =>
                {
                    cmd = _db.NewCommand("udma.Insert_DprApplyNewInsallCheckList", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@fundReleaseId", payload.FundReleaseId);
                    cmd.Parameters.AddWithValue("@chkId", t.ChkId);
                    cmd.Parameters.AddWithValue("@IsChecked", t.IsChecked);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@verfiedOn", payload.VerificationDate.ToDbDate());
                    _db.ExecuteNonQuery(cmd);
                });
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        #endregion

        #region Old Dpr Submission
        public DprModel.OldDpr GetOldDprModel(long? DprId,int? municipalityId)
        {
            var mstr = new DprModel.OldDpr()
            {
                Part1 = new DprModel.Part()
                ,
                Part2 = new DprModel.Part()
            };
            if (DprId != null)
            {
                var cmd = _db.NewCommand("Udma.Get_OldDprInDetail", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", DprId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@municipalityId", municipalityId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<DprModel.OldDpr>();
                if (rslt.Count > 0)
                {
                    IncludePhasingExpenditure(rslt.ToList<DprModel.Master>());
                    IncludeSubProjects(rslt.ToList<DprModel.Master>());
                    IncludeInstallment(rslt.ToList<DprModel.Master>());
                    mstr = rslt[0];
                }
                else
                {
                    throw new Exception("Tergate DPR not found");
                }
            }
            MunicipalityBLL mnBll = new MunicipalityBLL();
            mstr.SchemeCategories = GetSchemeCategories(null);
            mstr.SchemeTypes = GetSchemeTypes();
            mstr.Municipalities = mnBll.GetAllMunicipalities().Select(t => new KeyValue<int, string> { Key = t.MunicipalityID ?? 0, Value = t.MunicipalityName }).ToList();
            mstr.Units = GetUnits();
            mstr.instlmntDrpDwn = GetInst();
            Random rnd = new Random();
            int rndNo = rnd.Next(5000);
            mstr.IdentityStamp = DateTime.Now.Ticks + "" + rndNo;
            mstr.Installments.ForEach(t => {
                t.SubProjects = IncludeInstallmentSubProj(t.FundReleaseId, DprId);
            });
            return mstr;
        }
        public DprModel.SubmittedDpr NewEntryOldDpr(DprModel.OldDpr form, Base64File file, long userId)
        {
            SqlTransaction tr = null;
            try
            {
                //insert master and fetch id
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                var filePath = "";
                var AAFSFilePath = "";
                if (form.AAFSFile == null)
                    throw new Exception("Please upload AA & FS.");
                if (file!=null)
                    filePath = file.SaveString64File();
                tr = cn.BeginTransaction();
                var cmd = _db.NewCommand("Udma.Insert_DprOldMasterForm", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@SchemeDate", form.SchemeDate == null ? DBNull.Value : form.SchemeDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DeptType", DBNull.Value);
                cmd.Parameters.AddWithValue("@MunicipalityDevAuthoID", form.MunicipalityId);
                cmd.Parameters.AddWithValue("@NameOfWork", form.NameOfWork.ReplaceDbNull());                
                cmd.Parameters.AddWithValue("@ImplementingAgency", form.AgencyName.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@EstimatedCost", form.EstimatedCost);
                cmd.Parameters.AddWithValue("@RevEstimatedCost", form.RevisedEstimatedCost);
                cmd.Parameters.AddWithValue("@NameOfEngineer", form.NameOfEngineer.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Designation", form.Designation.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ContactNumber", form.ContactNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoNumber", form.MemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoDate", form.MemoDate == null ? DBNull.Value : form.MemoDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DocFilePath", DBNull.Value);
                cmd.Parameters.AddWithValue("@SchemeTypeId", form.SchemeTypeId);
                cmd.Parameters.AddWithValue("@SchemeCategoryId", form.SchemeCategoryId);
                cmd.Parameters.AddWithValue("@EmailId", form.EmailId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@PorjectNo", form.ProjectNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Projectdate", form.Projectdate == null ? DBNull.Value : form.Projectdate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DprVerifiedOn", form.DprVerifiedOn == null ? DBNull.Value : form.DprVerifiedOn.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DprApprovalDate", form.DprApprovalDate == null ? DBNull.Value : form.DprApprovalDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@ApprovedAmount", form.ApprovedAmount);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@dprSubmittedOn", form.DprSubmissionDate.ToDbDate());
                cmd.Parameters.AddWithValue("@SanctionMemoNo", form.SanctionMemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@AccountHeadNo", form.AccountHeadNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UONo", form.UONo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UODate", form.UODate.ToDbDate());
                cmd.Parameters.AddWithValue("@ReceivedAmt", form.ReceivedAmt.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UtilizationCrtifictPth", filePath.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@AAFSFilePath", AAFSFilePath.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@StageId", form.AAFSFile.StageId);
                cmd.Parameters.AddWithValue("@Remarks", form.Remarks.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);
                if (dt.Rows.Count <= 0)
                {
                    throw new Exception("DPR not submitted successfully");
                }
                var submitttedDpr = dt.AsEnumerable().Select(t => new DprModel.SubmittedDpr
                {
                    DocVerificationDate = t.Field<DateTime>("DprVerificationDate"),
                    MstDprId = t.Field<long>("tabId")
                }).ToList()[0];
                var mstTabId = submitttedDpr.MstDprId;

                //Insert Dpr Executive Summary
                cmd = _db.NewCommand("udma.insert_dprExecutiveSummary", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@ExecutiveSummary", form.ExecutiveSummary.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                cmd.Parameters.AddWithValue("@CommencementDate", form.CommencementDate == null ? DBNull.Value : form.CommencementDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@CompletionDate", form.CompletionDate == null ? DBNull.Value : form.CompletionDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                _db.ExecuteNonQuery(cmd);
                //delete all previous phasing of expenditures on perticular dpr id
                cmd = _db.NewCommand("udma.delete_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                _db.ExecuteNonQuery(cmd);

                //delete all previous subprojects on perticular dpr id
                cmd = _db.NewCommand("udma.delete_dprsubprojects", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                _db.ExecuteNonQuery(cmd);
                form.PhasingOfExpenditures.ForEach(t =>
                {
                    t.Validate();
                    cmd = _db.NewCommand("udma.insert_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@mstDprId", mstTabId.ReplaceDbNull());
                    cmd.Parameters.AddWithValue("@finYear", t.FinYear);
                    cmd.Parameters.AddWithValue("@expenditureAmount", t.ExpenditureAmount);
                    _db.ExecuteNonQuery(cmd);
                });
                if(form.HasSubProj==true)
                {
                    if (form.SubProjects.Count == 0)
                    {
                        throw new Exception("Please enter sub project details.");
                    }
                    int i = 1;
                    long StageId = 0;
                    form.SubProjects.ForEach(t =>
                    {
                        var SubProjFilePath = "";
                        form.SubpProjFiles.ForEach(t1 =>
                        {
                            if (t.RandId == t1.RandId)
                            {
                                //if(t1.file!=null)
                                //    SubProjFilePath = t1.file.SaveString64File();
                                StageId = t1.StageId;
                            }
                        });
                        t.Validate();
                        if(StageId>0)
                        {
                            cmd = _db.NewCommand("udma.dpr_Insert_subprojects", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@mstDprId", mstTabId.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@subProjectName", t.Name.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@estimatedCost", t.Cost);
                            cmd.Parameters.AddWithValue("@qty", t.Qty);
                            cmd.Parameters.AddWithValue("@unitid", t.UnitId);
                            cmd.Parameters.AddWithValue("@SubProjFile", SubProjFilePath.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@StageId", StageId);
                            cmd.Parameters.AddWithValue("@IsCalledForOldDpr", 'Y');
                            cmd.Parameters.AddWithValue("@SubProjSlNo", i);
                            t.Id = _db.GetScalar<long>(cmd);
                            i++;
                        }
                        else
                            throw new Exception("Please upload Subproject File.");
                        StageId = 0;

                    });
                    form.Installments.ForEach(t => {
                        long instId = 0;
                        decimal totalInstSubprojAmt = 0;
                        decimal totalInstSurrenderAmt = 0;
                        long SCorTCFileStageId = 0;
                        t.SubProjects.ForEach(tt => {
                            totalInstSubprojAmt = totalInstSubprojAmt + tt.Amount;
                        });
                        t.SubProjects.ForEach(tt => {
                            totalInstSurrenderAmt = (totalInstSurrenderAmt + tt.SurrenderAmount) ?? 0;
                            if (SCorTCFileStageId == 0)
                                SCorTCFileStageId = tt.StageId;
                        });
                        if (totalInstSubprojAmt!= t.InstAmnt)
                            throw new Exception("Released Order Amount must be equal to sum of Sub Project's amount for installment No: "+ t.InstallmentId);
                        if (t.StageId>0)
                        {
                            cmd = _db.NewCommand("udma.Insert_OldDprInstallment", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@mstDprId", mstTabId);
                            cmd.Parameters.AddWithValue("@installmentAmount", t.InstAmnt);
                            cmd.Parameters.AddWithValue("@memoNo", t.OrderNo.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@memoDate", t.OderDate.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@installmentId ", t.InstallmentId);
                            cmd.Parameters.AddWithValue("@userId", userId);
                            cmd.Parameters.AddWithValue("@municipalityId", form.MunicipalityId);
                            cmd.Parameters.AddWithValue("@StageId", t.StageId);
                            cmd.Parameters.AddWithValue("@ReleaseOrdrNo", t.SancNo.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@ReleaseOrdrDt", t.SancDate.ReplaceDbNull());
                            instId = _db.GetScalar<long>(cmd);
                        }
                        else
                            throw new Exception("Please upload Released Installment File.");
                        t.StageId = 0;
                        t.SubProjects.ForEach(tt => {
                            if(tt.SurrenderAmount>0 && tt.StageId==0)
                                throw new Exception("Please upload Surrender Certificate / Treasury Challan File.");
                            var subProject = form.SubProjects.FirstOrDefault(s => string.Equals(s._SKey, tt._SKey, StringComparison.OrdinalIgnoreCase));
                            subProject = subProject ?? throw new Exception("Subproject not exist in subproject master");
                            var subProjectId = subProject.Id;
                            cmd = _db.NewCommand("udma.Inster_SubProjectInstallment", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@subProjectID", subProjectId);
                            cmd.Parameters.AddWithValue("@mstDprID", mstTabId);
                            cmd.Parameters.AddWithValue("@fundReleaseId", instId);
                            cmd.Parameters.AddWithValue("@amount", tt.Amount);
                            cmd.Parameters.AddWithValue("@SurrenderAmount", tt.SurrenderAmount);
                            cmd.Parameters.AddWithValue("@StageId", tt.StageId);
                            _db.ExecuteNonQuery(cmd);
                            //udma.Inster_SubProjectInstallment
                            //@subProjectID bigint,=>subProjectId
                            //@mstDprID bigint,=>mstTabId
                            //@fundReleaseId bigint,=>instId
                            //@amount decimal(18, 2)=>tt.Amount
                        });
                        cmd = _db.NewCommand("udma.Inster_DprAllocationFundRelease", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@FundReleaseId", instId);
                        cmd.Parameters.AddWithValue("@amount", t.InstAmnt);
                        cmd.Parameters.AddWithValue("@TotalInstSurrenderAmt", totalInstSurrenderAmt);
                        cmd.Parameters.AddWithValue("@SCorTCFileStageId", SCorTCFileStageId);
                        _db.ExecuteNonQuery(cmd);
                        //Inster_DprAllocationFundRelease
                        //@FundReleaseId bigint,
                        //@amount decimal(18, 2)
                    });
                }
                tr.Commit();
                return submitttedDpr;
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        public void UpdateDprMasterForm(DprModel.OldDpr form, long userId)
        {
            SqlTransaction tr = null;
            try
            {
                //insert master and fetch id
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                tr = cn.BeginTransaction();
                var cmd = _db.NewCommand("Udma.Update_DprMasterForm", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@SchemeDate", form.SchemeDate == null ? DBNull.Value : form.SchemeDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DeptType", DBNull.Value);
                cmd.Parameters.AddWithValue("@MunicipalityDevAuthoID", form.MunicipalityId);
                cmd.Parameters.AddWithValue("@NameOfWork", form.NameOfWork.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ImplementingAgency", form.AgencyName.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@RevEstimatedCost", form.RevisedEstimatedCost);
                cmd.Parameters.AddWithValue("@EstimatedCost", form.EstimatedCost);
                cmd.Parameters.AddWithValue("@NameOfEngineer", form.NameOfEngineer.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Designation", form.Designation.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@ContactNumber", form.ContactNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoNumber", form.MemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@MemoDate", form.MemoDate == null ? DBNull.Value : form.MemoDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DocFilePath", DBNull.Value);
                cmd.Parameters.AddWithValue("@SchemeTypeId", form.SchemeTypeId);
                cmd.Parameters.AddWithValue("@SchemeCategoryId", form.SchemeCategoryId);
                cmd.Parameters.AddWithValue("@EmailId", form.EmailId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@PorjectNo", form.ProjectNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@Projectdate", form.Projectdate == null ? DBNull.Value : form.Projectdate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DprVerifiedOn", form.DprVerifiedOn == null ? DBNull.Value : form.DprVerifiedOn.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@DprApprovalDate", form.DprApprovalDate == null ? DBNull.Value : form.DprApprovalDate.Value.ToString("yyyy'-'MM'-'dd") as object);
                cmd.Parameters.AddWithValue("@ApprovedAmount", form.ApprovedAmount);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.Parameters.AddWithValue("@dprSubmittedOn", form.DprSubmissionDate.ToDbDate());

                cmd.Parameters.AddWithValue("@SanctionMemoNo", form.SanctionMemoNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@AccountHeadNo", form.AccountHeadNo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UONo", form.UONo.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@UODate", form.UODate.ToDbDate());
                if(form.AAFSFile !=null && form.AAFSFile.StageId >0)
                    cmd.Parameters.AddWithValue("@StageId", form.AAFSFile.StageId);
                cmd.Parameters.AddWithValue("@Remarks", form.Remarks.ReplaceDbNull());
                DataTable dt = _db.GetResult(cmd);

                //delete all previous phasing of expenditures on perticular dpr id
                //cmd = _db.NewCommand("udma.delete_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                //cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                //_db.ExecuteNonQuery(cmd);

                //delete all previous subprojects on perticular dpr id
                //cmd = _db.NewCommand("udma.delete_dprsubprojects", cn, tr, CommandType.StoredProcedure);
                //cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                //_db.ExecuteNonQuery(cmd);
                //form.PhasingOfExpenditures.ForEach(t =>
                //{
                //    t.Validate();
                //    cmd = _db.NewCommand("udma.insert_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                //    cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                //    cmd.Parameters.AddWithValue("@finYear", t.FinYear);
                //    cmd.Parameters.AddWithValue("@expenditureAmount", t.ExpenditureAmount);
                //    _db.ExecuteNonQuery(cmd);
                //});
                //if (form.HasSubProj == true)
                //{
                //    int i = 1;
                //    form.SubProjects.ForEach(t =>
                //    {
                //        t.Validate();
                //        cmd = _db.NewCommand("udma.dpr_Insert_subprojects", cn, tr, CommandType.StoredProcedure);
                //        cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                //        cmd.Parameters.AddWithValue("@subProjectName", t.Name.ReplaceDbNull());
                //        cmd.Parameters.AddWithValue("@estimatedCost", t.Cost);
                //        cmd.Parameters.AddWithValue("@SubProjSlNo", i);
                //        _db.ExecuteNonQuery(cmd);
                //        i++;
                //    });
                //}

                cmd = _db.NewCommand("udma.delete_PhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@PhasingIds", string.Join(",", form.PhasingOfExpenditures.Select(t => t.Id).ToList()));
                cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                _db.ExecuteNonQuery(cmd);
                form.PhasingOfExpenditures.ForEach(t =>
                {
                    t.Validate();
                    if(t.Id>0)
                    {
                        cmd = _db.NewCommand("udma.Update_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@PhasingId", t.Id.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@finYear", t.FinYear);
                        cmd.Parameters.AddWithValue("@expenditureAmount", t.ExpenditureAmount);
                        _db.ExecuteNonQuery(cmd);
                    }
                    else
                    {
                        cmd = _db.NewCommand("udma.insert_dprPhasingOfExpenditure", cn, tr, CommandType.StoredProcedure);
                        cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                        cmd.Parameters.AddWithValue("@finYear", t.FinYear);
                        cmd.Parameters.AddWithValue("@expenditureAmount", t.ExpenditureAmount);
                        _db.ExecuteNonQuery(cmd);
                    }
                });
               
                if (form.HasSubProj == true)
                {
                    if (form.SubProjects.Count == 0)
                    {
                        throw new Exception("Please enter sub project details.");
                    }
                    int i = 1;
                    long StageId = 0;
                    cmd = _db.NewCommand("udma.delete_subprojects", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@SbpIDs", string.Join(",", form.SubProjects.Select(t => t.Id).ToList()));
                    cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                    _db.ExecuteNonQuery(cmd);
                    form.SubProjects.ForEach(t =>
                    {
                        if(t.Id>0)
                        {
                            form.SubpProjFiles.ForEach(t1 =>
                            {
                                if (t.RandId == t1.RandId)
                                {
                                    StageId = t1.StageId;
                                }
                            });

                            t.Validate();
                            cmd = _db.NewCommand("udma.dpr_Update_subprojects", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@SbpID", t.Id);
                            cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@subProjectName", t.Name.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@estimatedCost", t.Cost);
                            cmd.Parameters.AddWithValue("@qty", t.Qty);
                            cmd.Parameters.AddWithValue("@unitid", t.UnitId);
                            if(StageId>0)
                                cmd.Parameters.AddWithValue("@StageId", StageId);
                            _db.ExecuteNonQuery(cmd);
                        }
                        else
                        {
                            var SubProjFilePath = "";
                            form.SubpProjFiles.ForEach(t1 =>
                            {
                                if (t.RandId == t1.RandId)
                                {
                                    StageId = t1.StageId;
                                }
                            });
                            t.Validate();
                            cmd = _db.NewCommand("udma.dpr_Insert_subprojects", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@subProjectName", t.Name.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@estimatedCost", t.Cost);
                            cmd.Parameters.AddWithValue("@qty", t.Qty);
                            cmd.Parameters.AddWithValue("@unitid", t.UnitId);
                            cmd.Parameters.AddWithValue("@SubProjFile", SubProjFilePath.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@StageId", StageId);
                            cmd.Parameters.AddWithValue("@IsCalledForOldDpr", 'Y');
                            cmd.Parameters.AddWithValue("@SubProjSlNo", i);
                            t.Id = _db.GetScalar<long>(cmd);
                            i++;
                        }
                        StageId = 0;

                    });
                    cmd = _db.NewCommand("udma.delete_OldDprInstallment", cn, tr, CommandType.StoredProcedure);
                    cmd.Parameters.AddWithValue("@FundReleaseIds", string.Join(",", form.Installments.Select(t => t.FundReleaseId).ToList()));
                    cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId.ReplaceDbNull());
                    _db.ExecuteNonQuery(cmd);
                    form.Installments.ForEach(t => {
                        if(t.FundReleaseId>0)
                        {
                            cmd = _db.NewCommand("udma.Update_OldDprInstallment", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@FundReleaseId", t.FundReleaseId);
                            cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId);
                            cmd.Parameters.AddWithValue("@installmentAmount", t.InstAmnt);
                            cmd.Parameters.AddWithValue("@memoNo", t.OrderNo.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@memoDate", t.OderDate.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@installmentId ", t.InstallmentId);
                            cmd.Parameters.AddWithValue("@ReleaseOrdrNo", t.SancNo.ReplaceDbNull());
                            cmd.Parameters.AddWithValue("@ReleaseOrdrDt", t.SancDate.ReplaceDbNull());
                            if(t.StageId>0)
                                cmd.Parameters.AddWithValue("@StageId", t.StageId);
                            _db.ExecuteNonQuery(cmd);

                            t.SubProjects.ForEach(tt => {
                                if(tt.TabId>0)
                                {
                                    var subProject = form.SubProjects.FirstOrDefault(s => s.Id == tt.sbpId);
                                    subProject = subProject ?? throw new Exception("Subproject not exist in subproject master");
                                    var subProjectId = subProject.Id;
                                    cmd = _db.NewCommand("udma.Update_SubProjectInstallment", cn, tr, CommandType.StoredProcedure);
                                    cmd.Parameters.AddWithValue("@TabId", tt.TabId);
                                    cmd.Parameters.AddWithValue("@subProjectID", subProjectId);
                                    cmd.Parameters.AddWithValue("@mstDprID", form.DPChkSlId);
                                    cmd.Parameters.AddWithValue("@fundReleaseId", t.FundReleaseId);
                                    cmd.Parameters.AddWithValue("@amount", tt.Amount);
                                    _db.ExecuteNonQuery(cmd);
                                }
                                else
                                {
                                    var subProject = form.SubProjects.FirstOrDefault(s => string.Equals(s._SKey, tt._SKey, StringComparison.OrdinalIgnoreCase));
                                    subProject = subProject ?? throw new Exception("Subproject not exist in subproject master");
                                    var subProjectId = subProject.Id;
                                    cmd = _db.NewCommand("udma.Inster_SubProjectInstallment", cn, tr, CommandType.StoredProcedure);
                                    cmd.Parameters.AddWithValue("@subProjectID", subProjectId);
                                    cmd.Parameters.AddWithValue("@mstDprID", form.DPChkSlId);
                                    cmd.Parameters.AddWithValue("@fundReleaseId", t.FundReleaseId);
                                    cmd.Parameters.AddWithValue("@amount", tt.Amount);
                                    cmd.Parameters.AddWithValue("@SurrenderAmount", tt.SurrenderAmount);
                                    _db.ExecuteNonQuery(cmd);
                                }
                            });
                           
                        }
                        else
                        {
                            long instId = 0;
                            if (t.StageId > 0)
                            {
                                cmd = _db.NewCommand("udma.Insert_OldDprInstallment", cn, tr, CommandType.StoredProcedure);
                                cmd.Parameters.AddWithValue("@mstDprId", form.DPChkSlId);
                                cmd.Parameters.AddWithValue("@installmentAmount", t.InstAmnt);
                                cmd.Parameters.AddWithValue("@memoNo", t.OrderNo.ReplaceDbNull());
                                cmd.Parameters.AddWithValue("@memoDate", t.OderDate.ReplaceDbNull());
                                cmd.Parameters.AddWithValue("@installmentId ", t.InstallmentId);
                                cmd.Parameters.AddWithValue("@userId", userId);
                                cmd.Parameters.AddWithValue("@municipalityId", form.MunicipalityId);
                                cmd.Parameters.AddWithValue("@StageId", t.StageId);
                                cmd.Parameters.AddWithValue("@ReleaseOrdrNo", t.SancNo.ReplaceDbNull());
                                cmd.Parameters.AddWithValue("@ReleaseOrdrDt", t.SancDate.ReplaceDbNull());
                                instId = _db.GetScalar<long>(cmd);
                            }
                           else
                                throw new Exception("Please upload release order file for installment No: " + t.InstallmentId);

                            decimal totalInstSurrenderAmt = 0;
                            decimal totalInstSubprojAmt = 0;
                            long SCorTCFileStageId = 0;
                            t.SubProjects.ForEach(tt => {
                                totalInstSubprojAmt = totalInstSubprojAmt + tt.Amount;
                            });
                            t.SubProjects.ForEach(tt => {
                                totalInstSurrenderAmt = (totalInstSurrenderAmt + tt.SurrenderAmount) ?? 0;
                                if (SCorTCFileStageId == 0)
                                    SCorTCFileStageId = tt.StageId;
                            });
                            if (totalInstSubprojAmt != t.InstAmnt)
                                throw new Exception("Released Order Amount must be equal to sum of Sub Project's amount for installment No: " + t.InstallmentId);

                            t.StageId = 0;
                            t.SubProjects.ForEach(tt => {
                                if (tt.SurrenderAmount > 0 && tt.StageId == 0)
                                    throw new Exception("Please upload Surrender Certificate / Treasury Challan File.");
                                var subProject = form.SubProjects.FirstOrDefault(s => string.Equals(s._SKey, tt._SKey, StringComparison.OrdinalIgnoreCase));
                                subProject = subProject ?? throw new Exception("Subproject not exist in subproject master");
                                var subProjectId = subProject.Id;
                                cmd = _db.NewCommand("udma.Inster_SubProjectInstallment", cn, tr, CommandType.StoredProcedure);
                                cmd.Parameters.AddWithValue("@subProjectID", subProjectId);
                                cmd.Parameters.AddWithValue("@mstDprID", form.DPChkSlId);
                                cmd.Parameters.AddWithValue("@fundReleaseId", instId);
                                cmd.Parameters.AddWithValue("@amount", tt.Amount);
                                cmd.Parameters.AddWithValue("@SurrenderAmount", tt.SurrenderAmount);
                                cmd.Parameters.AddWithValue("@StageId", tt.StageId);
                                _db.ExecuteNonQuery(cmd);
                            });
                            cmd = _db.NewCommand("udma.Inster_DprAllocationFundRelease", cn, tr, CommandType.StoredProcedure);
                            cmd.Parameters.AddWithValue("@FundReleaseId", instId);
                            cmd.Parameters.AddWithValue("@amount", t.InstAmnt);
                            cmd.Parameters.AddWithValue("@TotalInstSurrenderAmt", totalInstSurrenderAmt);
                            cmd.Parameters.AddWithValue("@SCorTCFileStageId", SCorTCFileStageId);
                            _db.ExecuteNonQuery(cmd);
                        }
                        
                    });
                }


                if (dt.Rows.Count <= 0)
                {
                    throw new Exception("DPR not submitted successfully");
                }
                tr.Commit();
            }
            catch
            {
                if (tr != null)
                {
                    tr.Rollback();
                }
                throw;
            }
        }
        void IncludePhasingExpenditure(List<DprModel.Master> dprs)
        {
            dprs.ForEach(t => {
                var cmd = _db.NewCommand("Udma.get_dpr_PashingOfExpenditures", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", t.DPChkSlId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<DprModel.PhasingOfExpenditure>();
                t.PhasingOfExpenditures = rslt;
            });
            
        }
        
        void IncludeSubProjects(List<DprModel.Master> dprs)
        {
            dprs.ForEach(t => {
                var cmd = _db.NewCommand("Udma.get_dpr_SubPorjects", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", t.DPChkSlId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<DprModel.SubProject>();
                t.SubProjects = rslt;
            });
        }
        void IncludeInstallment(List<DprModel.Master> dprs)
        {
            dprs.ForEach(t => {
                var cmd = _db.NewCommand("Udma.Get_OldDprInstallment", cn, CommandType.StoredProcedure);
                cmd.Parameters.AddWithValue("@mstDprId", t.DPChkSlId.ReplaceDbNull());
                var rslt = _db.GetResult(cmd).Convert<DprModel.OldDprFundRelease>();
                t.Installments = rslt;
            });
        }
        List<DprModel.OldDprSubProjects> IncludeInstallmentSubProj(long instID, long? DprId)
        {
            var cmd = _db.NewCommand("Udma.Get_SubProjectInstallment", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprID", DprId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@instlmntID", instID);
            var rslt = _db.GetResult(cmd).Convert<DprModel.OldDprSubProjects>().ToList();
            return rslt;
        }
        #endregion

        #region Dpr Dashboard
        public List<DprMinDash> GetDprDashBoardMainOld(DprMinDashPayload dprMinDashPayload)
        {
            MunicipalityBLL bll = new MunicipalityBLL();
            var cmd = _db.NewCommand("udma.Get_DashBoard_dprSubmittedList", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds ?? new List<int>()));
            cmd.Parameters.AddWithValue("@timeSpanFlag", dprMinDashPayload.TimeSpanFlag);
            cmd.Parameters.AddWithValue("@dateForm", dprMinDashPayload.FromDate.ToDbDate());
            cmd.Parameters.AddWithValue("@dateTo", dprMinDashPayload.ToDate.ToDbDate());
            cmd.Parameters.AddWithValue("@authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceDbNull());
            var rlsts = _db.GetResult(cmd).Convert<DprMinDash>();

            var releasedInstallments = GetReleaseStatus<ReleaseStatus2>(rlsts.Select(t => t.DprId).ToList(), dprMinDashPayload.TimeSpanFlag, dprMinDashPayload.FromDate, dprMinDashPayload.ToDate);
            var scemeTypes = GetSchemeTypes();
            var municipalities = bll.GetMunicipalitiesByIds(rlsts.Select(t => t.MunicipalityId).ToArray())
                .Select(t => new KeyValue<int, string>
                {
                    Key = t.MunicipalityID ?? 0,
                    Value = t.MunicipalityName
                });
            rlsts.ForEach(t =>
            {
                t.InstallmentReleases = releasedInstallments.Where(tt => tt.MstDprId == t.DprId).ToList();

                t.Scheme = scemeTypes
                .Where(tt => tt.Id == t.ScemeTypeId)
                .Select(tt => new KeyValue<int, string> { Key = tt.Id, Value = tt.Name })
                .FirstOrDefault();

                t.Municipality = municipalities.FirstOrDefault(tt => tt.Key == t.MunicipalityId);
            });
            return rlsts;
        }
        public DprModel.DprDashboard2.Model_1 GetDprDashBoardMain(DprMinDashPayload dprMinDashPayload)
        {
            var cmd = _db.NewCommand("udma.Get_DashBoard_dprSubmittedListV2", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds ?? new List<int>()));
            cmd.Parameters.AddWithValue("@timeSpanFlag", dprMinDashPayload.TimeSpanFlag);
            cmd.Parameters.AddWithValue("@dateForm", dprMinDashPayload.FromDate.ToDbDate());
            cmd.Parameters.AddWithValue("@dateTo", dprMinDashPayload.ToDate.ToDbDate());
            cmd.Parameters.AddWithValue("@authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
            var dsets = _db.GetResults(cmd);
            var OutObj = new DprModel.DprDashboard2.Model_1();
            OutObj.Submitteds = dsets.Convert<DprModel.DprDashboard2.Submitted>(0);
            OutObj.Returneds = dsets.Convert<DprModel.DprDashboard2.Submitted>(1);
            OutObj.Apporveds = dsets.Convert<DprModel.DprDashboard2.Apporved>(2);
            OutObj.Releaseds = dsets.Convert<DprModel.DprDashboard2.Released>(3);

            return OutObj;
        }
        public DprMinDash GetDprDashBoardDprDetail(long mstDprID,int?municipalityId)
        {
            MunicipalityBLL bll = new MunicipalityBLL();
            var cmd = _db.NewCommand("udma.Get_DashBoard_dprDetail", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@mstDprId", mstDprID);
            var rlsts = _db.GetResult(cmd).Convert<DprMinDash>();

            var releasedInstallments = GetReleaseStatus<ReleaseStatus2>(rlsts.Select(t => t.DprId).ToList());
            var scemeTypes = GetSchemeTypes();
            var municipalities = bll.GetMunicipalitiesByIds(rlsts.Select(t => t.MunicipalityId).ToArray())
                .Select(t => new KeyValue<int, string>
                {
                    Key = t.MunicipalityID ?? 0,
                    Value = t.MunicipalityName
                });
            rlsts.ForEach(t =>
            {
                t.InstallmentReleases = releasedInstallments.Where(tt => tt.MstDprId == t.DprId).ToList();

                t.Scheme = scemeTypes
                .Where(tt => tt.Id == t.ScemeTypeId)
                .Select(tt => new KeyValue<int, string> { Key = tt.Id, Value = tt.Name })
                .FirstOrDefault();

                t.Municipality = municipalities.FirstOrDefault(tt => tt.Key == t.MunicipalityId);
            });
            if (rlsts.Count <= 0)
                throw new Exception("DPR Not found");
            return rlsts[0];
        }

        public OnLoadData GetDashBoardOnLoadData(int? municipality)
        {
            MunicipalityBLL bll = new MunicipalityBLL();
            var obj = new OnLoadData();
            obj.Municipalities = bll.GetAllMunicipalities();
            using(AuthorityBll abll=new AuthorityBll())
            {
                obj.Authorities = abll.GetAuthorities(isActive:true);
            }
            return obj;
        }
        public List<FindProject> FindProject(string projectNo,string status, int? municipalityId)
        {
            var cmd = _db.NewCommand("udma.Get_DashBoard_FindDpr", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MunicipalityId", municipalityId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@ProjectNo", projectNo.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@status", status.ReplaceDbNull());
            var rlsts = _db.GetResult(cmd).Convert<FindProject>();
            return rlsts;
        }
        public List<FindProject> FindProjectByName(string projectName, string status, int? municipalityId)
        {
            var cmd = _db.NewCommand("udma.Get_DashBoard_FindDprByName", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MunicipalityId", municipalityId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@ProjectName", projectName.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@status", status.ReplaceDbNull());
            var rlsts = _db.GetResult(cmd).Convert<FindProject>();
            return rlsts;
        }

        public DprModel.DprDashboard2.Model_1 GetDashboardFinYearWiseData(DprDashPayloadModl1 _payload)
        {
            var payload = new DprMinDashPayload()
            {
                AuthorityTypeId = _payload. AuthorityTypeId,
                FromDate = new FinYearQtr(_payload.FromFinYear).GetFinYearFirstDate(),
                ToDate = new FinYearQtr(_payload.ToFinYear).GetFinYearLastDate(),
                MunicipalityIds = _payload.MunicipalityIds,
                TimeSpanFlag = "date-range",
                SchemeTypeId=_payload.SchemeTypeId,
                SchemeCategoryId=_payload.SchemeCategoryId
            };
            return GetDprDashBoardMain(payload);
        }
        public List<KeyValue<string,decimal>> GetDashboardFinYearExpenditure(DprDashPayloadModl2 _payload)
        {
            MunicipalityBLL bll = new MunicipalityBLL();
            var cmd = _db.NewCommand("udma.Rpt_YearWiseFundReq", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@ULBSid", _payload.MunicipalityIds.Count>0?string.Join(",", _payload.MunicipalityIds) :"ALL");
            cmd.Parameters.AddWithValue("@FinYear", _payload.date.ToDbDate());
            cmd.Parameters.AddWithValue("@schemeTypeId", _payload.SchemeTypeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@schemeCategoryId", _payload.SchemeCategoryId.ReplaceDbNull());
            var rlsts = _db.GetResult(cmd)
                .AsEnumerable()
                .Select(t=>new KeyValue<string, decimal> {
                    Key=t.Field<string>("FinYear"),
                    Value=t.Field<decimal>("FundAmt")
            }).ToList();
            return rlsts;
        }
        #endregion

        #region helper methodsg
        public List<DprModel.Dpr> IncludeFundReleaseStatus(List<DprModel.Dpr> lst, string TimeSpanFlag = null, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            lst = lst ?? new List<DprModel.Dpr>();
            var lstFiltered = lst.Where(t => t.DPChkStatus == "A").ToList();
            if (lstFiltered.Count == 0)
                return lst;
            var cmd = _db.NewCommand("udma.Get_DprFundReleaseStatus", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", string.Join(",", lstFiltered.Select(t => t.DPChkSlId).ToList()));
            cmd.Parameters.AddWithValue("@timeSpanFlag", TimeSpanFlag.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateForm", FromDate.ToDbDate().ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateTo", ToDate.ToDbDate().ReplaceDbNull());
            var rlst = _db.GetResult(cmd).Convert<DprModel.DprFundRelease.FundReleaseStatus>();

            foreach (var x in lst)
            {
                x.FundReleaseStatus = rlst.Where(t => t.MstDprId == x.DPChkSlId).ToList();
            }
            return lst;
        }
        private List<T> GetReleaseStatus<T>(List<long> dprIdList,string TimeSpanFlag=null, DateTime? FromDate=null,DateTime? ToDate=null)
        {
            dprIdList = dprIdList ?? new List<long>();
            var cmd = _db.NewCommand("udma.Get_DprFundReleaseStatus", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@mstDprId", string.Join(",", dprIdList));
            cmd.Parameters.AddWithValue("@timeSpanFlag", TimeSpanFlag.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateForm", FromDate.ToDbDate().ReplaceDbNull());
            cmd.Parameters.AddWithValue("@dateTo", ToDate.ToDbDate().ReplaceDbNull());
            var rlst = _db.GetResult(cmd).Convert<T>();
            return rlst;
        }
        #endregion
    }
}
