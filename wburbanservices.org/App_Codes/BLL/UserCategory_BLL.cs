﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class UserCategory_BLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public UserCategory_BLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<UserCategory> Get_UserType()
        {
            SqlCommand cmd = new SqlCommand("Get_MSTUserType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converter(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        List<UserCategory> Converter(DataTable dt)
        {
            List<UserCategory> lst = new List<UserCategory>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new UserCategory();
                obj.UserTypeID = t.Field<int>("UserTypeID");
                obj.UserType = t.Field<string>("UserType");
                return obj;
            }).ToList();
            return lst;
        }
    }
}