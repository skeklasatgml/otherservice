﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Valuation_Board.App_Codes.BLL
{
    public class MnChangePasswordBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        JsonResult js = new JsonResult();
        public MnChangePasswordBLL()
        {
            cn = new SqlConnection(conString);
        }
        public string Insert(string Old, string New, string RePwd, long userid)
        {
           // string Jsonval="";
            SqlCommand cmd = new SqlCommand("Change_Password", cn);
            cmd.Parameters.AddWithValue("@OldPwd", Old);
            cmd.Parameters.AddWithValue("@NewPwd", New);
            cmd.Parameters.AddWithValue("@RePwd", RePwd);
            cmd.Parameters.AddWithValue("@USERid", userid);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                string val = dt.Rows[0]["DBSTATUS"].ToString();
                return val.ToString();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
            
        }

        internal void Insert(string Old, string New, string RePwd, Models.Account.MunicipalityUser mUser)
        {
            throw new NotImplementedException();
        }
    }
}