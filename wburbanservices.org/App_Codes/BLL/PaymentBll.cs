﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    /// <summary>
    /// 1. Various Searching capability on payments
    /// 2. Payment Cancellation Available
    /// </summary>
    public class PaymentBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public PaymentBll()
        {
            cn = new SqlConnection(conString);
        }
        public List<PaymentSummary> GetPaymentsByAssesseeID(int WardID, int LocationID,
                                                            long assesseeID , int municipalityID,
                                                            DateTime collectionDtFrom, 
                                                            DateTime collectionDtTo, PaymentType paymentType)
        {
            List<PaymentSummary> lst = new List<PaymentSummary>();
            if (paymentType == PaymentType.Offline)
            {
                SqlCommand cmd = new SqlCommand("GET_PaymentDataByAssesseeID", cn);
                cmd.Parameters.AddWithValue("@FromDate", collectionDtFrom);
                cmd.Parameters.AddWithValue("@ToDate", collectionDtTo);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@WardID", WardID);
                cmd.Parameters.AddWithValue("@LocationID", LocationID);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        return ConvertToPaymentSummary(dt); ;
                    }
                    return null;
                }
                catch (SqlException se)
                {
                    throw new Exception("Database Error...!" + se.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
            else {
                throw new ArgumentException("Online Payment Cancelation Not Available");
            }
        }

        public void CancelOfflinePayment(long paymentID,int municipalityID, long assesseeID,List<CancellationReason> CancellationReasons,long UserID ,string remarksText)
        {
            SqlTransaction tr = null;
           
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand("CANCEL_PaymentDtls", cn, tr);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@CanceledBy", UserID);
                cmd.Parameters.AddWithValue("@PaymentID", paymentID);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
                cmd.Parameters.AddWithValue("@CancelText", remarksText);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                CancellationReasons.ForEach(t=> {
                    cmd = new SqlCommand("INSERT_DatCancelPaymentWithReason",cn,tr);
                    cmd.Parameters.AddWithValue("@PaymentID", paymentID);
                    cmd.Parameters.AddWithValue("@ReasonID", t.ReasonID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                });
                tr.Commit();
            }
            catch (SqlException se)
            {
                tr.Rollback();
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<CancellationReason> GetCancelReasonList()
        {

            List<CancellationReason> lst = new List<CancellationReason>();
            SqlCommand cmd = new SqlCommand("GET_CancelReason", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt.AsEnumerable().Select(r =>
                {
                    var reason = new CancellationReason();
                    reason.ReasonID = r.Field<int>("ReasonID");
                    reason.ReasonText = r.Field<string>("ReasonText");
                    return reason;
                }).ToList();
            }
            catch (SqlException se)
            {
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<PaymentSummary> ConvertToPaymentSummary(DataTable dt)
        {
            List<PaymentSummary> lst = new List<PaymentSummary>();
            foreach (DataRow t in dt.Rows)
            {
                PaymentSummary p = new PaymentSummary();
                p.PaymentID = t.Field<long>("PaymentID");
                p.NetAmount = t.Field<decimal>("PaidAmount");
                p.PaymentReceiveDate = t.Field<DateTime>("PaymentReceiveDate");
                p.AssesseeID = t.Field<long>("AssesseeID");
                p.AssesseeName = t.Field<string>("AssesseeName");
                p.HoldingNo = t.Field<string>("HoldingNo");
                p.RecieptID = t.Field<string>("RecieptNo");
                lst.Add(p);
            }
            return lst;
            
        }
        public List<CancelInstrumentDetails> GetInstrumentDetails(int municipalityID, long assesseeID, long paymentID)
        {
            List<CancelInstrumentDetails> lst = new List<CancelInstrumentDetails>();
            SqlCommand cmd = new SqlCommand("Get_PaymentDetailsByID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
            cmd.Parameters.AddWithValue("@PaymentID", paymentID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return ConvertToInstrumentDtls(dt);
                }
                return null;
            }
            catch (SqlException se)
            {
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        private List<CancelInstrumentDetails> ConvertToInstrumentDtls(DataTable dt)
        {
            List<CancelInstrumentDetails> instLst = new List<CancelInstrumentDetails>();
            foreach (DataRow dr in dt.Rows)
            {
                CancelInstrumentDetails ci = new CancelInstrumentDetails();
                ci.PaymentModeDescription = dr.Field<string>("PaymentModeDescription");
                ci.InstrumentNo = dr.Field<string>("InstrumentNo");
                ci.InstrumentDate = dr.Field<string>("InstrumentDate");
                ci.InstrumentAmount = dr.Field<decimal>("InstrumentAmount");
                ci.BankName = dr.Field<string>("BankName");
                ci.PaymentModeCode = dr.Field<string>("PaymentModeCode");
                instLst.Add(ci);
            }
            return instLst;
        }
    }
}