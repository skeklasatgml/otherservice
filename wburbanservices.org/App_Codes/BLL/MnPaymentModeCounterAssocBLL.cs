﻿using System.Configuration;
using System.Data.SqlClient;
using System;
using Valuation_Board.Models.Application;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Valuation_Board.App_Codes.BLL
{
    public class MnPaymentModeCounterAssocBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MnPaymentModeCounterAssocBLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<MunicipalityPaymentCounter> CountersByMunicipalityID(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_CounterByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertToPaymentCounter(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        public List<PaymentMode> GetPaymentModes()
        {
            SqlCommand cmd = new SqlCommand("Get_AllPaymentModes", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertPaymentMode(dt);

            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public MNPaymentModesCounterAssociation SavePaymentModeCounterAssociation(List<MNPaymentModesCounterAssociation> lstobj)
        {
            for (int i = 0; i < lstobj.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("Save_PaymentModes_Counter_Association", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CounterId", lstobj[i].CounterId);
                cmd.Parameters.AddWithValue("@FromDate", lstobj[i].FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@Todate", lstobj[i].Todate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@MunicipalityId", lstobj[i].MunicipalityId);
                cmd.Parameters.AddWithValue("@PaymentModeCode", lstobj[i].PaymentModeCode);
                try
                {
                    cn.Open();
                    var RowId = cmd.ExecuteScalar();
                    lstobj[0].RowId = System.Convert.ToInt32(RowId);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            return lstobj[0];

        }
        public List<MNPaymentModesCounterAssociation> GetPaymentModeCounterWise(int CounterId)
        {
            SqlCommand cmd = new SqlCommand("Get_PaymentModeCounterWise", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CounterId", CounterId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertPaymentModeCounterAssociation(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        List<MunicipalityPaymentCounter> ConvertToPaymentCounter(DataTable dt)
        {
            List<MunicipalityPaymentCounter> lst = new List<MunicipalityPaymentCounter>();
            lst = dt.AsEnumerable().Select(t =>
            {
                //a.CounterID, a.CounterCode,a.IsActive,a.fk_MunicipalityID as MunicipalityID
                MunicipalityPaymentCounter obj = new MunicipalityPaymentCounter();
                obj.CounterID = t.Field<int>("CounterID");
                obj.CounterCode = t.Field<string>("CounterCode");
                obj.IsActive = t.Field<bool>("IsActive");
                obj.MunicipalityID = t.Field<int>("MunicipalityID");
                return obj;
            }).ToList();
            return lst;
        }
        List<PaymentMode> ConvertPaymentMode(DataTable dt)
        {
            List<PaymentMode> lst = new List<PaymentMode>();
            lst = dt.AsEnumerable().Select(t => 
            {
                PaymentMode obj = new PaymentMode();
                obj.PaymentModeCode = t.Field<string>("PaymentModeCode");
                obj.PaymentModeDescription = t.Field<string>("PaymentModeDescription");
                obj.HasInstrument = t.Field<bool>("HasInstrument");
                return obj;
            }).ToList();
            return lst;
        }
        List<MNPaymentModesCounterAssociation> ConvertPaymentModeCounterAssociation(DataTable dt)
        {
            List<MNPaymentModesCounterAssociation> lst = new List<MNPaymentModesCounterAssociation>();
            lst = dt.AsEnumerable().Select(t =>
            {
                MNPaymentModesCounterAssociation obj = new MNPaymentModesCounterAssociation();


                        obj.RowId = t.Field<long>("RowID");
                        obj.CounterId= t.Field<int>("fk_Counter");
                        obj.MunicipalityId = t.Field<int>("fk_MunicipalityID");
                        obj.FromDate = t.Field<DateTime>("DateFrom");
                        obj.Todate = t.Field<DateTime>("DateTo");
                        obj.PaymentModeCode= t.Field<string>("fk_PaymentModeID");
                        obj.PaymentMode = t.Field<string>("PaymentModeDescription");
                     
                return obj;
            }).ToList();
            return lst;
        }
        public List<MNPaymentModesCounterAssociation> IncludesMunicipality(List<MNPaymentModesCounterAssociation> objs)
        {
            MunicipalityBLL mnBll = new MunicipalityBLL();
            List<Municipality> mncplty= mnBll.GetMunicipalitiesByIds(objs.Select( t=> { return t.MunicipalityId; }).ToArray());
            objs.ForEach(t => {
                t.Municipality = mncplty.FirstOrDefault(f => f.MunicipalityID == t.MunicipalityId);
            });
            return objs;
           
        }
        public MNPaymentModesCounterAssociation UpdatePaymentModeCounterAssociation(List<MNPaymentModesCounterAssociation> lstobj)
        {
            for (int i = 0; i < lstobj.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("Update_PaymentModes_Counter_Association", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RowId", lstobj[i].RowId);
                cmd.Parameters.AddWithValue("@CounterId", lstobj[i].CounterId);
                cmd.Parameters.AddWithValue("@FromDate", lstobj[i].FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@Todate", lstobj[i].Todate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@MunicipalityId", lstobj[i].MunicipalityId);
                cmd.Parameters.AddWithValue("@PaymentModeCode", lstobj[i].PaymentModeCode);
                try
                {
                    cn.Open();
                    var RowId = cmd.ExecuteScalar();
                    lstobj[0].RowId = System.Convert.ToInt32(RowId);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            return lstobj[0];

        }
        public int DeletePaymentModes(int RowId)
        {
            int RowID = 0;
            SqlCommand cmd = new SqlCommand("Delete_PaymentModes_Counter_Association", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RowId", RowId);
            try
            {
                cn.Open();
                RowID = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return RowID;
        }
    }
}