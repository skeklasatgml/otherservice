﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class MunicipalityConfigurationBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MunicipalityConfigurationBll()
        {
            cn = new SqlConnection(conString);
        }
        public MunicipalityConfiguration GetConfiguration(int municipalityID)
        {
            try
            {
              List<Municipality> mnLst = new MunicipalityBLL().GetMunicipalitiesByIds(municipalityID);
                MunicipalityConfiguration obj = new MunicipalityConfiguration() {
                    DemandBillConfiguration = new MunicipalityConfiguration.DemandBillConfig(),
                    PaymentConfiguration = new MunicipalityConfiguration.PaymentConfig()
              };
                if (mnLst.Count>0)
                {
                    obj.BasicInfoConfiguration = mnLst.Select(t => new MunicipalityConfiguration.BasicInfoConfig {
                        Address=t.Address,
                        DistrictID=t.DistrictID,
                        Email=t.Email,
                        Mobile=t.Mobile,
                        MunicipalityName=t.MunicipalityName,
                        Fax=t.Fax,
                         WebSite=t.WebSite,
                         TelePhone=t.TelePhone
                    }).ToList()[0];
                    obj.MunicipalityID = mnLst[0].MunicipalityID??0;
                   
                }


                SqlCommand cmd = new SqlCommand("Get_MunicipalityConfigurations", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    DataRow t = dt.Rows[0];
                    obj.PaymentConfiguration.IsAdvanceAutoAdjustableEnabled = t.Field<bool>("IsAdvanceAutoAdjustable");
                    obj.DemandBillConfiguration.DemandBillRebateIntrstText = (t.Table.Columns.Contains("DemandBillCalculatedRateText") ? t.Field<string>("DemandBillCalculatedRateText") : null);
                };
            
                //return default value
                return obj;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public MunicipalityConfiguration GetConfiguration(int municipalityID,long collectorId)
        {
            try
            {
                List<Municipality> mnLst = new MunicipalityBLL().GetMunicipalitiesByIds(municipalityID);
                MunicipalityConfiguration obj = new MunicipalityConfiguration()
                {
                    DemandBillConfiguration = new MunicipalityConfiguration.DemandBillConfig(),
                    PaymentConfiguration = new MunicipalityConfiguration.PaymentConfig(),
                    CollectorAdjustmentConfiguration = new MunicipalityConfiguration.CollectorAdjustmentConfig()
                };
                if (mnLst.Count > 0)
                {
                    obj.BasicInfoConfiguration = mnLst.Select(t => new MunicipalityConfiguration.BasicInfoConfig
                    {
                        Address = t.Address,
                        DistrictID = t.DistrictID,
                        Email = t.Email,
                        Mobile = t.Mobile,
                        MunicipalityName = t.MunicipalityName,
                        Fax = t.Fax,
                        WebSite = t.WebSite,
                        TelePhone = t.TelePhone
                    }).ToList()[0];
                    obj.MunicipalityID = mnLst[0].MunicipalityID ?? 0;

                }


                SqlCommand cmd = new SqlCommand("Get_MunicipalityConfigurations", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    DataRow t = dt.Rows[0];
                    obj.PaymentConfiguration.IsAdvanceAutoAdjustableEnabled = t.Field<bool>("IsAdvanceAutoAdjustable");
                    obj.DemandBillConfiguration.DemandBillRebateIntrstText = (t.Table.Columns.Contains("DemandBillCalculatedRateText") ? t.Field<string>("DemandBillCalculatedRateText") : null);
                    obj.CollectorAdjustmentConfiguration.IsActiveForMunicipality = t.Field<bool>("IsActiveCollctrAdjstmnt");
                    obj.CollectorAdjustmentConfiguration.MaxAdjustableAmount = t.Field<decimal>("CollctrAdjstableMaxAmount");
                    obj.CollectorAdjustmentConfiguration.CollctrAdjustmentType = t.Field<string>("CollctrAdjustmentType");
                    obj.CollectorAdjustmentConfiguration.CollctrAdjustmentTypeDescription = t.Field<string>("CollctrAdjustmentTypeDescription");

                    cmd = new SqlCommand("get_MunicipalityConfig_Collector", cn);
                    cmd.Parameters.AddWithValue("@CollectorId", collectorId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        obj.CollectorAdjustmentConfiguration.IsEnabledCollectorAdjustmentOnUser = dt.Rows[0].Field<bool>("IsEnabledCollectorAdjustmentOnUser");
                    }
                };

                //return default value
                return obj;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void SaveConfiguration(MunicipalityConfiguration.DemandBillConfig DemandBillConfig,int municipalityId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Save_MunicipalityDemandBillConfig", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@DemandBillCalculatedRateText", DemandBillConfig.DemandBillRebateIntrstText?? DBNull.Value as object);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch { throw; }
            finally{ cn.Close(); }
           
        }

        public void SaveConfiguration(MunicipalityConfiguration.PaymentConfig PaymentConfig, int municipalityId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Save_MunicipalityPaymentConfig", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@IsAdvanceAutoAdjustable", PaymentConfig.IsAdvanceAutoAdjustableEnabled);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch { throw; }
            finally { cn.Close(); }

        }

        public void SaveConfiguration(MunicipalityConfiguration.BasicInfoConfig BasicInfoConfig, int municipalityId,long userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Update_MunicipalityBasicInfo", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId);
                cmd.Parameters.AddWithValue("@MunicipalityName", BasicInfoConfig.MunicipalityName??DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Address", BasicInfoConfig.Address ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Email", BasicInfoConfig.Email ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Mobile", BasicInfoConfig.Mobile ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@website", BasicInfoConfig.WebSite ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Fax", BasicInfoConfig.Fax ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@telephone", BasicInfoConfig.TelePhone ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch { throw; }
            finally { cn.Close(); }

        }

    }
}