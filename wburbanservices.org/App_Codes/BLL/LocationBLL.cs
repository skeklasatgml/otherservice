﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class LocationBLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public LocationBLL()
        {
            cn = new SqlConnection(constring);
        }
        /// <summary>
        /// Get Locations filtered by locaitonName,municipalityID and wardID
        /// </summary>
        /// <param name="locationName"></param>
        /// <param name="municipalityID"></param>
        /// <param name="wardID"></param>
        /// <returns></returns>
        public List<Location> GetLocations(string locationName, int municipalityID, int wardID)
        {

            SqlCommand cmd = new SqlCommand("Get_LocationByNameMncpltyIdWardID", cn);
            cmd.Parameters.AddWithValue("@LocationName", locationName);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@WardID", wardID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Location> GetLocationsIDRange(int wardID, int municipalityID,List<int> locationIDs)
        {
            if (locationIDs == null || locationIDs.Count == 0)
            {
                return new List<Location>();
            }
            try
            {
                string commaSeparatedNums = locationIDs.Select(b => b.ToString()).Aggregate<string>((items, item) => items + "," + item);

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "Get_LocationsByIdRange";
                cmd.Parameters.AddWithValue("@IdRange", commaSeparatedNums);
                cmd.Parameters.AddWithValue("@Delimiter", ",");
                cmd.Parameters.AddWithValue("@WardID", wardID);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    sda.Fill(dt);
                    return Convert(dt);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            catch (SqlException)
            {
                throw new Exception("Database Error");
            }
        }
        #region
        List<Location> Convert(DataTable dt)
        {
            List<Location> lst = new List<Location>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Location();
                obj.LocationID = t.Field<int?>("LocationID");
                obj.LocationName = t.Field<string>("LocationName");
                obj.WardID = t.Field<int?>("WardID");
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
        #region
        public List<Location> IncludeWard(List<Location> locations)
        {
            throw new NotImplementedException();
        }
        public Location IncludeWard(Location location)
        {
            throw new NotImplementedException();
        }
        public List<Location> IncludeMunicipality(List<Location> locations)
        {
            throw new NotImplementedException();
        }
        public Location IncludeMunicipality(Location location)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}