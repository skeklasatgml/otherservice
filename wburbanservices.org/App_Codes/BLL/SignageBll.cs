﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using static Valuation_Board.ViewModels.Assessment_VM;

namespace Valuation_Board.App_Codes.BLL
{
    public class SignageBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        DbHandler _db;
        SqlConnection cn;
        public SignageBll()
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public List<SignageMD.Surface> GetTypeofSurface(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SURFACE_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Surface(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.RoadType> GetTypeofRoad(string SurfaceID, int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SurfaceID", SurfaceID);
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_ROAD_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_TypeofRoad(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public string GetApplicantName(long UserID, int UserType, int? ApplicationId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (UserType == 4)
            {
                cmd.Parameters.AddWithValue("@ApplicantID", UserID);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS");
            }
            else if (UserType == 2)
            {
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS_MUNICIPALITY");
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string NAME = dt.Rows[0]["NAME"].ToString();
                    string DESIGNATION = dt.Rows[0]["DESIGNATION"].ToString();
                    string COMPANYNAME = dt.Rows[0]["COMPANYNAME"].ToString();
                    string MOBILENO = dt.Rows[0]["MOBILENO"].ToString();

                    return NAME + ", " + DESIGNATION + ", " + COMPANYNAME + ", " + MOBILENO;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Municipality> GetMunicipality()
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_MUNICIPALITY");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Municipality(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Purposes> GetPurpose(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_PURPOSE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Purposes(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Specification> GetSpecification(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATION");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Specification> GetSpecificationbyID(string SpecificationID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SpecificationID", SpecificationID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATIONBYID");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public Boolean? GetModuleDetail(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SUBMODULE_DETAILS");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Boolean ISDetailMultiple = Convert.ToBoolean(dt.Rows[0]["ISDetailMultiple"].ToString());
                    return ISDetailMultiple;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.UploadDetail> GetUploadDetail(int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_UPLOAD_DETAILS");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_UploadDetail(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Ward> GetWard(string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Ward(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Location> GetLocationByWard(string WardID, string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_LOCATION_BY_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Location(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<SignageMD.Holding> GetHoldingNo(string WardID, string MunicipalityID, string LocationID, string Desc)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@LocationID", LocationID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_HOLDING");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Holding(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converters
        public List<SignageMD.Surface> Convert_Surface(DataTable dt)
        {
            List<SignageMD.Surface> lst = new List<SignageMD.Surface>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Surface();
                obj.SurfaceID = t.Field<int?>("SurfaceID");
                obj.SurfaceName = t.Field<string>("SurfaceName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.RoadType> Convert_TypeofRoad(DataTable dt)
        {
            List<SignageMD.RoadType> lst = new List<SignageMD.RoadType>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.RoadType();
                obj.RoadTypeID = t.Field<int?>("RoadTypeID");
                obj.RoadTypeName = t.Field<string>("RoadTypeName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Municipality> Convert_Municipality(DataTable dt)
        {
            List<SignageMD.Municipality> lst = new List<SignageMD.Municipality>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Municipality();
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.MunicipalityName = t.Field<string>("MunicipalityName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Purposes> Convert_Purposes(DataTable dt)
        {
            List<SignageMD.Purposes> lst = new List<SignageMD.Purposes>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Purposes();
                obj.TypeID = t.Field<int?>("TypeID");
                obj.Purpose = t.Field<string>("Purpose");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Specification> Convert_Specification(DataTable dt)
        {
            List<SignageMD.Specification> lst = new List<SignageMD.Specification>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Specification();
                obj.ID = t.Field<long?>("ID");
                obj.Description = t.Field<string>("Description");
                obj.SpecificationRWidth = t.Field<Boolean?>("SpecificationRWidth");
                obj.SpecificationRDepth = t.Field<Boolean?>("SpecificationRDepth");
                obj.SpecificationPNo = t.Field<Boolean?>("SpecificationPNo");
                obj.SpecificationPLength = t.Field<Boolean?>("SpecificationPLength");
                obj.SpecificationPWidth = t.Field<Boolean?>("SpecificationPWidth");
                obj.SpecificationPDepth = t.Field<Boolean?>("SpecificationPDepth");
                obj.SpecificationTDiameter = t.Field<Boolean?>("SpecificationTDiameter");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.UploadDetail> Convert_UploadDetail(DataTable dt)
        {
            List<SignageMD.UploadDetail> lst = new List<SignageMD.UploadDetail>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.UploadDetail();
                obj.DocTypeID = t.Field<int?>("DocTypeID");
                obj.DocName = t.Field<string>("DocName");
                obj.ExtensionType = t.Field<string>("ExtensionType");
                obj.maxQuantity = t.Field<int?>("maxQuantity");
                obj.isMandatory = t.Field<int?>("isMandatory");
                obj.MaxSize = t.Field<decimal?>("MaxSize");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Ward> Convert_Ward(DataTable dt)
        {
            List<SignageMD.Ward> lst = new List<SignageMD.Ward>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Ward();
                obj.WardID = t.Field<int?>("WardID");
                obj.WardName = t.Field<string>("WardName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Location> Convert_Location(DataTable dt)
        {
            List<SignageMD.Location> lst = new List<SignageMD.Location>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Location();
                obj.LocationID = t.Field<int?>("LocationID");
                obj.LocationName = t.Field<string>("LocationName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<SignageMD.Holding> Convert_Holding(DataTable dt)
        {
            List<SignageMD.Holding> lst = new List<SignageMD.Holding>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SignageMD.Holding();
                obj.AssesseeID = t.Field<long?>("AssesseeID");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}