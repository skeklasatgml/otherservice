﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class PropertyTaxCollectionBLL
    {
        protected string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        protected SqlConnection cn;
        public PropertyTaxCollectionBLL()
        {
            cn = new SqlConnection(constring);
        }
        /// <summary>
        /// Over Load:1
        /// </summary>
        /// <param name="propertyTaxMaster"></param>
        /// <param name="paymentModes"></param>
        /// <param name="isPaymentSuccessfull"></param>
        /// <returns></returns>
        //[Obsolete("PayPropertyTax(), Overload 1 is deprecated, please use Overload 2.", true)]
        //public long? PayPropertyTax(PropertyTaxPaymentMaster propertyTaxMaster, List<PaymentMode> paymentModes, bool isPaymentSuccessfull)
        //{
        //    SqlTransaction tr = null;
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = cn;
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        cn.Open();
        //        tr = cn.BeginTransaction();
        //        cmd.Transaction = tr;
        //        cmd.CommandText = "Insert_PayPropertyTaxMaster";
        //        cmd.Parameters.AddWithValue("@PaymentType", propertyTaxMaster.PaymentType ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@AssesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@MunicipalityID", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@BillID", propertyTaxMaster.BillID ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@PaymentCollectionDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@PaymentReceiveDate", DateTime.Now.ToString("yyyy'-'MM'-'dd  HH:mm:ss") ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@NetAmount", propertyTaxMaster.NetAmount ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@RoundOffAdjust", propertyTaxMaster.RoundOffAdjust ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@PayMode", DBNull.Value);
        //        cmd.Parameters.AddWithValue("@InstrumentNo", DBNull.Value);
        //        cmd.Parameters.AddWithValue("@InstrumentDate", DBNull.Value);
        //        cmd.Parameters.AddWithValue("@PaidAmount", propertyTaxMaster.PaidAmount ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@counterID", getCounter(DateTime.Now, propertyTaxMaster.MunicipalityID ?? 0, propertyTaxMaster.InsertedBy ?? 0).CounterID);
        //        cmd.Parameters.AddWithValue("@CollectorID", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@TransactionID", propertyTaxMaster.TransactionID ?? DBNull.Value as object);

        //        cmd.Parameters.AddWithValue("@GrossDtlAmount", propertyTaxMaster.GrossAmount);
        //        cmd.Parameters.AddWithValue("@AdjustedAmount", propertyTaxMaster.AdjustedAmount);
        //        cmd.Parameters.AddWithValue("@NoticeServedOn", propertyTaxMaster.NoticeServedDate != null ? propertyTaxMaster.NoticeServedDate.Value.ToString("yyyy'-'MM'-'dd") : DBNull.Value as object);
        //        cmd.Parameters.AddWithValue("@TotalCalculatedAmnt", propertyTaxMaster.TotalCalculatedAmnt);
        //        cmd.Parameters.AddWithValue("@EffectiveQtrFrom", propertyTaxMaster.EffectiveQtrFrom ?? DBNull.Value as object);
        //        var mstPaymentID = cmd.ExecuteScalar();
        //        foreach (PropertyTaxPaymentDetail item in propertyTaxMaster.PropertyTaxPaymentDetails)
        //        {
        //            cmd = new SqlCommand("Insert_PayPropertyTaxDetail", cn, tr);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
        //            cmd.Parameters.AddWithValue("@BillDemandType", item.PayArrrearBill ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@BillDemandID", item.DemandTypeID ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@QtrNo", item.QtrNo ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@TaxValue", item.TaxValue ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@SurchargeValue", item.SurchargeValue ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@CalculatedValue", item.CalculatedValue ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@PaidTaxAmt", item.PaidTaxAmt ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@PaidSurchargeAmt", item.PaidSurchargeAmt ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@PaidCalculatedAmt", item.PaidCalculatedAmt ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@payHeadID", item.PayHeadTypeID ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@FinYear", item.FinYear ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@LineTotalRoundOff", item.LineTotalRoundOff);
        //            cmd.Parameters.AddWithValue("@LineTotal", item.LineTotal);
        //            cmd.ExecuteNonQuery();

        //        }
        //        paymentModes.ForEach(t =>
        //        {
        //            Func<PaymentMode, decimal> getInstrumentAmount = (_paymentMode) =>
        //            {
        //                decimal totalAmount = 0;
        //                if (_paymentMode.HasInstrument == false)
        //                {
        //                    totalAmount = _paymentMode.TotalAmount;
        //                }
        //                else
        //                {
        //                    _paymentMode.PaymentInstruments.ForEach(i =>
        //                    {
        //                        totalAmount += i.InstrumentAmount;
        //                    });
        //                }

        //                return totalAmount;
        //            };
        //            cmd = new SqlCommand("Insert_Dat_PaymentModes", cn, tr);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.AddWithValue("@PaymentModeCode", t.PaymentModeCode);
        //            cmd.Parameters.AddWithValue("@ModeAmount", getInstrumentAmount(t));
        //            cmd.Parameters.AddWithValue("@MstPaymentID", mstPaymentID);
        //            object datPaymentModeID = cmd.ExecuteScalar();

        //            t.PaymentInstruments.ForEach(i =>
        //            {
        //                cmd = new SqlCommand("Insert_Dat_PaymentInstrument", cn, tr);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@Dat_PaymentModeID", datPaymentModeID);

        //                if (i.ModeType == 1)//cash
        //                {
        //                    throw new InvalidOperationException("Payment mode type can't be inserted on Dat_PaymentInstrument table");
        //                }
        //                else if (i.ModeType == 2)//DD,N,Q,T
        //                {
        //                    var obj = i as PaymentInstrument;
        //                    cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
        //                    cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
        //                    cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@BankID", (obj.BankCode ?? DBNull.Value as object));
        //                }
        //                else if (i.ModeType == 3)
        //                {//online
        //                    throw new InvalidOperationException("Invalid Payment mode");
        //                }
        //                else if (i.ModeType == 4)
        //                {//fund transfer
        //                    var obj = i as PaymentInsturmentType4;
        //                    cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
        //                    cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
        //                    cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@FromAccountNo", (obj.FromAccountNo ?? DBNull.Value as object));
        //                }
        //                else if (i.ModeType == 5)
        //                {//credit and debit card
        //                    var obj = i as PaymentInsturmentType5;
        //                    cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
        //                    cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
        //                    cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
        //                    cmd.Parameters.AddWithValue("@CardNo", (obj.CardNo ?? DBNull.Value as object));
        //                }

        //                cmd.ExecuteNonQuery();
        //            });
        //        });
        //        if (isPaymentSuccessfull == true)
        //        {
        //            SetPaymentStatus(tr, cn, (long?)mstPaymentID, isPaymentSuccessfull);
        //        }

        //        propertyTaxMaster.AdjustmentSummaries.ForEach(t =>
        //        {
        //            cmd = new SqlCommand("Insert_DatPayheadMaster", cn, tr);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
        //            cmd.Parameters.AddWithValue("@PayHeadID", t.MstPayHeadID);
        //            cmd.Parameters.AddWithValue("@PayHeadValue", t.AdjustedAmount);
        //            cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
        //            cmd.Parameters.AddWithValue("@MstAdjustmentID", t.MstAdjID);
        //            cmd.ExecuteNonQuery();
        //        });
        //        tr.Commit();
        //        return (long?)mstPaymentID;
        //    }
        //    catch (InvalidOperationException ex)
        //    {
        //        tr.Rollback();
        //        throw new InvalidOperationException(ex.Message);
        //    }
        //    catch (SqlException ex)
        //    {
        //        tr.Rollback();
        //        throw new InvalidOperationException("Database Error: " + ex.Message);
        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }
        //}
        /// <summary>
        /// Over Load:2
        /// Advance payment available
        /// </summary>
        /// <param name="propertyTaxMaster"></param>
        /// <param name="paymentModes"></param>
        /// <param name="isPaymentSuccessfull"></param>
        /// <param name="advanceAmount"></param>
        /// <returns></returns>
        public long? PayPropertyTax(PropertyTaxPaymentMaster propertyTaxMaster, List<PaymentMode> paymentModes, bool isPaymentSuccessfull)
        {
            SqlTransaction tr = null;
            SqlCommand cmd = new SqlCommand();
            
            cmd.Connection = cn;
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                cmd.Transaction = tr;
                cmd.CommandText = "Insert_PayPropertyTaxMaster";
                cmd.Parameters.AddWithValue("@PaymentType", propertyTaxMaster.PaymentType ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityID", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@BillID", propertyTaxMaster.BillID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentCollectionDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentReceiveDate", DateTime.Now.ToString("yyyy'-'MM'-'dd  HH:mm:ss") ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@NetAmount", propertyTaxMaster.NetAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@RoundOffAdjust", propertyTaxMaster.RoundOffAdjust ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PayMode", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentDate", DBNull.Value);
                cmd.Parameters.AddWithValue("@PaidAmount", propertyTaxMaster.PaidAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@counterID", getCounter(DateTime.Now, propertyTaxMaster.MunicipalityID ?? 0, propertyTaxMaster.InsertedBy ?? 0).CounterID);
                cmd.Parameters.AddWithValue("@CollectorID", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TransactionID", propertyTaxMaster.TransactionID ?? DBNull.Value as object);

                cmd.Parameters.AddWithValue("@GrossDtlAmount", propertyTaxMaster.GrossAmount);
                cmd.Parameters.AddWithValue("@AdjustedAmount", propertyTaxMaster.AdjustedAmount);
                cmd.Parameters.AddWithValue("@NoticeServedOn", propertyTaxMaster.NoticeServedDate != null ? propertyTaxMaster.NoticeServedDate.Value.ToString("yyyy'-'MM'-'dd") : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TotalCalculatedAmnt", propertyTaxMaster.TotalCalculatedAmnt);
                cmd.Parameters.AddWithValue("@EffectiveQtrFrom", propertyTaxMaster.EffectiveQtrFrom ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AdvanceAmount", propertyTaxMaster.AdvanceAmount);
                cmd.Parameters.AddWithValue("@CollectorAdjustedAmount", propertyTaxMaster.CollectorAdjustmentAmount);
                cmd.Parameters.AddWithValue("@BillReceiptNo", (string.IsNullOrEmpty(propertyTaxMaster.BillReciptNo)? DBNull.Value as object : propertyTaxMaster.BillReciptNo) );

                var mstPaymentID = cmd.ExecuteScalar();//@AdvanceAmount

                //insert into Advance details table
                long? datAdvancePaymentID = null;
                if (propertyTaxMaster.AdvanceAmount != 0)
                {
                    cmd = new SqlCommand("Insert_AdvancePayment", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@municipalityId", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@assesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@mstPaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@advanceDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@advanceAmount", propertyTaxMaster.AdvanceAmount);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        datAdvancePaymentID = dt.Rows[0].Field<long?>("AdvPaymentID");
                    }
                }
                foreach (PropertyTaxPaymentDetail item in propertyTaxMaster.PropertyTaxPaymentDetails)
                {
                    cmd = new SqlCommand("Insert_PayPropertyTaxDetail", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@BillDemandType", item.PayArrrearBill ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@BillDemandID", item.DemandTypeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@QtrNo", item.QtrNo ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@TaxValue", item.TaxValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@SurchargeValue", item.SurchargeValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@CalculatedValue", item.CalculatedValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidTaxAmt", item.PaidTaxAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidSurchargeAmt", item.PaidSurchargeAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidCalculatedAmt", item.PaidCalculatedAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@payHeadID", item.PayHeadTypeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@FinYear", item.FinYear ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@LineTotalRoundOff", item.LineTotalRoundOff);
                    cmd.Parameters.AddWithValue("@LineTotal", item.LineTotal);

                    cmd.ExecuteNonQuery();

                }
                paymentModes.ForEach(t =>
                {
                    Func<PaymentMode, decimal> getInstrumentAmount = (_paymentMode) =>
                    {
                        decimal totalAmount = 0;
                        if (_paymentMode.HasInstrument == false)
                        {
                            totalAmount += _paymentMode.TotalAmount;
                        }
                        else
                        {
                            _paymentMode.PaymentInstruments.ForEach(i =>
                            {
                                totalAmount += i.InstrumentAmount;
                            });
                        }

                        return totalAmount;
                    };
                    cmd = new SqlCommand("Insert_Dat_PaymentModes", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@PaymentModeCode", t.PaymentModeCode);
                    cmd.Parameters.AddWithValue("@ModeAmount", getInstrumentAmount(t));
                    cmd.Parameters.AddWithValue("@MstPaymentID", mstPaymentID);
                    object datPaymentModeID = cmd.ExecuteScalar();

                    t.PaymentInstruments.ForEach(i =>
                    {
                        cmd = new SqlCommand("Insert_Dat_PaymentInstrument", cn, tr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Dat_PaymentModeID", datPaymentModeID);

                        if (i.ModeType == 1)//cash
                        {
                            throw new InvalidOperationException("Payment mode type can't be inserted on Dat_PaymentInstrument table");
                        }
                        else if (i.ModeType == 2)//DD,N,Q,T
                        {
                            var obj = i as PaymentInstrument;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@BankID", (obj.BankCode ?? DBNull.Value as object));
                        }
                        else if (i.ModeType == 3)
                        {//online
                            throw new InvalidOperationException("Invalid Payment mode");
                        }
                        else if (i.ModeType == 4)
                        {//fund transfer
                            var obj = i as PaymentInsturmentType4;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@FromAccountNo", (obj.FromAccountNo ?? DBNull.Value as object));
                        }
                        else if (i.ModeType == 5)
                        {//credit and debit card
                            var obj = i as PaymentInsturmentType5;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@CardNo", (obj.CardNo ?? DBNull.Value as object));
                        }

                        cmd.ExecuteNonQuery();
                    });
                });
                if (isPaymentSuccessfull == true)
                {
                    SetPaymentStatus(tr, cn, (long?)mstPaymentID, isPaymentSuccessfull);
                }

                propertyTaxMaster.AdjustmentSummaries.ForEach(t =>
                {
                    cmd = new SqlCommand("Insert_DatPayheadMaster", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@PayHeadID", t.MstPayHeadID);
                    cmd.Parameters.AddWithValue("@PayHeadValue", t.AdjustedAmount);
                    cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@MstAdjustmentID", t.MstAdjID);
                    cmd.ExecuteNonQuery();
                });
                tr.Commit();
                return (long?)mstPaymentID;
            }
            catch (InvalidOperationException ex)
            {
                tr.Rollback();
                throw new InvalidOperationException(ex.Message);
            }
            catch (SqlException ex)
            {
                tr.Rollback();
                throw new InvalidOperationException("Database Error: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public bool IsFinyearQtrsPaidWithAdvanceAdjustment(List<FinYearQtr> finYearQtrs,long assesseeId)
        {
            //all the qtrs must be paid with advance adjustment and must be with in same date
            var flag = true;
            var currDate = DateTime.Now.Date;
            try
            {
                string formatParameter = string.Join("|", finYearQtrs.Select(t => t.ToString()).ToList<string>()); //2012-2013,1|2012-2013,2|2012-2013,3|2012-2013,4
                cn.Open();
                SqlCommand cmd = new SqlCommand("Get_HowFinyearQtrIsPaid", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@lstFinyearQtr", formatParameter ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@assesseeId", assesseeId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                foreach (var t in finYearQtrs)
                {
                    if (dt.AsEnumerable().FirstOrDefault(tt => tt.Field<int>("qtr") == t.Qtr && tt.Field<string>("finYear") == t.GetFinYear()&&tt.Field<DateTime>("PaymentCollectionDate").Date==currDate) == null)
                    {
                        flag = false;
                        break;
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return flag;
        }
        public bool IsPaymentExist(DateTime paymentDate,long assesseeId)
        {
            var flag = true;
            var currDate = DateTime.Now.Date;
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Get_IsPaymentExist", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@paymentDate", paymentDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@assesseeId", assesseeId);
                flag= Convert.ToBoolean(cmd.ExecuteScalar());              
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return flag;
        }
        public long MakeAdvancePayment( decimal advanceAmount, List<PaymentMode> paymentModes, PropertyTaxPaymentMaster propertyTaxMaster)
        {
            SqlTransaction tr = null;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.StoredProcedure;
            long mstPaymentID = 0;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                cmd.Transaction = tr;
                cmd.CommandText = "Insert_PayPropertyTaxMaster";
                cmd.Parameters.AddWithValue("@PaymentType", propertyTaxMaster.PaymentType ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityID", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@BillID", propertyTaxMaster.BillID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentCollectionDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentReceiveDate", DateTime.Now.ToString("yyyy'-'MM'-'dd  HH:mm:ss") ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@NetAmount", propertyTaxMaster.NetAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@RoundOffAdjust", propertyTaxMaster.RoundOffAdjust ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PayMode", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentDate", DBNull.Value);
                cmd.Parameters.AddWithValue("@PaidAmount", propertyTaxMaster.PaidAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@counterID", propertyTaxMaster.CounterID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@CollectorID", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TransactionID", propertyTaxMaster.TransactionID ?? DBNull.Value as object);

                cmd.Parameters.AddWithValue("@GrossDtlAmount", propertyTaxMaster.GrossAmount );
                cmd.Parameters.AddWithValue("@AdjustedAmount", propertyTaxMaster.AdjustedAmount);
                cmd.Parameters.AddWithValue("@NoticeServedOn", propertyTaxMaster.NoticeServedDate ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TotalCalculatedAmnt", propertyTaxMaster.TotalCalculatedAmnt);
                cmd.Parameters.AddWithValue("@EffectiveQtrFrom", propertyTaxMaster.EffectiveQtrFrom ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AdvanceAmount", advanceAmount);

                mstPaymentID = (long)cmd.ExecuteScalar();//@AdvanceAmount

                paymentModes.ForEach(t =>
                {
                    Func<PaymentMode, decimal> getInstrumentAmount = (_paymentMode) =>
                    {
                        decimal totalAmount = 0;
                        if (_paymentMode.HasInstrument == false)
                        {
                            totalAmount += _paymentMode.TotalAmount;
                        }
                        else
                        {
                            _paymentMode.PaymentInstruments.ForEach(i =>
                            {
                                totalAmount += i.InstrumentAmount;
                            });
                        }
                        return totalAmount;
                    };
                    cmd = new SqlCommand("Insert_Dat_PaymentModes", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentModeCode", t.PaymentModeCode);
                    cmd.Parameters.AddWithValue("@ModeAmount", getInstrumentAmount(t));
                    cmd.Parameters.AddWithValue("@MstPaymentID", mstPaymentID);
                    object datPaymentModeID = cmd.ExecuteScalar();

                    t.PaymentInstruments.ForEach(i =>
                    {
                        cmd = new SqlCommand("Insert_Dat_PaymentInstrument", cn, tr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Dat_PaymentModeID", datPaymentModeID);

                        if (i.ModeType == 1)//cash
                        {
                            throw new InvalidOperationException("Payment mode type can't be inserted on Dat_PaymentInstrument table");
                        }
                        else if (i.ModeType == 2)//DD,N,Q,T
                        {
                            var obj = i as PaymentInstrument;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@BankID", (obj.BankCode ?? DBNull.Value as object));
                        }
                        else if (i.ModeType == 3)
                        {//online
                            throw new InvalidOperationException("Invalid Payment mode");
                        }
                        else if (i.ModeType == 4)
                        {//fund transfer
                            var obj = i as PaymentInsturmentType4;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@FromAccountNo", (obj.FromAccountNo ?? DBNull.Value as object));
                        }
                        else if (i.ModeType == 5)
                        {//credit and debit card
                            var obj = i as PaymentInsturmentType5;
                            cmd.Parameters.AddWithValue("@InstrumentNo", (obj.InstrumentNo ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@InstrumentDate", obj.InstrumentDate.ToString("yyyy'-'MM'-'dd"));
                            cmd.Parameters.AddWithValue("@InstrumentAmount", obj.InstrumentAmount);
                            cmd.Parameters.AddWithValue("@BankName", (obj.BankName ?? DBNull.Value as object));
                            cmd.Parameters.AddWithValue("@CardNo", (obj.CardNo ?? DBNull.Value as object));
                        }

                        cmd.ExecuteNonQuery();
                    });
                });
                SetPaymentStatus(tr, cn, (long?)mstPaymentID, true);
                if (advanceAmount != 0)
                {
                    cmd = new SqlCommand("Insert_AdvancePayment", cn, tr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@municipalityId", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@assesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@mstPaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@advanceDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@advanceAmount", advanceAmount);

                    cmd.ExecuteNonQuery();
                }
                tr.Commit();
            }
            catch (SqlException)
            {
                tr.Rollback();
                throw;
            }
            return mstPaymentID;
        }

        public long? PayPropertyTaxOnline(PropertyTaxPaymentMaster propertyTaxMaster, bool isPaymentSuccessfull, string pgCode)
        {
            // SqlTransaction tr = null;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //get counter default
                Func<DateTime, int, long, int?> GetCounterID = (date, municipalityID, userID) =>
                {
                    try
                    {
                        MunicipalityPaymentCounter obj = getCounter(date, municipalityID, userID);
                        if (obj != null)
                        {
                            return obj.CounterID;
                        }
                        return null;
                    }
                    catch
                    {
                        return null;
                    }
                };

                cn.Open();
                //tr = cn.BeginTransaction();
                //cmd.Transaction = tr;
                cmd.CommandText = "Insert_PayPropertyTaxMaster";
                cmd.Parameters.AddWithValue("@PaymentType", propertyTaxMaster.PaymentType ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeID", propertyTaxMaster.AssesseeID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityID", propertyTaxMaster.MunicipalityID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@BillID", propertyTaxMaster.BillID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentCollectionDate", propertyTaxMaster.PaymentCollectionDate ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PaymentReceiveDate", DateTime.Now.ToString("yyyy'-'MM'-'dd  HH:mm:ss") ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@NetAmount", propertyTaxMaster.NetAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@RoundOffAdjust", propertyTaxMaster.RoundOffAdjust ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PayMode", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@InstrumentDate", DBNull.Value);
                cmd.Parameters.AddWithValue("@PaidAmount", propertyTaxMaster.PaidAmount ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@counterID", DBNull.Value as object);
                cmd.Parameters.AddWithValue("@CollectorID", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TransactionID", propertyTaxMaster.TransactionID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PgCode", pgCode);
                cmd.Parameters.AddWithValue("@appOrigin", "WEB");

                cmd.Parameters.AddWithValue("@GrossDtlAmount", propertyTaxMaster.GrossAmount);
                cmd.Parameters.AddWithValue("@AdjustedAmount", propertyTaxMaster.AdjustedAmount);
                cmd.Parameters.AddWithValue("@NoticeServedOn", propertyTaxMaster.NoticeServedDate != null ? propertyTaxMaster.NoticeServedDate.Value.ToString("yyyy'-'MM'-'dd") : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@TotalCalculatedAmnt", propertyTaxMaster.TotalCalculatedAmnt);
                cmd.Parameters.AddWithValue("@EffectiveQtrFrom", propertyTaxMaster.EffectiveQtrFrom ?? DBNull.Value as object);
                var mstPaymentID = cmd.ExecuteScalar();
                foreach (PropertyTaxPaymentDetail item in propertyTaxMaster.PropertyTaxPaymentDetails)
                {
                    cmd = new SqlCommand("Insert_PayPropertyTaxDetail", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@BillDemandType", item.PayArrrearBill ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@BillDemandID", item.DemandTypeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@QtrNo", item.QtrNo ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@TaxValue", item.TaxValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@SurchargeValue", item.SurchargeValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@CalculatedValue", item.CalculatedValue ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidTaxAmt", item.PaidTaxAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidSurchargeAmt", item.PaidSurchargeAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@PaidCalculatedAmt", item.PaidCalculatedAmt ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@payHeadID", item.PayHeadTypeID ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@FinYear", item.FinYear ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@LineTotalRoundOff", item.LineTotalRoundOff);
                    cmd.Parameters.AddWithValue("@LineTotal", item.LineTotal);
                    cmd.ExecuteNonQuery();

                }
                propertyTaxMaster.AdjustmentSummaries.ForEach(t =>
                {
                    cmd = new SqlCommand("Insert_DatPayheadMaster", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentID", mstPaymentID);
                    cmd.Parameters.AddWithValue("@PayHeadID", t.MstPayHeadID);
                    cmd.Parameters.AddWithValue("@PayHeadValue", t.AdjustedAmount);
                    cmd.Parameters.AddWithValue("@InsertedBy", propertyTaxMaster.InsertedBy ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@MstAdjustmentID", t.MstAdjID);
                    cmd.ExecuteNonQuery();
                });
                //tr.Commit();
                return (long?)mstPaymentID;
            }
            catch (InvalidOperationException ex)
            {
                //tr.Rollback();
                throw new InvalidOperationException(ex.Message);
            }
            catch (SqlException ex)
            {
                // tr.Rollback();
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        protected MunicipalityPaymentCounter getCounter(DateTime date, int municipalityID, long userID)
        {
            MunicipalityTaxPaymentCounter_BLL objBll = new MunicipalityTaxPaymentCounter_BLL();
            MunicipalityPaymentCounter objCounter = objBll.GetCounter(municipalityID, date, userID);
            if (objCounter == null)
            {
                throw new InvalidOperationException("Payment Counter Not found. Counter may not be configured");
            }
            return objCounter;
        }
        public void SetPaymentStatus(SqlTransaction sqlTransaction, SqlConnection sqlConnection, long? mstPaymentID, bool isPaid)
        {
            SqlCommand cmd = new SqlCommand("Change_PaymentStatus", sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@mstPaymentID", mstPaymentID ?? DBNull.Value as object);
            cmd.Parameters.AddWithValue("@isPaid", isPaid);
            cmd.ExecuteNonQuery();
        }
        public List<PropertyTaxOutstading> GetPropertyTaxPaymentDetails(int municipalityID, int wardID, int locationID, long assesseeID)
        {
            List<PropertyTaxOutstading> lst = new List<PropertyTaxOutstading>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 6000;
                cmd.CommandText = "Get_GenerateOutstanding";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@WardID", wardID);
                cmd.Parameters.AddWithValue("@LocationID", locationID);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                return ConvetToPropertyTaxPayment(dt);

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public List<PaymentSummary> GetLastPayments(int noOfRows, int municipalityID, int counterID)
        {
            List<PaymentSummary> lst = new List<PaymentSummary>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "GetLast_N_OfflinePayments";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@countOfRows", noOfRows);
                cmd.Parameters.AddWithValue("@MuniciaplityID", municipalityID);
                cmd.Parameters.AddWithValue("@counterID", counterID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                lst = dt.AsEnumerable().Select(t =>
                {
                    PaymentSummary p = new PaymentSummary();
                    p.PaymentID = t.Field<long>("PaymentID");
                    p.NetAmount = t.Field<decimal>("NetAmount");
                    p.PaymentReceiveDate = t.Field<DateTime>("PaymentReceiveDate");
                    p.AssesseeID = t.Field<long>("AssesseeID");
                    p.AssesseeName = t.Field<string>("AssesseeName");
                    p.HoldingNo = t.Field<string>("HoldingNo");
                    return p;
                }).ToList();

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

            return lst;
        }
        public List<PaymentSummary> GetLastPayments(int noOfRows, long assesseeId)
        {
            List<PaymentSummary> lst = new List<PaymentSummary>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Get_Last_N_PaymentsByAssesseeID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@countOfRows", noOfRows);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                lst = dt.AsEnumerable().Select(t =>
                {
                    PaymentSummary p = new PaymentSummary();
                    p.PaymentID = t.Field<long>("PaymentID");
                    p.NetAmount = t.Field<decimal>("NetAmount");
                    p.PaymentReceiveDate = t.Field<DateTime>("PaymentReceiveDate");
                    p.AssesseeID = t.Field<long>("AssesseeID");
                    p.AssesseeName = t.Field<string>("AssesseeName");
                    p.HoldingNo = t.Field<string>("HoldingNo");
                    return p;
                }).ToList();

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }

        protected List<PropertyTaxOutstading> ConvetToPropertyTaxPayment(DataTable dt)
        {
            List<PropertyTaxOutstading> lst = new List<PropertyTaxOutstading>();
            foreach (DataRow dr in dt.Rows)
            {
                PropertyTaxOutstading obj = new PropertyTaxOutstading();
                obj.MunicipalityID = dr.Field<int>("MunicipalityID");
                obj.WardID = dr.Field<int>("WardID");
                obj.LocationID = dr.Field<int>("LocationID");
                obj.AssesseeID = dr.Field<long>("AssesseeID");
                obj.DemandType = dr.Field<string>("DemandType");
                obj.FinYear = dr.Field<string>("FinYear");
                obj.DemandTypeID = dr.Field<long>("DemandTypeID");
                obj.QtrNo = dr.Field<int>("QtrNo");
                obj.BillDueDate = dr.Field<DateTime>("BillDueDate");
                obj.PropertyTaxValue = dr.Field<decimal>("PropertyTaxValue");
                obj.SurchargeValue = dr.Field<decimal>("SurchargeValue");
                obj.PaidPropertyTaxAmt = dr.Field<decimal>("PaidPropertyTaxAmt");
                obj.PaidSurchargeAmt = dr.Field<decimal>("PaidSurchargeAmt");
                obj.OSPropertyTaxAmt = dr.Field<decimal>("OSPropertyTaxAmt");
                obj.OSSurchargeAmt = dr.Field<decimal>("OSSurchargeAmt");
                obj.Rank = (int)dr.Field<long>("RowNumber");
                lst.Add(obj);
            }
            return lst;
        }
        public List<PropertyTaxOutstading> GetAllArrearDemanBillRecord(int wardID, long assesseeID, int locationID, int municipalityID)
        {
            List<PropertyTaxOutstading> lst = new List<PropertyTaxOutstading>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 6000;
                cmd.CommandText = "Get_GenerateOutstandingForAssesseeUpdation";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@WardID", wardID);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
                cmd.Parameters.AddWithValue("@LocationID", locationID);
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                lst = ConvetToPropertyTaxPayment(dt);

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }

        public PropertyTaxPaymentMaster GetPaymentMasterByPaymentID(long paymentID)
        {

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Get_MstPaymentByID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@PaymentID", paymentID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return ConvertToPropertyTaxPaymentMaster(dt)[0];
                }
                return null;

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public PropertyTaxPaymentMaster GetPaymentMasterByPaymentID(string transactionID)
        {

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Get_MstPaymentByID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@TransactionID", transactionID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return ConvertToPropertyTaxPaymentMaster(dt)[0];
                }
                return null;

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public decimal GetUnadjustedAdvanceAmount(int municipalityID, long assesseeID)
        {
            decimal advanceAmount = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select [taxation].[FUNC_UnadjustedAdvance](@municipalityID,@assesseeID) as UnAdjustedAdvance";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@municipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@assesseeID", assesseeID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    advanceAmount = dt.Rows[0].Field<decimal>("UnAdjustedAdvance");
                }
            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
            return advanceAmount;
        }
        public void Adjust_UndjustedAdvance(int municipalityID, long assesseeID, long userID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 6000;
                cmd.CommandText = "Get_AdvanceAdjustment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
                cmd.Parameters.AddWithValue("@userID", userID);
                cmd.Parameters.AddWithValue("@advType", "A");//A: autometic

                cn.Open();
                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        protected List<PropertyTaxPaymentMaster> ConvertToPropertyTaxPaymentMaster(DataTable dt)
        {
            List<PropertyTaxPaymentMaster> lst = new List<PropertyTaxPaymentMaster>();
            foreach (DataRow dr in dt.Rows)
            {
                PropertyTaxPaymentMaster obj = new PropertyTaxPaymentMaster();
                obj.PaymentID = dr.Field<long?>("PaymentID");
                obj.PaymentType = dr.Field<string>("PaymentType");
                obj.AssesseeID = dr.Field<long?>("AssesseeID");
                obj.MunicipalityID = dr.Field<int?>("MunicipalityID");
                obj.CounterID = dr.Field<int?>("CounterID");
                obj.BillID = dr.Field<long?>("BillID");
                obj.FinYear = dr.Field<string>("FinYear");
                obj.PaymentCollectionDate = dr.Field<DateTime?>("PaymentCollectionDate");
                obj.PaymentReceiveDate = dr.Field<DateTime?>("PaymentReceiveDate");
                obj.NetAmount = dr.Field<decimal?>("NetAmount");
                obj.RoundOffAdjust = dr.Field<decimal?>("RoundOffAdjust");
                obj.PayMode = dr.Field<string>("PayMode");
                obj.InstrumentNo = dr.Field<string>("InstrumentNo");
                obj.InstrumentDate = dr.Field<DateTime?>("InstrumentDate");
                obj.PaidAmount = dr.Field<decimal?>("PaidAmount");
                obj.CollectorID = dr.Field<int?>("CollectorID");
                obj.TransactionID = dr.Field<string>("TransactionID");
                obj.IsPaymentSuccessfull = dr.Field<bool>("IsPaymentSuccessfull");
                obj.InsertedBy = dr.Field<int?>("InsertedBy");
                obj.InsertedOn = dr.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = dr.Field<int?>("UpdatedBy");
                obj.UpdatedOn = dr.Field<DateTime?>("UpdatedOn");
                lst.Add(obj);
            }

            return lst;
        }

    }
}