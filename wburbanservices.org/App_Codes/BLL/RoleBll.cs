﻿using System.Collections.Generic;
using Valuation_Board.Models.Account;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Valuation_Board.App_Codes.BLL
{
    public class RoleBll
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public RoleBll()
        {
            cn = new SqlConnection(constring);
        }
        public List<Role> GetRoles(long userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_Roles_ByUserID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return Convert(dt);
            }
            finally
            {
                cn.Close();
            }
        }

        List<Role> Convert(DataTable dt)
        {
            return dt.AsEnumerable().Select(t => new Role()
            {
                UserType = t.Field<UserType>("UserTypeID"),
                Description = t.Field<string>("Description"),
                RoleId = t.Field<int>("RoleID"),
                RoleName = t.Field<string>("RoleName"),
                IsActive = t.Field<bool>("IsActive")
            }).ToList();
        }
    }
}