﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;

namespace Valuation_Board.App_Codes.BLL
{
    public class OtherCitizenUserBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public OtherCitizenUserBll()
        {
            cn = new SqlConnection(conString);
        }
        public void GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public string GetUrl_LandingPage(OtherCitizenUser user)
        {
            return new BllBase().GetLandingUrlByUserId(user.UserID);
        }
        public string GetUrl_LandingPage(OtherCitizenUser user, int moduleId)
        {
            return new BllBase().GetLandingUrlByUserId(user.UserID, moduleId);
        }

        public OtherCitizenUser GetUser(string userName, string password)
        {
            cn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "Get_OtherCitizenUserr";
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs = Convert(dt);
                if (objs.Count > 0)
                {
                    return objs[0];
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        #region
        public List<OtherCitizenUser> Convert(DataTable dt)
        {
            List<OtherCitizenUser> lst = new List<OtherCitizenUser>();
            foreach (DataRow dr in dt.Rows)
            {
                OtherCitizenUser vu = new OtherCitizenUser()
                {
                    UserID = dr.Field<long>("UserID"),
                    RoleId = dr.Field<int>("RoleId"),
                    Email = dr.Field<string>("Email"),
                    FirstName = dr.Field<string>("FirstName"),
                    LastName = dr.Field<string>("LastName"),
                    PhoneNo = dr.Field<string>("PhoneNo"),
                    IsAccountVerified = dr.Field<byte>("IsAccountVerified") == 1 ? true : false,
                    IsAccountLocked = dr.Field<byte>("IsAccountLocked") == 1 ? true : false,
                    UserName = dt.Rows[0].Field<string>("UserName"),
                    UserType = dt.Rows[0].Field<UserType>("UserType"),
                    CompanyName = dt.Rows[0].Field<string>("companyname")
                };
                lst.Add(vu);
            }
            return lst;

        }
        #endregion
    }

}