﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class PG_BLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public PG_BLL()
        {
            cn = new SqlConnection(conString);
        }
        public PG_Model Get_PGModel(string transactionId)
        {
            SqlCommand cmd = new SqlCommand("Get_OnlinePGInputData", cn);
            cmd.Parameters.AddWithValue("@transactionID", transactionId);
            //cmd.Parameters.AddWithValue("@paymentID", transactionId);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public PG_Model Get_PGModel(long paymentID)
        {
            throw new NotFiniteNumberException();
        }
        List<PG_Model> Convert(DataTable dt)
        {
            List<PG_Model> lst = new List<PG_Model>();
            foreach (DataRow dr in dt.Rows)
            {
                PG_Model pg = new PG_Model();
                pg.CustomerID = dr.Field<string>("CustomerID");
	            pg.TxnAmount = dr.Field<decimal>("TxnAmount").ToString();
                pg.HoldingNo = dr.Field<string>("HoldingNo").ToString();
                pg.WardID = dr.Field<int>("WardID").ToString();
                pg.MunicipalityID = dr.Field<int>("MunicipalityID").ToString();
                pg.LocationID = dr.Field<int>("LocationID").ToString();
                pg.AssesseeName = dr.Field<string>("AssesseeName").ToString();
                pg.PaymentID = dr.Field<string>("PaymentID").ToString();
                pg.UserID = dr.Field<long>("UserID").ToString();
                pg.AssesseeNo = dr.Field<string>("AssesseeNo");
                lst.Add(pg);
            }
            return lst;
        }
    }
}

