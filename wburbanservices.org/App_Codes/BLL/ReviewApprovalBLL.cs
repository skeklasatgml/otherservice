﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using System.Globalization;

namespace Valuation_Board.App_Codes.BLL
{
    public class ReviewApprovalBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ReviewApprovalBLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<ReviewApproval_VM> Get_AllReviewData(int municipalityID, string EffectFromDT, string EffectToDT, string ApproveStatus)
        {
            DateTime dtFrom = DateTime.ParseExact(EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //System.Diagnostics.Debug.WriteLine(dtFrom.ToString("yyyy'-'MM'-'dd"));
            //DateTime dtFrom = Convert.ToDateTime(FromDate);
            //DateTime dtTo = Convert.ToDateTime(ToDate);

            SqlCommand cmd = new SqlCommand("Get_ReviewApproval", cn);
            cmd.Parameters.AddWithValue("@FromDate", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@ToDate", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@ApproveStatus", ApproveStatus);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertAllAdjEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }

        List<ReviewApproval_VM> ConvertAllAdjEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                ReviewApproval_VM p = new ReviewApproval_VM();
                p.ReviewID = t.Field<long?>("ReviewID");
                p.AssesseeID = t.Field<long?>("AssesseeID");
                p.MunicipalityID = t.Field<int?>("MunicipalityID");
                p.WardName = t.Field<string>("WardName");
                p.LocationName = t.Field<string>("LocationName");
                p.OldAssesseeNo = t.Field<long?>("OldAssesseeNo");
                p.HoldingNo = t.Field<string>("HoldingNo");
                p.AssesseeName = t.Field<string>("AssesseeName");
                p.AssesseeAddress = t.Field<string>("AssesseeAddress");

                p.PtaxRValue = t.Field<decimal?>("PtaxRValue");
                p.SurchargeRValue = t.Field<decimal?>("SurchargeRValue");
                p.FromFinYear = t.Field<string>("FromFinYear");
                p.FromQtrNo = t.Field<int>("FromQtrNo");
                p.ToFinYear = t.Field<string>("ToFinYear");
                p.ToQtrNo = t.Field<int>("ToQtrNo");
                p.ApprovedFlag = t.Field<string>("ApprovedFlag");

                p.ApprovedStatus = t.Field<string>("ApprovedStatus");
                p.InsertedOn = t.Field<string>("InsertedOn");
                return p;
            }).ToList();
        }

        public void UpdateReviewApproved(long UserID, ReviewApproval_VM ReviewApproval)
        {            
            SqlCommand cmd = new SqlCommand("Update_ReviewApproval", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", ReviewApproval.AssesseeID);
            cmd.Parameters.AddWithValue("@MunicipalityID", ReviewApproval.MunicipalityID);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewApproval.ReviewID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                if (ReviewApproval.ApprovedFlag == "N")
                {
                    cmd.ExecuteNonQuery();
                }                
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error...!" + ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
    }
}