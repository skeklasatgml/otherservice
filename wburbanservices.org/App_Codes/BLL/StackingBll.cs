﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using static Valuation_Board.ViewModels.Assessment_VM;

namespace Valuation_Board.App_Codes.BLL
{
    public class StackingBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        DbHandler _db;
        SqlConnection cn;
        public StackingBll()
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public List<ApplicationFormMD.Surface> GetTypeofSurface(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SURFACE_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Surface(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.RoadType> GetTypeofRoad(string SurfaceID, int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SurfaceID", SurfaceID);
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_ROAD_TYPE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_TypeofRoad(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public string GetApplicantName(long UserID, int UserType, int? ApplicationId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (UserType == 4)
            {
                cmd.Parameters.AddWithValue("@ApplicantID", UserID);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS");
            }
            else if (UserType == 2)
            {
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);
                cmd.Parameters.AddWithValue("@TransType", "LOAD_APPLICANT_DETAILS_MUNICIPALITY");
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string NAME = dt.Rows[0]["NAME"].ToString();
                    string DESIGNATION = dt.Rows[0]["DESIGNATION"].ToString();
                    string COMPANYNAME = dt.Rows[0]["COMPANYNAME"].ToString();
                    string MOBILENO = dt.Rows[0]["MOBILENO"].ToString();

                    return NAME + ", " + DESIGNATION + ", " + COMPANYNAME + ", " + MOBILENO;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Municipality> GetMunicipality(string Desc)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_MUNICIPALITY");
            cmd.Parameters.AddWithValue("@Desc", Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Municipality(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Purposes> GetPurpose(int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_PROJECT_PURPOSE");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Purposes(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Specification> GetSpecification(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATION");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Specification> GetSpecificationbyID(string SpecificationID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SpecificationID", SpecificationID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SPECIFICATIONBYID");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Specification(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public Boolean? GetModuleDetail(string SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_SUBMODULE_DETAILS");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Boolean ISDetailMultiple = Convert.ToBoolean(dt.Rows[0]["ISDetailMultiple"].ToString());
                    return ISDetailMultiple;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.UploadDetail> GetUploadDetail(int SubModuleId, int? ApplicationId, long UserID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_UPLOAD_DETAILS");
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);
            cmd.Parameters.AddWithValue("@ApplicantID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_UploadDetail(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Ward> GetWard(string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Ward(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Location> GetLocationByWard(string WardID, string MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_LOCATION_BY_WARD");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Location(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Action> GetDesignation(int? ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Desig(dt.Tables[0]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Action> GetDesignationByAction(string ActionID, string ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID == "" ? DBNull.Value : (object)ApplicationmID);
            cmd.Parameters.AddWithValue("@ActionType", ActionID == "" ? DBNull.Value : (object)ActionID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Desig(dt.Tables[1]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Action> GetAction(int? ApplicationmID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Proc_Get_UserAction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationmID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Tables.Count > 0)
                {
                    return Convert_Action(dt.Tables[0]);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.Inspector> GetInspectorName(string MunicipalityID, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("UDServices.InspectorSchedule_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LOAD_INSPECTOR");
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityID);
            //cmd.Parameters.AddWithValue("@InspectionOnFrom", FromDate);
            //cmd.Parameters.AddWithValue("@InspectionOnTo", ToDate);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_Inspector(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ApplicationFormMD.OtherCharges> GetOtherCharges(int SubModuleId)
        {
            SqlCommand cmd = new SqlCommand("UDServices.ULBS_services_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@TransType", "PageLoadOtherCharges");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert_OtherCharges(dt);
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converters
        public List<ApplicationFormMD.Surface> Convert_Surface(DataTable dt)
        {
            List<ApplicationFormMD.Surface> lst = new List<ApplicationFormMD.Surface>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Surface();
                obj.SurfaceID = t.Field<int?>("SurfaceID");
                obj.SurfaceName = t.Field<string>("SurfaceName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.RoadType> Convert_TypeofRoad(DataTable dt)
        {
            List<ApplicationFormMD.RoadType> lst = new List<ApplicationFormMD.RoadType>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.RoadType();
                obj.RoadTypeID = t.Field<int?>("RoadTypeID");
                obj.RoadTypeName = t.Field<string>("RoadTypeName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Municipality> Convert_Municipality(DataTable dt)
        {
            List<ApplicationFormMD.Municipality> lst = new List<ApplicationFormMD.Municipality>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Municipality();
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.MunicipalityName = t.Field<string>("MunicipalityName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Purposes> Convert_Purposes(DataTable dt)
        {
            List<ApplicationFormMD.Purposes> lst = new List<ApplicationFormMD.Purposes>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Purposes();
                obj.TypeID = t.Field<int?>("TypeID");
                obj.Purpose = t.Field<string>("Purpose");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Specification> Convert_Specification(DataTable dt)
        {
            List<ApplicationFormMD.Specification> lst = new List<ApplicationFormMD.Specification>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Specification();
                obj.ID = t.Field<long?>("ID");
                obj.Description = t.Field<string>("Description");
                obj.SpecificationRWidth = t.Field<Boolean?>("SpecificationRWidth");
                obj.SpecificationRDepth = t.Field<Boolean?>("SpecificationRDepth");
                obj.SpecificationPNo = t.Field<Boolean?>("SpecificationPNo");
                obj.SpecificationPLength = t.Field<Boolean?>("SpecificationPLength");
                obj.SpecificationPWidth = t.Field<Boolean?>("SpecificationPWidth");
                obj.SpecificationPDepth = t.Field<Boolean?>("SpecificationPDepth");
                obj.SpecificationTDiameter = t.Field<Boolean?>("SpecificationTDiameter");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.UploadDetail> Convert_UploadDetail(DataTable dt)
        {
            List<ApplicationFormMD.UploadDetail> lst = new List<ApplicationFormMD.UploadDetail>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.UploadDetail();
                obj.DocTypeID = t.Field<int?>("DocTypeID");
                obj.DocName = t.Field<string>("DocName");
                obj.ExtensionType = t.Field<string>("ExtensionType");
                obj.maxQuantity = t.Field<int?>("maxQuantity");
                obj.isMandatory = t.Field<int?>("isMandatory");
                obj.MaxSize = t.Field<decimal?>("MaxSize");
                obj.isVisible = t.Field<Boolean?>("isVisible");
                obj.isDelete = t.Field<Boolean?>("isDelete");
                obj.isEnable = t.Field<Boolean?>("isEnable");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Ward> Convert_Ward(DataTable dt)
        {
            List<ApplicationFormMD.Ward> lst = new List<ApplicationFormMD.Ward>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Ward();
                obj.WardID = t.Field<int?>("WardID");
                obj.WardName = t.Field<string>("WardName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Location> Convert_Location(DataTable dt)
        {
            List<ApplicationFormMD.Location> lst = new List<ApplicationFormMD.Location>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Location();
                obj.LocationID = t.Field<int?>("LocationID");
                obj.LocationName = t.Field<string>("LocationName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Action> Convert_Desig(DataTable dt)
        {
            List<ApplicationFormMD.Action> lst = new List<ApplicationFormMD.Action>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Action();
                obj.DesignationID = t.Field<int?>("DesignationID");
                obj.DesignationName = t.Field<string>("DesignationName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Action> Convert_Action(DataTable dt)
        {
            List<ApplicationFormMD.Action> lst = new List<ApplicationFormMD.Action>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Action();
                obj.ActionType = t.Field<string>("ActionType");
                obj.ActionName = t.Field<string>("ActionName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.Inspector> Convert_Inspector(DataTable dt)
        {
            List<ApplicationFormMD.Inspector> lst = new List<ApplicationFormMD.Inspector>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.Inspector();
                obj.InspectorID = t.Field<int?>("InspectorID");
                obj.InspectorName = t.Field<string>("InspectorName");
                return obj;
            }).ToList();
            return lst;
        }
        public List<ApplicationFormMD.OtherCharges> Convert_OtherCharges(DataTable dt)
        {
            List<ApplicationFormMD.OtherCharges> lst = new List<ApplicationFormMD.OtherCharges>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ApplicationFormMD.OtherCharges();
                obj.OtherChargeID = t.Field<int?>("OtherChargeID");
                obj.ChargeName = t.Field<string>("ChargeName");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}