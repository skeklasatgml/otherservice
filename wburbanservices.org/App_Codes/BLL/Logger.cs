﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Valuation_Board.Models.AppResource;

namespace Valuation_Board.App_Codes.BLL
{
    public class Logger
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public Logger()
        {
            cn = new SqlConnection(constring);
        }

        #region
        public void LogError(Exception ex)
        {
            try
            {
                ErrorLog error = new ErrorLog();
                error.Msg = ex.Message;
                error.StackStrace = ex.StackTrace;
                error.ManagedThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
                error.SessionID = HttpContext.Current.Session.SessionID;
                Task.Factory.StartNew(() =>
                {
                    LogError_db(error);
                });
            }
            catch {
            }
            
        }
        #endregion
        private void LogError_db(ErrorLog error)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Insert_ErrorLog";
           
            cmd.Parameters.AddWithValue("@Msg",error.Msg);
            cmd.Parameters.AddWithValue("@StackStrace",error.StackStrace);
            cmd.Parameters.AddWithValue("@SessionID",error.SessionID);
            cmd.Parameters.AddWithValue("@ManagedThreadId", error.ManagedThreadId);
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch 
            {
               
            }
            finally
            {
                cn.Close();
            }
        }
    }
}