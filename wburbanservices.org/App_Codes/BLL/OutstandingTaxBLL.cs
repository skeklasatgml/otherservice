﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Valuation_Board.Models.WebApi;

namespace Valuation_Board.App_Codes.BLL
{
    public class OutstandingTaxBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public OutstandingTaxBLL()
        {
            cn = new SqlConnection(conString);
        }

        public IEnumerable<OutstandingTax> GetData(string Token, int MunicipalityID, string AssesseeNo)
        {
            //List<OutstandingTax> lstOutTax = new List<OutstandingTax>();
            SqlCommand cmd = new SqlCommand("Get_OutstandingTaxValueForAPI", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@AssesseeNo", AssesseeNo);
            cmd.Parameters.AddWithValue("@Token", Token.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertOutstanding(dt);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<OutstandingTax> ConvertOutstanding(DataTable dt)
        {
            List<OutstandingTax> lst = new List<OutstandingTax>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new OutstandingTax();
                obj.WardName = t.Field<string>("WardName");
                obj.LocationName = t.Field<string>("LocationName");
                obj.AssesseeNo = t.Field<string>("AssesseeNo");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                obj.AssesseeName = t.Field<string>("AssesseeName");
                obj.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                obj.FinYear = t.Field<string>("FinYear");
                obj.QtrNo = t.Field<int>("QtrNo");
                obj.OSPropertyTaxAmt = t.Field<decimal>("OSPropertyTaxAmt");
                obj.OSSurchargeAmt = t.Field<decimal>("OSSurchargeAmt");
                return obj;
            }).ToList();
            return lst;
        }
    }
}