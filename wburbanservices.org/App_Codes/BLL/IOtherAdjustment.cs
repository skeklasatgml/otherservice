﻿using System;
using System.Collections.Generic;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public interface IOtherAdjustment
    {
         long MstAdjID { get; set; }
         int MunicipalityID { get; set; }
         string PayHeadType { get; set; }
         int MstPayHeadID { get; set; }
         string PayHeadBehave { get; set; }
         string PayHeadDesc { get; set; }
        /// <summary>
        /// A=All Assessee, I=Indivisual Assessee
        /// </summary>
         string EffectType { get; set; }
        /// <summary>
        /// valid from
        /// </summary>
         DateTime EffectiveFromDt { get; set; }
        /// <summary>
        /// valid to
        /// </summary>
         DateTime EffectiveToDt { get; set; }
        /// <summary>
        /// A=absolute, P=Percent
        /// </summary>
         string RateValueType { get; set; }
         decimal RateValue { get; set; }
        /// <summary>
        /// A = For All Arrear and Demand.
        /// R = For Specified Range eg. 2015-2016(2nd qtr) to 2017-2018(4th qtr).
        /// C = Only for Current Demand.
        /// </summary>
         string ApplicableTaxRangeType { get; set; }
         bool IsPropertyTax { get; set; }
         bool IsSurcharge { get; set; }
         bool IsPropertyTaxInt { get; set; }
         bool IsSurchargeInt { get; set; }
         bool IsApproved { get; set; }
         DateTime? ApprovedOn { get; set; }
         long? MstDocumentsID { get; set; }

         string FromFinYear { get; set; }
         int? FromFinYearQtr { get; set; }
         string ToFinYear { get; set; }
         int? ToFinYearQtr { get; set; }
         string Remarks { get; set; }
        decimal GetAdjustableAmount(List<PropertyTaxPaymentDetail> propertyTaxPaymentDetails,long assesseeID);

    }
}