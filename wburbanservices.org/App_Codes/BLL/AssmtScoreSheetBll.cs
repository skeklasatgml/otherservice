﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.assmt;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssmtScoreSheetBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AssmtScoreSheetBll()
        {
            cn = new SqlConnection(conString);
        }
        #region
        List<AssmtScoreSheet> Convert(DataTable dt)
        {
            List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new AssmtScoreSheet();
                obj.ScoreSheetID = t.Field<long>("ScoreSheetID");
                obj.EffectiveDateForm = t.Field<DateTime>("EffectiveDateForm");
                obj.EffectiveDateTo = t.Field<DateTime>("EffectiveDateTo");
                obj.MunicipalityName = t.Field<string>("MunicipalityName");
                obj.YearId = t.Field<string>("Year");
                obj.ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc");
                obj.ScoreCode = t.Field<string>("ScoreCode");
                obj.ScoreDesc = t.Field<string>("ScoreDesc");
                obj.ScoreValue = t.Field<decimal>("ScoreValue");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
        public List<AssmtScoreSheet> GetAllAssmtScoreSheet(int municipalityId, string year, long userId)
        {
            SqlCommand cmd = new SqlCommand("get_Assmt_ScoreSheet", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public object GetYear()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_AllYear";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Conversion(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        object Conversion(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                MunicipalityID = t.Field<int>("MunicipalityID"),
                Year = t.Field<int>("YearId"),
                YearDesc = t.Field<string>("YearDesc"),
                EffectiveDateForm = t.Field<DateTime?>("EffectiveDateForm"),
                EffectiveDateTo = t.Field<DateTime?>("EffectiveDateTo"),
                Factor = t.Field<decimal?>("Factor")
            }).ToList();
            return lst;
        }

        List<T> CreateEmptyGenericList<T>(T example)
        {
            return new List<T>();
        }

        public object GetEffectivedate(int municipalityId, string year, long userId)
        {
            SqlCommand cmd = new SqlCommand("get_EffectiveDate", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Conversion1(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        object Conversion1(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                MunicipalityID = t.Field<int>("MunicipalityID"),
                Year = t.Field<int>("YearId"),
                YearDesc = t.Field<string>("YearDesc"),
                EffectiveDateForm = t.Field<DateTime>("EffectiveDateForm"),
                EffectiveDateTo = t.Field<DateTime>("EffectiveDateTo"),
                Factor = t.Field<decimal>("Factor")
            }).ToList();
            return lst;
        }

        public object getScoreType() {
            SqlCommand cmd = new SqlCommand("taxation.sp_Assmt_GetScoreType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            //cmd.Parameters.AddWithValue("@year", year);
            //cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    ScoreBeltTypeId = t.Field<int>("ScoreBeltTypeId"),
                    ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc"),
                    ScoreBelt = t.Field<string>("ScoreBelt")

                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object GetScoreByTypeId(string  TypeId) {
            SqlCommand cmd = new SqlCommand("taxation.GetScoreByTypeId", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            //cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@TypeId", TypeId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    ScoreCode = t.Field<string>("ScoreCode"),
                    ScoreDesc = t.Field<string>("ScoreDesc")
                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public void updateScoreSheet(List<ScoreSheetUpdate> ScoreValues)
        {
            SqlTransaction Transaction = null;
            cn.Open();
            Transaction = cn.BeginTransaction();

            try
            {
                foreach (var item in ScoreValues)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Transaction = Transaction;
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = "Update_Assmt_UpdateScoreSheet";
                    cmd.CommandText = "Update_Assmt_UpdateScoreSheet";
                    cmd.Parameters.AddWithValue("@ScoreSheetID", item.ScoreSheetID);
                    cmd.Parameters.AddWithValue("@ScoreValue", item.ScoreValue);
                    cmd.ExecuteNonQuery();
                }
                Transaction.Commit();
            }
            catch (Exception )
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        //public void Insert_User(MunicipalityUser obj, string Password)
        //{
        //    SqlTransaction Transaction = null;
        //    try
        //    {

        //        var comd = new SqlCommand();
        //        cn.Open();
        //        Transaction = cn.BeginTransaction();
        //        comd.CommandType = CommandType.StoredProcedure;
        //        comd.CommandText = "Insert_UserRegistration";
        //        comd.Connection = cn;
        //        comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
        //        comd.Parameters.AddWithValue("@LastName", obj.LastName);
        //        comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
        //        comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
        //        comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
        //        comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
        //        comd.Parameters.AddWithValue("@UserType", obj.UserType);
        //        comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
        //        comd.Parameters.AddWithValue("@UserName", obj.UserName);
        //        comd.Parameters.AddWithValue("@Password", Password);
        //        comd.Transaction = Transaction;
        //        comd.ExecuteNonQuery();

        //        Transaction.Commit();
        //    }
        //    catch (Exception)
        //    {
        //        Transaction.Rollback();
        //        throw;
        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }

        //}

        //public void Update_Password(long UserID, string password)
        //{
        //    SqlCommand cmd = new SqlCommand("Update_ResetPasswordByUserID", cn);
        //    cmd.Parameters.AddWithValue("@UserID", UserID);
        //    cmd.Parameters.AddWithValue("@Password", password);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        cn.Open();
        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (SqlException)
        //    {
        //        throw new Exception("Database error");
        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }
        //}


        public void SaveUpScoreSheet(AssmtScoreSheetInsUp assmtScoreSheetInsUp)
        {
            //dynamic fromto = EffctFromTo(blockPeriod);
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[Sp_SaveUpScoreSheet]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@MunicipalityID", assmtScoreSheetInsUp.MunicipalityID);
                cmd.Parameters.AddWithValue("@YearId", assmtScoreSheetInsUp.YearId);
                cmd.Parameters.AddWithValue("@ScoreSheetID", assmtScoreSheetInsUp.ScoreSheetID);
                cmd.Parameters.AddWithValue("@ScoreBeltTypeId", assmtScoreSheetInsUp.ScoreBeltTypeId);
                cmd.Parameters.AddWithValue("@ScoreCode", assmtScoreSheetInsUp.ScoreCode);
                cmd.Parameters.AddWithValue("@ScoreDesc",!string.IsNullOrEmpty(assmtScoreSheetInsUp.ScoreDesc) ? assmtScoreSheetInsUp.ScoreDesc : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ScoreValue", assmtScoreSheetInsUp.ScoreValue);
                cmd.Parameters.AddWithValue("@CurrentUserId", assmtScoreSheetInsUp.CurrentUserId);

                //comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                //comd.Parameters.AddWithValue("@LastName", obj.LastName);
                //comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                //comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                //comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                //comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                //comd.Parameters.AddWithValue("@UserType", obj.UserType);
                //comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                //comd.Parameters.AddWithValue("@UserName", obj.UserName);
                //comd.Parameters.AddWithValue("@Password", Password);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void DeleteScoreSheet(long id)
        {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[Sp_DeleteScoreSheet]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch 
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreSheetInsUp> GetAllAssmtScoreSheets(int municipalityId, string year, long userId)
        {
            SqlCommand cmd = new SqlCommand("[taxation].[get_Assmt_ScoreSheets]", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                //return Convert(dt);
                List<AssmtScoreSheetInsUp> lst = new List<AssmtScoreSheetInsUp>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new AssmtScoreSheetInsUp();
                    obj.ScoreSheetID = t.Field<long>("ScoreSheetID");
                    obj.ScoreBeltTypeId = t.Field<int>("ScoreBeltTypeId");
                    obj.ScoreCode = t.Field<string>("ScoreCode");
                    obj.ScoreDesc = t.Field<string>("ScoreDesc");
                    obj.ScoreValue = t.Field<decimal>("ScoreValue");
                    return obj;
                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }


    }
}