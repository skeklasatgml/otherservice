﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using System.Globalization;

namespace Valuation_Board.App_Codes.BLL
{
    public class AdvanceAdjustmentBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AdvanceAdjustmentBLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<AdvanceAdjustment_VM> Get_AllAdvanceData(int municipalityID, string EffectFromDT, string EffectToDT, string WardID, string LocationID, string AssesseeID)
        {
            DateTime dtFrom = DateTime.ParseExact(EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //System.Diagnostics.Debug.WriteLine(dtFrom.ToString("yyyy'-'MM'-'dd"));
            //DateTime dtFrom = Convert.ToDateTime(FromDate);
            //DateTime dtTo = Convert.ToDateTime(ToDate);

            SqlCommand cmd = new SqlCommand("Get_AdvanceDtlsALL", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@FromDate", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@ToDate", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@WardID", (WardID == "" ? 0 : (object)Convert.ToInt32(WardID)));
            cmd.Parameters.AddWithValue("@LocationID", (LocationID == "" ? 0 : (object)Convert.ToInt32(LocationID)));
            cmd.Parameters.AddWithValue("@AssesseeID", (AssesseeID == "" ? 0 : (object)Convert.ToInt64(AssesseeID)));

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertAllAdvEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        List<AdvanceAdjustment_VM> ConvertAllAdvEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                AdvanceAdjustment_VM p = new AdvanceAdjustment_VM();
                p.AssesseeID = t.Field<long>("FK_AssesseeID");
                p.AdvPaymentID = t.Field<long>("AdvPaymentID");
                p.MunicipalityID = t.Field<int>("FK_MunicipalityID");
                p.AssesseeName = t.Field<string>("AssesseeName");
                p.HoldingNo = t.Field<string>("HoldingNo");
                p.ReceiptNo = t.Field<string>("RecieptNo");
                p.AdvanceDate = t.Field<string>("AdvanceDate");
                p.AdvAmt = t.Field<decimal>("AdvAmt");
                p.UnadjustAdvAmt = t.Field<decimal>("UnAdjAdvAmt");
                p.AdjustAdvAmt = t.Field<decimal>("AdjAdvAmt");
                return p;
            }).ToList();
        }

        public List<AdvAdjustmentDtls_VM> Get_AdvAdjData(long AdvPaymentID, int MunicipalityID, long AssesseeID)
        {
            SqlCommand cmd = new SqlCommand("Get_AdvanceAdjustmentDtlsByAdvID", cn);
            cmd.Parameters.AddWithValue("@AdvPaymentID", AdvPaymentID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertAdvAdjData(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        List<AdvAdjustmentDtls_VM> ConvertAdvAdjData(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                AdvAdjustmentDtls_VM p = new AdvAdjustmentDtls_VM();
                p.FinYear = t.Field<string>("FinYear");
                p.QtrNo = t.Field<int>("QtrNo");
                p.TaxValue = t.Field<decimal>("TaxValue");
                p.PaidAmt = t.Field<decimal>("PaidTaxAmt");
                p.SurchargeValue = t.Field<decimal>("SurchargeValue");
                p.PaidSurchargeValue = t.Field<decimal>("PaidSurchargeAmt");
                p.AdjustedOn = t.Field<string>("InsertedOn");
                return p;
            }).ToList();
        }

        public string UpdateAdjustAdvance(long AdvPaymentID, int MunicipalityID, long AssesseeID, string ReceiptNo, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Update_AdvanceAdjustmentManually", cn);
            cmd.CommandTimeout = 6000;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.Parameters.AddWithValue("@PaymentID", ReceiptNo);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return dt.Rows[0]["Status"].ToString();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public ManualAdjustmentAvailability GetManualAdvAdjAvailabilityInfo(int MunicipalityID,int WardID, int LocationID, long AssesseeID)
        {
            Action<int, int, int, long> _validateBusiness = (_municipalityId, _wardId, _locationId, _assesseeId) => {
                AssesseeBll asBll = new AssesseeBll();
                var assessee = asBll.GetAssesseebyId(_assesseeId);
                if (assessee != null && assessee.WardID == _wardId && assessee.LocationID == _locationId)
                {
                   //ok
                }
                else
                {
                    throw new ArgumentException("Invalid Assessee");
                }
            };

            _validateBusiness(MunicipalityID, WardID, LocationID, AssesseeID);
            SqlCommand cmd = new SqlCommand("Get_UnadjustedAdvanceDtls", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.Parameters.AddWithValue("@AdvType", "M");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return new ManualAdjustmentAvailability {IsAvailable= true,AdvanceAmount = dt.Rows[0].Field<decimal>("UnAdjAdvAmt"),AdvanceDate=dt.Rows[0].Field<DateTime?>("AdvDate") };
                }
                else
                {
                    return new ManualAdjustmentAvailability { IsAvailable = false, AdvanceAmount = 0 };
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
    }
}