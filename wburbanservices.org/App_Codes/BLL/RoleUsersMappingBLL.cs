﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Valuation_Board.App_Codes.BLL
{
    public class RoleUsersMappingBLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection con;
        SqlCommand comd;
        SqlTransaction transaction;
        public RoleUsersMappingBLL()
        {
            con = new SqlConnection(constring);
            comd = new SqlCommand();
            comd.Connection = con;
            comd.CommandType = CommandType.StoredProcedure;
        }       

        public void Insert(List<RoleUsersMapping> obj,int MunicipalityID)
        {
            try
            {
                if (obj.Count > 0)
                {
                    con.Open();
                    transaction = con.BeginTransaction();
                    int RoleID = 0;
                    foreach (var Item in obj)
                    {
                        RoleID = Convert.ToInt32(Item.RoleID);
                    }                   
                    comd = new SqlCommand();
                    comd.Connection = con;
                    comd.CommandType = CommandType.StoredProcedure;
                    comd.CommandText = "Delete_RoleUsersMapping";
                    comd.Parameters.AddWithValue("@RoleID",RoleID);
                    comd.Parameters.AddWithValue("@MunicipalityID",MunicipalityID);
                    comd.Transaction = transaction;
                    comd.ExecuteNonQuery();

                    foreach (var Item in obj)
                    {
                        comd = new SqlCommand();
                        comd.Connection = con;
                        comd.CommandType = CommandType.StoredProcedure;
                        comd.CommandText = "Insert_RoleUsersMapping";                       
                        comd.Parameters.AddWithValue("@RoleID", Item.RoleID);
                        comd.Parameters.AddWithValue("@UserID", Item.UserID);
                        comd.Transaction = transaction;
                        comd.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        public List<RoleUsersMapping> Get_Roles()
        {
            List<RoleUsersMapping> lst_r = new List<RoleUsersMapping>();
            try
            {
                comd.CommandText = "Get_RoleName";              
                SqlDataAdapter ads = new SqlDataAdapter(comd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                lst_r = ConvertRoles(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
            
            return lst_r;
        }
        public List<RoleUsersMapping> Get_Roles(int userTypeID)
        {
            List<RoleUsersMapping> lst_r = new List<RoleUsersMapping>();
            try
            {
                comd.CommandText = "Get_RolesByUserType";
                comd.Parameters.AddWithValue("@userTypeID",userTypeID);
                SqlDataAdapter ads = new SqlDataAdapter(comd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                lst_r = ConvertRoles(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

            return lst_r;
        }
        public List<RoleUsersMapping> ConvertRoles(DataTable dt)
        {
            List<RoleUsersMapping> lst_role = new List<RoleUsersMapping>();
            lst_role = dt.AsEnumerable().Select(t =>
            {
                RoleUsersMapping r = new RoleUsersMapping();
                r.RoleID = t.Field<int>("RoleID");
                r.RoleName = t.Field<string>("RoleName");
                return r;
            }).ToList();
            return lst_role;
        }
        public List<RoleUsersMapping> Get_UserByRoleID(int RoleID,int MunicipalityID)
        {
            List<RoleUsersMapping> lst_r = new List<RoleUsersMapping>();
            try
            {
                comd.CommandText = "Get_RoleUsersMappingByRoleID";
                comd.Parameters.AddWithValue("@RoleID", RoleID);
                comd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
                SqlDataAdapter ads = new SqlDataAdapter(comd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                lst_r = ConvertRoleUser(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

            return lst_r;
        }
        public List<RoleUsersMapping> ConvertRoleUser(DataTable dt)
        {
            List<RoleUsersMapping> lst_role = new List<RoleUsersMapping>();
            lst_role = dt.AsEnumerable().Select(t =>
            {
                RoleUsersMapping r = new RoleUsersMapping();
                r.RoleID = t.Field<int>("RoleID");
                r.UserID = t.Field<long>("UserID");
                return r;
            }).ToList();
            return lst_role;
        }
        
    }
}