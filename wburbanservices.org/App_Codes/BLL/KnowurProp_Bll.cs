﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class KnowurProp_Bll
    {
        public class Resp<T>
        {
            public HttpStatusCode Status { get; set; }
            public T Data { get; set; }
        }
        protected DbHandler _db;
        protected SqlConnection cn;
        RestClient client = new RestClient("http://banglarbhumi.gov.in");
        public KnowurProp_Bll()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }

        public void Dispose()
        {
            cn.Close();
        }
        private List<OutStanding> getOutStandings(string idn,string plotId,string khatianId)
        {
            idn = idn.PadLeft(7, '0');
            var cmd = _db.NewCommand("taxation.get_Outstanding_nic", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@plotId", plotId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@khatianNo", khatianId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@idn", idn);
            return _db.GetResult(cmd).Convert<OutStanding>();
        }
        private Resp<T> PlotInformation<T>(string nidCode,string _plotNo)where T:new()
        {
            var _url = string.Format("NizamWebservice/service/PlotInformation/{0}/{1}/04/RGxycyMxMjM=Ulc5RVFsOWtiSEp6SXpBMA==", nidCode, _plotNo);
            var request = new RestRequest(_url, Method.GET);
            var resp= client.Execute<T>(request);
            var content = resp.Data; 

            return new Resp<T>() { Status=resp.StatusCode,Data=content };
        }
        private Resp<T> KhatianInformation<T>(string _nidCode, string _khatianNo) where T : new()
        {
            
            var _url = string.Format("NizamWebservice/service/landOwnerdetail/{0}/{1}/0/04/RGxycyMxMjM=Ulc5RVFsOWtiSEp6SXpBMA==", _nidCode, _khatianNo);
            var request = new RestRequest(_url, Method.GET);
            var resp = client.Execute(request);
            var content = Newtonsoft.Json.JsonConvert.DeserializeObject<T>( resp.Content); 
            return new Resp<T>() { Status = resp.StatusCode, Data = content };
            
        }
        private MutationData getSpecialMutationData(MutationData mt,string _idn,string _khatian)
        {
            //1301004
            if(_idn == "1301004" &&  _khatian== "01623")
            {
                return new MutationData { KhatianCreationDate="18/09/2013"};
            }
            else if (_idn == "1301004" && _khatian == "01498")
            {
                return new MutationData { KhatianCreationDate = "19/12/2012" };
            }
            else
            {
               return mt;
            }
        }
        private MutationData getMutationData(string _nidCode, string _khatianNo)
        {
            try
            {
                var _url = string.Format("/SDCWebservice/service/landOwnerdetail/{0}/{1}/0/07/RGxycyMxMjM=OWFhZDE2MTJiNGQ3MGM2NDM2ZmY3ZTdiYzA4MTMzZmQ=/92161d6f458c4105a29c5fce857b9060aae3405912cdfb33/8zaIivyzhw8=MTAuMTczLjUzLjE2Mw==", _nidCode, _khatianNo);
                var request = new RestRequest(_url, Method.GET);
                var req = new RestClient("http://202.61.117.165:9080");
                var resp = req.Execute(request);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<MutationData>>(resp.Content).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
        public KnowurProp<List<PlotInfo>> SearchByPlot(string _nidCode, string _plotNo)
        {
            var _plotInfoRsp = PlotInformation<List<PlotInfo>>(_nidCode.PadLeft(7, '0'), _plotNo);
            var _plotInfo = _plotInfoRsp.Data;
            return new KnowurProp<List<PlotInfo>>() {RemoteData= _plotInfo,OutStandings=getOutStandings(_nidCode,_plotNo,null) };
        }
        public KnowurProp<List<KhatianDetail>> SearchByKhatian(string _nidCode, string _khatianNo)
        {
            _nidCode = _nidCode.PadLeft(7, '0');
            _khatianNo = _khatianNo.PadLeft(5, '0');
            var _mutationData = getMutationData(_nidCode, _khatianNo);
            var _khatianInfoRsp = KhatianInformation<List<KhatianDetail>>(_nidCode, _khatianNo);
            _mutationData = getSpecialMutationData(_mutationData, _nidCode, _khatianNo);
            var _khatianInfo = _khatianInfoRsp.Data;
            if(_khatianInfoRsp.Data != null)
            {
                _khatianInfoRsp.Data.ForEach(t => {
                    t.MutationData = _mutationData;
                });
            }
            return new KnowurProp<List<KhatianDetail>>() { RemoteData = _khatianInfo, OutStandings = getOutStandings(_nidCode, null, _khatianNo) };
        }
        public List<Mouza<int,string>> GetMouzas(string distCode,string blockCode)
        {
            var cmd = _db.NewCommand("taxation.get_NicMouzas", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@dcode",distCode.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@bcode",blockCode.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<Mouza<int, string>>();
        }
        public List<CodeValue<int,string>> GetBlocks(string distCode)
        {
            var cmd = _db.NewCommand("taxation.get_NicBlocks", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@dcode", distCode.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<CodeValue<int, string>>();
        }
        public List<CodeValue<int, string>> GetDisticts()
        {
            var cmd = _db.NewCommand("taxation.get_NicDistricts", cn, CommandType.StoredProcedure);
            return _db.GetResult(cmd).Convert<CodeValue<int, string>>();
        }
        public Search_Name_Property SearchService_ForName(string dist_code,string ro_code, string deed_no, string deed_year)
        {
            string payload = @"<Search_Name>
                            <uname>UDMA</uname>
                            <pwd>en@th1k@r@n</pwd>
                            <dist_code>{0}</dist_code>
                            <ro_code>{1}</ro_code>
                            <deed_no>{2}</deed_no>
                            <deed_year>{3}</deed_year>
                            <book>1</book>
                        </Search_Name>";

            payload = string.Format(payload, dist_code, ro_code, deed_no, deed_year);
                RestClient client = new RestClient("https://wbregistration.gov.in/wcf/SearchService.svc/SearchService_ForNameAndProperty");
                //https://wbregistration.gov.in/(S(gzztqdwiyp3akw4ltqpdg054))/wcf/SearchService.svc
                var request = new RestRequest(Method.GET);
                var resp = client.Execute(request);
                client.BaseUrl = resp.ResponseUri;
                request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "text/plain");
                request.Parameters.Clear();
                request.AddParameter("text/plain", payload, ParameterType.RequestBody);
                
                var resp1 = client.Execute<Search_Name_Property>(request);
            CheckError(resp1);
            if(resp1.Data == null || ( resp1.Data !=null && resp1.Data.Deed_details == null))
            {
                throw new Exception("No data found from remote server");
            }
            return resp1.Data;
            
            
        }
        public List<CourtCaseDetail> GetCourtCaseDetail(string idnCode,string plotNumber,string khatianNumber)
        {
            //string url = string.Format("http://www.banglarbhumi.gov.in/NizamWebservice/service/courtCaseDetail/{0}/{1}/0/{2}/0/04/{3}",idnCode, khatianNumber, plotNumber,AppSettings.SrvcPwdCourtCase);
              string url = string.Format("https://banglarbhumi.gov.in/NizamWebservice/service/courtCaseDetail/{0}/{1}/0/{2}/0/04/{3}", idnCode, khatianNumber, plotNumber, AppSettings.SrvcPwdCourtCase);
            RestClient client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            var resp = client.Execute(request);
            //client.BaseUrl = resp.ResponseUri;
            //request = new RestRequest(Method.POST);
            //request.AddHeader("Content-Type", "text/plain");
            //request.Parameters.Clear();

            var resp1 = client.Execute<List<CourtCaseDetail>>(request);
            if(resp1.StatusCode!=HttpStatusCode.OK)
                throw new Exception("Remote serve error: unable to fetch court case details");
            if (resp1.Data.FirstOrDefault(t => string.Equals(t.ERROR, "1", StringComparison.OrdinalIgnoreCase)) == null)
                throw new Exception(resp1.Data.FirstOrDefault().ERROR);
            return resp1.Data;


        }
        public  void CheckError(IRestResponse r)
        {
            if (r.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Remote serve error");
            }
            else
            {
                XDocument xmlDoc = XDocument.Parse(r.Content);

                var result_status = xmlDoc.Elements().Descendants().FirstOrDefault(t => string.Equals(t.Name.LocalName, "result_status", StringComparison.OrdinalIgnoreCase));
                var remarks = xmlDoc.Elements().Descendants().FirstOrDefault(t => string.Equals(t.Name.LocalName, "remarks", StringComparison.OrdinalIgnoreCase));
                if (result_status != null && string.Equals(result_status.Value, "F", StringComparison.OrdinalIgnoreCase))
                {
                    throw new Exception(remarks.Value);
                }
            }
        }
    }
    
}