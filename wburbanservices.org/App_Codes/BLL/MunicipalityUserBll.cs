﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class MunicipalityUserBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MunicipalityUserBll()
        {
            cn = new SqlConnection(conString);
        }
        public void GetUser(int id)
        {
            throw new NotImplementedException();
        }
        public string GetUrl_LandingPage(MunicipalityUser user)
        {

            return new BllBase().GetLandingUrlByUserId(user.UserID);
        }
        public string GetUrl_LandingPage(MunicipalityUser user,int moduleID)
        {

            return new BllBase().GetLandingUrlByUserId(user.UserID, moduleID);
        }
        public string GetUrl_AfterLogout()
        {
            return "/Home/Index";
        }
        public MunicipalityUser GetUser(string userName,string password,int municipailityID)
        {
             cn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "Get_MunicipalityUserByMnIdUserNmPass";
            cmd.Parameters.AddWithValue("@MunicipalityID", municipailityID);
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs = Convert(dt);
                if(objs.Count>0)
                {
                   return objs[0];
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MunicipalityUser> Get_AllUsersByMunicipalityID(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_AllUsersDetailsByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        public void Insert_User(MunicipalityUser obj, string Password)
        {
            SqlTransaction Transaction = null;
            try
            {
               
                var comd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                comd.CommandType = CommandType.StoredProcedure;
                comd.CommandText = "Insert_UserRegistration";
                comd.Connection = cn;
                comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                comd.Parameters.AddWithValue("@LastName", obj.LastName);
                comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                comd.Parameters.AddWithValue("@UserType", obj.UserType);
                comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                comd.Parameters.AddWithValue("@UserName", obj.UserName);
                comd.Parameters.AddWithValue("@Password", Password);
                comd.Transaction = Transaction;
                comd.ExecuteNonQuery();
                
                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }

        }
        public void Update_Password(long UserID, string password)
        {
            SqlCommand cmd = new SqlCommand("Update_ResetPasswordByUserID", cn);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        #region Converter
        public  List<MunicipalityUser> Convert(DataTable dt)
        {
            List<MunicipalityUser> lst = new List<MunicipalityUser>();
            foreach (DataRow dr in dt.Rows)
            {
                MunicipalityUser mn = new MunicipalityUser();

                mn.UserID = dr.Field<long>("UserID");
                mn.Email = dr.Field<string>("Email");
                mn.FirstName = dr.Field<string>("FirstName");
                mn.LastName = dr.Field<string>("LastName");
                mn.PhoneNo = dr.Field<string>("PhoneNo");
                mn.IsAccountVerified = dr.Field<byte>("IsAccountVerified") == 1 ? true : false;
                mn.IsAccountLocked = dr.Field<byte>("IsAccountLocked") == 1 ? true : false;
                mn.UserName = dr.Field<string>("UserName");
                mn.MunicipalityID = dr.Field<int>("MunicipalityID");
                mn.UserType = (UserType)dr.Field<int>("UserType");
               
                lst.Add(mn);
            }
            return lst;
            
        }
#endregion
    }
}