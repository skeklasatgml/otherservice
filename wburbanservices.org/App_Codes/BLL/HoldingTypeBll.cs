﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class HoldingTypeBll
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public HoldingTypeBll()
        {
            cn = new SqlConnection(constring);
        }
        public List<HoldingType> GetHoldingTypes(int? municipalityId)
        {
            SqlCommand cmd = new SqlCommand("Select_HoldingType_Master", cn);
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId ?? DBNull.Value as object);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        List<HoldingType> Convert(DataTable dt)
        {
            List<HoldingType> lst = new List<HoldingType>();
            foreach (DataRow dr in dt.Rows)
            {
                HoldingType h = new HoldingType();
                h.HoldingTypeID = dr.Field<int>("HoldingTypeID");
                h.HoldingTypeDesc = dr.Field<string>("HoldingTypeDesc");
                lst.Add(h);
            }
            return lst;
        }
        public List<HoldingType> GetHoldingTypes(int? municipalityId, string desc)
        {
            SqlCommand cmd = new SqlCommand("GET_TypHolding", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityId ?? DBNull.Value as object);
            cmd.Parameters.AddWithValue("@HoldingTypeDesc", desc ?? "");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}