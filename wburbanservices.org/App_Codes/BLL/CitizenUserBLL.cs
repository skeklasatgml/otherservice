﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class CitizenUserBLL
    {
        public string GetUrl_LandingPage(CitizenUser user)
        {
            return new BllBase().GetLandingUrl(4);
        }
        public CitizenUser GetUserByAssesseeID(long assesseeID)
        {
            throw new NotImplementedException();
        }
        public string GetUrl_AfterLogout()
        {
            return "/Home/Index";
        }
       
    }
}