﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class BllBase
    {
        DbHandler _db;
        SqlConnection _cn;
        public BllBase()
        {
            _db = new DbHandler();
            _cn = _db.NewConnection();
        }
        public string GetLandingUrl(int roleId)
        {
            try
            {
                string appTag = ConfigurationManager.AppSettings.Get("AppTag");
                _cn.Open();
                var cmd = _db.NewCommand("get_app_mst_LandingUlrByRole", _cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@roleId", roleId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@appTag", appTag.ReplaceDbNull());
                string url = _db.GetScalar<string>(cmd);
                if (string.IsNullOrEmpty(url)) { throw new InvalidOperationException("Landing url not found"); }
                return url;
            }
            catch
            {
                throw;
            }
            finally
            {
                _cn.Close();
            }
        }
        public string GetLandingUrl(int roleId,int moduleId)
        {
            try
            {
                string appTag = ConfigurationManager.AppSettings.Get("AppTag");
                _cn.Open();
                var cmd = _db.NewCommand("get_app_mst_LandingUlrByRoleModlue", _cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@roleId", roleId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@appTag", appTag.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@moduleId", moduleId);
                string url = _db.GetScalar<string>(cmd);
                if (string.IsNullOrEmpty(url)) { throw new InvalidOperationException("Landing url not found"); }
                return url;
            }
            catch
            {
                throw;
            }
            finally
            {
                _cn.Close();
            }
        }
        public string GetLandingUrlByUserId(long userId)
        {
            try
            {
                List<Role> roles = new RoleBll().GetRoles(userId);
                int roleId = roles[0].RoleId;
                
                return GetLandingUrl(roleId);
            }
            catch
            {
                throw;
            }
            finally
            {
                _cn.Close();
            }
        }
        public string GetLandingUrlByUserId(long userId,int moduleId)
        {
            try
            {
                List<Role> roles = new RoleBll().GetRoles(userId);
                int roleId = roles[0].RoleId;

                return GetLandingUrl(roleId, moduleId);
            }
            catch
            {
                throw;
            }
            finally
            {
                _cn.Close();
            }
        }

    }
}