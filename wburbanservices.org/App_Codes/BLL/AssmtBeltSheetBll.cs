﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.assmt;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssmtBeltSheetBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AssmtBeltSheetBll()
        {
            cn = new SqlConnection(conString);
        }

        
        public List<AssmtBeltSheet> GetAllAssmtBeltSheet(int municipalityId, string year,long userId)
        {
            SqlCommand cmd = new SqlCommand("get_Assmt_BeltSheets", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<AssmtBeltSheet> Convert(DataTable dt)
        {
            List<AssmtBeltSheet> lst = new List<AssmtBeltSheet>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new AssmtBeltSheet();
                obj.BeltSheetID = t.Field<long>("BeltSheetID");
                obj.ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc");
                obj.FromRange = t.Field<decimal>("FromRange");
                obj.ToRange = t.Field<decimal>("ToRange");
                obj.BeltUnit = t.Field<string>("BeltUnit");
                obj.BeltValue = t.Field<string>("BeltValue");
                return obj;
            }).ToList();
            return lst;
        }

        public void updateBeltSheet(List<BeltSheetUpdate> BeltValues)
        {
            SqlTransaction Transaction = null;
            cn.Open();
            Transaction = cn.BeginTransaction();

            try
            {
                foreach (var item in BeltValues)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Transaction = Transaction;
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = "Update_Assmt_UpdateScoreSheet";
                    cmd.CommandText = "Update_Assmt_UpdateBeltSheet";
                    cmd.Parameters.AddWithValue("@BeltSheetID", item.BeltSheetID);
                    cmd.Parameters.AddWithValue("@From", item.From);
                    cmd.Parameters.AddWithValue("@To", item.To);
                    cmd.Parameters.AddWithValue("@BeltValue", item.BeltValue);
                    cmd.ExecuteNonQuery();
                }
                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object getBeltTypevalue() {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_BeltTypeval";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    ValTag = t.Field<string>("ValTag"),
                    ValDesc = t.Field<string>("ValDesc")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }


        public List<AssmtBeltSheetInsUp> GetAllAssmtBeltSheets(int municipalityId, string year, long userId)
        {
            SqlCommand cmd = new SqlCommand("[taxation].[get_Assmt_BeltSheets]", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                //return Convert(dt);
                List<AssmtBeltSheetInsUp> lst = new List<AssmtBeltSheetInsUp>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new AssmtBeltSheetInsUp();
                    obj.BeltSheetID = t.Field<long>("BeltSheetID");
                    obj.ScoreBeltTypeId = t.Field<int>("ScoreBeltTypeId");
                    obj.FromRange = t.Field<decimal>("FromRange");
                    obj.ToRange = t.Field<decimal>("ToRange");
                    obj.BeltValue = t.Field<string>("BeltValue");
                    return obj;
                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void SaveUpBeltSheet(AssmtBeltSheetInsUp assmtBeltSheetInsUp)
        {
            //dynamic fromto = EffctFromTo(blockPeriod);
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[Sp_SaveUpBeltSheet]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@MunicipalityID", assmtBeltSheetInsUp.MunicipalityID);
                cmd.Parameters.AddWithValue("@YearId", assmtBeltSheetInsUp.YearId);
                cmd.Parameters.AddWithValue("@BeltSheetID", assmtBeltSheetInsUp.BeltSheetID);
                cmd.Parameters.AddWithValue("@ScoreBeltTypeId", assmtBeltSheetInsUp.ScoreBeltTypeId);
                cmd.Parameters.AddWithValue("@FromRange", assmtBeltSheetInsUp.FromRange);
                //cmd.Parameters.AddWithValue("@ScoreDesc", !string.IsNullOrEmpty(assmtBeltSheetInsUp.ToRange) ? assmtBeltSheetInsUp.ToRange : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ToRange", assmtBeltSheetInsUp.ToRange);
                cmd.Parameters.AddWithValue("@BeltValue", assmtBeltSheetInsUp.BeltValue);
                cmd.Parameters.AddWithValue("@CurrentUserId", assmtBeltSheetInsUp.CurrentUserId);

                //comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                //comd.Parameters.AddWithValue("@LastName", obj.LastName);
                //comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                //comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                //comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                //comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                //comd.Parameters.AddWithValue("@UserType", obj.UserType);
                //comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                //comd.Parameters.AddWithValue("@UserName", obj.UserName);
                //comd.Parameters.AddWithValue("@Password", Password);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch 
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void DeleteBeltSheet(long id) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[Sp_DeleteBeltSheet]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch 
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

    }
}