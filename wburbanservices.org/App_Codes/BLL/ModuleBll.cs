﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class ModuleBll : IDisposable
    {
        DbHandler _db = null;
        SqlConnection cn = null;
        public ModuleBll()
        {
            _db = new DbHandler();
            cn = new SqlConnection(_db.ConnectionString);
            cn.Open();
        }
        public List<Module> GetAuthorisedModules(long userId,UserType userType)
        {
            var cmd = _db.NewCommand("get_authorisedModules", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userType", userType);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.Id = t.Field<int>("Id");
                x.Name = t.Field<string>("Name");
                x.Code = t.Field<string>("Code");
                x.Description = t.Field<string>("Description");
                return x;
            }).ToList();
        }
        public Module GetModule(int? moduleId,string moduleCode)
        {
            var cmd = _db.NewCommand("get_Module", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@moduleId", moduleId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@moduleCode", moduleCode.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            var rs= dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.Id = t.Field<int>("Id");
                x.Name = t.Field<string>("Name");
                x.Code = t.Field<string>("Code");
                x.Description = t.Field<string>("Description");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public Module GetSubModule(int? moduleId, string moduleCode)
        {
            var cmd = _db.NewCommand("udservices.get_subModule", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@moduleId", moduleId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@moduleCode", moduleCode.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.SubModuleId = t.Field<int>("Id");
                x.SubModuleName = t.Field<string>("Name");
                x.SubModuleCode = t.Field<string>("Code");
                x.SubModuleDescription = t.Field<string>("Description");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public void Dispose()
        {
            cn.Close();
        }
    }
}