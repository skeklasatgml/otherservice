﻿using System.Configuration;
using System.Data.SqlClient;
using System;
using Valuation_Board.Models.Application;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace Valuation_Board.App_Codes.BLL
{
    public class MunicipalityTaxPaymentCounter_BLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MunicipalityTaxPaymentCounter_BLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<MunicipalityPaymentCounter> GetCounters(int municipalityID,DateTime date)
        {
            throw new NotImplementedException();
        }
        public MunicipalityPaymentCounter GetCounter(int municipalityID,DateTime date, long municipalityUserID)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Get_MuncpltyCounter",cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@UserID", municipalityUserID);
                cmd.Parameters.AddWithValue("@CurDate", date.ToString("yyyy'-'MM'-'dd"));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return ConvertToPaymentCounter(dt)[0];
                }
                else
                {
                    return null;
                }
            }catch(SqlException ex)
            {
               throw new InvalidOperationException("DataBase Error",ex);
            }
        }
        public void Insert(MunicipalityPaymentCounter obj)
        {
            SqlCommand cmd = new SqlCommand("Insert_MunicipalityCounter", cn);
            cmd.Parameters.AddWithValue("@CounterCode", obj.CounterCode);
            cmd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToByte(obj.IsActive));
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }

        }
        public List<MunicipalityPaymentCounter> CountersByMunicipalityID(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_CounterByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);            
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertToPaymentCounter(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        public List<MunicipalityPaymentCounter> CountersByMunicipalityIDUserID(int MunicipalityID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_CounterByMunicipalityIDUserID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertToPaymentCounter(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        public List<MunicipalityPaymentCounter>GetCounters(List<int> municipalityCounterIDs)
        {
            return new List<MunicipalityPaymentCounter>();
        }
        public List<MunicipalityPaymentCounter> IncludesPaymentInstrument(List<MunicipalityPaymentCounter> municipalityPaymentCounters)
        {
            throw new NotImplementedException();
        }
        public MunicipalityPaymentCounter IncludesPaymentInstrument(MunicipalityPaymentCounter municipalityPaymentCounter)
        {
            throw new NotImplementedException();
        }
        public List<MunicipalityPaymentCounter> IncludesMunicipality(List<MunicipalityPaymentCounter> municipalityPaymentCounters)
        {
            throw new NotImplementedException();
        }
        public MunicipalityPaymentCounter IncludesMunicipality(MunicipalityPaymentCounter municipalityPaymentCounter)
        {
            throw new NotImplementedException();
        }
        public void ChangeStatusByCounterID(int counterID)
        {
            SqlCommand cmd = new SqlCommand("Update_MunicipalityCounterStatus", cn);
            cmd.Parameters.AddWithValue("@CounterID", counterID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }

        }
        #region Converters
        List<MunicipalityPaymentCounter> ConvertToPaymentCounter(DataTable dt)
        {
            List<MunicipalityPaymentCounter> lst = new List<MunicipalityPaymentCounter>();
            lst=dt.AsEnumerable().Select(t=> {
                //a.CounterID, a.CounterCode,a.IsActive,a.fk_MunicipalityID as MunicipalityID
                MunicipalityPaymentCounter obj = new MunicipalityPaymentCounter();
                obj.CounterID = t.Field<int>("CounterID");
                obj.CounterCode = t.Field<string>("CounterCode");
                obj.IsActive = t.Field<bool>("IsActive");
                obj.MunicipalityID = t.Field<int>("MunicipalityID");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion

    }
}