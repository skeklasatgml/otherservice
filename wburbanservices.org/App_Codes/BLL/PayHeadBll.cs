﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class PayHeadBll
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public PayHeadBll()
        {
            cn = new SqlConnection(constring);
        }

        public List<PayHead> Get_PayHeadsByMuniciplty(int municipalityID, DateTime CollectionDate)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadsBy_MmncpltyID", cn);
            cmd.Parameters.AddWithValue("@municipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@effectiveDate", CollectionDate.ToString("yyyy'-'MM'-'dd"));
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public List<PayheadGroup> GetPayheadGroups(int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadGroupByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return dt.AsEnumerable().Select(t => {
                       return new PayheadGroup() {
                            GroupID=t.Field<long>("GroupID"),
                            GroupName= t.Field<string>("GroupName"),
                            MunicipalityId= t.Field<int>("MunicipalityId"),
                        };
                    }).ToList();
                }
                else
                {
                    return new List<PayheadGroup>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<AssesseePayheadGroup> GetAssesseesPayheadGroup(int municipalityId,long? payheadGroupId,int? wardId,int? locationId,bool? isMapped,string NameOrHolding)
        {
            SqlCommand cmd = new SqlCommand("GetAssesseesFromPayheadGroup", cn);
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            //cmd.Parameters.AddWithValue("@PayheadGroupId", payheadGroupId ?? DBNull.Value as object);
            cmd.Parameters.AddWithValue("@wardId", wardId ?? DBNull.Value as object);
            cmd.Parameters.AddWithValue("@locationId", locationId ?? DBNull.Value as object);
            //cmd.Parameters.AddWithValue("@IsMapped", isMapped ?? DBNull.Value as object);
            cmd.Parameters.AddWithValue("@NameOrHolding", NameOrHolding ?? DBNull.Value as object);
            
               cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return dt.AsEnumerable().Select(t=> {
                    return new AssesseePayheadGroup() {
                        AssesseeID =t.Field<long>("AssesseeID"),
                        AssesseeName =t.Field<string>("AssesseeName"),
                        HoldingNo =t.Field<string>("HoldingNo"),
                        IsMapped =t.Field<bool>("IsMapped"),
                        LocationID =t.Field<int>("LocationID"),
                        WardID =t.Field<int>("WardID"),
                        GroupID =t.Field<long?>("GroupID"),
                        GroupName=t.Field<string>("GroupName")
                    };
                }).ToList();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public PayheadGroup AddNewPayHeadGroup(string groupName,int municipailtyId)
        {
            SqlCommand cmd = new SqlCommand("Insert_PayHeadConfigGroup", cn);
            cmd.Parameters.AddWithValue("@GroupName", groupName);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipailtyId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return dt.AsEnumerable().Select(t => {
                        return new PayheadGroup()
                        {
                            GroupID = t.Field<long>("GroupID"),
                            GroupName = t.Field<string>("GroupName"),
                            MunicipalityId = t.Field<int>("MunicipalityId"),
                        };
                    }).ToList()[0];
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        [Obsolete("Because, it support only municipality configuration. please use other overload and get including assessee level configuration also")]
        public List<PayHead> Get_PayHeadDetailLevel(int municipalityID, int qtrNo, string finYear, DateTime paymentDate, DateTime noticeServedOn)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadDetailLevel", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@PaymentDate", paymentDate.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@Qtr", qtrNo);
            cmd.Parameters.AddWithValue("@FinYear", finYear);
            cmd.Parameters.AddWithValue("@NoticeServedOn", noticeServedOn.ToString("yyyy'-'MM'-'dd"));

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<PayHead> Get_PayHeadDetailLevel(int municipalityID, int qtrNo, string finYear, DateTime paymentDate, DateTime noticeServedOn,long AssesseeId)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadDetailLevel1", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@PaymentDate", paymentDate.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@Qtr", qtrNo);
            cmd.Parameters.AddWithValue("@FinYear", finYear);
            cmd.Parameters.AddWithValue("@NoticeServedOn", noticeServedOn.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@AsseesseeId", AssesseeId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<PayHead> Get_PayHeadSummaryLevel(int municipalityID, string finYear, DateTime paymentDate)
        {
            SqlCommand cmd = new SqlCommand("Get_PayHeadsBy_MmncpltyID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@PaymentDate", paymentDate.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@FinYear", finYear);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayheadEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        List<PayHead> ConvertToPayheadEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t => {
                PayHead p = new PayHead();
                p.PayHeadID = t.Field<int?>("PayHeadID");
                p.PayHeadDesc = t.Field<string>("PayHeadDesc");
                p.PayHeadType = t.Field<string>("PayHeadType");
                p.PayRate = t.Field<decimal?>("PayRate");
                //p.GracePeriodInMonth = t.Field<int>("GracePeriodInMonth");
                p.MinLimit = t.Field<decimal?>("MinLimit");
                p.MaxLimit = t.Field<decimal?>("MaxLimit");
                p.ApplicableType = t.Field<string>("ApplicableType");
                p.ApprovalLevel = t.Field<string>("ApprovalLevel");
                p.MunicipalityID = t.Field<int?>("MunicipalityID");
                p.PayHeadBehave = t.Field<string>("PayHeadBehave");
                p.InsertedBy = t.Field<int?>("InsertedBy");
                p.InsertedOn = t.Field<DateTime?>("InsertedOn");
                p.UpdatedBy = t.Field<int?>("UpdatedBy");
                p.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                p.EffectiveDateForm = t.Field<DateTime>("EffectiveDateForm");
                p.EffectiveDateTo = t.Field<DateTime>("EffectiveDateTo");
                p.QtrNo = t.Field<int?>("QtrNo");
                p.FinYear = t.Field<string>("FinYear");
                p.InterestPeriodMethod = t.Field<string>("InterestPeriodMethod");
                p.InterestPeriodRndOffDirection = t.Field<string>("InterestPeriodRndOffDirection");
                return p;
            }).ToList();
        }

        public bool MappAll(int PayheadGroupId, int? WardId, int? LocationId, int municiaplityId)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("Insert_MapAllAssesseePayHeadConfigGroup", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municiaplityId);
            cmd.Parameters.AddWithValue("@GroupID", PayheadGroupId);
            cmd.Parameters.AddWithValue("@WardID", (WardId ?? DBNull.Value as object));
            cmd.Parameters.AddWithValue("@LocationID", (LocationId??DBNull.Value as object));
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Cannot insert duplicate key"))
                    throw new Exception("Some Assessees found mapped with other payhead group");
                else
                    throw;
            }
            catch (Exception)
            {
                throw;// new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public bool UnMapAll(int? PayheadGroupId, int WardId, int? LocationId, int municiaplityId)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("Delete_UnMapAllAssesseePayHeadConfigGroup", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municiaplityId);
            cmd.Parameters.AddWithValue("@GroupID", (PayheadGroupId??DBNull.Value as object));
            cmd.Parameters.AddWithValue("@WardID", WardId);
            cmd.Parameters.AddWithValue("@LocationID", (LocationId ?? 0));
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Cannot insert duplicate key"))
                    throw new Exception("Some Assessees found mapped with other payhead group");
                else
                    throw;
            }
            catch (Exception)
            {
                throw;// new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public bool UnMapGroup(long assesseeId)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("Delete_UnMapAssesseePayHeadConfigGroup", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException)
            {
                throw;// new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public bool MapGroup(int PayheadGroupId, int? WardId, int? LocationId, long AssesseeId,int municiaplityId)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("Insert_MapAssesseePayHeadConfigGroup", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municiaplityId);
            cmd.Parameters.AddWithValue("@GroupID", PayheadGroupId);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeId);
            cmd.Parameters.AddWithValue("@WardID", WardId);
            cmd.Parameters.AddWithValue("@LocationID", LocationId);
            cmd.CommandType = CommandType.StoredProcedure;
            
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch(SqlException ex)
            {
                if (ex.Message.Contains("Cannot insert duplicate key"))
                    throw new Exception(string.Format("Assessee id -{0} already Mapped With Group Id {0}", AssesseeId, PayheadGroupId));
                else
                    throw;
            }
            catch 
            {
                throw;// new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<PayHead> Get_PayHeadsByMuniciplty(int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("GET_PayHeadByMunicipalityWise", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertToPayhead(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<PayHead> ConvertToPayhead(DataTable dt)
        {
            return dt.AsEnumerable().Select(t => {
                PayHead p = new PayHead();
                p.PayHeadID = t.Field<int?>("PayHeadID");
                p.PayHeadDesc = t.Field<string>("PayHeadDesc");
                p.PayHeadType = t.Field<string>("PayHeadType");
                return p;
            }).ToList();
        }

    }
}