﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssmtScoreBeltBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AssmtScoreBeltBLL()
        {
            cn = new SqlConnection(conString);
        }
        public object GetYear()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_AllYear";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Conversion(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        object Conversion(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.Convert< ScoreBeltYear>();
            return lst;
        }
        public object getScoreType()
        {
            SqlCommand cmd = new SqlCommand("taxation.sp_Assmt_GetScoreType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            //cmd.Parameters.AddWithValue("@year", year);
            //cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    ScoreBeltTypeId = t.Field<int>("ScoreBeltTypeId"),
                    ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc"),
                    ScoreBelt = t.Field<string>("ScoreBelt")

                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public object GetScoreByTypeId(string TypeId)
        {
            SqlCommand cmd = new SqlCommand("taxation.GetScoreByTypeId", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            //cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@TypeId", TypeId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    ScoreCode = t.Field<string>("ScoreCode"),
                    ScoreDesc = t.Field<string>("ScoreDesc")
                }).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public object GetEffectivedate(int municipalityId, string year, long userId)
        {
            SqlCommand cmd = new SqlCommand("get_EffectiveDate", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@municipalityId", municipalityId);
            cmd.Parameters.AddWithValue("@year", year);
            cmd.Parameters.AddWithValue("@userId", userId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Conversion1(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        object Conversion1(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                MunicipalityID = t.Field<int>("MunicipalityID"),
                Year = t.Field<int>("YearId"),
                YearDesc = t.Field<string>("YearDesc"),
                EffectiveDateForm = t.Field<DateTime>("EffectiveDateForm"),
                EffectiveDateTo = t.Field<DateTime>("EffectiveDateTo"),
                Factor = t.Field<decimal>("Factor")
            }).ToList();
            return lst;
        }

        public object getBeltTypevalue()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_BeltTypeval";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.Convert<BeltTypevalue>();
                return lst;
            }
            catch 
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<AssmtScoreBelt> ScoreView(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@TransactionType", AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@municipalityId", AssmtScoreBeltObj.municipalityId);
                cmd.Parameters.AddWithValue("@year", AssmtScoreBeltObj.year);
                cmd.Parameters.AddWithValue("@UserID", AssmtScoreBeltObj.UserID);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    ScoreSheetID = t.Field<Int64?>("ScoreSheetID"),
                    ScoreBeltTypeId = t.Field<int?>("ScoreBeltTypeId"),
                    ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc"),
                    ScoreCode = t.Field<string>("ScoreCode"),
                    ScoreDesc = t.Field<string>("ScoreDesc"),
                    ScoreValue = t.Field<decimal?>("ScoreValue"),
                }).ToList();
                return lst;

            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreBelt> BeltView(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@TransactionType", AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@municipalityId", AssmtScoreBeltObj.municipalityId);
                cmd.Parameters.AddWithValue("@year", AssmtScoreBeltObj.year);
                cmd.Parameters.AddWithValue("@UserID", AssmtScoreBeltObj.UserID);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    BeltSheetID = t.Field<Int64?>("BeltSheetID"),
                    ScoreBeltTypeId=t.Field<int?>("ScoreBeltTypeId"),
                    ScoreBeltTypeDesc = t.Field<string>("ScoreBeltTypeDesc"),
                    FromRange = t.Field<decimal?>("FromRange"),
                    ToRange = t.Field<decimal?>("ToRange"),
                    BeltValue = t.Field<string>("BeltValue"),
                    BeltDesc = t.Field<string>("BeltDesc")

                }).ToList();
                return lst;

            }
            catch 
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreBelt> ScoreDel(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@TransactionType", string.IsNullOrEmpty(AssmtScoreBeltObj.TransactionType) ? DBNull.Value : (object)AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@ScoreSheetID", AssmtScoreBeltObj.ScoreSheetID);
                //cmd.Parameters.AddWithValue("@FinYear", string.IsNullOrEmpty(BudgetObj.FinYear) ? DBNull.Value : (object)BudgetObj.FinYear);
                //cmd.Parameters.AddWithValue("@BugetTypeID", BudgetObj.BugetTypeID ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@BudgetDate", string.IsNullOrEmpty(BudgetObj.BudgetDate) ? DBNull.Value : (object)BudgetObj.BudgetDate);
                //cmd.Parameters.AddWithValue("@CostCenterCode", BudgetObj.CostCenterCode ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@DeptCode", BudgetObj.DeptCode ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@MAccountCode", string.IsNullOrEmpty(BudgetObj.MAccountCode) ? DBNull.Value : (object)BudgetObj.MAccountCode);
                //cmd.Parameters.AddWithValue("@AccountCode", string.IsNullOrEmpty(BudgetObj.AccountCode) ? DBNull.Value : (object)BudgetObj.AccountCode);
                //cmd.Parameters.AddWithValue("@Amt", Convert.ToDecimal(BudgetObj.Amt));
                //cmd.Parameters.AddWithValue("@WardNo", BudgetObj.WardNo ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@radioVal", BudgetObj.radioVal ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@SectorID", BudgetObj.SectorID ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@UserID", BudgetObj.UserID ?? DBNull.Value as object);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    msg = t.Field<string>("msg")

                }).ToList();
                return lst;

            }
            catch 
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreBelt> BeltDel(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@TransactionType", string.IsNullOrEmpty(AssmtScoreBeltObj.TransactionType) ? DBNull.Value : (object)AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@BeltSheetID", AssmtScoreBeltObj.BeltSheetID);
                //cmd.Parameters.AddWithValue("@FinYear", string.IsNullOrEmpty(BudgetObj.FinYear) ? DBNull.Value : (object)BudgetObj.FinYear);
                //cmd.Parameters.AddWithValue("@BugetTypeID", BudgetObj.BugetTypeID ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@BudgetDate", string.IsNullOrEmpty(BudgetObj.BudgetDate) ? DBNull.Value : (object)BudgetObj.BudgetDate);
                //cmd.Parameters.AddWithValue("@CostCenterCode", BudgetObj.CostCenterCode ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@DeptCode", BudgetObj.DeptCode ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@MAccountCode", string.IsNullOrEmpty(BudgetObj.MAccountCode) ? DBNull.Value : (object)BudgetObj.MAccountCode);
                //cmd.Parameters.AddWithValue("@AccountCode", string.IsNullOrEmpty(BudgetObj.AccountCode) ? DBNull.Value : (object)BudgetObj.AccountCode);
                //cmd.Parameters.AddWithValue("@Amt", Convert.ToDecimal(BudgetObj.Amt));
                //cmd.Parameters.AddWithValue("@WardNo", BudgetObj.WardNo ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@radioVal", BudgetObj.radioVal ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@SectorID", BudgetObj.SectorID ?? DBNull.Value as object);
                //cmd.Parameters.AddWithValue("@UserID", BudgetObj.UserID ?? DBNull.Value as object);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    msg = t.Field<string>("msg")

                }).ToList();
                return lst;

            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreBelt> ScoreInsUpd(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@municipalityId", AssmtScoreBeltObj.municipalityId ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@year",string.IsNullOrEmpty(AssmtScoreBeltObj.year) ? DBNull.Value:(object)AssmtScoreBeltObj.year);
                cmd.Parameters.AddWithValue("@TransactionType", string.IsNullOrEmpty(AssmtScoreBeltObj.TransactionType) ? DBNull.Value : (object)AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@ScoreSheetID", AssmtScoreBeltObj.ScoreSheetID);
                cmd.Parameters.AddWithValue("@ScoreBeltTypeId", AssmtScoreBeltObj.ScoreBeltTypeId ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@ScoreCode", string.IsNullOrEmpty(AssmtScoreBeltObj.ScoreCode) ? DBNull.Value : (object)AssmtScoreBeltObj.ScoreCode);
                cmd.Parameters.AddWithValue("@ScoreDesc", string.IsNullOrEmpty(AssmtScoreBeltObj.ScoreDesc) ? DBNull.Value : (object)AssmtScoreBeltObj.ScoreDesc);
                cmd.Parameters.AddWithValue("@ScoreValue", Convert.ToDecimal(AssmtScoreBeltObj.ScoreValue));
                cmd.Parameters.AddWithValue("@UserID", AssmtScoreBeltObj.UserID ?? DBNull.Value as object);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    msg = t.Field<string>("msg")

                }).ToList();
                return lst;

            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AssmtScoreBelt> BeltInsUpd(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[taxation].[sp_Assmt_ScoreBelt]";
                cmd.Parameters.AddWithValue("@municipalityId", AssmtScoreBeltObj.municipalityId ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@year", string.IsNullOrEmpty(AssmtScoreBeltObj.year) ? DBNull.Value : (object)AssmtScoreBeltObj.year);
                cmd.Parameters.AddWithValue("@TransactionType", string.IsNullOrEmpty(AssmtScoreBeltObj.TransactionType) ? DBNull.Value : (object)AssmtScoreBeltObj.TransactionType);
                cmd.Parameters.AddWithValue("@BeltSheetID", AssmtScoreBeltObj.BeltSheetID);
                cmd.Parameters.AddWithValue("@ScoreBeltTypeId", AssmtScoreBeltObj.ScoreBeltTypeId ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@FromRange", Convert.ToDecimal(AssmtScoreBeltObj.FromRange));
                cmd.Parameters.AddWithValue("@ToRange", Convert.ToDecimal(AssmtScoreBeltObj.ToRange));
                cmd.Parameters.AddWithValue("@BeltValue", string.IsNullOrEmpty(AssmtScoreBeltObj.BeltValue) ? DBNull.Value : (object)AssmtScoreBeltObj.BeltValue);
                cmd.Parameters.AddWithValue("@UserID", AssmtScoreBeltObj.UserID ?? DBNull.Value as object);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new AssmtScoreBelt
                {
                    msg = t.Field<string>("msg")

                }).ToList();
                return lst;

            }
            catch 
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

    }
}