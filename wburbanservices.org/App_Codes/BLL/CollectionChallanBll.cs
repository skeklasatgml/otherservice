﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Valuation_Board.Models.Report;

namespace Valuation_Board.App_Codes.BLL
{
    public class CollectionChallanBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public CollectionChallanBll()
        {
            cn = new SqlConnection(conString);
        }
        public List<Collection> GetCollectionDtls(int Muid, string DtF, string DtT, int? WID, int? LID, string PType, string Counter)
        {
            List<Collection> cLst = new List<Collection>();
            SqlCommand cmd = new SqlCommand("Get_DailyCollectionChallanForExcel", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", Muid);
            cmd.Parameters.AddWithValue("@WardNo", (WID ?? 0));
            cmd.Parameters.AddWithValue("@LocationNo", (LID ?? 0));
            cmd.Parameters.AddWithValue("@ReceiptFromDate", DtF);
            cmd.Parameters.AddWithValue("@ReceiptToDate", DtT);
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@PaymentType", PType);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 4000;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                //SqlDataReader sdr = cmd.ExecuteReader();
                //dt.Load(sdr);
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    sda.Fill(dt);
                }
                cLst = ConvertToCollection(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return cLst;
        }
        private List<Collection> ConvertToCollection(DataTable dt)
        {
            List<Collection> cLst = new List<Collection>();
            try
            {
                cLst = dt.AsEnumerable().Select(t =>
                {
                    var objlst = new Collection();
                    objlst.MunicipalityID = t.Field<int>("MunicipalityID");
                    objlst.MunicipalityName = t.Field<string>("MunicipalityName");
                    objlst.HoldingNo = t.Field<string>("HoldingNo");
                    objlst.HeadingDate = t.Field<string>("HeadingDate");
                    objlst.PaymentReceiveDate = t.Field<string>("PaymentReceiveDate");
                    objlst.receiptNo = t.Field<string>("receiptNo");
                    objlst.finyear = t.Field<string>("finyear");
                    objlst.PaymentType = t.Field<string>("PaymentType");
                    objlst.NoOfPayment = t.Field<int>("NoOfPayment");
                    objlst.PaymentTypeDesc = t.Field<string>("PaymentTypeDesc");
                    objlst.LocationID = t.Field<int>("LocationID");
                    objlst.LocationName = t.Field<string>("LocationName");
                    objlst.WardID = t.Field<int>("WardID");
                    objlst.WardName = t.Field<string>("WardName");
                    objlst.AssesseeName = t.Field<string>("AssesseeName");
                    objlst.Arr_tax = t.Field<decimal>("Arr_tax").ToString("0.00");
                    objlst.Arr_sur = t.Field<decimal>("Arr_sur").ToString("0.00");
                    objlst.Qtr1_Ptax = t.Field<decimal>("Qtr1_Ptax").ToString("0.00");
                    objlst.Qtr1_sur = t.Field<decimal>("Qtr1_sur").ToString("0.00");
                    objlst.Qtr2_Ptax = t.Field<decimal>("Qtr2_Ptax").ToString("0.00");
                    objlst.Qtr2_sur = t.Field<decimal>("Qtr2_sur").ToString("0.00");
                    objlst.Qtr3_Ptax = t.Field<decimal>("Qtr3_Ptax").ToString("0.00");
                    objlst.Qtr3_sur = t.Field<decimal>("Qtr3_sur").ToString("0.00");
                    objlst.Qtr4_Ptax = t.Field<decimal>("Qtr4_Ptax").ToString("0.00");
                    objlst.Qtr4_sur = t.Field<decimal>("Qtr4_sur").ToString("0.00");
                    objlst.Rebate = t.Field<decimal>("Rebate").ToString("0.00");
                    objlst.Interest = t.Field<decimal>("Interest").ToString("0.00");
                    objlst.ArrearEduCess = t.Field<decimal>("ArrearEduCess").ToString("0.00");
                    objlst.CurrentEduCess = t.Field<decimal>("CurrentEduCess").ToString("0.00");
                    objlst.WarrantPenalty = t.Field<decimal>("WarrantPenalty").ToString("0.00");
                    objlst.ExcessAdj = t.Field<decimal>("ExcessAdj").ToString("0.00");
                    objlst.CollectorName = t.Field<string>("CollectorName");
                    objlst.CounterID = t.Field<int>("CounterID");
                    objlst.TotalTax = t.Field<decimal>("TotalTax").ToString("0.00");
                    objlst.TotalSur = t.Field<decimal>("TotalSur").ToString("0.00");
                    objlst.CounterCode = t.Field<string>("CounterCode");
                    objlst.UserID = t.Field<Int64>("UserID").ToString();
                    objlst.UserName = t.Field<string>("UserName");
                    objlst.BillReceiptNo = t.Field<string>("BillReceiptNo");
                    objlst.AdjustedAmount = t.Field<decimal>("AdjustedAmount").ToString("0.00");
                    objlst.CollectorAdjustedAmount = t.Field<decimal>("CollectorAdjustedAmount").ToString("0.00");
                    objlst.AdvanceAmount = t.Field<decimal>("AdvanceAmount").ToString("0.00");
                    objlst.NetAmount = t.Field<decimal>("NetAmount").ToString("0.00");
                    objlst.PaymentID = t.Field<Int64>("PaymentID");
                    objlst.RoundOffAdjust = t.Field<decimal>("RoundOffAdjust").ToString("0.00");
                    objlst.ReceiptFromDate = t.Field<string>("ReceiptFromDate");
                    objlst.ReceiptToDate = t.Field<string>("ReceiptToDate");
                    objlst.Cash = t.Field<decimal>("Cash").ToString("0.00");
                    objlst.Cheque = t.Field<decimal>("Cheque").ToString("0.00");
                    objlst.Others = t.Field<decimal>("Others").ToString("0.00");
                    objlst.HoldingTypeID = t.Field<int>("HoldingTypeID");
                    objlst.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                    return objlst;
                }).ToList();
                return cLst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}