﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.App_Codes.BLL
{
    public class MakePayment_Bll
    {
        private IEncryptDecrypt chiperAlgorithm;
        DbHandler _db = null;
        SqlConnection cn = null;

        public MakePayment_Bll()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
            _db = new DbHandler();
            cn = new SqlConnection(_db.ConnectionString);
        }

        public Service_Model GetServiceInfo(string MunicipalityID, string ServiceID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Get_ServiceDetails", cn);
            cmd.Parameters.AddWithValue("@ServApplicationID", ServiceID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return ConvertService(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }          
        }

        public List<Service_Model> ConvertService(DataTable dt)
        {
            List<Service_Model> lst = new List<Service_Model>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Service_Model();
                obj.MunicipalityDevAuthoId = t.Field<int>("MunicipalityDevAuthoId");
                obj.ServMstId = t.Field<long>("ServMstId");
                obj.SubModuleID = t.Field<int>("SubModuleID");
                obj.ApplicationNo = t.Field<string>("ApplicationNo");
                obj.PurposeOfService = t.Field<int>("PurposeOfService");
                obj.NameOfProject = t.Field<string>("NameOfProject");
                obj.NameOfOrganisation = t.Field<string>("NameOfOrganisation");
                obj.ServiceCurrStatus = t.Field<string>("ServiceCurrStatus");
                obj.TotalPaidAmount = t.Field<decimal>("PaidAmount");
                obj.TransactionID = t.Field<string>("TransactionID");
                return obj;
            }).ToList();
            return lst;
        }

        public long? MakePayment(Service_Model service_Model, string PG, long userID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Insert_MST_Payment", cn);
            cmd.Parameters.AddWithValue("@PaymentType", "O");
            cmd.Parameters.AddWithValue("@ServMstId", service_Model.ServMstId);
            cmd.Parameters.AddWithValue("@MunicipalityID", service_Model.MunicipalityDevAuthoId);
            cmd.Parameters.AddWithValue("@SubModuleID", service_Model.SubModuleID);
            cmd.Parameters.AddWithValue("@ApplicationNo", service_Model.ApplicationNo);
            //cmd.Parameters.AddWithValue("@PaymentCollectionDate", ServiceID);
            cmd.Parameters.AddWithValue("@PaidAmount", service_Model.TotalPaidAmount);
            cmd.Parameters.AddWithValue("@CollectorID", userID);
            cmd.Parameters.AddWithValue("@TransactionID", service_Model.TransactionID);
            cmd.Parameters.AddWithValue("@IsPaymentSuccessfull", 0);
            cmd.Parameters.AddWithValue("@PgCode", PG);
            cmd.Parameters.AddWithValue("@AppOrigin", "WEB");
            cmd.Parameters.AddWithValue("@InsertedBy", userID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt64(dt.Rows[0]["PaymentID"].ToString());
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public PG_ModelOtherServices Get_PGModel(string transactionId, string serviceID)
        {
            SqlCommand cmd = new SqlCommand("UDServices.Get_OnlinePGInputDataForOtherServices", cn);
            cmd.Parameters.AddWithValue("@transactionID", transactionId);
            cmd.Parameters.AddWithValue("@ServiceID", Convert.ToInt64(serviceID));
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return ConvertPGModel(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<PG_ModelOtherServices> ConvertPGModel(DataTable dt)
        {
            List<PG_ModelOtherServices> lst = new List<PG_ModelOtherServices>();
            foreach (DataRow dr in dt.Rows)
            {
                PG_ModelOtherServices pg = new PG_ModelOtherServices();
                pg.CustomerID = dr.Field<string>("CustomerID");
                pg.TxnAmount = dr.Field<decimal>("PaidAmount").ToString();
                pg.ServMstId = dr.Field<long>("ServMstId").ToString();
                pg.SubModuleID = dr.Field<int>("SubModuleID").ToString();
                pg.MunicipalityID = dr.Field<int>("MunicipalityID").ToString();
                pg.NameOfProject = dr.Field<string>("NameOfProject").ToString();
                pg.PaymentID = dr.Field<string>("PaymentID").ToString();
                //pg.UserID = dr.Field<long>("UserID").ToString();
                pg.SubModuleName = dr.Field<string>("SubModuleName");
                lst.Add(pg);
            }
            return lst;
        }

    }
}