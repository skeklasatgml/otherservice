﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Valuation_Board.ViewModels;
using System.Globalization;

namespace Valuation_Board.App_Codes.BLL
{
    public class ScheduleIIIBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ScheduleIIIBLL()
        {
            cn = new SqlConnection(conString);
        }

        public DataTable GetMuncplty()
        {
            SqlCommand cmd = new SqlCommand("Get_Muncplty", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;

            }
            catch (SqlException se)
            {
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_MstScheduleIII()
        {
            SqlCommand cmd = new SqlCommand("Get_MstScheIII_Appli_Window", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;

            }
            catch (SqlException se)
            {
                throw new Exception("Database Error...!" + se.Message);
            }
            finally
            {
                cn.Close();
            }
        }


        public DataTable SaveUpdate_ScheduleIII(int AppliWindowId, int MunicipalityID, string FromDate, string ToDate, long userId)
        {
            SqlCommand cmd = new SqlCommand("INSERT_MstScheIII_Appli_Window", cn);
            cmd.Parameters.AddWithValue("@AppliWindowId", AppliWindowId);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@ScheIIIStartDt", FromDate);
            cmd.Parameters.AddWithValue("@ScheIIIDt", ToDate);
            cmd.Parameters.AddWithValue("@InsertedBy", userId);
            cmd.Parameters.AddWithValue("@UpdatedBy", userId);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }

            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}