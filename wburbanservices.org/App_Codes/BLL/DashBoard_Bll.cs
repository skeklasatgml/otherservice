﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.ViewModels;

namespace Valuation_Board.App_Codes.BLL
{
    public class DashBoard_Bll
    {
        SqlConnection cn;
        DbHandler _db;
        public DashBoard_Bll()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }
        public long CountTotalAssessesAllMunicipality()
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_TotalAssesseeCount", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var x = Convert.ToInt64(cmd.ExecuteScalar());
                return x;
            }
            finally
            {
                cn.Close();
            }
        }
        public int TotalMuniciaplityCount()
        {

            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_TotalMunicilaityCount", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var x = Convert.ToInt32(cmd.ExecuteScalar());
                return x;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.AssesseesByDistrict> AssesseCountInfoGroupByDistricts(DateTime FromDate, DateTime ToDate)
        {
            List<Dashboard_vm.AssesseesByDistrict> lstVm = new List<Dashboard_vm.AssesseesByDistrict>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_AssesseCountInfoGroupByDistricts", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@toDate", ToDate.ToString("yyyy'-'MM'-'dd"));

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.AssesseesByDistrict o = new Dashboard_vm.AssesseesByDistrict();
                    o.DistrictId = t.Field<int>("DistrictID");
                    o.DistrictName = t.Field<string>("DistrictName");
                    o.NewAssesseeCount = t.Field<int>("newAssesseeCount");
                    o.TotalAssesseeCount = t.Field<int>("oldAssesseeCount") + t.Field<int>("newAssesseeCount");

                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.AssesseesByMuncipaity> AssesseCountInfoGroupByMunicipalities(int DistrictId, DateTime FromDate, DateTime ToDate)
        {
            List<Dashboard_vm.AssesseesByMuncipaity> lstVm = new List<Dashboard_vm.AssesseesByMuncipaity>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_AssesseCountInfoGroupByMunicipalities", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@toDate", ToDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@districtId", DistrictId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.AssesseesByMuncipaity o = new Dashboard_vm.AssesseesByMuncipaity();
                    o.MunicipalityId = t.Field<int>("MunicipalityID");
                    o.MunicipalityName = t.Field<string>("MunicipalityName");
                    o.NewAssesseeCount = t.Field<int>("newAssesseeCount");
                    o.TotalAssesseeCount = t.Field<int>("oldAssesseeCount") + t.Field<int>("newAssesseeCount");
                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.AssesseesByMuncipaityWard> AssesseesCountInfoByWards(int MunicipalityId, DateTime FromDate, DateTime ToDate)
        {
            List<Dashboard_vm.AssesseesByMuncipaityWard> lstVm = new List<Dashboard_vm.AssesseesByMuncipaityWard>();

            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_AssesseesCountInfoByWards", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@toDate", ToDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@MunicipalityId", MunicipalityId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.AssesseesByMuncipaityWard o = new Dashboard_vm.AssesseesByMuncipaityWard();
                    o.MunicipalityId = t.Field<int>("MunicipalityID");
                    o.MunicipalityName = t.Field<string>("MunicipalityName");
                    o.Ward = t.Field<string>("WardName");
                    o.WardId = t.Field<int>("WardID");
                    o.NewAssesseeCount = t.Field<int>("newAssesseeCount");
                    o.TotalAssesseeCount = t.Field<int>("oldAssesseeCount") + t.Field<int>("newAssesseeCount");
                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.Municipality> GetMunicipalityList(int? countRow)
        {
            List<Dashboard_vm.Municipality> lstVm = new List<Dashboard_vm.Municipality>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_MunicipalitiesDescByDate", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RowCount", countRow??DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.Municipality o = new Dashboard_vm.Municipality();
                    o.MunicipalityId = t.Field<int>("MunicipalityID");
                    o.MunicipalityName = t.Field<string>("MunicipalityName");
                    o.InsertedOn = t.Field<DateTime?>("InsertedOn");
                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.MunicipalityValuation> GetMunicipalityValuations()
        {

            List<Dashboard_vm.MunicipalityValuation> lstVm = new List<Dashboard_vm.MunicipalityValuation>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_GetMunicipalityValuations", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.MunicipalityValuation o = new Dashboard_vm.MunicipalityValuation();
                    o.MunicipalityId = t.Field<int>("MunicipalityID");
                    o.MunicipalityName = t.Field<string>("MunicipalityName");
                    o.TotalAssesseeCount = t.Field<int>("TotalAssesseeCount");
                    o.TotalValuation = t.Field<decimal>("TotalValueation");
                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.AssesseesByWardMonthWise> AssesseCountInfoByWardMonthWise(int MunicipalityId, int WardId, DateTime FromDate, DateTime ToDate)
        {
            List<Dashboard_vm.AssesseesByWardMonthWise> lstVm = new List<Dashboard_vm.AssesseesByWardMonthWise>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_NewAddedAssessesCountInfoByWardMonthWise", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", FromDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@toDate", ToDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@MunicipalityId", MunicipalityId);
                cmd.Parameters.AddWithValue("@wardId", WardId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.AssesseesByWardMonthWise o = new Dashboard_vm.AssesseesByWardMonthWise();
                    o.MunicipalityId = t.Field<int>("MunicipalityID");
                    o.MunicipalityName = t.Field<string>("MunicipalityName");
                    o.Ward = t.Field<string>("WardName");
                    o.WardId = t.Field<int>("WardID");
                    o.NewAssesseeCount = t.Field<int>("newAssesseeCount");
                    o.MonthYear = t.Field<string>("MonthYear");
                    return o;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.Assessee> AssesseListByWardMonthYear(int MunicipalityId, int WardId, string MonthYear)
        {

            AssesseeBll bll = new AssesseeBll();
            List<Dashboard_vm.Assessee> lstVm = new List<Dashboard_vm.Assessee>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_Dashboard_AssesseListByWardMonthYear", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MonthYear", MonthYear);
                cmd.Parameters.AddWithValue("@MunicipalityId", MunicipalityId);
                cmd.Parameters.AddWithValue("@wardId", WardId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.Assessee b = new Dashboard_vm.Assessee();
                    b.AssesseeId = t.Field<long>("AssesseeId");
                    b.AssesseeNo = t.Field<string>("AssesseeNo");
                    b.EmailID = t.Field<string>("EmailID");
                    b.HoldingNo = t.Field<string>("HoldingNo");
                    b.LocationId = t.Field<int?>("LocationID");
                    b.MunicipalityId = t.Field<int?>("MunicipalityID");
                    b.Name = t.Field<string>("Name");
                    b.PhoneNo = t.Field<string>("PhoneNo");
                    b.Ward = t.Field<string>("Ward");
                    b.AssesseeAddress = t.Field<string>("AssesseeAddress");
                    b.PropertyTax = t.Field<decimal>("PropertyTax");
                    b.Surcharge = t.Field<decimal>("Surcharge");
                    b.Location = t.Field<string>("Location");
                    return b;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.MunicipalityWiseCollection2> GetCollectionSummary(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate)
        {
            List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new List<Dashboard_vm.MunicipalityWiseCollection2>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetCollectionSummary", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate",(FromDate==null? DBNull.Value as object: FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds!=null?string.Join(",",MunicipalityIds):DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.MunicipalityWiseCollection2 b = new Dashboard_vm.MunicipalityWiseCollection2();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");

                    return b;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.MunicipalityWiseCollection2> GetTotalCollection(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate)
        {
            List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new List<Dashboard_vm.MunicipalityWiseCollection2>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetTotalCollection", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.MunicipalityWiseCollection2 b = new Dashboard_vm.MunicipalityWiseCollection2();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");

                    return b;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.MunicipalityWiseCollection2> MunicipalityWiseCollection(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate,int RowCount,string OrderBy)
        {
            List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new List<Dashboard_vm.MunicipalityWiseCollection2>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetCollectionSummaryByMunicipality", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@rowCount", RowCount);
                cmd.Parameters.AddWithValue("@orderBy", OrderBy??DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.MunicipalityWiseCollection2 b = new Dashboard_vm.MunicipalityWiseCollection2();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");

                    return b;
                }).ToList();

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.WardWiseCollection> WardWiseCollection(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate, int? RowCount, string OrderBy)
        {
            List<Dashboard_vm.WardWiseCollection> lstVm = new List<Dashboard_vm.WardWiseCollection>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetCollectionSummary_WardWise", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.WardWiseCollection b = new Dashboard_vm.WardWiseCollection();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");
                    b.WardId = t.Field<int>("WardID");
                    b.WardName = t.Field<string>("WardName");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");

                    return b;
                }).ToList();
                if(!string.IsNullOrEmpty(OrderBy))
                {
                    if (OrderBy.Equals("desc", StringComparison.OrdinalIgnoreCase))
                    {
                        lstVm = lstVm.OrderByDescending(t => t.NetPaidAmount).ToList();
                    }
                    else
                    {
                        lstVm = lstVm.OrderByDescending(t => t.NetPaidAmount).ToList();
                    }
                }
                if(RowCount>0)
                {
                    lstVm = lstVm.Take(RowCount ?? 0).ToList();
                }

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.CollectorWiseCollection> CollectorWiseCollection(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate, int? RowCount, string OrderBy)
        {
            List<Dashboard_vm.CollectorWiseCollection> lstVm = new List<Dashboard_vm.CollectorWiseCollection>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetCollectionSummary_ByCollector", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.CollectorWiseCollection b = new Dashboard_vm.CollectorWiseCollection();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");

                    b.CollectorId = t.Field<long>("CollectorId");
                    b.CollectorName = t.Field<string>("CollectorName");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");
                    return b;
                }).ToList();
                if (!string.IsNullOrEmpty(OrderBy))
                {
                    if (OrderBy.Equals("desc", StringComparison.OrdinalIgnoreCase))
                    {
                        lstVm = lstVm.OrderByDescending(t => t.NetPaidAmount).ToList();
                    }
                    else
                    {
                        lstVm = lstVm.OrderByDescending(t => t.NetPaidAmount).ToList();
                    }
                }
                if (RowCount > 0)
                {
                    lstVm = lstVm.Take(RowCount ?? 0).ToList();
                }

                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.MunicipalityWiseCollection2> AppOrigineWiseCollection(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate)
        {
            List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new List<Dashboard_vm.MunicipalityWiseCollection2>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_GetCollectionSummary_origineWise", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.MunicipalityWiseCollection2 b = new Dashboard_vm.MunicipalityWiseCollection2();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.AppOrigine = t.Field<string>("AppOrigin");
                    b.MunicipalityId = t.Field<int>("MunicipalityID");
                    b.MunicipalityName = t.Field<string>("MunicipalityName");
                    return b;
                }).ToList();
                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.CollectionFlowByTimeSpan> GetCollectionFlowTimeSpan(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate,long? CollectorID,List<int>WardIds,string dataSheetType)
        {
            List<Dashboard_vm.CollectionFlowByTimeSpan> lstVm = new List<Dashboard_vm.CollectionFlowByTimeSpan>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_CollectionPaymentFlowByTimeSpan", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@CollectorId", CollectorID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@wardIds", WardIds != null ? string.Join(",", WardIds) : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@dataSetType", dataSheetType ?? DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.CollectionFlowByTimeSpan b = new Dashboard_vm.CollectionFlowByTimeSpan();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.FinYear = t.Field<string>("FinYear");
                    b.DtSpanBase = t.Field<string>("DtSpanBase");
                    return b;
                }).ToList();
                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Dashboard_vm.CollectionFlowByTimeSpan> GetCollectionFlowTimeSpanPayheadWise(List<int> MunicipalityIds, DateTime? FromDate, DateTime? ToDate, long? CollectorID, List<int> WardIds, string dataSheetType)
        {
            List<Dashboard_vm.CollectionFlowByTimeSpan> lstVm = new List<Dashboard_vm.CollectionFlowByTimeSpan>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("get_dashBoard_CollectionFlowByTimeSpan_PayheadWise", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fromDate", (FromDate == null ? DBNull.Value as object : FromDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@toDate", (ToDate == null ? DBNull.Value as object : ToDate.Value.ToString("yyyy'-'MM'-'dd") as object));
                cmd.Parameters.AddWithValue("@CollectorId", CollectorID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@wardIds", WardIds != null ? string.Join(",", WardIds) : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@MunicipalityIds", MunicipalityIds != null ? string.Join(",", MunicipalityIds) : DBNull.Value as object);
                cmd.Parameters.AddWithValue("@dataSetType", dataSheetType ?? DBNull.Value as object);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                lstVm = dt.AsEnumerable().Select(t =>
                {
                    Dashboard_vm.CollectionFlowByTimeSpan b = new Dashboard_vm.CollectionFlowByTimeSpan();
                    b.ArrPropTax = t.Field<decimal>("ArrPropTax");
                    b.ArrSurcharge = t.Field<decimal>("ArrSurcharge");
                    b.CurPropTax = t.Field<decimal>("CurPropTax");
                    b.CurSurcharge = t.Field<decimal>("CurSurcharge");

                    b.OtherAdjustment = t.Field<decimal>("OtherAdjustedAmount");
                    b.Rebate = t.Field<decimal>("Rebate");
                    b.Interest = t.Field<decimal>("Interest");
                    b.NetPaidAmount = t.Field<decimal>("NetPaidAmount");
                    b.FinYear = t.Field<string>("FinYear");
                    b.DtSpanBase = t.Field<string>("DtSpanBase");
                    return b;
                }).ToList();
                return lstVm;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.DemandVsCollectionTotalSummary> DemandVsCollectionTotalSummaryBll(int MunicipalityId)
        {
            List<Dashboard_vm.DemandVsCollectionTotalSummary> lstDvP = new List<Dashboard_vm.DemandVsCollectionTotalSummary>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_DemandVsPaymentTotalSummary", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                lstDvP = ds.Tables[0].AsEnumerable().Select(t =>
                {
                    Dashboard_vm.DemandVsCollectionTotalSummary b = new Dashboard_vm.DemandVsCollectionTotalSummary();
                    b.TotalDemand = t.Field<decimal?>("TotalDemand");
                    b.TotalCollection = t.Field<decimal?>("TotalCollection");
                    b.ArrearDemand = t.Field<decimal?>("ArrearDemand");
                    b.ArrearCollection = t.Field<decimal?>("ArrearCollection");
                    b.CurrentDemand = t.Field<decimal?>("CurrentDemand");
                    b.CurrentCollection = t.Field<decimal?>("CurrentCollection");
                    return b;
                }).ToList();
                return lstDvP;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Dashboard_vm.DemandVsCollectionTotalSummary> DemandVsCollectionWardWiseSummaryBll(int MunicipalityId)
        {
            List<Dashboard_vm.DemandVsCollectionTotalSummary> lstDvP = new List<Dashboard_vm.DemandVsCollectionTotalSummary>();
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_DemandVsPaymentTotalSummary", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                lstDvP = ds.Tables[1].AsEnumerable().Select(t =>
                {
                    Dashboard_vm.DemandVsCollectionTotalSummary b = new Dashboard_vm.DemandVsCollectionTotalSummary();
                    b.TotalDemand = t.Field<decimal?>("TotalDemand");
                    b.TotalCollection = t.Field<decimal?>("TotalCollection");
                    b.WardID = t.Field<string>("WardID");
                    b.ArrearDemand = t.Field<decimal?>("ArrearDemand");
                    b.ArrearCollection = t.Field<decimal?>("ArrearCollection");
                    b.CurrentDemand = t.Field<decimal?>("CurrentDemand");
                    b.CurrentCollection = t.Field<decimal?>("CurrentCollection");
                    return b;
                }).ToList();
                return lstDvP;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}