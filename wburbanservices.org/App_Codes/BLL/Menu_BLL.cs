﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class Menu_BLL
    {

        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public Menu_BLL()
        {
            cn = new SqlConnection(constring);
        }
        public List<Menu> getMenuDrtails(long userId, int userTypeId)
        {

            SqlCommand cmd = new SqlCommand("Get_Menu_RoleWise", cn);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userTypeId", userTypeId);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Menu> lstMenu = new List<Menu>();
            try
            {
                cn.Open();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)

                {
                    Menu menu = new Menu();
                    menu.MenuID = row.Field<int>("MenuID");
                    menu.MenuName = row.Field<string>("MenuName");
                    menu.ParrentMenuID = row.Field<int?>("ParrentMenuID");
                    menu.ActionMethodID = row.Field<int?>("ActionMethodID");
                    menu.Rank = row.Field<int>("Rank");
                    menu.IsActive = (row.Field<byte>("IsActive") == 1 ? true : false);
                    menu.Url = (string.IsNullOrWhiteSpace(row.Field<string>("Directory")) ? "" : string.Format("/{0}", row.Field<string>("Directory"))) + string.Format("/{0}/{1}/", row.Field<string>("ControllerName"), row.Field<string>("ActionName"));
                    lstMenu.Add(menu);
                }
                var mainList = GetMenuTree(lstMenu, null);

                return mainList;
                //JavaScriptSerializer js = new JavaScriptSerializer();
                //return js.Serialize(mainList);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Menu> getMenuDrtails(long userId, int userTypeId,string appTag)
        {

            SqlCommand cmd = new SqlCommand("Get_Menu_RoleWiseAppTag", cn);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userTypeId", userTypeId);
            cmd.Parameters.AddWithValue("@appTag", appTag);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Menu> lstMenu = new List<Menu>();
            try
            {
                cn.Open();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)

                {
                    Menu menu = new Menu();
                    menu.MenuID = row.Field<int>("MenuID");
                    menu.MenuName = row.Field<string>("MenuName");
                    menu.ParrentMenuID = row.Field<int?>("ParrentMenuID");
                    menu.ActionMethodID = row.Field<int?>("ActionMethodID");
                    menu.Rank = row.Field<int>("Rank");
                    menu.IsActive = (row.Field<byte>("IsActive") == 1 ? true : false);
                    menu.Url = (string.IsNullOrWhiteSpace(row.Field<string>("Directory")) ? "" : string.Format("/{0}", row.Field<string>("Directory"))) + string.Format("/{0}/{1}/", row.Field<string>("ControllerName"), row.Field<string>("ActionName"));
                    lstMenu.Add(menu);
                }
                var mainList = GetMenuTree(lstMenu, null);

                return mainList;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Menu> getMenuDrtails(long userId, int userTypeId, string appTag,int? moduleId)
        {

            SqlCommand cmd = new SqlCommand("Get_Menu_RoleWiseAppTagModuleId", cn);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userTypeId", userTypeId);
            cmd.Parameters.AddWithValue("@appTag", appTag);
            cmd.Parameters.AddWithValue("@moduleId", moduleId.ReplaceDbNull());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Menu> lstMenu = new List<Menu>();
            try
            {
                cn.Open();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)

                {
                    Menu menu = new Menu();
                    menu.MenuID = row.Field<int>("MenuID");
                    menu.MenuName = row.Field<string>("MenuName");
                    menu.ParrentMenuID = row.Field<int?>("ParrentMenuID");
                    menu.ActionMethodID = row.Field<int?>("ActionMethodID");
                    menu.Rank = row.Field<int>("Rank");
                    menu.IsActive = (row.Field<byte>("IsActive") == 1 ? true : false);
                    menu.Url = (string.IsNullOrWhiteSpace(row.Field<string>("Directory")) ? "" : string.Format("/{0}", row.Field<string>("Directory"))) + string.Format("/{0}/{1}/", row.Field<string>("ControllerName"), row.Field<string>("ActionName"));
                    var _params = row.Field<string>("params");
                    if (string.IsNullOrEmpty(_params) == false)
                    {
                        if (!_params.StartsWith("?"))
                        {
                            _params = string.Format("?{0}", _params);
                        }
                        menu.Url += _params;
                    }
                    lstMenu.Add(menu);
                }
                var mainList = GetMenuTree(lstMenu, null);

                return mainList;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        private List<Menu> GetMenuTree(List<Menu> list, int? parent)
        {
            return list.Where(x => x.ParrentMenuID == parent).Select(x => new Menu
            {
                MenuID = x.MenuID,
                MenuName = x.MenuName,
                ParrentMenuID = x.ParrentMenuID,
                ActionMethodID = x.ActionMethodID,
                Rank = x.Rank,
                IsActive = x.IsActive,
                Url = x.Url,
                List = GetMenuTree(list, x.MenuID)
            }).ToList();
        }
    }
}