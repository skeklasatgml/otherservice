﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.App_Codes.BLL
{
    public class OtherServicesBll : IDisposable
    {
        private IEncryptDecrypt chiperAlgorithm;
        DbHandler _db = null;
        SqlConnection cn = null;

        public OtherServicesBll()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
            _db = new DbHandler();
            cn = new SqlConnection(_db.ConnectionString);
            cn.Open();
        }
        public List<Module> GetAuthorisedModules(long userId, UserType userType)
        {
            var cmd = _db.NewCommand("get_authorisedModules", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userType", userType);
            DataTable dt = _db.GetResult(cmd);
            return dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.Id = t.Field<int>("Id");
                x.Name = t.Field<string>("Name");
                x.Code = t.Field<string>("Code");
                x.Description = t.Field<string>("Description");
                return x;
            }).ToList();
        }
        public List<Module> GetAuthorisedSubModules(long userId, UserType userType, int? MunicipalityID)
        {
            var cmd = _db.NewCommand("get_authorisedSubModules", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.Parameters.AddWithValue("@userType", userType);
            cmd.Parameters.AddWithValue("@ModuleId", 6);
            DataTable dt = _db.GetResult(cmd);
            var rslt = dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.Id = t.Field<int>("Id");
                x.Name = t.Field<string>("Name");
                x.Code = t.Field<string>("Code");
                x.Description = t.Field<string>("Description");
                return x;
            }).ToList();
            rslt.ForEach(t => {
                if (userType == UserType.MunicipalityUser)
                {
                    t.Counter = GetApplicationCount(t.Id, MunicipalityID, null, "M");
                }
                else if (userType == UserType.OtherCitizenUser)
                {
                    t.Counter = GetApplicationCount(t.Id, null, userId, "U");
                }

            });
            return rslt;
        }
        public ApplicationCount GetApplicationCount(int SubModuleId, long? MunicipalityId, long? userId, string MuniOrUser)
        {
            var cmd = _db.NewCommand("UDServices.Load_Ulbs_Services_Status", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleId);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityId);
            cmd.Parameters.AddWithValue("@UserID", userId);
            cmd.Parameters.AddWithValue("@MuniOrUser", MuniOrUser);
            DataTable dt = _db.GetResult(cmd);
            var rslt = dt.AsEnumerable().Select(t =>
            {
                var x = new ApplicationCount();
                x.APPROVED = t.Field<int>("APPROVED");
                x.SUBMITTED = t.Field<int>("SUBMITTED");
                x.REJECTED = t.Field<int>("REJECTED");
                x.INPROCESS = t.Field<int>("INPROCESS");
                return x;
            }).ToList();
            return rslt[0];
        }
        public List<ApplicationListModule> GetGridData(string SubModuleid, string UserId, string Municipality, string MuniOrUser = "USER")
        {
            var cmd = _db.NewCommand("UDServices.Load_CitizenApplication_Status", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@SubModuleID", Convert.ToInt32(SubModuleid));
            cmd.Parameters.AddWithValue("@UserID", UserId != null && UserId != "" ? Convert.ToInt32(UserId) : "".ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityId", Municipality != null && Municipality != "" ? Convert.ToInt32(Municipality) : "".ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MuniOrUser", MuniOrUser);

            DataTable dt = _db.GetResult(cmd);
            var rslt = dt.AsEnumerable().Select(t =>
            {
               
                var x = new ApplicationListModule();
                x.slno = t.Field<long>("SlNo");

                x.ServMstId = t.Field<long>("ServMstId");

                x.ServMstIdEncrypted = chiperAlgorithm.Encrypt(t.Field<long>("ServMstId").ToString());
                x.ApplicationNo = t.Field<string>("ApplicationNo");
                x.ApplicationDate = t.Field<string>("ApplicationDate");
                x.ProjectDescription = t.Field<string>("ProjectDescription");
                x.ServiceCurrStatus = t.Field<string>("ServiceCurrStatus");
                x.DESCRIPTION = t.Field<string>("DESCRIPTION");
                x.MunicipalityDevAuthoId = t.Field<int?>("MunicipalityDevAuthoId");
                x.MunicipalityDevAuthoIdEncrypt = chiperAlgorithm.Encrypt( t.Field<int?>("MunicipalityDevAuthoId").ToString());
                x.MunicipalityName = t.Field<string>("MunicipalityName");
                return x;
            }).ToList();
            rslt.ForEach(t => {
                t.StatusActions = GetStatusActions(SubModuleid, UserId, Municipality, t.ServMstId, MuniOrUser);
            });
            return rslt;
        }
        public List<URLMode> GetStatusActions(string SubModuleid, string UserId, string Municipality, long ServMstId, string MuniOrUser = "USER")
        {
            var cmd = _db.NewCommand("UDServices.Load_CitizenApplication_Status", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@SubModuleID", Convert.ToInt32(SubModuleid));
            cmd.Parameters.AddWithValue("@UserID", UserId != null && UserId != "" ? Convert.ToInt32(UserId) : "".ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MunicipalityId", Municipality != null && Municipality != "" ? Convert.ToInt32(Municipality) : "".ReplaceDbNull());
            cmd.Parameters.AddWithValue("@MuniOrUser", MuniOrUser);
            cmd.Parameters.AddWithValue("@ServMstId", ServMstId);
            cmd.Parameters.AddWithValue("@Transtype", "URLMODE");
            DataTable dt = _db.GetResult(cmd);
            var rslt = dt.AsEnumerable().Select(t =>
            {
                var x = new URLMode();

                var obj = new { ServMstId = t.Field<long?>("ServMstId"), urlmode = t.Field<string>("urlmode") };
                JavaScriptSerializer js = new JavaScriptSerializer();
                string serializedData = js.Serialize(obj);
                string encryptedUser = chiperAlgorithm.Encrypt(serializedData);
                string urlEncodedData = HttpUtility.UrlEncode(encryptedUser);

                x.Actions = t.Field<string>("ACTIONS");
                x.urlmode = t.Field<string>("urlmode");
                x.ServMstId = t.Field<long?>("ServMstId");
                x.Data = urlEncodedData;
                return x;
            }).ToList();
            return rslt;
        }
        public Module GetModule(int? moduleId, string moduleCode)
        {
            var cmd = _db.NewCommand("get_Module", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@moduleId", moduleId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@moduleCode", moduleCode.ReplaceDbNull());
            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new Module();
                x.Id = t.Field<int>("Id");
                x.Name = t.Field<string>("Name");
                x.Code = t.Field<string>("Code");
                x.Description = t.Field<string>("Description");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public OtpStatusModule SaveRegistration(CitizenReg CitizenReg)
        {

            var cmd = _db.NewCommand("UDServices.Insert_ApplicantRegn", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@UserName", CitizenReg.UserName);
            cmd.Parameters.AddWithValue("@Password", CitizenReg.Password);
            cmd.Parameters.AddWithValue("@ConfirmPassword", CitizenReg.ConfirmPassword);
            cmd.Parameters.AddWithValue("@Name", CitizenReg.Name);
            cmd.Parameters.AddWithValue("@Designation", CitizenReg.Designation);
            cmd.Parameters.AddWithValue("@Email", CitizenReg.Email);
            cmd.Parameters.AddWithValue("@MobileNo", CitizenReg.MobileNo);
            cmd.Parameters.AddWithValue("@CompanyName", CitizenReg.CompanyName);
            cmd.Parameters.AddWithValue("@CompanyAddress", CitizenReg.CompanyAddress);
            cmd.Parameters.AddWithValue("@IsAccountLocked", 0);
            cmd.Parameters.AddWithValue("@IsAccountVerified", 1);
            cmd.Parameters.AddWithValue("@UserType", 4);
            cmd.Parameters.AddWithValue("@RegistrationType", CitizenReg.TypeOfRegistration);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new OtpStatusModule();
                x.Status = t.Field<int>("Status");
                x.isExist = t.Field<int>("isExist");
                x.isVerified = t.Field<int>("isVerified");
                x.Msg = t.Field<string>("Msg");
                x.Mobile = t.Field<string>("Mobile");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }

        public otpstatus GetOtpVerify(string Otpdata, string MobileNo)
        {

            var cmd = _db.NewCommand("UDServices.Get_ApplicantOTPVerification", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@Otp", Otpdata);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new otpstatus();
                x.Status = t.Field<int>("Status");
                x.Msg = t.Field<string>("Msg");
                x.UserID = t.Field<long>("UserID");
                x.Mobile = t.Field<string>("Mobile");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public OtpStatusModule ResendOtp(string Type, string MobileNo)
        {

            var cmd = _db.NewCommand("UDServices.Get_ResendOTP", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new OtpStatusModule();
                x.Status = t.Field<int>("Status");
                x.isVerified = t.Field<int>("isVerified");
                x.Msg = t.Field<string>("Msg");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public OtpStatusModule InspectorRegistration(InspectorRegModule InspectorReg, long UserID, int MunicipalityDevAuthoId)
        {
            var cmd = _db.NewCommand("UDServices.Insert_InspectorUser", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@name", InspectorReg.InspectorName);
            cmd.Parameters.AddWithValue("@InspectorMobile", InspectorReg.InspectorMobile);
            cmd.Parameters.AddWithValue("@InspectorEmail", InspectorReg.InspectorEmail);
            cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", MunicipalityDevAuthoId);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            //  cmd.Parameters.AddWithValue("@IsActive", "Inspector");
            cmd.Parameters.AddWithValue("@Username", InspectorReg.InspectorUserName);
            cmd.Parameters.AddWithValue("@Password ", InspectorReg.InspectorPassword);
            cmd.Parameters.AddWithValue("@IsAccountLocked", 0);
            cmd.Parameters.AddWithValue("@IsAccountVerified", 1);
            cmd.Parameters.AddWithValue("@UserType", 2);
            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new OtpStatusModule();
                x.Status = t.Field<int>("Status");
                x.isExist = t.Field<int>("isExist");
                x.isVerified = t.Field<int>("isVerified");
                x.Msg = t.Field<string>("Msg");
                x.Mobile = t.Field<string>("Mobile");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public List<AllocationDetails> GetAllocationDetails(long ApplicationId)
        {
            var cmd = _db.NewCommand("UDServices.LoadRPT_OthsAllocation", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@ServMstID", ApplicationId);
            DataTable dt = _db.GetResult(cmd);
            var rslt = dt.AsEnumerable().Select(t =>
            {
                var x = new AllocationDetails();
                x.ServMstId = t.Field<long>("ServMstId");
                x.ApplicationNo = t.Field<string>("ApplicationNo");
                x.ApplicationDate = t.Field<string>("ApplicationDate");
                x.NameOfProject = t.Field<string>("NameOfProject");
                x.DesignationName = t.Field<string>("DesignationName");
                x.ResponseStatus = t.Field<string>("ResponseStatus");
                x.Description = t.Field<string>("Description");
                x.allotedOn = t.Field<string>("allotedOn");
                x.updatedOn = t.Field<string>("updatedOn");
                x.Remarks = t.Field<string>("Remarks");
                x.ResponseStatusColor = t.Field<string>("ResponseStatusColor");
                return x;
            }).ToList();
            return rslt;
        }
        public List<ApplicationFormMD.Inspector> GetAssignedInspector(long? ApplicationId)
        {
            var cmd = _db.NewCommand("UDServices.InspectorSchedule_SP", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_GRID");
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);

            //DataTable dt = _db.GetResult(cmd);
            DataSet ds = _db.GetResults(cmd);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = ds.Tables[1];

             var rslt = dt.AsEnumerable().Select(t =>
                {
                    var x = new ApplicationFormMD.Inspector();
                    x.InspectorID = t.Field<int?>("InspectorID");
                    x.InspectorName = t.Field<string>("InspectorName");
                    x.InspectionOnFrom = Convert.ToDateTime(t.Field<string>("InspectionOnFrom"));
                    x.InspectionOnTo = Convert.ToDateTime(t.Field<string>("InspectionOnTo"));
                    x.SCHEDULEID = t.Field<long?>("SCHEDULEID");
                    x.InspOnFrom = t.Field<string>("InspectionOnFrom");
                    x.InspOnTo = t.Field<string>("InspectionOnTo");
                    return x;
                }).ToList();
                return rslt;
            
          
        }
        public string GetAssignedInspectorMinDate(long? ApplicationId)
        {
            var cmd = _db.NewCommand("UDServices.InspectorSchedule_SP", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@TransType", "LOAD_GRID");
            cmd.Parameters.AddWithValue("@ServMstId", ApplicationId);

            //DataTable dt = _db.GetResult(cmd);
            DataSet ds = _db.GetResults(cmd);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = ds.Tables[1];

            return dt1.Rows[0]["MinDate"].ToString() + "#" + dt1.Rows[0]["MaxDate"].ToString();

        }
        public otpstatus GetOtpVerify_Inspector(string Otpdata, string MobileNo)
        {

            var cmd = _db.NewCommand("UDServices.Get_InspectorOTPVerification", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@Otp", Otpdata);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new otpstatus();
                x.Status = t.Field<int>("Status");
                x.Msg = t.Field<string>("Msg");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }

        public OtpStatusModule ResendOtp_Inspector(string Type, string MobileNo)
        {

            var cmd = _db.NewCommand("UDServices.Get_ResendOTPInspector", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new OtpStatusModule();
                x.Status = t.Field<int>("Status");
                x.isVerified = t.Field<int>("isVerified");
                x.Msg = t.Field<string>("Msg");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
        public void Dispose()
        {
            cn.Close();
        }
        public OtpStatusModule Forgetpass_BLL(string Password, string confPassWord, string MobileNo, long userId)
        {
            var cmd = _db.NewCommand("UDServices.Update_ApplicantProfilePassword", cn, CommandType.StoredProcedure);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt64(userId));
            cmd.Parameters.AddWithValue("@NewPassword", Password);
            cmd.Parameters.AddWithValue("@ConfirmNewPassword", confPassWord);

            DataTable dt = _db.GetResult(cmd);
            var rs = dt.AsEnumerable().Select(t =>
            {
                var x = new OtpStatusModule();
                x.Status = t.Field<int>("Status");
                //  x.isVerified = t.Field<int>("isVerified");
                x.Msg = t.Field<string>("Msg");
                return x;
            }).ToList();
            return rs.FirstOrDefault();
        }
    }
}