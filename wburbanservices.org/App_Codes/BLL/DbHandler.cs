﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Valuation_Board.App_Codes.BLL
{
    public class DbHandler
    {
        string _constring = "";
        public string ConnectionString
        {
            get { return _constring; }
            private set { _constring = value; }
        }
        public DbHandler()
        {
            _constring= ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        }
        public DbHandler(string conString)
        {
            _constring = conString;
        }
        public void ResetConnectionString(string newConnectionString)
        {
            _constring = newConnectionString;
        }
        public virtual  SqlCommand NewCommand(string commandText, SqlConnection cn, SqlTransaction tr, CommandType commandType)
        {
            var Command = NewCommand();
            Command.CommandText = commandText;
            Command.Connection = cn;
            Command.Transaction = tr;
            Command.CommandType = commandType;
            return Command;
        }
        public virtual SqlCommand NewCommand(string commandText, SqlConnection cn,  CommandType commandType)
        {
            var Command = NewCommand();
            Command.CommandText = commandText;
            Command.Connection = cn;
            Command.CommandType = commandType;
            return Command;
        }

        public virtual SqlCommand NewCommand(string commandText, SqlConnection cn, SqlTransaction tr)
        {
            var Command = NewCommand();
            Command.CommandText = commandText;
            Command.Connection = cn;
            Command.Transaction = tr;
            return Command;
        }
        public virtual SqlCommand NewCommand(string commandText, SqlConnection cn)
        {
            var Command = NewCommand();
            Command.CommandText = commandText;
            Command.Connection = cn;
            return Command;
        }
        public virtual SqlCommand NewCommand(string commandText)
        {
            var command = NewCommand();
            command.CommandText = commandText;
            return command;
        }
        public SqlCommand NewCommand()
        {
            var command=new SqlCommand();
            command.CommandTimeout = CommandTimeOut();
            return command;
        }
        public SqlConnection NewConnection()
        {
            return new SqlConnection(ConnectionString);
        }
        int CommandTimeOut()
        {
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(ConnectionString);
           return builder.ConnectTimeout;
        }

        public DataSet GetResults(SqlCommand command)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(ds);
            return ds;
        }
        public DataTable GetResult(SqlCommand command)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dt);
            return dt;
        }
        public T GetScalar<T>(SqlCommand cmd)
        {
            var x=cmd.ExecuteScalar();
            return (T)Convert.ChangeType(x, typeof(T)); ;
        }
        public int ExecuteNonQuery(SqlCommand cmd)
        {
            return cmd.ExecuteNonQuery();
        }

    }
}