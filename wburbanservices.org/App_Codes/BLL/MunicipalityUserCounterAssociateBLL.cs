﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.Models.Account;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Valuation_Board.App_Codes.BLL
{
    public class MunicipalityUserCounterAssociateBLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MunicipalityUserCounterAssociateBLL()
        {
            cn = new SqlConnection(constring);
        }
        public string Insert(MunicipalityUserCounterAssociate obj,int UserID)
        {
            SqlCommand cmd = new SqlCommand("Insert_MunicipalityCounterUserAssociate", cn);
            cmd.Parameters.AddWithValue("@fk_CounterID", obj.CounterID);
            cmd.Parameters.AddWithValue("@fk_UserID", obj.UserID);
            cmd.Parameters.AddWithValue("@DateFrom", obj.DateFrom);
            cmd.Parameters.AddWithValue("@DateTo", obj.DateTo);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter ads = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                ads.Fill(dt);
                return dt.Rows[0]["msg"].ToString();
             //   cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public string Update(MunicipalityUserCounterAssociate obj, int UserID)
        {
            SqlCommand cmd = new SqlCommand("Update_MunicipalityCounterUserAssociate", cn);
            cmd.Parameters.AddWithValue("@RowID", obj.RowID);
            cmd.Parameters.AddWithValue("@fk_CounterID", obj.CounterID);
            cmd.Parameters.AddWithValue("@fk_UserID", obj.UserID);
            cmd.Parameters.AddWithValue("@DateFrom", obj.DateFrom);
            cmd.Parameters.AddWithValue("@DateTo", obj.DateTo);
            cmd.Parameters.AddWithValue("@UpdatedBy", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter ads = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                ads.Fill(dt);
                return dt.Rows[0]["msg"].ToString();
             //   cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public void Delete(int RowID)
        {
            SqlCommand cmd = new SqlCommand("Delete_MunicipalityCounterUserAssociate", cn);
            cmd.Parameters.AddWithValue("@RowID", RowID);  
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<MunicipalityUser> GetUserByMunicipalityID(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_UserByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converter2(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        List<MunicipalityUser> Converter2(DataTable dt)
        {
            List<MunicipalityUser> lst = new List<MunicipalityUser>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new MunicipalityUser();                
                obj.UserName = t.Field<string>("UserName");
                obj.UserID = Convert.ToInt32(t.Field<long>("UserID"));               
                return obj;
            }).ToList();
            return lst;
        }
        
        public List<MunicipalityUserCounterAssociate> Get_UserCounterAssos(int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_CounterUserAssociateByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converter(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }

        }
        List<MunicipalityUserCounterAssociate> Converter(DataTable dt)
        {
            List<MunicipalityUserCounterAssociate> lst = new List<MunicipalityUserCounterAssociate>();
            lst = dt.AsEnumerable().Select(t =>
            {              
            var obj = new MunicipalityUserCounterAssociate();
                obj.RowID =t.Field<long>("RowID");
                obj.UserName = t.Field<string>("UserName");
                obj.UserID = t.Field<long>("UserID");
                obj.CounterID = t.Field<int>("CounterID");
                obj.CounterCode = t.Field<string>("CounterCode");
                obj.MunicipalityID = t.Field<int>("MunicipalityID");
                obj.DateFrom = t.Field<string>("DateFrom");
                obj.DateTo = t.Field<string>("DateTo");
                return obj;
            }).ToList();
            return lst;
        }
    }
}
