﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL
{
    public class ValuationBoardUserBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ValuationBoardUserBll()
        {
            cn = new SqlConnection(conString);
        }
        public void GetUser(int id)
        {
            throw new NotImplementedException();
        }
        public ValuationBoardUser GetUser(string userName, string password)
        {
            cn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "Get_ValuationBrdUserByUserNmPass";
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs = Convert(dt);
                if (objs.Count > 0)
                {
                    return objs[0];
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        #region
        public List<ValuationBoardUser> Convert(DataTable dt)
        {
            List<ValuationBoardUser> lst = new List<ValuationBoardUser>();
            foreach (DataRow dr in dt.Rows)
            {
                ValuationBoardUser vu = new ValuationBoardUser()
                {
                    UserID = dr.Field<long>("UserID"),
                    Email = dr.Field<string>("Email"),
                    FirstName = dr.Field<string>("FirstName"),
                    LastName = dr.Field<string>("LastName"),
                    PhoneNo = dr.Field<string>("PhoneNo"),
                    IsAccountVerified = dr.Field<byte>("IsAccountVerified") == 1 ? true : false,
                    IsAccountLocked = dr.Field<byte>("IsAccountLocked") == 1 ? true : false,
                    UserName = dt.Rows[0].Field<string>("UserName"),
                };
                lst.Add(vu);
            }
            return lst;

        }
        #endregion
    }
}