﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.ApiService;

namespace Valuation_Board.App_Codes.BLL.ApiService
{
    public class TokenService
    {
        private readonly string _token = null;
        private readonly long _profileID ;
        DbHandler _db;
        SqlConnection cn;
        public TokenService(string token, long profileID)
        {
            this._profileID = profileID;
            this._token = token;
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public bool Validate()
        {
            try
            {
                bool tokenStatus = GetTokenStatus();
                return tokenStatus;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close(); 
            }
        }

        public bool GetTokenStatus()
        {
            try
            {
                bool response = false;
                if (string.IsNullOrEmpty(_token) || string.IsNullOrEmpty(_profileID.ToString()))
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_ValidateToken", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProfileID", this._profileID);
                cmd.Parameters.AddWithValue("@Token", this._token);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true && Convert.ToBoolean(dt.Rows[0]["ExpiredStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["ExpiredStatus"]) == true)
                {
                    throw new AppException(ErrorConstants.Errors["TE0002"], "TE0002");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["ExpiredStatus"]) != true)
                {
                    response = true;
                }
                return response;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public bool GetAssesseeProfileMappingStatus(long ProfileID, long? AssesseeID)
        {
            try
            {
                bool response = false;
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_AssesseeProfileMappingStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProfileID", ProfileID);
                cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["mapStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["ANM0012"], "ANM0012");
                }
                if (Convert.ToBoolean(dt.Rows[0]["mapStatus"]) == true)
                {
                    response = true;
                }
                return response;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public void Validate(long AssesseeId)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("has_assesseInProfile", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", this._token);
                cmd.Parameters.AddWithValue("@AssesseeId", AssesseeId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if(dt.Rows.Count<=0 || dt.Rows[0].Field<bool>("IsExists") == false){
                    throw new SecurityException("Assessee does not exist in current profile");
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public OnilneCitizenProfile GetProfile()
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_CitizenProfileByToken", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", this._token);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                var profs= dt.AsEnumerable().Select(t=> {
                    var prof = new OnilneCitizenProfile();
                    prof.AuthToken = t.Field<string>("AuthToken");
                    prof.ProfileId = t.Field<long>("ProfileId");
                    prof.Mobile = t.Field<string>("Mobile");
                    prof.Email = t.Field<string>("Email");
                    prof.TokenExpiredOn = t.Field<DateTime?>("TokenExpiredOn");
                    prof.OTP = t.Field<string>("Otp");
                    prof.IsPhoneNoVerified = t.Field<bool>("IsAccountVerified");
                    return prof;
                }).ToList();
                if (profs.Count <= 0)
                {
                    throw new SecurityException("Profile not found");
                }
                return profs[0];
            }
            catch {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
    public class CitizenProfileService
    {
        DbHandler _db;
        SqlConnection cn;
        int _profileID;
        public CitizenProfileService(int profileId)
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
            _profileID = profileId;
        }
        public List<Assessee> GetAssessees()
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_AssessesByProfile", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProfileId", _profileID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<long> assesseIds = dt.AsEnumerable().Select(t => t.Field<long>("AssesseeId")).ToList();
                List<Assessee> assesses = new AssesseeBll().GetAssessees(assesseIds);
                return assesses;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public OnilneCitizenProfile GetProfile()
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Get_CitizenProfileById", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@profileId", this._profileID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                var profs = dt.AsEnumerable().Select(t => {
                    var prof = new OnilneCitizenProfile();
                    prof.AuthToken = t.Field<string>("AuthToken");
                    prof.ProfileId = t.Field<long>("ProfileId");
                    prof.Mobile = t.Field<string>("Mobile");
                    prof.Email = t.Field<string>("Email");
                    prof.TokenExpiredOn = t.Field<DateTime?>("TokenExpiredOn");
                    prof.OTP = t.Field<string>("Otp");
                    prof.IsPhoneNoVerified = t.Field<bool>("IsAccountVerified");
                    return prof;
                }).ToList();
                if (profs.Count <= 0)
                {
                    throw new SecurityException("Profile not found");
                }
                return profs[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
       

    }
    public class ApiSecurityService
    {
        DbHandler _db;
        SqlConnection cn;
        public ApiSecurityService()
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public OnilneCitizenProfile Authenticate(string phoneNo, string password)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Authenticate_OnlineCitizenModule", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@phoneNo", phoneNo);
                cmd.Parameters.AddWithValue("@password", password);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                var profs = dt.AsEnumerable().Select(t => {
                    var prof = new OnilneCitizenProfile();
                    prof.AuthToken = t.Field<string>("AuthToken");
                    prof.ProfileId = t.Field<long>("ProfileId");
                    prof.Mobile = t.Field<string>("Mobile");
                    prof.Email = t.Field<string>("Email");
                    prof.TokenExpiredOn = t.Field<DateTime?>("TokenExpiredOn");
                    prof.OTP = t.Field<string>("Otp");
                    prof.IsPhoneNoVerified = t.Field<bool>("IsAccountVerified");
                    return prof;
                }).ToList();
                if (profs.Count <= 0)
                {
                    throw new SecurityException("Profile not found");
                }
                if (profs[0].IsPhoneNoVerified == false)
                {
                    throw new SecurityException("Mobile no verification pending");
                }
                return profs[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public OnilneCitizenProfile Register(string Mobile,string Email, string password)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = _db.NewCommand("Insert_OnlineCitizenProfileRegister", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@phoneNo", Mobile);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@password", password);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                var profs = dt.AsEnumerable().Select(t => {
                    var prof = new OnilneCitizenProfile();
                    prof.AuthToken = t.Field<string>("AuthToken");
                    prof.ProfileId = t.Field<long>("ProfileId");
                    prof.Mobile = t.Field<string>("Mobile");
                    prof.Email = t.Field<string>("Email");
                    prof.TokenExpiredOn = t.Field<DateTime?>("TokenExpiredOn");
                    prof.OTP = t.Field<string>("Otp");
                    prof.IsPhoneNoVerified = t.Field<bool>("IsAccountVerified");
                    return prof;
                }).ToList();
                if (profs.Count <= 0)
                {
                    throw new SecurityException("Profile not found");
                }
                if (profs[0].IsPhoneNoVerified == false)
                {
                    throw new SecurityException("Mobile no verification pending");
                }
                return profs[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}