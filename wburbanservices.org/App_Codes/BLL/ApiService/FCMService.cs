﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Valuation_Board.ViewModels.ApiService;

namespace Valuation_Board.App_Codes.BLL.ApiService
{
    public class FCMService
    {
        public string PushNotification(DeviceNotification deviceNotification)
        {
            RegisterNewBLL rn = new RegisterNewBLL();
            string FireBaseID = rn.GetDeviceIDByProfileID(deviceNotification.ProfileID, deviceNotification.MobileNo);
            string msg = "Your Payment for Property Tax of Rs. " + deviceNotification.PaidAmount + " against " + deviceNotification.NotificationText + " has been received.";

            var result = "-1";
            if (FireBaseID != "")
            {
                string serverKey = "AAAAlOkBCwI:APA91bFxjnupeZ7wwk0E-trUb2ixfdauVOfkeUifjV7Ji94NuKNeRW7_H7jsSmkE97dHdCFG_hJpnqagKkxK49EN9qcQgcY6GyH0JX6hjS1IkBH0TQ645f1vWL-xfugsWQWET5HduiObhBXQYqJ5UyzKC4Z7GFRh4Q";
                try
                {
                    //var result = "-1";
                    var webAddr = "https://fcm.googleapis.com/fcm/send";
                    var regID = FireBaseID;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                    httpWebRequest.ContentType = "application/json";
                    //httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                    httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=" + serverKey);
                    httpWebRequest.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = "{\"to\": \"" + regID + "\",\"notification\": {\"title\": \"UDMA Notification\",\"body\": \"" + msg + "\",\"sound\": \"default\"}}";
                        //registration_ids, array of strings -  to, single recipient
                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                    }
                    //return result;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return "err";
                }
            }
            return result;
        }
    }
}