﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.ApiService;
using Valuation_Board.App_Codes.Utils;
using System.Web.Http.Results;
using System.Web.Mvc;
using Valuation_Board.Models;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.App_Codes.BLL.ApiService
{
    public class RegisterNewBLL
    {
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public RegisterNewBLL()
        {
            cn = new SqlConnection(conString);
        }
        public bool RegisterNewBll(ProfileRegister Register)
        {
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Insert_RegisterNewForApi", cn);
            cmd.Parameters.AddWithValue("@PhoneNo", Register.Phone.Trim());
            cmd.Parameters.AddWithValue("@Email", Register.Email == null ? DBNull.Value:(object)Register.Email.Trim());
            cmd.Parameters.AddWithValue("@Password", Register.Password == null ? DBNull.Value : (object)Register.Password.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                
                if(Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) == true)
                {
                    throw new AppException(ErrorConstants.Errors["PE0004"], "PE0004");
                }
                else if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PE0006"], "PE0006");
                }
                return true;
                //else if (Register.Type.Trim() == "R")
                //{
                //    if (profs.Count > 0 && profs[0].IsPhoneNoVerified == true)
                //    {
                //        throw new AppException(ErrorConstants.Errors["IO0005"], "IO0005");
                //    }
                //}

            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool ProfileVerificationBll(OnilneCitizenProfile CitizenProfile)
        {
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Get_ProfileVerification", cn);
            cmd.Parameters.AddWithValue("@PhoneNo", CitizenProfile.Mobile.Trim());
            cmd.Parameters.AddWithValue("@Otp", CitizenProfile.OTP);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["IO0005"], "IO0005");
                }

                return true;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool RegisterPasswordBll(OnilneCitizenProfile CitizenProfile)
        {
            SqlCommand cmd = new SqlCommand("Update_ProfilePassword", cn);
            cmd.Parameters.AddWithValue("@PhoneNo", CitizenProfile.Mobile.Trim());
            cmd.Parameters.AddWithValue("@Otp", CitizenProfile.OTP);
            cmd.Parameters.AddWithValue("@Email", string.IsNullOrEmpty(CitizenProfile.Email) ? DBNull.Value : (object)CitizenProfile.Email.Trim());
            cmd.Parameters.AddWithValue("@Password", CitizenProfile.Password == null ? DBNull.Value : (object)CitizenProfile.Password.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["OTPStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["IO0005"], "IO0005");
                }

                return true;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public OnilneCitizenProfile ProfileLoginBll(ProfileRegister Register)
        {
            var obj = new object();
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            var profs=new List<OnilneCitizenProfile>();
            //string responseObj = "";
            SqlCommand cmd = new SqlCommand("Get_ProfileLoginForApi", cn);
            cmd.Parameters.AddWithValue("@PhoneNo", Register.Phone.Trim());
            cmd.Parameters.AddWithValue("@Password", Register.Password == null ? DBNull.Value : (object)Register.Password.Trim());
            cmd.Parameters.AddWithValue("@FireBaseID", Register.FireBaseID == null ? DBNull.Value : (object)Register.FireBaseID.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true && Convert.ToBoolean(dt.Rows[0]["PW"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["PW"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PIC0007"], "PIC0007");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["PW"]) == true)
                {
                        profs = dt.AsEnumerable().Select(t => {
                        var prof = new OnilneCitizenProfile();
                        prof.ProfileId = t.Field<long>("ProfileId");
                        prof.Mobile = t.Field<string>("Mobile");
                        prof.Email = t.Field<string>("Email");
                        prof.Name = t.Field<string>("Name");
                        prof.AuthToken = t.Field<string>("AuthToken");
                        prof.TokenExpiredOn = t.Field<DateTime?>("TokenExpiredOn");
                        prof.TimeZone = t.Field<string>("TimeZone");
                        prof.IsPhoneNoVerified = t.Field<bool>("IsAccountVerified");
                        return prof;
                    }).ToList();                    
                }
                return profs[0];
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool ForgotPasswordBll(ProfileRegister Register)
        {
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Get_ResendOTP", cn);
            cmd.Parameters.AddWithValue("@Type", Register.Type.Trim());
            cmd.Parameters.AddWithValue("@PhoneNo", Register.Phone.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if(Register.Type == "F")
                {
                    if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) != true)
                    {
                        throw new AppException(ErrorConstants.Errors["PE0006"], "PE0006");
                    }
                    if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) != true)
                    {
                        throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                    }
                }
                if (Register.Type == "R")
                {
                    if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) == true)
                    {
                        throw new AppException(ErrorConstants.Errors["PE0004"], "PE0004");
                    }
                    if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true && Convert.ToBoolean(dt.Rows[0]["isVerified"]) != true)
                    {
                        throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                    }
                }

                return true;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<SearchAssessse> GetAssesseeListProfileWiseBll(string Token, long ProfileID)
        {
            string currDate = DateTime.Now.ToString("yyyy-MM-dd");
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Get_AssesseeListByProfile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var profs = dt.AsEnumerable().Select(t => {
                    var prof = new SearchAssessse();
                    prof.ProfileId = t.Field<long>("ProfileId");
                    prof.AssesseeID = t.Field<long>("AssesseeID");
                    prof.DistrictID = t.Field<int?>("DistrictID");
                    prof.MunicipalityID = t.Field<int?>("MunicipalityID");
                    prof.WardID = t.Field<int?>("WardID");
                    prof.LocationID = t.Field<int?>("LocationID");
                    prof.AssesseeName = t.Field<string>("AssesseeName");
                    prof.HoldingNo = t.Field<string>("HoldingNo");
                    prof.NetAmount = GetNetAmt(t.Field<int?>("MunicipalityID") ?? 0, t.Field<int?>("WardID") ?? 0,
                                               t.Field<int?>("LocationID") ?? 0, t.Field<long>("AssesseeID"), currDate);
                    prof.MunicipalityName = t.Field<string>("MunicipalityName");
                    prof.WardName = t.Field<string>("WardName");
                    prof.LocationName = t.Field<string>("LocationName");
                    return prof;
                }).ToList();

                return profs;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public decimal GetNetAmt(int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate)
        {
            decimal netAmt = 0;
            PTaxCollectionGiantObject pTaxCollectionGiantObject = new PTaxCollectionGiantObject();
            pTaxCollectionGiantObject = Get_PropertyTaxPaymentDetails(MunicipalityID, WardID, LocationID, AssesseeID, PaymentDate, null);

            if(pTaxCollectionGiantObject != null)
            {
                if (pTaxCollectionGiantObject.propertyTaxPaymentMaster.NetAmount != null)
                {
                    netAmt = pTaxCollectionGiantObject.propertyTaxPaymentMaster.NetAmount ?? 0;
                }
            }            
            return netAmt;
        }

        public bool AddAssesseeProfileWiseBll(string Token, SearchAssessse searchAssessse)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Insert_AssesseeByProfile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", searchAssessse.ProfileId);
            cmd.Parameters.AddWithValue("@MunicipalityID", searchAssessse.MunicipalityID);
            cmd.Parameters.AddWithValue("@WardID", searchAssessse.WardID);
            cmd.Parameters.AddWithValue("@LocationID", searchAssessse.LocationID);
            cmd.Parameters.AddWithValue("@HoldingNo", searchAssessse.HoldingNo.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) != true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) != true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) != true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["ANF0009"], "ANF0009");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) == true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) == true)
                {
                    throw new AppException(ErrorConstants.Errors["ANF0010"], "ANF0010");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) == true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) != true)
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool DeleteAssesseeProfileWiseBll(string Token, SearchAssessse searchAssessse)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Delete_AssesseeByProfile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", searchAssessse.ProfileId);
            cmd.Parameters.AddWithValue("@AssesseeID", searchAssessse.AssesseeID);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) != true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) != true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) != true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["ANF0009"], "ANF0009");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["assesseeStatus"]) == true
                    && Convert.ToBoolean(dt.Rows[0]["tagStatus"]) == true)
                {
                    response = true;
                }

                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool ChangeProfilePasswordBll(string Token, OnilneCitizenProfile onilneCitizenProfile)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Change_ProfilePassword", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", onilneCitizenProfile.ProfileId);
            cmd.Parameters.AddWithValue("@OldPassword", onilneCitizenProfile.OldPassword.Trim());
            cmd.Parameters.AddWithValue("@NewPassword", onilneCitizenProfile.Password.Trim());

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) != true && Convert.ToBoolean(dt.Rows[0]["oldPassStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["AR0001"], "AR0001");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["oldPassStatus"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["OPI0011"], "OPI0011");
                }
                if (Convert.ToBoolean(dt.Rows[0]["tokenStatus"]) == true && Convert.ToBoolean(dt.Rows[0]["oldPassStatus"]) == true)
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public PTaxCollectionGiantObject Get_PropertyTaxPaymentDetails(int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters)
        {
            PTaxCollectionGiantObject pTaxCollectionGiantObject = new PTaxCollectionGiantObject();
            try
            {
               // var request = Request.Params;
                
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                PaymentProcessor payment = new PaymentProcessor();
                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                pTaxCollectionGiantObject = payment.SearchOutStanding(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, null, null, AssesseeID);

                //cr.Data = Object;
                //cr.Status = ResponseStatus.SUCCESS;
                //cr.Message = "Payment Details found";
            }
            catch (Exception ex)
            {
                throw;
                //cr.Data = null;
                //cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                //cr.Message = ex.Message;
            }
            //js.Data = cr;
            return pTaxCollectionGiantObject;
        }

        public string MakePaymentTempBll(string Mobile, string Email, string PG, int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters)
        {
            string redirectUrl = "";
            JsonResult js = new JsonResult();
            ClientJsonResult jc = new ClientJsonResult();
            IEncryptDecrypt chiperAlgorithm;
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
            //temporary payment, with flag trunsaction not successfull
            //PaymentDate statnds for collection date
            try
            {
                AssesseeBll bll = new AssesseeBll();
                try
                {
                    bll.UpdateAssesseeEmailID(AssesseeID, Email.Trim());
                    bll.UpdateAssesseeMobileNo(AssesseeID, Mobile.Trim());
                }
                catch { }

                Assessee assesseeObj = bll.GetAssesseebyId(AssesseeID);
                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                PaymentProcessor payment = new PaymentProcessor();
                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                Object = payment.Calculate(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, null, null, AssesseeID);
                //Object.propertyTaxPaymentMaster.CollectorID = (int)mObj.UserID;
                Object.propertyTaxPaymentMaster.InsertedBy = null;
                Object.propertyTaxPaymentMaster.InsertedOn = DateTime.Now;
                Object.propertyTaxPaymentMaster.PaymentType = "O";
                Object.propertyTaxPaymentMaster.CollectorID = null;
                Object.propertyTaxPaymentMaster.TransactionID = PG_Model.GenTransactionID(assesseeObj.AssesseeNo);


                //reassign paid amount
                Object.propertyTaxPaymentMaster.PaidAmount = Object.propertyTaxPaymentMaster.NetAmount;


                long? paymentID = payment.MakePaymentOnline_Temp(Object.propertyTaxPaymentMaster, TaxPaymentCheckParameters, PG);
                if (paymentID == null)
                {
                    throw new Exception("Unknown Error! Transaction canceled");
                }
                else
                {
                    PropertyTaxCollectionBLL pbll = new PropertyTaxCollectionBLL();
                    PropertyTaxPaymentMaster obj = pbll.GetPaymentMasterByPaymentID(paymentID ?? 0);
                    if (obj == null)
                    {
                        throw new Exception("PropertyTaxPaymentMaster not found by payment id");
                    }
                    JavaScriptSerializer jss = new JavaScriptSerializer();

                    //Generate response url like [http://localhost/Online/onlinepayment/RedirectToPG?Data={TransactionID:xxxxxxxx,PG:xx}]
                     redirectUrl = string.Format("/Online/onlinepayment/RedirectToPG?Data={0}", chiperAlgorithm.Encrypt(jss.Serialize(new { TransactionID = obj.TransactionID, PG = PG })));
                    //jc.Data = new { RedirectTo = redirectUrl };
                }
                //jc.Status = ResponseStatus.SUCCESS;
                //jc.Message = "paid successfully";
            }
            catch 
            {
                throw;
                //jc.Data = null;
                //jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                //jc.Message = ex.Message;
            }
            //js.Data = jc;
            return redirectUrl;
        }

        public bool UpdateMobileOnProfileBll(string Token, TaxOutstanding taxOutstanding)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Update_MobileOnProfile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", taxOutstanding.ProfileId);
            cmd.Parameters.AddWithValue("@Mobile", taxOutstanding.Mobile.Trim());

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    throw new AppException(ErrorConstants.Errors["PAE0013"], "PAE0013");
                }
                
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool UpdateOtherInfoOnProfileBll(string Token, TaxOutstanding taxOutstanding)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Update_OtherInfoOnProfile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", taxOutstanding.ProfileId);
            cmd.Parameters.AddWithValue("@Email", taxOutstanding.Email == null ? DBNull.Value : (object)taxOutstanding.Email.Trim());
            cmd.Parameters.AddWithValue("@Name", taxOutstanding.Name == null ? DBNull.Value : (object)taxOutstanding.Name.Trim());

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    response = true;
                }

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PE0006"], "PE0006");                    
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool VerificationOnUpdatedMobileNoBll(string Token, OnilneCitizenProfile onilneCitizenProfile)
        {
            var response = false;
            var obj = new object();
            SqlCommand cmd = new SqlCommand("Get_VerificationOnUpdatedMobile", cn);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@ProfileID", onilneCitizenProfile.ProfileId);
            cmd.Parameters.AddWithValue("@Mobile", onilneCitizenProfile.Mobile.Trim());
            cmd.Parameters.AddWithValue("@OTP", onilneCitizenProfile.OTP);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["IO0005"], "IO0005");
                }

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public string GetDeviceIDByProfileID(long ProfileID, string MobileNo)
        {
            string FireBaseID = "";
            SqlCommand cmd = new SqlCommand("Get_DeviceIDByProfileID", cn);
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID == 0 ? DBNull.Value : (object)ProfileID);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo == null ? DBNull.Value : (object)MobileNo);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                FireBaseID = dt.Rows[0]["FireBaseID"].ToString();               

                return FireBaseID;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }



        ////------------------------------------- Narayangang Municipality BLL -------------------------------------------------------

        public NG_OnlineCitizenProfile NG_ProfileLoginBll(NG_Profile Register)
        {
            var obj = new object();
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            var profs = new List<NG_OnlineCitizenProfile>();
            //string responseObj = "";
            SqlCommand cmd = new SqlCommand("Get_ProfileLoginForApi_NG", cn);
            cmd.Parameters.AddWithValue("@UserName", Register.UserName.Trim());
            cmd.Parameters.AddWithValue("@Password", Register.Password == null ? DBNull.Value : (object)Register.Password.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true && Convert.ToBoolean(dt.Rows[0]["PW"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["PW"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PIC0007"], "PIC0007");
                }
                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true && Convert.ToBoolean(dt.Rows[0]["PW"]) == true)
                {
                    profs = dt.AsEnumerable().Select(t => {
                        var prof = new NG_OnlineCitizenProfile();
                        prof.ProfileId = t.Field<long>("ProfileId");
                        prof.Mobile = t.Field<string>("Mobile");
                        prof.Email = t.Field<string>("Email");
                        prof.AuthToken = t.Field<string>("AuthToken");
                        prof.UserName = t.Field<string>("UserName");
                        prof.FullName = t.Field<string>("Name");
                        return prof;
                    }).ToList();
                }
                return profs[0];
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool NG_RegisterNewBll(NG_Profile Register)
        {
            var response = false;
            SqlCommand cmd = new SqlCommand("Insert_RegisterNewForApi_NG", cn);
            cmd.Parameters.AddWithValue("@UserName", Register.UserName.Trim());
            cmd.Parameters.AddWithValue("@FullName", Register.FullName.Trim());
            cmd.Parameters.AddWithValue("@PhoneNo", Register.Mobile == null ? DBNull.Value : (object)Register.Mobile.Trim());
            cmd.Parameters.AddWithValue("@Email", Register.Email == null ? DBNull.Value : (object)Register.Email.Trim());
            cmd.Parameters.AddWithValue("@Password", Register.Password == null ? DBNull.Value : (object)Register.Password.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    throw new AppException(ErrorConstants.Errors["PAE0013"], "PAE0013");
                }
                else if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true )
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool NG_RegisterComplainBll(ComplainNarayangang Register)
        {
            var response = false;
            SqlCommand cmd = new SqlCommand("Insert_RegisterComplain_NG", cn);
            cmd.Parameters.AddWithValue("@ComType", Register.ComType);
            cmd.Parameters.AddWithValue("@ProfileID", Register.ProfileID);
            cmd.Parameters.AddWithValue("@Com_Header", Register.Com_Header == null ? DBNull.Value : (object)Register.Com_Header.Trim());
            cmd.Parameters.AddWithValue("@Com_Body", Register.Com_Body == null ? DBNull.Value : (object)Register.Com_Body.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                }
                else if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    response = true;
                }
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<ComplainNarayangang> NG_GetComplainBll(ComplainNarayangang Register)
        {
            var profs = new List<ComplainNarayangang>();
            SqlCommand cmd = new SqlCommand("Get_RegisterComplain_NG", cn);
            cmd.Parameters.AddWithValue("@ProfileID", Register.ProfileID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (Convert.ToBoolean(dt.Rows[0]["Status"]) != true)
                {
                    throw new AppException(ErrorConstants.Errors["PC0008"], "PC0008");
                }
                else if (Convert.ToBoolean(dt.Rows[0]["Status"]) == true)
                {
                    profs = dt.AsEnumerable().Select(t => {
                        var prof = new ComplainNarayangang();
                        prof.ProfileID = t.Field<long>("ProfileID");
                        prof.ComID = t.Field<long>("ComID");
                        prof.ComType = t.Field<int>("ComType");
                        prof.Com_Header = t.Field<string>("Com_Header");
                        prof.Com_Body = t.Field<string>("Com_Body");
                        prof.Com_Status = t.Field<int>("Com_Status");
                        prof.InsertedOn = t.Field<string>("InsertedOn");
                        prof.UpdatedOn = t.Field<string>("UpdatedOn");
                        return prof;
                    }).ToList();
                }
                return profs;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public bool NG_RegisterUserLocationBLL(NG_UserLocation nG_UserLocation)
        {
            var response = false;
            SqlCommand cmd = new SqlCommand("Insert_RegisterUserLocation_NG", cn);
            cmd.Parameters.AddWithValue("@ProfileID", nG_UserLocation.ProfileID);
            cmd.Parameters.AddWithValue("@LocationCoordinates", nG_UserLocation.LocationCoordinates.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                response = true;
                return response;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<NG_UserLocation> NG_GetUserLocationBll(NG_UserLocation nG_UserLocation)
        {
            var profs = new List<NG_UserLocation>();
            SqlCommand cmd = new SqlCommand("Get_UserLocation_NG", cn);
            cmd.Parameters.AddWithValue("@ProfileID", nG_UserLocation.ProfileID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    profs = dt.AsEnumerable().Select(t => {
                        var prof = new NG_UserLocation();
                        prof.ProfileID = t.Field<long>("ProfileID");
                        prof.LocationCoordinates = t.Field<string>("LocationCoordinates");
                        prof.InsertedOn = t.Field<DateTime>("InsertedOn");
                        return prof;
                    }).ToList();
                }
                return profs;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<NG_TrackingPermission> NG_TrackingPermissionBll(NG_TrackingPermission  nG_TrackingPermission)
        {
            var profs = new List<NG_TrackingPermission>();
            SqlCommand cmd = new SqlCommand("Get_TrackingPermission_NG", cn);
            cmd.Parameters.AddWithValue("@TrackerProfileID", nG_TrackingPermission.TrackerProfileID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    profs = dt.AsEnumerable().Select(t => {
                        var prof = new NG_TrackingPermission();
                        prof.TrackedProfileID = t.Field<long>("TrackedProfileID");
                        prof.TrackerProfileID = t.Field<long>("TrackerProfileID");
                        prof.TrackingFlag = t.Field<string>("TrackingFlag");
                        prof.TrackedProfileName = t.Field<string>("TrackedProfileName");
                        prof.InsertedOn = t.Field<DateTime>("InsertedOn");
                        return prof;
                    }).ToList();
                }
                return profs;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

    }
}