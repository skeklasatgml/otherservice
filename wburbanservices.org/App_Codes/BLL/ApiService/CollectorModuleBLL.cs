﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.ApiService;
using Valuation_Board.App_Codes.Utils;
using System.Web.Http.Results;
using System.Web.Mvc;
using Valuation_Board.Models;
using Valuation_Board.Utils.EncryptionDecrytion;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.App_Codes.Authenticator;
using Valuation_Board.Models.Account;

namespace Valuation_Board.App_Codes.BLL.ApiService
{
    public class CollectorModuleBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public CollectorModuleBLL()
        {
            cn = new SqlConnection(conString);
        }

        public MunicipalityUser CollectorLoginBll(MunicipalityLogin MunicipalityLogin)
        {
            try
            {
                MunicipalityAuthenticator mAuth = new MunicipalityAuthenticator(MunicipalityLogin);
                MunicipalityUser mUser = mAuth.Authenticate();                
                return mUser;
                //App_Codes.SessionContext.Login(mUser);
                //cr.Data = new LoginResponse()
                //{
                //    IsLoggedIn = true,
                //    RedirectUrl = new MunicipalityUserBll().GetUrl_LandingPage(mUser)
                //};
                //cr.Message = "Login Successfull";
                //cr.Status = ResponseStatus.SUCCESS;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
                //cr.Data = new LoginResponse() { IsLoggedIn = false };
                //cr.Message = ex.Message;
                //cr.Status = ResponseStatus.UNAUTHORIZED;
            }
            //js.Data = cr;
            //return js;
        }

        public List<Assessee> GetAssesseesByHoldingsBll(SearchAssessse searchAssessse)
        {
            try
            {
                AssesseeBll _assesseeBLL = new AssesseeBll();                
                List<Assessee> lstAssessee = _assesseeBLL.GetAssessees(searchAssessse.MunicipalityID ?? 0 , searchAssessse.HoldingNo, searchAssessse.ProfileId);
                lstAssessee = _assesseeBLL.IncludeWard(lstAssessee, searchAssessse.MunicipalityID ?? 0);
                lstAssessee.ForEach(t => {
                    var assessee = _assesseeBLL.IncludeLocation(t);
                    t.Location = assessee.Location == null ? null : assessee.Location;
                });                

                return lstAssessee;
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
                
            }
        }

        public long MakePaymentBll(string Mobile, string Email, long user_ID, int Municipality_ID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters, List<PaymentMode> PaymentModes, DateTime? NoticeServedOn, int? EffectQtrFrom, string BillReciptNo)
        {
            //PaymentDate statnds for collection date
            long MstPaymentID;
            try
            {                
                AssesseeBll bll = new AssesseeBll();
                try
                {
                    bll.UpdateAssesseeEmailID(AssesseeID, Email.Trim());
                    bll.UpdateAssesseeMobileNo(AssesseeID, Mobile.Trim());
                }
                catch { }

                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                //MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;

                PaymentProcessor payment = new PaymentProcessor();

                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, Municipality_ID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(Municipality_ID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });
                Object = payment.Calculate(Municipality_ID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, NoticeServedOn, EffectQtrFrom, user_ID);

                Object.propertyTaxPaymentMaster.InsertedBy = (int)user_ID;
                Object.propertyTaxPaymentMaster.InsertedOn = DateTime.Now;
                Object.propertyTaxPaymentMaster.PaymentType = "F";//F: Offline
                Object.propertyTaxPaymentMaster.CollectorID = (user_ID == 0 ? null : (int?)user_ID);
                Object.propertyTaxPaymentMaster.TransactionID = null;

                //reassign paid amount
                Object.propertyTaxPaymentMaster.PaidAmount = Object.propertyTaxPaymentMaster.NetAmount;
                Object.propertyTaxPaymentMaster.BillReciptNo = BillReciptNo;

                long? paymentID = payment.MakePayment(Object.propertyTaxPaymentMaster, TaxPaymentCheckParameters, PaymentModes);
                if (paymentID == null)
                {
                    throw new Exception("Unknown Error! Transaction canceled");
                }
                else
                {
                    MstPaymentID = paymentID ?? 0;
                    //jc.Data = new { MstPaymentID = paymentID, RedirectTo = string.Format("/Municipality/MnReports/PaymentReciept?PaymentId={0}", paymentID) };
                }
                //jc.Status = ResponseStatus.SUCCESS;
                //jc.Message = "paid successfully";
            }
            catch 
            {
                throw;
                //jc.Data = null;
                //jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                //jc.Message = ex.Message;
            }
            //js.Data = jc;
            return MstPaymentID;
        }

        public PaymentDetails GetPaymentDetailsByPaymentID(long PaymentID)
        {
            var lstPaymentDetails = new List<PaymentDetails>();
            SqlCommand cmd = new SqlCommand("Get_PaymentDetailsByPaymentID", cn);
            cmd.Parameters.AddWithValue("@PaymentID", PaymentID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);

                lstPaymentDetails = dt.AsEnumerable().Select(t => {
                    var paymentDetails = new PaymentDetails();
                    paymentDetails.MunicipalityID = t.Field<int>("MunicipalityID");
                    paymentDetails.WardID = t.Field<int>("WardID");
                    paymentDetails.LocationID = t.Field<int>("LocationID");
                    paymentDetails.HoldingNo = t.Field<string>("HoldingNo");
                    paymentDetails.AssesseeName = t.Field<string>("AssesseeName");
                    paymentDetails.AssesseeNo = t.Field<string>("AssesseeNo");
                    paymentDetails.PaymentMode = t.Field<string>("PaymentMode");
                    paymentDetails.ReceiptNo = t.Field<string>("ReceiptNo");
                    paymentDetails.PaymentCollectionDate = t.Field<string>("PaymentCollectionDate");
                    paymentDetails.PaidAmount = t.Field<decimal>("PaidAmount");
                    return paymentDetails;
                }).ToList();

                return lstPaymentDetails[0];
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            finally
            {
                cn.Close();
            }
        }

    }
}