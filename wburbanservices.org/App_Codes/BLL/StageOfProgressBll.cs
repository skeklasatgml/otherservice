﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.Models.Application;
using static Valuation_Board.Models.Application.DprModel;
using static Valuation_Board.Models.Application.ScheduleThree;
using static Valuation_Board.Models.Application.StageOfProgress;

namespace Valuation_Board.App_Codes.BLL
{
    public class StageOfProgressBll : IDisposable
    {
        protected DbHandler _db;
        protected SqlConnection cn;

        public StageOfProgressBll()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }

        public void Dispose()
        {
            cn.Close();
        }
        SqlCommand newCommand(string commandText)
        {
            var cmd = _db.NewCommand(commandText, cn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }
        public List<StageGroup> getStageGroups(int? Id = null)
        {
            var cmd = newCommand("udma.get_StageGroups");
            cmd.Parameters.AddWithValue("@Id", Id.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<StageGroup>();
        }
        public List<WorkMileStone> getMileStones(int? groupId = null, int? milestoneId = null)
        {
            var cmd = newCommand("udma.get_StageMilestones");
            cmd.Parameters.AddWithValue("@Id", milestoneId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@groupId", groupId.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<WorkMileStone>();
        }
        public void includeMileStone(List<StageGroup> stageGroups)
        {
            stageGroups.ForEach(t =>
            {
                t.WorkMileStones = getMileStones(t.Id);
            });
        }

        public StageGroup SaveStageGroup(IdVal<int?, string> payload)
        {
            var cmd = newCommand("udma.save_stageGroup");
            cmd.Parameters.AddWithValue("@Id", payload.Id.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@name", payload.Value.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<StageGroup>().FirstOrDefault();
        }
        public WorkMileStone SaveMileStone(WorkMileStone payload)
        {
            if (cn.State != System.Data.ConnectionState.Open)
                cn.Open();
            var cmd = newCommand("udma.save_milestone");
            cmd.Parameters.AddWithValue("@Id", payload.Id.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@Dscr", payload.Dscr.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@percentage", payload.PerComplited.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@groupId", payload.GroupId.ReplaceDbNull());
            int x = _db.GetScalar<int>(cmd);
            return getMileStones(null, x).FirstOrDefault();
        }
        public void DeleteMileStone(int mileStoneId)
        {
            var cmd = newCommand("udma.del_MileStone");
            cmd.Parameters.AddWithValue("@Id", mileStoneId.ReplaceDbNull());
            _db.GetResult(cmd);
        }
        public List<SchemeCategoryMappedMileStone> getSchemeCategoriesMappedMilestoneGrp(int? schemeId, int? groupId)
        {
            var cmd = newCommand("udma.get_SchemeCategoriesMappedMilestones");
            cmd.Parameters.AddWithValue("@schemeTypeId", schemeId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@groupId", groupId.ReplaceDbNull());
            return _db.GetResult(cmd).Convert<SchemeCategoryMappedMileStone>();
        }

        public void updateMappingSchemeteMilestoneGrp(int? scemeCatId, int? groupId, bool isMapped)
        {
            var cmd = newCommand("udma.update_SchemeCatMilestoneGrpMapping");
            cmd.Parameters.AddWithValue("@scemeCatId", scemeCatId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@groupId", groupId.ReplaceDbNull());
            cmd.Parameters.AddWithValue("@isMapped", isMapped.ReplaceDbNull());
            _db.GetResult(cmd);
        }
    }
}