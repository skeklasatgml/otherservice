﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.App_Codes.BLL
{
    public class ArrearDemandBll
    {
        
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ArrearDemandBll()
        {
            cn = new SqlConnection(constring);
        }
        public void UpdateArrears(List<ArrearDemand> arrearDemands, long userID,long assesseeId,int municipalityID)
        {
            SqlTransaction sqlTransaction = null;
            try
            {
                cn.Open();
                sqlTransaction = cn.BeginTransaction();
                arrearDemands.ForEach(t => {
                    SqlCommand cmd = new SqlCommand("Update_ArrearDemand", cn, sqlTransaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                    cmd.Parameters.AddWithValue("@FinYear", t.FinYear ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@Qtr", t.Qtr);
                    cmd.Parameters.AddWithValue("@PTaxAmount", t.PropertyTaxAmnt);
                    cmd.Parameters.AddWithValue("@SurchargeAmount", t.SurchargeAmnt);
                    cmd.Parameters.AddWithValue("@UserID", userID);
                    
                    cmd.ExecuteNonQuery();
                });
                sqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                sqlTransaction.Rollback();
                throw new Exception("DbError: Arrear not updated. Details: "+ex.Message);
            }
            finally
            {
                cn.Close();
            }
            
        }
        public void InsertArrears(FinYearQtr fromFinYearQtr, FinYearQtr toFinYearQtr,decimal pTax,decimal sCh, long userID, long assesseeId, int municipalityID)
        {
            SqlTransaction sqlTransaction = null;
            try
            {
                if(fromFinYearQtr>toFinYearQtr)
                {
                    throw new Exception("[From Fin-Year Qtr.] should be greater or equal to [To Fin-Year Qtr.]");
                }
                if(pTax<0 || sCh<0)
                {
                    throw new Exception("Value of Property Tax or Surcharge can not be less than 0");
                }

                Func<FinYearQtr, FinYearQtr, List<FinYearQtr>> GetDecomposedFinYearRangeList = (_fromFinYearQtr, _toFinYearQtr) => {
                    #region description and sample
                    //make a list linearly fin year qtr
                    /***
                     * sample list _fromFinYearQtr=[2012-2013,1] and _toFinYearQtr=[2013-2014,3]
                     * ---------------
                     * 2012-2013,1
                     * 2012-2013,2
                     * 2012-2013,3
                     * 2012-2013,4
                     * 2013-2014,1
                     * 2013-2014,2
                     * 2013-2014,3
                     * 
                     ***/
                    #endregion
                    List<FinYearQtr> lstFinYearQtr = new List<FinYearQtr>();
                    FinYearQtr currFinyear = _fromFinYearQtr;
                    while (currFinyear <= _toFinYearQtr)
                    {

                        lstFinYearQtr.Add(CommonUtils.DeepClone<FinYearQtr>(currFinyear));
                        currFinyear.AddQtr(1);
                    }
                    //throw new Exception();
                    return lstFinYearQtr;
                };
                //validate finyear qtr insertable
                Func<List<FinYearQtr>, List<FinYearQtr>> isValidInsertableFinYear = (_lstFinYears) =>
                {
                    List<FinYearQtr> _invalidFinYearList = new List<FinYearQtr>();
                    PropertyTaxCollectionBLL propertyTaxCollectionBLL = new PropertyTaxCollectionBLL();
                    AssesseeBll assesseeBll = new AssesseeBll();
                    Assessee assessee = assesseeBll.GetAssesseebyId(assesseeId);
                    if (assessee != null && assessee.MunicipalityID == municipalityID)
                    {
                        List<PropertyTaxOutstading> lstPropOutStanding = propertyTaxCollectionBLL.GetAllArrearDemanBillRecord(assessee.WardID ?? 0, assessee.AssesseeID, assessee.LocationID ?? 0, assessee.MunicipalityID ?? 0);
                        _lstFinYears.ForEach(t =>
                        {
                            var findAnyRow = lstPropOutStanding.FirstOrDefault(f => f.FinYear == t.GetFinYear() && f.QtrNo == t.Qtr);
                            if (findAnyRow!=null)
                            {
                                _invalidFinYearList.Add(t);
                            }
                        });
                    }
                    else
                    {
                        throw new ArgumentException("Assessee not found! Invalid AssesseeID");
                    }
                    return _invalidFinYearList;
                };
                List<FinYearQtr> lstDecomposedFinYears = GetDecomposedFinYearRangeList(fromFinYearQtr, toFinYearQtr);
                List<FinYearQtr> invalidFinYearList= isValidInsertableFinYear(lstDecomposedFinYears);
                if(invalidFinYearList.Count>0)
                {

                    Dictionary<string, FinYearQtr> datas = invalidFinYearList.ToDictionary(t => t.ToString(), t=>t );
                    throw new AppException<string,FinYearQtr>("FinYear Qtr Validation Failed","InsertArrear.InvalidFinyearsQtr", datas);
                }
                cn.Open();
                sqlTransaction = cn.BeginTransaction();
                lstDecomposedFinYears.ForEach(t =>
                {
                    SqlCommand cmd = new SqlCommand("Insert_ArrearDemand", cn, sqlTransaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                    cmd.Parameters.AddWithValue("@FinYear", t.GetFinYear() ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@Qtr", t.Qtr);
                    cmd.Parameters.AddWithValue("@PTaxAmount", pTax);
                    cmd.Parameters.AddWithValue("@SurchargeAmount", sCh);
                    cmd.Parameters.AddWithValue("@UserID", userID);

                    cmd.ExecuteNonQuery();
                });
                sqlTransaction.Commit();
            }
            catch(AppException)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();
                
                    throw;
                
            }
            catch (Exception ex)
            {
                if(sqlTransaction!=null)
                sqlTransaction.Rollback();

               
                throw new Exception("DbError: Arrear not Inserted. Details: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }

        }

        public void UpdateDemand(List<ArrearDemand> Demands, long userID, long assesseeId, int municipalityID)
        {
            SqlTransaction sqlTransaction = null;
            try
            {
                int[] supportedDemandQtrs = new int[] { 1, 2, 3, 4 };
                PropertyTaxCollectionBLL propertyTaxCollectionBLL = new PropertyTaxCollectionBLL();
                AssesseeBll assesseeBll = new AssesseeBll();
                Assessee assessee = assesseeBll.GetAssesseebyId(assesseeId);
                List<PropertyTaxOutstading> lstPropOutStanding = propertyTaxCollectionBLL.GetAllArrearDemanBillRecord(assessee.WardID ?? 0, assessee.AssesseeID, assessee.LocationID ?? 0, assessee.MunicipalityID ?? 0);

                Demands.ForEach(t => {
                    if(supportedDemandQtrs.Contains(t.Qtr)==false)
                    {
                        throw new ArgumentException(string.Format("invalid qtr no {0}",t.Qtr));
                    }
                    if(t.FinYear!=CommonUtils.GetFinYear(DateTime.Now))
                    {
                        throw new ArgumentException(string.Format("invalid Fin-Year no {0}", t.FinYear));
                    }
                    var finYear = lstPropOutStanding.FirstOrDefault(s => s.FinYear == t.FinYear && s.QtrNo == t.Qtr);
                    if(finYear!=null)
                    {
                        if(finYear.OSPropertyTaxAmt+finYear.OSSurchargeAmt<=0)
                        {
                            throw new ArgumentException(string.Format("{0},{1} can not be updated because already get paid",t.FinYear,t.Qtr));
                        }
                    }
                    if(t.PropertyTaxAmnt+t.SurchargeAmnt<0)
                    {
                        throw new ArgumentException(string.Format("The value of (Surcharge+Property Tax Amount) can not be less than zero on {0},{0}",t.FinYear,t.Qtr));
                    }
                });
                if(Demands.Count>4)
                {
                    throw new ArgumentException(string.Format("A Fin-Year may have maximum 4 qtr"));
                }

                cn.Open();
                sqlTransaction = cn.BeginTransaction();
                Demands.ForEach(t => {
                    SqlCommand cmd = new SqlCommand("Update_CurrentDemand", cn, sqlTransaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                    cmd.Parameters.AddWithValue("@FinYear", t.FinYear ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@Qtr", t.Qtr);
                    cmd.Parameters.AddWithValue("@PTaxAmount", t.PropertyTaxAmnt);
                    cmd.Parameters.AddWithValue("@SurchargeAmount", t.SurchargeAmnt);
                    cmd.Parameters.AddWithValue("@UserID", userID);

                    cmd.ExecuteNonQuery();
                });
                sqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                sqlTransaction.Rollback();
                throw new Exception("DbError: Demand not updated. Details: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }

        }
        public void InsertDemand(List<ArrearDemand> Demands, long userID, long assesseeId, int municipalityID)
        {
            SqlTransaction sqlTransaction = null;
            try
            {
                int[] supportedDemandQtrs = new int[] { 1, 2, 3, 4 };
                PropertyTaxCollectionBLL propertyTaxCollectionBLL = new PropertyTaxCollectionBLL();
                AssesseeBll assesseeBll = new AssesseeBll();
                Assessee assessee = assesseeBll.GetAssesseebyId(assesseeId);
                List<PropertyTaxOutstading> lstPropOutStanding = propertyTaxCollectionBLL.GetAllArrearDemanBillRecord(assessee.WardID ?? 0, assessee.AssesseeID, assessee.LocationID ?? 0, assessee.MunicipalityID ?? 0);

                Demands.ForEach(t => {
                    if (supportedDemandQtrs.Contains(t.Qtr) == false)
                    {
                        throw new ArgumentException(string.Format("invalid qtr no {0}", t.Qtr));
                    }
                    if (t.FinYear != CommonUtils.GetFinYear(DateTime.Now))
                    {
                        throw new ArgumentException(string.Format("invalid Fin-Year no {0}", t.FinYear));
                    }
                    var finYear = lstPropOutStanding.FirstOrDefault(s => s.FinYear == t.FinYear && s.QtrNo == t.Qtr);
                    if (finYear != null)
                    {
                        
                            throw new ArgumentException(string.Format("{0},{1} can not be Inserted because already get Inserted", t.FinYear, t.Qtr));
                        
                    }
                });
                if (Demands.Count > 4)
                {
                    throw new ArgumentException(string.Format("A Fin-Year may have maximum 4 qtr"));
                }

                cn.Open();
                sqlTransaction = cn.BeginTransaction();
                Demands.ForEach(t => {
                    SqlCommand cmd = new SqlCommand("Insert_CurrentDemand", cn, sqlTransaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
                    cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                    cmd.Parameters.AddWithValue("@FinYear", t.FinYear ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@Qtr", t.Qtr);
                    cmd.Parameters.AddWithValue("@PTaxAmount", t.PropertyTaxAmnt);
                    cmd.Parameters.AddWithValue("@SurchargeAmount", t.SurchargeAmnt);
                    cmd.Parameters.AddWithValue("@UserID", userID);

                    cmd.ExecuteNonQuery();
                });
                sqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                if(sqlTransaction!=null)
                sqlTransaction.Rollback();
                throw new Exception("DbError: Demand not updated. Details: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }

        }
        public void InsertReview(string FromFinYear, string FromQtr, string ToFinYear, string ToQtr, decimal pTax, decimal sCh, string ReferanceNo,
                                 string ReferanceText, long userID, long assesseeId, int municipalityID, string ReviewID)
        {
            int finFrom = Convert.ToInt32(FromFinYear.Substring(0, 4));
            int finTo = Convert.ToInt32(ToFinYear.Substring(0, 4));
            if (finFrom > finTo)
            {
                throw new Exception("[From Fin-Year.] should be greater to [To Fin-Year.]");
            }
            if (pTax < 0 || sCh < 0)
            {
                throw new Exception("Value of Property Tax or Surcharge can not be less than 0");
            }
            SqlCommand cmd = new SqlCommand("Insert_DatReview", cn);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID == null ? DBNull.Value : (object)Convert.ToInt64(ReviewID));
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", assesseeId);
            cmd.Parameters.AddWithValue("@FromFinYear", FromFinYear);
            cmd.Parameters.AddWithValue("@FromQtrNo", FromQtr);
            cmd.Parameters.AddWithValue("@ToFinYear", ToFinYear);
            cmd.Parameters.AddWithValue("@ToQtrNo", ToQtr);
            cmd.Parameters.AddWithValue("@PtaxRValue", pTax);
            cmd.Parameters.AddWithValue("@SurchargeRValue", sCh);
            cmd.Parameters.AddWithValue("@RefNo", ReferanceNo);
            cmd.Parameters.AddWithValue("@RefRemarks", ReferanceText);
            cmd.Parameters.AddWithValue("@InsertedBy", userID);

            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("DbError: Review not Inserted. Details:...!" + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR...: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<ReviewData_VM> Get_AllReviewData(long AssesseeID, int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_ReviewData", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertReviewData(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        List<ReviewData_VM> ConvertReviewData(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                ReviewData_VM p = new ReviewData_VM();
                p.ReviewID = t.Field<long>("ReviewID");
                p.MunicipalityID = t.Field<int>("MunicipalityID");
                p.AssesseeID = t.Field<long>("AssesseeID");
                p.FromFinYear = t.Field<string>("FromFinYear");
                p.FromQtr = t.Field<int>("FromQtrNo").ToString();
                p.ToFinYear = t.Field<string>("ToFinYear");
                p.ToQtr = t.Field<int>("ToQtrNo").ToString();
                p.RPtax = t.Field<decimal?>("PtaxRValue");
                p.RSch = t.Field<decimal?>("SurchargeRValue");
                p.ReferanceNo = t.Field<string>("RefNo");
                p.ReferanceText = t.Field<string>("RefRemarks");
                p.ApprovedFlag = t.Field<string>("ApprovedFlag");
                p.Status = t.Field<string>("Status");
                return p;
            }).ToList();
        }
    }
}