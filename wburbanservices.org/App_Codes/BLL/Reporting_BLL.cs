﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class Reporting_BLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public Reporting_BLL()
        {
            cn = new SqlConnection(constring);
        }
        public Report GetReport(Int64 reportCode)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("GET_ReportDetail", cn);
                cmd.Parameters.AddWithValue("@ReportCode",reportCode);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return ConvertToReport(dt)[0];
                }
                return null;
            }
            catch 
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<Report> ConvertToReport(DataTable dt)
        {

            List<Report> lst = new List<Models.Application.Report>();
            foreach (DataRow row in dt.Rows)
            {
                lst.Add(new Report() {
                    ReportCode = row.Field<int>("ReportCode"),
                    ReportFilePath = row.Field<string>("ReportFilePath"),
                    ReportName = row.Field<string>("ReportName"),
                    ConnectionString = row.Field<string>("dbConnectionString")
                });
            }
            return lst;
        }

        public bool CheckUserAuthorisation(int reportCode,long UserID)
        {
            bool ret = false;
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("GET_ReportAuthorisation", cn);
                cmd.Parameters.AddWithValue("@ReportCode", reportCode);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (Convert.ToInt16(dt.Rows[0]["cnt"]) > 0)
                {
                    ret = true;
                }
            }
            catch 
            {
                throw new Exception("Database Error");
            }
            finally
            {
                cn.Close();
            }
            return ret;
        }
    }
}