﻿using System.Configuration;
using System.Data.SqlClient;
using System;
using Valuation_Board.Models.Application;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace Valuation_Board.App_Codes.BLL
{
    public class TaxPaymentMode_BLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public TaxPaymentMode_BLL()
        {
            cn = new SqlConnection(conString);
        }
        public List<PaymentMode> GetPaymentModes(int municipalityID, long municipalityUserID, DateTime date)
        {
            List<PaymentMode> lst = new List<PaymentMode>();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Get_MuncpltyCounterPaymentModes", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@municipalityID",municipalityID);
                cmd.Parameters.AddWithValue("@MunicipalityUserID", municipalityUserID);
                cmd.Parameters.AddWithValue("@curDate", date.ToString("yyyy'-'MM'-'dd"));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    lst= ConvertToPaymentMode(dt);
                }
               
            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("DataBase Error", ex);
            }
            return lst;
        }
        public List<PaymentMode> GetPaymentMethods(int municipalityID, int municipalityCounterID, DateTime date)
        {
            throw new NotImplementedException();
        }
        #region Converters
        List<PaymentMode> ConvertToPaymentMode(DataTable dt)
        {
            List<PaymentMode> lst = new List<PaymentMode>();
            lst = dt.AsEnumerable().Select(t=> {
                PaymentMode pobj = new PaymentMode();
                pobj.PaymentModeCode = t.Field<string>("PaymentModeCode");
                pobj.PaymentModeDescription = t.Field<string>("PaymentModeDescription");
                pobj.HasInstrument = t.Field<bool>("HasInstrument");
                pobj.ModeType = t.Field<int>("type");
                return pobj;
            }).ToList();
            return lst;
        }
        #endregion

    }
}