﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using static Valuation_Board.ViewModels.Assessment_VM;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssesseeBll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        DbHandler _db;
        SqlConnection cn;
        public AssesseeBll()
        {
            this._db = new DbHandler();
            this.cn = _db.NewConnection();
        }
        public Assessee GetAssesseebyId(long AssesseeID)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByAsseeID", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert1(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public Assessee GetAssesseebyIdForKnowYourProps(long AssesseeID)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByAsseeIDForKnowYourProps", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", AssesseeID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Assessee> GetAssessee(string AssesseeNo)
        {

            SqlCommand cmd = new SqlCommand("Get_AssesseeByAsseeNo", cn);
            cmd.Parameters.AddWithValue("@AssesseeNo", AssesseeNo);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public Assessee GetAssessee(int municipalityID, int wardID, int locationID,string holdingNumber)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByHldngnoMncpltyIDWrdIDLctnID", cn);
            cmd.Parameters.AddWithValue("@HoldingNo", holdingNumber);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@WardID", wardID);
            cmd.Parameters.AddWithValue("@LocationID", locationID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs= Convert(dt);
                if(objs.Count>0)
                {
                    return objs.FirstOrDefault(t=>t.HoldingNo.ToUpper()==holdingNumber.Trim().ToUpper());
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public  List<Assessee> SearchHolding(int municipalityID, int wardID, int locationID, string holdingNumber)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByHldngnoMncpltyIDWrdIDLctnID", cn);
            cmd.Parameters.AddWithValue("@HoldingNo", holdingNumber);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@WardID", wardID);
            cmd.Parameters.AddWithValue("@LocationID", locationID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var objs = Convert(dt);
                return objs;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Assessee> GetAssessees(List<long> assesseIds)//assesseeIds
        {
            SqlCommand cmd = _db.NewCommand("Get_AssesseesByIds", cn);
            cmd.Parameters.AddWithValue("@assesseeIds", string.Join(",",assesseIds));
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error: "+ex.Message, ex);
            }
            finally
            {
                cn.Close();
            }
        }

        internal List<Assessee> GetAssessees(int municipalityID, string holdingText)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByHoldingText", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@HoldingNo", holdingText);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        internal List<Assessee> GetAssessees(int municipalityID ,string holdingText, long userID)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByUserIdNHoldingText", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@HoldingNo", holdingText);
            cmd.Parameters.AddWithValue("@userId", userID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert2(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception( ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }


        public List<Assessee> GetAssessees(int municipalityID, int wardID, int locationID, string assesseeName)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeBySuggestionTxt", cn);
            cmd.Parameters.AddWithValue("@WardID", wardID);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@AssesseeName", assesseeName);
            cmd.Parameters.AddWithValue("@LocationID", locationID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert2(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        internal List<Assessee> GetAssesseesForKnowyourProps(int municipalityID, string holdingText, string assesseeName, string searchTypeVal)
        {
            SqlCommand cmd = new SqlCommand("Get_AssesseeByHoldingNoAssesseeNameForKnowYourProps", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@HoldingNo", holdingText);
            cmd.Parameters.AddWithValue("@AssesseeName", assesseeName);
            cmd.Parameters.AddWithValue("@SearchTypeVal", searchTypeVal);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert2(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<string> GetHoldingBySuggestion(int municipalityID, int wardID, int locationID,string holdingSuggestionTxt)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_HoldingNoSuggetion",cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                cmd.Parameters.AddWithValue("@WardID", wardID);
                cmd.Parameters.AddWithValue("@LocationID", locationID);
                cmd.Parameters.AddWithValue("@HoldingSuggetionTxt", holdingSuggestionTxt);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt.AsEnumerable().Select(t=>t.Field<string>("HoldingNo")).ToList();

            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public void UpdateAssesseeMobileNo(long assesseeID,string mobileNo)
        {
            SqlCommand cmd = new SqlCommand("Update_AssesseeMobileNo", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
            cmd.Parameters.AddWithValue("@MobileNo", (string.IsNullOrEmpty(mobileNo)?DBNull.Value: mobileNo.Trim() as object));
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public void UpdateAssesseeEmailID(long assesseeID, string Email)
        {
            SqlCommand cmd = new SqlCommand("Update_AssesseeEmail", cn);
            cmd.Parameters.AddWithValue("@AssesseeID", assesseeID);
            cmd.Parameters.AddWithValue("@Email", (string.IsNullOrEmpty( Email) ? DBNull.Value : Email.Trim() as object));
            cmd.CommandType = CommandType.StoredProcedure;
           
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }
        public Assessee InsertNewAssesse(AssesseWithRef assessee)
        {
            SqlTransaction tr = null;
            
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand("Insert_AssesseeWithReferenceNo", cn,tr);
                cmd.Parameters.AddWithValue("@MunicipalityID", assessee.MunicipalityID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@WardID", assessee.WardID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@HoldingTypeID", assessee.HoldingTypeID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@LocationID", assessee.LocationID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@HoldingNo", assessee.HoldingNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeName", assessee.AssesseeName ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseAddress", assessee.AssesseeAddress ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PrimaryPhNo", assessee.PrimaryPhNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PrimaryEmail", assessee.PrimaryEmail ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@InsertedBy", assessee.InsertedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@refNo", assessee.ReferenceNo);
                cmd.Parameters.AddWithValue("@refDate", assessee.ReferenceDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@OldAssesseeID", assessee.OldAssesseeNo ?? DBNull.Value as object);

                cmd.Parameters.AddWithValue("@Block", assessee.Block ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Mouja", assessee.Mouja ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Plot", assessee.Plot ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Khatian", assessee.Khatian ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@JLNo", assessee.JLNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@DeedNo", assessee.DeedNo ?? DBNull.Value as object);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
               
                
                da.Fill(dt);
                tr.Commit();
                cn.Close();
                if(dt.Rows.Count>0)
                {
                    return GetAssesseebyId(dt.Rows[0].Field<long>("AssesseeID"));
                }
                return null;
            }
            catch (SqlException ex)
            {
                tr.Rollback();
                if(ex.Message.Contains("UNIQUE KEY"))
                {
                    throw new Exception("Assessee already exists");
                }
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
            
        }
        public void UpdateAssessee(AssesseWithRef assessee)
        {
            SqlTransaction tr = null;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand("Update_AssesseeWithReferenceNo", cn,tr);
                cmd.Parameters.AddWithValue("@MunicipalityID", assessee.MunicipalityID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@WardID", assessee.WardID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@HoldingTypeID", assessee.HoldingTypeID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@LocationID", assessee.LocationID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@HoldingNo", assessee.HoldingNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeName", assessee.AssesseeName ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseAddress", assessee.AssesseeAddress ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PrimaryPhNo", assessee.PrimaryPhNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@PrimaryEmail", assessee.PrimaryEmail ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@UpdatedBy", assessee.UpdatedBy ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@OldAssesseeNo", assessee.OldAssesseeNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AssesseeID", assessee.AssesseeID);
                cmd.Parameters.AddWithValue("@refNo", assessee.ReferenceNo);
                cmd.Parameters.AddWithValue("@refDate", assessee.ReferenceDate.ToString("yyyy'-'MM'-'dd"));
                cmd.Parameters.AddWithValue("@IsDisputed", assessee.IsDisputed);
                cmd.Parameters.AddWithValue("@DisputeReason", assessee.DisputeReason??DBNull.Value as object);

                cmd.Parameters.AddWithValue("@Block", assessee.Block ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Mouja", assessee.Mouja ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Plot", assessee.Plot ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@Khatian", assessee.Khatian ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@JLNo", assessee.JLNo ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@DeedNo", assessee.DeedNo ?? DBNull.Value as object);

                cmd.Parameters.AddWithValue("@DeedRegistrationYear", assessee.DeedRegistrationYear ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@DeedRegistrationOffice", assessee.DeedRegistrationOffice ?? DBNull.Value as object);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                tr.Commit();
            }
            catch (SqlException ex)
            {
                tr.Rollback();
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }

        }
        public Assessee IncludeLocation(Assessee assessee)
        {
            LocationBLL bll = new LocationBLL();
            int[] locations= { assessee.LocationID ??0 };
            List<Location> lst= bll.GetLocationsIDRange(assessee.WardID ?? 0, assessee.MunicipalityID ?? 0,locations.ToList());
            if(lst.Count>0)
            {
                assessee.Location = lst[0];
            }
            return assessee;
        }
        
        public Assessee IncludeWard(Assessee assessee)
        {
            WardBLL bll = new WardBLL();
            int[] wardIDs = { assessee.WardID ?? 0 };
            List<Ward> lst = bll.GetWards(wardIDs.ToList(),assessee.MunicipalityID ??0);
            if (lst.Count > 0)
            {
                assessee.Ward = lst[0];
            }
            return assessee;
        }
        public List<Assessee> IncludeWard(List<Assessee> assessees,int municipalityID)
        {
            WardBLL bll = new WardBLL();
            int[] wardIDs = assessees.Select(t => t.WardID??0).ToArray();
            List<Ward> lst = bll.GetWards(wardIDs.ToList(), municipalityID);

            assessees.ForEach(t=>t.Ward=lst.FirstOrDefault(s=>s.WardID==t.WardID));
            return assessees;
        }
        public Assessee IncludeMunicipality(Assessee assessee)
        {
            MunicipalityBLL bll = new MunicipalityBLL();
            int[] municplityIds = { assessee.MunicipalityID ?? 0 };
            List<Municipality> lst = bll.GetMunicipalitiesByIds(municplityIds);
            if (lst.Count > 0)
            {
                assessee.Municipality = lst[0];
            }
            return assessee;
        }

        #region Converters
        public List<Assessee> Convert(DataTable dt)
        {
            List<Assessee> lst = new List<Assessee>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Assessee();
                obj.AssesseeID = t.Field<long>("AssesseeID");
                obj.OldAssesseeNo = t.Field<long?>("OldAssesseeNo");
                obj.ParentAssesseeID = t.Field<long?>("ParentAssesseeID");
                obj.AssesseeNo = t.Field<string>("AssesseeNo");
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.WardID = t.Field<int?>("WardID");
                if(dt.Columns.Contains("WardName"))
                obj.WardName = t.Field<string>("WardName");
                obj.LocationID = t.Field<int?>("LocationID");
                if (dt.Columns.Contains("WardName"))
                    obj.LocationName = t.Field<string>("LocationName");
                obj.HoldingTypeID = t.Field<int?>("HoldingTypeID");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                obj.AssesseeName = t.Field<string>("AssesseeName");
                obj.AssesseeAddress = t.Field<string>("AssesseeAddress");
                obj.EffectiveFrom = t.Field<DateTime?>("EffectiveFrom");
                obj.EffectiveTo = t.Field<DateTime?>("EffectiveTo");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                if (t.Table.Columns.Contains("HoldingTypeDesc"))
                {
                    obj.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                }
                obj.PrimaryPhNo = t.Field<string>("PrimaryPhNo");
                obj.PrimaryEmail = t.Field<string>("PrimaryEmail");
                obj.IsPrimaryEmailVerified = (t.Field<byte?>("IsPrimaryEmailVerified") ?? 0) == 0 ? false : true;
                obj.IsPrimaryPhoneNoVerified = (t.Field<byte?>("IsPrimaryPhoneNoVerified") ?? 0) == 0 ? false : true;
                obj.IsDisputed = t.Table.Columns.Contains("IsDisputed") ? t.Field<bool>("IsDisputed") : false;
                obj.DisputeReason= t.Table.Columns.Contains("DisputeReason") ? t.Field<string>("DisputeReason") : null;

                obj.Block = t.Field<string>("Block");
                obj.Mouja = t.Field<string>("Mouja");
                obj.Plot = t.Field<string>("Plot");
                obj.Khatian = t.Field<string>("Khatian");
                obj.JLNo = t.Field<string>("JLNo");
                obj.DeedNo = t.Field<string>("DeedNo");
                obj.MoujaID = t.Field<int?>("MoujaID");

                obj.DeedRegistrationYear = t.Field<string>("DeedRegistrationYear");
                obj.DeedRegistrationOffice = t.Field<int?>("DeedRegistrationOffice");

                return obj;
            }).ToList();
            return lst;
        }
        public List<Assessee> Convert1(DataTable dt)
        {
            List<Assessee> lst = new List<Assessee>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Assessee();
                obj.IsDisputed= t.Field<bool>("isDisputed");
                obj.AssesseeID = t.Field<long>("AssesseeID");
                obj.ParentAssesseeID = t.Field<long?>("ParentAssesseeID");
                obj.AssesseeNo = t.Field<string>("AssesseeNo");
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.WardID = t.Field<int?>("WardID");
                obj.LocationID = t.Field<int?>("LocationID");
                obj.HoldingTypeID = t.Field<int?>("HoldingTypeID");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                obj.AssesseeName = t.Field<string>("AssesseeName");
                obj.AssesseeAddress = t.Field<string>("AssesseeAddress");
                obj.EffectiveFrom = t.Field<DateTime?>("EffectiveFrom");
                obj.EffectiveTo = t.Field<DateTime?>("EffectiveTo");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                if (t.Table.Columns.Contains("HoldingTypeDesc"))
                {
                    obj.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                }
                obj.PrimaryPhNo = t.Field<string>("PrimaryPhNo");
                obj.PrimaryEmail = t.Field<string>("PrimaryEmail");
                obj.IsPrimaryEmailVerified = (t.Field<byte?>("IsPrimaryEmailVerified") ?? 0) == 0 ? false : true;
                obj.IsPrimaryPhoneNoVerified = (t.Field<byte?>("IsPrimaryPhoneNoVerified") ?? 0) == 0 ? false : true;

                return obj;
            }).ToList();
            return lst;
        }

        public List<Assessee> Convert2(DataTable dt)
        {
            List<Assessee> lst = new List<Assessee>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Assessee();
                obj.AssesseeID = t.Field<long>("AssesseeID");
                obj.OldAssesseeNo = t.Field<long?>("OldAssesseeNo");
                obj.ParentAssesseeID = t.Field<long?>("ParentAssesseeID");
                obj.AssesseeNo = t.Field<string>("AssesseeNo");
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.WardID = t.Field<int?>("WardID");
                if (dt.Columns.Contains("WardName"))
                    obj.WardName = t.Field<string>("WardName");
                obj.LocationID = t.Field<int?>("LocationID");
                if (dt.Columns.Contains("WardName"))
                    obj.LocationName = t.Field<string>("LocationName");
                obj.HoldingTypeID = t.Field<int?>("HoldingTypeID");
                obj.HoldingNo = t.Field<string>("HoldingNo");
                obj.AssesseeName = t.Field<string>("AssesseeName");
                obj.AssesseeAddress = t.Field<string>("AssesseeAddress");
                obj.EffectiveFrom = t.Field<DateTime?>("EffectiveFrom");
                obj.EffectiveTo = t.Field<DateTime?>("EffectiveTo");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                if (t.Table.Columns.Contains("HoldingTypeDesc"))
                {
                    obj.HoldingTypeDesc = t.Field<string>("HoldingTypeDesc");
                }
                obj.PrimaryPhNo = t.Field<string>("PrimaryPhNo");
                obj.PrimaryEmail = t.Field<string>("PrimaryEmail");
                obj.IsPrimaryEmailVerified = (t.Field<byte?>("IsPrimaryEmailVerified") ?? 0) == 0 ? false : true;
                obj.IsPrimaryPhoneNoVerified = (t.Field<byte?>("IsPrimaryPhoneNoVerified") ?? 0) == 0 ? false : true;
                obj.IsDisputed = t.Table.Columns.Contains("IsDisputed") ? t.Field<bool>("IsDisputed") : false;
                obj.DisputeReason = t.Table.Columns.Contains("DisputeReason") ? t.Field<string>("DisputeReason") : null;

                //obj.Block = t.Field<string>("Block");
                //obj.Mouja = t.Field<string>("Mouja");
                //obj.Plot = t.Field<string>("Plot");
                //obj.Khatian = t.Field<string>("Khatian");
                //obj.JLNo = t.Field<string>("JLNo");
                //obj.DeedNo = t.Field<string>("DeedNo");


                return obj;
            }).ToList();
            return lst;
        }
        #endregion

        public List<Block_VM> GetBlocks(int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_BlockByAssesseeID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertBlock(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Block_VM> ConvertBlock(DataTable dt)
        {
            List<Block_VM> lst = new List<Block_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Block_VM();
                obj.Bcode = t.Field<int?>("Bcode");
                obj.uni_bname = t.Field<string>("uni_bname");
                return obj;
            }).ToList();
            return lst;
        }

        public List<Mauja_VM> GetMaujas(int municipalityID, int Blockid)
        {
            SqlCommand cmd = new SqlCommand("Get_MaujaByBlockID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@BlockID", Blockid);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertMauja(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Mauja_VM> ConvertMauja(DataTable dt)
        {
            List<Mauja_VM> lst = new List<Mauja_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Mauja_VM();
                obj.Moucode = t.Field<int?>("Moucode");
                obj.eng_mouname = t.Field<string>("eng_mouname");
                return obj;
            }).ToList();
            return lst;
        }


        public List<RegOfficeNIC_VM> GetRegOfficeNIC(int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_RegistrarOfficeNIC", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertRegOfficeNIC(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error!", ex);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<RegOfficeNIC_VM> ConvertRegOfficeNIC(DataTable dt)
        {
            List<RegOfficeNIC_VM> lst = new List<RegOfficeNIC_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new RegOfficeNIC_VM();
                obj.ROcode = t.Field<int?>("ROcode");
                obj.RO_name = t.Field<string>("RO_name");
                return obj;
            }).ToList();
            return lst;
        }
    }
}