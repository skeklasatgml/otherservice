﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class PaymentGateway_Bll
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public PaymentGateway_Bll()
        {
            cn = new SqlConnection(conString);
        }

        public List<PaymentGatewayMunicipality> GetPGsByMunicipalityId(int municipalityID)
        {
            SqlCommand cmd = new SqlCommand("Get_PgsByMunicipalityID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return ConvertToPaymentGatewayMunicipality(dt);
              
            }
            catch
            {
                throw new Exception("database error");
            }
            finally
            {
                cn.Close();
            }
        }
        List<PaymentGatewayMunicipality> ConvertToPaymentGatewayMunicipality(DataTable dt)
        {
            List<PaymentGatewayMunicipality> lst = new List<PaymentGatewayMunicipality>();
            foreach (DataRow dr in dt.Rows)
            {
                var obj = new PaymentGatewayMunicipality();
                obj.PG_Code = dr.Field<string>("PG_Code");
                obj.PG_Name = dr.Field<string>("PG_Name");
                obj.Description = dr.Field<string>("Description");
                obj.IsActive = dr.Field<bool>("IsActive");
                obj.MunicipalityID = dr.Field<int>("MunicipalityID");
                obj.MapID= dr.Field<int>("RowID");
                lst.Add(obj);
            }
            return lst;
        }
    }
}
