﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using System.Globalization;

namespace Valuation_Board.App_Codes.BLL
{
    public class AssessmentApprovalBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public AssessmentApprovalBLL()
        {
            cn = new SqlConnection(conString);
        }

        public List<AsessmentApproval_VM> Get_AllAssessmentData(int municipalityID, string EffectFromDT, string EffectToDT, string ApproveStatus)
        {
            DateTime dtFrom = DateTime.ParseExact(EffectFromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(EffectToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //System.Diagnostics.Debug.WriteLine(dtFrom.ToString("yyyy'-'MM'-'dd"));
            //DateTime dtFrom = Convert.ToDateTime(FromDate);
            //DateTime dtTo = Convert.ToDateTime(ToDate);

            SqlCommand cmd = new SqlCommand("Get_AssessmentApproval", cn);
            cmd.Parameters.AddWithValue("@FromDate", dtFrom.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@ToDate", dtTo.ToString("yyyy'-'MM'-'dd"));
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.Parameters.AddWithValue("@ApproveStatus", ApproveStatus);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
                return ConvertAllAdjEntity(dt);
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error", ex);
            }
            finally
            {
                cn.Close();
            }

        }

        List<AsessmentApproval_VM> ConvertAllAdjEntity(DataTable dt)
        {
            return dt.AsEnumerable().Select(t =>
            {
                AsessmentApproval_VM p = new AsessmentApproval_VM();
                p.MunicipalityID = t.Field<int?>("MunicipalityID");
                p.WardName = t.Field<string>("WardName");
                p.v_ward_no = t.Field<int?>("v_ward_no");
                p.LocationName = t.Field<string>("LocationName");
                p.v_street_code = t.Field<int?>("v_street_code");
                p.v_year = t.Field<int?>("v_year");
                p.v_id_no = t.Field<int?>("v_id_no");
                p.v_srl = t.Field<int?>("v_srl");
                p.v_holding_no = t.Field<string>("v_holding_no").ToString();
                p.v_name = t.Field<string>("v_name");
                p.v_address = t.Field<string>("v_address").ToString();

                p.v_yr_proptax = t.Field<decimal?>("v_yr_proptax");
                p.v_qtr_proptax = t.Field<decimal?>("v_qtr_proptax");
                p.v_val_year = t.Field<int?>("v_val_year");
                p.v_val_qtr = t.Field<int?>("v_val_qtr");

                p.ApprovedStatus = t.Field<string>("ApprovedStatus");

                p.v_entry_time = t.Field<string>("v_entry_time");
                return p;
            }).ToList();
        }

        public void UpdateAssessmentApproved(long UserID, AsessmentApproval_VM AsessmentApproval)
        {
            SqlCommand cmd = new SqlCommand("Update_AssessmentApproval", cn);
            cmd.Parameters.AddWithValue("@v_id_no", AsessmentApproval.v_id_no);
            cmd.Parameters.AddWithValue("@v_srl", AsessmentApproval.v_srl);
            cmd.Parameters.AddWithValue("@MunicipalityID", AsessmentApproval.MunicipalityID);
            cmd.Parameters.AddWithValue("@v_year", AsessmentApproval.v_year);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error...!" + ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
    }
}