﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class MunicipalityBLL
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public MunicipalityBLL()
        {
            cn = new SqlConnection(conString);
        }
        /// <summary>
        /// Fetch municipalities filtered by District id
        /// </summary>
        /// <param name="distID">District ID</param>
        /// <returns>Return List<Municipality></returns>
        public List<Municipality> GetMunicipalitiesByDistID(int distID)
        {
            SqlCommand cmd = new SqlCommand("Get_MunicipalityByDistricID_ForDPR", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DistrictID", distID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        #region Municipality by ID Range 
        public List<Municipality> GetMunicipalitiesByIds(params int[] ids)
        {
            if (ids == null || ids.Length == 0)
            {
                return new List<Municipality>();
            }
            string commaSeparatedNums = ids.Select(b => b.ToString()).Aggregate<string>((items, item) => items + "," + item);

            SqlCommand cmd = new SqlCommand("Get_MunicipalityByIDs", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MunicipalityIds", commaSeparatedNums);
            cmd.Parameters.AddWithValue("@Deliminator", ",");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }

            //make a comma concatinated string from ids "2,3,4,5,1"

        }
        #endregion

        #region
        List<Municipality> Convert(DataTable dt)
        {
            List<Municipality> lst = new List<Municipality>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Municipality();
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.MunicipalityName = t.Field<string>("MunicipalityName");
                obj.Address = t.Field<string>("Address");
                obj.DistrictID = t.Field<int?>("DistrictID");
                obj.Email = t.Field<string>("Email");
                obj.Mobile = t.Field<string>("Mobile");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                obj.WebSite = t.Table.Columns.Contains("Website") ? t.Field<string>("Website"):null;
                obj.Fax = t.Table.Columns.Contains("Fax") ? t.Field<string>("Fax") :null;
                obj.TelePhone = t.Table.Columns.Contains("TelephoneNo") ? t.Field<string>("TelephoneNo") :null;
                obj.AuthorityId = t.Field<int?>("AuthorityId");
                obj.Type = t.Field<string>("Type");

                return obj;
            }).ToList();
            return lst;
        }
        #endregion
        #region Include navingation properties
        #region Include district
        public Municipality IncludeDistrict(Municipality municipality)
        {
            DistrictBLL dstBll = new DistrictBLL();
            List<District> dst = dstBll.GetDistrictRange(municipality.DistrictID ?? 0);
            municipality.Disctrict = (dst.Count > 0 ? dst[0] : null);
            return municipality;
        }
        public List<Municipality> IncludeDistrict(List<Municipality> municipalities)
        {
            DistrictBLL dstBll = new DistrictBLL();
            List<District> dstLst = dstBll.GetDistrictRange(municipalities.Select(t=> { return t.DistrictID ?? 0; }).ToArray());
            foreach (var mn in municipalities)
            {
                var dst = dstLst.FirstOrDefault(t=>t.DistrictID==mn.DistrictID);
                mn.Disctrict = dst;
            }
            return municipalities;
        }
        #endregion
        #endregion
        public List<Municipality> GetAllMunicipalities()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_AllMunicipalities";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Convert(dt);
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        
    }
}

