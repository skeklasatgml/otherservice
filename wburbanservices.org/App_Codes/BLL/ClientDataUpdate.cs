﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Valuation_Board.Models.WebApi;

namespace Valuation_Board.App_Codes.BLL
{
    public class ClientDataUpdate
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public ClientDataUpdate()
        {
            cn = new SqlConnection(conString);
        }
        public void ClientCurValDataUpdate(List<CurValModelApi> dataLst)
        {
            List<CurValModelApi> lst = new List<CurValModelApi>();

            foreach (CurValModelApi row in dataLst)
            {
                if (row.v_ward_no.ToString().Trim().Length != 0 && row.v_street_code.ToString().Trim().Length != 0 
                    && row.v_holding_no.Trim().Length != 0 && row.v_name.Trim().Length != 0)
                {
                    SqlCommand cmd = new SqlCommand("Insert_CurVal", cn);
                    cmd.Parameters.AddWithValue("@MunicipalityID", row.MunicipalityID);
                    cmd.Parameters.AddWithValue("@v_year", row.v_year);
                    cmd.Parameters.AddWithValue("@v_id_no", row.v_id_no);
                    cmd.Parameters.AddWithValue("@v_srl", row.v_srl);
                    cmd.Parameters.AddWithValue("@v_ward_no", row.v_ward_no);
                    cmd.Parameters.AddWithValue("@v_street_code", row.v_street_code);
                    cmd.Parameters.AddWithValue("@v_holding_no", row.v_holding_no);
                    cmd.Parameters.AddWithValue("@v_name", row.v_name);
                    cmd.Parameters.AddWithValue("@v_address", row.v_address);
                    cmd.Parameters.AddWithValue("@v_holding_type", row.v_holding_type);
                    cmd.Parameters.AddWithValue("@v_hold_type", row.v_hold_type);
                    cmd.Parameters.AddWithValue("@v_hold_area", row.v_hold_area);
                    cmd.Parameters.AddWithValue("@v_zone_code", row.v_zone_code);
                    cmd.Parameters.AddWithValue("@v_use_code", row.v_use_code);
                    cmd.Parameters.AddWithValue("@v_const_code", row.v_const_code);
                    cmd.Parameters.AddWithValue("@v_tot_wt", row.v_tot_wt);
                    cmd.Parameters.AddWithValue("@v_age", row.v_age);
                    cmd.Parameters.AddWithValue("@v_land_area_kt", row.v_land_area_kt);
                    cmd.Parameters.AddWithValue("@v_land_area_ch", row.v_land_area_ch);
                    cmd.Parameters.AddWithValue("@v_land_area_sft", row.v_land_area_sft);
                    cmd.Parameters.AddWithValue("@v_covered_area", row.v_covered_area);
                    cmd.Parameters.AddWithValue("@v_plinth_area", row.v_plinth_area);
                    cmd.Parameters.AddWithValue("@v_land_cost", row.v_land_cost);
                    cmd.Parameters.AddWithValue("@v_arv", row.v_arv);
                    cmd.Parameters.AddWithValue("@v_disc_per", row.v_disc_per);
                    cmd.Parameters.AddWithValue("@v_disc", row.v_disc);
                    cmd.Parameters.AddWithValue("@v_rev_val", row.v_rev_val);
                    cmd.Parameters.AddWithValue("@v_ptax_per", row.v_ptax_per);
                    cmd.Parameters.AddWithValue("@v_yr_proptax", row.v_yr_proptax);
                    cmd.Parameters.AddWithValue("@v_qtr_proptax", row.v_qtr_proptax);
                    cmd.Parameters.AddWithValue("@v_prv_qtr_proptax", row.v_prv_qtr_proptax);
                    cmd.Parameters.AddWithValue("@v_srchg_tag", row.v_srchg_tag);
                    cmd.Parameters.AddWithValue("@v_qtr_srchg", row.v_qtr_srchg);
                    cmd.Parameters.AddWithValue("@v_edu_tag", row.v_edu_tag);
                    cmd.Parameters.AddWithValue("@v_qtr_edu_cess", row.v_qtr_edu_cess);
                    cmd.Parameters.AddWithValue("@v_mouza", row.v_mouza);
                    cmd.Parameters.AddWithValue("@v_khatian", row.v_khatian);
                    cmd.Parameters.AddWithValue("@v_dag", row.v_dag);
                    cmd.Parameters.AddWithValue("@v_deed_no", row.v_deed_no);
                    cmd.Parameters.AddWithValue("@v_building_dtl", row.v_building_dtl);
                    cmd.Parameters.AddWithValue("@v_water_tag", row.v_water_tag);
                    cmd.Parameters.AddWithValue("@v_ferrule_size", row.v_ferrule_size);
                    cmd.Parameters.AddWithValue("@v_val_tag", row.v_val_tag);
                    cmd.Parameters.AddWithValue("@v_val_year", row.v_val_year);
                    cmd.Parameters.AddWithValue("@v_val_qtr", row.v_val_qtr);
                    cmd.Parameters.AddWithValue("@v_val_srl", row.v_val_srl);
                    cmd.Parameters.AddWithValue("@v_val_sub_srl", row.v_val_sub_srl);
                    cmd.Parameters.AddWithValue("@v_val_from_to", row.v_val_from_to);
                    cmd.Parameters.AddWithValue("@v_from_ward_no", row.v_from_ward_no);
                    cmd.Parameters.AddWithValue("@v_from_street_code", row.v_from_street_code);
                    cmd.Parameters.AddWithValue("@v_from_holding_no", row.v_from_holding_no);
                    cmd.Parameters.AddWithValue("@v_old_ward_no", row.v_old_ward_no);
                    cmd.Parameters.AddWithValue("@v_old_street_code", row.v_old_street_code);
                    cmd.Parameters.AddWithValue("@v_old_holding_no", row.v_old_holding_no);
                    cmd.Parameters.AddWithValue("@v_factor", row.v_factor);
                    cmd.Parameters.AddWithValue("@v_mc_id", row.v_mc_id);
                    cmd.Parameters.AddWithValue("@v_user_id", row.v_user_id);
                    cmd.Parameters.AddWithValue("@v_entry_time", row.v_entry_time);


                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    try
                    {
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException se)
                    {
                        throw new Exception("Database Error...!" + se.Message);
                    }
                    finally
                    {
                        cn.Close();
                    }
                }
            }
        }
    }
}