﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.BLL
{
    public class WardBLL
    {
        string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SqlConnection cn;
        public WardBLL()
        {
            cn = new SqlConnection(constring);
        }
        public List<Ward> GetWards(int municipalityID,string wardName)
        {
            SqlCommand cmd = new SqlCommand("Get_WardByNameMuncpltyID", cn);
            cmd.Parameters.AddWithValue("@WardName", wardName);
            cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Filter by Ward ID Range
        /// </summary>
        /// <param name="wardIds">C# int array</param>
        /// <returns>List<Ward></returns>
        public List<Ward> GetWards(List<int> wardIds,int municipalityID)
        {
            if (wardIds == null || wardIds.Count == 0)
            {
                return new List<Ward>();
            }
            try
            {
                string commaSeparatedNums = wardIds.Select(b => b.ToString()).Aggregate<string>((items, item) => items + "," + item);

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "Get_WardsByIdRange";
                cmd.Parameters.AddWithValue("@IdRange", commaSeparatedNums);
                cmd.Parameters.AddWithValue("@Delimiter", ",");
                cmd.Parameters.AddWithValue("@MunicipalityID", municipalityID);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    sda.Fill(dt);
                    return Convert(dt);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            catch (SqlException)
            {
                throw new Exception("Database Error");
            }
        }
        /// <summary>
        /// Return List of wards mapped with user. If no mapped wards found returns all wards
        /// </summary>
        /// <param name="mnUserID"></param>
        /// <param name="municipalityID"></param>
        /// <returns></returns>
        public List<Ward> GetMappedWardsOrAllByUserID(long mnUserID, int municipalityID)
        {
            
            try
            {
               

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "Get_MappedWardsOrAllOnUserID";
               
                cmd.Parameters.AddWithValue("@MunicipalityUserID", mnUserID);
                cmd.Parameters.AddWithValue("@municipalityID", municipalityID);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    sda.Fill(dt);
                    return Convert(dt);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Database Error",ex);
            }
        }

        /// <summary>
        /// Add mapp user and ward to map table
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="WardID"></param>
        /// <param name="MunicipalityID"></param>
        public void AddWardToUser(long UserID, int WardID, int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Insert_UserWardMapping", cn);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                //return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Remove mapping user and ward from mapping table
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="WardID"></param>
        /// <param name="MunicipalityID"></param>
        public void RemoveWardToUser(long UserID, int WardID, int MunicipalityID)
        {
            SqlCommand cmd = new SqlCommand("Delete_UserWardMapping", cn);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@WardID", WardID);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                //return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// returns only mapped wards with user
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="MunicipalityID"></param>
        /// <returns></returns>
        public List<Ward> GetMappedWardsByUserID(long UserID, int MunicipalityID)
        {

            SqlCommand cmd = new SqlCommand("Get_MappedWardsByMnUserID", cn);
            cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        #region Converter
        List<Ward> Convert(DataTable dt)
        {
            List<Ward> lst = new List<Ward>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Ward();
                obj.WardName = t.Field<string>("WardName");
                obj.WardID = t.Field<int?>("WardID");
                obj.MunicipalityID = t.Field<int?>("MunicipalityID");
                obj.InsertedBy = t.Field<int?>("InsertedBy");
                obj.InsertedOn = t.Field<DateTime?>("InsertedOn");
                obj.UpdatedBy = t.Field<int?>("UpdatedBy");
                obj.UpdatedOn = t.Field<DateTime?>("UpdatedOn");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion

        #region Include Navigation
        public List<Ward> IncludeMunicipality(List<Ward> wards)
        {
            throw new NotImplementedException();
        }
        public Ward IncludeMunicipality(Ward ward)
        {
            throw new NotImplementedException();
        }
        
       
        #endregion
    }
}