﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.Events;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.App_Codes
{
    public static class SessionContext
    {
        public static string AppTag
        {
            get
            {
                return ConfigurationManager.AppSettings["AppTag"].ToLower();
            }
        }
        public static bool IsAuthenticated
        {
            get
            {
                return CurrentUser != null;
            }
        }
        public static object CurrentUser
        {
            get
            {
                var user = GetCurrentUser();
                return user;
            }
            private set
            {
                HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentUser_Key] = value;
            }
        }
        public static Module CurrentModue
        {
            get
            {
                return HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentModuleKey] as Module;
            }
            private set
            {
                HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentModuleKey] = value;
            }
        }
        public static void SetModule(Module module)
        {
            CurrentModue = module;
        }
        public static void Login(IInternalUser internalUser)
        {
            CurrentUser = internalUser;
            HttpCookie _CookieUserType = new HttpCookie("_crdTyp");
            _CookieUserType.Value = ((int)internalUser.UserType).ToString();
            HttpContext.Current.Response.Cookies.Add(_CookieUserType);
        }
        public static void Login(CitizenUser citizenUser)
        {
            IEncryptDecrypt chiperAlgorithm;
            chiperAlgorithm = new AES_Algorithm(ConfigurationManager.AppSettings["ChiperKey"] as string);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            //inject authorization cookiee for further credential, Secured filter upgraded
            HttpCookie _CookieCridential = new HttpCookie("_crd");
            _CookieCridential.Value = chiperAlgorithm.Encrypt(jss.Serialize(citizenUser));
            HttpContext.Current.Response.Cookies.Add(_CookieCridential);

            HttpCookie _CookieUserType = new HttpCookie("_crdTyp");
            _CookieUserType.Value =((int) citizenUser.UserType).ToString();
            HttpContext.Current.Response.Cookies.Add(_CookieUserType);
        }
        public static void Logout()
        {
            if (CurrentUser!=null && ((IUserType)CurrentUser).UserType == UserType.CitizenUser)
            {
                RemoveCustomCookies();
            }
            else
            {
                System.Web.HttpContext.Current.Session.Clear();
            }
        }
        public static void Logout(string sessionID)
        {
            if (((IInternalUser)CurrentUser).UserType == UserType.CitizenUser)
            {
                RemoveCustomCookies();
            }
            else
            {
                var session = GlobalContext_Operator.GetSession(sessionID);
                if (session != null)
                {
                    session.Clear();
                }
            }
        }
        static object  GetCurrentUser()
        {
            var http = HttpContext.Current;
            UserType? userType = null;
            try
            {
                userType=(UserType?)Convert.ToInt32(http.Request.Cookies["_crdTyp"].Value);
            }catch
            { }

            if(userType!=null && userType.Value == UserType.CitizenUser)
            {
                IEncryptDecrypt chiperAlgorithm;
                chiperAlgorithm = new AES_Algorithm(ConfigurationManager.AppSettings["ChiperKey"] as string);
                string pramJSON = chiperAlgorithm.Decrypt(http.Request.Cookies["_crd"].Value);
                JavaScriptSerializer js = new JavaScriptSerializer();
                CitizenUser user = js.Deserialize<CitizenUser>(pramJSON);
                return user;//from cookee
            }
            else if (userType != null && (userType.Value == UserType.MunicipalityUser || userType.Value==UserType.AdministrativeUser || userType.Value == UserType.OtherCitizenUser))
            {
                return HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentUser_Key];
            }
            return null;
        }

        public static void RemoveCustomCookies()
        {
            var httpCtx = HttpContext.Current;
            if(httpCtx.Request.Cookies["_crd"]!=null)
                httpCtx.Request.Cookies["_crd"].Expires = DateTime.Now.AddDays(-2);
            if (httpCtx.Request.Cookies["_crdTyp"] != null)
                httpCtx.Request.Cookies["_crdTyp"].Expires = DateTime.Now.AddDays(-2);
        }
    }
}

