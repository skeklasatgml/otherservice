﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.Events;

namespace Valuation_Board.App_Codes
{
    public static class GlobalContext_Operator
    {
         static GlobalContext_Operator()
        {
        }
        public static void AddSession(System.Web.SessionState.HttpSessionState session)
        {
           GlobalContext.LoggedInUsers[session.SessionID] = session;
        }
        public static void RemoveSession(System.Web.SessionState.HttpSessionState session)
        {
            GlobalContext.LoggedInUsers.Remove(session.SessionID);
        }
        public static void RemoveSession(string sessionID)
        {
            GlobalContext.LoggedInUsers.Remove(sessionID);
        }
        public static System.Web.SessionState.HttpSessionState GetSession(string sessionID)
        {
            System.Web.SessionState.HttpSessionState ss = GlobalContext.LoggedInUsers[sessionID] as System.Web.SessionState.HttpSessionState;
            return ss;
        }
    }
}