﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.Extension
{
    public static class OtherExtensions
    {
        #region object
        public static string ToJson(this object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
        #endregion
        public static int ToInt32(this object data)
        {
            return Convert.ToInt32(data);
        }
        public static long ToInt64(this object data)
        {
            return Convert.ToInt64(data);
        }
        public static short ToInt16(this object data)
        {
            return Convert.ToInt16(data);
        }
        public static decimal ToDecimal(this object data)
        {
            return Convert.ToDecimal(data);
        }

        //
        public static int? ToInt32Null(this object data)
        {
            if (data == null) return null;
            return Convert.ToInt32(data);
        }
        public static long? ToInt64Null(this object data)
        {
            if (data == null) return null;
            return Convert.ToInt64(data);
        }
        public static short? ToInt16Null(this object data)
        {
            if (data == null) return null;
            return Convert.ToInt16(data);
        }
        public static decimal? ToDecimalNull(this object data)
        {
            if (data == null) return null;
            return Convert.ToDecimal(data);
        }

        public static bool? ToBoolNull(this object data)
        {
            if (data == null) return null;
            return Convert.ToBoolean(data);
        }
        public static bool ToBool(this object data)
        {

            return Convert.ToBoolean(data);
        }
        public static bool IsNull(this object data)
        {
            if (data == null) return true;
            return false;
        }
        public static bool IsNotNull(this object data)
        {
            return !data.IsNull();
        }
        public static T IsNullThen<T>(this object data, T replace)
        {
            if (data == null) return replace;
            return (T)data;
        }
        public static object IsNullThen(this object data, object replace)
        {
            if (data == null) return replace;
            return data;
        }

        public static object ReplaceDbNull(this object val)
        {
            if ((val == null))
            {
                return DBNull.Value;
            }
            else if (val.GetType() == typeof(string))
            {
                if (string.IsNullOrEmpty(val as string))
                {
                    return DBNull.Value;
                }
            }

            return val;

        }
        public static string ReplaceNull(this Object val)
        {
            if ((val == null))
            {
                return null;
            }
            else if (val.GetType() == typeof(string))
            {
                if (string.IsNullOrEmpty(val as string))
                {
                    return null;
                }
            }

            return val as string;
        }
        public static bool IsValidFinYear(this string finYear)
        {
            if (string.IsNullOrEmpty(finYear))
                return false;

            string[] arr = finYear.Split('-');
            if (arr.Length != 2)
                return false;

            var leftYear = arr[0].ToInt32();
            var rightYear = arr[1].ToInt32();
            if (leftYear + 1 != rightYear)
                return false;

            return true;
        }
        /// <summary>
        /// if null return DBNull.Value or Return String("yyyy'-'MM'-'dd HH:mm:ss")
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static object ToDbDate(this DateTime? date)
        {
            if (date == null)
                return DBNull.Value;
            else
                return date.Value.ToString("yyyy'-'MM'-'dd HH:mm:ss");
        }
        public static object ToDbDate(this DateTime? date, object nullReplace)
        {
            if (date == null)
                return nullReplace;
            else
                return date.Value.ToString("yyyy'-'MM'-'dd HH:mm:ss");
        }
        public static string ToDbDate(this DateTime date)
        {
            return date.ToString("yyyy'-'MM'-'dd HH:mm:ss");
        }
        public static string ToStrDate(this DateTime? date, string format = "dd'/'MM'/'yyyy")
        {
            if (date == null)
                return "";
            else
                return date.Value.ToString(format);
        }
        public static string ToStrDate(this DateTime date, string format = "dd'/'MM'/'yyyy")
        {
            return date.ToString(format);
        }
        public static DateTime GetFinYearFirstDate(this FinYearQtr FinYear)
        {
            var finYear = FinYear.GetFinYear();
            int leftYear = Convert.ToInt32(finYear.Split('-')[0]);
            return DateTime.ParseExact(string.Format("{0}-{1}-{2}", leftYear, "04", "01"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static DateTime GetFinYearLastDate(this FinYearQtr FinYear)
        {
            var finYear = FinYear.GetFinYear();
            int rightYear = Convert.ToInt32(finYear.Split('-')[1]);
            return DateTime.ParseExact(string.Format("{0}-{1}-{2}", rightYear, "03", "31"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

        }
        public static Uri getHost()
        {
            return HttpContext.Current.Request.Url;
        }
        public static string BindHost(this string relativePath)
        {
            Uri result = null;
            if (Uri.TryCreate(getHost(), relativePath, out result))
            {
                return result.ToString();
            }
            return relativePath;
        }
        public static string ReplaceCurrHost(this string oldUrl)
        {
            var builder1 = new UriBuilder(getHost());
            var builder = new UriBuilder(oldUrl);
            builder.Host = builder1.Host;
            builder.Port = builder1.Port;
            return builder.Uri.ToString();
        }
    }

}