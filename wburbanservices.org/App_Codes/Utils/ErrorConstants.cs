﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes.Utils
{
    public static class ErrorConstants
    {
        public static Dictionary<string, string> Errors { get; private set; }
        static ErrorConstants()
        {
            if (Errors == null)
            {
                Errors = new Dictionary<string, string>();
            }
            //password is incorrect
            Errors["AR0001"] = "Authentication required.";
            Errors["TE0002"] = "Token expierd.";
            Errors["AV0003"] = "Profile verification required.";
            Errors["PE0004"] = "Profile already exist and verified.";
            Errors["IO0005"] = "Invalid OTP.";
            Errors["PE0006"] = "Profile already exist but not verified.";
            Errors["PIC0007"] = "Password is incorrect.";
            Errors["PC0008"] = "Profile does not exist.";
            Errors["ANF0009"] = "Assessee not found.";
            Errors["ANF0010"] = "Assessee already mapped. If you want to map this assessee please contact nearest Municipality.";
            Errors["OPI0011"] = "Old Password is incorrect.";
            Errors["ANM0012"] = "Assessee is not mapped to current profile.";
            Errors["PAE0013"] = "This mobile no belongs to another profile.";//"Profile already exist.";
            Errors["UE500"] = "Unknown error.";

            ///  Narayangang Municipality            ///  

            //Errors["NG_PC0008"] = "Profile does not exist.";
            //Errors["NG_PIC0007"] = "Password is incorrect.";
        }
    }
}