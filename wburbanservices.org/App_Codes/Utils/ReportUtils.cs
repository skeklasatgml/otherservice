﻿using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.App_Codes.Utils
{
    public class ReportUtils
    {
        private IEncryptDecrypt chiperAlgorithm;
        public ReportUtils()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
        }
        public string GetReportViewerURL(int ReportID, Dictionary<string, object> Params)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            var _params = js.Serialize(Params);
            _params = chiperAlgorithm.Encrypt(_params);
           _params= HttpUtility.UrlEncode(_params);
           return string.Format("~/Reporting/Viewer/ReportViewer.aspx?ReportCode={0}&Params={1}",ReportID,_params);
        }
    }
}