﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using Valuation_Board.Models.Application;

namespace Valuation_Board.App_Codes.Utils
{
    public static class CommonUtils
    {
        public static string GetFinYear(DateTime date)
        {
            string finYear = "";
            int curMonth = date.Month;
            if (curMonth > 3 && curMonth <= 12)
            {
                finYear = string.Format("{0}-{1}", date.Year, date.Year + 1);
            }
            else if (curMonth >= 1 && curMonth <= 3)
            {
                finYear = string.Format("{0}-{1}", date.Year - 1, date.Year);
            }
            return finYear;
        }
        public static string GetFinYear(DateTime? date)
        {
            if (date == null)
                return null;
            else
                return GetFinYear(date.Value);
        }
        public static int GetYearDiffrentFromTwoDates(DateTime bigDate, DateTime smallDate)
        {
            string bigFinYear = GetFinYear(bigDate);
            string smallFinYear = GetFinYear(smallDate);
            var bigFinYearQtr = new FinYearQtr(bigFinYear);
            var SmallYearQtr = new FinYearQtr(smallFinYear);
            return (bigFinYearQtr.StartYear - SmallYearQtr.StartYear) + 1;
        }
        public static int GetMonthDiffFromTwoDates(DateTime bigDate, DateTime smallDate)
        {
            return (bigDate.Year * 12 + bigDate.Month) - (smallDate.Year * 12 + smallDate.Month);
        }
        public static double GetDaysDiffFromTwoDates(DateTime bigDate, DateTime smallDate)
        {
            return (bigDate - smallDate).TotalDays;
        }
        //public static DateTime GetFinYearFirstDate(int year)
        //{
        //    return DateTime.ParseExact(string.Format("{0}-{1}-{2}", year, "04", "01"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        //}
        public static DateTime GetFinYearFirstDate(DateTime curDate)
        {
            return GetFinYearFirstDate(GetFinYear(curDate));
        }

        public static DateTime GetFinYearFirstDate(string finYear)
        {
            int leftYear = Convert.ToInt32(finYear.Split('-')[0]);
            return DateTime.ParseExact(string.Format("{0}-{1}-{2}", leftYear, "04", "01"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static DateTime GetFinYearLastDate(string finYear)
        {
            int rightYear = Convert.ToInt32(finYear.Split('-')[1]);
            return DateTime.ParseExact(string.Format("{0}-{1}-{2}", rightYear, "03", "31"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

        }
        public static int GetStartedYear(string finYear)
        {
            return Convert.ToInt32(finYear.Split('-')[0]);
        }
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        public static string GetUniqueFileName(string extension)
        {
            return string.Format(@"{0}{1}", DateTime.Now.Ticks, (string.IsNullOrEmpty(extension) ? "" : string.Format(".{0}", extension.Replace(".", ""))));
        }
        public static string SaveFile(this HttpPostedFileBase file)
        {
            string relativePath = ConfigurationManager.AppSettings["FileUploadDir"];
            if (string.IsNullOrEmpty(relativePath))
            {
                throw new Exception("File Upload location Not Configured, Key=>FileUploadDir");
            }
            var actualLocation = HttpContext.Current.Server.MapPath("~" + relativePath);
            if (!Directory.Exists(actualLocation))
            {
                Directory.CreateDirectory(actualLocation);
            }
            var fileName = Path.GetFileName(file.FileName);
            string documentName = fileName;
            string extension = new FileInfo(documentName).Extension;
            string wellKnownFileName = GetUniqueFileName(extension);
            var path = Path.Combine(actualLocation, wellKnownFileName);
            file.SaveAs(path);
            return Path.Combine(relativePath, wellKnownFileName);
        }
        public static string GetExtension(this string documentName)
        {
            if (documentName == null)
            {
                return null;
            }
            return new FileInfo(documentName).Extension;
        }
        public static string RemoveBase64FileHeader(this string base64value)
        {
            // sample of value => "data:image/png;base64,iVBORw..............
            // take and return the value part  => iVBORw..............

            //var response = Regex.Replace(result, @"^data:[a-zA-Z]+\/[a-zA-Z]+;base64,", string.Empty); //not use but working
            var resp = base64value.Substring(base64value.IndexOf(',') + 1);
            return resp;
        }
        public static string SaveString64File(this Base64File file)
        {
            string extension = file.Name.GetExtension();
            var base64String = file.Result.RemoveBase64FileHeader();
            string relativePath = ConfigurationManager.AppSettings["FileUploadDir"];

            if (string.IsNullOrEmpty(relativePath))
            {
                throw new Exception("File Upload location Not Configured, Key=>FileUploadDir");
            }
            var RelativeDir = relativePath;
            var localAbsDir = HttpContext.Current.Server.MapPath("~" + relativePath);
            if (!Directory.Exists(localAbsDir))
            {
                Directory.CreateDirectory(localAbsDir);
            }
            DirectoryInfo absDir = new DirectoryInfo(localAbsDir);
            var LocalBaseDir = absDir.Parent.FullName;
            byte[] imageBytes = Convert.FromBase64String(base64String);
            string wellKnownFileName = GetUniqueFileName(extension);
            var NativeFilename = wellKnownFileName;
            var FileName = file.Name;
            var path = Path.Combine(localAbsDir, wellKnownFileName);
            File.WriteAllBytes(path, imageBytes);
            return Path.Combine(relativePath, wellKnownFileName);
        }
        public static bool DeleteFile(string Dir, string fileName)
        {
            try
            {
                var filePath = HttpContext.Current.Server.MapPath("~" + Path.Combine(Dir, fileName));
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);

                }
                return true;
            }
            catch (FileNotFoundException)
            {
                throw new Exception(string.Format("File Deletion Aborted. File not found -'{0}'", fileName));
            }
        }
    }
}