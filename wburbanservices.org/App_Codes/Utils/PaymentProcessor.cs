﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.App_Codes.Utils
{
    public class PaymentProcessor
    {
        List<IOtherAdjustment> lstAdjustmnets;
        public PaymentProcessor()
        {
            lstAdjustmnets = new List<IOtherAdjustment>();
        }

        public void AddOtherAdjustment(IOtherAdjustment adjustment)
        {
            lstAdjustmnets.Add(adjustment);
        }
        public PTaxCollectionGiantObject SearchOutStanding(int MunicipalityID, int WardID, int LocationID, long AssesseeID, DateTime paymentDate, List<TaxPaymentCheckParameter> taxPaymentCheckParameters, DateTime? noticeServedOn, int? effectQtrFrom, long userID)
        {
            PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
            MunicipalityConfigurationBll cngf = new MunicipalityConfigurationBll();
            if (cngf.GetConfiguration(MunicipalityID).PaymentConfiguration.IsAdvanceAutoAdjustableEnabled == true && bll.GetUnadjustedAdvanceAmount(MunicipalityID, AssesseeID) > 0)
            {
                bll.Adjust_UndjustedAdvance(MunicipalityID, AssesseeID, userID);
            }
            return Calculate(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, taxPaymentCheckParameters, noticeServedOn, effectQtrFrom, userID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MunicipalityID"></param>
        /// <param name="WardID"></param>
        /// <param name="LocationID"></param>
        /// <param name="AssesseeID"></param>
        /// <param name="paymentDate">Collection Date</param>
        /// <param name="taxPaymentCheckParameters"></param>
        /// <returns></returns>
        public PTaxCollectionGiantObject Calculate(int MunicipalityID, int WardID, int LocationID, long AssesseeID, DateTime paymentDate, List<TaxPaymentCheckParameter> taxPaymentCheckParameters, DateTime? noticeServedOn, int? effectQtrFrom, long userID)
        {
            OtherAdjustmentBLL otherAdjustmentBLL = new OtherAdjustmentBLL();
            PayHeadBll phBll = new PayHeadBll();
            PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
            
            List<PropertyTaxOutstading> lstPropertyTaxOutstading = bll.GetPropertyTaxPaymentDetails(MunicipalityID, WardID, LocationID, AssesseeID);

            #region code injected! validation on notice served date
            Action<int> _validateQtr = (_qtr) =>
            {
                if (!new int[] { 1, 2, 3, 4 }.Contains(_qtr))
                {
                    throw new InvalidOperationException();
                }
            };
            Action<DateTime?, int?> __validateNoticeServedDt = (_noticeServedOn, _effectiveQtrFrom) =>
            {

                if (_noticeServedOn == null && _effectiveQtrFrom != null)
                {
                    _validateQtr(_effectiveQtrFrom ?? 0);
                    throw new InvalidOperationException("Required Notice Served Date");
                }
                else if (_noticeServedOn != null && _effectiveQtrFrom == null)
                {
                    _validateQtr(_effectiveQtrFrom ?? 0);
                    throw new InvalidOperationException("Required Effective Quarter for Notice Served Date");
                }
            };
            #endregion
            __validateNoticeServedDt(noticeServedOn, effectQtrFrom);
            //load check parameters to outstanding list
            //configure check parameter by default
            if (taxPaymentCheckParameters.Where(t => t.IsChecked == true).Count() == 0)
            {
                lstPropertyTaxOutstading.ForEach(t =>
                {
                    t.taxPaymentCheckParameter = new TaxPaymentCheckParameter()
                    {
                        FinYear = t.FinYear,
                        IsChecked = true,
                        QtrNo = t.QtrNo,
                        Rank = t.Rank
                    };
                });
            }
            else
            {
                lstPropertyTaxOutstading.ForEach(t =>
                {
                    t.taxPaymentCheckParameter = new TaxPaymentCheckParameter()
                    {
                        FinYear = t.FinYear,
                        IsChecked = false,
                        QtrNo = t.QtrNo,
                        Rank = t.Rank
                    };
                });
            }

            //assign common properties to List<propertyTaxPaymentDetail>
            List<PropertyTaxPaymentDetail> lstTaxPaymentDetails = lstPropertyTaxOutstading.Select(propertyTaxOutstading =>
            {
                PropertyTaxPaymentDetail propertyTaxPaymentDetail = new PropertyTaxPaymentDetail();

                propertyTaxPaymentDetail.FinYear = propertyTaxOutstading.FinYear;
                propertyTaxPaymentDetail.PayArrrearBill = propertyTaxOutstading.DemandType;
                propertyTaxPaymentDetail.DemandTypeID = propertyTaxOutstading.DemandTypeID;
                propertyTaxPaymentDetail.QtrNo = propertyTaxOutstading.QtrNo;
                propertyTaxPaymentDetail.TaxValue = propertyTaxOutstading.OSPropertyTaxAmt;
                propertyTaxPaymentDetail.SurchargeValue = propertyTaxOutstading.OSSurchargeAmt;
                propertyTaxPaymentDetail.PaidTaxAmt = 0;
                propertyTaxPaymentDetail.PaidSurchargeAmt = 0;
                propertyTaxPaymentDetail.CalculatedValue = 0;
                propertyTaxPaymentDetail.PaidCalculatedAmt = 0;
                propertyTaxPaymentDetail.taxPaymentCheckParameter = new TaxPaymentCheckParameter()
                {
                    FinYear = propertyTaxPaymentDetail.FinYear,
                    IsChecked = propertyTaxOutstading.taxPaymentCheckParameter.IsChecked,
                    QtrNo = propertyTaxPaymentDetail.QtrNo ?? 0,
                    Rank = propertyTaxOutstading.Rank
                };


                return propertyTaxPaymentDetail;
            }).ToList();

            if (taxPaymentCheckParameters.Where(t => t.IsChecked == true).Count() == 0)
            {
                lstTaxPaymentDetails.Where(ss => ss.taxPaymentCheckParameter.IsChecked).ToList().ForEach(gg =>
                {
                    taxPaymentCheckParameters.Add(gg.taxPaymentCheckParameter);
                });
            }

            taxPaymentCheckParameters.Where(ttt => ttt.IsChecked).OrderBy(s => s.Rank).ToList().ForEach(t =>
              {
                  if (t.IsChecked)
                  {
                      PropertyTaxPaymentDetail propertyTaxPaymentDetail = lstTaxPaymentDetails.FirstOrDefault(l => (l.FinYear == t.FinYear && l.QtrNo == t.QtrNo));
                      PropertyTaxOutstading propertyTaxOutstading = lstPropertyTaxOutstading.FirstOrDefault(l => (l.FinYear == t.FinYear && l.QtrNo == t.QtrNo));
                      propertyTaxPaymentDetail.taxPaymentCheckParameter.IsChecked = true;
                      propertyTaxOutstading.taxPaymentCheckParameter.IsChecked = true;
                      if (propertyTaxOutstading != null && propertyTaxPaymentDetail != null)
                      {
                          DateTime noticeServedDate = (CommonUtils.GetFinYearFirstDate(CommonUtils.GetFinYear(paymentDate)));//01-04-curYear
                          if (propertyTaxOutstading.QtrNo >= (effectQtrFrom ?? 0) && effectQtrFrom != null && noticeServedOn != null)
                          {
                              //reset noticeServedDate =noticeServedOn
                              noticeServedDate = noticeServedOn.Value;
                          }

                          //get payheads
                          List<PayHead> phs = phBll.Get_PayHeadDetailLevel(propertyTaxOutstading.MunicipalityID, propertyTaxOutstading.QtrNo, propertyTaxOutstading.FinYear, paymentDate, noticeServedDate,AssesseeID);
                          //fetch interest
                          List<PayHead> phIntrsts = phs.Where(p => p.PayHeadBehave == "A").ToList();
                          //fetch rebates 
                          List<PayHead> phRebates = phs.Where(p => p.PayHeadBehave == "D").ToList();

                          #region assign value common properties on PropertyTaxPaymentDetail object
                          propertyTaxPaymentDetail.PaidTaxAmt = propertyTaxOutstading.OSPropertyTaxAmt;
                          propertyTaxPaymentDetail.PaidSurchargeAmt = propertyTaxOutstading.OSSurchargeAmt;

                          Func<double, string, double> __roundOffPeriod = (_periodValue, _direction) =>
                          {
                              //_periodValue = Math.Round(_periodValue, 2, MidpointRounding.AwayFromZero);
                              if (_direction.Equals("UP", StringComparison.OrdinalIgnoreCase))
                              {
                                  return (double)Math.Ceiling(_periodValue);

                              }
                              else if (_direction.Equals("DW", StringComparison.OrdinalIgnoreCase))
                              {
                                  return (double)Math.Floor(_periodValue);
                              }
                              else
                              {
                                  return (double)Math.Ceiling(_periodValue);
                                  // throw new InvalidOperationException(string.Format("Round off direction '{0}' not implemented",_direction));
                              }
                          };

                          //_periodDiff may be month or day or year
                          Func<DateTime, DateTime, decimal, decimal, PayHead, decimal> __calculateInterest = (_fromDate, _toDate, _amount, _payRatePerYear, _payHead) =>
                            {

                                decimal intVal = 0;

                                switch (_payHead.InterestPeriodMethod.ToUpper())
                                {
                                    case "DD":
                                        {
                                            double _periodDiff = CommonUtils.GetDaysDiffFromTwoDates(_toDate, _fromDate);
                                            decimal payRatePerday = _payRatePerYear / 365;
                                            intVal = (_amount * payRatePerday * (decimal)_periodDiff / 100);
                                            break;
                                        }
                                    case "YY":
                                        {
                                            double _period = CommonUtils.GetYearDiffrentFromTwoDates(_toDate, _fromDate) ;
                                            intVal = (_amount * _payRatePerYear * (decimal)_period / 100);
                                            break;
                                        }
                                    case "MM":
                                    default:
                                        {
                                            decimal payRatePerMonth = _payRatePerYear / 12;
                                            double _period = CommonUtils.GetMonthDiffFromTwoDates(_toDate, _fromDate.AddDays(-1));
                                            intVal = (_amount * payRatePerMonth * (decimal)_period / 100);
                                            break;
                                        }
                                }


                                return Math.Round(intVal, 2, MidpointRounding.AwayFromZero);
                            };
                          //interest calculate if available
                          phIntrsts.ForEach(phi =>
                            {
                                double dayDiff = CommonUtils.GetDaysDiffFromTwoDates(paymentDate, phi.EffectiveDateForm);
                                decimal paidPropTaxInterest = __calculateInterest(phi.EffectiveDateForm, paymentDate, propertyTaxOutstading.OSPropertyTaxAmt, phi.PayRate ?? 0, phi);// (propertyTaxOutstading.OSPropertyTaxAmt) * payRatePerMonth * effectedMonth / 100;
                                decimal paidSurchargeInterest = __calculateInterest(phi.EffectiveDateForm, paymentDate, propertyTaxOutstading.OSSurchargeAmt, phi.PayRate ?? 0, phi); //(propertyTaxOutstading.OSSurchargeAmt) * payRatePerMonth * effectedMonth / 100;//end 18112017
                                decimal calCulatedValue = __calculateInterest(phi.EffectiveDateForm, paymentDate, (propertyTaxOutstading.OSPropertyTaxAmt + propertyTaxOutstading.OSSurchargeAmt), phi.PayRate ?? 0, phi); // (propertyTaxOutstading.OSPropertyTaxAmt + propertyTaxOutstading.OSSurchargeAmt) * payRatePerMonth * effectedMonth / 100;
                                calCulatedValue = calCulatedValue < 0 ? 0 : calCulatedValue;
                                propertyTaxPaymentDetail.CalculatedValue += Math.Round(calCulatedValue, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PaidSurchrgInterest += Math.Round(paidSurchargeInterest, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PaidPropTaxInterest += Math.Round(paidPropTaxInterest, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PaidCalculatedAmt += Math.Round(calCulatedValue, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PayHeadTypeID = phi.PayHeadID;
                                propertyTaxPaymentDetail.payHead = phi;
                            });

                          //rebate calculate if available. effect only on current demand
                          phRebates.ForEach(phr =>
                            {
                                //int effectedMonth = CommonUtils.GetMonthDiffFromTwoDates(paymentDate, phr.EffectiveDateForm);
                                decimal calCulatedValue = (propertyTaxOutstading.OSPropertyTaxAmt + propertyTaxOutstading.OSSurchargeAmt) * (phr.PayRate ?? 0) / 100;
                                calCulatedValue = calCulatedValue < 0 ? 0 : calCulatedValue;
                                propertyTaxPaymentDetail.CalculatedValue -= Math.Round(calCulatedValue, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PaidCalculatedAmt -= Math.Round(calCulatedValue, 2, MidpointRounding.AwayFromZero);
                                propertyTaxPaymentDetail.PayHeadTypeID = phr.PayHeadID;
                                propertyTaxPaymentDetail.payHead = phr;
                            });
                          #endregion

                          #region make line total, code injected 03112017
                          propertyTaxPaymentDetail.LineTotal = (propertyTaxPaymentDetail.PaidTaxAmt ?? 0) + (propertyTaxPaymentDetail.PaidCalculatedAmt ?? 0) + (propertyTaxPaymentDetail.PaidSurchargeAmt ?? 0);
                          propertyTaxPaymentDetail.LineTotalRoundOff = -1 * (propertyTaxPaymentDetail.LineTotal - Math.Round(propertyTaxPaymentDetail.LineTotal, 0, MidpointRounding.AwayFromZero));//round to 0 scaling
                          #endregion
                      }

                  }
              });

            PropertyTaxPaymentMaster propertyTaxPaymentMaster = Convert(lstTaxPaymentDetails, AssesseeID);
            DateTime _getFistDateOfFinYear = (CommonUtils.GetFinYearFirstDate(paymentDate));
            PTaxCollectionGiantObject GiantObject = new PTaxCollectionGiantObject();
            if (propertyTaxPaymentMaster != null)
            {
                propertyTaxPaymentMaster.PaymentReceiveDate = DateTime.Now;
                propertyTaxPaymentMaster.PaymentCollectionDate = paymentDate;
                propertyTaxPaymentMaster.MunicipalityID = MunicipalityID;
                propertyTaxPaymentMaster.AssesseeID = AssesseeID;
                propertyTaxPaymentMaster.NoticeServedDate = noticeServedOn ?? _getFistDateOfFinYear;
                propertyTaxPaymentMaster.EffectiveQtrFrom = effectQtrFrom ?? 1;

                GiantObject.propertyTaxPaymentMaster = propertyTaxPaymentMaster;
                GiantObject.propertyTaxOutstadings = lstPropertyTaxOutstading;

                return GiantObject;
            }

            return null;
        }

        public decimal MakeAdvancePayment(int MunicipalityID, int WardID, int LocationID, long assesseeID,int userId, List<PaymentMode> paymentModes)
        {
            Action<long> validateAssesseeDispution = (_asseseeId) => {
                AssesseeBll _bll = new AssesseeBll();
                var assessee = _bll.GetAssesseebyId(_asseseeId);
                if (assessee != null)
                {
                    if (assessee.IsDisputed)
                    {
                        throw new InvalidOperationException("Payment Aborted! Assessee marked as disputed");
                    }
                }

            };
            PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
            Func<decimal,int,int,long,PropertyTaxPaymentMaster> getPropertyTaxMasterObject = (_advanceAmount, _userId,_municipalityId,_assesseeId) =>
            {
                var today = DateTime.Now.Date;
                MunicipalityTaxPaymentCounter_BLL objBll = new MunicipalityTaxPaymentCounter_BLL();
                MunicipalityPaymentCounter objCounter = objBll.GetCounter(_municipalityId, DateTime.Now, _userId);
                PropertyTaxPaymentMaster pm = new PropertyTaxPaymentMaster();
                pm.PaymentType = "F";
                pm.AssesseeID = _assesseeId;
                pm.MunicipalityID = _municipalityId;
                pm.BillID = null;
                pm.PaymentCollectionDate = today;
                pm.PaymentReceiveDate = today;

                pm.NoticeServedDate = CommonUtils.GetFinYearFirstDate(today);
                pm.NetAmount = 0;

                pm.PaidAmount = _advanceAmount;

                pm.RoundOffAdjust = 0;
                pm.PayMode = null;
                pm.InstrumentNo = null;
                pm.InstrumentDate = null;

                pm.CounterID = (objCounter == null ? null :(int?) objCounter.CounterID);
                pm.CollectorID = _userId;
                pm.InsertedBy = _userId;
                pm.TransactionID = null;

                pm.AdjustedAmount = 0;


                pm.TotalCalculatedAmnt = 0;
                pm.EffectiveQtrFrom = null;
                return pm;
            };
            Func<List<PaymentMode>, List<PaymentMode>> __removeUnwantedPaymentModes = (__paymentModes) =>
            {
                List<PaymentMode> pnLst = new List<PaymentMode>();
                __paymentModes.ForEach(t =>
                {
                    if (t.HasInstrument == false)
                    {
                        //cash or online
                        if (t.TotalAmount != 0)
                        {
                            pnLst.Add(t);
                        }
                    }
                    else
                    {
                        //other have instruments
                        if (t.PaymentInstruments.Count > 0 && t.TotalAmount > 0)
                        {
                            pnLst.Add(t);
                        }
                    }
                });

                return pnLst;
            };
            Func<List<PaymentMode>, decimal> getTotalAmountFormPaymentMode = (__paymentModes) =>
            {
                decimal instrumentalTotalAmount = __paymentModes.Where(t => t.HasInstrument == true).Sum(x => x.PaymentInstruments.Sum(j => j.InstrumentAmount));
                decimal nonInstrumentalTotalAmount = __paymentModes.Where(t => t.HasInstrument == false).Sum(x => x.TotalAmount);

                return instrumentalTotalAmount + nonInstrumentalTotalAmount;
            };
            Action<int, int, int, long, PropertyTaxCollectionBLL,List<PaymentMode>,PropertyTaxPaymentMaster> _businessValidation = (_municipalityID, _wardID , _locationID, _assesseeID, _bll, __paymentModes, _PropertyTaxPaymentMaster) =>
            {
                AssesseeBll asBll = new AssesseeBll();
                var assessee = asBll.GetAssesseebyId(_assesseeID);
                if (assessee != null && assessee.WardID == _wardID && assessee.LocationID == _locationID)
                {
                    if (getTotalAmountFormPaymentMode(__paymentModes) <= 0)
                    {
                        throw new InvalidOperationException("Advance Amount should be greater than zero");
                    }
                }
                else
                {
                    throw new ArgumentException("Invalid Assessee");
                }
                List<PropertyTaxOutstading> lstPropertyTaxOutstading = bll.GetPropertyTaxPaymentDetails(_municipalityID, _wardID, _locationID, _assesseeID);
                if (lstAdjustmnets.Count > 0)
                {
                    string x = string.Join(",", lstPropertyTaxOutstading.Select(t => string.Format("{0},{1}", t.FinYear, t.QtrNo)).ToArray());
                    x = string.Format("[{0}]", x);
                    throw new InvalidOperationException("Invalid Operation: Previouse outstanding pending. Details: " + x);
                }
                if (_PropertyTaxPaymentMaster.CounterID == null)
                {
                    throw new ArgumentException("Counter ID not found associated with logged in user.Hint: Please configure or map current user to counter");
                }
                
            };

            paymentModes = __removeUnwantedPaymentModes(paymentModes);
            var advanceAmount = getTotalAmountFormPaymentMode(paymentModes);
            var propertyTaxObject = getPropertyTaxMasterObject(advanceAmount,userId,MunicipalityID,assesseeID);
            //call validation
            validateAssesseeDispution(assesseeID);
            _businessValidation(MunicipalityID, WardID, LocationID, assesseeID, bll,paymentModes, propertyTaxObject);
            return bll.MakeAdvancePayment(advanceAmount, paymentModes, propertyTaxObject);
        }

        public long? MakePayment(PropertyTaxPaymentMaster PropertyTaxPaymentMaster, List<TaxPaymentCheckParameter> taxPaymentCheckParameters, List<PaymentMode> paymentModes)
        {
            //PTaxCollectionGiantObject __gaintObject = Calculate(MunicipalityID,WardID,LocationID,AssesseeID,paymentDate,taxPaymentCheckParameters);
            MunicipalityConfiguration ColAdjconfiguration = new MunicipalityConfigurationBll().GetConfiguration(PropertyTaxPaymentMaster.MunicipalityID ?? 0, PropertyTaxPaymentMaster.InsertedBy ?? 0);

            if (paymentModes == null || paymentModes.Count == 0)
            {
                throw new ArgumentException("No Payment Information Found");
            }
            #region helper methods
            Func<decimal> getSuppliedNetAmount = () =>
            {
                decimal instrumentalTotalAmount = paymentModes.Where(t => t.HasInstrument == true).Sum(x => x.PaymentInstruments.Sum(j => j.InstrumentAmount));
                decimal nonInstrumentalTotalAmount = paymentModes.Where(t => t.HasInstrument == false).Sum(x => x.TotalAmount);

                return instrumentalTotalAmount + nonInstrumentalTotalAmount;
            };
            Func<PropertyTaxPaymentMaster, decimal> getCalculatedNetAmount = (__propertyTaxPaymentMaster) =>
            {
                decimal netAmount = __propertyTaxPaymentMaster.NetAmount ?? 0;
                return netAmount;
            };
            #endregion
            #region validators
            Action<long> validateAssesseeDispution = (_asseseeId) => {
                AssesseeBll _bll = new AssesseeBll();
                var assessee = _bll.GetAssesseebyId(_asseseeId);
                if(assessee!=null)
                {
                    if(assessee.IsDisputed)
                    {
                        throw new InvalidOperationException("Payment Aborted! Assessee Marked as disputed");
                    }
                }

            };
            
            Action<PropertyTaxPaymentMaster, List<PaymentMode>> validatePaymentMaster = (__propertyTaxPaymentMaster, __paymentModes) =>
            {

                #region validate Instruments has Proper Details
                Action validateInstrumentsInformation = () =>
                {
                    paymentModes.ForEach(t =>
                    {
                        if (t.HasInstrument)
                        {
                            t.PaymentInstruments.ForEach(i =>
                            {
                                if (i.ModeType == 2)//DD,N,Q,T
                                {
                                    var obj = i as PaymentInstrument;
                                    if (string.IsNullOrEmpty(obj.BankName.Trim()))
                                    {
                                        throw new ArgumentException(string.Format("Incomplete Instrument Information! Where Instrument Amount {0} And Instrument Type -{1} ('{2}')", "Bank Name", t.PaymentModeDescription, t.PaymentModeCode));
                                    }
                                    if (string.IsNullOrEmpty(obj.InstrumentNo.Trim()))
                                    {
                                        throw new ArgumentException(string.Format("Incomplete Instrument Information! Where Instrument Amount {0} And Instrument Type -{1} ('{2}')", "Instrument No", t.PaymentModeDescription, t.PaymentModeCode));
                                    }
                                }
                            });
                        }
                    });
                };
                #endregion
                #region validate more than one qtr checked
                Action validate_HaveMoreThanZeroChecks = () =>
                {
                    int countCheckedQtr = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(t => t.taxPaymentCheckParameter.IsChecked == true).Count();
                    if (countCheckedQtr <= 0)
                    {
                        throw new ArgumentException("No Selected Quarters found");
                    }
                };
                #endregion

                #region validate net amount with uploaded payment modes
               
                Action validate_NetAmount = () =>
                {
                    var paidAmount = getSuppliedNetAmount();
                    var totalNetAmount = getCalculatedNetAmount(__propertyTaxPaymentMaster);
                    if (paidAmount < 0 || totalNetAmount < 0)
                    {
                        throw new ArgumentException(string.Format("Paid Amount or Net Amount should be >=0"));
                    }
                    
                };
                #endregion

                #region validation notice served Date
                Action<DateTime?, DateTime> _validateNoticeServedDate = (noticeServedDate,paymentDate) =>
                {
                    if (noticeServedDate != null)
                    {
                        //DateTime x = CommonUtils.GetFinYearFirstDate(CommonUtils.GetFinYear(DateTime.Now));
                        DateTime x = CommonUtils.GetFinYearFirstDate(CommonUtils.GetFinYear(paymentDate));
                        if (noticeServedDate < x)
                        {
                            throw new ArgumentException(string.Format("Invalid Notice Served Date"));
                        }
                    }
                };
                #endregion  
                #region call inner validators
                _validateNoticeServedDate(__propertyTaxPaymentMaster.NoticeServedDate, __propertyTaxPaymentMaster.PaymentCollectionDate.Value);
                validateInstrumentsInformation();
                validate_HaveMoreThanZeroChecks();
                validate_NetAmount();
                #endregion
            };
            #endregion
            #region Converters
            Func<List<PaymentMode>, List<PaymentMode>> __removeUnwantedPaymentModes = (__paymentModes) =>
            {
                List<PaymentMode> pnLst = new List<PaymentMode>();
                __paymentModes.ForEach(t =>
                {
                    if (t.HasInstrument == false)
                    {
                        //cash or online
                        if (t.TotalAmount != 0)
                        {
                            pnLst.Add(t);
                        }
                    }
                    else
                    {
                        //other have instruments
                        if (t.PaymentInstruments.Count > 0 && t.TotalAmount > 0)
                        {
                            pnLst.Add(t);
                        }
                    }
                });

                return pnLst;
            };
            Func<PropertyTaxPaymentMaster, List<TaxPaymentCheckParameter>, PropertyTaxPaymentMaster> __removeUnwantedQtrs = (__propertyTaxPaymentMaster, __taxPaymentCheckParameters) =>
            {
                List<PropertyTaxPaymentDetail> lstCheckedQtrs = new List<PropertyTaxPaymentDetail>();
                __taxPaymentCheckParameters.Where(f => f.IsChecked == true).ToList().ForEach(t =>
                {
                    PropertyTaxPaymentDetail __PropertyTaxPaymentDetail = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.FirstOrDefault(p => p.FinYear == t.FinYear && p.QtrNo == t.QtrNo);
                    if (__PropertyTaxPaymentDetail != null)
                    {
                        lstCheckedQtrs.Add(__PropertyTaxPaymentDetail);
                        //__propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Remove(__PropertyTaxPaymentDetail);
                    }
                });
                var itemsToBeDeleted = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(t => !lstCheckedQtrs.Contains(t)).ToList();
                itemsToBeDeleted.ForEach(i =>
                {
                    __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Remove(i);
                });
                return __propertyTaxPaymentMaster;
            };
            #endregion
            #region Collector Adjusment route
            Action<PropertyTaxPaymentMaster,decimal,decimal> __PassAllPaymentConfigRoute = (_propertyTaxPaymentMaster,_paidAmount,_payableAmount) => {
                if (_paidAmount > _payableAmount)
                {
                    //advance payment route
                    var advanceAmount = _paidAmount - _payableAmount;
                    _propertyTaxPaymentMaster.AdvanceAmount = advanceAmount;
                    //after implementation of advance payment. override Paid Amount=supplied amount()
                    _propertyTaxPaymentMaster.PaidAmount = _paidAmount;
                }
                else if (_paidAmount == _payableAmount)
                {
                    //normal route
                    //skip this route, nothing to do
                }
                else
                {
                    //_paidAmount < _payableAmount
                    //collector adjustment route
                    if (ColAdjconfiguration==null||ColAdjconfiguration.CollectorAdjustmentConfiguration == null)
                    {
                        throw new InvalidOperationException("Collector adjustment configuration not found. Please contact with vendor and configuare it");
                    }
                    else if (ColAdjconfiguration.CollectorAdjustmentConfiguration.IsActiveForMunicipality && ColAdjconfiguration.CollectorAdjustmentConfiguration.IsEnabledCollectorAdjustmentOnUser)
                    {
                        var collectorAdjustedAmount = _payableAmount - _paidAmount;
                        decimal maxAdjustableAmount = 0;

                        //if adjustment type field value(FV), (Max adjustable amount specified in configuration field, named=>MaxAdjustableAmount)
                        if (ColAdjconfiguration.CollectorAdjustmentConfiguration.CollctrAdjustmentType == "FV")
                        {
                            maxAdjustableAmount = ColAdjconfiguration.CollectorAdjustmentConfiguration.MaxAdjustableAmount;
                        }
                        //if adjustment type Interest(IN), (Max adjustable amount will be payable interest amount)
                        else if (ColAdjconfiguration.CollectorAdjustmentConfiguration.CollctrAdjustmentType == "IN")
                        {
                            var totalInterestAmount = _propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(rr => rr.PaidCalculatedAmt > 0).Sum(rs => rs.PaidCalculatedAmt ?? 0);
                            maxAdjustableAmount = Math.Floor(totalInterestAmount);
                        }
                        else 
                        {
                            throw new ArgumentException("Bad Collector Adjustment configuration, No adjustment configuration found");
                        }
                        

                        if (collectorAdjustedAmount <= maxAdjustableAmount)
                        {
                            _propertyTaxPaymentMaster.NetAmount = _payableAmount - collectorAdjustedAmount;
                            _propertyTaxPaymentMaster.PaidAmount = _payableAmount - collectorAdjustedAmount;
                            _propertyTaxPaymentMaster.CollectorAdjustmentAmount = collectorAdjustedAmount;
                        }
                        else
                        {
                            //adjustment amount range exceeded.
                            throw new ArgumentException(string.Format("Unable to Perform Collector Adjustment, Adjustable Amount Exceeded Range.Max Adjustable Amount Rs." + maxAdjustableAmount));
                        }
                    }
                    else
                    {
                        //adjustment payment not possible
                       throw new ArgumentException(string.Format("Paid amount should not be less than net amount. Expected Amount {0}, Paid Amount {1}", _payableAmount, _paidAmount));
                    }
                }

                
            };
            #endregion
            List<PaymentMode> pnModes = __removeUnwantedPaymentModes(paymentModes);
            PropertyTaxPaymentMaster propertyTaxPaymentMaster = __removeUnwantedQtrs(PropertyTaxPaymentMaster, taxPaymentCheckParameters);

            validatePaymentMaster(propertyTaxPaymentMaster, paymentModes);
            __PassAllPaymentConfigRoute(propertyTaxPaymentMaster, getSuppliedNetAmount(), getCalculatedNetAmount(propertyTaxPaymentMaster));
            validatePaymentMaster(propertyTaxPaymentMaster, paymentModes);

            validateAssesseeDispution(propertyTaxPaymentMaster.AssesseeID??0);
            
            long? paymentID = new PropertyTaxCollectionBLL().PayPropertyTax(propertyTaxPaymentMaster, pnModes, true);
            return paymentID;
        }
        public long? MakePaymentOnline_Temp(PropertyTaxPaymentMaster PropertyTaxPaymentMaster, List<TaxPaymentCheckParameter> taxPaymentCheckParameters, string pgCode)
        {
            #region validators
            Action<long> validateAssesseeDispution = (_asseseeId) => {
                AssesseeBll _bll = new AssesseeBll();
                var assessee = _bll.GetAssesseebyId(_asseseeId);
                if (assessee != null)
                {
                    if (assessee.IsDisputed)
                    {
                        throw new InvalidOperationException("Payment Aborted! Assessee Marked as disputed");
                    }
                }

            };
            Action<PropertyTaxPaymentMaster> validatePaymentMaster = (__propertyTaxPaymentMaster) =>
            {
                #region validate more than one qtr checked
                
                Action validate_HaveMoreThanZeroChecks = () =>
                {
                    int countCheckedQtr = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(t => t.taxPaymentCheckParameter.IsChecked == true).Count();
                    if (countCheckedQtr <= 0)
                    {
                        throw new ArgumentException("No Selected Quarters found");
                    }
                };
                #endregion
                #region validate net amount with uploaded payment modes
                Action validate_NetAmount = () =>
                {
                    Func<decimal> getCalculatedNetAmount = () =>
                    {
                        // decimal totalPaidAmount = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(t => t.taxPaymentCheckParameter.IsChecked == true).Sum(x=>(x.PaidSurchargeAmt+x.PaidCalculatedAmt+x.PaidTaxAmt)) ??0;
                        decimal netAmount = __propertyTaxPaymentMaster.NetAmount ?? 0;
                        return netAmount;
                    };
                    var totalNetAmount = getCalculatedNetAmount();
                    if (totalNetAmount <= 0)
                    {
                        throw new ArgumentException(string.Format("Paid Amount or Net Amount should be >=0"));
                    }
                };
                #endregion
                #region validation notice served Date
                Action<DateTime?> _validateNoticeServedDate = (noticeServedDate) =>
                {
                    if (noticeServedDate != null)
                    {
                        DateTime x = CommonUtils.GetFinYearFirstDate(CommonUtils.GetFinYear(DateTime.Now));
                        if (noticeServedDate < x)
                        {
                            throw new ArgumentException(string.Format("Invalid Notice Served Date"));
                        }
                    }
                };
                #endregion
                #region call inner validators
                _validateNoticeServedDate(__propertyTaxPaymentMaster.NoticeServedDate);
                validate_HaveMoreThanZeroChecks();
                validate_NetAmount();
                #endregion
            };
            #endregion
            #region Converters
            Func<PropertyTaxPaymentMaster, List<TaxPaymentCheckParameter>, PropertyTaxPaymentMaster> __removeUnwantedQtrs = (__propertyTaxPaymentMaster, __taxPaymentCheckParameters) =>
            {
                List<PropertyTaxPaymentDetail> lstCheckedQtrs = new List<PropertyTaxPaymentDetail>();
                __taxPaymentCheckParameters.Where(f => f.IsChecked == true).ToList().ForEach(t =>
                {
                    PropertyTaxPaymentDetail __PropertyTaxPaymentDetail = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.FirstOrDefault(p => p.FinYear == t.FinYear && p.QtrNo == t.QtrNo);
                    if (__PropertyTaxPaymentDetail != null)
                    {
                        lstCheckedQtrs.Add(__PropertyTaxPaymentDetail);
                        //__propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Remove(__PropertyTaxPaymentDetail);
                    }
                });
                var itemsToBeDeleted = __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Where(t => !lstCheckedQtrs.Contains(t)).ToList();
                itemsToBeDeleted.ForEach(i =>
                {
                    __propertyTaxPaymentMaster.PropertyTaxPaymentDetails.Remove(i);
                });
                return __propertyTaxPaymentMaster;
            };
            #endregion
            //List<PaymentMode> pnModes = __removeUnwantedPaymentModes(paymentModes);
            PropertyTaxPaymentMaster propertyTaxPaymentMaster = __removeUnwantedQtrs(PropertyTaxPaymentMaster, taxPaymentCheckParameters);
            validatePaymentMaster(PropertyTaxPaymentMaster);
            validateAssesseeDispution(propertyTaxPaymentMaster.AssesseeID ?? 0);
            PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
            long? paymentID = bll.PayPropertyTaxOnline(propertyTaxPaymentMaster, false, pgCode);
            return paymentID;
        }
        PropertyTaxPaymentMaster Convert(List<PropertyTaxPaymentDetail> lstTaxPaymentDetails,long assesseeID)
        {
            if (lstTaxPaymentDetails.Count > 0)
            {
                #region Code injected 031120174 for other adjustment
                List<OtherAdjustmentSummary_VM> lstAdjSummary = new List<OtherAdjustmentSummary_VM>();
                decimal _adjustableAmount = 0;
                foreach (IOtherAdjustment adjustment in lstAdjustmnets)
                {
                    decimal _adjustableAmountPerRow = 0;
                    _adjustableAmountPerRow = Math.Round(adjustment.GetAdjustableAmount(lstTaxPaymentDetails.Where(t => t.taxPaymentCheckParameter.IsChecked).ToList(), assesseeID), 2, MidpointRounding.AwayFromZero);
                    _adjustableAmount += _adjustableAmountPerRow;
                    lstAdjSummary.Add(new OtherAdjustmentSummary_VM()
                    {
                        MstAdjID = adjustment.MstAdjID,
                        PayHeadType = adjustment.PayHeadType,
                        AdjustedAmount = _adjustableAmountPerRow,
                        AdjustmentText = string.Format("{0}: {1}Rs/-", adjustment.PayHeadDesc, _adjustableAmountPerRow),
                        RateValue = adjustment.RateValue,
                        //RateValueType = adjustment.RateValueType,
                        WellKnownName = adjustment.PayHeadDesc,
                        MstPayHeadID = adjustment.MstPayHeadID
                    });
                }
                #endregion

                decimal grossAmount = lstTaxPaymentDetails.Where(x => x.taxPaymentCheckParameter.IsChecked == true).Sum(t => (t.PaidTaxAmt ?? 0) + (t.PaidSurchargeAmt ?? 0));//sum(paidPtax+paidSurcharg)
                decimal totCalculatedAmount = lstTaxPaymentDetails.Where(x => x.taxPaymentCheckParameter.IsChecked == true).Sum(t => t.CalculatedValue ?? 0);

                PropertyTaxPaymentMaster mainObj = new PropertyTaxPaymentMaster();
                mainObj.GrossAmount = Math.Round(grossAmount, 2, MidpointRounding.AwayFromZero);
                mainObj.NetAmount = _adjustableAmount + totCalculatedAmount + grossAmount;

                mainObj.PropertyTaxPaymentDetails = lstTaxPaymentDetails;
                mainObj.AdjustmentSummaries = lstAdjSummary;
                mainObj.AdjustedAmount = _adjustableAmount;

                mainObj.TotalCalculatedAmnt = totCalculatedAmount;
                mainObj.PaidAmount = 0;
                if (mainObj.NetAmount < 0)
                {
                    mainObj.AdjustedAmount = mainObj.AdjustedAmount - mainObj.NetAmount ?? 0;

                    foreach (var t in lstAdjSummary)
                    {
                        if (((t.AdjustedAmount) * -1) >= (mainObj.NetAmount ?? 0 * -1))
                        {
                            t.AdjustedAmount = t.AdjustedAmount - mainObj.NetAmount ?? 0;
                            mainObj.NetAmount = 0;
                            break;
                        }
                    }
                }
                mainObj.RoundOffAdjust = (Math.Round(mainObj.NetAmount ?? 0, 0, MidpointRounding.AwayFromZero) - mainObj.NetAmount);
                mainObj.NetAmount = mainObj.NetAmount + mainObj.RoundOffAdjust;//make round
                return mainObj;
            }
            else
            {
                return null;
            }

        }

    }
}