﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes.Utils
{
    public class Converter
    {
        public static class Area
        {
            public static decimal ConvertToSFT(decimal kt,decimal ch, decimal sft)
            {
                return kt * 720 + ch * 45 + sft;
            }
            public static decimal ConvertToSFT(decimal kt, decimal ch)
            {
                return kt * 720 + ch * 45;
            }
            public static decimal ConvertFrom_KtToSFT(decimal kt)
            {
                return kt * 720 ;
            }
            public static decimal ConvertFrom_ChToSFT(decimal ch)
            {
                return ch * 45;
            }
        }
        
    }
}