﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes.Utils
{
    public class SiteConstants
    {
        public static string ErrorViewPath = @"~\Views\Shared\ErrorView.cshtml";
        public static string SuccessViewPath = @"~\Views\Shared\SuccessView.cshtml";
    }
}