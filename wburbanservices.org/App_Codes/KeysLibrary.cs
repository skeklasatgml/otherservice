﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes
{
    internal class KeysLibrary
    {
        public class SessionKeys
        {
            public static string CurrentUser_Key
            {
                get
                {
                    return "{C67B8BAB-F639-4ED2-BD98-72AE0EFC419C}";
                }
            }
            public static string CurrentModuleKey
            {
                get
                {
                    return "{96857292-8733-4483-846D-C4F53AB19C9C}";
                }
            }
        }
        public class GlobalKeys
        {
            public static string LoggedInUsers_Key
            {
                get
                {
                    return "{2D387E82-D86E-4923-8B0B-F617AF4F7FD2}";
                }
            }
            public static string ApplicationInfo_Key
            {
                get
                {
                    return "{82DFE1D3-CB97-40AF-A5B2-EE2E2658D64A}";
                }
            }
        }
        

    }
}