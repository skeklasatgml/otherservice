﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.App_Codes.Exceptions
{
    public class AppException: ApplicationException
    {
        public string ErrorCode { get; set; }
        public AppException(string message):base(message)
        {

        }
        public AppException(string message,string errorCode) : base(message)
        {
            this.ErrorCode = errorCode;
        }

        public object Serialize()
        {
            return new { Message = this.Message, ErrorCode = this.ErrorCode };
        }
    }
    public class AppException<t1,t2>: AppException
    {
        
        public AppException(string Message,string ErrorCode,IDictionary<t1,t2> Data):base(Message,ErrorCode)
        {
            if (Data != null)
            {
                foreach (var item in Data)
                {
                    base.Data.Add(item.Key, item.Value);
                }
            }
        }
    }
}