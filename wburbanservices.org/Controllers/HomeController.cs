﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.App_Codes.Authenticator;
using Valuation_Board.Models.Account;
using Valuation_Board.Models;
using Valuation_Board.Models.AppResource;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using System.Security;
using Valuation_Board.Utils.EncryptionDecrytion;
using System.Configuration;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;

namespace Valuation_Board.Controllers
{
    [SecuredFilter]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : Controller
    {
        #region Global Variables
        ClientJsonResult cr = new ClientJsonResult();
        JsonResult js = new JsonResult();
        #endregion
        public ActionResult Index()
        {

            if (SessionContext.IsAuthenticated)
            {
                var userType = (SessionContext.CurrentUser as IUserType).UserType;
                var user = (SessionContext.CurrentUser as IInternalUser);
                if (userType == UserType.MunicipalityUser)
                {
                    return Redirect("/Home/PopulateModules");
                }
                else if (userType == UserType.AdministrativeUser)
                {
                    return Redirect("/Home/PopulateModules");
                }
                else
                {
                    SessionContext.RemoveCustomCookies();
                    return View();
                }
            }
            else
            {
                SessionContext.RemoveCustomCookies();
                return View();
            }

        }
        public JsonResult MunicipalityLogin(MunicipalityLogin MunicipalityLogin)
        {
            try
            {
                MunicipalityAuthenticator mAuth = new MunicipalityAuthenticator(MunicipalityLogin);
                MunicipalityUser mUser = mAuth.Authenticate();

                App_Codes.SessionContext.Login(mUser);
                Session["session_MunicipalityID"] = MunicipalityLogin.MunicipalityID;
                Session["session_UserName"] = MunicipalityLogin.UserName;
                Session["session_Password"] = MunicipalityLogin.Password;
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = true,
                    RedirectUrl = "/Home/PopulateModules"
                };
                cr.Message = "Login Successful";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (SecurityException ex)
            {
                cr.Data = new LoginResponse() { IsLoggedIn = false };
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNAUTHORIZED;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult AdminstrativeLogin(AdministrativeLogin ValuationBoardLogin)
        {
            try
            {
                AdministrativedAuthenticator vAuth = new AdministrativedAuthenticator(ValuationBoardLogin);
                AdministrativeUser vUser = vAuth.Authenticate();
                if (vUser == null)
                {
                    throw new ArgumentException("Invalid Credentials");
                }
                App_Codes.SessionContext.Login(vUser);
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = true,
                    RedirectUrl = "/Home/PopulateModules"
                };
                cr.Message = "Login Successfull";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = false,
                };
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNAUTHORIZED;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult OtherCitizenLogin(UserLogin userLogin)
        {
            try
            {
                //userLogin = new UserLogin
                //{
                //    UserName = "EKLAS",
                //    Password = "12345"
                //};
                OtherCitizenAuthenticator vAuth = new OtherCitizenAuthenticator(userLogin);
                OtherCitizenUser vUser = vAuth.Authenticate();
                if (vUser == null)
                {
                    throw new ArgumentException("Invalid Credentials");
                }
                App_Codes.SessionContext.Login(vUser);
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = true,
                    RedirectUrl = "/Home/PopulateModules"
                };
                cr.Message = "Login Successfull";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = false,
                };
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNAUTHORIZED;
            }
            js.Data = cr; js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        public JsonResult CitizenLogin(CitizenLogin CitizenLogin)
        {
            try
            {
                IEncryptDecrypt chiperAlgorithm;
                chiperAlgorithm = new AES_Algorithm(ConfigurationManager.AppSettings["ChiperKey"] as string);

                CitizenAuthenticator cAuth = new CitizenAuthenticator(CitizenLogin);
                CitizenUser cUser = cAuth.Authenticate();
                if (cUser == null)
                {
                    throw new ArgumentException("Assessee Not Found");
                }
                JavaScriptSerializer jss = new JavaScriptSerializer();
                if (string.Equals(CitizenLogin.Cmd, "rdct_assmt", StringComparison.OrdinalIgnoreCase))
                {
                    cr.Data = new LoginResponse()
                    {
                        IsLoggedIn = true,
                        RedirectUrl = string.Format("/Administration/ScheduleThree/S3Container?Data={0}", chiperAlgorithm.Encrypt(jss.Serialize(cUser)))
                    };
                }
                else
                {
                    cr.Data = new LoginResponse()
                    {
                        IsLoggedIn = true,
                        RedirectUrl = string.Format("{0}?Data={1}", new BllBase().GetLandingUrl(4), chiperAlgorithm.Encrypt(jss.Serialize(cUser)))
                    };
                }

                SessionContext.Login(cUser);

                cr.Message = "Login Successfull";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cr.Data = new LoginResponse()
                {
                    IsLoggedIn = false,
                };
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNAUTHORIZED;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult LogOut()
        {
            try
            {
                App_Codes.SessionContext.Logout();
                cr.Data = new LoginResponse()
                {
                    RedirectUrl = new CitizenUserBLL().GetUrl_AfterLogout()
                };
                cr.Message = "Logged Out";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cr.Data = new LoginResponse();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        public ActionResult PopulateModules()
        {
            try
            {
                using (ModuleBll vs = new ModuleBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    var data = vs.GetAuthorisedModules(obj.UserID, obj.UserType);
                    //if (data.Count == 1 && string.Equals(data.FirstOrDefault().Code, "OTHER", StringComparison.OrdinalIgnoreCase))
                    //{
                    //    return new RedirectResult("/OtherServices/OtherServices/ApplicantDashboard");
                    //}
                    return View("~/Views/Home/ModulesPopulation.cshtml", data);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View("~/Views/Home/ModulesPopulation.cshtml");
            }

        }
        public ActionResult RedirectFromModule(string modcode)
        {
            var user = SessionContext.CurrentUser as IInternalUserMin;
            try
            {
                using (ModuleBll bll = new ModuleBll())
                {

                    var module = bll.GetModule(null, modcode);
                    if (module == null)
                        throw new Exception("Module not found");
                    string url = string.Empty;
                    SessionContext.SetModule(module);
                    if (user.UserType == UserType.MunicipalityUser)
                        url = new MunicipalityUserBll().GetUrl_LandingPage(user as MunicipalityUser, module.Id);
                    else if (user.UserType == UserType.AdministrativeUser)
                        url = new AdministrativeUserBll().GetUrl_LandingPage(user as AdministrativeUser, module.Id);
                    else
                        throw new Exception("Invalid module redirection");

                    if (string.Equals(modcode, "ACCNT", StringComparison.OrdinalIgnoreCase))
                    {
                        if (user.UserType == UserType.AdministrativeUser) throw new Exception("Can not login from Administrative module");

                        string MunicipalityID = Session["session_MunicipalityID"].ToString();
                        string UserName = Session["session_UserName"].ToString();
                        string Password = Session["session_Password"].ToString();

                        var obj = new { MunicipalityID = Session["session_MunicipalityID"], UserName = Session["session_UserName"], Password = Session["session_Password"] };

                        IEncryptDecrypt chiperAlgorithm;
                        chiperAlgorithm = new AES_Algorithm(ConfigurationManager.AppSettings["ChiperKey"] as string);


                        JavaScriptSerializer js = new JavaScriptSerializer();
                        string serializedData = js.Serialize(obj);
                        string encryptedUser = chiperAlgorithm.Encrypt(serializedData);
                        string urlEncodedData = HttpUtility.UrlEncode(encryptedUser);
                        url = url + "LoginManager/RedirectfromOtherDomain?Data=" + urlEncodedData + "";
                    }

                    return new RedirectResult(url);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(SiteConstants.ErrorViewPath, appExp);
            }
        }
    }
}