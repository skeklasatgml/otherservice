﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Shared
{[SecuredFilter]
    public class LocationController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        LocationBLL lcBll = new LocationBLL();
        #endregion
        [HttpPost]
        public JsonResult GetLocations(int MunicipalityID,int WardID,string LocationName)
        {
            List<Location_Ddl_VM> lstLcVm = new List<Location_Ddl_VM>();
            try
            {
                List<Location> lstLc = lcBll.GetLocations(LocationName,MunicipalityID,WardID);
                lstLcVm = lstLc.Select(t =>
                {
                    return new Location_Ddl_VM()
                    {
                        LocationID=t.LocationID ?? 0,
                        LocationName = t.LocationName
                    };
                }).ToList();
                cr.Data = lstLcVm;
                cr.Message = string.Format("{0} Locations Found", lstLcVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = lstLcVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}