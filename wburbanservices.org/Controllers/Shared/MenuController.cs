﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Application;
using Valuation_Board.Models.Account;
using Valuation_Board.Models;

namespace Valuation_Board.Controllers.Shared
{
    public class MenuController:Controller
    {
        Menu_BLL menuBll = new Menu_BLL();

        public ActionResult GetMenu()
        {
            List<Menu> lstMenu = new List<Menu>();
            if (SessionContext.IsAuthenticated == true)
            {
                var curUser = SessionContext.CurrentUser;
                var userMin = curUser as IInternalUserMin;
                    var curModule = SessionContext.CurrentModue;
                    int? moduleId =( curModule == null ? null : curModule.Id as int?);
                    lstMenu = menuBll.getMenuDrtails(((IInternalUserMin)curUser).UserID, (int)((IUserType)curUser).UserType, SessionContext.AppTag,moduleId).Where(t => t.IsActive == true).ToList();
            }
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult()
            {
                Data = lstMenu,
                Message = string.Format("{0} Menus found", lstMenu.Count),
                Status = ResponseStatus.SUCCESS
            };
            js.Data = cr;
            return js;
        }
    }
}