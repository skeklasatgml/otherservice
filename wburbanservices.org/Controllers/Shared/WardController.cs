﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Shared
{[SecuredFilter]
    public class WardController : Controller
    {

        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        WardBLL wrdBLL = new WardBLL();
        #endregion
        [HttpPost]
        public JsonResult GetWards(int MunicipalityID,string WardName)
        {
            List<Ward_Ddl_VM> wrVm = new List<Ward_Ddl_VM>();
            try
            {
                List<Ward> lstWr = wrdBLL.GetWards(MunicipalityID,WardName);
                wrVm = lstWr.Select(t =>
                {
                    return new Ward_Ddl_VM()
                    {
                        WardID=t.WardID ?? 0,
                        WardName=t.WardName
                    };
                }).ToList();
                cr.Data = wrVm;
                cr.Message = string.Format("{0} Wards Found", wrVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = wrVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}