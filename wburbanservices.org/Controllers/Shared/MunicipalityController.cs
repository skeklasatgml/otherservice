﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Shared
{[SecuredFilter]
    public class MunicipalityController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MunicipalityBLL mnBll = new MunicipalityBLL();
        #endregion
        [HttpPost]
        public JsonResult GetMunicipalitesByDistrictID(int DistrictID)
        {
            List<Municipality_Ddl_VM> mnVm = new List<Municipality_Ddl_VM>();
            try
            {
                List<Models.Application.Municipality> lstMn = mnBll.GetMunicipalitiesByDistID(DistrictID);
                mnVm = lstMn.Select(t =>
                {
                    return new Municipality_Ddl_VM()
                    {
                        MunicipalityID = t.MunicipalityID??0,
                        MunicipalityName = t.MunicipalityName
                    };
                }).ToList();
                cr.Data = mnVm;
                cr.Message = string.Format("{0} Municipalities Found", mnVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = mnVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}