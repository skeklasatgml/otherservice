﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Controllers.Shared
{
    [SecuredFilter]
    public class AssesseController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        // GET: Assesse
        [HttpPost]
        public JsonResult SearchHoldingSuggetion(int MunicipalityID,int WardID,int LocationID,string HoldingSuggetion)
        {
            List<string> lst = new List<string>();
            try
            {
                AssesseeBll bll = new AssesseeBll();
                lst = bll.GetHoldingBySuggestion(MunicipalityID, WardID, LocationID, HoldingSuggetion).OrderBy(t=>t).Take(20).ToList();
                cr.Data = lst;
                cr.Message = string.Format("{0} holdings found",lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                cr.Data = lst;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAssesseesByHoldingNoForKnowYoueProps(int MunicipalityID, string HoldingNo, string AssesseeName, string SearchTypeVal)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                AssesseeBll _assesseeBLL = new AssesseeBll();
                //MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<Assessee> lstAssessee = _assesseeBLL.GetAssesseesForKnowyourProps(MunicipalityID, HoldingNo, AssesseeName, SearchTypeVal);
                lstAssessee = _assesseeBLL.IncludeWard(lstAssessee, MunicipalityID);
                lstAssessee.ForEach(t => {
                    var assessee = _assesseeBLL.IncludeLocation(t);
                    t.Location = assessee.Location == null ? null : assessee.Location;
                });

                var data = lstAssessee.Select(t => {
                    return new
                    {
                        AssesseeID = t.AssesseeID,
                        AssesseeName = t.AssesseeName,
                        AssesseeNo = t.AssesseeNo,
                        WardName = t.Ward == null ? null : t.Ward.WardName,
                        HoldingNo = t.HoldingNo,
                        LocationName = t.Location == null ? null : t.Location.LocationName
                    };
                });
                cr.Data = data;
                cr.Message = string.Format("{0} Assessees found", lstAssessee.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Assessee>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult GetAssesseeByID(long AssesseeID)
        {
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                AssesseeBll _assesseeBLL = new AssesseeBll();
                //MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                Assessee assessee = _assesseeBLL.GetAssesseebyIdForKnowYourProps(AssesseeID);
                if (assessee == null)
                {
                    assessee = null;
                }
                else
                {
                    assessee = _assesseeBLL.IncludeLocation(assessee);
                    assessee = _assesseeBLL.IncludeWard(assessee);
                }
                cr.Data = assessee;
                cr.Message = string.Format("Assessees found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_PropertyTaxPaymentDetails(int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters, DateTime? NoticeServedOn, int? EffectQtrFrom)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult jc = new ClientJsonResult();
            try
            {
                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                //MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                PaymentProcessor payment = new PaymentProcessor();

                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });
                Object = payment.SearchOutStanding(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, NoticeServedOn, EffectQtrFrom, AssesseeID);
                if (Object != null)
                {
                    AssesseeBll assbll = new AssesseeBll();
                    object assessee = assbll.GetAssesseebyId(AssesseeID);
                    Object.AdditionalData = new { Assessee = assessee };
                }

                jc.Data = Object;
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = "Payment Details found";
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Message = ex.Message;
            }
            js.Data = jc;
            return js;
        }
    }
}