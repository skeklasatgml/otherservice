﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using static Valuation_Board.Models.Application.ScheduleThree;

namespace Valuation_Board.Controllers.Shared
{
    
    public class OTPServerController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        // GET: OTPServer
        [HttpPost]
        public JsonResult SenOtp(string MobileNo)
        {
            var idenitfire = "";
            if ((SessionContext.CurrentUser as IUserType).UserType == UserType.CitizenUser)
            {
                idenitfire=(SessionContext.CurrentUser as CitizenUser).AssesseeNo;
            }
            else
            {
                idenitfire=(SessionContext.CurrentUser as IInternalUserMin).UserID.ToString();
            }
            OtpServer otp = new OtpServer(idenitfire);
            try
            {
                if (string.IsNullOrEmpty(MobileNo))
                {
                    throw new NullReferenceException("Please provide mobile no");
                }
              RequestOtp _sOtp=  otp.RequerstOTP(MobileNo);
                cr.Message = string.Format("Otp successfully sent to {0}, expired on: {1}", _sOtp.MobileNo, string.Format("{0:hh:mm:ss tt}", _sOtp.ExpiredOn.Value));
                cr.Status = ResponseStatus.SUCCESS;
                cr.Data = _sOtp;
            }
            catch(Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}