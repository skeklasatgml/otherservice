﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Shared
{[SecuredFilter]
    public class DistrictController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr=new ClientJsonResult ();
        DistrictBLL dstBll = new DistrictBLL();
        #endregion
        [HttpPost]
        public JsonResult GetDistricts()
        {
            List<District_Ddl_VM> dstVm = new List<District_Ddl_VM>();
            try
            {
               List<District> lstDst= dstBll.GetAllDistricts();
                 dstVm = lstDst.Select(t =>
                {
                    return new District_Ddl_VM() {
                        DistrictID=t.DistrictID??0,
                        DistrictName=t.DistrictName
                    };
                }).ToList();
                cr.Data = dstVm;
                cr.Message = string.Format("{0} Districts Found",dstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex){
                cr.Data = dstVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            //js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
    }
}