﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;

namespace Valuation_Board.Controllers.Shared
{
    public class KnowurPropController : Controller
    {
        ClientJsonResult c = new ClientJsonResult();
        KnowurProp_Bll bll = new KnowurProp_Bll();
        public ActionResult Index()
        {
            return View("~/Views/Shared/KnowurProp/index.cshtml");
        }
        [HttpPost]
        public JsonResult FetchInfo(Payload payload)
        {
            try {
                if (string.Equals(payload.SearchBy, "pl", StringComparison.OrdinalIgnoreCase))
                {
                    var x = bll.SearchByPlot(payload.NidCode,payload.PlotOrKhatNo);
                    c.Data = x;
                    c.Message = "Plot Information";
                }
                else
                {
                    var x = bll.SearchByKhatian(payload.NidCode, payload.PlotOrKhatNo);
                    c.Data = x;
                    c.Message = "Khatian Information";
                }
                c.Status = ResponseStatus.SUCCESS;
                
            } catch(Exception ex) {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult GetDistrictsNic()
        {
            try
            {
                var lst= bll.GetDisticts(); 
                c.Data = lst;
                c.Message = string.Format("{0} results found", lst.Count);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = new List<string>();
                c.Message = string.Format(ex.Message);
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult GetBlockNic(string distCode)
        {
            try
            {
                var lst = bll.GetBlocks(distCode);
                c.Data = lst;
                c.Message = string.Format("{0} results found", lst.Count);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = new List<string>();
                c.Message = string.Format(ex.Message);
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult GetMouzaNic(string distCode,string blockCode)
        {
            try
            {
                var lst = bll.GetMouzas(distCode,blockCode);
                c.Data = lst;
                c.Message = string.Format("{0} results found", lst.Count);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = new List<string>();
                c.Message = string.Format(ex.Message);
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult SearchService_ForName(string dist_code, string ro_code, string deed_no, string deed_year)
        {
            try
            {
                var lst = bll.SearchService_ForName(dist_code, ro_code,deed_no, deed_year);
                c.Data = lst;
                //c.Message = string.Format("{0} results found", lst.Count);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = string.Format(ex.Message);
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        public class Payload
        {
            public string NidCode { get; set; }
            public string SearchBy { get; set; }
            public string PlotOrKhatNo { get; set; }
            public string dcode { get; set; }
            public string bcode { get; set; }
            public string mouzacode { get; set; }
        }

    }
}