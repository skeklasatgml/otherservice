﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.PGI.Gateways;
using Valuation_Board.PGI.Gateways.HDFC;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.Controllers.Online
{
    //[SecuredFilter]
    public class onlinepaymentController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult jc = new ClientJsonResult();
        private IEncryptDecrypt chiperAlgorithm;
        string modcode = "";
        #endregion

        public onlinepaymentController()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);

            var modulsubmodule = SessionContext.CurrentModue as Module;
            if (modulsubmodule != null)
            {
                modcode = modulsubmodule.SubModuleCode;

            }
            ViewBag.SubModuleCode = modcode;
        }
        // GET: online
        public ActionResult OnlineTaxPayment()
        {
            try
            {
                string queryStringParam = Request.QueryString["Data"];
                if (string.IsNullOrEmpty(queryStringParam))
                {
                    throw new Exception("Invalid Request! Data Malformed! Assessee could not be loaded");
                }
                else
                {
                    string htmlDecodedChiperText = HttpUtility.UrlDecode(queryStringParam).Replace(" ", "+");//plush need to replaced after decode string to get actuall encoded value
                                                                                                             // string htmlDecodedChiperText = Convert.ToString(Request.QueryString["Params"]);

                    string pramJSON = chiperAlgorithm.Decrypt(htmlDecodedChiperText);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    CitizenUser user = js.Deserialize<CitizenUser>(pramJSON);
                    AssesseeBll assesseeBll = new AssesseeBll();
                    List<Assessee> lstObj = assesseeBll.GetAssessee(user.AssesseeNo);
                    if (lstObj.Count > 0)
                    {
                        Assessee assessee = lstObj[0];
                        assesseeBll.IncludeLocation(assessee);
                        assesseeBll.IncludeMunicipality(assessee);
                        assesseeBll.IncludeWard(assessee); 

                        PaymentGateway_Bll bll = new PaymentGateway_Bll();
                        List<PaymentGatewayMunicipality> lst = bll.GetPGsByMunicipalityId(assessee.MunicipalityID ?? 0);
                        var lstPgs = lst.Where(t => t.IsActive == true).Select(f =>
                        {
                            PaymentGateway p = new PaymentGateway();
                            p.PG_Code = f.PG_Code;
                            p.PG_Name = f.PG_Name;
                            p.Description = f.Description;
                            return p;
                        }).ToDictionary(f => f.PG_Code, f => f.PG_Name);
                        PropertyTaxCollectionBLL pbll = new PropertyTaxCollectionBLL();
                        List< PaymentSummary> lstLastPayments= pbll.GetLastPayments(5, assessee.AssesseeID);


                        ViewBag.Assessee = assessee;
                        ViewBag.PaymentGetways = lstPgs;
                        ViewBag.Last5Payments = lstLastPayments;

                        return View("~/Views/online/onlinepayment/OnlineTaxPayment.cshtml");
                    }
                    else
                    {
                        throw new Exception(string.Format("Assessee not found. {0}", Server.HtmlDecode("Click <a href='/Home/Index'>Here</a> to go back")));
                    }
                }
               

            }
            catch (FormatException)
            {
                ModelState.AddModelError("Error", "Parameters Malformed: paramater Not Readable");
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("Error", ex.Message);
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }

        }

        [HttpPost]
        public JsonResult GetPaymentGateWays(int MunicipalityID)
        {
            try
            {
                PaymentGateway_Bll bll = new PaymentGateway_Bll();
                List<PaymentGatewayMunicipality> lst = bll.GetPGsByMunicipalityId(MunicipalityID);
                var lstPgs = lst.Where(t => t.IsActive == true).Select(f =>
                {
                    PaymentGateway p = new PaymentGateway();
                    p.PG_Code = f.PG_Code;
                    p.PG_Name = f.PG_Name;
                    p.Description = f.Description;
                    return p;
                });
                jc.Data = lstPgs;
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = string.Format("{0} Payment gatways found");
            }
            catch (Exception ex)
            {
                jc.Message = ex.Message;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Data = new List<PaymentGateway>();
            }
            return Json(jc);
        }

        [HttpPost]
        public JsonResult Get_PropertyTaxPaymentDetails(int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters)
        {
            try
            {
                var request = Request.Params;
                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                PaymentProcessor payment = new PaymentProcessor();
                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                Object = payment.SearchOutStanding(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters,null,null, AssesseeID);

                jc.Data = Object;
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = "Payment Details found";
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Message = ex.Message;
            }
            js.Data = jc;
            return js;
        }

        #region  Payment section
        [HttpPost]
        public JsonResult MakePaymentTemp(string Mobile,string Email,string PG, int MunicipalityID, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters)
        {
            //temporary payment, with flag trunsaction not successfull
            //PaymentDate statnds for collection date
            try
            {
                AssesseeBll bll = new AssesseeBll();
                try
                {
                    bll.UpdateAssesseeEmailID( AssesseeID, Email.Trim());
                    bll.UpdateAssesseeMobileNo(AssesseeID, Mobile.Trim());
                }
                catch { }
                
                Assessee assesseeObj = bll.GetAssesseebyId(AssesseeID);
                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                PaymentProcessor payment = new PaymentProcessor();
                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t => {
                    payment.AddOtherAdjustment(t);
                });

                Object = payment.Calculate(MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters,null,null,AssesseeID);
                //Object.propertyTaxPaymentMaster.CollectorID = (int)mObj.UserID;
                Object.propertyTaxPaymentMaster.InsertedBy = null;
                Object.propertyTaxPaymentMaster.InsertedOn = DateTime.Now;
                Object.propertyTaxPaymentMaster.PaymentType = "O";
                Object.propertyTaxPaymentMaster.CollectorID = null;
                Object.propertyTaxPaymentMaster.TransactionID = PG_Model.GenTransactionID(assesseeObj.AssesseeNo);

                
                //reassign paid amount
                Object.propertyTaxPaymentMaster.PaidAmount = Object.propertyTaxPaymentMaster.NetAmount;


                long? paymentID = payment.MakePaymentOnline_Temp(Object.propertyTaxPaymentMaster, TaxPaymentCheckParameters, PG);
                if (paymentID == null)
                {
                    throw new Exception("Unknown Error! Transaction canceled");
                }
                else
                {
                    PropertyTaxCollectionBLL pbll = new PropertyTaxCollectionBLL();
                    PropertyTaxPaymentMaster obj = pbll.GetPaymentMasterByPaymentID(paymentID ?? 0);
                    if (obj == null)
                    {
                        throw new Exception("PropertyTaxPaymentMaster not found by payment id");
                    }
                    JavaScriptSerializer js = new JavaScriptSerializer();

                    //Generate response url like [http://localhost/Online/onlinepayment/RedirectToPG?Data={TransactionID:xxxxxxxx,PG:xx}]
                    string redirectUrl = string.Format("/Online/onlinepayment/RedirectToPG?Data={0}", chiperAlgorithm.Encrypt(js.Serialize(new { TransactionID = obj.TransactionID, PG = PG })));
                    jc.Data = new { RedirectTo = redirectUrl };
                }
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = "paid successfully";
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Message = ex.Message;
            }
            js.Data = jc;
            return js;
        }

        //use any getway
        public ActionResult RedirectToPG()
        {
            try
            {
                string queryString = Request.QueryString["Data"];
                if (string.IsNullOrEmpty(queryString))
                {
                    throw new Exception("Invalid Request! Data Malformed!");
                }
                else
                {
                    string htmlDecodedChiperText = HttpUtility.UrlDecode(queryString).Replace(" ", "+");//plush need to replaced after decode string to get actuall encoded value
                    // string htmlDecodedChiperText = Convert.ToString(Request.QueryString["Params"]);
                    string pramJSON = chiperAlgorithm.Decrypt(htmlDecodedChiperText);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    Dictionary<string, string> datas = js.Deserialize<Dictionary<string, string>>(pramJSON);

                    string transactionID = datas["TransactionID"];
                    string PG = datas["PG"];

                    PG_BLL bll = new PG_BLL();

                    PG_Model pgObj = bll.Get_PGModel(transactionID);
                    
                    if (pgObj != null)
                    {
                        pgObj.PG_Code = PG;
                        switch ( PG)
                        {
                            case "BD":
                                {
                                    Bildesk pgGateway = new Bildesk();
                                    pgGateway.DoRedirectionToPaymentGateway(pgObj);
                                    break;
                                }
                            case "HC":
                            case "HF":
                                {
                                    HDFC hdfc = new HDFC();
                                    hdfc.DoRedirectionToPaymentGateway(pgObj);
                                    break;
                                }
                            default:
                                {
                                    throw new NullReferenceException("Invalid payment getway");
                                }
                    }
                    }
                    else
                    {
                        throw new NullReferenceException("Data not found for online payment");
                    }
                }
            }
            catch (FormatException)
            {
                ModelState.AddModelError("Error", "Data not Readable! Data malformed");
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }
            catch (NullReferenceException ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }

            return new EmptyResult();

        }

        public ActionResult RedirectToPGForOtherServices()
        {
            try
            {
                string queryString = Request.QueryString["Data"];
                if (string.IsNullOrEmpty(queryString))
                {
                    throw new Exception("Invalid Request! Data Malformed!");
                }
                else
                {
                    string htmlDecodedChiperText = HttpUtility.UrlDecode(queryString).Replace(" ", "+");//plush need to replaced after decode string to get actuall encoded value
                    string pramJSON = chiperAlgorithm.Decrypt(htmlDecodedChiperText);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    Dictionary<string, string> datas = js.Deserialize<Dictionary<string, string>>(pramJSON);

                    string transactionID = datas["TransactionID"];
                    string serviceID = datas["ServiceID"];
                    string PG = datas["PG"];

                    MakePayment_Bll payBll = new MakePayment_Bll();

                    PG_ModelOtherServices pgObj = payBll.Get_PGModel(transactionID, serviceID);

                    if (pgObj != null)
                    {
                        pgObj.PG_Code = PG;
                        switch (PG)
                        {
                            case "BD":
                                {
                                    BildeskOtherService pgGateway = new BildeskOtherService();
                                    pgGateway.DoRedirectionToPaymentGateway(pgObj);
                                    break;
                                }
                            default:
                                {
                                    throw new NullReferenceException("Invalid payment getway");
                                }
                        }
                    }
                    else
                    {
                        throw new NullReferenceException("Data not found for online payment");
                    }
                }
            }
            catch (FormatException)
            {
                ModelState.AddModelError("Error", "Data not Readable! Data malformed");
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }
            catch (NullReferenceException ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("~/Views/online/onlinepayment/AssesseeLodingError.cshtml");
            }

            return new EmptyResult();

        }

        [HttpPost]//for bidesk
        public RedirectResult paymentresponse()
        {
            //http://holdingtax.co.in/online/onlinepayment/paymentresponse
            //response from payment gateway
            #region Test Code
            try
            {
                Bildesk bldsk = new Bildesk();
                return Redirect(bldsk.SaveResponseNRedirect());
            }
            catch (SqlException ex)
            {
                //return RedirectToAction("PaymentError", new { Message = chiperAlgorithm.Encrypt("Database error. " + ex.Message) });
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt("Database error. " + ex.Message))));
            }
            catch (InvalidOperationException ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt( ex.Message))));

            }
            catch (Exception ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt( ex.Message))));

            }
            #endregion
        }
        [HttpPost]//for hdfc
        public RedirectResult paymentresponseHDFC()
        {
            //http://holdingtax.co.in/online/onlinepayment/paymentresponse
            //response from payment gateway
            #region Test Code
            try
            {
                HDFC bldsk = new HDFC();
                return Redirect(bldsk.SaveResponseNRedirect());
            }
            catch (SqlException ex)
            {
                //return RedirectToAction("PaymentError", new { Message = chiperAlgorithm.Encrypt("Database error. " + ex.Message) });
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt("Database error. " + ex.Message))));
            }
            catch (InvalidOperationException ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            catch (Exception ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            #endregion
        }
        [HttpPost]//hdfc cc avenew
        public RedirectResult paymentresponseHDFC_CCAVNW()
        {
            //http://holdingtax.co.in/online/onlinepayment/paymentresponse
            //response from payment gateway
            #region Test Code
            try
            {
                HDFC bldsk = new HDFC();
                return Redirect(bldsk.SaveResponseNRedirect_CCAVNW());
            }
            catch (SqlException ex)
            {
                //return RedirectToAction("PaymentError", new { Message = chiperAlgorithm.Encrypt("Database error. " + ex.Message) });
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt("Database error. " + ex.Message))));
            }
            catch (InvalidOperationException ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            catch (Exception ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            #endregion
        }

        [HttpPost]//for bidesk
        public RedirectResult paymentresponseBillDeskOtherServices()
        {
            //http://holdingtax.co.in/online/onlinepayment/paymentresponse
            //response from payment gateway
            #region Test Code
            try
            {
                BildeskOtherService bldsk = new BildeskOtherService();
                return Redirect(bldsk.SaveResponseNRedirect());
            }
            catch (SqlException ex)
            {
                //return RedirectToAction("PaymentError", new { Message = chiperAlgorithm.Encrypt("Database error. " + ex.Message) });
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt("Database error. " + ex.Message))));
            }
            catch (InvalidOperationException ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            catch (Exception ex)
            {
                return Redirect(string.Format("/online/onlinepayment/PaymentError?Message={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(ex.Message))));

            }
            #endregion
        }

        public ActionResult PaymentSuccessfull()
        {
            return View("~/Views/online/onlinepayment/PaymentSuccessfull.cshtml");
        }
        public ActionResult PaymentSuccessfull_OtherServices()
        {
            return View("~/Views/online/onlinepayment/PaymentSuccessfull_OtherServices.cshtml");
        }
        public ActionResult PaymentError()
        {
            return View("~/Views/online/onlinepayment/PaymentError.cshtml");
        }
        public ActionResult PaymentCancel()
        {
            HDFC hDFC = new HDFC();
            string msg = "";
            try
            {
                hDFC.SaveResponseErrorResponseManuallyCCAvenue(Request.Form["orderNo"], Request.Form["encResp"]);
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }
            ViewBag.error = msg;
            return View("~/Views/online/onlinepayment/CancelPayment.cshtml");
        }
        #endregion
    }
}