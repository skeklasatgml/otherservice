﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using static Valuation_Board.ViewModels.Assessment_VM;
using static Valuation_Board.ViewModels.Assessment_VM.AssessmentForm;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class AssessmentController : Controller
    {
        ClientJsonResult _cr = new ClientJsonResult();
        #region assessment form entry
        #region Container UIs
        public ActionResult GetAssessmentContainer(string ValTag, string SubAction)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);
                    ViewBag.ValTag = ValTag;
                    ViewBag.SubAction = SubAction;
                    return View(@"~\Views\Municipality\Assessment\AssessmentContainer.cshtml", bll.GetAssessmentTypes("M"));
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }

        [NonAction]
        public ActionResult GetAssessmentForm(string Valtag, string SubAction)
        {
            Assessment_VM.AssessmentForm obj = null;
            PartialViewResult view = null;
            using (AssessmentBll bll = new AssessmentBll())
            {

                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    switch (Valtag.ToUpper())
                    {
                        case "L":
                            {
                                if (string.IsNullOrEmpty(SubAction) || SubAction == "HL")
                                {
                                    obj = bll.GetNewForm(mnId, Valtag, SubAction);
                                    view = PartialView(@"~\Views\Municipality\Assessment\NewHolding\NewHoldingForm.cshtml", obj);
                                }
                                else if (SubAction == "OT")
                                {
                                    throw new NotImplementedException("Operation not implemented");
                                }
                                break;
                            }
                        case "R":
                        case "I":
                            {
                                obj = bll.GetNewForm(mnId, Valtag, null);
                                view = PartialView(@"~\Views\Municipality\Assessment\Review Improvement\ReviewInprovmentContainer.cshtml", obj);

                                break;
                            }
                        case "M":
                            {
                                obj = bll.GetNewForm(mnId, Valtag, null);
                                view = PartialView(@"~\Views\Municipality\Assessment\Separation\SeparationContainer.cshtml", obj);

                                break;
                            }
                        case "A":
                            {
                                obj = bll.GetNewForm(mnId, Valtag, null);
                                view = PartialView(@"~\Views\Municipality\Assessment\Amalgamation\Container.cshtml", obj);

                                break;
                            }
                        case "N":
                            {
                                obj = bll.GetNewForm(mnId, Valtag, null);
                                view = PartialView(@"~\Views\Municipality\Assessment\ChangeOfOwner\Container.cshtml", obj);

                                break;
                            }
                        case "S":
                            {
                                obj = bll.GetNewForm(mnId, Valtag, null);
                                view = PartialView(@"~\Views\Municipality\Assessment\StrikeOff\Container.cshtml", obj);

                                break;
                            }
                        default:
                            {
                                throw new Exception("Invalid Asssessment Type Requested");
                            }
                    }
                    bll.Dispose();

                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    view = PartialView(SiteConstants.ErrorViewPath, appExp);
                }
            }
            return view;
        }
        #endregion

        [HttpPost]
        public JsonResult CalCulateValuationDetails(Assessment_VM.AssessmentForm.ValuationDetails ValuationDetails)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                        var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                        var obj = bll.CalculateValuation(ValuationDetails, mnId);
                        _cr.Data = obj;
                        _cr.Message = "value caluated";
                        _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
            }

            return Json(_cr);
        }
        [HttpPost]
        public JsonResult GetHoldingTypes(int? HoldingGroupTypeId, int? MunicipalityID = null)
        {
            int mnId;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    if (MunicipalityID == null)
                         mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID; 
                    else
                         mnId= MunicipalityID.Value;

                        var obj = bll.GetHoldingTypes(HoldingGroupTypeId, mnId);
                        _cr.Data = obj;
                        _cr.Message = "value caluated";
                        _cr.Status = ResponseStatus.SUCCESS;
                }
            catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult GetWards(string Ward)
        {
            try
            {
                var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                var wards = new WardBLL().GetWards(mnId, Ward);
                _cr.Data = wards;
                _cr.Message = string.Format("{0} wards found", wards.Count());
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        [HttpPost]//autocomplete suggetion
        public JsonResult GetLocations(string Location, int WardId)
        {
            try
            {
                var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                var locations = new LocationBLL().GetLocations(Location, mnId, WardId);
                _cr.Data = locations;
                _cr.Message = string.Format("{0} locations found", locations.Count());
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult GetHoldings(string Holding, int WardId, int LocationID)
        {

            try
            {
                var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                var assessees = new AssesseeBll().SearchHolding(mnId, WardId, LocationID, Holding);
                _cr.Data = assessees;
                _cr.Message = string.Format("{0} Assessees found", assessees.Count());
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]
        public JsonResult SaveAssessmentData(Assessment_VM.AssessmentForm AssessmentForm)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {

                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);
                    bool result = false;
                    if (AssessmentForm.AssessmentValTag == "L")
                    {
                        result = bll.AddNewHolding(AssessmentForm, mnObj.MunicipalityID, mnObj.UserID);
                        if (!result)
                        {
                            throw new InvalidOperationException("Assessment data not saved");
                        }
                    }
                    _cr.Data = result;
                    _cr.Message = "Data save successfully";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region Approval
        public ActionResult AssessementApprovalContainer()
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                var formBase = new Assessment_VM.AssmtApprovalBase();
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    formBase.AssessmentTypes = bll.GetAssessmentTypes("M");
                    formBase.YearId = bll.GetMaxYear(mnObj.MunicipalityID);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                }
                return View(@"~\Views\Municipality\Assessment\Approval\Container.cshtml", formBase);
            }
        }
        [HttpPost]
        public ActionResult GetApprovalForm(string ValTag, int YearID)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                Assessment_VM.AssmtApprovalForm obj = new Assessment_VM.AssmtApprovalForm();
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    obj.AssmtTransactions = bll.GetUnApprovedList(ValTag, YearID, mnObj.MunicipalityID);
                    obj.ValTag = ValTag;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                }
                return View(@"~\Views\Municipality\Assessment\Approval\ApprovalForm.cshtml", obj);
            }
        }
        [HttpPost]
        public ActionResult Approve(List<ApprovalPayload> Approvals)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    bll.Approve(Approvals ?? new List<ApprovalPayload>(), mnObj.MunicipalityID, mnObj.UserID);
                    return View(SiteConstants.SuccessViewPath, model: "Selected List Successfully Approved");
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }
        #endregion

        #region review and improvment
        public ActionResult GetReviewOrImprovementForm(string ValTag, int wardId, int LocationId, string HoldingNo)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    obj = bll.GetAssessmtFormData(mnId, wardId, LocationId, HoldingNo);
                    if (obj == null)
                    {
                        throw new NullReferenceException("Details not found");
                    }
                    obj.setValTag(ValTag);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                }
                return View(@"~\Views\Municipality\Assessment\Review Improvement\ReviewImprovmentForm.cshtml", obj);
            }
        }
        [HttpPost]
        public JsonResult GetValuationDetailsJsonData(string ValTag, int wardId, int LocationId, string HoldingNo, bool IsTemp = true)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    var data = bll.GetValuationDetailsData(mnId, wardId, LocationId, HoldingNo, IsTemp);
                    _cr.Data = data;
                    _cr.Message = "Valuation Details Found";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = "Valuation details not fund";
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult GetOtherTabDetails(string ValTag, int wardId, int LocationId, string HoldingNo)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    var data = bll.GetAssessmtFormOtherTabData(mnId, wardId, LocationId, HoldingNo);
                    _cr.Data = data;
                    _cr.Message = "Valuation Details Found";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = "Valuation details not fund";
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        public JsonResult SaveReviewOrImporvement(Assessment_VM.AssessmentForm AssessmentForm)
        {
            int refNo = 0;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    if (AssessmentForm == null)
                    {
                        throw new NullReferenceException("Request payload can not be null");
                    }
                    if (AssessmentForm._ValuationDetails == null)
                    {
                        throw new NullReferenceException("Valuation details can not be null");
                    }
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    refNo = bll.SaveReviewOrImprovement(AssessmentForm, mnObj.MunicipalityID, mnObj.UserID);
                    if (refNo == 0)
                    {
                        throw new InvalidOperationException("Record not saved. Unknown error");
                    }
                    _cr.Data = null;
                    _cr.Message = string.Format("Record saved successfully. Ref no- {0}", refNo);
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region separation
        public ActionResult SearchHoldingOldData(string ValTag, int wardId, int LocationId, string HoldingNo)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    obj = bll.GetAssessmtFormData(mnId, wardId, LocationId, HoldingNo);
                    if (obj == null)
                    {
                        throw new NullReferenceException("Details not found");
                    }
                    obj.setValTag(ValTag);
                    return View(@"~\Views\Municipality\Assessment\Separation\SearchHoldingSeparation.cshtml", obj);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }
        [HttpPost]
        public ActionResult GetSeparationNewForm(string ValTag, int wardId, int LocationId, string HoldingNo)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    obj = bll.GetAssessmtFormData(mnId, wardId, LocationId, HoldingNo);
                    // obj._ValuationDetails = new AssessmentForm.ValuationDetails();
                    obj._ValuationDetails = new List<AssessmentForm.ValuationDetails>();
                    if (obj == null)
                    {
                        throw new NullReferenceException("Details not found");
                    }
                    obj.setValTag("L");
                    return View(@"~\Views\Municipality\Assessment\Separation\NewForm.cshtml", obj);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }
        [HttpPost]
        //public JsonResult SaveSeparationData(AssmtTransactionModel ExistingHolding, List<AssmtTransactionModel> NewHoldings)
        public JsonResult SaveSeparationData(AssmtTransactionWithValuationList ExistingHolding, List<AssmtTransactionWithValuationList> NewHoldings)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnobj = (SessionContext.CurrentUser as MunicipalityUser);

                    bll.SaveSeparation(ExistingHolding, NewHoldings, mnobj.MunicipalityID, mnobj.UserID);
                    _cr.Data = null;
                    _cr.Message = "Data Saved Succesfully";
                    _cr.Status = ResponseStatus.SUCCESS;

                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred=true;
                    _cr.Data = null;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region amalgamation
        [HttpPost]
        public ActionResult SearchHolding(string Holding, int WardId, int LocationID)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {

                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    var holdingData = bll.GetAssessmtFormData(mnId, WardId, LocationID, Holding);
                    _cr.Data = holdingData ?? throw new NullReferenceException("Hoding not found");
                    _cr.Message = string.Format("Holding found found");
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult SaveAmalgamationData(List<AssmtTransactionModel> ChildHoldings, AssmtTransactionModel MohtherHolding, List<ValuationDetails> MotherHoldingValuationDetails)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnobj = (SessionContext.CurrentUser as MunicipalityUser);

                    bll.SaveAmalgamationData(ChildHoldings, MohtherHolding, MotherHoldingValuationDetails, mnobj.MunicipalityID, mnobj.UserID);
                    _cr.Data = null;
                    _cr.Message = "Data Saved Succesfully";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region name changes
        [HttpPost]
        public JsonResult SaveNameChanges(AssmtTransactionModel ExistingHolding, List<Owner> Names)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnobj = (SessionContext.CurrentUser as MunicipalityUser);

                    bll.SaveNameChange(ExistingHolding, Names, mnobj.MunicipalityID, mnobj.UserID);
                    _cr.Data = null;
                    _cr.Message = "Data Saved Succesfully";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region strike off
        public ActionResult GetStrikeOffForm(string ValTag, int wardId, int LocationId, string HoldingNo)
        {
            Assessment_VM.AssessmentForm obj = null;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;

                    obj = bll.GetAssessmtFormData(mnId, wardId, LocationId, HoldingNo);
                    if (obj == null)
                    {
                        throw new NullReferenceException("Details not found");
                    }
                    obj.setValTag(ValTag);
                    return View(@"~\Views\Municipality\Assessment\StrikeOff\Form.cshtml", obj);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }
        public JsonResult SaveStrikeOffData(AssmtTransactionModel SelectedHolding)
        {

            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnobj = (SessionContext.CurrentUser as MunicipalityUser);

                    bll.SaveStrikeOffData(SelectedHolding, mnobj.MunicipalityID, mnobj.UserID);
                    _cr.Data = null;
                    _cr.Message = "Data Saved Succesfully";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Data = null;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
                return Json(_cr);
            }
        }
        #endregion

        #region new assessee entry
        public ActionResult GetAssesseUpdationContainer()
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    var data = bll.GetNewForm(mnObj.MunicipalityID, "UA", "");
                    return View(@"~\Views\Municipality\Assessment\AssesseeUpdation\AssesseeUpdationContainer.cshtml", data);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = ex.Message;
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }
        }
        public ActionResult GetAssesseeUpdationForm(int WardId,int LocationId, string HoldingNo)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnObj = (SessionContext.CurrentUser as MunicipalityUser);

                    var data = bll.GetAssessmtFormData(mnObj.MunicipalityID, WardId, LocationId, HoldingNo);
                    if (data == null)
                    {
                        throw new NullReferenceException("Assessee not found");
                    }
                    data.setValTag("UA");
                    return View(@"~\Views\Municipality\Assessment\AssesseeUpdation\AssesseeUpdationForm.cshtml", data);
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = ex.Message;
                    return View(SiteConstants.ErrorViewPath, appExp);
                }
            }

        }
        #endregion

        public ActionResult MutationForm(int? TabId = null)
        {
            AssessmentBll bll = new AssessmentBll();
            Valuation_Board.Models.Application.MutationForm mutation = new Valuation_Board.Models.Application.MutationForm();
            if (TabId != null)
            {
                var MutationList = bll.GetMutationList(TabId);
                mutation = MutationList[0];
            }
            return View(@"~\Views\Municipality\Assessment\MutationForm.cshtml", mutation);
        }
        public ActionResult MutationList()
        {
            AssessmentBll bll = new AssessmentBll();
            var MutationList = bll.GetMutationList();
            return View(@"~\Views\Municipality\Assessment\MutationList.cshtml", MutationList);
        }
        public JsonResult SaveMutationForm(MutationForm data)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    bll.SaveMutationForm(data, mnId);
                    _cr.Status = ResponseStatus.SUCCESS;
                    _cr.Message = "Save successfull..";
                }
                catch (Exception ex)
                {
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Message = ex.Message;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult GetMutationType(int? MutationTypeId)
        {
            try
            {
                AssessmentBll bll = new AssessmentBll();
                _cr.Data = bll.GetMutationType();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "Save successfull..";
            }
            catch (Exception ex)
            {
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            return Json(_cr);
        }
        [HttpPost]
        public JsonResult GetIdProofType(int? IdProofId)
        {
            try
            {
                AssessmentBll bll = new AssessmentBll();
                _cr.Data = bll.GetIdProofType();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "Save successfull..";
            }
            catch (Exception ex)
            {
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            return Json(_cr);
        }
        [HttpPost]
        public JsonResult GetBllock(int? BlockId, int? MunicipalityID = null)
        {
            int mnId;
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    if (MunicipalityID == null)
                        mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    else
                        mnId = MunicipalityID.Value;

                    var obj = bll.GetBlocks(mnId);
                    _cr.Data = obj;
                    _cr.Message = "value caluated";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult GetDevolvedType(int? DevolvedTypeId)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var obj = bll.GetDevolvedType();
                    _cr.Data = obj;
                    _cr.Message = "value caluated";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult UpdateStatus(int? TabId, string Status)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    bll.UpdateStatus(TabId, Status, mnId);
                    _cr.Status = ResponseStatus.SUCCESS;
                    _cr.Message = "Update successfull..";
                }
                catch (Exception ex)
                {
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Message = ex.Message;
                }
                return Json(_cr);
            }
        }
        [HttpPost]
        public JsonResult GetMouzas(int? BlockCode)
        {
            using (AssessmentBll bll = new AssessmentBll())
            {
                try
                {
                    var mnId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    var obj = bll.GetMouzaByBcode(BlockCode, mnId);
                    _cr.Data = obj;
                    _cr.Message = "Mouza Found";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
                catch (Exception ex)
                {
                    bll.IsExceptionOcurred = true;
                    _cr.Message = ex.Message;
                    _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    _cr.Data = null;
                }
                return Json(_cr);
            }
        }

    }

}