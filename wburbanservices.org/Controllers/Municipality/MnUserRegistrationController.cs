﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.Models.Account;
using Valuation_Board.App_Codes;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnUserRegistrationController : Controller
    {
        // GET: MnUserRegistration
        MunicipalityUserBll obj_bll = new MunicipalityUserBll();
        UserCategory_BLL userCategoryBll = new UserCategory_BLL();
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Insert_User(MunicipalityUser obj, string Password)
        {
            MunicipalityUser obj2 = (MunicipalityUser)SessionContext.CurrentUser;
            JsonResult js = new JsonResult();
            try
            {
                obj.MunicipalityID = Convert.ToInt32(obj2.MunicipalityID);
                obj_bll.Insert_User(obj, Password);

                js.Data = new ClientJsonResult()
                {
                    Data = true,
                    Message = "Insert Succesfull",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            return js;
        }

        [HttpGet]
        public JsonResult Get_UserType()
        {
            JsonResult js = new JsonResult();
            try
            {              
             List<UserCategory> lst = userCategoryBll.Get_UserType();

                js.Data = new ClientJsonResult()
                {
                    Data = lst,
                    Message = "",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = new List<UserCategory>(),
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
    }
}