﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class AdvanceAdjustmentController : Controller
    {
        ClientJsonResult jc = new ClientJsonResult();
        
        public ActionResult Index(int? WardId,int? LocationId ,long? AssesseeId ,string AdvanceDate)
        {
            AdvanceAdjustmentPage_VM obj = new AdvanceAdjustmentPage_VM();
            if (WardId!=null && LocationId!=null && AssesseeId!=null && AdvanceDate!=null)
            {
                try//avoid throw exceptions in this block
                {
                    MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                    LocationBLL bll = new LocationBLL();
                    var locations = bll.GetLocationsIDRange(WardId.Value, mUser.MunicipalityID, new List<int> { LocationId.Value });
                    WardBLL wardBLL = new WardBLL();
                    var wards = wardBLL.GetWards(new List<int> { WardId.Value }, mUser.MunicipalityID);

                    var assessee = new AssesseeBll().GetAssesseebyId(AssesseeId.Value);
                    if (locations.Count > 0 && wards.Count > 0 && assessee != null && assessee.LocationID == LocationId.Value && assessee.WardID == WardId.Value && mUser.MunicipalityID == assessee.MunicipalityID)
                    {
                        obj = new AdvanceAdjustmentPage_VM
                        {
                            AdvanceDate = DateTime.ParseExact(AdvanceDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                            AssesseeName = assessee.AssesseeName,
                            AssesseeId = assessee.AssesseeID,
                            LocationId = locations[0].LocationID,
                            LocationName = locations[0].LocationName,
                            WardId = wards[0].WardID,
                            WardName = wards[0].WardName
                        };
                    }
                }
                catch { }
            }
            return View("~/Views/Municipality/AdvanceAdjustment/Index.cshtml", obj);
        }

        public ActionResult GetAdvanceData(string EffectFromDT, string EffectToDT, string WardID, string LocationID, string AssesseeID)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                AdvanceAdjustmentBLL assApp = new AdvanceAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                List<AdvanceAdjustment_VM> pDetails = assApp.Get_AllAdvanceData(uObj.MunicipalityID, EffectFromDT, EffectToDT, WardID, LocationID, AssesseeID);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} record found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<AdvanceAdjustment_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult GetAdvAdjDtlsData(long AdvPaymentID, int MunicipalityID, long AssesseeID)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                string msg = "";
                AdvanceAdjustmentBLL assApp = new AdvanceAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                List<AdvAdjustmentDtls_VM> pDetails = assApp.Get_AdvAdjData(AdvPaymentID, MunicipalityID, AssesseeID);
                cr.Data = pDetails;
                cr.Message = msg;
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult AdjustUnadjustedAdvance(long AdvPaymentID, int MunicipalityID, long AssesseeID, string ReceiptNo)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                string msg = "";
                AdvanceAdjustmentBLL assApp = new AdvanceAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                msg=assApp.UpdateAdjustAdvance(AdvPaymentID, MunicipalityID, AssesseeID, ReceiptNo, uObj.UserID);
                cr.Data = null;
                cr.Message = msg;
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult GetManualAdvAdjAvailabilityInfo(int WardID, int LocationID, long AssesseeID)
        {
            try
            {
                AdvanceAdjustmentBLL bll = new AdvanceAdjustmentBLL();
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                var manualAdjustmentAvailability = bll.GetManualAdvAdjAvailabilityInfo(mObj.MunicipalityID, WardID, LocationID, AssesseeID);
                jc.Data = new { ManualAdjAvailabity = manualAdjustmentAvailability, RedirectTo = string.Format("/Municipality/AdvanceAdjustment/Index?WardId={0}&LocationId={1}&AssesseeId={2}&AdvanceDate={3}",WardID,LocationID,AssesseeID, manualAdjustmentAvailability.AdvanceDate==null?"": manualAdjustmentAvailability.AdvanceDate.Value.ToString("dd'/'MM'/'yyyy")) };
                jc.Message = "Manual Adjustment Availability info rendered";
                jc.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                jc.Message = ex.Message;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(jc);
        }
    }

    
}