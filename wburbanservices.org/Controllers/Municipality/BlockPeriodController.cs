﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.ViewModels.assmt;

namespace Valuation_Board.Controllers.Municipality
{
    public class BlockPeriodController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        BlockPeriodBll bp = new BlockPeriodBll();
        // GET: BlockPeriod
        public ActionResult Index()
        {
            return View(@"~\Views\Municipality\Assessment\BlockPeriod\index.cshtml");
        }

        [HttpPost]
        public JsonResult checkFinYear(int finyear) {
            try
            {
                var mUser = SessionContext.CurrentUser as MunicipalityUser;
                var lst = bp.checkFinYear(mUser.MunicipalityID, finyear);
                cr.Data = lst;
                cr.Message = "Result Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveBlock(blockPeriod blockPeriod) {
            try
            {
                var mUser = SessionContext.CurrentUser as MunicipalityUser;
                blockPeriod.CurrentUserId = mUser.UserID;
                //blockPeriod.MunicipalityID = mUser.MunicipalityID;

                blockPeriod.MunicipalityID = blockPeriod.MunicipalityID ?? mUser.MunicipalityID;
                blockPeriod.YearId = blockPeriod.YearId ?? blockPeriod.YearId;

                bp.InsertBlock(blockPeriod);
                cr.Data = "";
                cr.Message = "Save Successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]

        public JsonResult GetAllBlockPeriod() {
            try
            {
                var lst = bp.GetAllBlockPeriod();
                cr.Data = lst;
                cr.Message = "";
                cr.Status= ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }



    }

}