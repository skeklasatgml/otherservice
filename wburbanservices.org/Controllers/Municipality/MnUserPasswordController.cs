﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.App_Codes.BLL;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnUserPasswordController : Controller
    {
        #region Globals
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MnChangePasswordBLL mnBll = new MnChangePasswordBLL();
        #endregion

        #region UIs
        public ActionResult Index()
        {
            return View("~/Views/Municipality/MnUserPassword/Index.cshtml");
        }
        public ActionResult ResetPasswordUI()
        {
            return View("~/Views/Municipality/MnUserPassword/ResetPassword.cshtml");
        }
        #endregion

        #region json response
        [HttpPost]
        public JsonResult ChkOldPwd(string Old, string New, string RePwd)
        {
           try
            {
                string msg = "";
                var mUser = (IInternalUserMin)SessionContext.CurrentUser;
                if (mUser.UserType == UserType.AdministrativeUser || mUser.UserType == UserType.MunicipalityUser)
                {
                    msg = mnBll.Insert(Old, New, RePwd, mUser.UserID);
                    cr.Data = null;
                    cr.Message = msg;
                    cr.Status = ResponseStatus.SUCCESS;
                }
                else
                {
                    throw new Exception("Current user type not supported this action");
                }
            }
           catch (Exception ex)
           {
               cr.Data = false;
               cr.Message = ex.Message;
               cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
           }
           js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult ResetPassword(long UserID)
        {
            try
            {
                MunicipalityUserBll objbll = new MunicipalityUserBll();
                string password = "";
                password = string.Join("", Guid.NewGuid().ToString("n").Take(8).Select(o => o));
                password = password.ToUpper();
                objbll.Update_Password(UserID, password);
                cr.Data = password;
                cr.Message = "Password Reset Successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        #endregion
    }
}
