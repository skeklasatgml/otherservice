﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.App_Codes;
using Valuation_Board.Models.Account;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnPaymentModeConfigurationController : Controller
    {
        //
        // GET: /MnPaymentModeConfiguration/
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MnPaymentModeCounterAssocBLL ObjMnPModeCounter = new MnPaymentModeCounterAssocBLL();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SavePaymentMode_Counter_Assocation(List<MNPaymentModesCounterAssociation> obj)
        {
            JsonResult js = new JsonResult();
            try
            {
                string Message="";
                ResponseStatus Status1 = 0;
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                //obj.RowId=0;
                var data = ObjMnPModeCounter.SavePaymentModeCounterAssociation(obj);
                if (data.RowId != 0)
                {
                    Message = "Details Save Successfully";
                    Status1= ResponseStatus.SUCCESS;
                }
                else
                {
                    Message = "Date Range alreadt exists for this Payment type and Counter";
                    Status1=ResponseStatus.INVALID_DATA;
                }
                js.Data = new ClientJsonResult()
                {
                    Data = data,
                    Message=Message,
                    Status = Status1
                };
                return js;
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = null,
                    Message = ex.Message,
                    Status = 0
                };
              return js;
            }

        }
        [HttpPost]
        public JsonResult GetAllCounterMunicipalityWise()
        {
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityPaymentCounter> lst = ObjMnPModeCounter.CountersByMunicipalityID(mUser.MunicipalityID);
                cr.Data = lst;
                cr.Message = string.Format("{0} Counters Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js; 
            
        }
        [HttpPost]
        public JsonResult GetOffLinePaymentModes()
        {

            List<PaymentMode> lstMct = ObjMnPModeCounter.GetPaymentModes();
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult()
            {
                Data = lstMct,
                Message = string.Format("{0} Payment Modes found", lstMct.Count),
                Status = ResponseStatus.SUCCESS
            };
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult GetPaymentModeCounterWise(int CounterId)
        {
            List<MNPaymentModesCounterAssociation> lstMct = ObjMnPModeCounter.GetPaymentModeCounterWise(CounterId);
            lstMct=ObjMnPModeCounter.IncludesMunicipality(lstMct);
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult()
            {
                Data = lstMct,
                Message = string.Format("{0} Payment Modes found", lstMct.Count),
                Status = ResponseStatus.SUCCESS
            };
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult UpdatePaymentMode_Counter_Assocation(List<MNPaymentModesCounterAssociation> obj)
        {
            string Message = "";
            ResponseStatus Status1 = 0;
            JsonResult js = new JsonResult();
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                //obj.RowId=0;
                var data = ObjMnPModeCounter.UpdatePaymentModeCounterAssociation(obj);
                if (data.RowId == 0)
                {
                    Message = "Details Updated Successfully";
                    Status1 = ResponseStatus.SUCCESS;
                }
                else
                {
                    Message = "Date Range alreadt exists for this Payment type and Counter";
                    Status1 = ResponseStatus.INVALID_DATA;
                }
                js.Data = new ClientJsonResult()
                {
                    Data = data,
                    Message = Message,
                    Status = Status1
                };
                return js;
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = null,
                    Message = ex.Message,
                    Status = 0
                };
                return js;
            }

        }

        
        [HttpPost]
        public JsonResult DeletePaymentMode_Counter_Assocation(int RowId)
        {
            int data = ObjMnPModeCounter.DeletePaymentModes(RowId);
            JsonResult js = new JsonResult();
            js.Data = new ClientJsonResult()
            {
                Data = data,
                Message = "Payment Modes Deleted",
                Status = ResponseStatus.SUCCESS
            };
            js.Data = cr;
            return js;
        }
    }
}
