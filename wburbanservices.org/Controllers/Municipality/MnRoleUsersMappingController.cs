﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.Models.Account;
using Valuation_Board.App_Codes;

namespace Valuation_Board.Controllers.Municipality
{[SecuredFilter]
    public class MnRoleUsersMappingController : Controller
    {
        // GET: MnRoleUsersMapping
        RoleUsersMappingBLL obj_bll = new RoleUsersMappingBLL();
        MunicipalityUser objMU = (MunicipalityUser)SessionContext.CurrentUser;
        JsonResult js = new JsonResult();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Insert(List<RoleUsersMapping> obj)
        {            
            try
            {              
                obj_bll.Insert(obj,Convert.ToInt32(objMU.MunicipalityID));
                js.Data = new ClientJsonResult()
                {
                    Data = null,
                    Message = "",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }          
            return js;
        }

        [HttpPost]      
        public JsonResult Get_UsersByRoleID(int RoleID)
        {           
            try
            {
                List<RoleUsersMapping> lst = new List<RoleUsersMapping>();
                lst = obj_bll.Get_UserByRoleID(RoleID,Convert.ToInt32(objMU.MunicipalityID));
                js.Data = new ClientJsonResult()
                {
                    Data = lst,
                    Message = "",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            return js;
        }
        [HttpGet]
        public JsonResult Get_Roles()
        {           
            try
            {
                List<RoleUsersMapping> lst = new List<RoleUsersMapping>();
                MunicipalityUser mnUser = (MunicipalityUser)SessionContext.CurrentUser;
                lst = obj_bll.Get_Roles((int)mnUser.UserType);
                js.Data = new ClientJsonResult()
                {
                    Data = lst,
                    Message = "",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }    
    }
}