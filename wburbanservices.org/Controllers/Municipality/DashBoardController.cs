﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class DashBoardController:Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        public ActionResult UI()
        {
            MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
            RoleBll rlBll = new RoleBll();
            List<Role> roles = rlBll.GetRoles(user.UserID);
            if (roles.FirstOrDefault(t => t.RoleId == 2 || t.RoleId == 5) != null)
            {
                return View(@"~\Views\Municipality\Dashboard\DashboardUI.cshtml");
            }
            else
            {
                //collector user
                return View(@"~\Views\Municipality\Dashboard\DashboardUI_Collector.cshtml");
            }
        }
        [HttpPost]
        public JsonResult CollectorWiseCollection(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.CollectorWiseCollection> lstVm = new DashBoard_Bll().CollectorWiseCollection(new List<int> {user.MunicipalityID }, FromDate, ToDate, null, null);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection2>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult WardWiseCollection(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.WardWiseCollection> lstVm = new DashBoard_Bll().WardWiseCollection(new List<int> { user.MunicipalityID }, FromDate, ToDate, null, null);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection2>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult AppOrigineWiseCollection(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new DashBoard_Bll().AppOrigineWiseCollection(new List<int> { user.MunicipalityID }, FromDate, ToDate);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection2>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetCollectionFlowTimeSpanAllCollector(DateTime? FromDate, DateTime? ToDate,string DataSheetType)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.CollectionFlowByTimeSpan> lstVm = new DashBoard_Bll().GetCollectionFlowTimeSpan(new List<int> { user.MunicipalityID }, FromDate, ToDate,null,null, DataSheetType);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection2>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetCollectionFlowTimeSpanCurCollector(DateTime? FromDate, DateTime? ToDate, string DataSheetType)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.CollectionFlowByTimeSpan> lstVm = new DashBoard_Bll().GetCollectionFlowTimeSpanPayheadWise(new List<int> { user.MunicipalityID }, FromDate, ToDate, user.UserID, null, DataSheetType);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection2>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }


        [HttpPost]
        public JsonResult DemandVsCollectionSummary()
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.DemandVsCollectionTotalSummary> lstVm = new DashBoard_Bll().DemandVsCollectionTotalSummaryBll(user.MunicipalityID);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.DemandVsCollectionTotalSummary>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult DemandVsCollectionWardWiseSummary()
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<Dashboard_vm.DemandVsCollectionTotalSummary> lstVm = new DashBoard_Bll().DemandVsCollectionWardWiseSummaryBll(user.MunicipalityID);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.DemandVsCollectionTotalSummary>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

    }
}