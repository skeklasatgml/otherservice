﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class AssessmentApprovalController : Controller
    {
        // GET: AssessmentApproval
        public ActionResult Index()
        {
            return View("~/Views/Municipality/AssessmentApproval/Index.cshtml");
        }

        public ActionResult GetAssessmentData(string EffectFromDT, string EffectToDT, string ApproveStatus)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                AssessmentApprovalBLL assApp = new AssessmentApprovalBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                List<AsessmentApproval_VM> pDetails = assApp.Get_AllAssessmentData(uObj.MunicipalityID, EffectFromDT, EffectToDT, ApproveStatus);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Payheas found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<OtherAdjustmentAll_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult AssesseeApprovalUpdateData(List<AsessmentApproval_VM> AllCheckedData)
        {
            AssessmentApprovalBLL assApp = new AssessmentApprovalBLL();
            MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                foreach (AsessmentApproval_VM AsessmentApproval in AllCheckedData)
                {
                    assApp.UpdateAssessmentApproved(mUser.UserID, AsessmentApproval);
                }
                cr.Data = null;
                cr.Message = "Successfully Approved";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}