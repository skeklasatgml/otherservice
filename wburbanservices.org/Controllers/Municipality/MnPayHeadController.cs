﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnPayHeadController : Controller
    {
        // GET: MnPayHeads
        [HttpPost]
        public JsonResult Get_Payheads( DateTime CollectionDate)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll bll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<PayHead_VM> lst = bll.Get_PayHeadsByMuniciplty(uObj.MunicipalityID, CollectionDate).Select(t =>
                {
                    PayHead_VM p = new PayHead_VM();
                    p.PayHeadID = t.PayHeadID;
                    p.PayHeadDesc = t.PayHeadDesc;
                    p.PayHeadType = t.PayHeadType;
                    p.PayRate = t.PayRate;
                    p.MinLimit = t.MinLimit;
                    p.MaxLimit = t.MaxLimit;
                    p.PayHeadBehave = t.PayHeadBehave;
                    p.ApplicableType = t.ApplicableType;
                    p.ApprovalLevel = t.ApprovalLevel;
                    p.MunicipalityID = t.MunicipalityID;
                    p.MunicipalityID = t.MunicipalityID;
                    p.GracePeriodInMonth = t.GracePeriodInMonth;
                    return p;
                }).ToList();
                cr.Data = lst;
                cr.Message = string.Format("{0} Payheas found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PayHead_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }
        [HttpGet]
        public ActionResult Configure_PayHead()
        {
            WardBLL lc = new WardBLL();
            MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
            var wards = lc.GetWards(uObj.MunicipalityID, "");
            return View(wards);
        }

        [HttpPost]
        public ActionResult SearchOnConfigurePayHead(string FinYear, string ApplicableType, string PayHeadBehave,long? GroupId)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadSubmitBll submitBll = new PayHeadSubmitBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<PayHeadDetails> pDetails = submitBll.Get_PayHeadsDetailsByMunicipltyID(uObj.MunicipalityID, FinYear, ApplicableType, PayHeadBehave, GroupId);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Payheas found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PayHeadDetails>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public ActionResult SaveRebatePayHead(string FinYear, string ApplicableType, string PayHeadBehave, long? GroupId, List<PayHeadDetails> tableDataRebate)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadSubmitBll submitBll = new PayHeadSubmitBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<PayHeadDetails> pDetails = submitBll.Save_PayHeadsDetailsByMunicipltyID(uObj.MunicipalityID, FinYear, ApplicableType, PayHeadBehave, tableDataRebate, GroupId);
                cr.Data = pDetails;
                cr.Message = "Submitted Successfully.";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PayHeadDetails>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult GetPayheadGroups()
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<PayheadGroup> groups = submitBll.GetPayheadGroups(uObj.MunicipalityID);
                cr.Data = groups;
                cr.Message = string.Format("{0} payhead groups found", groups.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PayheadGroup>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }


        [HttpPost]
        public JsonResult GetAssesseesPayheadGroup(long PayheadGroupId, int? WardId, int? LocationId, bool? IsMapped, string NameOrHolding)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                var groups = submitBll.GetAssesseesPayheadGroup(uObj.MunicipalityID, PayheadGroupId, WardId,LocationId,IsMapped,NameOrHolding);
                cr.Data = groups;
                cr.Message = string.Format("{0} Assesses found", groups.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<AssesseePayheadGroup>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult AddNewPayHeadGroup(string GroupName)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                PayheadGroup groups = submitBll.AddNewPayHeadGroup(GroupName,uObj.MunicipalityID);
                cr.Data = groups;
                cr.Message = string.Format("Group '{0}' successfully added",GroupName);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult MapGroupAll(int PayheadGroupId, int WardId, int? LocationId )
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                var isMapped = submitBll.MappAll(PayheadGroupId, WardId, LocationId, uObj.MunicipalityID);
                cr.Data = isMapped;
                cr.Message = string.Format("Assessee Mapped From Payhead Group");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult UnMapGroupAll(int? PayheadGroupId, int WardId, int? LocationId)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                var isMapped = submitBll.UnMapAll(PayheadGroupId, WardId, LocationId, uObj.MunicipalityID);
                cr.Data = isMapped;
                cr.Message = string.Format("Assessee Mapped From Payhead Group");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult UnMapGroup(long assesseeId)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                var isUnMapped = submitBll.UnMapGroup(assesseeId);
                cr.Data = isUnMapped;
                cr.Message = string.Format("Assessee Un-Mapped From Payhead Group");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;

        }
        [HttpPost]
        public JsonResult MapGroup(int PayheadGroupId, int? WardId, int? LocationId,long AssesseeId)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll submitBll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                var isMapped = submitBll.MapGroup(PayheadGroupId,WardId,LocationId,AssesseeId,uObj.MunicipalityID);
                cr.Data = isMapped;
                cr.Message = string.Format("Assessee Mapped From Payhead Group");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult GetPayheads()
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                PayHeadBll bll = new PayHeadBll();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<PayHead> lst = bll.Get_PayHeadsByMuniciplty(uObj.MunicipalityID);
                cr.Data = lst;
                cr.Message = string.Format("{0} Payheas found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PayHead_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }
    }
}