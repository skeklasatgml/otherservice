﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
//using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
//using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;


namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class AdjustmentApprovalController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        // GET: AdjustmentApproval
        ApprovedAdjustBLL _adjust = new ApprovedAdjustBLL();
        [HttpPost]
        public ActionResult GetApprovalAdjustmentData(string effectType)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<ApproveOtherAdjustment> lstAdjust = _adjust.GetAdjustmentData(mUser.MunicipalityID, effectType);
                cr.Data = lstAdjust;
                
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<ApproveOtherAdjustment>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult UpdateData(List<string> chkId)
        {
            MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                foreach (string chkID in chkId)
                {
                    _adjust.UpdateApproved(mUser.UserID, mUser.MunicipalityID, Convert.ToInt64(chkID));
                }
                cr.Message = "Successfully Approved";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}