﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.Models.Account;
using Valuation_Board.App_Codes;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnUserCounterAssociateController : Controller
    {
        // GET: UserCounterAssociate
        public ActionResult Index()
        {
            return View();
        }

        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MunicipalityUserCounterAssociateBLL objbll = new MunicipalityUserCounterAssociateBLL();
       
        [HttpPost]
        public JsonResult Insert_Update(MunicipalityUserCounterAssociate obj)
        {
           
            try
            {
                MunicipalityUser objMunici = (MunicipalityUser)SessionContext.CurrentUser;
                string msg = "";
                obj.MunicipalityID = Convert.ToInt32(objMunici.MunicipalityID);
                if(obj.RowID==0)
                {
                    msg= objbll.Insert(obj, Convert.ToInt32(objMunici.UserID));
                }
                else
                {
                    msg= objbll.Update(obj, Convert.ToInt32(objMunici.UserID));
                }
               
                cr.Data = null;
                cr.Message = msg;
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        public JsonResult Delete(string RowID)
        {
            try
            {
                objbll.Delete(Convert.ToInt32(RowID));
                cr.Data = "";
                cr.Message = "Successfully Deleted";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult Get_UserCouterAssos()
        {
            try
            {
                MunicipalityUser objMunici = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityUserCounterAssociate> lst = objbll.Get_UserCounterAssos(objMunici.MunicipalityID);
                cr.Data = lst;
                cr.Message = string.Format("{0} Counter Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        public JsonResult GetUserByMunicipalityID()
        {
            try
            {
                MunicipalityUser objMunici = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityUser> lst = objbll.GetUserByMunicipalityID(Convert.ToInt32(objMunici.MunicipalityID));
                cr.Data = lst;
                cr.Message = string.Format("{0} Counter Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}