﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using Valuation_Board.ViewModels.assmt;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class ScoreController : Controller
    {

        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MunicipalityBLL mnBll = new MunicipalityBLL();
        AssmtScoreSheetBll scbll = new AssmtScoreSheetBll();
        AssmtBeltSheetBll bbll = new AssmtBeltSheetBll();
        #endregion


        // GET: Score
        public ActionResult Index()
        {
            return View("~/Views/Municipality/WeightageOfValuation/Index.cshtml");
        }

        [HttpPost]
        public JsonResult GetEffectiveDate(string year)
        {
            try
            {
                var mObj = SessionContext.CurrentUser as MunicipalityUser;
                long userId = mObj.UserID;
                var municipalityId = mObj.MunicipalityID;
                var lst = scbll.GetEffectivedate(municipalityId, year, userId);
                cr.Data = lst;
                cr.Message = string.Format("{0} ScoreSheet Found", 1);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult GetAssmtBeltSheet(string year)
        {
            try
            {
                var mObj = SessionContext.CurrentUser as MunicipalityUser;
                long userId = mObj.UserID;
                var municipalityId = mObj.MunicipalityID;
                var lst = bbll.GetAllAssmtBeltSheet( municipalityId, year,userId);
                cr.Data = lst;
                cr.Message = string.Format("{0} ScoreSheet Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            } catch(Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult SaveAssmtBeltSheet(List<BeltSheetUpdate> BeltValues)
        {
            JsonResult js = new JsonResult();
            try
            {
                bbll.updateBeltSheet(BeltValues);
                js.Data = new ClientJsonResult()
                {
                    Data = true,
                    Message = "Data Saved Succesfully",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            return js;
        }
        [HttpPost]
        public JsonResult GetAssmtScoreSheet(string year)
        {
            List<AssmtScoreSheet> sLIst = new List<AssmtScoreSheet>();
            try
            {
                var mObj = SessionContext.CurrentUser as MunicipalityUser;
                long userId = mObj.UserID;
                var municipalityId = mObj.MunicipalityID;
                sLIst = scbll.GetAllAssmtScoreSheet(municipalityId, year, userId);
                cr.Data = sLIst;
                cr.Message = string.Format("{0} ScoreSheet Found", sLIst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                cr.Data = sLIst;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult SaveAssmtScoreSheet( List<ScoreSheetUpdate> ScoreValues )
        {
            JsonResult js = new JsonResult();
            try
            {
                scbll.updateScoreSheet(ScoreValues);
                js.Data = new ClientJsonResult()
                {
                    Data = true,
                    Message = "Data Saved Succesfully",
                    Status = ResponseStatus.SUCCESS
                };
            }
            catch (Exception ex)
            {
                js.Data = new ClientJsonResult()
                {
                    Data = false,
                    Message = ex.Message,
                    Status = ResponseStatus.UNKNOWN_SERVER_ERROR
                };
            }
            return js;
        }
        [HttpPost]
        public JsonResult GetAllMunicipalites()
        {
            List<Municipality_Ddl_VM> mnVm = new List<Municipality_Ddl_VM>();
            
            try
            {
                var lstmn = mnBll.GetAllMunicipalities();
                mnVm = lstmn.Select(t =>
                {
                   return new Municipality_Ddl_VM()
                   {
                      MunicipalityID = t.MunicipalityID ?? 0,
                      MunicipalityName = t.MunicipalityName
                   };
                }).ToList();
                cr.Data = mnVm;
                cr.Message = string.Format("{0} Municipalities Found", mnVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = mnVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetBeltTypevalue() {
            try
            {
                var x = bbll.getBeltTypevalue();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetYear()
        {

            try {
                var x =scbll.GetYear();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex ) {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}