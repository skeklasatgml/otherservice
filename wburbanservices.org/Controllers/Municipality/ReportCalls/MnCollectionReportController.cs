﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models.Account;

namespace Valuation_Board.Controllers.Municipality.ReportCalls
{
    [SecuredFilter]
    public class MnCollectionReportController : Controller
    {
        // GET: MnCollectionReport
        public ActionResult Index()
        {
            MunicipalityUserBll mu = new MunicipalityUserBll();
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            List<MunicipalityUser> lstUser = mu.Get_AllUsersByMunicipalityID(MnUser.MunicipalityID);
            ViewBag.listUser = lstUser;
            return View("~/Views/Municipality/ReportCalls/MnCollectionReport/Index.cshtml");
        }
    }
}