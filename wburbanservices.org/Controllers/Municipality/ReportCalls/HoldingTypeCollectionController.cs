﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models.Account;
using Valuation_Board.App_Start;

namespace Valuation_Board.Controllers.Municipality.ReportCalls
{
    [SecuredFilter]
    public class HoldingTypeCollectionController : Controller
    {
        // GET: HoldingTypeCollection
        public ActionResult Index()
        {
            MunicipalityUserBll mu = new MunicipalityUserBll();
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            List<MunicipalityUser> lstUser = mu.Get_AllUsersByMunicipalityID(MnUser.MunicipalityID);
            ViewBag.listUser = lstUser;
            return View("~/Views/Municipality/ReportCalls/HoldingTypeWiseCollection/Index.cshtml");
        }
    }
}