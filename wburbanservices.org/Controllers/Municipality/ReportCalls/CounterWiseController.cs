﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Start;

namespace Valuation_Board.Controllers.Municipality.ReportCalls
{
    [SecuredFilter]
    public class CounterWiseController : Controller
    {
        // GET: CounterWise
        public ActionResult Index()
        {
            return View("~/Views/Municipality/ReportCalls/CounterWise/Index.cshtml");
        }
    }
}