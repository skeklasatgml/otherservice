﻿using System.Web.Mvc;
using Valuation_Board.App_Start;

namespace Valuation_Board.Controllers.Municipality.ReportCalls
{
    [SecuredFilter]
    public class MnDemandReportController : Controller
    {
        // GET: DemandReport
        [SecuredFilter]
        public ActionResult Index()
        {
            return View("~/Views/Municipality/ReportCalls/MnDemandReport/Index.cshtml");
        }
    }
}