﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;


namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class PaymentCancellationController : Controller
    {
        #region
        ClientJsonResult cr = new ClientJsonResult();
        PaymentBll paymentBll = new PaymentBll();
        #endregion
        [HttpGet]
        public ActionResult PaymentCancellationUI()
        {
            return View("~/Views/Municipality/PaymentCancellation/PaymentCancellationUI.cshtml");
        }
        [HttpPost]
        public JsonResult SearchOfflinePayments(int WardID,int LocationID,long AssesseeID, DateTime CollectionDtFrom, DateTime CollectionDtTo)
        {
            List<PaymentSummary> lstPaymentSumarry= new List<PaymentSummary>();
            try
            {
                MunicipalityUser mUser = SessionContext.CurrentUser as MunicipalityUser;
                lstPaymentSumarry = paymentBll.GetPaymentsByAssesseeID(WardID: WardID,
                                                                       assesseeID: AssesseeID, 
                                                                       municipalityID: mUser.MunicipalityID, 
                                                                       collectionDtFrom: CollectionDtFrom, 
                                                                       collectionDtTo: CollectionDtTo, 
                                                                       paymentType: PaymentType.Offline,
                                                                       LocationID: LocationID);

                cr.Message = string.Format("{0} payments found",lstPaymentSumarry.Count());
                cr.Status = ResponseStatus.SUCCESS;
            }catch(Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            cr.Data =lstPaymentSumarry;
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CancelOfflinePayment(long PaymentID, long AssesseeID, List<CancellationReason> CancellationReasons, string RemarksText) {
            List<PaymentSummary> lstPaymentSumarry = new List<PaymentSummary>();
            try
            {
                MunicipalityUser mUser = SessionContext.CurrentUser as MunicipalityUser;
                paymentBll.CancelOfflinePayment(PaymentID, mUser.MunicipalityID,AssesseeID, CancellationReasons, mUser.UserID, RemarksText);
                cr.Message = "Payment cancelled succesfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetCancelationReasonList()
        {
            List<CancellationReason> lstReason = new List<CancellationReason>();
            try
            {

                lstReason=paymentBll.GetCancelReasonList();
                cr.Message = string.Format("{0} Reasons found",lstReason.Count) ;
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            cr.Data = lstReason;
            return Json(cr);
        }
        public JsonResult SearchInstrumentDtls(long assesseeID, long paymentID)
        {
            List<CancelInstrumentDetails> dtlsInstrumentLst = new List<CancelInstrumentDetails>();
            try
            {
                MunicipalityUser mUser = SessionContext.CurrentUser as MunicipalityUser;
                dtlsInstrumentLst = paymentBll.GetInstrumentDetails(municipalityID: mUser.MunicipalityID, paymentID: paymentID, assesseeID: assesseeID);
                cr.Message = string.Format("{0} payments found", dtlsInstrumentLst.Count());
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            cr.Data = dtlsInstrumentLst;
            return Json(cr);
        }
    }
}