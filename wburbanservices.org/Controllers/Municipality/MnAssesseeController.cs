﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnAssesseeController : Controller
    {
        AssesseeBll _assesseeBLL = new AssesseeBll();
        [HttpPost]
        public ActionResult GetAssessees(int WardID, int LocationID, string AssesseeName)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<Assessee> lstAssessee = _assesseeBLL.GetAssessees(mUser.MunicipalityID, WardID, LocationID, AssesseeName);
                cr.Data = lstAssessee.Select(t =>
                {
                    Assessee_Ddl_VM obj = new Assessee_Ddl_VM() { AssesseeID = t.AssesseeID, AssesseeName = t.AssesseeName,HoldingNo= t.HoldingNo };
                    return obj;
                }).Take(30).ToList();
                cr.Message = string.Format("{0} Assessees found", lstAssessee.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Assessee_Ddl_VM>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult GetAssesseesByHoldings(string HoldingNo)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<Assessee> lstAssessee = _assesseeBLL.GetAssessees(mUser.MunicipalityID, HoldingNo, mUser.UserID);
                lstAssessee = _assesseeBLL.IncludeWard(lstAssessee, mUser.MunicipalityID);
                lstAssessee.ForEach(t => {
                    var assessee = _assesseeBLL.IncludeLocation(t);
                    t.Location = assessee.Location==null?null: assessee.Location;
                });

                var data = lstAssessee.Select(t=> {
                    return new {
                        AssesseeID =t.AssesseeID,
                        AssesseeName=t.AssesseeName,
                        AssesseeNo=t.AssesseeNo,
                        WardName=t.Ward==null?null:t.Ward.WardName,
                        HoldingNo = t.HoldingNo,
                        LocationName=t.Location==null?null:t.Location.LocationName
                    };
                });
                cr.Data = data;
                cr.Message = string.Format("{0} Assessees found", lstAssessee.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Assessee>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult GetLast20Payments(long AssesseeID)
        {
            List<PaymentSummary> lstLastPayments = new List<PaymentSummary>();
            ClientJsonResult cr = new ClientJsonResult();
            cr.Data = lstLastPayments;
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                Assessee assessee = _assesseeBLL.GetAssesseebyId(AssesseeID);
                
                if (assessee == null || user.MunicipalityID != assessee.MunicipalityID)
                {
                    assessee = null;
                }
                else
                {
                    PropertyTaxCollectionBLL pbll = new PropertyTaxCollectionBLL();
                     lstLastPayments = pbll.GetLastPayments(20, assessee.AssesseeID);
                    cr.Data = assessee;
                }
                cr.Data = lstLastPayments;
                cr.Message = string.Format(string.Format("{0} payments found",lstLastPayments.Count));
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAssesseeByID(long AssesseeID)
        {
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                Assessee assessee = _assesseeBLL.GetAssesseebyId(AssesseeID);
                if (assessee == null || user.MunicipalityID != assessee.MunicipalityID)
                {
                    assessee = null;
                }
                else
                {
                    assessee = _assesseeBLL.IncludeLocation(assessee);
                    assessee = _assesseeBLL.IncludeWard(assessee);
                }
                cr.Data = assessee;
                cr.Message = string.Format("Assessees found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAssesseesByHoldingSuggestion(int WardID, int LocationID, string HoldingNo,int? TakeRowCount)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<Assessee> lstAssessee = _assesseeBLL.SearchHolding(mUser.MunicipalityID,WardID,LocationID,HoldingNo);
                if (TakeRowCount != null)
                {
                    lstAssessee = lstAssessee.OrderBy(t=>t.HoldingNo).Take(TakeRowCount ?? 0).ToList();
                }
                
                cr.Data = lstAssessee;
                cr.Message = string.Format("{0} Holdings found", lstAssessee.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Assessee>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult UpdateAssesse(AssesseWithRef assessee)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                
                var mUser = SessionContext.CurrentUser as MunicipalityUser;
                assessee.UpdatedBy = mUser.UserID;
                assessee.MunicipalityID = mUser.MunicipalityID;
                AssesseeBll bll = new AssesseeBll();
                bll.UpdateAssessee(assessee);
                cr.Data = true;
                cr.Message = "Assessee updated Succesfully";
                cr.Status = ResponseStatus.SUCCESS;
                
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = "Assessee not updated: "+ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        [HttpPost]
        public JsonResult InsertNewAssesse(AssesseWithRef assessee)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {

                var mUser = SessionContext.CurrentUser as MunicipalityUser;
                assessee.InsertedBy = mUser.UserID;
                assessee.MunicipalityID = mUser.MunicipalityID;
                AssesseeBll bll = new AssesseeBll();
              var assesseeObj= bll.InsertNewAssesse(assessee);
                cr.Data = assesseeObj;
                cr.Message = "Assessee Inserted Succesfully";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = "Assessee not Inserted: " + ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

    }
}