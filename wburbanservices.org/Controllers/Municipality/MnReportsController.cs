﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Globalization;
using Valuation_Board.Models;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Account;
using Valuation_Board.Reporting.Viewer;

namespace Valuation_Board.Controllers.Municipality
{
    //please do not add secured filter 
    //another copy of security filter added to report viewer
    public class MnReportsController : Controller
    {
        #region
        Dictionary<string, object> _params = new Dictionary<string, object>();
        ReportUtils rt = new ReportUtils();
        FileCreator fc = new FileCreator();
        #endregion
        // GET: ReportCall
        [HttpGet]
        public RedirectResult PaymentReciept(int PaymentId)
        {

            _params.Add("PaymentID", PaymentId);
            return Redirect(rt.GetReportViewerURL(1, _params));
        }
        [HttpPost]
        public RedirectResult CollectionReport(string DateFrom, string DateTo, int? WardID, int? LocationID, string PaymentTyp, string Counter)
        {
            var dt = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardNo", WardID ?? 0);
            _params.Add("LocationNo", LocationID ?? 0);
            //_params.Add("ReceiptFromDate", DateFrom);
            //_params.Add("ReceiptToDate", DateTo);
            _params.Add("ReceiptFromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ReceiptToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("Counter", Counter);
            //_params.Add("UserID", UserID ?? 0);
            _params.Add("PaymentType", PaymentTyp);
            return Redirect(rt.GetReportViewerURL(3, _params));
        }
        [HttpPost]
        public JsonResult CollectionReportExcel(string DateFrom, string DateTo, int? WardID, int? LocationID, string PaymentTyp, string CounterID)
        {
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

                _params.Add("DateFrom", DateFrom);
                _params.Add("DateTo", DateTo);
                _params.Add("WardID", WardID);
                _params.Add("LocationID", LocationID);
                _params.Add("PaymentTyp", PaymentTyp);
                _params.Add("Counter", CounterID);
                _params.Add("MunicipalityID", MnUser.MunicipalityID);
                cr.Data = fc.GetChallanExcellFile(_params);
                cr.Message = "Successfully Create File";
                cr.Status = ResponseStatus.SUCCESS;

                return Json(cr);
            }
            catch (Exception ex)
            {
                cr.Data = "";
                cr.Message = "Error";
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                throw new Exception(ex.Message);
            }
        }
        [HttpPost]
        public RedirectResult CollectorWisesReport(string DateFrom, string DateTo, int? WardID, int? LocationID, string PaymentTyp, string Counter)
        {
            var dt = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardNo", WardID ?? 0);
            _params.Add("LocationNo", LocationID ?? 0);
            _params.Add("ReceiptFromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ReceiptToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("Counter", Counter);
            _params.Add("PaymentType", PaymentTyp);
            return Redirect(rt.GetReportViewerURL(14, _params));
        }
        [HttpPost]
        public RedirectResult HoldingTypeWisesReport(string DateFrom, string DateTo, int? WardID, int? LocationID, string PaymentTyp, string Counter)
        {
            var dt = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardNo", WardID ?? 0);
            _params.Add("LocationNo", LocationID ?? 0);
            _params.Add("ReceiptFromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ReceiptToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("Counter", Counter);
            _params.Add("PaymentType", PaymentTyp);
            return Redirect(rt.GetReportViewerURL(15, _params));
        }
        [HttpPost]
        public RedirectResult DemandReport(int WardID, int? LocationID, int? AssesseeID)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardID", WardID);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("AssesseeID", AssesseeID ?? 0);

            return Redirect(rt.GetReportViewerURL(2, _params));
        }
        [HttpPost]
        public RedirectResult DemandOutstanding(int WardID, int? LocationID, int? AssesseeID)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardID", WardID);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("AssesseeID", AssesseeID ?? 0);

            return Redirect(rt.GetReportViewerURL(18, _params));
        }
        [HttpPost]
        public RedirectResult AssesseeListReport(int? WardID, int? LocationID, string OutstandingFrom, string OutstandingTo, int? HoldingTypeID)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardID", WardID ?? 0);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("HoldingTypeID", HoldingTypeID ?? 0);
            _params.Add("MinFinYearFrom", OutstandingFrom);
            _params.Add("MinFinYearTo", OutstandingTo);
            return Redirect(rt.GetReportViewerURL(4, _params));
        }
        [HttpPost]
        public RedirectResult AssesseeListRpt(int? WardID, int? LocationID, string OutstandingFrom, string OutstandingTo, int? HoldingTypeID)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardID", WardID ?? 0);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("HoldingTypeID", HoldingTypeID ?? 0);
            _params.Add("MinFinYearFrom", OutstandingFrom);
            _params.Add("MinFinYearTo", OutstandingTo);
            return Redirect(rt.GetReportViewerURL(17, _params));
        }
        [HttpPost]
        public RedirectResult CounterWiseReport(string DateFrom, string DateTo)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("DateFrom", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));

            return Redirect(rt.GetReportViewerURL(5, _params));
        }
        [HttpPost]
        public RedirectResult RemissionReport(string DateFrom, string DateTo, int PayHead)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("DateFrom", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("DateTo", DateTime.ParseExact(DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("PayHeadID", PayHead);
            return Redirect(rt.GetReportViewerURL(6, _params));
        }
        [HttpPost]
        public RedirectResult WardWiseReport(string DateFrom, string DateTo, int? WardID, string Counter)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("FromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("WardID", WardID ?? 0);
            _params.Add("Counter", Counter);
            return Redirect(rt.GetReportViewerURL(7, _params));
        }
        [HttpPost]
        public RedirectResult CancelPaymentReport(string DateFrom, string DateTo, int? WardID, int? LocationID, string Counter)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("FromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("WardID", WardID ?? 0);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("Counter", Counter);
            return Redirect(rt.GetReportViewerURL(8, _params));
        }
        [HttpPost]
        public RedirectResult OutstandingReport(int? WardID, int? LocationID, int? AssesseeID)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardID", WardID ?? 0);
            _params.Add("LocationID", LocationID ?? 0);
            _params.Add("AssesseeID", AssesseeID ?? 0);
            return Redirect(rt.GetReportViewerURL(9, _params));
        }
        [HttpPost]
        public RedirectResult DemandVsPaymentReport(string FinYear)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("FinYear", FinYear);
            return Redirect(rt.GetReportViewerURL(12, _params));
        }
        [HttpPost]
        public RedirectResult DemandVsPaymentWardWiseReport(string FinYear)
        {
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("FinYear", FinYear);
            return Redirect(rt.GetReportViewerURL(13, _params));
        }

        [HttpPost]
        public RedirectResult CollectionReportPDF(string DateFrom, string DateTo, int? WardID, int? LocationID, string PaymentTyp, string Counter)
        {
            var dt = DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;

            _params.Add("MunicipalityID", MnUser.MunicipalityID);
            _params.Add("WardNo", WardID ?? 0);
            _params.Add("LocationNo", LocationID ?? 0);
            _params.Add("ReceiptFromDate", DateTime.ParseExact(DateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("ReceiptToDate", DateTime.ParseExact(DateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"));
            _params.Add("Counter", Counter);
            _params.Add("PaymentType", PaymentTyp);
            return Redirect(rt.GetReportViewerURL(25, _params));
        }

        //OTHER SERVICES
        #region 
        [HttpGet]
        public RedirectResult RoadCuttingAnnexure1(string ApplicationID)
        {
            _params.Add("ApplicationID", ApplicationID);
            //_params.Add("ServApplicationID", ServApplicationID);
            return Redirect(rt.GetReportViewerURL(59, _params));
        }

        #endregion
    }
}