﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{[SecuredFilter]
    public class MnLocationsController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        #endregion
        [HttpPost]
        public JsonResult GetLocation(int wardID, string locationName)
        {
            LocationBLL locationObject = new LocationBLL();
            MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
            List<Location_Ddl_VM> lstLocaiton = locationObject.GetLocations(locationName,mObj.MunicipalityID, wardID).Select(t=> {
                return new Location_Ddl_VM() {LocationID=t.LocationID ??0, LocationName=t.LocationName };
            }).ToList();
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult()
            {
                Data = lstLocaiton,
                Message = string.Format("{0} Locations found", lstLocaiton.Count),
                Status = ResponseStatus.SUCCESS
            };
            js.Data = cr;
            return js;
        }

    }
}