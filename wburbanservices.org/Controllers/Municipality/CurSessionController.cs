﻿using System.Web.Mvc;
using Valuation_Board.App_Start;
using Valuation_Board.Models.Account;
using System;
using Valuation_Board.Models;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using System.Collections.Generic;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class CurSessionController:Controller
    {
        //Get User
        #region Global Variable
        JsonResult js = new JsonResult();
        ClientJsonResult cj = new ClientJsonResult();
        MunicipalityTaxPaymentCounter_BLL counterBll = new MunicipalityTaxPaymentCounter_BLL();
        TaxPaymentMode_BLL PaymentMode_BLL = new TaxPaymentMode_BLL();
        #endregion
        public JsonResult GetCurrentUser()
        {
            js.Data = SessionContext.CurrentUser;
            js.Data = cj;
            return js;
        }
        public JsonResult GetCurrentPaymentCounter()
        {
            js.Data = cj;
            try
            {
                MunicipalityUser mnUser = SessionContext.CurrentUser as MunicipalityUser;
                cj.Data = counterBll.GetCounter(mnUser.MunicipalityID, DateTime.Now, mnUser.UserID);
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = string.Format("Tax Payment counter found. On user [{0}]", mnUser.UserName);
            }
            catch (Exception ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cj.Data = null;
            }
            return js;
        }
        public JsonResult GetPaymentModes()
        {
            js.Data = cj;
            List<PaymentMode> PaymentModes = new List<PaymentMode>();
            try
            {
                MunicipalityUser mnUser = SessionContext.CurrentUser as MunicipalityUser;
                PaymentModes= PaymentMode_BLL.GetPaymentModes(mnUser.MunicipalityID, mnUser.UserID, DateTime.Now);
                cj.Message = string.Format("{0} payment modes found", PaymentModes.Count);
                cj.Data = PaymentModes;
                cj.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cj.Data = PaymentModes;
            }
            return js;
        }
        [HttpGet]
        public JsonResult GetActiveUsers()
        {
            try
            {
                MunicipalityUserBll objbll = new MunicipalityUserBll();
                MunicipalityUser objMunici = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityUser> lst = objbll.Get_AllUsersByMunicipalityID(Convert.ToInt32(objMunici.MunicipalityID));
                cj.Data = lst;
                cj.Message = string.Format("{0} Counter Found", lst.Count);
                cj.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cj.Data = new LinkedList<MunicipalityUser>();
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cj;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        public JsonResult LoggedInOn()
        {
            throw new NotImplementedException();
        }
    }
}