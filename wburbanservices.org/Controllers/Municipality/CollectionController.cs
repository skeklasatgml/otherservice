﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.ModelBinders;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class CollectionController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult jc = new ClientJsonResult();
        #endregion
        public ActionResult Index()
        {


            return View("~/Views/Municipality/Collection/Index.cshtml");
        }
        [HttpPost]
        public JsonResult GetCollectorAdjustmentConfiguration()
        {
            try
            {
                MunicipalityConfigurationBll bll = new MunicipalityConfigurationBll();
                var mnUser = SessionContext.CurrentUser as MunicipalityUser;
                MunicipalityConfiguration config = bll.GetConfiguration(mnUser.MunicipalityID, mnUser.UserID);
                if (config.CollectorAdjustmentConfiguration == null)
                {
                    throw new InvalidOperationException("Collector adjustment configuration not found");
                }

                jc.Data = config.CollectorAdjustmentConfiguration;
                jc.Message = string.Format("Collector Adjustment Configuration Found");
                jc.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Message = ex.Message;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = jc;
            return js;
        }

        /// <summary>
        /// Make server side calculation and return result sheet according to request payload
        /// </summary>
        /// <param name="WardID">tergate ward ID</param>
        /// <param name="LocationID">tergate location id</param>
        /// <param name="AssesseeID">tergate assessee id</param>
        /// <param name="PaymentDate">basically collection date from customar</param>
        /// <param name="TaxPaymentCheckParameters">Complex type, generic type list! express selected selected qtrs to be calculated </param>
        /// <param name="NoticeServedOn">date when notice is served</param>
        /// <param name="EffectQtrFrom">if notice served date is not null, It expect from which qtr notice served date will be effected, by default first qtr</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Get_PropertyTaxPaymentDetails(int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters, DateTime? NoticeServedOn, int? EffectQtrFrom)
        {
            try
            {
                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                PaymentProcessor payment = new PaymentProcessor();

                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, mObj.MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(mObj.MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });
                Object = payment.SearchOutStanding(mObj.MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, NoticeServedOn, EffectQtrFrom, mObj.UserID);
                if (Object != null)
                {
                    AssesseeBll assbll = new AssesseeBll();
                    object assessee = assbll.GetAssesseebyId(AssesseeID);
                    Object.AdditionalData = new { Assessee = assessee };
                }

                jc.Data = Object;
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = "Payment Details found";
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Message = ex.Message;
            }
            js.Data = jc;
            return js;
        }

        /// <summary>
        /// Make server side calculation and return result sheet according to request payload
        /// </summary>
        /// <param name="WardID">tergate ward ID</param>
        /// <param name="LocationID">tergate location id</param>
        /// <param name="AssesseeID">tergate assessee id</param>
        /// <param name="PaymentDate">basically collection date from customar</param>
        /// <param name="TaxPaymentCheckParameters">Complex type, generic type list! express selected selected qtrs to be calculated </param>
        /// <param name="NoticeServedOn">date when notice is served</param>
        /// <param name="effectQtrFrom">if notice served date is not null, It expect from which qtr notice served date will be effected, by default first qtr</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MakePayment(string Mobile, string Email, int WardID, int LocationID, long AssesseeID, string PaymentDate, List<TaxPaymentCheckParameter> TaxPaymentCheckParameters,[ModelBinder(typeof(PaymentMode_ModelBinder))] List<PaymentMode> PaymentModes, DateTime? NoticeServedOn, int? EffectQtrFrom,string BillReciptNo)
        {
            //PaymentDate statnds for collection date
            try
            {
                AssesseeBll bll = new AssesseeBll();
                try
                {
                    bll.UpdateAssesseeEmailID(AssesseeID, Email.Trim());
                    bll.UpdateAssesseeMobileNo(AssesseeID, Mobile.Trim());
                }
                catch { }

                PTaxCollectionGiantObject Object = new PTaxCollectionGiantObject();
                //if null then assing empty list
                TaxPaymentCheckParameters = TaxPaymentCheckParameters ?? new List<TaxPaymentCheckParameter>();
                //convert date time to proper format
                DateTime paymentDate = DateTime.ParseExact(PaymentDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;

                PaymentProcessor payment = new PaymentProcessor();

                //gather if any adjustment available
                OtherAdjustmentBLL otherAdjustmentBll = new OtherAdjustmentBLL();
                List<OtherAdjustmentLevelAssessee> lstOtherAdjsLvlAssessee = otherAdjustmentBll.GetAssesseeLevelAdjustments(AssesseeID, paymentDate, mObj.MunicipalityID);
                List<OtherAdjustmentMaster> listOtherAdjustmentMasters = otherAdjustmentBll.GetLevelAllAdjustments(mObj.MunicipalityID, paymentDate);

                //add adjustments to payment proccessor
                //add overall level adjustments first
                listOtherAdjustmentMasters.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });

                //add assessee level adjustments second
                lstOtherAdjsLvlAssessee.ForEach(t =>
                {
                    payment.AddOtherAdjustment(t);
                });
                Object = payment.Calculate(mObj.MunicipalityID, WardID, LocationID, AssesseeID, paymentDate, TaxPaymentCheckParameters, NoticeServedOn, EffectQtrFrom, mObj.UserID);

                Object.propertyTaxPaymentMaster.InsertedBy = (int)mObj.UserID;
                Object.propertyTaxPaymentMaster.InsertedOn = DateTime.Now;
                Object.propertyTaxPaymentMaster.PaymentType = "F";//F: Offline
                Object.propertyTaxPaymentMaster.CollectorID = (mObj.UserID == 0 ? null : (int?)mObj.UserID);
                Object.propertyTaxPaymentMaster.TransactionID = null;

                //reassign paid amount
                Object.propertyTaxPaymentMaster.PaidAmount = Object.propertyTaxPaymentMaster.NetAmount;
                Object.propertyTaxPaymentMaster.BillReciptNo = BillReciptNo;

                long? paymentID = payment.MakePayment(Object.propertyTaxPaymentMaster, TaxPaymentCheckParameters, PaymentModes);
                if (paymentID == null)
                {
                    throw new Exception("Unknown Error! Transaction canceled");
                }
                else
                {
                    jc.Data = new { MstPaymentID = paymentID, RedirectTo = string.Format("/Municipality/MnReports/PaymentReciept?PaymentId={0}", paymentID) };
                }
                jc.Status = ResponseStatus.SUCCESS;
                jc.Message = "paid successfully";
            }
            catch (Exception ex)
            {
                jc.Data = null;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                jc.Message = ex.Message;
            }
            js.Data = jc;
            return js;
        }

        [HttpPost]
        public JsonResult GetLast20PaymentsSummary()
        {

            try
            {
                MunicipalityUser mnUser = (MunicipalityUser)SessionContext.CurrentUser;
                PropertyTaxCollectionBLL obj = new PropertyTaxCollectionBLL();
                MunicipalityTaxPaymentCounter_BLL counterBll = new MunicipalityTaxPaymentCounter_BLL();
                MunicipalityPaymentCounter cObj = counterBll.GetCounter(mnUser.MunicipalityID, DateTime.Now, mnUser.UserID);
                if (cObj != null)
                {
                    List<PaymentSummary> lstSummary = obj.GetLastPayments(10, mnUser.MunicipalityID, cObj.CounterID);
                    jc.Data = lstSummary;
                    jc.Message = string.Format("{0} Payments found", lstSummary.Count);
                    jc.Status = ResponseStatus.SUCCESS;
                }
                else
                {
                    jc.Data = new List<PaymentSummary>();
                    jc.Message = string.Format("{0} Payments found", 0);
                    jc.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                jc.Data = new List<PaymentSummary>();
                jc.Message = ex.Message;
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = jc;
            return js;
        }

        [HttpPost]
        public JsonResult PayAdvance(int WardID, int LocationID, long AssesseeID, List<PaymentMode> PaymentModes, string Mobile, string Email)
        {
            try
            {
                AssesseeBll bll = new AssesseeBll();
                try
                {
                    bll.UpdateAssesseeEmailID(AssesseeID, Email.Trim());
                    bll.UpdateAssesseeMobileNo(AssesseeID, Mobile.Trim());
                }
                catch { }
                PaymentProcessor p = new PaymentProcessor();
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                var paymentId = p.MakeAdvancePayment(mObj.MunicipalityID, WardID, LocationID, AssesseeID, (int)mObj.UserID, PaymentModes);
                jc.Data = new { MstPaymentID = paymentId, RedirectTo = string.Format("/Municipality/MnReports/PaymentReciept?PaymentId={0}", paymentId) };
                jc.Message = "Payment Successfull";
                jc.Status = ResponseStatus.SUCCESS;
            }
            catch (SqlException ex)
            {

                jc.Message = string.Format(ex.Message);
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            catch (Exception ex)
            {
                jc.Message = string.Format(ex.Message);
                jc.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(jc);
        }


    }
}