﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class OtherAdjustmentController : Controller
    {
        // GET: OtherAdjustment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllAdjustData(OtherAdjustmentAll_VM AllAdjData)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
               // OtherAdjustmentAll_VM OthrAdj = new OtherAdjustmentAll_VM();
                List<OtherAdjustmentAll_VM> pDetails = OthAdjBlll.Get_AllOtherAdjustment(uObj.MunicipalityID, AllAdjData.EffectFromDT, AllAdjData.EffectToDT);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Payheas found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<OtherAdjustmentAll_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult GetIndAdjustData(string EffectFromDT, string EffectToDT, string AssesseeID)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                // OtherAdjustmentAll_VM OthrAdj = new OtherAdjustmentAll_VM();
                List<OtherAdjustmentIndividual_VM> pDetails = OthAdjBlll.Get_IndOtherAdjustment(uObj.MunicipalityID, EffectFromDT, EffectToDT, AssesseeID);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Payheas found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<OtherAdjustmentIndividual_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult UpdateAllAdjustData(OtherAdjustmentAll_VM AllAdjData)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
                
                OthAdjBlll.Update_AllOtherAdjustment(AllAdjData, uObj.UserID);
                cr.Data = null;
                cr.Message = string.Format("Updated Successfully.");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult UpdateIndAdjustData(OtherAdjustmentIndividual_VM IndAdjData)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                OthAdjBlll.Update_IndividualOtherAdjustment(IndAdjData, uObj.UserID);
                cr.Data = null;
                cr.Message = string.Format("Updated Successfully.");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult EntryAdjustAllData(OtherAdjustmentAll_VM AllAdjData)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                OthAdjBlll.Insert_AllOtherAdjustment(AllAdjData, uObj.MunicipalityID, uObj.UserID);
                cr.Data = null;
                cr.Message = string.Format("Submitted Successfully.");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public ActionResult EntryAdjustIndData(OtherAdjustmentIndividual_VM AllAdjData)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                OthAdjBlll.Insert_IndOtherAdjustment(AllAdjData, uObj.MunicipalityID, uObj.UserID);
                cr.Data = null;
                cr.Message = string.Format("Submitted Successfully.");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public JsonResult GetPayHeadDesc()
        {
            List<Municipality_Ddl_VM> mnVm = new List<Municipality_Ddl_VM>();
            MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                OtherAdjustmentBLL OthAdjBlll = new OtherAdjustmentBLL();
                List<PayHeadDesc_VM> lstMn = OthAdjBlll.GetPayHeadDesc(uObj.MunicipalityID);               
                cr.Data = lstMn;
                cr.Message = string.Format("{0} PayHead", mnVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = mnVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}