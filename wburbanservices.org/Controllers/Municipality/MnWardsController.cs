﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MnWardsController : Controller
    {
        #region
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        WardBLL wrdBLL = new WardBLL();
        #endregion
        [HttpPost]
        public JsonResult GetWards(string WardName)
        {
            List<Ward_Ddl_VM> wrVm = new List<Ward_Ddl_VM>();
            try
            {
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<Ward> lstWr = wrdBLL.GetWards(mObj.MunicipalityID, WardName);
                wrVm = lstWr.Select(t =>
                {
                    return new Ward_Ddl_VM()
                    {
                        WardID = t.WardID ?? 0,
                        WardName = t.WardName
                    };
                }).ToList();
                cr.Data = wrVm;
                cr.Message = string.Format("{0} Wards Found", wrVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = wrVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult GetMappedWards(string WardName)
        {
            List<Ward_Ddl_VM> wrVm = new List<Ward_Ddl_VM>();
            try
            {
                MunicipalityUser mObj = (MunicipalityUser)SessionContext.CurrentUser;
                List<Ward> lstWr = wrdBLL.GetWards(mObj.MunicipalityID, WardName);
                List<Ward> lstMappedWards = wrdBLL.GetMappedWardsOrAllByUserID(mObj.UserID, mObj.MunicipalityID);


                wrVm = lstWr.Where(s => lstMappedWards.FirstOrDefault(ss => ss.WardID == s.WardID) != null).Select(t =>
                        {
                            return new Ward_Ddl_VM()
                            {
                                WardID = t.WardID ?? 0,
                                WardName = t.WardName
                            };
                        }).ToList();
                cr.Data = wrVm;
                cr.Message = string.Format("{0} Wards Found", wrVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = wrVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        #region action injected 16112017. Besically used on user and ward mapping
        [HttpPost]
        public JsonResult AddWardToUser(long UserID, int WardID)
        {
            try
            {
                //check user and ward is in session's municipality id
                //insert proc from bll
                wrdBLL.AddWardToUser(UserID, WardID, (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                cr.Data = true;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = "Ward successfully added";
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult RemoveWardToUser(long UserID, int WardID)
        {
            try
            {
                wrdBLL.RemoveWardToUser(UserID, WardID, (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                cr.Data = true;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = "Ward successfully removed";
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetMappedWardsByUserID(long UserID)
        {
            List<Ward> wards = new List<Ward>();
            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            wards = wrdBLL.GetMappedWardsByUserID(UserID, MnUser.MunicipalityID);
            ClientJsonResult cj = new Models.ClientJsonResult()
            {
                Data = wards,
                Status = ResponseStatus.SUCCESS,
                Message = string.Format("{0} wards found", wards.Count)
            };
            return Json(cj);
        }
        public ActionResult UserWardsMappingUI()
        {
            MunicipalityUserBll mu = new MunicipalityUserBll();

            MunicipalityUser MnUser = (MunicipalityUser)SessionContext.CurrentUser;
            List<MunicipalityUser> lstUser = mu.Get_AllUsersByMunicipalityID(MnUser.MunicipalityID);
            List<Ward> wards = wrdBLL.GetWards(MnUser.MunicipalityID, "").OrderBy(t => t.WardID).ToList();
            ViewBag.listUser = lstUser;
            ViewBag.wards = wards;
            return View("~/Views/Municipality/MnWards/UserWardsMappingUI.cshtml");
        }
        #endregion

    }
}