﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class MunicipalityConfigMasterController:Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        JsonResult js = new JsonResult();
        public MunicipalityConfigMasterController()
        {
            js.Data = cr;
        }
        public ActionResult GetConfigurationUI()
        {
            MunicipalityConfiguration_vm vm=new MunicipalityConfiguration_vm();
            try
            {
                MunicipalityConfiguration config = null;
                var bll = new MunicipalityConfigurationBll();
                var districtBll = new DistrictBLL();
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                config = bll.GetConfiguration(user.MunicipalityID);
                vm.Districts = districtBll.GetAllDistricts();
                vm.MunicipalityConfiguration = config;
            }
            catch { }
            return View("~/Views/Municipality/ConfigurationMstr/ConfigurationMasterUI.cshtml", vm);
        }
        [HttpPost]
        public JsonResult SaveDemandBillConfig(MunicipalityConfiguration.DemandBillConfig DemandBillConfig)
        {
            try
            {
                var bll = new MunicipalityConfigurationBll();
                MunicipalityUser user=SessionContext.CurrentUser as MunicipalityUser;
                MunicipalityConfiguration config= bll.GetConfiguration(user.MunicipalityID);
                
                config.DemandBillConfiguration= DemandBillConfig;
                
                bll.SaveConfiguration(config.DemandBillConfiguration,(SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                cr.Data = null;
                cr.Message = "Text Changed";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

        [HttpPost]
        public JsonResult SavePaymentConfig(MunicipalityConfiguration.PaymentConfig PaymentConfig)
        {
            try
            {
                var bll = new MunicipalityConfigurationBll();
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                MunicipalityConfiguration config = bll.GetConfiguration(user.MunicipalityID);

                config.PaymentConfiguration = PaymentConfig;

                bll.SaveConfiguration(config.PaymentConfiguration, (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                cr.Data = null;
                cr.Message = "Text Changed";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

        [HttpPost]
        public JsonResult SaveBasicConfig(MunicipalityConfiguration.BasicInfoConfig BasicInfoConfig)
        {
            try
            {
                var bll = new MunicipalityConfigurationBll();
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                MunicipalityConfiguration config = bll.GetConfiguration(user.MunicipalityID);

                config.BasicInfoConfiguration = BasicInfoConfig;
                var userObj = SessionContext.CurrentUser as MunicipalityUser;
                bll.SaveConfiguration(config.BasicInfoConfiguration, userObj.MunicipalityID,userObj.UserID);
                cr.Data = null;
                cr.Message = "Basic information updated";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

    }
}