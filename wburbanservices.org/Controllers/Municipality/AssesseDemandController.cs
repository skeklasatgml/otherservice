﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class AssesseDemandController:Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        JsonResult js = new JsonResult();
        public AssesseDemandController()
        {
            js.Data = cr;
        }
        public ActionResult AssesseDemandUI()
        {
            try
            {
                WardBLL bll = new WardBLL();
                HoldingTypeBll hBll = new HoldingTypeBll();
                MunicipalityUser mobj = (SessionContext.CurrentUser) as MunicipalityUser;
                var holdingTypes = hBll.GetHoldingTypes(mobj.MunicipalityID).OrderBy(t=>t.HoldingTypeDesc).ToList();
                var wards= bll.GetWards((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID, "");
                ViewBag.Wards = wards;
                ViewBag.HoldingTypes = holdingTypes;
            }
            catch (Exception)
            {

            }
            
            return View("~/Views/Municipality/AssesseDemand/AssesseDemandUI.cshtml");
        }
        public JsonResult GetAllArrearBillRecord(int WardID,int LocationID,long AssesseeID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
                var datas = bll.GetAllArrearDemanBillRecord(WardID,AssesseeID,LocationID,user.MunicipalityID);
                datas.RemoveAll(t => t.FinYear == CommonUtils.GetFinYear(DateTime.Now));
                cr.Data = datas;
                cr.Message = string.Format("Total {0} records found", datas.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PropertyTaxOutstading>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

        public JsonResult UpdateArrear(List<ArrearDemand> ArrearDemands,long AssesseID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                ArrearDemandBll bll = new ArrearDemandBll();
                ArrearDemands.RemoveAll(t => t.FinYear == CommonUtils.GetFinYear(DateTime.Now));
                bll.UpdateArrears(ArrearDemands, user.UserID, AssesseID, user.MunicipalityID);
                cr.Data = true;
                cr.Message = "Arrear Updated";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }
        [HttpPost]
        public JsonResult InsertArrear(FinYearQtr FromFinYearQtr, FinYearQtr ToFinYearQtr, decimal PTax, decimal SCh, long AssesseeId)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                ArrearDemandBll bll = new ArrearDemandBll();
                bll.InsertArrears(FromFinYearQtr, ToFinYearQtr, PTax, SCh, user.UserID, AssesseeId, user.MunicipalityID);
                cr.Data = null;
                cr.Message = "Arrear Inserted Successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (AppException ex)
            {
                if(ex.ErrorCode== "InsertArrear.InvalidFinyearsQtr")
                {
                    cr.Data = new { ErrorCode=ex.ErrorCode, ErrorData = ex.Data };
                }
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }
        [HttpPost]
        public JsonResult UpdateDemand(List<ArrearDemand> ArrearDemands, long AssesseID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                ArrearDemandBll bll = new ArrearDemandBll();
                ArrearDemands.RemoveAll(t => t.FinYear != CommonUtils.GetFinYear(DateTime.Now));
                bll.UpdateDemand(ArrearDemands, user.UserID, AssesseID, user.MunicipalityID);
                cr.Data = true;
                cr.Message = "Demand Updated";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }
        [HttpPost]
        public JsonResult InsertDemand(List<ArrearDemand> ArrearDemands, long AssesseID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                ArrearDemandBll bll = new ArrearDemandBll();
                ArrearDemands.RemoveAll(t => t.FinYear != CommonUtils.GetFinYear(DateTime.Now));
                bll.InsertDemand(ArrearDemands, user.UserID, AssesseID, user.MunicipalityID);
                cr.Data = true;
                cr.Message = "Demand Inserted";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

        [HttpPost]
        public JsonResult GetDemandRecords(int WardID, int LocationID, long AssesseeID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
                var datas = bll.GetAllArrearDemanBillRecord(WardID, AssesseeID, LocationID, user.MunicipalityID);
                datas.RemoveAll(t => t.FinYear != CommonUtils.GetFinYear(DateTime.Now));
                cr.Data = datas;
                cr.Message = string.Format("Total {0} records found", datas.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<PropertyTaxOutstading>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }

        [HttpPost]
        public JsonResult InsertReview(string FromFinYear, string FromQtr, string ToFinYear, string ToQtr, decimal PTax, decimal SCh, string ReferanceNo,
                                       string ReferanceText, long AssesseeId, string ReviewID)
        {
            try
            {
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                ArrearDemandBll bll = new ArrearDemandBll();
                bll.InsertReview(FromFinYear, FromQtr, ToFinYear, ToQtr, PTax, SCh, ReferanceNo, ReferanceText, user.UserID, AssesseeId, user.MunicipalityID, ReviewID);
                cr.Data = null;
                cr.Message = "Review Sumnitted Successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return js;
        }
        public JsonResult ReviewAllData(string AssesseeID)
        {
            //JsonResult js = new JsonResult();
            //ClientJsonResult cr = new ClientJsonResult();
            try
            {
                ArrearDemandBll bll = new ArrearDemandBll();
                MunicipalityUser user = SessionContext.CurrentUser as MunicipalityUser;
                List<ReviewData_VM> pDetails = bll.Get_AllReviewData(Convert.ToInt64(AssesseeID), user.MunicipalityID);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Data found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<ReviewData_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            //js.Data = cr;
            return js;
        }
    }
}