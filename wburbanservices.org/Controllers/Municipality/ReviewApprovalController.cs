﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;


namespace Valuation_Board.Controllers.Municipality
{
    [SecuredFilter]
    public class ReviewApprovalController : Controller
    {
        // GET: ReviewApproval
        public ActionResult Index()
        {
            return View("~/Views/Municipality/ReviewApproval/Index.cshtml");
        }

        public ActionResult GetReviewData(string EffectFromDT, string EffectToDT, string ApproveStatus)
        {
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                ReviewApprovalBLL assApp = new ReviewApprovalBLL();
                MunicipalityUser uObj = (MunicipalityUser)SessionContext.CurrentUser;

                List<ReviewApproval_VM> pDetails = assApp.Get_AllReviewData(uObj.MunicipalityID, EffectFromDT, EffectToDT, ApproveStatus);
                cr.Data = pDetails;
                cr.Message = string.Format("{0} Data found", pDetails.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<ReviewApproval_VM>();
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            js.Data = cr;
            return js;
        }

        public JsonResult ReviewApprovalUpdateData(List<ReviewApproval_VM> AllCheckedData)
        {
            ReviewApprovalBLL assApp = new ReviewApprovalBLL();
            MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
            JsonResult js = new JsonResult();
            ClientJsonResult cr = new ClientJsonResult();
            try
            {
                foreach (ReviewApproval_VM ReviewApproval in AllCheckedData)
                {
                    assApp.UpdateReviewApproved(mUser.UserID, ReviewApproval);
                }
                cr.Data = null;
                cr.Message = "Successfully Approved";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}
