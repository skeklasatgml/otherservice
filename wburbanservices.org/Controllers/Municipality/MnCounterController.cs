﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.App_Codes;
using Valuation_Board.Models.Account;

namespace Valuation_Board.Controllers.Municipality
{
   [SecuredFilter]
    public class MnCounterController : Controller
    {
        // GET: Counter
        public ActionResult Index()
        {
            return View();
        }

        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MunicipalityTaxPaymentCounter_BLL objbll = new MunicipalityTaxPaymentCounter_BLL();
        [HttpPost]
        public JsonResult AddCounter(MunicipalityPaymentCounter obj)
        {
            try
            {
             MunicipalityUser mUser=(MunicipalityUser)   SessionContext.CurrentUser;
                obj.MunicipalityID = mUser.MunicipalityID;
                objbll.Insert(obj);
                cr.Data = null;
                cr.Message = "Insert Succesfull";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult CountersByMunicipalityID()
        {
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityPaymentCounter> lst= objbll.CountersByMunicipalityID(mUser.MunicipalityID);
                
                cr.Data = lst;
                cr.Message = string.Format("{0} Counters Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js; 
        }
        public JsonResult GetActiveCounters() {
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityPaymentCounter> lst = objbll.CountersByMunicipalityID(mUser.MunicipalityID).Where(t=>t.IsActive==true).ToList();

                cr.Data = lst;
                cr.Message = string.Format("{0} Counters Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
        public JsonResult GetActiveCountersForReports()
        {
            try
            {
                MunicipalityUser mUser = (MunicipalityUser)SessionContext.CurrentUser;
                List<MunicipalityPaymentCounter> lst = objbll.CountersByMunicipalityIDUserID(mUser.MunicipalityID, mUser.UserID).Where(t => t.IsActive == true).ToList();

                cr.Data = lst;
                cr.Message = string.Format("{0} Counters Found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult ChangeStatusByCounterID(string CounterID)
        {
            try
            {
                objbll.ChangeStatusByCounterID(Convert.ToInt32(CounterID));
                cr.Data = "";
                cr.Message = "Change Succefull";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }
    }
}