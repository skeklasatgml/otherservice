﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;
using static Valuation_Board.Models.Application.DprExtension;
using static Valuation_Board.Models.Application.DprModel;
using static Valuation_Board.Models.Application.StageOfProgress;

namespace Valuation_Board.Controllers.Modules
{
    [SecuredFilter]
    public class DprExtensionController : Controller
    {
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        // GET: DprExtension
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Budget()
        {
            try
            {
                using (DprBll obj = new DprBll())
                {
                    var FundType = obj.GetFundTypeForBudget();
                    return View(@"~\Views\Modules\DprExtension\Budget.cshtml", FundType);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/Budget.cshtml");

            }
        }
        public ActionResult UlbWiseContactDetails()
        {
            try
            {
                using (DprBll obj = new DprBll())
                {
                    var Designation = obj.GetDesignation();
                    return View(@"~\Views\Modules\DprExtension\UlbWiseContactDetails.cshtml", Designation);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/UlbWiseContactDetails.cshtml");

            }
        }
        [HttpGet]
        public ActionResult Tender()
        {
            try
            {
                return View(@"~\Views\Modules\DprExtension\Tender.cshtml");
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/Tender.cshtml");

            }
        }
        [HttpGet]
        public ActionResult WorkStatus()
        {
            try
            {
                return View(@"~\Views\Modules\DprExtension\WorkStatus.cshtml");
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/WorkStatus.cshtml");

            }
        }
        [HttpGet]
        public ActionResult AddVendor()
        {
            try
            {
                return View(@"~\Views\Modules\DprExtension\AddVendor.cshtml");
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/AddVendor.cshtml");

            }
        }
        [HttpGet]
        public ActionResult SurrenderInstallmentAmount()
        {
            try
            {
                return View(@"~\Views\Modules\DprExtension\SurrenderInstallmentAmount.cshtml");
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/SurrenderInstallmentAmount.cshtml");
            }
        }
        [HttpGet]
        public ActionResult ReClameSurrenderAmount()
        {
            try
            {
                return View(@"~\Views\Modules\DprExtension\ReClameSurrenderAmount.cshtml");
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/ReClameSurrenderAmount.cshtml");
            }
        }
        [HttpPost]
        public JsonResult SaveBudgetData(DprModel.Budget BudgetDetails)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddBudgetData(BudgetDetails);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveUlbContact(UlbWiseContact data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddOrUpdateUlbContact(data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = data.MunicipalityID;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult AddVendorData(VendorData data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddVendor(data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveTenderData(Tender TenderDetails)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddTenderData(TenderDetails);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveWorkStatus()
        {
            try
            {
                WorkStatus WorkStatusDtls = JsonConvert.DeserializeObject<WorkStatus>(Request.Form["WorkStatusDtls"]);
                HttpFileCollectionBase files = null;
                if (Request.Files.Count > 0)
                {
                    files = Request.Files;
                }

                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddWorkStatus(WorkStatusDtls, files);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllMunicipalites(bool considerMnId = false)
        {
            List<Municipality_Ddl_VM> mnVm = new List<Municipality_Ddl_VM>();
            try
            {

                MunicipalityBLL mnBll = new MunicipalityBLL();
                var lstmn = mnBll.GetAllMunicipalities();
                mnVm = lstmn.Select(t =>
                {
                    return new Municipality_Ddl_VM()
                    {
                        MunicipalityID = t.MunicipalityID ?? 0,
                        MunicipalityName = t.MunicipalityName
                    };
                }).ToList();
                if (considerMnId)
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    var mnId = (obj as MunicipalityUser).MunicipalityID;
                    cr.Data = mnVm.Where(x => x.MunicipalityID == mnId);
                }
                else
                    cr.Data = mnVm;
                cr.Message = string.Format("{0} Municipalities Found", mnVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = mnVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllBudgetCode()
        {
            List<BudgetCode> Bcodes = new List<BudgetCode>();
            try
            {
                DprBll obj = new DprBll();
                Bcodes = obj.GetBudgetCodes();
                cr.Data = Bcodes;
                cr.Message = string.Format("Budget Codes Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllVendorsList()
        {
            List<VendorData> VData = new List<VendorData>();
            try
            {

                DprBll obj = new DprBll();
                VData = obj.GetAllVendors();
                cr.Data = VData;
                cr.Message = string.Format("Vendors Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetProjectInstallmentDetails(SurrenderInsAmt Data)
        {
            List<SurrenderInsAmt> VData = new List<SurrenderInsAmt>();
            try
            {

                DprBll obj = new DprBll();
                VData = obj.GetAllInstAmt(Data);
                cr.Data = VData;
                cr.Message = string.Format("Installment Details Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult SaveSurrenderAmt(SurrenderInsAmt Data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.SaveSurrenderAmt(Data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveReClameAmount(SurrenderInsAmt Data)
        {
            try
            {
                DprBll obj = new DprBll();
                var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                bool result = false;
                result = obj.SaveReClameAmt(Data, userId);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllMilestone(int vendorId, string wrkorderNo)
        {
            List<WrkMilestone> VData = new List<WrkMilestone>();
            try
            {

                DprBll obj = new DprBll();
                VData = obj.GetAllMilestone(vendorId, wrkorderNo);
                cr.Data = VData;
                cr.Message = string.Format("Vendors Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllWorkStatus(int vendorId, int workCatId, string wrkorderNo)
        {
            List<WorkStatus> Bcodes = new List<WorkStatus>();
            try
            {
                var objusr = SessionContext.CurrentUser as IInternalUserMin;
                var mnId = (objusr as MunicipalityUser).MunicipalityID;
                DprBll obj = new DprBll();
                Bcodes = obj.GetWorkStatus(mnId, vendorId, workCatId, wrkorderNo);
                cr.Data = Bcodes;
                cr.Message = string.Format("Budget Codes Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllTenders()
        {
            List<Tender> Bcodes = new List<Tender>();
            try
            {
                var objusr = SessionContext.CurrentUser as IInternalUserMin;
                var mnId = (objusr as MunicipalityUser).MunicipalityID;
                DprBll obj = new DprBll();
                Bcodes = obj.GetAllTendersData(mnId);
                cr.Data = Bcodes;
                cr.Message = string.Format("Budget Codes Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllVendors(string VendorName = null, bool considerMnId = false)
        {
            List<VendorDropDwn> Vendors = new List<VendorDropDwn>();
            try
            {
                var objusr = SessionContext.CurrentUser as IInternalUserMin;
                var mnId = (objusr as MunicipalityUser).MunicipalityID;
                DprBll obj = new DprBll();
                if (considerMnId)
                    Vendors = obj.GetVendors(VendorName, mnId);
                else
                    Vendors = obj.GetVendors(VendorName);
                cr.Data = Vendors;
                cr.Message = string.Format("Vendors Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllWorkTypes()
        {
            List<WorkTypDropDwn> WorkTypes = new List<WorkTypDropDwn>();
            try
            {
                DprBll obj = new DprBll();
                WorkTypes = obj.GetWTypes();
                cr.Data = WorkTypes;
                cr.Message = string.Format("Work Types Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetBudgetDtl(int? Mnid, string BCode, string FinYr)
        {
            Budget budgetDetails = new Budget();
            try
            {
                DprBll obj = new DprBll();
                budgetDetails = obj.GetBudgetDetails(Mnid, BCode, FinYr);
                cr.Data = budgetDetails;
                cr.Message = string.Format("Budget Codes Found");
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetAllBudgets(int mnId, string finYr)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.GetAllBudgets(mnId, finYr);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetDesigList(int mnId)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.GetDesigList(mnId);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllFundSource()
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.GetAllFundSource();
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllFundSrcAndSchmCat()
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.GetAllFundSrcAndSchmCat();
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllProjectNos(string ProjNo, bool considerMnId = false)
        {
            try
            {
                DprBll obj = new DprBll();
                List<ProjectNoDropDwn> result = new List<ProjectNoDropDwn>();
                if (considerMnId)
                {
                    var Objct = SessionContext.CurrentUser as IInternalUserMin;
                    var mnId = (Objct as MunicipalityUser).MunicipalityID;
                    result = obj.GetAllProjectNos(ProjNo, mnId);
                }
                else
                    result = obj.GetAllProjectNos(ProjNo);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllSubProjectNos(string SubProjNo, string ProjNo, bool considerMnId = false)
        {
            try
            {
                DprBll obj = new DprBll();
                List<SubProjectNoDropDwn> result = new List<SubProjectNoDropDwn>();
                if (considerMnId)
                {
                    var Objct = SessionContext.CurrentUser as IInternalUserMin;
                    var mnId = (Objct as MunicipalityUser).MunicipalityID;
                    result = obj.GetAllSubProjectNos(SubProjNo, ProjNo, mnId);
                }
                else
                    result = obj.GetAllSubProjectNos(SubProjNo, ProjNo);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllWorkOrder(string VendorName, string WrkOrdrNo)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.GetAllWrkOrdr(VendorName, WrkOrdrNo);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        public ActionResult FundSource()
        {
            try
            {
                using (DprBll obj = new DprBll())
                {
                    var FundType = obj.GetFundTypeForBudget();
                    return View(@"~\Views\Modules\DprExtension\FundSource.cshtml", FundType);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/FundSource.cshtml");
            }
        }
        public ActionResult SubProjectType()
        {
            try
            {
                using (DprBll obj = new DprBll())
                {
                    return View(@"~\Views\Modules\DprExtension\SubprojType.cshtml");
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/SubprojType.cshtml");
            }
        }
        [HttpPost]
        public JsonResult GetStageGroup(int? Id = null)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.getStageGroups(Id);
                if (result.Count == 0)
                {
                    throw new InvalidOperationException("Data Not Found");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetSubProjectType(int? Id = null)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.getSubProjType(Id);
                //if (result.Count == 0)
                //{
                //    throw new InvalidOperationException("Data Not Found");
                //}
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult DelSubProjectType(int Id)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.DelSubProjType(Id);
                if (!result)
                {
                    throw new InvalidOperationException("Sorry!! Data not deleted!!");
                }
                cr.Data = result;
                cr.Message = "Data Found";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveSubProjType(SubProjectType data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddOrEditSpt(data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult SaveFundSource(FundSourceMaster data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddOrEditFundSource(data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
        public ActionResult SchemeCategory()
        {
            try
            {
                using (DprBll obj = new DprBll())
                {
                    var FundType = obj.GetFundTypeForBudget();
                    return View(@"~\Views\Modules\DprExtension\SchemeCategory.cshtml", FundType);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~/Views/Modules/DprExtension/SchemeCategory.cshtml");
            }
        }
        [HttpPost]
        public JsonResult SaveSchemeCategory(SchemeCategoryMaster data)
        {
            try
            {
                DprBll obj = new DprBll();
                bool result = false;
                result = obj.AddOrEditSchemeCat(data);
                if (!result)
                {
                    throw new InvalidOperationException("Data Not Saved");
                }
                cr.Data = result;
                cr.Message = "Data save successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }

        #region stage of progress
        public ActionResult StageGroupMasterui()
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    var model = new StageOfProgressVM();
                    var groups = obj.getStageGroups();
                    obj.includeMileStone(groups);
                    model.StageGroups = groups;
                    return View(@"~\Views\Modules\StageOfProgress\StageGroupMaster.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                AppException appExp = new AppException(ex.Message, "");
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\StageOfProgress\StageGroupMaster.cshtml");

            }
        }
        public ActionResult GetWorkMileStones(int groupId)
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    var group = obj.getMileStones(groupId);
                    cr.Data = group;
                    cr.Message = string.Format("{0} value effected", group.Count);
                    cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                cr.Data = new List<object>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        public ActionResult SaveStageGroup(IdVal<int?, string> payload)
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    payload = payload ?? throw new Exception("Data can not be null");
                    payload.Value = payload.Value ?? throw new Exception("Group name can not be empty");
                    var group = obj.SaveStageGroup(payload);
                    cr.Data = group;
                    cr.Message = "Stage group saved sucessfully";
                    cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        public ActionResult SaveChangeMileStone(WorkMileStone payload, string mode)
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    payload = payload ?? throw new Exception("Data can not be null");
                    if (new List<string> { "add", "edit", "delete" }.FirstOrDefault(t => string.Equals(mode, t, StringComparison.OrdinalIgnoreCase)) != null)
                    {
                        if (new List<string> { "add", "edit" }.FirstOrDefault(t => string.Equals(mode, t, StringComparison.OrdinalIgnoreCase)) != null)
                        {
                            payload.Dscr = string.IsNullOrEmpty(payload.Dscr) ? throw new Exception("Group name can not be empty") : payload.Dscr;
                            payload.PerComplited = (payload.PerComplited < 0 || payload.PerComplited > 100) ? throw new Exception("Invalid Percentage. Value should be with in 0 to 100") : payload.PerComplited;
                            var group = obj.SaveMileStone(payload);
                            cr.Data = group;
                            cr.Message = "Milestone saved sucessfully";
                        }
                        else
                        {
                            //delete
                            obj.DeleteMileStone(payload.Id ?? 0);
                            cr.Message = "Milestone delete";
                        }

                        cr.Status = ResponseStatus.SUCCESS;
                    }
                    else
                    {
                        throw new Exception("Invalid Mode Selected");
                    }

                }
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult MapMilestoneScemeCatUI()
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    using (DprBll dpr = new DprBll())
                    {
                        var model = new ScemeCatMilestoneMapping();
                        model.SchemeTypes = dpr.GetSchemeTypes();
                        model.Groups = obj.getStageGroups().Select(t => new IdVal<int, string> { Id = t.Id, Value = t.Value }).ToList();
                        return View(@"~\Views\Modules\StageOfProgress\MapScemeCategoryMilestone.cshtml", model);
                    }

                }
            }
            catch (Exception ex)
            {
                AppException appExp = new AppException(ex.Message, ""); 
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\StageOfProgress\MapScemeCategoryMilestone.cshtml");

            }
        }
        [HttpPost]
        public ActionResult GetMileStoneGroupedMappedSchemeCats(int? schemeTypeId, int? groupId)
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    var rslt = obj.getSchemeCategoriesMappedMilestoneGrp(schemeTypeId, groupId);
                    cr.Data = rslt;
                    cr.Message = string.Format("{0} data effected", rslt.Count);
                    cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                cr.Data = new List<object>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public ActionResult updateMappingSchemeteMilestoneGrp(int? scemeCatId, int? groupId, bool isMapped)
        {
            try
            {
                using (StageOfProgressBll obj = new StageOfProgressBll())
                {
                    obj.updateMappingSchemeteMilestoneGrp(scemeCatId, groupId, isMapped);
                    cr.Data = null;
                    cr.Message = isMapped ? "Successfully mapped" : "Successfully un-mapped";
                    cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        #endregion
        [HttpPost]
        public JsonResult CheckWrkStatusDtWithWrkCompltnDt(string WrkStatusDt,int VId, string WorkOrderNo)
        {
            try
            {
                DprBll obj = new DprBll();
                var result = obj.IsWrkStatusDtGtrThnWrkCompltnDt(WrkStatusDt, VId, WorkOrderNo);
                cr.Data = result;
                if(result==true)
                {
                    cr.Message = "Work status date is getter than work completion date i.e delay to comlete";
                }
                else
                {
                    cr.Message = "Work status date is less than work completion date i.e no delay in work completion";
                }
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Data = null;
            }
            return Json(cr);
        }
    }
}