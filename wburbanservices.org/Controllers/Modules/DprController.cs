﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using static Valuation_Board.Models.Application.DprModel;
using static Valuation_Board.Models.Application.DprModel.DprDashboard;
using static Valuation_Board.Models.Application.DprModel.DprFundRelease;



namespace Valuation_Board.Controllers.Modules
{
    [SecuredFilter]
    public class DprController : Controller
    {
        protected DbHandler _db;
        protected SqlConnection cn;
        ClientJsonResult c = new ClientJsonResult();
        string _viewPath = @"~\Views\Modules\Dpr\";
        log4net.ILog logger = log4net.LogManager.GetLogger("default logger");

        public DprController()
        {
            _db = new DbHandler();
            cn = _db.NewConnection();
        }

        [HttpGet]
        public ActionResult DprListUi(string mode,int isCalledFromDashBoard=0, string DateType=null)
        {
            using (DprBll dprDprBll = new DprBll())
            {
                TempData["isCalledFromDashBoard"] = Convert.ToString(isCalledFromDashBoard);
                TempData["DateType"] = Convert.ToString(DateType);
                if (string.Equals(mode, "menu", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        int? municipality = null;
                        var user = SessionContext.CurrentUser as IInternalUserMin;
                        if (user.UserType == UserType.AdministrativeUser)
                        {
                            //skip
                        }
                        else if (user.UserType == UserType.MunicipalityUser)
                        {
                            municipality = (user as MunicipalityUser).MunicipalityID;
                        }
                        var onLoadData = dprDprBll.GetDashBoardOnLoadData(municipality);
                        if (user.UserType == UserType.AdministrativeUser)
                        {
                            List<int> MnIds = new List<int>();
                            var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                            var Mnids = dprDprBll.GetMnIdForLoginDistrictByUserId(userId);
                            if(Mnids.Count>0)
                            {
                                onLoadData.Municipalities = onLoadData
                               .Municipalities
                               .Where(t => Mnids.Any(y => {
                                   return y == t.MunicipalityID;
                               }))
                               .ToList();
                                onLoadData.FilterConfig.AddRemoveMunicipality = false;
                            }
                            
                        }
                        if (user.UserType == UserType.MunicipalityUser)
                        {
                            onLoadData.Municipalities = onLoadData
                                .Municipalities
                                .Where(t => t.MunicipalityID == (user as MunicipalityUser).MunicipalityID)
                                .ToList();
                            onLoadData.FilterConfig.AddRemoveMunicipality = false;
                        }
                        onLoadData.SchemeCategories = dprDprBll.GetSchemeCategories(null);
                        onLoadData.SchemeTypes = dprDprBll.GetSchemeTypes();
                        return View(@"~\Views\Modules\Dpr\DprDashBoard.cshtml", onLoadData);
                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return PartialView(@"~\Views\Modules\Dpr\DprDashBoard.cshtml");
                    }
                }
                else
                {

                    var model = new DprListModel();
                    try
                    {

                        var authReports = new List<object>();
                        var authActions = new List<object>();

                        var user = SessionContext.CurrentUser as IInternalUserMin;
                        MunicipalityBLL bll = new MunicipalityBLL();

                        model.Ulbs = bll.GetAllMunicipalities().Select(t => new KeyValue<int, string> { Key = t.MunicipalityID ?? 0, Value = t.MunicipalityName }).ToList();
                        if (user.UserType == UserType.AdministrativeUser)
                        {
                            authReports.Add(new { code = "rp1", description = "report form" });
                            authActions.Add(new { code = "av", description = "approval form link" });
                            List<int> MnIds = new List<int>();
                            var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                            var Mnids = dprDprBll.GetMnIdForLoginDistrictByUserId(userId);
                            if(Mnids.Count>0)
                            {
                                model.Ulbs = model.Ulbs
                               .Where(t => Mnids.Any(y => {
                                   return y == t.Key;
                               }))
                               .ToList();
                            }
                        }
                        else if (user.UserType == UserType.MunicipalityUser)
                        {
                            authReports.Add(new { code = "rp1", description = "report form" });
                            model.CurrUlbId = (user as MunicipalityUser).MunicipalityID;
                            model.Ulbs.RemoveAll(t => t.Key != model.CurrUlbId);
                        }
                        model.SchemeTypes = dprDprBll.GetSchemeTypes();
                        model.SchemeCategories = dprDprBll.GetSchemeCategories(null);
                        return View(@"~\Views\Modules\Dpr\DprList.cshtml", model);
                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return PartialView(@"~\Views\Modules\Dpr\DprDashBoard.cshtml");
                    }
                }
            }
        }
        private List<Dpr> getDprList(DprMinDashPayloadStat payload)
        {
            var isCalledFromDashBoard =Convert.ToString(TempData["isCalledFromDashBoard"]);
            var DateType = Convert.ToString(TempData["DateType"]);
            if (payload == null)
            {
                throw new Exception("Invalid payload");
            }
            if (!new List<string> { "P", "S", "A", "RT", "RL", "AL", "DV","IA", "SD","SC","MD" }.Any(s => s.Equals(payload.Status, StringComparison.OrdinalIgnoreCase)))
            {
                //--S=>submitted,A=>approved,Rt=>returned,RL=>released,Al=>all
                throw new Exception(string.Format("Invalid DPR Status {0}", payload.Status));
            }
            if(DateType !=null && DateType.Length>0)
            {
                payload.Status = DateType.ToUpper();
                payload.MyList = "N";
            }
            else
            {
                payload.Status = payload.Status.ToUpper();
            }
            payload.MunicipalityIds = payload.MunicipalityIds ?? new List<int>();
            using (DprBll form = new DprBll())
            {
                var data = new List<DprModel.Dpr>();
                long? userId = null;
                int? devAuthId = null;
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                    devAuthId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    payload.MunicipalityIds = new List<int>();
                    payload.MunicipalityIds.Add(devAuthId ?? 0);
                }
                else if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.AdministrativeUser)
                {
                    List<int> MnIds = new List<int>();
                    userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                    var Mnids=form.GetMnIdForLoginDistrictByUserId(userId);
                    if(Mnids.Count>0)
                    {
                        payload.MunicipalityIds.AddRange(Mnids);
                    }
                    // devAuthId = UlbId;
                    //skip
                }
                else
                {
                    throw new Exception("Current User not support this resource call");
                }
                userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                data = form.GetDprList(userId, payload);
                data = form.IncludeFundReleaseStatus(data);
                return data;
            }
        }

        [HttpGet]
        public ActionResult Excel()
        {
            try
            {
                decimal Total = 0; string Position = ""; string SumFieldName = "";
                string Name = "UnReleased Installment";

                string fileName = Name + "_" + ".xls";

                var cmd = _db.NewCommand("Udma.Proc_Get_Reports", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@fdate", "");
                    cmd.Parameters.AddWithValue("@tdate", "");
                    cmd.Parameters.AddWithValue("@munids", "");
                    cmd.Parameters.AddWithValue("@transtype", "UnReleasedFunds");
                    cmd.Parameters.AddWithValue("@oldnew", "");
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    cn.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            string saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        [HttpPost]
        public string DprList(DprMinDashPayloadStat payload)
        {
            try
            {
                var data = getDprList(payload);
                c.Data = data;
                c.Message = string.Format("{0} datas found", data.Count);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            var objJSS = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return objJSS.Serialize(c);
            //return Json(c);
        }
        public ActionResult ExportDprList(string _payload)
        {
            byte[] d = Convert.FromBase64String(HttpUtility.UrlDecode(_payload));
            string decodedString = Encoding.UTF8.GetString(d);
            DprMinDashPayloadStat payload = JsonConvert.DeserializeObject<DprMinDashPayloadStat>(decodedString);

            var data = getDprList(payload);

            DataTable table = new DataTable();
            table.Columns.Add("#", typeof(string));
            table.Columns.Add("Dev Auth", typeof(string));
            table.Columns.Add("Memo No", typeof(string));
            table.Columns.Add("Memo Date", typeof(string));
            table.Columns.Add("Name Of Work", typeof(string));
            table.Columns.Add("Project No", typeof(string));
            table.Columns.Add("Doc. Verified on", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("Released/ Approved", typeof(string));


            int i = 1;
            data.ForEach(t =>
            {
                table.Rows.Add(i, t.MunicipalityDevAuthoName, t.MemoNumber, t.MemoDate.ToStrDate(), t.NameOfWork, t.ProjectNo, t.ProjectDate.ToStrDate(), t.dprProgStat, string.Format("{0}/{1}", t.TotalReleasedAmnt, t.TotalApprovedAmount));
                i++;
            });
            table.Rows.Add("", "", "", "", "", "", "", "", string.Format("{0}/{1}", data.Sum(t => t.TotalReleasedAmnt), data.Sum(t => t.TotalApprovedAmount)));
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table, "Work Book");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("DPR List- from {0} to {1}.xlsx", payload.FromDate.ToStrDate("dd'-'MM'-'yyyy"), payload.ToDate.ToStrDate("dd'-'MM'-'yyyy")));
                }
            }
        }
        [HttpGet]
        public ActionResult DprNewForm()
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    if (obj.UserType != UserType.MunicipalityUser)
                    {
                        throw new Exception("Current user type not supported to avail this form");
                    }
                    return View(@"~\Views\Modules\Dpr\DprForm.cshtml", form.GetEmptyForm((obj as MunicipalityUser).MunicipalityID, obj.UserID));
                }

            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\Dpr\DprForm.cshtml");

            }
        }
        [HttpPost]
        public ActionResult SubmitForm(DprModel.Master form)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    if (obj.UserType != UserType.MunicipalityUser)
                    {
                        throw new Exception("Current user type not supported to avail this form");
                    }
                    var municipalityId = (obj as MunicipalityUser).MunicipalityID;
                    var userId = obj.UserID;
                    var submittedDpr = bll.SubmitDpr(form, municipalityId, userId);
                    c.Data = null;
                    c.Message = string.Format("DPR saved succesfully. <a target='_blank' href='{0}'>Click here</a> to Print <b>CheckList Form</b>", "/Modules/dpr/DprReport?mstDprId=" + submittedDpr.MstDprId);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult UploadImage(string RandId, Base64File file)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var StageId = bll.SaveFileToStage(file);
                    c.Data = StageId;
                    c.Message = string.Format("File Saved!!");
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = 0;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult StageFileByFormData()
        {

            try
            {
                if (Request.Files.Count < 1)
                {
                    throw new Exception("No files selected.");
                }
                long? StageId = null;
                //  Get all files from Request object  
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                    //string filename = Path.GetFileName(Request.Files[i].FileName);  

                    HttpPostedFileBase file = files[i];
                    string fname;

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }

                    // Get the complete folder path and store the file inside it.  
                    using (DprBll bll = new DprBll())
                    {
                        var data = bll.SaveFileToStage(file);
                        StageId = Convert.ToInt64(data[0]);
                        c.Data = StageId;
                        c.Message = string.Format("File Uploaded Successfully!");
                        c.FileLink = data[1];
                        c.Status = ResponseStatus.SUCCESS;
                    }
                    break;
                }
            }
            catch (Exception ex)
            {
                c.Data = 0;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        public ActionResult DprFinalSubmitionUI(long mstDprId)
        {
            using (DprBll dprDprBll = new DprBll())
            {
                try
                {
                    var user = SessionContext.CurrentUser as IInternalUserMin;
                    if (user.UserType == UserType.MunicipalityUser)
                    {
                        var _user = SessionContext.CurrentUser as MunicipalityUser;
                        var dprs = dprDprBll.GetDprList(_user.MunicipalityID, user.UserID, null, null, "AL", mstDprId);
                        dprs = dprs != null && dprs.Count > 0 ? dprs : throw new Exception("DPR Not found");
                        return View(@"~\Views\Modules\Dpr\dprFinalSubmition.cshtml", dprs[0]);
                    }
                    else
                    {
                        throw new Exception("Invalid User Requested");
                    }

                }
                catch (Exception ex)
                {
                    AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                    ViewBag.error = appExp;
                    return PartialView(@"~\Views\Modules\Dpr\dprFinalSubmition.cshtml");
                }
            }
        }
        [HttpPost]
        public ActionResult DprFinalSubmit(long mstDprId, Base64File file)
        {
            using (DprBll DprBll = new DprBll())
            {
                try
                {
                    if (file == null)
                        throw new Exception("Please upload checklist scancopy");
                    var user = SessionContext.CurrentUser as IInternalUserMin;
                    if (user.UserType == UserType.MunicipalityUser)
                    {
                        var _user = SessionContext.CurrentUser as MunicipalityUser;
                        var submittedDpr = DprBll.DprFinalSubmit(mstDprId, file, _user.MunicipalityID, user.UserID);
                        c.Data = null;
                        c.Message = string.Format("DPR submitted successfully & checklist form uploaded and your document verification date is {0} ", String.Format("{0:dd/MM/yyyy}", submittedDpr.DocVerificationDate));
                        c.Status = ResponseStatus.SUCCESS;
                    }
                    else
                    {
                        throw new Exception("Invalid User Requested");
                    }
                }
                catch (Exception ex)
                {
                    c.Data = null;
                    c.Message = ex.Message;
                    c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                }
            }
            return Json(c);
        }
        [HttpGet]
        public ActionResult GetDprDraftList()
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    var lst = new List<DprModel.DprDraft>();
                    if (obj.UserType == UserType.MunicipalityUser)
                    {
                        var mnUser = obj as MunicipalityUser;
                        lst = form.GetDraftList(mnUser.MunicipalityID, mnUser.UserID, null);
                    }
                    else
                    {
                        //load all undeleted draft
                        lst = form.GetDraftList(null, null, null);
                    }
                    return View(@"~\Views\Modules\Dpr\DprDraftList.cshtml", lst);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\Dpr\DprDraftList.cshtml");
            }
        }
        [HttpGet]
        public ActionResult DprFormByDraft(string identityStamp)
        {
            string viewPath = _viewPath + "OldDprEntry/OldDprEntryOrUpdateForm.cshtml";
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    if (obj.UserType != UserType.MunicipalityUser)
                    {
                        throw new Exception("Current user type not supported to avail this form");
                    }
                    bool isMunicipalityUser = false;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        isMunicipalityUser = true;
                    }
                    var mnUser = obj as MunicipalityUser;
                    var lst = form.GetDraftList(mnUser.MunicipalityID, mnUser.UserID, identityStamp);
                    if (lst.Count == 0)
                        throw new Exception("Draft not found");
                    var mstObj = lst[0].DprMaster;
                    var s = mstObj.SchemeCategories.Where(t => t.SchemeTypeId == mstObj.SchemeTypeId).ToList();
                    if(mstObj.IsOldDpr==true)
                    {
                        if (isMunicipalityUser)
                        {
                            mstObj.MunicipalityId = mnUser.MunicipalityID;
                        }
                        return View(viewPath, mstObj);
                    }
                    return View(@"~\Views\Modules\Dpr\DprForm.cshtml", mstObj);
                }

            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\Dpr\DprForm.cshtml");
            }
        }
        [HttpPost]
        public ActionResult SaveDprAsDraft(DprModel.Master form)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    if (obj.UserType != UserType.MunicipalityUser)
                    {
                        throw new Exception("Current user type not supported to avail this form");
                    }
                    var municipalityId = (obj as MunicipalityUser).MunicipalityID;
                    var userId = obj.UserID;
                    bll.SaveAsDraft(JsonConvert.SerializeObject(form), municipalityId, userId, form.IdentityStamp,false);
                    c.Data = true;
                    c.Message = "DPR saved as draft";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = false;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        public ActionResult SaveAsDraftOldDpr(OldDpr form)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    if (obj.UserType != UserType.MunicipalityUser)
                    {
                        throw new Exception("Current user type not supported to avail this form");
                    }
                    var municipalityId = (obj as MunicipalityUser).MunicipalityID;
                    var userId = obj.UserID;
                    form.MunicipalityId = municipalityId;
                    form.MunicipalityName =bll.GetMunicipalityNameByMnId(municipalityId);
                    bll.SaveAsDraft(JsonConvert.SerializeObject(form), municipalityId, userId, form.IdentityStamp,true);
                    c.Data = true;
                    c.Message = "DPR saved as draft";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = false;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #region Eklas, Dated: 27/10/2018.  Work: Dpr verifaction or return
        /// <summary>
        /// Basically doc verification
        /// </summary>
        /// <returns></returns>
        public ActionResult DprApproveOrReturn(long DPChkSlId)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    ViewBag.DPChkSlId = DPChkSlId;
                    return View(@"~\Views\Modules\Dpr\DprApproveOrReturn.cshtml", form.GetParticularListByDPChkSlId(DPChkSlId));
                }

            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(@"~\Views\Modules\Dpr\DprApproveOrReturn.cshtml");

            }
        }
        [HttpPost]
        public ActionResult VerifyDpr(DprModel.Master form)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    var userId = obj.UserID;
                    bll.verifyDpr(form, userId);
                    c.Data = null;
                    c.Message = "DPR Processed Successfully";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        public ActionResult UpdateDocVerificationDt()
        {
            return View(@"~\Views\Modules\Dpr\UpdtDocVerificationDt.cshtml");
        }
        [HttpPost]
        public JsonResult GetDprListByDocVrifyDtOrMemoNo(DateTime? dateFrom, DateTime? dateTo, string memoNo)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var data = new List<DprModel.Dpr>();
                    long? userId = null;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType != UserType.AdministrativeUser)
                    {
                        throw new Exception("Current User not support this resource call");
                    }

                    data = form.GetFilteredDprList(userId, dateFrom, dateTo, memoNo);
                    c.Data = data;
                    c.Message = string.Format("{0} datas found", data.Count);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult UpdateVerifyDate(long mstDprId, DateTime? uvDate)
        {
            try
            {
                if (uvDate == null)
                {
                    throw new Exception("Please select new date");
                }
                long? userId = null;
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                }
                using (DprBll form = new DprBll())
                {

                    var data = form.updateVieificationDateByNewDate(mstDprId, uvDate, userId);
                    c.Data = data;
                    c.Message = "Successfully Updated";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion

        #region approval
        [HttpPost]
        public ActionResult Approval_GetDprApporvalUI(int mstDprId)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    var x = form.GetDprDetailsForApproval(mstDprId, obj.UserID);
                    return PartialView(@"~\Views\Modules\Dpr\DprAppovalUI.cshtml", x);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return PartialView(@"~\Views\Modules\Dpr\DprAppovalUI.cshtml");
            }
        }
        [HttpPost]
        public ActionResult Approval_ChangeDprAllocationSatus(ChangeDprApproval payload)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    var user = SessionContext.CurrentUser as IInternalUserMin;
                    bll.ChangeStatus(payload, user.UserID);
                    c.Data = null;
                    c.Status = ResponseStatus.SUCCESS;
                    c.Message = "DPR status successfully changed";
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }

        [HttpPost]
        public ActionResult Approval_GenerateAAFS(ChangeDprApproval payload)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    form.ChangeStatus(payload, obj.UserID, true);
                    c.Data = 1;
                    c.Message = "Generated";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public ActionResult Approval_GenerateReleaseOrder(ChangeDprApproval payload)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    form.ChangeStatus(payload, obj.UserID, true);
                    c.Data = 1;
                    c.Message = "Generated";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public ActionResult SaveInternalMessage(InterMessage payload)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    form.SaveInternalMessage(payload, obj.UserID);
                    c.Data = null;
                    c.Status = ResponseStatus.SUCCESS;
                    c.Message = "DPR status successfully changed";
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion


        #region Reports
        [HttpGet]
        public RedirectResult DprReport(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(19, _params));
        }

        [HttpGet]
        public RedirectResult RptProjectDetails(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(29, _params));
        }
        [HttpGet]
        public RedirectResult RptFileCover(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(28, _params));
        }
        [HttpGet]
        public RedirectResult RptReturnMemo(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(27, _params));
        }
        [HttpGet]
        public RedirectResult RptAcknowledgement(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(26, _params));
        }
        [HttpGet]
        public RedirectResult RptAcknowledgementFundRelease(long mstDprId, long fundReleaseId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            _params.Add("FundReleaseId", fundReleaseId);
            return Redirect(rt.GetReportViewerURL(30, _params));
        }
        public RedirectResult Rpt_AA_And_FS(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("P_DPChkSlId", mstDprId);
            return Redirect(rt.GetReportViewerURL(31, _params));
        }
        public RedirectResult Rpt_NoteSheet(long mstDprId)
        {
            ReportUtils rt = new ReportUtils();
            Dictionary<string, object> _params = new Dictionary<string, object>();
            _params.Add("mstDprId", mstDprId);
            return Redirect(rt.GetReportViewerURL(40, _params));
        }

        [HttpPost]
        public JsonResult geturl_Rpt_AverageTimeDpr(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                int? municipalityId = null;
                dprMinDashPayload.MunicipalityIds = dprMinDashPayload.MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    dprMinDashPayload.MunicipalityIds = new List<int>();
                    dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("dateForm", dprMinDashPayload.FromDate.ToDbDate(null));
                _params.Add("dateTo", dprMinDashPayload.ToDate.ToDbDate(null));
                _params.Add("P_MunicipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("reportName", "average-time");
                c.Data = rt.GetReportViewerURL(32, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult geturl_Rpt_FundreleaseFinYearWise(string FromFinYear, string ToFinYear, List<int> MunicipalityIds, int? AuthorityTypeId)
        {
            try
            {
                FromFinYear = FromFinYear.IsValidFinYear() ? FromFinYear : throw new Exception(string.Format("Invalid From Fin Year {0}", FromFinYear));
                ToFinYear = ToFinYear.IsValidFinYear() ? ToFinYear : throw new Exception(string.Format("Invalid To Fin Year {0}", ToFinYear));
                MunicipalityIds = MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    MunicipalityIds.Add((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                }
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", MunicipalityIds));
                _params.Add("authorityTypeId", AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", "date-range");
                _params.Add("dateForm", new FinYearQtr(FromFinYear).GetFinYearFirstDate().ToDbDate());
                _params.Add("dateTo", new FinYearQtr(ToFinYear).GetFinYearLastDate().ToDbDate());
                _params.Add("reportName", "fund-release");
                _params.Add("P_MunicipalityIdList", string.Join(",", MunicipalityIds));
                c.Data = rt.GetReportViewerURL(33, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult geturl_Rpt_ProjectStatusSummary(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                int? municipalityId = null;
                dprMinDashPayload.MunicipalityIds = dprMinDashPayload.MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    dprMinDashPayload.MunicipalityIds = new List<int>();
                    dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("dateForm", dprMinDashPayload.FromDate.ToDbDate(null));
                _params.Add("dateTo", dprMinDashPayload.ToDate.ToDbDate(null));
                _params.Add("reportName", "project-summary");
                c.Data = rt.GetReportViewerURL(34, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult geturl_Rpt_ProposalDetailsReport(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                int? municipalityId = null;
                dprMinDashPayload.MunicipalityIds = dprMinDashPayload.MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    dprMinDashPayload.MunicipalityIds = new List<int>();
                    dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_MunicipalityDevAuthoID", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate(null));
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate(null));
                c.Data = rt.GetReportViewerURL(35, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult geturl_Rpt_YearWiseFundReq(DateTime? date, List<int> MunicipalityIds)
        {
            try
            {
                MunicipalityIds = MunicipalityIds ?? new List<int>();
                date = date ?? DateTime.Now;
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    MunicipalityIds.Add((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("ULBSid", MunicipalityIds.Count > 0 ? string.Join(",", MunicipalityIds) : "ALL");
                _params.Add("FinYear", date.ToDbDate(null));
                c.Data = rt.GetReportViewerURL(36, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult geturl_Rpt_SchemeCatgoryWiseProjectDtls(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                int? municipalityId = null;
                dprMinDashPayload.MunicipalityIds = dprMinDashPayload.MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    dprMinDashPayload.MunicipalityIds = new List<int>();
                    dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("dateForm", dprMinDashPayload.FromDate.ToDbDate(null));
                _params.Add("dateTo", dprMinDashPayload.ToDate.ToDbDate(null));
                c.Data = rt.GetReportViewerURL(38, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        public JsonResult geturl_Rpt_FundReleaseSummeryReport(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                int? municipalityId = null;
                dprMinDashPayload.MunicipalityIds = dprMinDashPayload.MunicipalityIds ?? new List<int>();
                if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                {
                    municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    dprMinDashPayload.MunicipalityIds = new List<int>();
                    dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                }

                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_MunicipalityDevAuthoID", string.Join(",", dprMinDashPayload.MunicipalityIds));
                _params.Add("authorityTypeId", dprMinDashPayload.AuthorityTypeId.ReplaceNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate(null));
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate(null));
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(39, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult SynopsisOfRSP(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(42, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
            //return Redirect(rt.GetReportViewerURL(42, _params));
        }
        [HttpPost]
        public JsonResult RSPDraft(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(43, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
            //return Redirect(rt.GetReportViewerURL(43, _params));
        }
        [HttpPost]
        public JsonResult SchemeWiseUC(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(44, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
            //return Redirect(rt.GetReportViewerURL(44, _params));
        }
        [HttpPost]
        public JsonResult YearWiseUC(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(45, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
            //return Redirect(rt.GetReportViewerURL(45, _params));
        }
        [HttpPost]
        public JsonResult CategoryWiseCount(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(46, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
           // return Redirect(rt.GetReportViewerURL(46, _params));
        }
        [HttpPost]
        public JsonResult PhysicalProgressOfProject(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                c.Data = rt.GetReportViewerURL(47, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
           // return Redirect(rt.GetReportViewerURL(47, _params));
        }
        [HttpPost]
        public JsonResult RPT_ProjApprvInFavOfMuni(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(48, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
            // return Redirect(rt.GetReportViewerURL(47, _params));
        }
        [HttpPost]
        public JsonResult RPT_ULBOrDAWiseAAFS(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(49, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_FundReleaseSummary(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(50, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_ProjWiseFundReleased(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(54, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_ProjWiseFundReleaseDetail(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(51, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_AAFSSummary(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                var fgghh = string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(52, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_PhasingSummary(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(53, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_DPRSubmittedButAAFSNotIssued(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(55, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpGet]
        public ActionResult RPT_DPRSubmittedButAAFSNotIssuedExcel(List<int?> MunicipalityIds, string TimeSpanFlag, DateTime? FromDate, DateTime? ToDate, int? SchemeTypeId, int? SchemeCategoryId)
        {
            try
            {
                decimal Total = 0; string Position = ""; string SumFieldName = "";
                string Name = "DPR Submitted But AAFS Not Issued";

                string fileName = Name + "_" + ".xls";

                var cmd = _db.NewCommand("Udma.RPT_DPRSubmittedButAAFSNotIssuedExcel", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@municipalityIdList", string.Join(",", MunicipalityIds).ReplaceDbNull());
                cmd.Parameters.AddWithValue("@P_FromDate", FromDate.ToDbDate().ReplaceDbNull());
                cmd.Parameters.AddWithValue("@P_ToDate", ToDate.ToDbDate().ReplaceDbNull());
                cmd.Parameters.AddWithValue("@timeSpanFlag", TimeSpanFlag.ReplaceNull());
                cmd.Parameters.AddWithValue("@schemeTypeId", SchemeTypeId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@schemeCategoryId", SchemeCategoryId.ReplaceDbNull());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    var workbook = new HSSFWorkbook();
                    var sheet = workbook.CreateSheet();

                    //Create a header row
                    var headerRow = sheet.CreateRow(0);

                    string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    for (int j = 0; j < columnNames.Length; j++)
                    {
                        headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                    }
                    int rowNumber = 1;
                    var row = sheet.CreateRow(rowNumber);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Create a new Row
                        row = sheet.CreateRow(rowNumber++);

                        string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNamess.Length; j++)
                        {
                            row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                        }

                        if (SumFieldName != "")
                        {
                            DataColumnCollection columns = dt.Columns;
                            if (columns.Contains(SumFieldName))
                            {
                                string value = dt.Rows[i][SumFieldName].ToString();
                                Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                Position = dt.Columns.IndexOf(SumFieldName).ToString();
                            }
                            else
                            {
                                Total = Total + 0;
                            }
                        }
                    }

                    if (SumFieldName != "")
                    {
                        // Add a row for the detail row
                        row = sheet.CreateRow(dt.Rows.Count + 1);
                        var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                        cell.SetCellValue("Total:");

                        string FinalAmt = Total.ToString();
                        row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                    }
                    using (var exportData = new MemoryStream())
                    {
                        workbook.Write(exportData);
                        string saveAsFileName = fileName;
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                        Response.Clear();
                        Response.BinaryWrite(exportData.GetBuffer());
                        Response.End();
                    }
                }
                return RedirectToAction("Index");
                //c.Data = 1;
                //c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                throw;
            }
            //return Json(c);
        }
        [HttpPost]
        public JsonResult RPT_DPRSubmittedWithoutAnySubScheme(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(56, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpGet]
        public ActionResult RPT_DPRSubmittedWithoutAnySubSchemeExcel(List<int?> MunicipalityIds, string TimeSpanFlag, DateTime? FromDate, DateTime? ToDate, int? SchemeTypeId, int? SchemeCategoryId)
        {
            try
            {
                decimal Total = 0; string Position = ""; string SumFieldName = "";
                string Name = "DPR Submitted Without Any Sub Scheme";
                string fileName = Name + "_" + ".xls";
                var cmd = _db.NewCommand("Udma.RPT_DPRSubmittedWithoutAnySubSchemeExcel", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@municipalityIdList", string.Join(",", MunicipalityIds).ReplaceDbNull());
                cmd.Parameters.AddWithValue("@P_FromDate", FromDate.ToDbDate().ReplaceDbNull());
                cmd.Parameters.AddWithValue("@P_ToDate", ToDate.ToDbDate().ReplaceDbNull());
                cmd.Parameters.AddWithValue("@timeSpanFlag", TimeSpanFlag.ReplaceNull());
                cmd.Parameters.AddWithValue("@schemeTypeId", SchemeTypeId.ReplaceDbNull());
                cmd.Parameters.AddWithValue("@schemeCategoryId", SchemeCategoryId.ReplaceDbNull());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    var workbook = new HSSFWorkbook();
                    var sheet = workbook.CreateSheet();
                    //Create a header row
                    var headerRow = sheet.CreateRow(0);
                    string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    for (int j = 0; j < columnNames.Length; j++)
                    {
                        headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                    }
                    int rowNumber = 1;
                    var row = sheet.CreateRow(rowNumber);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Create a new Row
                        row = sheet.CreateRow(rowNumber++);
                        string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNamess.Length; j++)
                        {
                            row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                        }
                        if (SumFieldName != "")
                        {
                            DataColumnCollection columns = dt.Columns;
                            if (columns.Contains(SumFieldName))
                            {
                                string value = dt.Rows[i][SumFieldName].ToString();
                                Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                Position = dt.Columns.IndexOf(SumFieldName).ToString();
                            }
                            else
                            {
                                Total = Total + 0;
                            }
                        }
                    }
                    if (SumFieldName != "")
                    {
                        // Add a row for the detail row
                        row = sheet.CreateRow(dt.Rows.Count + 1);
                        var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                        cell.SetCellValue("Total:");

                        string FinalAmt = Total.ToString();
                        row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                    }
                    using (var exportData = new MemoryStream())
                    {
                        workbook.Write(exportData);
                        string saveAsFileName = fileName;
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                        Response.Clear();
                        Response.BinaryWrite(exportData.GetBuffer());
                        Response.End();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult RPT_InstallmentClaimedButFundNotReleased(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                ReportUtils rt = new ReportUtils();
                Dictionary<string, object> _params = new Dictionary<string, object>();
                _params.Add("municipalityIdList", string.Join(",", dprMinDashPayload.MunicipalityIds).ReplaceDbNull());
                _params.Add("P_FromDate", dprMinDashPayload.FromDate.ToDbDate().ReplaceDbNull());
                _params.Add("P_ToDate", dprMinDashPayload.ToDate.ToDbDate().ReplaceDbNull());
                _params.Add("timeSpanFlag", dprMinDashPayload.TimeSpanFlag.ReplaceNull());
                _params.Add("schemeTypeId", dprMinDashPayload.SchemeTypeId.ReplaceDbNull());
                _params.Add("schemeCategoryId", dprMinDashPayload.SchemeCategoryId.ReplaceDbNull());
                c.Data = rt.GetReportViewerURL(57, _params);
                c.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion

        #region fund release
        public ActionResult GetUIApplyFundRelease(long mstDprId, string cmd, Dictionary<string, object> Params)
        {
            Params = Params ?? new Dictionary<string, object>();
            using (DprBll form = new DprBll())
            {
                if (string.Equals(cmd, "inst-ui", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        var requestedInstallment = form.GetRequestForNewInstallMent(mstDprId, (Params.ContainsKey("installmentId") ? Params["installmentId"] : 0).ToInt32());
                        return PartialView(@"~\Views\Modules\Dpr\_partialApplyInstallment.cshtml", requestedInstallment);
                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return View(@"~\Views\Modules\Dpr\_partialApplyInstallment.cshtml");

                    }
                }
                else
                {
                    try
                    {
                        int? municipalityId = null;
                        if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                        {
                            municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                        }
                        var obj = form.getDprForFundRelease(mstDprId, municipalityId);
                        logger.Info(obj.ToJson());
                        return View(@"~\Views\Modules\Dpr\dprApprovedApplyFundRelease.cshtml", obj);
                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return View(@"~\Views\Modules\Dpr\dprApprovedApplyFundRelease.cshtml");

                    }
                }

            }

        }
        [HttpPost]
        public JsonResult applyNewInstallment(FundRelease fundRelease)
        {
            try
            {
                using (DprBll form = new DprBll())
                {
                    var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                    int municipalityId = 0;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    }
                    else
                    {
                        throw new Exception("User type not supported to invock this action");
                    }
                    var obj = form.appNewInstallment(fundRelease, userId, municipalityId);
                    c.Data = obj;
                    c.Message = "Intsallment Applied Successfully";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = "Insallment not submitted: " + ex.Message+ex.StackTrace;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }

        public ActionResult GetUIFundReleaseVerification(long mstDprId, string cmd, Dictionary<string, object> _params)
        {
            using (DprBll form = new DprBll())
            {
                if (string.Equals("verify_ui", cmd, StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {


                        var fundReleaseId = _params.ContainsKey("fundReleaseId")
                            ? _params["fundReleaseId"].ToInt64()
                            : throw new Exception("Fund Release Id not Supplied");
                        var isReadOnly = (_params.ContainsKey("isReadonly") ? (_params["isReadonly"].IsNotNull() ? _params["isReadonly"].ToBool() : true) : true);
                        var objs = form.getAlreadyAppliedInstallments(mstDprId, fundReleaseId);
                        var obj = objs.Count > 0 ? objs[0] : throw new Exception("Installment not found");
                        ViewBag.isReadonly = isReadOnly;
                        if (obj.Status != null) { ViewBag.isReadonly = true; }


                        return PartialView(@"~\Views\Modules\Dpr\_partialVerifyInstallmentForFundRelease.cshtml", obj);

                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return PartialView(@"~\Views\Modules\Dpr\_partialVerifyInstallmentForFundRelease.cshtml");
                    }
                }
                else
                {
                    try
                    {

                        var obj = form.getDprForFundRelease(mstDprId);
                        return View(@"~\Views\Modules\Dpr\dprVerifyForFundRelease.cshtml", obj);

                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return View(@"~\Views\Modules\Dpr\dprVerifyForFundRelease.cshtml");
                    }
                }
            }
        }
        [HttpPost]
        public JsonResult VerifyFundRelease(long mstDprId, long fundReleaseId, List<ApplyCheck> checks)
        {
            checks = checks ?? new List<ApplyCheck>();
            try
            {
                using (DprBll form = new DprBll())
                {
                    var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;

                    //return "R" or "V"; R => Returned, V => Verified
                    var obj = form.verifyForFundRelease(mstDprId, fundReleaseId, userId, checks);
                    if (string.IsNullOrEmpty(obj)) throw new Exception("Unknown error occured");

                    c.Data = obj;
                    c.Message = (string.Equals(obj, "R", StringComparison.OrdinalIgnoreCase) ? "Installment Returned" : "Intsallment Verified Successfully");
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }

        public ActionResult GetUIInstallmentApproval(long mstDprId, long? fundReleaseId)
        {
            using (DprBll form = new DprBll())
            {
                if (fundReleaseId != null)
                {
                    try
                    {

                        var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                        var obj = form.GetInstallmentObjectForApproval(mstDprId, fundReleaseId ?? 0, userId);
                        return PartialView(@"~\Views\Modules\Dpr\_partialDprInstallmentApproval.cshtml", obj);

                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return PartialView(@"~\Views\Modules\Dpr\_partialDprInstallmentApproval.cshtml");

                    }
                }
                else
                {
                    //serve parent ui
                    try
                    {

                        var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                        var obj = form.getDprForFundRelease(mstDprId);
                        return View(@"~\Views\Modules\Dpr\DprInstallmentApprovalParentUI.cshtml", obj);

                    }
                    catch (Exception ex)
                    {
                        AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                        ViewBag.error = appExp;
                        return View(@"~\Views\Modules\Dpr\DprInstallmentApprovalParentUI.cshtml");

                    }
                }
            }
        }

        [HttpPost]
        public JsonResult ChangeApprovalStatusInstallment(ChangeDprInstallmentApproval payload)
        {
            try
            {
                if (payload == null)
                {
                    throw new Exception("Payload can not be null");
                }
                var msgs = new Dictionary<string, string>();
                msgs["A"] = "Installment status successfully changed to Approved";
                msgs["R"] = "Installment status successfully changed to Returned";
                msgs["F"] = "Installment Forwarded";
                msgs["N"] = "Installment status successfully changed to Rejected";
                using (DprBll bll = new DprBll())
                {
                    var user = SessionContext.CurrentUser as IInternalUserMin;
                    bll.ChangeInstallmentApporvalStatus(payload, user.UserID);
                    c.Data = bll.GetReleaseOrderFile(payload.fundReleaseId);
                    c.Status = ResponseStatus.SUCCESS;
                    c.Message = msgs.ContainsKey(payload.status) ? msgs[payload.status.ToUpper()] : "Installment status successfully changed";
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion

        #region Old Dpr Entry
        public ActionResult OldDprEntryForm(long? DprId)
        {
            string viewPath = _viewPath + "OldDprEntry/OldDprEntryOrUpdateForm.cshtml";
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    bool isMunicipalityUser = false;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        isMunicipalityUser = true;
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    }
                    OldDpr obj = bll.GetOldDprModel(DprId, municipalityId);
                    if (isMunicipalityUser)
                    {
                        obj.MunicipalityId = municipalityId ?? 0;
                        //obj.Municipalities.RemoveAll(t => t.Key == (municipalityId ?? 0));
                    }
                    return View(viewPath, obj);
                }

            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(viewPath);
            }
        }
        public JsonResult SubmitOldDpr(OldDpr dpr, Base64File file)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    string msg = "";
                    if (dpr.DPChkSlId == 0)
                    {
                        var obj = bll.NewEntryOldDpr(dpr, file, (SessionContext.CurrentUser as IInternalUserMin).UserID);
                        if (obj == null)
                        {
                            throw new Exception("DPR not Submitted successfully");
                        }
                        msg = "DPR Submitted Successfully";
                    }
                    else
                    {
                        bll.UpdateDprMasterForm(dpr, (SessionContext.CurrentUser as IInternalUserMin).UserID);
                        msg = "DPR Updated Successfully";
                    }

                    c.Data = null;
                    c.Message = string.Format(msg);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion

        #region fund release update
        public ActionResult OldFundReleaseEntryForm(long DprId, long? fundReleaseId)
        {
            string viewPath = _viewPath + "OldFundRelease/OldFundRleaseEntryFrom.cshtml";
            try
            {
                using (DprBll form = new DprBll())
                {
                    var requestedInstallment = form.GetOldInstallMent(DprId, fundReleaseId ?? 0);
                    return View(viewPath, requestedInstallment);
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(viewPath);
            }
        }
        [HttpPost]
        public ActionResult UpdateOldFundRelease(OldFundRelease payload)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    long userId = 0;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = ((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                        userId = ((SessionContext.CurrentUser as IInternalUserMin).UserID);
                    }
                    else if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.AdministrativeUser)
                    {
                        userId = ((SessionContext.CurrentUser as IInternalUserMin).UserID);
                    }
                    bll.UpdateOldFundRelase(payload, userId, municipalityId);
                    c.Data = null;
                    c.Message = "Data updated successfully";
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        #endregion

        #region DashBoard
        [HttpPost]
        public JsonResult GetDashboardDataMain(DprDashboard.DprMinDashPayload dprMinDashPayload)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                        dprMinDashPayload.MunicipalityIds = new List<int>();
                        dprMinDashPayload.MunicipalityIds.Add(municipalityId ?? 0);
                    }
                    else if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.AdministrativeUser)
                    {
                        List<int> MnIds = new List<int>();
                        var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                        var Mnids = bll.GetMnIdForLoginDistrictByUserId(userId);
                        if(Mnids.Count>0)
                        {
                            dprMinDashPayload.MunicipalityIds.AddRange(Mnids);
                        }
                    }
                    var lst = bll.GetDprDashBoardMain(dprMinDashPayload);
                    c.Data = lst;
                    c.Message = string.Format("result found");
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult FindProject(string projectNo, string status)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    }
                    var lst = bll.FindProject(projectNo, status, municipalityId);
                    c.Data = lst;
                    c.Message = string.Format("{0} result found", lst.Count);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult FindProjectByName(string projectName, string status)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    }
                    var lst = bll.FindProjectByName(projectName, status, municipalityId);
                    c.Data = lst;
                    c.Message = string.Format("{0} result found", lst.Count);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = new List<object>();
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult GetProjectDetails(long MstDprId)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    int? municipalityId = null;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        municipalityId = (SessionContext.CurrentUser as MunicipalityUser).MunicipalityID;
                    }
                    var obj = bll.GetDprDashBoardDprDetail(MstDprId, municipalityId);
                    c.Data = obj;
                    c.Message = string.Format("project found");
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }
        [HttpPost]
        public JsonResult GetDashboardFinYearWiseData(DprDashPayloadModl1 payload)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    payload.FromFinYear = payload.FromFinYear.IsValidFinYear() ? payload.FromFinYear : throw new Exception(string.Format("Invalid From Fin Year {0}", payload.FromFinYear));
                    payload.ToFinYear = payload.ToFinYear.IsValidFinYear() ? payload.ToFinYear : throw new Exception(string.Format("Invalid To Fin Year {0}", payload.ToFinYear));
                    payload.MunicipalityIds = payload.MunicipalityIds ?? new List<int>();
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        payload.MunicipalityIds.Add((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                    }
                    else if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.AdministrativeUser)
                    {
                        List<int> MnIds = new List<int>();
                        var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                        var Mnids = bll.GetMnIdForLoginDistrictByUserId(userId);
                        if(Mnids.Count>0)
                        {
                            payload.MunicipalityIds.AddRange(Mnids);
                        }
                    }
                    var obj = bll.GetDashboardFinYearWiseData(payload);
                    c.Data = obj;
                    c.Message = string.Format("Result found");
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
           // var kjdsd = Json(c);
            var jsonResult = Json(c, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
          //  return kjdsd;
        }
        public JsonResult GetDashboardFinYearExpenditure(DprDashPayloadModl2 _payload)
        {
            try
            {
                using (DprBll bll = new DprBll())
                {
                    _payload.MunicipalityIds = _payload.MunicipalityIds ?? new List<int>();
                    _payload.date = _payload.date ?? DateTime.Now;
                    if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.MunicipalityUser)
                    {
                        _payload.MunicipalityIds.Add((SessionContext.CurrentUser as MunicipalityUser).MunicipalityID);
                    }
                    else if ((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.AdministrativeUser)
                    {
                        List<int> MnIds = new List<int>();
                        var userId = (SessionContext.CurrentUser as IInternalUserMin).UserID;
                        var Mnids = bll.GetMnIdForLoginDistrictByUserId(userId);
                        if(Mnids.Count>0)
                        {
                            _payload.MunicipalityIds.AddRange(Mnids);
                        }
                    }
                    var objs = bll.GetDashboardFinYearExpenditure(_payload);
                    c.Data = objs;
                    c.Message = string.Format("{0} Result found", objs.Count);
                    c.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                c.Data = null;
                c.Message = ex.Message;
                c.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(c);
        }

        #endregion
    }
}