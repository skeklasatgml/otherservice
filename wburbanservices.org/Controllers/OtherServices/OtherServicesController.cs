﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.Controllers.OtherServices
{
    public class OtherServicesController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        ClientJsonResult _cr = new ClientJsonResult();
        private IEncryptDecrypt chiperAlgorithm;
        int subModuleId = 0;
        long userId = 0;
        string modcode = "";
        int? MunicipalityID = null;
        public OtherServicesController()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);

            var modulsubModule = SessionContext.CurrentModue as Module;
            if (modulsubModule != null)
            {
                subModuleId = modulsubModule.SubModuleId;
                modcode = modulsubModule.SubModuleCode;
            }
            ViewBag.subModuleId = subModuleId;
            ViewBag.SubModuleCode = modcode;

            var user = SessionContext.CurrentUser as IInternalUserMin;
            if (user != null)
            {
                userId = user.UserID;

                ViewBag.SubModuleCode = modcode;
                ViewBag.UserID = userId;

                if (user.UserType == UserType.MunicipalityUser)
                {
                    var muniUser = user as MunicipalityUser;
                    userId = muniUser.UserID;
                    MunicipalityID = muniUser.MunicipalityID;
                    ViewBag.MunicipalityID = MunicipalityID;
                }
            }

        }
        // GET: OtherServices
        public ActionResult Index()
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                   
                    return View("~/Views/OtherServices/SubModulePopulation.cshtml", vs.GetAuthorisedSubModules(obj.UserID, obj.UserType, MunicipalityID));
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View("~/Views/OtherServices/SubModulePopulation.cshtml");
            }
        }
        public ActionResult ApplicantDashboard()
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var obj = SessionContext.CurrentUser as IInternalUserMin;
                    return View("~/Views/OtherServices/ApplicantDashboard.cshtml", vs.GetAuthorisedSubModules(obj.UserID, obj.UserType, MunicipalityID));
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View("~/Views/OtherServices/ApplicantDashboard.cshtml");
            }
        }
        [HttpPost]//autocomplete suggetion
        public JsonResult Registration(CitizenReg CitizenReg)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var reg = vs.SaveRegistration(CitizenReg);
                    _cr.Data = reg;
                    _cr.Message = "Registration Successfull..";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        public ActionResult InspectorSchedule(string Data)  //int? ApplicationId
        {
            string MinDate_MaxDate = "", MinDate ="", MaxDate="";

            long? ApplicationId = 0;
            string mode = "";
            if (Data != null)
            {
                string decryptedURL = chiperAlgorithm.Decrypt(Data);
                string urlDecodedData = HttpUtility.UrlDecode(decryptedURL);
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedData = js.Deserialize<URLMode>(urlDecodedData);
                ApplicationId = serializedData.ServMstId;
                mode = serializedData.urlmode;
            }

            List<ApplicationFormMD.Inspector> data = new List<ApplicationFormMD.Inspector>();
            using (OtherServicesBll vs = new OtherServicesBll())
            {
                data = vs.GetAssignedInspector(ApplicationId);
                MinDate_MaxDate = vs.GetAssignedInspectorMinDate(ApplicationId);

                string[] words = MinDate_MaxDate.Split('#');
                MinDate = words[0];
                MaxDate = words[1];
            }
            ViewBag.MinDate = MinDate;
            ViewBag.MaxDate = MaxDate;
            ViewBag.ApplicationId = ApplicationId;
            ViewBag.mode = mode;

            return View("~/Views/OtherServices/InspectorSchedule.cshtml", data);
        }

        public ActionResult RedirectFromModule(string modcode)
        {
            var user = SessionContext.CurrentUser as IInternalUserMin;
            try
            {
                using (ModuleBll bll = new ModuleBll())
                {

                    var module = bll.GetSubModule(null, modcode);
                    if (module == null)
                        throw new Exception("Module not found");
                    string url = string.Empty;
                    SessionContext.SetModule(module);
                    ViewBag.submoduleId = module.SubModuleId;
                    ViewBag.submoduleName = module.SubModuleName;
                    ViewBag.UserId = user.UserID;
                    if (user.UserType == UserType.MunicipalityUser)
                        return View("~/Views/OtherServices/MunicipalityDashboard.cshtml");
                    else
                        return View("~/Views/OtherServices/ApplicationDashboard.cshtml");
                }
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(SiteConstants.ErrorViewPath, appExp);
            }
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult GetGridDet(string SubModuleid, string UserId, string Municipality, string MuniOrUser = "USER")
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var wards = vs.GetGridData(SubModuleid, userId.ToString(), MunicipalityID.ToString(), MuniOrUser);
                    _cr.Data = wards;
                    _cr.Message = string.Format("{0} wards found", wards.Count());
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        public ActionResult InspectorMaster()
        {
            return View("~/Views/OtherServices/InspectorRegistration.cshtml");
        }
        [HttpPost]//autocomplete suggetion
        public JsonResult SaveInspectorReg(InspectorRegModule InspectorReg)
        {
            try
            {
                var user = SessionContext.CurrentUser as IInternalUserMin;
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var InsReg = vs.InspectorRegistration(InspectorReg, user.UserID, (int)(user.UserType));
                    _cr.Data = InsReg;
                    _cr.Message = "Inspector Registered";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult Save(string ApplicationID, string municipalityID, string Remarks, string ResponseDate, List<ApplicationFormMD.Inspector> obj)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable DT = new DataTable();
            try
            {
                if (obj != null)
                {
                    if (obj.Count > 0)
                    {
                        DT.Columns.Add("InspectorID", typeof(int));
                        DT.Columns.Add("ServMstId", typeof(int));
                        DT.Columns.Add("InspectionOnFrom", typeof(DateTime));
                        DT.Columns.Add("InspectionOnTo", typeof(DateTime));
                        DT.Columns.Add("InsertedBy", typeof(int));

                        foreach (var list in obj)
                        {
                            DataRow drow = DT.NewRow();
                            drow["InspectorID"] = list.InspectorID == null ? DBNull.Value : (object)list.InspectorID;
                            drow["ServMstId"] = ApplicationID;
                            drow["InspectionOnFrom"] = list.InspectionOnFrom == null ? DBNull.Value : (object)list.InspectionOnFrom;
                            drow["InspectionOnTo"] = list.InspectionOnTo == null ? DBNull.Value : (object)list.InspectionOnTo;
                            drow["InsertedBy"] = userId;

                            DT.Rows.Add(drow);
                        }
                    }
                }

                cmd = new SqlCommand("UDServices.InspectorSchedule_SP", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TransType", "INSERT");
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationID == "" ? 0 : (object)ApplicationID);
                cmd.Parameters.AddWithValue("@SubModuleID", subModuleId);

                cmd.Parameters.AddWithValue("@Remarks", Remarks);
                cmd.Parameters.AddWithValue("@ResponseDate", DateTime.Now); //ResponseDate

                cmd.Parameters.AddWithValue("@typ_InspectorSchedule", DT);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                transaction.Commit();

                _cr.Data = ds.GetXml();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "upload";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                _cr.Data = false;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr);
        }

        public ActionResult AllocationDetails(long ApplicationId)
        {
            List<AllocationDetails> data = new List<AllocationDetails>();
            using (OtherServicesBll vs = new OtherServicesBll())
            {
                data = vs.GetAllocationDetails(ApplicationId);
            }
            return View("~/Views/OtherServices/AllocationDetails.cshtml", data);
        }
        [HttpPost]//autocomplete suggetion
        public JsonResult otpVerify(string Otpdata, string MobileNo)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var wards = vs.GetOtpVerify(Otpdata, MobileNo);
                    _cr.Data = wards;
                    _cr.Message = "Otp Verified";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult ResendOtp(string Type, string MobileNo)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var resendoptp = vs.ResendOtp(Type, MobileNo);
                    _cr.Data = resendoptp;
                    _cr.Message = "Resend Otp sent";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult DeleteInspector(string SCHEDULEID)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable DT = new DataTable();
            try
            {
                cmd = new SqlCommand("UDServices.InspectorSchedule_SP", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TransType", "CANCEL");
                cmd.Parameters.AddWithValue("@ScheduleID", SCHEDULEID);
                cmd.Parameters.AddWithValue("@UserID", userId);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                transaction.Commit();

                _cr.Data = ds.GetXml();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "upload";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                _cr.Data = false;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr);
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult otpVerify_inspector(string Otpdata, string MobileNo)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var wards = vs.GetOtpVerify_Inspector(Otpdata, MobileNo);
                    _cr.Data = wards;
                    _cr.Message = "Otp Verified";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]//autocomplete suggetion
        public JsonResult ResendOtp_inspector(string Type, string MobileNo)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var resendoptp = vs.ResendOtp_Inspector(Type, MobileNo);
                    _cr.Data = resendoptp;
                    _cr.Message = "Resend Otp sent";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        public ActionResult ServiceReport(int? ApplicationId, string MuncipalityID, string ServiceID)
        {
            ViewBag.ApplicationId = ApplicationId;

            MuncipalityID = MuncipalityID.Replace(' ', '+');
            ViewBag.MuncipalityID = MuncipalityID;

            ServiceID = ServiceID.Replace(' ', '+');
            ViewBag.ServiceID = ServiceID;
            return View("~/Views/OtherServices/ServiceReport.cshtml");
        }
        public ActionResult ServiceReport_Stacking(int? ApplicationId, string MuncipalityID, string ServiceID) 
        {
            ViewBag.ApplicationId = ApplicationId;

            MuncipalityID = MuncipalityID.Replace(' ', '+');
            ViewBag.MuncipalityID = MuncipalityID;

            ServiceID = ServiceID.Replace(' ', '+');
            ViewBag.ServiceID = ServiceID;
            return View("~/Views/OtherServices/ServiceReport_Stacking.cshtml");
        }
        public ActionResult ServiceReport_Movie(int? ApplicationId, string MuncipalityID, string ServiceID) 
        {
            ViewBag.ApplicationId = ApplicationId;

            MuncipalityID = MuncipalityID.Replace(' ', '+');
            ViewBag.MuncipalityID = MuncipalityID;

            ServiceID = ServiceID.Replace(' ', '+');
            ViewBag.ServiceID = ServiceID;
            return View("~/Views/OtherServices/ServiceReport_Movie.cshtml");
        }
        public JsonResult ForgetPassword(string Password, string confPassWord, string MobileNo, long userId)
        {
            try
            {
                using (OtherServicesBll vs = new OtherServicesBll())
                {
                    var resendoptp = vs.Forgetpass_BLL(Password, confPassWord, MobileNo, userId);
                    _cr.Data = resendoptp;
                    _cr.Message = "Changed Password Successfully";
                    _cr.Status = ResponseStatus.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
    }
}