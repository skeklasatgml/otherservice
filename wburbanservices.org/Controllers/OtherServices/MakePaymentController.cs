﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.Controllers.OtherServices
{
    public class MakePaymentController : Controller
    {
        private IEncryptDecrypt chiperAlgorithm;
        JsonResult jr = new JsonResult();
        ClientJsonResult _cr = new ClientJsonResult();
        int subModuleId = 0;
        long userId = 0;

        public MakePaymentController()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);

            var modulsubModule = SessionContext.CurrentModue as Module;
            if (modulsubModule != null)
            {
                subModuleId = modulsubModule.SubModuleId;
            }


            var user = SessionContext.CurrentUser as IInternalUserMin;
            if (user != null)
            {
                userId = user.UserID;
            }
        }        
       
        // GET: MakePayment
        public ActionResult Index()
        {
            return View();
        }




        public JsonResult GetPGList(string MunicipalityID)
        {
            try
            {
                string DecryptedMunicipalityID = chiperAlgorithm.Decrypt(MunicipalityID);
                PaymentGateway_Bll bll = new PaymentGateway_Bll();
                List<PaymentGatewayMunicipality> lst = bll.GetPGsByMunicipalityId(Convert.ToInt32(DecryptedMunicipalityID));
                var lstPgs = lst.Where(t => t.IsActive == true).Select(f =>
                {
                    PaymentGateway p = new PaymentGateway();
                    p.PG_Code = f.PG_Code;
                    p.PG_Name = f.PG_Name;
                    p.Description = f.Description;
                    return p;
                }).ToList();
                _cr.Data = lstPgs;
                _cr.Message = "Successfull..";
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        [HttpPost]
        public JsonResult MakePaymentTemp(string MunicipalityID, string ServiceID, string PGCode)
        {
            try
            {
                string DecryptedMunicipalityID = chiperAlgorithm.Decrypt(MunicipalityID);
                string DecryptedServiceID = chiperAlgorithm.Decrypt(ServiceID);

                MakePayment_Bll payBll = new MakePayment_Bll();
                Service_Model service = payBll.GetServiceInfo(DecryptedMunicipalityID, DecryptedServiceID);

                long? paymentID = payBll.MakePayment(service, PGCode, userId);
                if (paymentID == null)
                {
                    throw new Exception("Unknown Error! Transaction canceled");
                }
                else
                {
                    //long ServiceID = 
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string redirectUrl = string.Format("/Online/onlinepayment/RedirectToPGForOtherServices?Data={0}", chiperAlgorithm.Encrypt(js.Serialize(new { TransactionID = service.TransactionID, ServiceID= service.ServMstId, PG = PGCode })));
                    _cr.Data = new { RedirectTo = redirectUrl };
                }
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "successfully";
            }
            catch(Exception ex)
            {
                _cr.Data = null;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }

            jr.Data = _cr;
            return jr;
        }
    }
}