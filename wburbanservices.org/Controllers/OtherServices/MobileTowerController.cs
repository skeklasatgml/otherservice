﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Controllers.OtherServices
{
    public class MobileTowerController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        ClientJsonResult _cr = new ClientJsonResult();
        MobileTowerBll bll = new MobileTowerBll();
        ModuleBll bll1 = new ModuleBll();
        int subModuleId = 0;
        long userId = 0;
        int UserTypeId = 0;
        public MobileTowerController()
        {
            // var modulsubModule = SessionContext.CurrentModue as Module;
            // if (modulsubModule == null)
            // {
            subModuleId = 5; //modulsubModule.Id;
                             // }
            ViewBag.subModuleId = subModuleId;


            var user = SessionContext.CurrentUser as IInternalUserMin;
            if (user != null)
            {
                userId = user.UserID;
                ViewBag.UserID = userId;

            }

        }
        public ActionResult Index(int? ApplicationId)
        {
            ViewBag.ApplicationId = ApplicationId;
            return View("~/Views/OtherServices/MobileTower.cshtml");
        }

        [HttpPost]
        public ActionResult Bind_Ward(string MunicipalityID)
        {
            try
            {
                var dt = bll.GetWard(MunicipalityID);
                _cr.Data = dt;
                _cr.Message = string.Format("{0} Found", dt == null ? 0 : dt.Count);
                _cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                _cr.Data = null;
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult Bind_LocationByWard(string WardID, string MunicipalityID)
        {
            try
            {
                var dt = bll.GetLocationByWard(WardID, MunicipalityID);
                _cr.Data = dt;
                _cr.Message = string.Format("{0} Found", dt == null ? 0 : dt.Count);
                _cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                _cr.Data = null;
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult Save(string ApplicationID, string municipalityID, string purposeID, string projectName, List<MobileTowerMD.ProjectDetail> obj)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable DT = new DataTable();
            try
            {
                if (obj != null)
                {
                    if (obj.Count > 0)
                    {
                        DT.Columns.Add("SurfaceType", typeof(int));
                        DT.Columns.Add("RoadType", typeof(int));
                        DT.Columns.Add("Srtlat", typeof(decimal));
                        DT.Columns.Add("Srtlong", typeof(decimal));
                        DT.Columns.Add("Endlat", typeof(decimal));
                        DT.Columns.Add("Endlong", typeof(decimal));

                        DT.Columns.Add("WardNo", typeof(String));
                        DT.Columns.Add("LocationId", typeof(int));
                        DT.Columns.Add("BoroughNo", typeof(int));
                        DT.Columns.Add("HoldingNo", typeof(String));

                        DT.Columns.Add("AssesseeNo", typeof(long));
                        DT.Columns.Add("LandMark", typeof(String));

                        DT.Columns.Add("WorkStdt", typeof(DateTime));
                        DT.Columns.Add("workEndDt", typeof(DateTime));
                        DT.Columns.Add("LengthOfEsca", typeof(decimal));
                        DT.Columns.Add("AreaOfPlot", typeof(decimal));

                        DT.Columns.Add("TowerArea", typeof(decimal));
                        DT.Columns.Add("TowerHight", typeof(decimal));
                        DT.Columns.Add("TowerVolumn_CM", typeof(decimal));
                        DT.Columns.Add("SignageTowerNo", typeof(string));
                        DT.Columns.Add("SpecififationType", typeof(int));
                        DT.Columns.Add("SpecificationRWidth", typeof(decimal));

                        DT.Columns.Add("SpecificationRDepth", typeof(decimal));
                        DT.Columns.Add("SpecificationTDiameter", typeof(decimal));
                        DT.Columns.Add("SpecificationPNo", typeof(int));
                        DT.Columns.Add("SpecificationPLength", typeof(decimal));
                        DT.Columns.Add("SpecificationPWidth", typeof(decimal));

                        DT.Columns.Add("SpecificationPDepth", typeof(decimal));
                        DT.Columns.Add("InsertedBy", typeof(int));
                        DT.Columns.Add("isActive", typeof(int));
                        

                        foreach (var list in obj)
                        {
                            DataRow drow = DT.NewRow();
                            drow["SurfaceType"] = list.TypSurface == null ? DBNull.Value : (object)list.TypSurface;
                            drow["RoadType"] = list.TypRoad == null ? DBNull.Value : (object)list.TypRoad;
                            drow["Srtlat"] = list.LatStart == null ? DBNull.Value : (object)list.LatStart;
                            drow["Srtlong"] = list.LongStart == null ? DBNull.Value : (object)list.LongStart;
                            drow["Endlat"] = list.LatEnd == null ? DBNull.Value : (object)list.LatEnd;
                            drow["Endlong"] = list.LongEnd == null ? DBNull.Value : (object)list.LongEnd;

                            drow["WardNo"] = list.Ward == null ? DBNull.Value : (object)list.Ward;
                            drow["LocationId"] = list.Location == null ? DBNull.Value : (object)list.Location;
                            drow["BoroughNo"] = list.BoroughNo == null ? DBNull.Value : (object)list.BoroughNo;
                            drow["HoldingNo"] = list.HoldingNo == null ? DBNull.Value : (object)list.HoldingNo;
                            drow["AssesseeNo"] = list.AssesseeNo == null ? DBNull.Value : (object)list.AssesseeNo;

                            drow["LandMark"] = list.LandMark == null ? DBNull.Value : (object)list.LandMark;
                            drow["WorkStdt"] = list.StartofWork == null ? DBNull.Value : (object)list.StartofWork;
                            drow["workEndDt"] = list.EndofWork == null ? DBNull.Value : (object)list.EndofWork;
                            drow["LengthOfEsca"] = list.Escavation == null ? DBNull.Value : (object)list.Escavation;

                            drow["AreaOfPlot"] = list.PlotArea == null ? DBNull.Value : (object)list.PlotArea;
                            drow["TowerArea"] = list.TowerArea == null ? DBNull.Value : (object)list.TowerArea;
                            drow["TowerHight"] = list.TowerHeight == null ? DBNull.Value : (object)list.TowerHeight;
                            drow["TowerVolumn_CM"] = list.TowerVolumn_CM == null ? DBNull.Value : (object)list.TowerVolumn_CM;
                            drow["SignageTowerNo"] = list.SignageTowerNo == null ? DBNull.Value : (object)list.SignageTowerNo;

                            drow["SpecififationType"] = list.Specification == null ? DBNull.Value : (object)list.Specification;
                            drow["SpecificationRWidth"] = list.RWidth == null ? DBNull.Value : (object)list.RWidth;
                            drow["SpecificationRDepth"] = list.RDepth == null ? DBNull.Value : (object)list.RDepth;
                            drow["SpecificationTDiameter"] = list.TDiameter == null ? DBNull.Value : (object)list.TDiameter;
                            drow["SpecificationPNo"] = list.PNo == null ? DBNull.Value : (object)list.PNo;
                            drow["SpecificationPLength"] = list.PLength == null ? DBNull.Value : (object)list.PLength;
                            drow["SpecificationPWidth"] = list.PWidth == null ? DBNull.Value : (object)list.PWidth;
                            drow["SpecificationPDepth"] = list.PDepth == null ? DBNull.Value : (object)list.PDepth;
                            drow["InsertedBy"] = userId;
                            drow["isActive"] = list.Status == null ? DBNull.Value : (object)list.Status;
                           

                            DT.Rows.Add(drow);
                        }
                    }
                }

                cmd = new SqlCommand("UDServices.ULBS_services_SP", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TransType", "INSERT_ONE");
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationID == "" ? 0 : (object)ApplicationID);
                cmd.Parameters.AddWithValue("@MunicipalityDevAuthoId", municipalityID);
                cmd.Parameters.AddWithValue("@NameOfProject", projectName);
                cmd.Parameters.AddWithValue("@PurposeOfService", purposeID);
                cmd.Parameters.AddWithValue("@ApplicantID", userId);
                cmd.Parameters.AddWithValue("@SubModuleID", subModuleId);
                cmd.Parameters.AddWithValue("@TYP_Dat_Ulbs_Services", DT);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                transaction.Commit();

                _cr.Data = ds.GetXml();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "upload";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                _cr.Data = false;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult Upload()
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string fname = "", fileWithEXT = "", docTypeId = "", ApplicationID = "", SubModuleID = "";
            DataTable dt = new DataTable();
            try
            {
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            // Checking for Internet Explorer  
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                string[] words = file.FileName.Split('#');
                                docTypeId = words[0];
                                ApplicationID = words[1];
                                SubModuleID = words[2];
                                fname = words[3];
                                //fname = file.FileName;
                            }

                            string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                            string fileEXT = System.IO.Path.GetExtension(fname);
                            fileWithEXT = fname;

                            //deleting code starts here
                            string dfname = Path.Combine(Server.MapPath("~/ApplicationFormExcel/MobileTower"));
                            string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                            //foreach (string f in dfiles)
                            //{
                            //    System.IO.File.Delete(f);
                            //}

                            string pathstoSave = "";
                            cmd = new SqlCommand("UDServices.ULBS_services_SP", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "INSERT_PHOTO");
                            cmd.Parameters.AddWithValue("@ServMstId", ApplicationID);
                            cmd.Parameters.AddWithValue("@DocTypeID", docTypeId);
                            cmd.Parameters.AddWithValue("@SubModuleID", SubModuleID);
                            cmd.Parameters.AddWithValue("@DocExtension", fileEXT);
                            cmd.Parameters.AddWithValue("@DocPath", "ApplicationFormExcel/MobileTower/");
                            //cmd.Parameters.AddWithValue("@DocId", fileWithoutEXT);
                            //cmd.Parameters.AddWithValue("@DocPath", pathstoSave);
                            cmd.Parameters.AddWithValue("@ApplicantID", userId);
                            dr = cmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    pathstoSave = dr["NEWPATH"].ToString();
                                }
                            }
                            dr.Close();


                            //pathstoSave = "ApplicationFormExcel/RoadCutting/" + ApplicationID + "_" + SubModuleID + "_" + docTypeId + "_" + fname;
                            fname = Path.Combine(Server.MapPath("~/ApplicationFormExcel/MobileTower"), pathstoSave);
                            file.SaveAs(fname);
                        }
                        transaction.Commit();
                        _cr.Data = fileWithEXT.ToString();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _cr.Data = 0;
                    }
                }

                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                _cr.Data = 0;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load(string ApplicationID)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable DT = new DataTable();
            try
            {
                cmd = new SqlCommand("UDServices.ULBS_services_SP", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TransType", "SEARCH");
                cmd.Parameters.AddWithValue("@ServMstId", ApplicationID);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                transaction.Commit();

                _cr.Data = ds.GetXml();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "upload";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                _cr.Data = false;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult DeleteImg(string DocID)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Close();
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable DT = new DataTable();
            try
            {
                cmd = new SqlCommand("UDServices.ULBS_services_SP", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TransType", "DELETE_PHOTO");
                cmd.Parameters.AddWithValue("@ServDocId", DocID);
                cmd.Parameters.AddWithValue("@ApplicantID", userId);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                transaction.Commit();

                _cr.Data = ds.GetXml();
                _cr.Status = ResponseStatus.SUCCESS;
                _cr.Message = "upload";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                _cr.Data = false;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(_cr);
        }

        [HttpPost]
        public ActionResult HoldingNo(string MunicipalityID, string Desc, string WardID, string LocationID)
        {
            try
            {
                var dt = bll.GetHoldingNo(WardID, MunicipalityID, LocationID, Desc);
                _cr.Data = dt;
                _cr.Message = string.Format("{0} Found", dt == null ? 0 : dt.Count);
                _cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                _cr.Data = null;
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(_cr);
        }
    }
}