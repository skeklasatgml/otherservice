﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Valuation_Board.Models.WebApi;
using Valuation_Board.App_Codes.BLL;


namespace Valuation_Board.Controllers
{
    [RoutePrefix("Api/DataTrans")]
    public class ClientDataController : ApiController
    {
        [HttpPost,Route("Update")]
        public HttpResponseMessage UploadData(List<CurValModelApi> ClientData)
        {
            HttpResponseMessage result = null;
            string ret = "N";
            try
            {
                if (ClientData != null)
                {
                    ClientDataUpdate cu = new ClientDataUpdate();
                    cu.ClientCurValDataUpdate(ClientData);
                    ret = "Successfully Updated "+ClientData.Count.ToString()+" Rows";
                    result = Request.CreateResponse(System.Net.HttpStatusCode.OK, ret, "application/json");
                }
            }
            catch(Exception ex)
            {
                result = Request.CreateResponse(System.Net.HttpStatusCode.Forbidden, ret, "application/json");
            }
            return result;
        }
    }
}
