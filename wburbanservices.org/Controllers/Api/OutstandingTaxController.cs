﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Valuation_Board.Models.WebApi;
using Valuation_Board.App_Codes.BLL;
using System.Web.Http.Cors;
namespace Valuation_Board.Controllers.Api
{
   
    [RoutePrefix("api/v1/OutstandingAmt"), EnableCorsAttribute("*", "*", "*")]
    public class OutstandingTaxController : ApiController
    {
        [HttpGet, Route("Test1/{id}")]
        public HttpResponseMessage test(int id)
        {
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, "Done");
        }

        [HttpGet, Route("GetOutstandingData/{MunicipalityID}/{AssesseeNo}")]
        public HttpResponseMessage GetOutstandingData(int MunicipalityID, string AssesseeNo)
        {
            HttpResponseMessage result = null;

            try
            {
                string Token = Request.Headers.GetValues("Token").First();
                List<OutstandingTax> lstOutTax = new List<OutstandingTax>();
                OutstandingTaxBLL outTax = new OutstandingTaxBLL();
                lstOutTax = outTax.GetData(Token, MunicipalityID, AssesseeNo.Trim()).ToList();

                if(lstOutTax.Count > 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, lstOutTax);
                }
                else
                {
                    result = Request.CreateErrorResponse(HttpStatusCode.NotFound, "Outstanding Not Found.");
                }
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError , ex.Message);
            }
            return result;
        }
    }
}
