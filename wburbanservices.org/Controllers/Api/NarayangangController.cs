﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.BLL.ApiService;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Account.Logins;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels.ApiService;

namespace Valuation_Board.Controllers.Api
{
    [RoutePrefix("api/v1/Narayangang/CitizenModule")]
    public class UnAuthenticatedNarayangangCitizenModuleController : ApiController
    {
        [HttpPost, Route("ProfileLogin")]
        public HttpResponseMessage ProfileLogin([FromBody]NG_Profile Login)
        {
            NG_OnlineCitizenProfile ng_citizenProfile = new NG_OnlineCitizenProfile();
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                ng_citizenProfile = rn.NG_ProfileLoginBll(Login);
                result = Request.CreateResponse(HttpStatusCode.OK, ng_citizenProfile);
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("RegisterNew")]
        public HttpResponseMessage RegisterNew([FromBody]NG_Profile Register)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.NG_RegisterNewBll(Register);
                if(response == true)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, "Successfully Registered");
                }                
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ProfileVerification")]
        public HttpResponseMessage ProfileVerification([FromBody]OnilneCitizenProfile CitizenProfile)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.ProfileVerificationBll(CitizenProfile);
                result = Request.CreateResponse(HttpStatusCode.OK, "Profile Verification Successful.");
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("RegisterPassword")]
        public HttpResponseMessage RegisterPassword([FromBody]OnilneCitizenProfile CitizenProfileUpdate)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.RegisterPasswordBll(CitizenProfileUpdate);
                result = Request.CreateResponse(HttpStatusCode.OK, "Password successfully registered.");
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ForgotPassword")]
        public HttpResponseMessage ForgotPassword([FromBody]ProfileRegister Register)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.ForgotPasswordBll(Register);
                result = Request.CreateResponse(HttpStatusCode.OK, "OTP has been sent.");
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("RequestOtp")]
        public HttpResponseMessage RequestOtp([FromBody]ProfileRegister Register)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.ForgotPasswordBll(Register);
                result = Request.CreateResponse(HttpStatusCode.OK, "OTP has been sent.");
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }


        [HttpGet, Route("LogOut/{PhoneNo}/{Otp}")]
        public HttpResponseMessage LogOut(string PhoneNo, string Otp)
        {
            throw new NotImplementedException();
        }

        [HttpPost, Route("RegisterComplain")]
        public HttpResponseMessage RegisterComplain([FromBody]ComplainNarayangang complainNarayangang)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.NG_RegisterComplainBll(complainNarayangang);
                if (response == true)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, "Successfully Registered");
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetComplain")]
        public HttpResponseMessage GetComplain([FromBody]ComplainNarayangang complainNarayangang)
        {
            //var response = false;
            List<ComplainNarayangang> lstComplainNarayangang = new List<ComplainNarayangang>();
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                lstComplainNarayangang = rn.NG_GetComplainBll(complainNarayangang);
                if (lstComplainNarayangang.Count > 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, lstComplainNarayangang);
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("RegisterUserLocation")]
        public HttpResponseMessage RegisterUserLocation([FromBody]NG_UserLocation nG_UserLocation)
        {
            var response = false;
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                response = rn.NG_RegisterUserLocationBLL(nG_UserLocation);
                if (response == true)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, "User location successfully registered.");
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetUserLocation")]
        public HttpResponseMessage GetUserLocation([FromBody]NG_UserLocation nG_UserLocation)
        {
            //var response = false;
            List<NG_UserLocation> lstNG_UserLocation = new List<NG_UserLocation>();
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                lstNG_UserLocation = rn.NG_GetUserLocationBll(nG_UserLocation);
                if (lstNG_UserLocation.Count > 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, lstNG_UserLocation);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, "No record found.");
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetTrackedProfile")]
        public HttpResponseMessage GetTrackedProfile([FromBody]NG_TrackingPermission nG_TrackingPermission)
        {
            //var response = false;
            List<NG_TrackingPermission> lstNG_TrackingPermission = new List<NG_TrackingPermission>();
            HttpResponseMessage result = null;
            try
            {
                RegisterNewBLL rn = new RegisterNewBLL();
                lstNG_TrackingPermission = rn.NG_TrackingPermissionBll(nG_TrackingPermission);
                if (lstNG_TrackingPermission.Count > 0)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, lstNG_TrackingPermission);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, "No record found.");
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

    }

    //authorized services
    [RoutePrefix("api/v1/Narayangang/CitizenModule")]
    public class AuthenticatedNarayangangCitizenModuleController : ApiController
    {
        TokenService _token = null;
        [HttpPost, Route("GetAssesseeListProfileWise")]
        public HttpResponseMessage GetAssesseeListProfileWise([FromBody] OnilneCitizenProfile onilneCitizenProfile)
        {
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token.Trim(), onilneCitizenProfile.ProfileId);
                if (_token.Validate() == true)
                {
                    List<SearchAssessse> searchAssessse = new List<SearchAssessse>();
                    RegisterNewBLL rn = new RegisterNewBLL();
                    searchAssessse = rn.GetAssesseeListProfileWiseBll(Token, onilneCitizenProfile.ProfileId);

                    if (searchAssessse.Count > 0)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, searchAssessse);
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "No assessee found for this profile.");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("AddAssesseeProfileWise")]
        public HttpResponseMessage AddAssesseeProfileWise([FromBody] SearchAssessse searchAssessse)
        {
            var response = false;
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, searchAssessse.ProfileId);
                if (_token.Validate() == true)
                {
                    RegisterNewBLL rn = new RegisterNewBLL();
                    response = rn.AddAssesseeProfileWiseBll(Token.Trim(), searchAssessse);

                    if (response == true)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, "Assessee successfully maped to the profile.");
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error.");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("DeleteAssesseeProfileWise")]
        public HttpResponseMessage DeleteAssesseeProfileWise([FromBody] SearchAssessse searchAssessse)
        {
            var response = false;
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, searchAssessse.ProfileId);
                if (_token.Validate() == true)
                {
                    if (_token.GetAssesseeProfileMappingStatus(searchAssessse.ProfileId, searchAssessse.AssesseeID) == true)
                    {
                        RegisterNewBLL rn = new RegisterNewBLL();
                        response = rn.DeleteAssesseeProfileWiseBll(Token.Trim(), searchAssessse);

                        if (response == true)
                        {
                            result = Request.CreateResponse(HttpStatusCode.OK, "Assessee successfully unmaped from the profile.");
                        }
                        else
                        {
                            result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error.");
                        }
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ChangeProfilePassword")]
        public HttpResponseMessage ChangeProfilePassword([FromBody] OnilneCitizenProfile onilneCitizenProfile)
        {
            var response = false;
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, onilneCitizenProfile.ProfileId);
                if (_token.Validate() == true)
                {
                    RegisterNewBLL rn = new RegisterNewBLL();
                    response = rn.ChangeProfilePasswordBll(Token.Trim(), onilneCitizenProfile);

                    if (response == true)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, "Password successfully changed.");
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error.");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetOutStandings")]
        public HttpResponseMessage GetOutStandings([FromBody] TaxOutstanding taxOutstanding)
        {
            PTaxCollectionGiantObject pTaxCollectionGiantObject = new PTaxCollectionGiantObject();
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, taxOutstanding.ProfileId);
                if (_token.Validate() == true)
                {
                    if (_token.GetAssesseeProfileMappingStatus(taxOutstanding.ProfileId, taxOutstanding.AssesseeID) == true)
                    {
                        RegisterNewBLL rn = new RegisterNewBLL();
                        pTaxCollectionGiantObject = rn.Get_PropertyTaxPaymentDetails(taxOutstanding.MunicipalityID, taxOutstanding.WardID, taxOutstanding.LocationID, taxOutstanding.AssesseeID,
                                                                    taxOutstanding.PaymentDate, taxOutstanding.TaxPaymentCheckParameters);

                        if (pTaxCollectionGiantObject != null)
                        {
                            result = Request.CreateResponse(HttpStatusCode.OK, pTaxCollectionGiantObject);
                        }
                        else
                        {
                            result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Outstanding not found.");
                        }
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetPGsList")]
        public HttpResponseMessage GetPGsList([FromBody] SearchAssessse searchAssessse)
        {
            List<PaymentGatewayMunicipality> PGlst = new List<PaymentGatewayMunicipality>();
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, searchAssessse.ProfileId);
                if (_token.Validate() == true)
                {
                    PaymentGateway_Bll bll = new PaymentGateway_Bll();
                    PGlst = bll.GetPGsByMunicipalityId(searchAssessse.MunicipalityID ?? 0);

                    if (PGlst != null)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, PGlst);
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "No PG list found.");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetLast5Payments")]
        public HttpResponseMessage GetLast5Payments([FromBody] SearchAssessse searchAssessse)
        {
            List<PaymentSummary> lstLastPayments = new List<PaymentSummary>();
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, searchAssessse.ProfileId);
                if (_token.Validate() == true)
                {
                    if (_token.GetAssesseeProfileMappingStatus(searchAssessse.ProfileId, searchAssessse.AssesseeID) == true)
                    {
                        PropertyTaxCollectionBLL pbll = new PropertyTaxCollectionBLL();
                        lstLastPayments = pbll.GetLastPayments(5, searchAssessse.AssesseeID);

                        if (lstLastPayments != null)
                        {
                            result = Request.CreateResponse(HttpStatusCode.OK, lstLastPayments);
                        }
                        else
                        {
                            result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "No payment status found.");
                        }
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("MakePaymentTemp")]
        public HttpResponseMessage MakePaymentTemp([FromBody] TaxOutstanding taxOutstanding)
        {
            string responseString = "";
            var response = new object();
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, taxOutstanding.ProfileId);
                if (_token.Validate() == true)
                {
                    if (_token.GetAssesseeProfileMappingStatus(taxOutstanding.ProfileId, taxOutstanding.AssesseeID) == true)
                    {
                        RegisterNewBLL rn = new RegisterNewBLL();
                        responseString = rn.MakePaymentTempBll(taxOutstanding.Mobile, taxOutstanding.Email, taxOutstanding.PG, taxOutstanding.MunicipalityID, taxOutstanding.WardID, taxOutstanding.LocationID, taxOutstanding.AssesseeID,
                                                                    taxOutstanding.PaymentDate, taxOutstanding.TaxPaymentCheckParameters);
                        if (responseString != "")
                        {
                            response = new { RedirectTo = responseString };
                            result = Request.CreateResponse(HttpStatusCode.OK, response);
                        }
                        else
                        {
                            result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error...");
                        }
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("UpdateMobileOnProfile")]
        public HttpResponseMessage UpdateMobileOnProfile([FromBody] TaxOutstanding taxOutstanding)
        {
            var response = false;
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, taxOutstanding.ProfileId);
                if (_token.Validate() == true)
                {
                    RegisterNewBLL rn = new RegisterNewBLL();
                    response = rn.UpdateMobileOnProfileBll(Token, taxOutstanding);
                    if (response == true)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, "OTP Successfully sent for mobile no updation.");
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error...");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("VerificationOnUpdatedMobileNo")]
        public HttpResponseMessage VerificationOnUpdatedMobileNo([FromBody] OnilneCitizenProfile onilneCitizenProfile)
        {
            var response = false;
            HttpResponseMessage result = null;
            string Token = null;
            if (Request.Headers.Contains("Token"))
            {
                Token = Request.Headers.GetValues("Token").First();
            }
            try
            {
                _token = new TokenService(Token, onilneCitizenProfile.ProfileId);
                if (_token.Validate() == true)
                {
                    RegisterNewBLL rn = new RegisterNewBLL();
                    response = rn.VerificationOnUpdatedMobileNoBll(Token, onilneCitizenProfile);
                    if (response == true)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, "Mobile No successfully updated.");
                    }
                    else
                    {
                        result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error...");
                    }
                }
            }
            catch (AppException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Serialize());
            }
            catch (Exception ex)
            {
                result = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }


        [HttpGet, Route("LogOut")]
        public HttpResponseMessage LogOut(string Token)
        {
            // _token = new TokenService(Token);
            _token.Validate();
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// ///////////////////////////////////////////////////////////////////  Collector Module /////////////////////////////////////////////////////////////////////////////////////
    /// </summary>

    [RoutePrefix("api/v1/Narayangang/CollectorModule")]
    public class NarayangangCollectorModuleController : ApiController
    {
        [HttpPost, Route("CollectorLogin")]
        public HttpResponseMessage CollectorLogin([FromBody]MunicipalityLogin MunicipalityLogin)
        {
            MunicipalityUser response = new MunicipalityUser();
            HttpResponseMessage result = null;
            try
            {
                CollectorModuleBLL cm = new CollectorModuleBLL();
                response = cm.CollectorLoginBll(MunicipalityLogin);

                var responseOBJ = new
                {
                    IsLoggedIn = true,
                    UserID = response.UserID,
                    Message = "Login Successful."
                };

                result = Request.CreateResponse(HttpStatusCode.OK, responseOBJ);
            }
            catch (SecurityException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetAssesseesByHoldings")]
        public HttpResponseMessage GetAssesseesByHoldings([FromBody] SearchAssessse searchAssessse)
        {
            List<Assessee> lstAssessee = new List<Assessee>();
            HttpResponseMessage result = null;
            try
            {
                CollectorModuleBLL cm = new CollectorModuleBLL();
                lstAssessee = cm.GetAssesseesByHoldingsBll(searchAssessse);

                var responseOBJ = lstAssessee.Select(t =>
                {
                    return new
                    {
                        AssesseeID = t.AssesseeID,
                        AssesseeName = t.AssesseeName,
                        AssesseeNo = t.AssesseeNo,
                        WardName = t.Ward == null ? null : t.Ward.WardName,
                        WardID = t.WardID,
                        HoldingNo = t.HoldingNo,
                        LocationName = t.Location == null ? null : t.Location.LocationName,
                        LocationID = t.LocationID
                    };
                });

                result = Request.CreateResponse(HttpStatusCode.OK, responseOBJ);
            }
            catch (SecurityException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("MakePayment")]
        public HttpResponseMessage MakePayment([FromBody] MakePaymentsCollector mpc)
        {
            PaymentDetails paymentDetails = new PaymentDetails();
            long responsePaymentID = 0;
            HttpResponseMessage result = null;
            try
            {
                CollectorModuleBLL cm = new CollectorModuleBLL();
                responsePaymentID = cm.MakePaymentBll(mpc.Mobile, mpc.Email, mpc.userID, mpc.MunicipalityID, mpc.WardID, mpc.LocationID, mpc.AssesseeID, mpc.PaymentDate, mpc.TaxPaymentCheckParameters,
                    mpc.PaymentModes, null, null, mpc.BillReciptNo);

                if (responsePaymentID != 0)
                {
                    paymentDetails = cm.GetPaymentDetailsByPaymentID(responsePaymentID);
                }
                else
                {
                    paymentDetails = null;
                }

                if (paymentDetails != null)
                {
                    result = Request.CreateResponse(HttpStatusCode.OK, paymentDetails);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Error.." });
                }

            }
            catch (SecurityException ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }
    }
}