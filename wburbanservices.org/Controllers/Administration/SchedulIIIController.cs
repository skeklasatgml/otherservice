﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.Models.Application;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.App_Start;
using Valuation_Board.App_Codes;
using Valuation_Board.Models.Account;
using System.Data;
using Valuation_Board.ViewModels;


namespace Valuation_Board.Controllers.Administration
{
    [SecuredFilter]
    public class SchedulrIIIController : Controller
    {
        // GET: SchedulrIII
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public JsonResult GetMunicipality()
        {
            ScheduleIII_VM model = new ScheduleIII_VM();
            try
            {

                DataTable dt = new ScheduleIIIBLL().GetMuncplty();
                
                model.MMunicipalityList = (from DataRow row in dt.Rows
                                  select new MMunicipality
                                  {
                                      MunicipalityID = row["MunicipalityID"].ToString(),
                                      MMunicipalityName = row["MunicipalityName"].ToString()
                                  }).ToList();
                model.Status = ResponseStatus.SUCCESS;
                model.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                model.MMunicipalityName = null;
                model.Message = ex.Message;
                model.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(model);
        }

        [HttpPost]
        public JsonResult Get_MstScheduleIII()
        {
            ScheduleIII_VM model = new ScheduleIII_VM();
            try
            {

                DataTable dt = new ScheduleIIIBLL().Get_MstScheduleIII();

                model.ScheduleIIIList = (from DataRow row in dt.Rows
                                           select new ScheduleIII_VM
                                           {
                                               AppliWindowId = row["AppliWindowId"].ToString(),
                                               MunicipalityID = row["MunicipalityID"].ToString(),
                                               MMunicipalityName = row["MunicipalityName"].ToString(),
                                               ScheIIIStartDt = Convert.ToDateTime( row["ScheIIIStartDt"]).ToShortDateString(),
                                               ScheIIIDt = Convert.ToDateTime(row["ScheIIIDt"]).ToShortDateString(),
                                           }).ToList();
                model.Status = ResponseStatus.SUCCESS;
                model.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                model.MMunicipalityName = null;
                model.Message = ex.Message;
                model.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(model);
        }

        [HttpPost]
        public JsonResult SaveUpdate_SchedulrIIIr(string AppliWindowId, string MunicipalityID, string FromDate, string ToDate)
        {
            ScheduleIII_VM model1 = new ScheduleIII_VM();
            try
            {
                int id = 0;
                if(AppliWindowId!="")
                {
                    id = Convert.ToInt32(AppliWindowId);
                }
                var mObj = SessionContext.CurrentUser as IInternalUser;
                long userId = mObj.UserID;
                DataTable dt = new ScheduleIIIBLL().SaveUpdate_ScheduleIII(id, Convert.ToInt32(MunicipalityID), FromDate, ToDate, userId);              
                model1.Status = ResponseStatus.SUCCESS;
                model1.Message = dt.Rows[0]["Messege"].ToString();
                model1.ErrorCode = dt.Rows[0]["ErrorCode"].ToString();
            }
            catch (Exception ex)
            {
                model1 = null;
                model1.Message = ex.Message;
            }
            return Json(model1);
        }
    }
}