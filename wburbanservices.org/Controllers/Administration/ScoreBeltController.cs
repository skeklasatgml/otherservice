﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Administration
{
    [SecuredFilter]
    public class ScoreBeltController : Controller
    {
        JsonResult js = new JsonResult();
        ClientJsonResult cr = new ClientJsonResult();
        MunicipalityBLL mnBll = new MunicipalityBLL();
        AssmtScoreBeltBLL ScoreBeltBLL = new AssmtScoreBeltBLL();
        UserType type = UserType.AdministrativeUser;
        public ScoreBeltController()
        {
            IInternalUserMin u = SessionContext.CurrentUser as IInternalUserMin;
            type = u.UserType;
        }
        // GET: ScoreBelt
        public ActionResult Index()
        {
            return View("~/Views/Administration/ScoreBelt/Index.cshtml");
        }

        public ActionResult ScoreBeltView() {
            return View("~/Views/Administration/ScoreBelt/ScoreBeltView.cshtml");
        }

       
        public JsonResult GetAllMunicipalites()
        {
            List<Municipality_Ddl_VM> mnVm = new List<Municipality_Ddl_VM>();

            try
            {
                List<Models.Application.Municipality> lstMn = mnBll.GetAllMunicipalities();
                var lstmn = mnBll.GetAllMunicipalities();
                mnVm = lstmn.Select(t =>
                {
                    return new Municipality_Ddl_VM()
                    {
                        MunicipalityID = t.MunicipalityID ?? 0,
                        MunicipalityName = t.MunicipalityName
                    };
                }).ToList();
                cr.Data = mnVm;
                cr.Message = string.Format("{0} Municipalities Found", mnVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = mnVm;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }
        [HttpPost]
        public JsonResult GetYear()
        {
            try
            {
                var x = ScoreBeltBLL.GetYear();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetScoreType()
        {
            try
            {
                var x = ScoreBeltBLL.getScoreType();
                //var lst = (IEnumerable<dynamic>)x;
                //var t = lst.Where(s => s.ScoreBelt == "S");

                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetScoreByTypeId(string typeId)
        {
            try
            {
                var lst = ScoreBeltBLL.GetScoreByTypeId(typeId);
                cr.Data = lst;
                cr.Message = string.Format("{0} score found", 1);
            }
            catch (Exception ex)
            {

                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetBeltTypevalue()
        {
            try
            {
                var x = ScoreBeltBLL.getBeltTypevalue();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetEffectiveDate(int municipalityId,string year,string name)
        {
            try
            {
                //var mUser = SessionContext.CurrentUser as MunicipalityUser;
                long userId=0;
                int MunId=0;
                if (type==UserType.AdministrativeUser)
                {
                    var mUser = SessionContext.CurrentUser as IInternalUserMin;
                    userId = mUser.UserID;
                    MunId = municipalityId;
                }
                else if (type == UserType.MunicipalityUser)
                {
                    var mUser = SessionContext.CurrentUser as MunicipalityUser;
                    userId = mUser.UserID;
                    MunId = mUser.MunicipalityID;
                }
                var lst = ScoreBeltBLL.GetEffectivedate(MunId, year, userId);
                //List<object> lst1 = ScoreBeltBLL.GetEffectivedate(mUser.MunicipalityID, year, userId)as List<object>;
                //List<dynamic> lst2 = ScoreBeltBLL.GetEffectivedate(mUser.MunicipalityID, year, userId) as List<dynamic>;
                cr.Data = lst;
                cr.Message = string.Format("{0} ScoreSheet Found", lst);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            js.Data = cr;
            return js;
        }

        [HttpPost]
        public JsonResult ScoreView(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                
                    if (type == UserType.AdministrativeUser)
                {
                    var mUser = SessionContext.CurrentUser as IInternalUserMin;
                    AssmtScoreBeltObj.UserID = mUser.UserID;
                }
                else if(type == UserType.MunicipalityUser)
                {
                    var mUser = SessionContext.CurrentUser as MunicipalityUser;
                    AssmtScoreBeltObj.UserID = mUser.UserID;
                    AssmtScoreBeltObj.municipalityId = mUser.MunicipalityID;
                }

                AssmtScoreBeltObj.TransactionType = "ScoreView";
                List<AssmtScoreBelt> lst = ScoreBeltBLL.ScoreView(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }


        [HttpPost]
        public JsonResult BeltView(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                if (type == UserType.AdministrativeUser)
                {
                    var mUser = SessionContext.CurrentUser as IInternalUserMin;
                    AssmtScoreBeltObj.UserID = mUser.UserID;
                }
                else if (type == UserType.MunicipalityUser)
                {
                    var mUser = SessionContext.CurrentUser as MunicipalityUser;
                    AssmtScoreBeltObj.UserID = mUser.UserID;
                    AssmtScoreBeltObj.municipalityId = mUser.MunicipalityID;
                }

                AssmtScoreBeltObj.TransactionType = "BeltView";
                List<AssmtScoreBelt> lst = ScoreBeltBLL.BeltView(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult ScoreDel(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                var mUser = SessionContext.CurrentUser as IInternalUserMin;
                AssmtScoreBeltObj.UserID = mUser.UserID;
                AssmtScoreBeltObj.TransactionType = "ScoreDel";

                List<AssmtScoreBelt> lst = ScoreBeltBLL.ScoreDel(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult BeltDel(AssmtScoreBelt AssmtScoreBeltObj)
        {
            try
            {
                var mUser = SessionContext.CurrentUser as IInternalUserMin;
                AssmtScoreBeltObj.UserID = mUser.UserID;
                AssmtScoreBeltObj.TransactionType = "BeltDel";

                List<AssmtScoreBelt> lst = ScoreBeltBLL.BeltDel(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult ScoreInsUpd(AssmtScoreBelt AssmtScoreBeltObj) {
            try
            {
                AssmtScoreBeltObj.TransactionType = "ScoreInsUpd";
                var mUser = SessionContext.CurrentUser as IInternalUserMin;
                AssmtScoreBeltObj.UserID = mUser.UserID;

                List<AssmtScoreBelt> lst = ScoreBeltBLL.ScoreInsUpd(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult BeltInsUpd(AssmtScoreBelt AssmtScoreBeltObj) {
            try
            {
                AssmtScoreBeltObj.TransactionType = "BeltInsUpd";
                var mUser = SessionContext.CurrentUser as IInternalUserMin;
                AssmtScoreBeltObj.UserID = mUser.UserID;

                List<AssmtScoreBelt> lst = ScoreBeltBLL.BeltInsUpd(AssmtScoreBeltObj);

                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}