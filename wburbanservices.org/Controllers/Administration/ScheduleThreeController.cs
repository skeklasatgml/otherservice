﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Exceptions;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Account;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Controllers.Administration
{
    [SecuredFilter]
    public class ScheduleThreeController:Controller
    {
        ClientJsonResult _cr = new ClientJsonResult();
        public ActionResult Index()
        {
            try
            {
                ScheduleThree_Bll bll = new ScheduleThree_Bll();
                return View(@"~\Views\Administration\Schedule3\AssmtList.cshtml", bll.GetSubmittedForms((SessionContext.CurrentUser as IInternalUserMin).UserID));
            }
            catch (Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(SiteConstants.ErrorViewPath, appExp);
            }
        }
        public ActionResult S3Container()
        {
            try
            {
               ScheduleThree_Bll bll = new ScheduleThree_Bll();
            //    var user = SessionContext.CurrentUser as IInternalUserMin;
                return View(@"~\Views\Administration\Schedule3\Container.cshtml", bll.GetMasterDatas((SessionContext.CurrentUser as IInternalUserMin).UserID));
            }
            catch(Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(SiteConstants.ErrorViewPath,appExp);
            }
            
        }
        [HttpPost]
        public ActionResult GetForm(int? MunicipalityID,long? assmtId)
        {
            try
            {
                ScheduleThree_Bll.TempData tBll = new ScheduleThree_Bll.TempData();
                ScheduleThree_Bll bll = new ScheduleThree_Bll();
                if((SessionContext.CurrentUser as IInternalUserMin).UserType == UserType.CitizenUser)
                {
                    //citizen user will be selected autometically from inside the function from session context
                    var forms = bll.GetSubmittedForms(0);
                    if (forms.Count > 0)
                    {
                        assmtId = forms.OrderByDescending(t => t.SubmittedOn).FirstOrDefault().AssmtId;
                    }
                }

                if (assmtId == null && MunicipalityID!=null)
                {
                    ScheduleThree.Form form = tBll.GetTempData((SessionContext.CurrentUser as IInternalUserMin).UserID, MunicipalityID??0);
                    var formEmpty = bll.GetForm((SessionContext.CurrentUser as IInternalUserMin).UserID, MunicipalityID??0);
                    if (form != null)
                    {

                        form.MasterData = formEmpty.MasterData;
                    }
                    else
                    {
                        form = formEmpty;
                    }
                    return PartialView(@"~\Views\Administration\Schedule3\GetForm.cshtml", form);
                }
                else
                {
                    //find by id assmt
                   var form= bll.FindScheduledIIIData(assmtId ?? 0, (SessionContext.CurrentUser as IInternalUserMin).UserID);
                    if (form == null)
                    {
                        throw new Exception("Details not found");
                    }
                    return PartialView(@"~\Views\Administration\Schedule3\GetForm.cshtml", form);
                }
                
            }
            catch(Exception ex)
            {
                AppException<string, string> appExp = new AppException<string, string>(ex.Message, "", null);
                ViewBag.error = appExp;
                return View(SiteConstants.ErrorViewPath,appExp);
            }
        }
        
        [HttpPost]//autocomplete suggetion
        public JsonResult GetWards(string Ward,int MunicipalityID)
        {
            try
            {
               
                var wards = new WardBLL().GetWards(MunicipalityID, Ward);
                _cr.Data = wards;
                _cr.Message = string.Format("{0} wards found", wards.Count());
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        [HttpPost]//autocomplete suggetion
        public JsonResult GetTempData( int MunicipalityID)
        {
            try
            {
                ScheduleThree_Bll.TempData tBll = new ScheduleThree_Bll.TempData();
                var wards = tBll.GetTempData((SessionContext.CurrentUser as IInternalUserMin).UserID, MunicipalityID); ;
                _cr.Data = wards;
                _cr.Message = string.Format("Temp Data Found");
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }

        #region services
        [HttpPost]
        public JsonResult Usages(int HldngTypGrpId)
        {
            try
            {
                ScheduleThree_Bll bll = new ScheduleThree_Bll();
                var obj = bll.GetHoldingTypes(HldngTypGrpId);
                _cr.Data = obj;
                _cr.Message = string.Format("{0} holding types found",obj.Count);
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        [HttpPost]
        public JsonResult SaveAsDraft(ScheduleThree.Form form)
        {
            try
            {
                ScheduleThree_Bll.TempData bll = new ScheduleThree_Bll.TempData();
                var obj = bll.SaveDataTemp((SessionContext.CurrentUser as IInternalUserMin).UserID, form.MunicipalityId, form);
                _cr.Data = obj;
                _cr.Message = "Data saved as draft";
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        [HttpPost]
        public JsonResult SaveForm()
        {
            try
            {
                ScheduleThree_Bll bll = new ScheduleThree_Bll();
                JavaScriptSerializer oJS = new JavaScriptSerializer();
                if (Request["FormData"] == null)
                {
                    throw new ArgumentException("Invalid Member information");
                }
                JsonSerializer _jsonWriter = new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
              
                var val = JsonConvert.DeserializeObject<ScheduleThree.Form>(Request["FormData"],
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                val.ImageTab.Images.ForEach(t =>
                {
                    var file = Request.Files[t.TypeCode];
                    if (file == null)
                    {
                        throw new ArgumentNullException(string.Format("Please upload file on Type {0}", t.TypeCode));
                    }
                    var uploadedPath = file.SaveFile();
                    t.Value = uploadedPath;
                });
                if((val.AssmtId??0) <=0)
                {
                    if (val.UserVerificationData.VerificationType == ScheduleThree.VerificationType.SignatureUpload)
                    {
                        var file = Request.Files["SIG"];
                        var dics = new Dictionary<string, object>();
                        dics["SIG"]=file ?? throw new ArgumentNullException(string.Format("Please upload signature file"));
                        val.UserVerificationData.VerificationData = dics;
                    }
                    _cr.Data = bll.InsertScheduleIIIData(val, (SessionContext.CurrentUser as IInternalUserMin).UserID);
                    _cr.Message = string.Format("Schedule III from submitted succesfully with ref id {0}", _cr.Data);
                }
                else
                {
                    throw new InvalidOperationException("Form can not be updated");
                }
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                _cr.Data = 0;
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(_cr);
        }
        [HttpGet]
        public ActionResult ShowScheduleIIIFormReadOnly(long TabId)
        {
            Dictionary<string, object> _params = new Dictionary<string, object>();
            ReportUtils rt = new ReportUtils();
            _params.Add("TabId", TabId);
            return Redirect(rt.GetReportViewerURL(16, _params));
        }
        [HttpPost]
        public JsonResult GetRoomTypes(int? UsageId)
        {
            try
            {
                ScheduleThree_Bll bll = new ScheduleThree_Bll();
                var obj = bll.GetRoomTypes(UsageId);
                _cr.Data = obj;
                _cr.Message = string.Format("{0} Room Types found", obj.Count);
                _cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                _cr.Message = ex.Message;
                _cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                _cr.Data = null;
            }
            return Json(_cr);
        }
        #endregion
    }
}