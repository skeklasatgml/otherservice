﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Start;
using Valuation_Board.Models;
using Valuation_Board.Models.Application;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Controllers.Administration
{
    [SecuredFilter]
    public class DashBoardController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        // GET: DashBoard
        public ActionResult Index()
        {
            var totalCount = new DashBoard_Bll().CountTotalAssessesAllMunicipality();
            var TotalMunicipalityCount= new DashBoard_Bll().TotalMuniciaplityCount();
            var TotalCollection = new DashBoard_Bll().GetTotalCollection(null,null,null).Sum(t=>t.NetPaidAmount);
            var Municipalities = new DashBoard_Bll().GetMunicipalityList(null);
            return View("~/Views/Administration/DashBoard/Index.cshtml",new Dashboard_vm() { TotalAssesseesCount=totalCount,TotalMunicipalityCount= TotalMunicipalityCount,TotalCollection= TotalCollection,Municipalities=Municipalities });
        }
        [HttpPost]
        public JsonResult DrillAssesseGroupByDistricts(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                List<Dashboard_vm.AssesseesByDistrict> lstVm = new DashBoard_Bll().AssesseCountInfoGroupByDistricts(FromDate,ToDate);
                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found",lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByDistrict>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult DrillAssesseGroupByMunicipalities(int DistrictId,DateTime FromDate, DateTime ToDate)
        {
            try
            {
                List<Dashboard_vm.AssesseesByMuncipaity> lstVm = new DashBoard_Bll().AssesseCountInfoGroupByMunicipalities(DistrictId, FromDate, ToDate);
                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByMuncipaity>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult DrillAssesseByWardMonthWise(int MunicipalityId,int WardId, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                List<Dashboard_vm.AssesseesByWardMonthWise> lstVm = new DashBoard_Bll().AssesseCountInfoByWardMonthWise(MunicipalityId,WardId,FromDate,ToDate);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByWardMonthWise>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult AssesseListByWardMonthYear(int MunicipalityId, int WardId, string MonthYear)
        {
            try
            {

                List<Dashboard_vm.Assessee> lst = new DashBoard_Bll().AssesseListByWardMonthYear(MunicipalityId, WardId, MonthYear);

                cr.Data = lst;
                cr.Message = string.Format("{0} records found", lst.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.Assessee>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult DrillAssesseesByWard(int MunicipalityId, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                List<Dashboard_vm.AssesseesByMuncipaityWard> lstVm = new DashBoard_Bll().AssesseesCountInfoByWards(MunicipalityId, FromDate, ToDate);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByMuncipaityWard>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetMunicipalitiesValuations()
        {
            try
            {
                List<Dashboard_vm.MunicipalityValuation> lstVm = new DashBoard_Bll().GetMunicipalityValuations();
                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByMuncipaityWard>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult LastCountMunicipalities(int CountRow)
        {
            try
            {
                List<Dashboard_vm.Municipality> lstVm = new DashBoard_Bll().GetMunicipalityList(CountRow);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.AssesseesByMuncipaityWard>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetCollectionSummary(DateTime FromDate, DateTime ToDate,List<int> MunicipalityIds)
        {
            try
            {
                var lstVm = new DashBoard_Bll().GetCollectionSummary(MunicipalityIds, FromDate,ToDate);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MuncipalityCollectionSummary>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult MunicipalityWiseCollection(DateTime FromDate, DateTime ToDate, List<int> MunicipalityIds, int RowCount,string OrderBy)
        {
            try
            {
                List<Dashboard_vm.MunicipalityWiseCollection2> lstVm = new DashBoard_Bll().MunicipalityWiseCollection(MunicipalityIds, FromDate, ToDate, RowCount,OrderBy);

                cr.Data = lstVm;
                cr.Message = string.Format("{0} records found", lstVm.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = new List<Dashboard_vm.MunicipalityWiseCollection>();
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }


    }
}