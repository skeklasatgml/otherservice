﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Valuation_Board.Controllers.Error
{
    public class Error401Controller : Controller
    {
        // GET: Error401
        public ActionResult Index()
        {
            return View("~/Views/Error/Error401/Index.cshtml");
        }
    }
}