﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.ModelBinders;
using Valuation_Board.App_Start;

namespace Valuation_Board
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configuration.EnsureInitialized();
            ModelBinderProviders.BinderProviders.Add(new CustomModelBinderProvider());

            //custom area init
            GlobalContext.Init();

            log4net.ILog logger = log4net.LogManager.GetLogger("default logger");
            logger.Info("Application Started up");
        }
    }
}
