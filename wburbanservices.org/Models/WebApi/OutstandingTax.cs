﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.WebApi
{
    public class OutstandingTax
    {
        public string WardName { get; set; }
        public string LocationName { get; set; }
        public string AssesseeNo { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingTypeDesc { get; set; }
        public string FinYear { get; set; }
        public int QtrNo { get; set; }
        public decimal OSPropertyTaxAmt { get; set; }
        public decimal OSSurchargeAmt { get; set; }
    }
}