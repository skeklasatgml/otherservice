﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Valuation_Board.Models.Account;

namespace Valuation_Board.Models.Account 
{
    public class MunicipalityUser:InternalUser
    {
        public string UserName
        {
            get;
            set;
        }
        public int MunicipalityID
        {
            get;
            set;
        }
    }
}
