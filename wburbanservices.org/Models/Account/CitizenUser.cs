﻿using System;
using Valuation_Board.Models.Application;

namespace Valuation_Board.Models.Account
{
    public class CitizenUser : IUserType, IInternalUserMin
    {
        public CitizenUser()
        {
        }
        public long AssesseeID { get; set; }
        public long? ParentAssesseeID { get; set; }
        public string AssesseeNo { get; set; }
        public int? MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public int? WardID { get; set; }
        public string WardName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public int? HoldingTypeID { get; set; }
        public string HoldingTypeDesc { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public string AssesseeAddress { get; set; }
        public string PrimaryPhNo { get; set; }

        public string PrimaryEmail { get; set; }

        public string Block { get; set; }
        public string Mouja { get; set; }
        public string Plot { get; set; }
        public string Khatian { get; set; }
        public string JLNo { get; set; }
        public string DeedNo { get; set; }

        public UserType UserType
        {
            get { return UserType.CitizenUser; }
        }

        public long UserID { get { return AssesseeID; } set { AssesseeID = value; } }
        public int RoleId {get;set;}

        public Assessee Convert()
        {
            return new Assessee()
            {
                AssesseeID = this.AssesseeID,
                ParentAssesseeID = this.ParentAssesseeID,
                AssesseeNo = this.AssesseeNo,
                MunicipalityID = this.MunicipalityID,
                MunicipalityName = this.MunicipalityName,
                WardID = this.WardID,
                WardName = this.WardName,
                LocationID = this.LocationID,
                LocationName = this.LocationName,
                HoldingTypeID = this.HoldingTypeID,
                HoldingTypeDesc = this.HoldingTypeDesc,
                HoldingNo = this.HoldingNo,
                AssesseeName = this.AssesseeName,
                AssesseeAddress = this.AssesseeAddress,
                PrimaryEmail=this.PrimaryEmail,
                PrimaryPhNo=this.PrimaryPhNo
            };
        }

    }
}
