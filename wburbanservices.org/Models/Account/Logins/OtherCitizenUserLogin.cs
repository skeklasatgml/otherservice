﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Account.Logins
{
    public class OtherCitizenUserLogin
    {

    }
    public class CitizenReg
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Name { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int IsActive { get; set; }
        public int IsOTPVerified { get; set; }
        public int TypeOfRegistration { get; set; }
    }
}