﻿namespace Valuation_Board.Models.Account
{
    public enum UserType
    {   CitizenUser=1,
        MunicipalityUser=2,
        AdministrativeUser=3,
        OtherCitizenUser = 4,
    }
}
