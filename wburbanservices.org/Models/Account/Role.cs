﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Account
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public UserType UserType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}