﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models
{
    public class KeyValue<T,T1>
    {
        public T Key { get; set; }
        public T1 Value { get; set; }
    }
    public class KeyValue<T, T1, T2> : KeyValue<T, T1>
    {
        public T2 Other { get; set; }
    }
}