﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class ArrearDemand
    {
        public string FinYear  { get; set; }
        public int Qtr { get; set; }
        public decimal PropertyTaxAmnt { get; set; }
        public decimal SurchargeAmnt { get; set; }
    }
}