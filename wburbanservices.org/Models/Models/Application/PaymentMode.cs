﻿using System.Collections.Generic;

namespace Valuation_Board.Models.Application
{
    public class PaymentMode
    {
        public string PaymentModeCode { get; set; }
        public string PaymentModeDescription { get; set; }
        public decimal TotalAmount { get; set; }
        public bool HasInstrument { get; set; }
        public List<PaymentInstrumentBase> PaymentInstruments { get; set; }
        public int ModeType { get; set; }
        public PaymentMode()
        {
            PaymentInstruments = new List<PaymentInstrumentBase>();
        }
    }
}