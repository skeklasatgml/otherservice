﻿using System;

namespace Valuation_Board.Models.Application
{
    public class PaymentInsturmentType5 : PaymentInstrumentBase
    {
        public string InstrumentNo { get; set; }
        public DateTime InstrumentDate { get; set; }
        public string BankName { get; set; }
        public string CardNo { get; set; }
    }
}