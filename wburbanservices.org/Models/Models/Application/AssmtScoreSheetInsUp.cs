﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssmtScoreSheetInsUp
    {
        public int MunicipalityID { get; set; }
        public string YearId { get; set; }
        public long ScoreSheetID { get; set; }
        public int ScoreBeltTypeId { get; set; }
        public string ScoreCode { get; set; }
        public string ScoreDesc { get; set; }
        public decimal ScoreValue { get; set; }
        public long CurrentUserId { get; set; }

    }
}