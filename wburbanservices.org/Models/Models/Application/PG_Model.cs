﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PG_Model
    {
        public PG_Model()
        {

        }
        public string billerid { get; set; }
        /// <summary>
        /// transaction ID,assesseno+curDate("ddMMyyHHmmss")
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// transaction Amount decimal
        /// </summary>
        public string TxnAmount { get; set; }
        /// <summary>
        /// AdditionalInfo1
        /// </summary>
        public string HoldingNo  { get; set; }
        /// <summary>
        /// AdditionalInfo2
        /// </summary>
        public string WardID { get; set; }
        /// <summary>
        /// AdditionalInfo3
        /// </summary>
        public string MunicipalityID  { get; set; }
        /// <summary>
        /// AdditionalInfo4
        /// </summary>
        public string LocationID { get; set; }
        /// <summary>
        /// AdditionalInfo5
        /// </summary>
        public string AssesseeName { get; set; }
        /// <summary>
        /// AdditionalInfo6,finyear+paymentid
        /// </summary>
        public string PaymentID { get; set; }
        /// <summary>
        /// AdditionalInfo7
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// Bildesk Url
        /// </summary>
        public string Reffaral_URL { get; set; }
        /// <summary>
        /// Return_URL
        /// </summary>
        public string Return_URL { get; set; }
        public string AssesseeNo { get; set; }
        public string PG_Code { get; set; }
        public string UsedTransactionId { get; set; }
        //public string GenTransactionID()
        //{
        //   return GenTransactionID(this.AssesseeNo);
        //}

        public static string GenTransactionID(string AssesseeNo)
        {
            return string.Format("{0}{1}", AssesseeNo, DateTime.Now.ToString("ddMMyyHHmmss"));
            //return string.Format("{0}", DateTime.Now.ToString("ddMMyyHHmmss"));
        }
    }
}