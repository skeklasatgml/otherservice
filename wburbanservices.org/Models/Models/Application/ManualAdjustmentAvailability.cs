﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class ManualAdjustmentAvailability
    {
        public bool IsAvailable { get; set; }
        public decimal AdvanceAmount { get; set; }
        public DateTime? AdvanceDate { get; set; }
    }
}