﻿namespace Valuation_Board.Models.Application
{
    public class ValudationUserPermissionScheduleIII
    {
        public int MunicipalityId { get; set; }
        public int WardId { get; set; }
        public string WardNo { get; set; }
    }
}