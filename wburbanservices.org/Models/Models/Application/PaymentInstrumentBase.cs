﻿namespace Valuation_Board.Models.Application
{
    public class PaymentInstrumentBase
    {
        public decimal InstrumentAmount { get; set; }
        public int ModeType { get; set; }

    }
}