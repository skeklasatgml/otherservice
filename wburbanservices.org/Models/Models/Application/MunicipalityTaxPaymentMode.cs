﻿using System.Collections.Generic;

namespace Valuation_Board.Models.Application
{
    public class MunicipalityTaxPaymentMode
    {
        public string PaymentCode { get; set; }
        public string PaymentCodeName { get; set; }
        public bool HasInstruments { get; set; }
        public decimal Amount { get; set; }
        public int MunicipalityID { get; set; }
        public Municipality Municipality { get; set; }
        public List<PaymentInstrument> PaymentInstruments { get; set; }
        public MunicipalityTaxPaymentMode()
        {
            PaymentInstruments = new List<PaymentInstrument>();
        }
    }
}