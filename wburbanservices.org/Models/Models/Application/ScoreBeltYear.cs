﻿using System;

namespace Valuation_Board.Models.Application
{
    public class ScoreBeltYear
    {
        public int MunicipalityID { get; set; }
        public int Year { get; set; }
        public string YearDesc { get; set; }
        public DateTime? EffectiveDateForm { get; set; }
        public DateTime? EffectiveDateTo { get; set; }
        public decimal? Factor { get; set; }
    }
}
