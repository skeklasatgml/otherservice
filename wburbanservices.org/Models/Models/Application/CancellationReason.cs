﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class CancellationReason
    {
        public int ReasonID { get; set; }
        public string ReasonText { get; set; }
    }
}