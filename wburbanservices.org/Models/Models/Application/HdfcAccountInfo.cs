﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public abstract class HdfcAccountBase
    {
        public int MappingID { get; set; }
        public string PGCode { get; set; }
        public int MuncipalityID { get; set; }
        public string PaymentUrl { get; set; }
        public string RefundUrl { get; set; }
        public bool IsActive { get; set; }
        public string ResposeUrl { get; set; }
        public string CancelUrl { get; set; }
    }
    public class HdfcAccountInfo: HdfcAccountBase
    {
        public string Algorithm { get; set; }
        public string AccountID { get; set; }
        public string SecreteKey { get; set; }
        
    }
    public class HdfcAccountInfo_CCAVNW : HdfcAccountBase
    {
        public string MarchentId { get; set; }
        public string AccessCode { get; set; }
        public string WorkingKey { get; set; }

    }
}

