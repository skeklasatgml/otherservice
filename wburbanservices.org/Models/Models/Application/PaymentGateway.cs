﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PaymentGateway
    {
        public string PG_Code { get; set; }
        public string PG_Name { get; set; }
        public string Description { get; set; }
    }
}