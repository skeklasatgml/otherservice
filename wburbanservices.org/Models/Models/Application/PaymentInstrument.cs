﻿using System;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PaymentInstrument: PaymentInstrumentBase
    {
        public string  InstrumentNo  { get; set; }
        public DateTime InstrumentDate { get; set; }
        public string BankName { get; set; }
        public int? BankCode { get; set; }
    }
}