﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PropertyTaxPaymentDetail
    {
        public long? PaymentDetailID { get; set; }
        public string FinYear { get; set; }
        public long? PaymentID { get; set; }
        public string PayArrrearBill { get; set; }
        public long? DemandTypeID { get; set; }
        public int? QtrNo { get; set; }
        public decimal? TaxValue { get; set; }
        public decimal? SurchargeValue { get; set; }
        public decimal? CalculatedValue { get; set; }
        public decimal? PaidTaxAmt { get; set; }
        public decimal? PaidSurchargeAmt { get; set; }
        #region 182112017 new properties added
        public decimal PaidPropTaxInterest { get; set; }
        public decimal PaidSurchrgInterest { get; set; }
        #endregion
        public decimal? PaidCalculatedAmt { get; set; }
        public int? InsertedBy { get; set; }
        public string InsertedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public int? PayHeadTypeID { get; set; }
        public decimal LineTotal { get; set; }
        public decimal LineTotalRoundOff { get; set; }
        public PayHead payHead { get; set; }
        public string AdvAdjstType { get; set; }
        public long? DatAdvPaymentID { get; set; }
        public TaxPaymentCheckParameter taxPaymentCheckParameter { get; set; }

        
    }
}