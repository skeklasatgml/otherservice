﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Base64File
    {
        public string Result { get; set; }
        public string Name { get; set; }
    }
    public class DprModel
    {
        public class DprListModel
        {
            public List<KeyValue<int, string>> Ulbs { get; set; }
            public int? CurrUlbId { get; set; }
            public List<SchemeType> SchemeTypes { get; set; }
            public List<SchemeCategory> SchemeCategories { get; set; }

            public DprListModel()
            {
                Ulbs = new List<KeyValue<int, string>>();
                SchemeCategories = new List<SchemeCategory>();
                SchemeTypes = new List<SchemeType>();
            }
        }
        public class Check
        {
            public int Id { get; set; }
            public string Perticular { get; set; }
            public string Val { get; set; }
            public string PageNo { get; set; }
            public string Remarks { get; set; }
            public bool Approve { get; set; }
            public int PartSlNo { get; set; }
        }
        public class Check2 : Check
        {
            public bool IsReadOnly { get; set; }
            public string DefaultValue { get; set; }
        }
        public class Note
        {
            public string Description { get; set; }
        }
        public class Part
        {

            public List<Check2> CheckList { get; set; }
            public List<Note> Notes { get; set; }

            public Part()
            {
                CheckList = new List<Check2>();
                Notes = new List<Note>();
            }
        }
        public class SchemeType
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }

        }
        public  class UnitMaster
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsActive { get; set; }
        }
        public class SchemeCategory
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public int SchemeTypeId { get; set; }
        }
        public class PhasingOfExpenditure : IValidatable
        {
            public long Id { get; set; }
            public string FinYear { get; set; }
            public decimal ExpenditureAmount { get; set; }
            public void Validate()
            {
                if (this.ExpenditureAmount <= 0) throw new Exception(string.Format("Proposed Expenditure can not be Rs. {0} on Fin-Year {1}", this.ExpenditureAmount, FinYear));
            }
        }
        public class SubProject : IValidatable
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public decimal Cost { get; set; }
            public long MstDprId { get; set; }
            public int Qty { get; set; }
            public int UnitId { get; set; }
            public string UnitName { get; set; }

            public void Validate()
            {
                if (string.IsNullOrEmpty(Name)) throw new ArgumentException("Sub Project Name cant not be empty");
                if (Cost < 0) throw new ArgumentException(string.Format("Estimated Cost of sub project '{0}' can not be less than 0", Name));
                if (Qty <= 0) throw new ArgumentException("Quantity can not be zero");
                if (UnitId <= 0) throw new ArgumentException("Invalid Unit Id");
            }
        }
        public class SubProjectInstallment:SubProject
        {
            public bool IsApplied { get; set; }
            public long? InstlmntId { get; set; }
            public long NativeId { get; set; }
        }
        public class SubmittedDpr
        {
            public long MstDprId { get; set; }
            public DateTime DocVerificationDate { get; set; }
        }
        public class Master
        {
            public int MunicipalityId { get; set; }
            public string MunicipalityName { get; set; }
            public string NameOfWork { get; set; }
            public string AgencyName { get; set; }
            public decimal EstimatedCost { get; set; }

            public Part Part1 { get; set; }
            public Part Part2 { get; set; }
            public DateTime? MemoDate { get; set; }
            public string MemoNo { get; set; }
            public string NameOfEngineer { get; set; }
            public string Designation { get; set; }
            public string ContactNo { get; set; }
            public DateTime? SchemeDate { get; set; }
            public string IdentityStamp { get; set; }
            public string EmailId { get; set; }
            public int SchemeTypeId { get; set; }
            public int SchemeCategoryId { get; set; }
            public List<SchemeType> SchemeTypes { get; set; }
            public List<SchemeCategory> SchemeCategories { get; set; }
            public List<PhasingOfExpenditure> PhasingOfExpenditures { get; set; }
            public bool HasSubProj { get; set; }
            public List<SubProject> SubProjects { get; set; }
            public List<UnitMaster> Units { get; set; }
            public long DPChkSlId { get; set; }
            public string Status { get; set; }
            public string CheckListDocUrl { get; set; }

            public Master()
            {
                SubProjects = new List<SubProject>();
                SchemeTypes = new List<SchemeType>();
                SchemeCategories = new List<SchemeCategory>();
                PhasingOfExpenditures = new List<PhasingOfExpenditure>();
                Units = new List<UnitMaster>();  
            }
        }
        public class OldDpr : Master
        {
            public List<KeyValue<int, string>> Municipalities { get; set; }
            public string ProjectNo { get; set; }
            public DateTime? Projectdate { get; set; }
            public DateTime? DprVerifiedOn { get; set; }
            public DateTime? DprApprovalDate { get; set; }
            public decimal ApprovedAmount { get; set; }
            public DateTime? DprSubmissionDate { get; set; }
            public string SanctionMemoNo { get; set; }
            public string AccountHeadNo { get; set; }
            public string UONo { get; set; }
            public DateTime? UODate { get; set; }
            public OldDpr()
            {
                Municipalities = new List<KeyValue<int, string>>();
            }
        }
        public class Dpr
        {
            public long DPChkSlId { get; set; }
            public string ProjectNo { get; set; }
            public string EmailID { get; set; }
            public DateTime? ProjectDate { get; set; }
            public string DeptType { get; set; }
            public int MunicipalityDevAuthoID { get; set; }
            public string MunicipalityDevAuthoName { get; set; }
            public string NameOfWork { get; set; }
            public string ImplementingAgency { get; set; }
            public decimal EstimatedCost { get; set; }
            public string NameOfEngineer { get; set; }
            public string Designation { get; set; }
            public string ContactNumber { get; set; }
            public string MemoNumber { get; set; }
            public DateTime MemoDate { get; set; }
            public string DPChkStatus { get; set; }
            public int? ApproChkBy { get; set; }
            public DateTime? ApproChkOn { get; set; }
            public string DocFilePath { get; set; }
            public int InsertedBy { get; set; }
            public DateTime InsertedOn { get; set; }
            public int? UpdatedBy { get; set; }
            public DateTime? UpdatedOn { get; set; }
            public DateTime? DocVerificationDate { get; set; }
            public bool existInDprAllocation { get; set; }
            public bool isFinallAprovalPossible { get; set; }
            public string dprProgStat { get; set; }
            //still not used
            public List<DprFundRelease.FundReleaseStatus> FundReleaseStatus { get; set; }
            public decimal TotalReleasedAmnt { get; set; }
            public decimal TotalApprovedAmount { get; set; }
            public int schemeTypeId { get; set; }
            public int schemeCategoryId { get; set; }
            public bool HasSubProj { get; set; }
            public string CheckListDocUrl { get; set; }

            public Dpr()
            {
                FundReleaseStatus = new List<DprFundRelease.FundReleaseStatus>();
            }
        }
        public class DprDraft
        {
            public Master DprMaster { get; set; }
            public DateTime DraftOn { get; set; }

        }
        public class DprApproval : Dpr
        {
            public decimal LastApprovedAmount { get; set; }
            public bool canApprovalAmtChangble { get; set; }
            public bool CanGenerateAAFS { get; set; }
            public List<Hierarchey> Hierarcheys { get; set; }
            public List<Designation> Designations { get; set; }
            public List<DprAllocationStep> DprAllocationSteps { get; set; }
            public List<ApprovalAction> ApprovalActions { get; set; }
            public List<BudgetMaster> Budgets { get; set; }
            public bool HasSubProj { get; set; }
            public List<SubProject> SubProjects { get; set; }
            public FinalApprovalExt AaFs { get; set; }
            public List<AppliedBudgetHead> BudgetHeadsApplied { get; set; }
            public DprApproval()
            {
                DprAllocationSteps = new List<DprAllocationStep>();
                Designations = new List<Designation>();
                Hierarcheys = new List<Hierarchey>();
                ApprovalActions = new List<ApprovalAction>();
                Budgets = new List<BudgetMaster>();
                SubProjects = new List<SubProject>();
                BudgetHeadsApplied = new List<AppliedBudgetHead>();
            }
        }
        public class Budget
        {
            public string BCode { get; set; }
            public int MnId { get; set; }
            public string BDesc { get; set; }
            public string FinYr { get; set; }
            public decimal BAmt { get; set; }
            public DateTime EffDt { get; set; }
            public int FundType { get; set; }
            public string Remarks { get; set; }
        }
        public class Tender
        {
            public int? MnId { get; set; }
            public string ProjectNo { get; set; }
            public string TenderNo { get; set; }
            public string TenderCallNo { get; set; }
            public DateTime TndrOpenDt { get; set; }
            public DateTime TndrCloseDt { get; set; }
            public string AwardedTo { get; set; }
            public string WrkOrdrNo { get; set; }
            public DateTime WrkOrdrDt { get; set; }
            public DateTime WrkStrtDt { get; set; }
            public DateTime WrkCmpltnDt { get; set; }
            public string EngName { get; set; }
            public string EngEmail { get; set; }
            public string EngPhone { get; set; }
            public long? SubProjId { get; set; }
            public string SubProjName { get; set; }
        }
        public class BudgetCode
        {
            public string BCode { get; set; }
            public string BCodeId { get; set; }
        }
        public class BudgetMaster
        {
            public int Id { get; set; }
            public string BCode { get; set; }
            public string Dscr { get; set; }
            public decimal BAmt { get; set; }
            public decimal AvAmnt { get; set; }
        }
        public class FinalApprovalExt
        {
            public string AccountHead { get; set; }
            public DateTime? UoDate { get; set; }
            public string SanctionNo { get; set; }
            public string UoNo { get; set; }
            public DateTime? ApprovalDate { get; set; }
            public DateTime? SanctionDate { get; set; }
            public int? BudgetId { get; set; }
            public List<SubProject> Subprojects { get; set; }
            public List<AppliedBudgetHead> BudgetHeads { get; set; }
            public string AccordedBy { get;  set; }
            public bool IsStagedData { get; set; }

            public FinalApprovalExt()
            {
                Subprojects = new List<SubProject>();
                BudgetHeads = new List<AppliedBudgetHead>();
            }
        }
        public class AppliedBudgetHead
        {
            public int Id { get; set; }
            public decimal DeductedAmount { get; set; }

        }
        public class ChangeDprApproval: FinalApprovalExt
        {
            public long mstDprId { get; set; }
            public string status { get; set; }
            public string remarks { get; set; }
            public int? targetDesignationId{ get; set; }
            public decimal approvedProjectAmount { get; set; }
            
        }
        public class ChangeDprInstallmentApproval : FinalApprovalExt
        {
            public long fundReleaseId { get; set; }
            public string status { get; set; }
            public string remarks { get; set; }
            public int? targetDesignationId { get; set; }
            public decimal approvedAmount { get; set; }


        }
        public class DprOtherAttributes
        {
            public bool IsApporvalAmountChangable { get; set; }
            public decimal LastApprovedAmount { get; set; }
            public bool CanGenerateAAFS { get; set; }
        }
        public class Hierarchey
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class Designation
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int HierarcheyId { get; set; }
        }
        public class DprAllocationStep
        {
            public long Id { get; set; }
            public string Remarks { get; set; }
            public int? TDesignationId { get; set; }
            public string TDesignationName { get; set; }
            public decimal AprovedProjectAmount { get; set; }
            public int? HiarcheyId { get; set; }
            public long? AssignedById { get; set; }
            public string AssignedByName { get; set; }
            public long? AssignedToId { get; set; }
            public string AssignedToName { get; set; }
            public DateTime? AssignedOn { get; set; }
            public string Status { get; set; }
            public DateTime? lastActivity { get; set; }
            public DateTime? RepliedOn { get; set; }
        }
        public class DprFundRelease
        {
            public class InitiationModel
            {
                public long MstDprId { get; set; }
                public string AgencyName { get; set; }
                public Dpr DprMaster { get; set; }
                public decimal ApprovedProjectCost { get; set; }
                public decimal TotalCumulativeAmount { get; set; }
                public List<InstallmentMaster> InstallmentMasters { get; set; }
                public string NameOfTheProject { get; set; }
                public string ProjectNo { get; set; }
                public List<FundReleaseAplied> AppliedInstallments { get; set; }

                public InitiationModel()
                {
                    InstallmentMasters = new List<InstallmentMaster>();
                    AppliedInstallments = new List<FundReleaseAplied>();
                }
            }
            public class InstallmentMaster
            {
                public int Id { get; set; }
                public string Name { get; set; }
                public int Rank { get; set; }
                public List<InstallmentCheckListMasterObject> CheckList { get; set; }
                public List<SubProjectInstallment> SubProjects { get; set; }
                public InstallmentMaster()
                {
                    CheckList = new List<InstallmentCheckListMasterObject>();
                    SubProjects = new List<SubProjectInstallment>();
                }
            }

            public class InstallmentCheckListMasterObject
            {
                public int ChkId { get; set; }
                public int InstallmentId { get; set; }
                public string Perticular { get; set; }
                public bool IsReadOnly { get; set; }
                public string DefValue { get; set; }
            }
            public class FundRelease
            {
                public long FundReleaseId { get; set; }
                public long MstDprId { get; set; }
                public int InstallmentId { get; set; }
                public decimal InstallmentAmount { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string MemoNo { get; set; }
                public DateTime? MemoDate { get; set; }
                public DateTime? AppliedOn { get; set; }
                public List<ApplyCheck> ApplyCheckList { get; set; }
                public InstallmentMaster InstallmentMaster { get; set; }
                public List<SubProjectInstallment> SubProjects { get; set; }
                public FundRelease()
                {
                    ApplyCheckList = new List<ApplyCheck>();
                    SubProjects = new List<SubProjectInstallment>();
                }
            }
            

            public class FundReleaseStatus
            {
                public long FundReleaseId { get; set; }
                public long MstDprId { get; set; }
                public string Status { get; set; }
                public string StatusDscr { get; set; }
                public string InstallmentName { get; set; }
            }
            public class FundReleaseAplied : FundRelease
            {
                public string Status { get; set; }
                public string StatusDscr { get; set; }
                public DateTime? LastStatusChangedOn { get; set; }
                public bool IsFinallAprovalPossible { get; set; }
                public bool canApprovalAmtChangble { get; set; }
            }
            public class OldFundRelease : FundReleaseAplied
            {
                public string UoNo { get; set; }
                public DateTime? UoDate { get; set; }
                public string SanctionMemoNo { get; set; }
                public string AccountHeadNo { get; set; }
                public DateTime? VerificationDate { get; set; }
                public decimal ApprovedAmount { get; set; }
                public DateTime? ApprovalDate { get; set; }
                public string ProjectNo { get; set; }
            }
            public class FundReleaseAppliedApproval : FundReleaseAplied
            {
                public List<Designation> Designations { get; set; }
                public List<DprAllocationStep> DprAllocationSteps { get; set; }
                public decimal LastApprovedAmount { get; set; }
                public List<ApprovalAction> ApprovalActions { get; set; }
                public FundReleaseAppliedApproval()
                {
                    Designations = new List<Designation>();
                    DprAllocationSteps = new List<DprAllocationStep>();
                    ApprovalActions = new List<ApprovalAction>();
                    
                }
            }
            public class ApplyCheck : InstallmentCheckListMasterObject
            {
                public bool IsChecked { get; set; }
                public string VerifyRemarks { get; set; }
                public bool isVerified { get; set; }
                public DateTime? verifiedOn { get; set; }
                public long verifiedby { get; set; }
                public long fundReleaseId { get; set; }
            }

        }
        public class DprDashboard
        {
            public class FindProject
            {
                public string PorjectNo { get; set; }
                public long MstDprId { get; set; }
            }
            public class FilterConfiguration
            {
                bool _AddRemoveMunicipality = true;
                public bool AddRemoveMunicipality
                {
                    get
                    {
                        return _AddRemoveMunicipality;
                    }
                    set
                    {
                        _AddRemoveMunicipality = value;
                    }
                }
            }
            public class OnLoadData
            {
                public FilterConfiguration FilterConfig { get; set; }
                public List<Municipality> Municipalities { get; set; }
                public List<Authority> Authorities { get; set; }
                public List<SchemeType> SchemeTypes { get; set; }
                public List<SchemeCategory> SchemeCategories { get; set; }
                public OnLoadData()
                {
                    Authorities = new List<Authority>();
                    Municipalities = new List<Municipality>();
                    FilterConfig = new FilterConfiguration();
                    SchemeTypes = new List<SchemeType>();
                    SchemeCategories = new List<SchemeCategory>();
                }

            }
            public class ReleaseStatus2 : DprFundRelease.FundReleaseStatus
            {
                public decimal ReleasedAmount { get; set; }
                public DateTime? ReleaseDate { get; set; }
            }
            public class DprMinDash
            {
                public long DprId { get; set; }
                public decimal EsimatedCost { get; set; }
                public int ScemeTypeId { get; set; }
                public decimal ApprovedAmount { get; set; }
                public decimal TotalReleasedAmnt { get; set; }
                public string Status { get; set; }
                public int MunicipalityId { get; set; }
                public string DeptType { get; set; }
                public DateTime? SubmissionDate { get; set; }
                public DateTime? ApprovalDate { get; set; }
                public KeyValue<int, string> Scheme { get; set; }
                public KeyValue<int, string> Municipality { get; set; }
                public List<ReleaseStatus2> InstallmentReleases { get; set; }
                public string NameOfWork { get; set; }
                public DprMinDash()
                {
                    InstallmentReleases = new List<ReleaseStatus2>();
                }
            }
            public class FinYearWiseData
            {
                public string FinYear { get; set; }
                public decimal TotalApproved { get; set; }
                public decimal TotalApplied { get; set; }
                public decimal TotalReleased { get; set; }
                public string Flag { get; set; }
                public string Status { get; set; }
            }
            public class DprMinDashPayload
            {
                public List<int> MunicipalityIds { get; set; }
                public string TimeSpanFlag { get; set; }
                public DateTime? FromDate { get; set; }
                public DateTime? ToDate { get; set; }
                public int? AuthorityTypeId { get; set; }
                public int? SchemeTypeId { get; set; }
                public int? SchemeCategoryId { get; set; }
                public DprMinDashPayload()
                {
                    MunicipalityIds = new List<int>();
                }
            }
            public class DprMinDashPayloadStat: DprMinDashPayload
            {
                public string Status { get; set; }
            }
            public class DprDashPayloadModl1
            {
               public string FromFinYear { get; set; }
               public string ToFinYear { get; set; }
               public List<int> MunicipalityIds { get; set; }
                public int? AuthorityTypeId { get; set; }
                public int? SchemeTypeId { get; set; }
                public int? SchemeCategoryId { get; set; }
                public DprDashPayloadModl1()
                {
                    MunicipalityIds = new List<int>();
                }
            }
            public class DprDashPayloadModl2
            {
               public DateTime? date { get; set; }
                public List<int> MunicipalityIds { get; set; }
                public int? SchemeTypeId { get; set; }
                public int? SchemeCategoryId { get; set; }
                public DprDashPayloadModl2()
                {
                    MunicipalityIds = new List<int>();
                }
            }
        }
        public class DprDashboard2
        {
            public class Submitted
            {
                public long DprId { get; set; }
                public decimal EstimatedAmount { get; set; }
                public int UlbID { get; set; }
                public string UlbName { get; set; }
                public int UlbTypeId { get; set; }
                public string FinYear { get; set; }
                public DateTime? SubmissionDate { get; set; }
                public int ScemeTypeId { get; set; }
                public string ScemeTypeName { get; set; }
            }
            public class Apporved : Submitted
            {
                public DateTime? approvedOn { get; set; }
                public decimal ApprovedAmount { get; set; }
            }
            public class Released : Apporved
            {
                public long ReleaseId { get; set; }
                public string ReleaseName { get; set; }
            }

            public class Model_1{
                public List<Submitted> Submitteds { get; set; }
                public List<Submitted> Returneds { get; set; }
                public List<Apporved> Apporveds { get; set; }
                public List<Released> Releaseds { get; set; }
                public Model_1()
                {
                    Releaseds = new List<Released>();
                    Apporveds = new List<Apporved>();
                    Returneds = new List<Submitted>();
                    Submitteds = new List<Submitted>();
                }
            }

        }
        public class ApprovalAction
        {
            public string Code { get; set; }
            public string Dscr { get; set; }
        }
    }
}
