﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssmtBeltSheet
    {
        public long BeltSheetID { get; set; }
        public string ScoreBeltTypeDesc { get; set; }
        public decimal FromRange { get; set; }
        public decimal ToRange { get; set; }
        public string BeltUnit { get; set; }
        public string BeltValue { get; set; }
    }
}