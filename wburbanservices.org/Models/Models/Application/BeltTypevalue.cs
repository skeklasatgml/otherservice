﻿namespace Valuation_Board.Models.Application
{
    public class BeltTypevalue
    {
        public string ValTag { get; set; }
        public string ValDesc { get; set; }
    }
}