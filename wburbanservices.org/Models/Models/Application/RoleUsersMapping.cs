﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class RoleUsersMapping
    {
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
    }
}