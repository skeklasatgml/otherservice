﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class BillDeskAccountInfo
    {
        
            public int MappingID { get; set; }
            public int MuncipalityID { get; set; }
            public string Algorithm { get; set; }
            public string AccountID { get; set; }
            public string SecreteKey { get; set; }
            public string PaymentUrl { get; set; }
            public string RefundUrl { get; set; }
            public bool IsActive { get; set; }
            public string ResposeUrl { get; set; }


    //<add key = "Algorithm" value="sha256" />
    //<add key = "AlgorithmApp" value="sha256" />
    //<add key = "MerchantID" value="MUNCTAXPAY" />
    //<add key = "SecurityID" value="munctaxpay" />
    //<add key = "CurrencyType" value="INR" />
    //<add key = "TypeField1" value="R" />
    //<add key = "TypeField2" value="F" />
    //<add key = "AdditionlaInfo1" value="NA" />
    //<add key = "AdditionlaInfo2" value="NA" />
    //<add key = "CheckSumKey" value="9YmVZpYb8HzV" />
    //<add key = "PaymentURL" value="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment" />
    //<!--<add key = "PaymentURL" value="http://localhost/online/onlinepayment/paymentresponse"/>-->
    //<add key = "ResponseURL" value="http://holdingtax.co.in/online/onlinepayment/paymentresponse" />
        
    }
}