﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.WebApi
{
    public class CurValModelApi
    {
        public int? MunicipalityID { get; set; }
        public int? v_year { get; set; }
        public int? v_id_no { get; set; }
        public int? v_srl { get; set; }
        public int? v_ward_no { get; set; }
        public int? v_street_code { get; set; }
        public string v_holding_no { get; set; }
        public string v_name { get; set; }
        public string v_address { get; set; }
        public string v_holding_type { get; set; }
        public byte? v_hold_type { get; set; }
        public string v_hold_area { get; set; }
        public string v_zone_code { get; set; }
        public string v_use_code { get; set; }
        public string v_const_code { get; set; }
        public decimal? v_tot_wt { get; set; }
        public short? v_age { get; set; }
        public short? v_land_area_kt { get; set; }
        public short? v_land_area_ch { get; set; }
        public short? v_land_area_sft { get; set; }
        public int? v_covered_area { get; set; }
        public int? v_plinth_area { get; set; }
        public decimal? v_land_cost { get; set; }
        public int? v_arv { get; set; }
        public decimal? v_disc_per { get; set; }
        public int? v_disc { get; set; }
        public int? v_rev_val { get; set; }
        public decimal? v_ptax_per { get; set; }
        public decimal? v_yr_proptax { get; set; }
        public decimal? v_qtr_proptax { get; set; }
        public decimal? v_prv_qtr_proptax { get; set; }
        public string v_srchg_tag { get; set; }
        public decimal? v_qtr_srchg { get; set; }
        public string v_edu_tag { get; set; }
        public decimal? v_qtr_edu_cess { get; set; }
        public string v_mouza { get; set; }
        public string v_khatian { get; set; }
        public string v_dag { get; set; }
        public string v_deed_no { get; set; }
        public string v_building_dtl { get; set; }
        public string v_water_tag { get; set; }
        public decimal? v_ferrule_size { get; set; }
        public string v_val_tag { get; set; }
        public int? v_val_year { get; set; }
        public int? v_val_qtr { get; set; }
        public int? v_val_srl { get; set; }
        public byte? v_val_sub_srl { get; set; }
        public string v_val_from_to { get; set; }
        public byte? v_from_ward_no { get; set; }
        public short? v_from_street_code { get; set; }
        public string v_from_holding_no { get; set; }
        public byte? v_old_ward_no { get; set; }
        public short? v_old_street_code { get; set; }
        public string v_old_holding_no { get; set; }
        public decimal? v_factor { get; set; }
        public string v_mc_id { get; set; }
        public string v_user_id { get; set; }
        public DateTime v_entry_time { get; set; }
    }
}