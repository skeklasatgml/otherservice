﻿namespace Valuation_Board.Models.Account
{
    public interface IUserType
    {
        UserType UserType { get;  }
    }
}
