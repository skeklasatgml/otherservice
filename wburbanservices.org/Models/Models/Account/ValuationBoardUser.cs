﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valuation_Board.Models.Account 
{
    public class AdministrativeUser:InternalUser
    {
        public string UserName
        {
            get;
            set;
        }
        public AdminType AdminType { get; set; }
    }
}
