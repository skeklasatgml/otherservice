﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Account.Logins
{
    public class CitizenLogin
    {
        public int MunicipalityID
        {
            get;
            set;
        }

        public int WardID
        {
            get;
            set;
        }

        public int LocationID
        {
            get;
            set;
        }

        public string HoldingNo
        {
            get;
            set;
        }
        public string Cmd { get; set; }
    }
}