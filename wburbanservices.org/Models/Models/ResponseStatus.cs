﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResponseStatus
/// </summary>
public enum ResponseStatus
{
	SUCCESS=200,
    CREATED=201,
    UNKNOWN_SERVER_ERROR=500,
    FORBIDDEN=403,//not authorised
    UNAUTHORIZED=401,//not authenticated
    INVALID_DATA=422,
    NOT_FOUND=404,
    UNPROCESSABLE_DATA=422
}