﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Report
{
    public class Collection
    {
        public int MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingNo { get; set; }
        public string HeadingDate { get; set; }
        public string PaymentReceiveDate { get; set; }
        public string receiptNo { get; set; }
        public string finyear { get; set; }
        public string PaymentType { get; set; }
        public int NoOfPayment { get; set; }
        public string PaymentTypeDesc { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int WardID { get; set; }
        public string WardName { get; set; }
        public string Arr_tax { get; set; }
        public string Arr_sur { get; set; }
        public string Qtr1_Ptax { get; set; }
        public string Qtr1_sur { get; set; }
        public string Qtr2_Ptax { get; set; }
        public string Qtr2_sur { get; set; }
        public string Qtr3_Ptax { get; set; }
        public string Qtr3_sur { get; set; }
        public string Qtr4_Ptax { get; set; }
        public string Qtr4_sur { get; set; }
        public string Rebate { get; set; }
        public string Interest { get; set; }
        public string ArrearEduCess { get; set; }
        public string CurrentEduCess { get; set; }
        public string WarrantPenalty { get; set; }
        public string ExcessAdj { get; set; }
        public string CollectorName { get; set; }
        public int CounterID { get; set; }
        public string TotalTax { get; set; }
        public string TotalSur { get; set; }
        public string CounterCode { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string BillReceiptNo { get; set; }
        public string AdjustedAmount { get; set; }
        public string CollectorAdjustedAmount { get; set; }
        public string AdvanceAmount { get; set; }
        public string NetAmount { get; set; }
        public Int64 PaymentID { get; set; }
        public string RoundOffAdjust { get; set; }
        public string ReceiptFromDate { get; set; }
        public string ReceiptToDate { get; set; }
        public string Cash { get; set; }
        public string Cheque { get; set; }
        public string Others { get; set; }
        public int HoldingTypeID { get; set; }
        public string HoldingTypeDesc { get; set; }
    }
}