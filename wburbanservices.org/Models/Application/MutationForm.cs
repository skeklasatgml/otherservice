﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MutationForm
    {
        public long? TabId { get; set; }
        public int? MutationTypeId { get; set; }
        public string MutationType { get; set; }
        public string ApplicantsName { get; set; }
        public string ApplicantsFatherHusband { get; set; }
        public string AddressNo { get; set; }
        public string AddressRdLn { get; set; }
        public string AddressViTn { get; set; }
        public string AddressPs { get; set; }
        public string AddressPO { get; set; }
        public string AddressDt { get; set; }
        public int? AddressPin { get; set; }
        public string AddressSt { get; set; }
        public string AddressLandMark { get; set; }
        public string CommunicationDetailsPh { get; set; }
        public string CommunicationDetailsMn { get; set; }
        public string CommunicationDetailsEmailIdn { get; set; }
        public int? IdProofType { get; set; }
        public HttpPostedFileBase IdProofData { get; set; }
        public string IdProofPath { get; set; }
        public string WardNo { get; set; }
        public string HoldingDtlsRoad { get; set; }
        public string HoldingDtlsLoMo { get; set; }
        public string HoldingDtlsHoldingNo { get; set; }
        public int? HoldingTypeId { get; set; }
        public int? LandRecBlock { get; set; }
        public string LandRecMouza { get; set; }
        public string JlNo { get; set; }
        public string PlotNoRs { get; set; }
        public string PlotNoLs { get; set; }
        public string KhatianNoRs { get; set; }
        public string KhatianNoLs { get; set; }
        public string QuanTumLandDc { get; set; }
        public string QuanTumLandKt { get; set; }
        public string LandTypeRor { get; set; }
        public string LandOwOcRor { get; set; }
        public string RorPath { get; set; }
        public HttpPostedFileBase RorData { get; set; }
        public bool? Assessed { get; set; }
        public string nameTaxPayer { get; set; }
        public string presentOwnerName { get; set; }
        public string presentOwnerAddress { get; set; }
        public string lastQuaterPaymentReceipt { get; set; }
        public string NameTrandVend { get; set; }
        public bool? TrandVendAssessee { get; set; }
        public string TrandVendTaxPaidUpto { get; set; }
        public string TrandVendNoRemarks { get; set; }
        public int? DeedTypeId { get; set; }
        public string DeedTypeNo { get; set; }
        public DateTime DeedTypeDate { get; set; }
        public string DeedTypeRegOff { get; set; }
        public decimal? DeedTypeVal { get; set; }
        public string DeedPath { get; set; }
        public HttpPostedFileBase DeedData { get; set; }
        public int? DevolvedType { get; set; }
        public string DevolvedPath { get; set; }
        public HttpPostedFileBase DevolvedData { get; set; }
        public string AreaDtlsTla { get; set; }
        public string AreaDtlsTvla { get; set; }
        public string AreaDtlsTp { get; set; }
        public string AreaDtlsLabf { get; set; }
        public string AreaDtlsFa { get; set; }
        public string parkingspace { get; set; }
        public string exclusivegarden { get; set; }
        public int? RoofTypeId { get; set; }
        public bool? CompleCert { get; set; }
        public string CompleCertNo { get; set; }
        public DateTime CompleCertDt { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string ApplicationNo { get; set; }
        public string AppliSigPath { get; set; }
        public HttpPostedFileBase AppliSigData { get; set; }
        public string OtpVerification { get; set; }
        public int? MunicipalityId { get; set; }
        public string ApplicationSource { get; set; }
        public string Status { get; set; }
    }
    public class MutationType
    {
        public int? MutationTypeId { get; set; }
        public string MutationTypeName { get; set; }
    }
    public class IdProofType
    {
        public int? IdProofTypeId { get; set; }
        public string IdProofTypeName { get; set; }
    }
    public class DevolvedType
    {
        public int? DevolvedTypeId { get; set; }
        public string DevolvedTypeName { get; set; }
    }
}