﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class SubModule
    {
        public int SubModuleId { get; set; }
        public string SubModuleName { get; set; }
        public string SubModuleCode { get; set; }
        public string SubModuleDescription { get; set; }
        public ApplicationCount Counter { get; set; }
    }
    public class ApplicationCount
    {
        public int APPROVED { get; set; }
        public int INPROCESS { get; set; }
        public int REJECTED { get; set; }
        public int SUBMITTED { get; set; }
    }
    public class Module : SubModule
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
    public enum Modules
    {
        PropertyTaxCollection=1,
        DprSubmission=2
    }
}