﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class ApplicationListModule
    {
        public long slno { get; set; }
        public long ServMstId { get; set; }
        public string ServMstIdEncrypted { get; set; }
        public string ApplicationNo { get; set; }
        public string ApplicationDate { get; set; }
        public string ProjectDescription { get; set; }
        public string ServiceCurrStatus { get; set; }
        public string DESCRIPTION { get; set; }
        public int? MunicipalityDevAuthoId { get; set; }
        public string MunicipalityDevAuthoIdEncrypt { get; set; }
        public List<URLMode> StatusActions { get; set; }
        public string MunicipalityName { get; set; }

    }
    public class URLMode
    {
        public int UserTypeId { get; set; }
        public int SubModuleId { get; set; }
        public string Actions { get; set; }
        public int Rank { get; set; }
        public int ServiceStatusId { get; set; }
        public string urlmode { get; set; }
        public long? ServMstId { get; set; }
        public string Data { get; set; }
    }
}