﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssmtScoreBelt
    {
        public string name { get; set; }
        public long? UserID { get; set; }
        public int? municipalityId { get; set; }
        public string year { get; set; }
        public string TransactionType { get; set; }


        public Int64? ScoreSheetID { get; set; }
        public int? ScoreBeltTypeId { get; set; }
        public string ScoreBeltTypeDesc { get; set; }
        public string ScoreCode { get; set; }
        public string ScoreDesc { get; set; }
        public decimal? ScoreValue { get; set; }


        public Int64? BeltSheetID { get; set; }
        public decimal? FromRange { get; set; }
        public decimal? ToRange { get; set; }
        public string BeltValue { get; set; }
        public string BeltDesc { get; set; }

        public string msg { get; set; }

    }
}