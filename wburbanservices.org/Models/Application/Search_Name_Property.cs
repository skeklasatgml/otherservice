﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{

    public class Search_Name_Property
    {
        public Deed_Details Deed_details { get; set; }
        public List<Party_Details> Party_details { get; set; }
        public List<Property_Details> Property_details { get; set; }
        public Search_Name_Property()
        {
            Party_details = new List<Party_Details>();
            Property_details = new List<Property_Details>();
        }
    }

    public class Deed_Details
    {
        public string District_code { get; set; }
        public string Ro_code { get; set; }
        public string Query_year { get; set; }
        public string Query_No { get; set; }
        public string Serial_year { get; set; }
        public string Serial_No { get; set; }
        public string Deed_year { get; set; }
        public string Deed_no { get; set; }
        public string Book { get; set; }
        public string Tran_maj_code { get; set; }
        public string Tran_min_code { get; set; }
        public string Dt_of_Completion { get; set; }
        public string Dt_of_Registration { get; set; }
    }

    public class Party_Details
    {
        public string Party_serialno { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Address { get; set; }
        public string Address_Country_Name { get; set; }
        public string Address_State_Name { get; set; }
        public string Address_District_Name { get; set; }
        public string Address_PS_Name { get; set; }
        public string Father_Mother { get; set; }
        public string Relative_Name { get; set; }
        public string Relative_FName { get; set; }
        public string Relative_LName { get; set; }
        public string CityVillage_Name { get; set; }
        public string Pincode { get; set; }
        public string Status_Desc { get; set; }
    }

    public class Property_Details
    {
        public string Property_Serialno { get; set; }
        public string Plot_no { get; set; }
        public string Bata_plot_no { get; set; }
        public string District_name { get; set; }
        public string Property_ro_name { get; set; }
        public string Ps_name { get; set; }
        public string Mouza { get; set; }
        public string Gp_muni_Corp_name { get; set; }
        public string Khatian_no { get; set; }
        public string Bata_Khatian_No { get; set; }
        public string Nature_of_Land_name { get; set; }
        public string Property_type { get; set; }
        public string Property_Area { get; set; }
    }

}