﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MunicipalityConfiguration
    {
        public int MunicipalityID { get; set; }
        public PaymentConfig PaymentConfiguration { get; set; }
        public BasicInfoConfig BasicInfoConfiguration { get; set; }
        public DemandBillConfig DemandBillConfiguration { get; set; }
        public CollectorAdjustmentConfig CollectorAdjustmentConfiguration { get; set; }

        public class PaymentConfig
        {
            /// <summary>
            /// true: Advance Auto Adjustment enabled
            /// </summary>
            public bool IsAdvanceAutoAdjustableEnabled { get; set; }
        }
        public class CollectorAdjustmentConfig
        {
            /// <summary>
            /// true: Collector can Adjust payable amount at runtime
            /// </summary>
            public bool IsActiveForMunicipality { get; set; }
            /// <summary>
            /// Collector can not exceed Adjustable amount from this amount
            /// </summary>
            public decimal MaxAdjustableAmount { get; set; }
            public bool IsEnabledCollectorAdjustmentOnUser { get; set; }
            public string CollctrAdjustmentType { get;  set; }
            public string CollctrAdjustmentTypeDescription { get; internal set; }
        }
        public class BasicInfoConfig
        {
            public string MunicipalityName { get; set; }
            public string Address { get; set; }
            public int? DistrictID { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public string TelePhone { get; set; }
            public string WebSite { get; set; }
            public string  Fax { get; set; }
        }
        public class DemandBillConfig
        {
            public string DemandBillRebateIntrstText { get; set; }
        }
    }
}