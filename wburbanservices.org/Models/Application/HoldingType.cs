﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class HoldingType
    {
        public int HoldingTypeID { get; set; }
        public string HoldingTypeDesc { get; set; }
    }
}