﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PropertyTaxOutstading
    {
        public int MunicipalityID { get; set; }
        public int WardID { get; set; }
        public int LocationID { get; set; }
        public long AssesseeID { get; set; }
        public string DemandType { get; set; }
        public string FinYear { get; set; }
        public long DemandTypeID { get; set; }
        public int QtrNo { get; set; }
        public DateTime BillDueDate { get; set; }
        public decimal PropertyTaxValue { get; set; }
        public decimal SurchargeValue { get; set; }
        public decimal PaidPropertyTaxAmt { get; set; }
        public decimal PaidSurchargeAmt { get; set; }
        public decimal OSPropertyTaxAmt { get; set; }
        public decimal OSSurchargeAmt { get; set; }
        public decimal PaidAmount { get; set; }
        public int Rank { get; set; }
        public TaxPaymentCheckParameter taxPaymentCheckParameter { get; set; }
    }
}