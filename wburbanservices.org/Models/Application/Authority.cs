﻿namespace Valuation_Board.Models.Application
{
    public class Authority
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool      IsActive { get; set; }
    }
}
