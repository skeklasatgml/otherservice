﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Valuation_Board.Models.Application.DprModel;

namespace Valuation_Board.Models.Application
{
    public class StageOfProgress
    {
        public class StageGroup : IdVal<int, string>
        {
            public List<WorkMileStone> WorkMileStones { get; set; }
            public StageGroup()
            {
                WorkMileStones = new List<WorkMileStone>();
            }
        }
        public class WorkMileStone
        {
            public int? Id { get; set; }
            public int GroupId { get; set; }
            public string Dscr { get; set; }
            public int Order { get; set; }
            public decimal PerComplited { get; set; }
        }
        public class SchemeCategoryMappedMileStone : SchemeCategory
        {
            public bool IsMapped { get; set; }
        }

        public class ScemeCatMilestoneMapping
        {
            public List<IdVal<int, string>> Groups { get; set; }
            public List<SchemeType> SchemeTypes { get; set; }
            public ScemeCatMilestoneMapping()
            {
                Groups = new List<IdVal<int, string>>();
                SchemeTypes = new List<SchemeType>();
            }
        }
    }


}