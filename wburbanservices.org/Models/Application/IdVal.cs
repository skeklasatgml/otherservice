﻿namespace Valuation_Board.Models.Application
{
    public class IdVal<T, T1>
    {
        public T Id { get; set; }
        public T1 Value { get; set; }
    }
}