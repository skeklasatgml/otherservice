﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PG_ModelOtherServices
    {
        
        /// <summary>
        /// transaction ID,assesseno+curDate("ddMMyyHHmmss")
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// transaction Amount decimal
        /// </summary>
        public string TxnAmount { get; set; }
        /// <summary>
        /// AdditionalInfo1
        /// </summary>
        public string ServMstId { get; set; }
        /// <summary>
        /// AdditionalInfo2
        /// </summary>
        public string SubModuleID { get; set; }
        /// <summary>
        /// AdditionalInfo3
        /// </summary>
        public string MunicipalityID { get; set; }
        /// <summary>
        /// AdditionalInfo4
        /// </summary>
        public string NameOfProject { get; set; }
        /// <summary>
        /// AdditionalInfo5
        /// </summary>
        public string SubModuleName { get; set; }
        /// <summary>
        /// AdditionalInfo6,finyear+paymentid
        /// </summary>
        public string PaymentID { get; set; }
        /// <summary>
        /// AdditionalInfo7
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// Bildesk Url
        /// </summary>
        public string Reffaral_URL { get; set; }
        /// <summary>
        /// Return_URL
        /// </summary>
        public string Return_URL { get; set; }
        public string AssesseeNo { get; set; }
        public string PG_Code { get; set; }
    }
}