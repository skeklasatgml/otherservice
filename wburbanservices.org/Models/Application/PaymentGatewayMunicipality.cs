﻿namespace Valuation_Board.Models.Application
{
    public class PaymentGatewayMunicipality: PaymentGateway
    {
        public int MapID { get; set; }
        public bool IsActive { get; set; }
        public Municipality Municipality { get; set; }
        public int MunicipalityID  { get; set; }
    }
}