﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MobileTowerMD
    {
        public class Surface
        {
            public int? SurfaceID { get; set; }
            public string SurfaceName { get; set; }
        }
        public class RoadType
        {
            public int? RoadTypeID { get; set; }
            public string RoadTypeName { get; set; }
        }
        public class Municipality
        {
            public int? MunicipalityID { get; set; }
            public string MunicipalityName { get; set; }
        }
        public class Purposes
        {
            public int? TypeID { get; set; }
            public string Purpose { get; set; }
        }
        public class Specification
        {
            public long? ID { get; set; }
            public string Description { get; set; }
            public Boolean? SpecificationRWidth { get; set; }
            public Boolean? SpecificationRDepth { get; set; }
            public Boolean? SpecificationPNo { get; set; }
            public Boolean? SpecificationPLength { get; set; }
            public Boolean? SpecificationPWidth { get; set; }
            public Boolean? SpecificationPDepth { get; set; }
            public Boolean? SpecificationTDiameter { get; set; }

        }
        public class UploadDetail
        {
            public int? DocTypeID { get; set; }
            public string DocName { get; set; }
            public string ExtensionType { get; set; }
            public int? maxQuantity { get; set; }
            public int? isMandatory { get; set; }
            public decimal? MaxSize { get; set; }
        }
        public class Ward
        {
            public int? WardID { get; set; }
            public string WardName { get; set; }
        }
        public class Location
        {
            public int? LocationID { get; set; }
            public string LocationName { get; set; }
        }
        public class Holding
        {
            public long? AssesseeID { get; set; }
            public string HoldingNo { get; set; }
            public string holding { get; set; }
        }
        public class ProjectDetail
        {
            public string DetailID { get; set; }
            public string Status { get; set; }
            public string TypSurface { get; set; }
            public string TypRoad { get; set; }
            public string LongStart { get; set; }
            public string LongEnd { get; set; }
            public string LatStart { get; set; }
            public string LatEnd { get; set; }
            public string Ward { get; set; }
            public string Location { get; set; }
            public string LandMark { get; set; }
            public string BoroughNo { get; set; }
            public string HoldingNo { get; set; }
            public string AssesseeNo { get; set; }
            public string PlotArea { get; set; }
            public string StartofWork { get; set; }
            public string EndofWork { get; set; }
            public string Escavation { get; set; }
            public string Specification { get; set; }
            public string RWidth { get; set; }
            public string RDepth { get; set; }
            public string PNo { get; set; }
            public string PLength { get; set; }
            public string PWidth { get; set; }
            public string PDepth { get; set; }
            public string TDiameter { get; set; }

            public string TowerArea { get; set; }
            public string TowerHeight { get; set; }
            public string TowerVolumn_CM { get; set; }
            public string SignageTowerNo { get; set; }
        }
    }
}