﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
   public class CodeValue<T1,T2>
    {
        public T1 Code { get; set; }
        public T2 Value { get; set; }
    }
}