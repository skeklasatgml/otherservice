﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Utils;

namespace Valuation_Board.Models.Application
{
    public class OtherAdjustmentMaster : IOtherAdjustment
    {
        public long MstAdjID { get; set; }
        public int MunicipalityID { get; set; }
        public string PayHeadType { get; set; }
        public int MstPayHeadID { get; set; }
        public string PayHeadBehave { get; set; }
        public string PayHeadDesc { get; set; }
        /// <summary>
        /// A=All Assessee, I=Indivisual Assessee
        /// </summary>
        public string EffectType { get; set; }
        /// <summary>
        /// valid from
        /// </summary>
        public DateTime EffectiveFromDt { get; set; }
        /// <summary>
        /// valid to
        /// </summary>
        public DateTime EffectiveToDt { get; set; }
        /// <summary>
        /// A=absolute, P=Percent
        /// </summary>
        public string RateValueType { get; set; }
        public decimal RateValue { get; set; }
        /// <summary>
        /// A = For All Arrear and Demand.
        /// R = For Specified Range eg. 2015-2016(2nd qtr) to 2017-2018(4th qtr).
        /// C = Only for Current Demand.
        /// </summary>
        public string ApplicableTaxRangeType { get; set; }
        public bool IsPropertyTax { get; set; }
        public bool IsSurcharge { get; set; }
        public bool IsPropertyTaxInt { get; set; }
        public bool IsSurchargeInt { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public long? MstDocumentsID { get; set; }

        public string FromFinYear { get; set; }
        public int? FromFinYearQtr { get; set; }
        public string ToFinYear { get; set; }
        public int? ToFinYearQtr { get; set; }
        public string Remarks { get; set; }
        public decimal GetAdjustableAmount(List<PropertyTaxPaymentDetail> propertyTaxPaymentDetails,long assesseeID)
        {
            decimal _adjustableAmount = 0;
            
            Func<IOtherAdjustment, List<PropertyTaxPaymentDetail>, decimal> __getCalculableAmount = (_IOtherAdjustment, _propertyTaxPaymentDetails) =>
            {
                decimal _calculableAmount = 0;
                //_filteredPropertyTaxPaymentDetails is filtered collection by ApplicableTaxRangeType
                Func<List<PropertyTaxPaymentDetail>, decimal> __getCalculableAmountByCriteria = (_filteredPropertyTaxPaymentDetails) =>
                {
                    //limit scope calculable amount where amount may be PaidTaxAmt||PaidPropTaxInterest||PaidSurchargeAmt||PaidSurchrgInterest or both all
                    decimal returnedVal = 0;
                    //make water fall summation by checking follwing
                    if(_IOtherAdjustment.IsPropertyTax)
                    {
                        returnedVal += _filteredPropertyTaxPaymentDetails.Sum(t => (t.PaidTaxAmt??0));
                    }
                    if(_IOtherAdjustment.IsPropertyTaxInt)
                    {
                        returnedVal += _filteredPropertyTaxPaymentDetails.Sum(t => (t.PaidPropTaxInterest));
                    }
                    if (_IOtherAdjustment.IsSurcharge)
                    {
                        returnedVal += _filteredPropertyTaxPaymentDetails.Sum(t => (t.PaidSurchargeAmt??0));
                    }
                    if(_IOtherAdjustment.IsSurchargeInt)
                    {
                        returnedVal += _filteredPropertyTaxPaymentDetails.Sum(t => (t.PaidSurchrgInterest));
                    }
                    return returnedVal;
                };
                //A=All range, R=Specific range, C=Current Demand
                if (string.Equals(_IOtherAdjustment.ApplicableTaxRangeType, "R", StringComparison.OrdinalIgnoreCase))
                {

                    var fromFinYearQtr = new FinYearQtr(_IOtherAdjustment.FromFinYear, _IOtherAdjustment.FromFinYearQtr??0);
                    var toFinYearQtr = new FinYearQtr(_IOtherAdjustment.ToFinYear, _IOtherAdjustment.ToFinYearQtr??0);
                    //if (fromFinYearQtr.StartYear == 0 || fromFinYearQtr.EndYear == 0 || fromFinYearQtr.Qtr == 0 || fromFinYearQtr.StartYear == 0 || toFinYearQtr.StartYear == 0 || toFinYearQtr.EndYear == 0 || toFinYearQtr.Qtr == 0)
                    //{
                        //_calculableAmount = 0;
                    //}
                    //else
                    //{
                        var filteredPropertyTaxPaymentDetails = _propertyTaxPaymentDetails.Where(t =>
                        {
                            var tergateFinYearQtr = new FinYearQtr(t.FinYear, t.QtrNo ?? 0);
                            if (fromFinYearQtr <= tergateFinYearQtr && toFinYearQtr >= tergateFinYearQtr)
                            {
                                return true;
                            }
                            return false;
                        }).ToList();
                        _calculableAmount = __getCalculableAmountByCriteria(filteredPropertyTaxPaymentDetails);
                    //}
                    //get amount in range
                }
                else if (string.Equals(_IOtherAdjustment.ApplicableTaxRangeType, "C", StringComparison.OrdinalIgnoreCase))
                {
                    //filtered by current finyear
                    var filteredPropertyTaxPaymentDetails = _propertyTaxPaymentDetails.Where(t=>t.FinYear==CommonUtils.GetFinYear(DateTime.Now)).ToList();
                    _calculableAmount = __getCalculableAmountByCriteria(filteredPropertyTaxPaymentDetails);
                    //only current demand
                }
                else if (string.Equals(_IOtherAdjustment.ApplicableTaxRangeType, "A", StringComparison.OrdinalIgnoreCase))
                {
                    
                    //calculable all finyear. pass all finyear and qtr availble
                    _calculableAmount = __getCalculableAmountByCriteria(_propertyTaxPaymentDetails);
                }
                
                return _calculableAmount;
            };
            decimal _calculableRawAmount = 0;
            if(string.Equals(this.PayHeadType, "SR", StringComparison.OrdinalIgnoreCase)){
                //no configuration will work for special rebate
                var lst = propertyTaxPaymentDetails.Where(t => t.FinYear == CommonUtils.GetFinYear(DateTime.Now) && t.taxPaymentCheckParameter.IsChecked == true).ToList();
                //verify all qtr selected
                if (lst.Count==4)
                {
                    _calculableRawAmount = lst.Sum(t=>(t.PaidTaxAmt??0)+(t.PaidSurchargeAmt??0));
                }
                //qtr 4rth is selected
                else if(lst.FirstOrDefault(t=>t.QtrNo==4)!=null)
                {
                    //check if any payment made on current demand on today
                    PropertyTaxCollectionBLL bll = new PropertyTaxCollectionBLL();
                    if (bll.IsPaymentExist(DateTime.Now.Date,assesseeID) ==true)
                    {
                        _calculableRawAmount = lst.Sum(t => (t.PaidTaxAmt ?? 0) + (t.PaidSurchargeAmt ?? 0));
                    }
                    else
                    {
                        _calculableRawAmount = 0;
                    }
                }
                else
                {
                    _calculableRawAmount = 0;
                }
            }
            else
            {
                _calculableRawAmount = __getCalculableAmount(this, propertyTaxPaymentDetails);
            }
            ////calculate value 
            if (this.RateValueType == "P")
            {
                _adjustableAmount = _calculableRawAmount * this.RateValue / 100;
            }
            else if (this.RateValueType == "A")
            {
                _adjustableAmount = RateValue;
            }

            //for deduction type make amount nagative
            //for addition behave make amount prositive
            if (this.PayHeadBehave == "D")
            {
                return -1 * (_adjustableAmount);
                //make in nagative form

            }
            else if (this.PayHeadBehave == "A")
            {
                return (_adjustableAmount);
                //leave it prositive form
            }
            return _adjustableAmount;
        }
    }
}