﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class InspectorRegModule
    {
        public string InspectorName { get; set; }
        public string InspectorMobile { get; set; }
        public string InspectorEmail { get; set; }
        public string InspectorUserName { get; set; }
        public string InspectorPassword { get; set; }
    }
}