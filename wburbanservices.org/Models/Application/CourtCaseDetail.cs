﻿namespace Valuation_Board.Models.Application
{
    public class CourtCaseDetailBase
    {
        public string ERROR { get; set; }
    }
    public class CourtCaseDetail: CourtCaseDetailBase
    {
        public string case_no { get; set; }
        public string party_name { get; set; }
        public string case_date { get; set; }
        public string status { get; set; }
        public string case_type { get; set; }
    }

}