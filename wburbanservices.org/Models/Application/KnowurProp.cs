﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class KnowurProp<T>
    {
        public T RemoteData { get; set; }
        public List<OutStanding> OutStandings { get; set; }
        public KnowurProp()
        {
            OutStandings = new List<OutStanding>();
        }
    }
    public class PlotInfo
    {
        public string PLOT_CLASSIFICATION { get; set; }
        public string PLOT_SHARE_AREA { get; set; }
        public string ERROR { get; set; }
        public string PLOT_NO { get; set; }
        public List<OwnerDetail> OWNER_DETAIL { get; set; }
        public PlotInfo()
        {
            OWNER_DETAIL = new List<OwnerDetail>();
        }
    }

    public class OwnerDetail
    {
        public string GUARDIAN_NAME { get; set; }
        public string OWNER_NAME { get; set; }
        public string KHATIAN_SHARE_AREA { get; set; }
        public string POSESSOR_NAME { get; set; }
        public List<PossessorDetail> POSESSOR_DETAIL { get; set; }
        public string KHATIAN_NO { get; set; }
        public string KHATIAN_SHARE { get; set; }
        public OwnerDetail()
        {
            POSESSOR_DETAIL = new List<PossessorDetail>();
        }
    }

    public class PossessorDetail
    {
        public string POC_ADDRESS { get; set; }
        public string POC_NAME { get; set; }
        public string POC_REMARKS { get; set; }
        public string POC_GURDIAN { get; set; }
    }

    public class KhatianDetail
    {
        public string TotalArea { get; set; }
        public string OwnerName { get; set; }
        public string OwnerType { get; set; }
        public string Address { get; set; }
        public string GurdianName { get; set; }
        public string Error { get; set; }
        public string NoOfPlots { get; set; }
        public object[] TENANT_DETAIL { get; set; }
        public MutationData MutationData { get; set; }
    }

        public class MutationData
        {
            public string TotalArea { get; set; }
            public string OwnerName { get; set; }
            public string OwnerType { get; set; }
            public string Address { get; set; }
            public string GurdianName { get; set; }
            public string KhatianCreationDate { get; set; }
            public string NoOfPlots { get; set; }
            public string ERROR { get; set; }
            public object[] TENANT_DETAIL { get; set; }
        }

    public class Mouza<T1, T2> : CodeValue<T1, T2>
    {
        public int Idn { get; set; }
    }
    public class OutStanding
    {
        public string municipalityName { get; set; }
        public string Ward { get; set; }
        public string HoldingNo { get; set; }
        public string AsseName { get; set; }
        public decimal TotalOutStanding { get; set; }
        public string khatian { get; set; }
        public string plot { get; set; }
        public string dist_code { get; set; }
        public string ro_code { get; set; }
        public string deed_no { get; set; }
        public string deed_year { get; set; }
    }

}