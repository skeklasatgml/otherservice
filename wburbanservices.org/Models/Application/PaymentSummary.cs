﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PaymentSummary
    {
        public string  RecieptID { get; set; }
        public long PaymentID { get; set; }
        public decimal NetAmount { get; set; }
        public DateTime PaymentReceiveDate { get; set; }
        public long AssesseeID { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingNo { get; set; }
    }
}