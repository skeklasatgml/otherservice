﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssmtBeltSheetInsUp
    {
        public int MunicipalityID { get; set; }
        public string YearId { get; set; }
        public long BeltSheetID { get; set; }
        public int ScoreBeltTypeId { get; set; }
        public decimal FromRange { get; set; }
        public decimal ToRange { get; set; }
        public decimal ScoreValue { get; set; }
        public string BeltValue { get; set; }
        public long CurrentUserId { get; set; }

    }
}