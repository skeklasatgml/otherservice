﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class OtpStatusModule
    {
        public int Status { get; set; }
        public int isExist { get; set; }
        public int isVerified { get; set; }
        public string Msg { get; set; }
        public string Mobile { get; set; }
    }

    public class otpstatus
    {
        public int Status { get; set; }
        public string Msg { get; set; }
        public long UserID { get; set; }
        public string Mobile { get; set; }
    }

}
