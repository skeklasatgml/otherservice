﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MNPaymentModesCounterAssociation
    {
        public long RowId { get; set; }
        public int CounterId { get; set; }
        public int MunicipalityId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime Todate { get; set; }
        public string PaymentModeCode{ get; set; }
        public MunicipalityPaymentCounter MunicipalityPaymentCounter { get; set; }
        public string PaymentMode { get; set; }
        public Municipality Municipality { get; set; }
    }
}