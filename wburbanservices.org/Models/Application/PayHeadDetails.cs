﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PayHeadDetails
    {
        public string EffectiveDateForm { get; set; }
        public string EffectiveDateTo { get; set; }
        public int? GracePeriodInMonth { get; set; }
        public decimal? PayRate { get; set; }       
        public byte? QtrNo { get; set; }
        public string FinYear { get; set; }
        public int? PayHeadID { get; set; }
        public bool IsDeleted { get; set; }
        public int PeriodOffset { get; set; }
    }
}