﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.Models.Account;

namespace Valuation_Board.Models.Application
{
    public class Assessee
    {
        public long AssesseeID { get; set; }
        public bool IsDisputed { get; set; }
        public string DisputeReason { get; set; }
        public long? ParentAssesseeID { get; set; }
        public string AssesseeNo { get; set; }
        public int? MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public int? WardID { get; set; }
        public string WardName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public int? HoldingTypeID { get; set; }
        public string HoldingTypeDesc { get; set; }
        public string HoldingNo { get; set; }
        public string AssesseeName { get; set; }
        public string AssesseeAddress { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public long? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string PrimaryPhNo { get; set; }

        public string PrimaryEmail { get; set; }

        public string Block { get; set; }
        public string Mouja { get; set; }
        public string Plot { get; set; }
        public string Khatian { get; set; }
        public string JLNo { get; set; }
        public string DeedNo { get; set; }
        public int? MoujaID{ get; set; }
        public string DeedRegistrationYear { get; set; }
        public int? DeedRegistrationOffice { get; set; }

        public bool IsPrimaryEmailVerified { get; set; }

        public bool IsPrimaryPhoneNoVerified { get; set; }
        public long? OldAssesseeNo { get; set; }
        public Municipality Municipality { get; set; }

        public Ward Ward { get; set; }
        public Location Location { get; set; }
        public CitizenUser Convert()
        {
            return new CitizenUser()
            {
                AssesseeID = this.AssesseeID,
                ParentAssesseeID = this.ParentAssesseeID,
                AssesseeNo = this.AssesseeNo,
                MunicipalityID = this.MunicipalityID,
                MunicipalityName = this.MunicipalityName,
                WardID = this.WardID,
                WardName = this.WardName,
                LocationID = this.LocationID,
                LocationName = this.LocationName,
                HoldingTypeID = this.HoldingTypeID,
                HoldingTypeDesc = this.HoldingTypeDesc,
                HoldingNo = this.HoldingNo,
                AssesseeName = this.AssesseeName,
                AssesseeAddress = this.AssesseeAddress,
                PrimaryEmail=this.PrimaryEmail,
                PrimaryPhNo=this.PrimaryPhNo,
                Block=this.Block,
                Mouja = this.Mouja,
                Plot = this.Plot,
                Khatian = this.Khatian,
                JLNo = this.JLNo,
                DeedNo=this.DeedNo

            };
        }
    }
}