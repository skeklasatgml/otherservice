﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Service_Model
    {
        public long ServMstId { get; set; }
        public int SubModuleID { get; set; }
        public string ApplicationNo { get; set; }
        public string ApplicationDate { get; set; }
        public int PurposeOfService { get; set; }
        public string NameOfProject { get; set; }
        public string NameOfOrganisation { get; set; }
        public long UserID { get; set; }
        public string ServiceCurrStatus { get; set; }
        public decimal TotalPaidAmount { get; set; }
        public string SubmittedOn { get; set; }
        public int AcceptedBy { get; set; }
        public string AcceptedOn { get; set; }
        public int ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public int RejectedBy { get; set; }
        public string RejectedOn { get; set; }
        public int MunicipalityDevAuthoId { get; set; }
        public bool isDelete { get; set; }
        public string TransactionID { get; set; }
    }
}