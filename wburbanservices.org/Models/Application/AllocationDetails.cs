﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AllocationDetails
    {
        public long ServMstId { get; set; }
        public string ApplicationNo { get; set; }
        public string ApplicationDate { get; set; }
        public string NameOfProject { get; set; }
        public string DesignationName { get; set; }
        public string ResponseStatus { get; set; }
        public string Description { get; set; }
        public string allotedOn { get; set; }
        public string updatedOn { get; set; }
        public string Remarks { get; set; }
        public string ResponseStatusColor { get; set; }
    }
}