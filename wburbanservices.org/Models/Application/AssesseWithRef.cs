﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssesseWithRef:Assessee
    {
        public string ReferenceNo { get; set; }
        public DateTime ReferenceDate { get; set; }

    }
}