﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Menu
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int? ParrentMenuID { get; set; }
        public int? ActionMethodID { get; set; }
        public int Rank { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
        public List<Menu> List { get; set; }
    }
}