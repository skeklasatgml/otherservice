﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssesseePayheadGroup
    {
        public long AssesseeID { get; set; }
        public string AssesseeName { get; set; }
        public string HoldingNo { get; set; }
        public bool IsMapped { get; set; }
        public int LocationID { get; set; }
        public int WardID { get; set; }
        public long? GroupID { get; set; }
        public string GroupName { get; set; }
    }
}