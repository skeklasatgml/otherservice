﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    [Serializable]
    public class FinYearQtr
    {
        /// <summary>
        /// {year}-{year}
        /// </summary>
        /// <param name="finYear">2017-2018</param>
        public FinYearQtr()
        { }
        public FinYearQtr(string finYear)
        {
            try
            {
                StartYear = Convert.ToInt32(finYear.Split('-')[0]);
                EndYear = Convert.ToInt32(finYear.Split('-')[1]);
            }catch(FormatException)
            {
                throw new ArgumentException(string.Format("FinYear [{0}] is in invalid format. Valid format [{int:year1}-{int:year2}]",finYear));
            }
        }
        public FinYearQtr(string finYear, int qtr)
        {
            try
            {
                StartYear = Convert.ToInt32(finYear.Split('-')[0]);
                EndYear = Convert.ToInt32(finYear.Split('-')[1]);
                _qtr = qtr;
            }
            catch (FormatException)
            {
                throw new ArgumentException(string.Format("FinYear [{0}] is in invalid format. Valid format [{int:year1}-{int:year2}]", finYear));
            }
            
        }
        int _qtr = 1;
        int _startYear = 0;
        int _endYear = 0;
        public int StartYear {
            get
            {
                return _startYear;
            }
            set
            {
                _startYear = value;
            }
        }
        public int EndYear {
            get
            {
                return _endYear;
            }
            set
            {
                _endYear = value;
            }
        }
        public int Qtr
        {
            get
            {
                return _qtr;
            }
            set
            {
                _qtr = value;
            }
        }

        public string GetFinYear()
        {
            return string.Format("{0}-{1}",this.StartYear,this.EndYear);
        }
        public static bool operator ==(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            return finYearQtr.StartYear == finYearQtr2.StartYear && finYearQtr.Qtr == finYearQtr2.Qtr;
        }
        public static bool operator !=(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            return !(finYearQtr.StartYear == finYearQtr2.StartYear && finYearQtr.Qtr == finYearQtr2.Qtr);
        }
        public static bool operator >(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            bool res = false;
            if (finYearQtr == finYearQtr2)
            {
                res = false;
            }
            else
            {
                if (finYearQtr.StartYear > finYearQtr2.StartYear)
                {
                    res = true;
                }
                else if (finYearQtr.StartYear == finYearQtr2.StartYear && finYearQtr.Qtr > finYearQtr2.Qtr)
                {
                    res = true;
                }
                else
                {
                    res = false;
                }


            }
            return res;
        }
        public static bool operator <(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            if (finYearQtr == finYearQtr2)
            {
                return false;
            }
            return !(finYearQtr > finYearQtr2);
        }
        public static bool operator >=(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            if (finYearQtr == finYearQtr2)
            {
                return true;
            }
            return (finYearQtr > finYearQtr2);
        }
        public static bool operator <=(FinYearQtr finYearQtr, FinYearQtr finYearQtr2)
        {
            if (finYearQtr == finYearQtr2)
            {
                return true;
            }
            return !(finYearQtr > finYearQtr2);
        }
        public override string ToString()
        {
            return string.Format("{0}-{1},{2}", this.StartYear, this.EndYear, this.Qtr);
        }
        public void AddYear(int year)
        {
            this.StartYear = this.StartYear + year;
            this.EndYear = this.EndYear + year;
        }
        public void AddQtr(int qtr)
        {
            if(qtr>=1)
            {
                    int dividend = qtr + this.Qtr;
                    int divisor = 4;
                    int reminder= dividend % divisor;
                    int quotient = dividend / divisor;

                    if (reminder == 0)
                    {
                    reminder = 4;
                    quotient -= 1;
                    }
                    this.Qtr = reminder;
                    this.StartYear += quotient;
                    this.EndYear += quotient;
            }
            else if(qtr<0)
            {

                int dividend = qtr*-1;
                int divisor = 4;
                int reminder = dividend % divisor;
                int quotient = dividend / divisor;

                
                if(this.Qtr-reminder==0)
                {
                    quotient = quotient + 1;
                    this.Qtr = 4;
                }else
                    {
                    this.Qtr = this.Qtr - reminder;
                }
                this.StartYear = this.StartYear - quotient;
                this.EndYear = this.EndYear - quotient;
            }
            else
            {
                throw new ArgumentException("Qtr value can not be 0");
            }
        }
    }
}