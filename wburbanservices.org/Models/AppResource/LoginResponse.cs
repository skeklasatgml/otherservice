﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.AppResource
{
    public class LoginResponse
    {
        public string RedirectUrl { get; set; }
        public bool IsLoggedIn { get; set; }
    }
}