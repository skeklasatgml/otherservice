﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.AppResource
{
    public class ErrorLog
    {
        public string Msg { get; set; }
        public string StackStrace { get; set; }
        public string LoggedOn { get; set; }
        public string SessionID { get; set; }
        public int ManagedThreadId { get; set; }
    }
}