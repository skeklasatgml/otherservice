﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.AppResource
{
    public class ApplicationInfo
    {
        public string AssemblyVersion { get; set; }
    }
}