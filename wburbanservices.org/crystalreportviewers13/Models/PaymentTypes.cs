﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models
{
    public enum PaymentType
    {
        Offline = 1,
        Online = 2,
        Offline_Online = 3
    }
}