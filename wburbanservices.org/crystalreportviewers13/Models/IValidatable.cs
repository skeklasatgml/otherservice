﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models
{
    public interface IValidatable
    {
        void Validate();
    }
}