﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.AppResource
{
    public class AppUrl
    {
        /// <summary>
        /// UrlID is basically action ID
        /// </summary>
        public int UrlID { get; set; }
        public string ControllerPackage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsSharedUrl { get; set; }
        public bool IsAnonymousUrl { get; set; }


    }
}