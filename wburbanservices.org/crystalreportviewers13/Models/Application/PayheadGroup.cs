﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PayheadGroup
    {
        public long GroupID { get; set; }
        public string  GroupName { get; set; }
        public int MunicipalityId { get; set; }
    }
}