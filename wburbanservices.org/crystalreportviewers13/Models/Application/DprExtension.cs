﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class DprExtension
    {
        public class VendorDropDwn
        {
            public int VId { get; set; }
            public string VName { get; set; }
        }
        public class WorkTypDropDwn
        {
            public int WTypId { get; set; }
            public string WTypName { get; set; }
        }
        public class ProjectNoDropDwn
        {
            public string ProjectNoVal { get; set; }
            public string ProjectNoTxt { get; set; }
        }
        public class SubProjectNoDropDwn
        {
            public long SubProjNoVal { get; set; }
            public string SubProjNoTxt { get; set; }
        }
        public class WrkOrdrNoDropDwn
        {
            public string WrkOrdrNoVal { get; set; }
            public string WrkOrdrNoTxt { get; set; }
        }
        public class WorkStatus
        {
            public int VId { get; set; }
            public string VendorName { get; set; }
            public int WTypId { get; set; }
            public string Remarks { get; set; }
            public string WorkOrderNo { get; set; }
            public DateTime? WrkStrtDt { get; set; }
            public DateTime? PaidOn { get; set; }
            public decimal CummiAmt { get; set; }
            public string TenderNo { get; set; }
            public string WrkOrdrNo { get; set; }
            public DateTime WrkOrdrDt { get; set; }
            public int? WMilestone { get; set; }
            public DateTime AllocatedWrkStrtDt { get; set; }
            public DateTime WrkCmpltnDt { get; set; }
            public DateTime? Pic1AsOn { get; set; }
            public DateTime? Pic2AsOn { get; set; }
            public DateTime? Pic3AsOn { get; set; }
            public string WMilestoneDesc { get; set; }
            public decimal? Percentage { get; set; }

        }
        public class VendorData
        {
            public string VendorName { get; set; }
            public string VendorReg { get; set; }
            public string VendorAdd { get; set; }
            public string VendorPAN { get; set; }
            public string VendorGST { get; set; }
            public string PrimaryCntctNo { get; set; }
            public string PrimaryCntctNm { get; set; }
            public string PrimaryEmail { get; set; }
            public string OtherCntctNo { get; set; }
        }
        public class WrkMilestone
        {
            public decimal percentage { get; set; }
            public int MileStoneId { get; set; }
            public string WmDesc { get; set; }
        }
    }
}