﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Ward
    {
        public int? WardID { get; set; }
        public int? MunicipalityID { get; set; }
        public string WardName { get; set; }
        public int? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Municipality Municipality { get; set; }
    }
}