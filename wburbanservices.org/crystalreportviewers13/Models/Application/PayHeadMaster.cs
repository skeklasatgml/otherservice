﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PayHeadMaster
    {
        public int? PayHeadID { get; set; }
        public string PayHeadDesc { get; set; }
        public string PayHeadType { get; set; }
        public decimal? MinLimit { get; set; }
        public decimal? MaxLimit { get; set; }
        public string ApplicableType { get; set; }
        public string ApprovalLevel { get; set; }
        public int? MunicipalityID { get; set; }
        public string PayHeadBehave { get; set; }
        public int? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public PayHeadDetails PayHeadDtl { get; set; }
    }
}