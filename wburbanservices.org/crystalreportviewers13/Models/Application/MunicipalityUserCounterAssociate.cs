﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MunicipalityUserCounterAssociate
    {
        public long RowID { get; set; }
        public int MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public int CounterID { get; set; }
        public string CounterCode { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}