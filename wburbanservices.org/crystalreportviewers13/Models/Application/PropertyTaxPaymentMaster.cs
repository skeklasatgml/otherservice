﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.ViewModels;

namespace Valuation_Board.Models.Application
{
    public class PropertyTaxPaymentMaster
    {
        public long? PaymentID { get; set; }
        public string PaymentType { get; set; }
        public long? AssesseeID { get; set; }
        public int? MunicipalityID { get; set; }
        public long? BillID { get; set; }
        public string FinYear { get; set; }
        public DateTime? PaymentCollectionDate { get; set; }
        public DateTime? PaymentReceiveDate { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? RoundOffAdjust { get; set; }
        public string PayMode { get; set; }
        public string InstrumentNo { get; set; }
        public DateTime? InstrumentDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public int? CollectorID { get; set; }
        public int? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public int? CounterID { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public List<PropertyTaxPaymentDetail> PropertyTaxPaymentDetails { get; set; }
        public Municipality Municipality { get; set; }
        public Assessee Assessee { get; set; }
        public string  TransactionID { get; set; }
        public bool IsPaymentSuccessfull { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal AdjustedAmount { get; set; }
        public DateTime? NoticeServedDate { get; set; }
        public decimal TotalCalculatedAmnt { get; set; }
        /// <summary>
        /// the qtr from when notice served date effected
        /// </summary>
        public int? EffectiveQtrFrom { get; set; }
        public decimal CollectorAdjustmentAmount { get; set; }
        /// <summary>
        /// excess amount after net payable amount, i.e Advance amount
        /// </summary>
        public decimal AdvanceAmount { get; set; }
        public string BillReciptNo { get; set; }
        public List<OtherAdjustmentSummary_VM> AdjustmentSummaries { get; set; }
        public PropertyTaxPaymentMaster()
        {
            PropertyTaxPaymentDetails = new List<PropertyTaxPaymentDetail>();
        }
    }
}