﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PayHead
    {
        public int? PayHeadID { get; set; }
        public DateTime EffectiveDateForm { get; set; }
        public DateTime EffectiveDateTo { get; set; }
        public int? QtrNo { get; set; }
        public string FinYear { get; set; }
        public string PayHeadDesc { get; set; }
        public string PayHeadType { get; set; }
        public decimal? PayRate { get; set; }
        public int GracePeriodInMonth { get; set; }
        public decimal? MinLimit { get; set; }
        public decimal? MaxLimit { get; set; }
        public string ApplicableType { get; set; }
        public string ApprovalLevel { get; set; }
        public int? MunicipalityID { get; set; }
        public string PayHeadBehave { get; set; }
        public int? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string InterestPeriodMethod { get; set; }
        public string InterestPeriodRndOffDirection { get; set; }
    }
}