﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using static Valuation_Board.ViewModels.Assessment_VM.AssessmentForm;

namespace Valuation_Board.Models.Application
{
    public class ScheduleThree
    {
        public class AssmtMaster
        {
            public long AssmtId { get; set; }
            public string AssesseName { get; set; }
            public string HoldingNo { get; set; }
            public DateTime? SubmittedOn { get; set; }
            public int? WardID { get; set; }
            public string Ward { get; set; }
            public int? MunicipalityId { get; set; }
            public string Municipalityname { get; set; }
        }
        public class Form {
            public long? AssmtId { get; set; }
            public int MunicipalityId { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string  Phone { get; set; }
            public DateTime? ApplicationDate { get; set; }
            public string Address { get; set; }
            public LocationTab LocationTab { get; set; }
            public UsageTab UsageTab { get; set; }
            public OtherTab OtherTab { get; set; }
            public ImagesTab ImageTab { get; set; }
            public MasterData MasterData { get; set; }
            public List<OtherInfo> DelImgRequests { get; set; }
            public UserVerification UserVerificationData { get; set; }
            public string ApprovalFlag { get; set; }
            public Form()
            {
                DelImgRequests = new List<OtherInfo>();
            }
        }
        public class RequestOtp
        {
            public DateTime? ExpiredOn { get; set; }
            public DateTime? NextOtpSendOn { get; set; }
            public string MobileNo { get; set; }
        }
        public enum VerificationType
        {
            SignatureUpload=1,
            Otp=2,
        }
        public class UserVerification
        {
            public VerificationType VerificationType { get; set; }

            public Dictionary<string,object> VerificationData{get;set;}
            public UserVerification()
            {
                VerificationData = new Dictionary<string, object>();
            }
        }
       
        public class MasterData
        {
            public List<OtherInfoType> Location_otherInfos { get; set; }
            public List<OtherInfoType> Images { get; set; }
            public List<KeyVal<int,string>> HoldingTypeGroups { get; set; }
            public List<KeyVal<int,string>> Floors { get; set; }
            public List<KeyVal<int,string>> RoomTypes { get; set; }
            public List<KeyVal<int,string>> RoofTypes { get; set; }
            public List<KeyVal<int,string>> FloorTypes { get; set; }
            public List<KeyVal<int,string>> WallTypes { get; set; }
            public List<KeyVal<int,string>> AssmtYears { get; set; }
            public List<Municipality> Municipalities { get; set; }
            public List<DeedType> DeedTypes { get; internal set; }

            public OtherInfoType AsseesmentYear { get; set; }
            public List<KeyVal<int,string>> LandTypesForVacantLand { get; set; }
            public OtherInfoType VacantLandLandTypeConfig { get; set; }
            public List<KeyVal<int, string>> Usages { get; set; }

            public MasterData()
            {
                HoldingTypeGroups = new List<KeyVal<int,string>>();
                Floors = new List<KeyVal<int,string>>();
                RoomTypes = new List<KeyVal<int,string>>();
                RoofTypes = new List<KeyVal<int,string>>();
                FloorTypes = new List<KeyVal<int,string>>();
                WallTypes = new List<KeyVal<int,string>>();
                AssmtYears = new List<KeyVal<int, string>>();
                Municipalities = new List<Municipality>();
                DeedTypes = new List<DeedType>();
                LandTypesForVacantLand = new List<KeyVal<int, string>>();
            }
        }
        #region tabs
        public class UsageTab
        {
            public int? HoldingTypeGroupId { get; set; }
            public List<Floor>   Floors { get; set; }
            public VacantLand VacantLand { get; set; }
            public UsageTab()
            {
                Floors = new List<Floor>();
            }
        }
        public class OtherTab
        {
            public bool WheatherAssessed { get; set; }
            public int? YearOfAssessment { get; set; }
            public decimal AnnualValuation { get; set; }
            public decimal QtrlyPropTax { get; set; }
            public bool IsOwnershpChanged { get; set; }
            public string OwnershpChanged_Desc { get; set; }
            public bool IsAddAltrModed { get; set; }
            public string AddAltrModed_Desc { get; set; }
            public bool IsNatureOfUsedChanged { get; set; }
            public string NatureOfUsedChanged_Desc { get; set; }
            public decimal TotalAreaApartment_sft { get; set; }
            public decimal OtherArea_sft { get; set; }
            public OtherInfo AssessmentYear { get; set; }
        }
        public class ImagesTab
        {
            public GeoLocation GeoLocation { get; set; }
            public List<OtherInfo> Images { get; set; }
            public ImagesTab()
            {
                Images = new List<OtherInfo>();
            }
        }
        
        public class LocationTab {
            public string Borough { get; set; }//Borough
            public string Ward { get; set; }//wardID
            public int WardID { get; set; }
            public string Holding { get; set; }//Holding no
            public string RoadLane { get; set; }//
            public int RoadLaneId { get; set; }
            public string Complex { get; set; }
            public string LandType { get; set; }
            public decimal LandArea_dec { get; set; }
            public decimal LandArea_sft { get; set; }
            public string MouzaName { get; set; }
            public string JlNo { get; set; }
            public string PS { get; set; }
            public string DagNo_Rs { get; set; }
            public string DagNo_Lr { get; set; }
            public string Khatian_Rs { get; set; }
            public string Khatian_Lr { get; set; }
            public string  RorNature { get; set; }
            public string DeedNo { get; set; }
            public DateTime? DeedDate  { get; set; }
            public int? DeetType { get; set; }
            public List<OtherInfo> OtherInfos { get; set; }
            public LocationTab()
            {
                OtherInfos = new List<OtherInfo>();
            }
        }
        #endregion
        #region Helper class/ model
        public class KeyVal<T,T1>
        {
            public T Key { get; set; }
            public T1 Value { get; set; }
        }
        public class VacantLand
        {
            public decimal Area_dec { get; set; }
            public decimal Area_sft { get; set; }
            public OtherInfo LandType { get; set; }
        }
        public class OtherInfoType
        {
            /// <summary>
            /// master id
            /// </summary>
            public int? InfoTypeId { get; set; }
            /// <summary>
            /// secondary identifire
            /// </summary>
            public string TypeCode { get; set; }
            /// <summary>
            /// text
            /// </summary>
            public string TypeDesc { get; set; }
           
            public string DataType { get; set; }
            /// <summary>
            /// Content type [I=Image,L=location prop]
            /// </summary>
            public string InfoType { get; set; }

        }
        public class OtherInfo:OtherInfoType
        {
            /// <summary>
            /// details id
            /// </summary>
            public long? DatId { get; set; }
            public string Value { get; set; }
           
            public void Validate()
            {
                try
                {
                    if (Value == null)
                    {
                        return;
                    }
                    if (DataType == "D")
                    {
                        decimal.Parse(Value);
                    }
                }
                catch (FormatException)
                {
                    throw new FormatException(string.Format("Bad Format. Invalid value for '{0}'. Support Decimal Value", this.TypeDesc));
                }
                catch (OverflowException)
                {
                    throw new FormatException(string.Format("Range Exceed. Invalid value for '{0}'. Support Decimal Value", this.TypeDesc));
                }
            }

        }
        public class Floor
        {
            public long? TabId { get; set; }
            public int? FloorID { get; set; }
            public string ConstYear { get; set; }
            public int? UseId { get; set; }
            public string NameOfOccupant { get; set; }
            public string OccuDetails { get; set; }
            public int? TotCoverdAreaSft { get; set; }
            public decimal MonRent { get; set; }
            public decimal MonSc { get; set; }
            /// <summary>
            /// no client render
            /// </summary>
            public int MunicipalityID { get; set; }
            public List<Room> Rooms { get; set; }
            public Floor()
            {
                Rooms = new List<Room>();
            }
        }
        
        public class Room
        {
            public long? TabId { get; set; }
            public long TabIdOccu { get; set; }
            public int RoomTypeId { get; set; }
            public decimal WSft { get; set; }
            public decimal LSft { get; set; }
            public decimal HSft { get; set; }
            public int CoverdAreaSft { get; set; }
            public string RoomDetails { get; set; }
            public int RoofType { get; set; }
            public int WallType { get; set; }
            public int FloorType { get; set; }
            public int MunicipalityID { get; set; }
        }
        public class GeoLocation
        {
            public long? TabId { get; set; }
            public decimal Geo_Lat { get; set; }
            public decimal Geo_Long { get; set; }
        }
        #endregion

    }
}