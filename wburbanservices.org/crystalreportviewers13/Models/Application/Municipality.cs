﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Municipality
    {
        public int? MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public string Address { get; set; }
        public int? DistrictID { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string TelePhone { get; set; }
        public string WebSite { get; set; }
        public string Fax { get; set; }
        public int? InsertedBy { get; set; }
        public DateTime? InsertedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public District Disctrict { get; set; }
        public int? AuthorityId { get; set; }
        public string Type { get; set; }
    }
}