﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class CancelInstrumentDetails
    {
        public string PaymentModeDescription { get; set; }
        public string InstrumentNo { get; set; }
        public string InstrumentDate { get; set; }
        public decimal InstrumentAmount { get; set; }
        public string BankName { get; set; }
        public string PaymentModeCode { get; set; }
    }
}