﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class Report
    {
        public int ReportCode { get; set; }
        public string ReportFilePath { get; set; }
        public string ReportName { get; set; }
        public string ConnectionString { get; set; }
    }
}