﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class AssmtScoreSheet
    {
        public long ScoreSheetID { get; set; }
        public DateTime EffectiveDateForm { get; set; }
        public DateTime EffectiveDateTo { get; set; }
        public int MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        public string YearId { get; set; }
        public string ScoreBeltTypeDesc { get; set; }
        public string ScoreCode { get; set; }
        public string ScoreDesc { get; set; }
        public decimal ScoreValue { get; set; }
    }
}