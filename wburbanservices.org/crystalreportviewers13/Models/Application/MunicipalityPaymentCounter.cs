﻿using System;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class MunicipalityPaymentCounter
    {
        public int CounterID { get; set; }
        public string CounterCode { get; set; }
        public int MunicipalityID { get; set; }
        public bool IsActive { get; set; }
        public Municipality Municipality { get; set; }
    }
}