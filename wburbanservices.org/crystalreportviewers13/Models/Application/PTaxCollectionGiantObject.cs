﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class PTaxCollectionGiantObject
    {
        public List<PropertyTaxOutstading> propertyTaxOutstadings { get; set; }
        public PropertyTaxPaymentMaster propertyTaxPaymentMaster { get; set; }
        public object AdditionalData { get; set; }
        public PTaxCollectionGiantObject()
        {
            propertyTaxOutstadings = new List<PropertyTaxOutstading>();
        }
    }
}