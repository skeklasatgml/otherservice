﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Application
{
    public class TaxPaymentCheckParameter
    {
        public string FinYear { get; set; }
        public int QtrNo  { get; set; }
        public bool IsChecked { get; set; }
        public int Rank { get; set; }
    }
}