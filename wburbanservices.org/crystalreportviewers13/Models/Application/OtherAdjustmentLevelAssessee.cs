﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.App_Codes.Utils;

namespace Valuation_Board.Models.Application
{
    public class OtherAdjustmentLevelAssessee: OtherAdjustmentMaster,IOtherAdjustment
    {
        /// <summary>
        /// DtlID
        /// </summary>
        public long RowID { get; set; }
        public long AssesseID { get; set; }
        public long? PayAdjustedBy { get; set; }
        public bool IsPayAdjusted { get; set; }
        public DateTime? PayAdjustedOn { get; set; }



    }
}