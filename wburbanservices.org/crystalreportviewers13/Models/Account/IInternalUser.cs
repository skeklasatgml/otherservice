﻿using System;
namespace Valuation_Board.Models.Account
{
    public interface IInternalUser: IInternalUserMin
    {
        string Email { get; set; }
        string FirstName { get; set; }
        bool IsAccountLocked { get; set; }
        bool IsAccountVerified { get; set; }
        string LastName { get; set; }
        string PhoneNo { get; set; }
    }
    public interface IInternalUserMin : IUserType
    {
        long UserID { get; set; }
    }
}
