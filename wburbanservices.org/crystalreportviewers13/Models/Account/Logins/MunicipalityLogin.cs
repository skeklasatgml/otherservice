﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.Models.Account.Logins
{
    public class MunicipalityLogin 
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int MunicipalityID { get; set; }
    }
}