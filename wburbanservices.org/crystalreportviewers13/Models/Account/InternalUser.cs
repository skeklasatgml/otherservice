﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valuation_Board.Models.Account 
{
    public class InternalUser : Valuation_Board.Models.Account.IInternalUser
    {

        public string Email
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public bool IsAccountLocked
        {
            get;
            set;
        }

        public bool IsAccountVerified
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string PhoneNo
        {
            get;
            set;
        }

        public long UserID
        {
            get;
            set;
        }

        public UserType UserType
        {
            get;
             set;
        }

        public void SetUserType(UserType userType)
        {
            if (userType == UserType.CitizenUser)
            {
                throw new ArgumentException("User type can not be citizen user");
            }
            UserType = userType;
        }
    }
}
