﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Valuation_Board.App_Start;
using Valuation_Board.App_Start.Routes;

namespace Valuation_Board
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            CustomRouter.AddRoute(new MunicipalityReportCalls(routes));
            CustomRouter.AddRoute(new MunicipalityRoute(routes));
            
            CustomRouter.AddRoute(new AdministrationRoute(routes));
            CustomRouter.AddRoute(new ModulesRoute(routes));
            CustomRouter.AddRoute(new SharedRoute(routes));
            CustomRouter.AddRoute(new ErrorRoute(routes));
            CustomRouter.AddRoute(new DefaultRoute(routes));
            CustomRouter.AddRoute(new OnlineRoute(routes));
            CustomRouter.AddRoute(new OtherServicesRoute(routes));
        }
    }
}
