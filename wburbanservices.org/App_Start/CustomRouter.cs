﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Valuation_Board.App_Start
{
    public static class CustomRouter
    {
        private static ObservableCollection<IRoute> Routes = new ObservableCollection<IRoute>();
        static CustomRouter()
        {
            Routes.CollectionChanged += HandleChange;
           
        }
        public static void AddRoute(IRoute route)
        {
            Routes.Add(route);
        }
        public static void RemoveRoute(IRoute route)
        {
            throw new NotImplementedException("Method not implemented");
        }
        private static void HandleChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                         
                        OnRouteAdded(Routes, e.NewStartingIndex, e.NewItems[0] as IRoute);
                        break;
                    }
               
                case NotifyCollectionChangedAction.Remove:
                    {
                        OnRouteRemoved(Routes, e.OldStartingIndex, e.OldItems[0] as IRoute);
                        break;
                    }
            }
        }
        public static void OnRouteAdded(ObservableCollection<IRoute> sender, int index, IRoute newItem)
        {
            newItem.MapRoute();
        }
        public static void OnRouteRemoved(ObservableCollection<IRoute> sender, int index, IRoute oldItem)
        {
            
        }
    }
}