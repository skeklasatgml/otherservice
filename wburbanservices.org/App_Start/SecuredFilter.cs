﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Valuation_Board.App_Codes;
using Valuation_Board.App_Codes.BLL;
using Valuation_Board.Models;
using Valuation_Board.Models.AppResource;

namespace Valuation_Board.App_Start
{
    public class SecuredFilter : FilterAttribute, IAuthenticationFilter, IAuthorizationFilter
    {
        //1
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            try
            {
                var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                var actionName = filterContext.ActionDescriptor.ActionName;
                string[] controllerPackages = filterContext.RouteData.DataTokens["Namespaces"] as string[];

                string controllerPackage = null;//  filterContext.RouteData.DataTokens["Namespaces"][0] as string ;
                if (controllerPackages.Length > 0)
                {
                    controllerPackage = controllerPackages[0];
                }
                else
                {
                    throw new InvalidOperationException("404$Resource Not Found");
                }
                UrlBLL urlHandler = new UrlBLL();
                //get url model
                AppUrl filterModel = urlHandler.GetUrl(actionName, controllerName, controllerPackage);

                if (filterModel == null)
                {
                    throw new InvalidOperationException("404$Resource Not Found");
                }
                //if anounymous take no action.
                if (!filterModel.IsAnonymousUrl)
                {
                    //must be authenticated. else throw 401
                    if (SessionContext.IsAuthenticated)
                    {
                        if (filterModel.IsSharedUrl)
                        {
                            //ok
                        }
                        else if (urlHandler.hasUrlInCurRole(filterModel, SessionContext.CurrentUser,SessionContext.AppTag))
                        {
                            //ok
                        }
                        else
                        {
                            throw new InvalidOperationException("403$Forbidden");
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("401$Authentication Required");
                    }
                }
            }
            catch (Exception ex)
            {
                Action<Exception> handeException = (exception) =>
                {
                    int statusCode = 500;
                    string message = "Unknown Application Error. Error source-'UrlFilter'";

                    new Logger().LogError(exception); ;
                    if (exception.GetType() == typeof(InvalidOperationException))
                    {
                        try
                        {
                            statusCode = Convert.ToInt32(exception.Message.Split('$')[0]);
                            message = exception.Message.Split('$')[1];
                        }
                        catch
                        {
                            statusCode = 500;
                            message =string.Format("Filter Exception: {0}", exception.Message);
                        }
                    }
                    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        ResponseStatus responseStatus = (ResponseStatus)statusCode;
                        filterContext.Result = new JsonResult()
                        {
                            Data = new ClientJsonResult()
                            {
                                Data = null,
                                Message = message,
                                Status = responseStatus
                            }
                        };
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult("ErrorRouter",
                               new System.Web.Routing.RouteValueDictionary{
                                {"controller", string.Format("Error{0}",statusCode)},
                                {"action", "Index"},
                                {"errorCode", statusCode},
                                {"Message", message},
                               });
                    }
                };

                handeException(ex);
            }

        }

        //3
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {

        }

        //2
        public void OnAuthorization(AuthorizationContext filterContext)
        {

        }

    }
}