﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Valuation_Board.App_Start
{
    public interface IRoute
    {
        void MapRoute();
    }
}