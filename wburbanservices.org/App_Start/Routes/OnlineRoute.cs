﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Valuation_Board.App_Start.Routes
{
    public class OnlineRoute:IRoute
    {
        private RouteCollection _routes { get; set; }
        public OnlineRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "CitizenRouter",
                url: "online/{controller}/{action}"
                ,defaults: new { controller = "onlinepayment", action = "OnlineTaxPayment", id = UrlParameter.Optional }
                , namespaces: new[] { "Valuation_Board.Controllers.online" }
            );
        }
    }
}