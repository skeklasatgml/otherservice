﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Valuation_Board.App_Start.Routes
{
    public class DefaultRoute : IRoute
    {
        private RouteCollection _routes { get; set; }
        public DefaultRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { controller = "(Home|AdditionalInfo|Profile)" }//(Home|Other)
                ,namespaces: new[] { "Valuation_Board.Controllers" }
            );
        }
    }
}