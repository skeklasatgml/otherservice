﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Valuation_Board.App_Start.Routes
{
    public class MunicipalityRoute:IRoute
    {
        private RouteCollection _routes { get; set; }
        public MunicipalityRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "MunicipalityRouter",
                url: "Municipality/{controller}/{action}/{id}",
                defaults: new { controller = "Collection", action = "Index", id = UrlParameter.Optional }
                ,namespaces:new[] { "Valuation_Board.Controllers.Municipality" }
            );
        }
    }
}