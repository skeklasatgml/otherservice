﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Valuation_Board.App_Start.Routes
{
    public class ModulesRoute : IRoute
    {
        private RouteCollection _routes { get; set; }
        public ModulesRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "ModulesRouter",
                url: "Modules/{controller}/{action}/{id}",
                defaults: new { controller = "Dpr", action = "DprListUi", id = UrlParameter.Optional }
                ,namespaces:new[] { "Valuation_Board.Controllers.Modules" }
            );
        }
    }
}