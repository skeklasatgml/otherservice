﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valuation_Board.PGI
{
    public abstract class PgiBase
    {
        protected string ReplaceCurrHost(string oldUrl)
        {
            var builder1 = new UriBuilder(HttpContext.Current.Request.Url);
            var builder = new UriBuilder(oldUrl);
            builder.Host = builder1.Host;
            builder.Port = builder1.Port;
            return builder.Uri.ToString();
        }
    }
}