﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.PGI.Gateways
{
    public class Bildesk:PgiBase
    {

        public void DoRedirectionToPaymentGateway(PG_Model pgObj)
        {
            try
            {
                ///EnquiryMaster master = Enquiry.enquiryMaster;
                string MerchantID = ConfigurationManager.AppSettings["MerchantID"];// Bill Desk Merchant ID
                string CustomerID = pgObj.CustomerID.ToString().Trim();//Transaction Reference No of Merchant
                string TransactionAmt = Convert.ToDouble(pgObj.TxnAmount).ToString("0.00");
                string CurrencyType = ConfigurationManager.AppSettings["CurrencyType"];
                string TypeField1 = ConfigurationManager.AppSettings["TypeField1"];
                string SecurityID = ConfigurationManager.AppSettings["SecurityID"];
                string TypeField2 = ConfigurationManager.AppSettings["TypeField2"];
                string PaymentURL = ConfigurationManager.AppSettings["PaymentURL"];// to open on live server"http://localhost:50624/OnlinePaymentResponseSS.ashx";//"http://localhost:56047/OnlinePaymentResponse.aspx";//
                string ResponseURL =ReplaceCurrHost( ConfigurationManager.AppSettings["ResponseURL"]);// to open on live server
                string CheckSumKey = ConfigurationManager.AppSettings["CheckSumKey"];
                Regex re = new Regex("[^A-Za-z0-9]");
                string CanName = (pgObj.AssesseeName.ToString().Trim() == "" ? "NA" : pgObj.AssesseeName.ToString().Trim());
                string AdditionalInfo1 = re.Replace(pgObj.HoldingNo.Replace(" ", ""), "_");//re.Replace(CanName.Replace(" ", ""), String.Empty);
                string AdditionalInfo2 = pgObj.WardID;//(master.MobileNo == String.Empty || master.MobileNo == null ? String.Empty.PadLeft(10, '0') : master.MobileNo.Trim());
                string AdditionalInfo3 = pgObj.MunicipalityID;//master.UserID.ToString().Trim();//1000001
                string AdditionalInfo4 = pgObj.LocationID;//master.ServiceId.ToString().Trim();

                string AdditionalInfo5 = re.Replace(pgObj.AssesseeName.Replace(" ", ""), String.Empty);
                string AdditionalInfo6 = re.Replace(pgObj.PaymentID.Replace(" ", ""), String.Empty);

                string AdditionalInfo7 = pgObj.UserID.Trim();//"NA";

                string Method = "post";
                string FormName = "payment";
                string hashData = "", checksumvalue = "";

                // *********************   for online  *******************************************
                string msg = MerchantID + "|" + CustomerID + "|" + "NA" + "|" + TransactionAmt + "|" + "NA|NA|NA|" + CurrencyType + "|"
                            + "NA" + "|" + TypeField1 + "|" + SecurityID + "|" + "NA|NA|" + TypeField2 + "|" + AdditionalInfo1 + "|"
                            + AdditionalInfo2 + "|" + AdditionalInfo3 + "|" + AdditionalInfo4 + "|" + AdditionalInfo5 + "|"
                            + AdditionalInfo6 + "|" + AdditionalInfo7 + "|" + ResponseURL;
                hashData = msg;
                checksumvalue = GetParamHashValue(hashData, CheckSumKey);
                msg += "|" + checksumvalue;


                // *********************   for offline testing *******************************************
                //string msg = MerchantID + "|" + CustomerID + "|" + "MSBI7323232001668" + "|" + "NA" + "|0000" + TransactionAmt + "|" + "SBI|22270726|NA|" + CurrencyType + "|"
                //            + "NA|NA|NA|NA|27-05-201508:58:45|0300|NA|" + AdditionalInfo1 + "|"
                //            + AdditionalInfo2 + "|" + AdditionalInfo3 + "|" + AdditionalInfo4 + "|" + AdditionalInfo5 + "|"
                //            + AdditionalInfo6 + "|" + AdditionalInfo7 + "|NA|NA";
                //hashData = msg;
                //checksumvalue = GetParamHashValue(hashData, CheckSumKey);
                //msg += "|" + checksumvalue;

                //string msg = MerchantID + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|0000" + TransactionAmt + "|" + "SBI|22270726|NA|" + CurrencyType + "|"
                //            + "NA|NA|NA|NA|21-12-201415:45:00|0002|NA|" + "NA" + "|"
                //            + "NA" + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|"
                //            + "NA" + "|" + "NA" + "|NA|NA";
                //hashData = msg;
                //checksumvalue = GetParamHashValue(hashData, CheckSumKey);
                //msg += "|" + checksumvalue;

                ////string msg = MerchantID + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|0000" + TransactionAmt + "|" + "SBI|22270726|NA|" + CurrencyType + "|"
                ////            + "NA|NA|NA|NA|21-12-201415:45:00|0002|NA|" + "NA" + "|"
                ////            + "NA" + "|" + "NA" + "|" + "NAPORT" + "|" + "NA" + "|"
                ////            + "NAREST" + "|" + "NA" + "|NA|NA";
                ////hashData = msg;
                ////checksumvalue = GetParamHashValue(hashData, CheckSumKey);
                ////msg += "|" + checksumvalue;

                ////string msg = MerchantID + "|" + CustomerID + "|" + "NA" + "|" + "NA" + "|0000" + TransactionAmt + "|" + "SBI|22270726|NA|" + CurrencyType + "|"
                ////            + "NA|NA|NA|NA|21-12-201415:45:00|0300|NA|" + "NA" + "|"
                ////            + "NA" + "|" + "NA" + "|" + "NAPORT" + "|" + "NA" + "|"
                ////            + "NAREST" + "|" + "NA" + "|NA|NA";
                ////hashData = msg;
                ////checksumvalue = GetParamHashValue(hashData, CheckSumKey);
                ////msg += "|" + checksumvalue;


                //********************************* end **************************************************
                //Response.Write(msg);


                HttpResponse Response = HttpContext.Current.Response;
                Response.Clear();
                Response.Write("<html><head>");
                Response.Write("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"no-store, no-cache, must-revalidate\" />");
                Response.Write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"no-store, no-cache, must-revalidate\" />");
                Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
                Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, PaymentURL));
                Response.Write("<input type=\"hidden\" name=\"msg\" value=" + msg + " />");
                Response.Write("</form>");
                Response.Write("</body></html>");
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }
        private static string GetParamHashValue(string HashData, string CheckSumKey)
        {
            string hashedvalue = "";
            try
            {
                if (!string.IsNullOrEmpty(HashData) && HashData.Length > 0)
                {

                    hashedvalue = Crypto.GenerateHashString(HashData, SortCommon.GetConfigAlgorithm("Algorithm"), Crypto.EncodingType.HEX, System.Text.Encoding.ASCII.GetBytes(CheckSumKey)).ToUpper();
                    //hashedvalue = HashData;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return hashedvalue;
        }
        private static DataTable SaveResponse(
                                                                          string ResponseCode,
                                                                          string ResponseMessage,
                                                                          string MerchantID,
                                                                          string CustomerID,
                                                                          string TxnReferenceNo,
                                                                          string BankReferenceNo,
                                                                          string TxnAmount,
                                                                          string BankID,
                                                                          string BankMerchantID,
                                                                          string TxnType,
                                                                          string CurrencyName,
                                                                          string ItemCode,
                                                                          string SecurityType,
                                                                          string SecurityID,
                                                                          string SecurityPassword,
                                                                          string TxnDate,
                                                                          string AuthStatus,
                                                                          string SettlementType,
                                                                          string AdditionalInfo1,
                                                                          string AdditionalInfo2,
                                                                          string AdditionalInfo3,
                                                                          string AdditionalInfo4,
                                                                          string AdditionalInfo5,
                                                                          string AdditionalInfo6,
                                                                          string AdditionalInfo7,
                                                                          string ErrorStatus,
                                                                          string ErrorDescription,
                                                                          string BankCheckSum,
                                                                          string CalculatedCheckSum,
                                                                          long UserID,
                                                                          string TransationType,
                                                                          string ResponseMode,
                                                                          int UserType,
                                                                          string ResponseMsg)
        {
            string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            SqlConnection cn = new SqlConnection(constring);
            SqlTransaction tr = null;
            try
            {
                cn.Open();

                tr = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand("Insert_PgBildesk_OnlineTransactionResponse", cn, tr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ResponseCode", ResponseCode);
                cmd.Parameters.AddWithValue("@ResponseMessage", ResponseMessage);
                cmd.Parameters.AddWithValue("@MerchantID", MerchantID);
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                cmd.Parameters.AddWithValue("@TxnReferenceNo", TxnReferenceNo);
                cmd.Parameters.AddWithValue("@BankReferenceNo", BankReferenceNo);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@BankID", BankID);
                cmd.Parameters.AddWithValue("@BankMerchantID", BankMerchantID);
                cmd.Parameters.AddWithValue("@TxnType", TxnType);
                cmd.Parameters.AddWithValue("@CurrencyName", CurrencyName);
                cmd.Parameters.AddWithValue("@ItemCode", ItemCode);
                cmd.Parameters.AddWithValue("@SecurityType", SecurityType);
                cmd.Parameters.AddWithValue("@SecurityID", SecurityID);
                cmd.Parameters.AddWithValue("@SecurityPassword", SecurityPassword);
                cmd.Parameters.AddWithValue("@TxnDate", TxnDate);
                cmd.Parameters.AddWithValue("@AuthStatus", AuthStatus);
                cmd.Parameters.AddWithValue("@SettlementType", SettlementType);
                cmd.Parameters.AddWithValue("@AdditionalInfo1", AdditionalInfo1);
                cmd.Parameters.AddWithValue("@AdditionalInfo2", AdditionalInfo2);
                cmd.Parameters.AddWithValue("@AdditionalInfo3", AdditionalInfo3);
                cmd.Parameters.AddWithValue("@AdditionalInfo4", AdditionalInfo4);
                cmd.Parameters.AddWithValue("@AdditionalInfo5", AdditionalInfo5);
                cmd.Parameters.AddWithValue("@AdditionalInfo6", AdditionalInfo6);
                cmd.Parameters.AddWithValue("@AdditionalInfo7", AdditionalInfo7);
                cmd.Parameters.AddWithValue("@ErrorStatus", ErrorStatus);
                cmd.Parameters.AddWithValue("@ErrorDescription", ErrorDescription);
                cmd.Parameters.AddWithValue("@BankCheckSum", BankCheckSum);
                cmd.Parameters.AddWithValue("@CalculatedCheckSum", CalculatedCheckSum);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Parameters.AddWithValue("@TransationType", "P");
                cmd.Parameters.AddWithValue("@ResponseMode", ResponseMode);
                cmd.Parameters.AddWithValue("@UserType", UserType);
                cmd.Parameters.AddWithValue("@ResponseMsg", ResponseMsg);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                tr.Commit();
                return dt;

            }
            catch (SqlException)
            {
                tr.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }

        }

        public string SaveResponseNRedirect()
        {
            HttpRequest Request = HttpContext.Current.Request;
            IEncryptDecrypt chiperAlgorithm = new AES_Algorithm(ConfigurationManager.AppSettings["ChiperKey"] as string);
            string respcd = null;
            string respmsg = null;

            string[] strarr = null;
            string HashedValue = "";
            if (Request.HttpMethod == "POST")
            {
                NameValueCollection ResponseFields = new NameValueCollection();
                for (int i = 0; i < Request.Form.Keys.Count; i++)
                {
                    ResponseFields.Add(Request.Form.Keys[i], Request.Form[i]);
                }
                //NameValueCollection ResponseFields = (Request.QueryString.Count > 0) ? Request.QueryString : Request.Form;
                string msg = ResponseFields["msg"].ToString();//Request.Form["msg"].ToString();//ResponseFields["msg"];
                string CheckSumKey = SortCommon.GetAppConfig("CheckSumKey");
                if (!string.IsNullOrEmpty(msg) && msg.Trim().Length > 0)
                {
                    strarr = msg.Split('|');
                    string hashData = "";
                    for (int i = 0; i < strarr.Length - 1; i++)
                    {
                        hashData += strarr[i] + "|";
                    }
                    if (!string.IsNullOrEmpty(hashData) && hashData.Trim().Length > 0)
                    {
                        hashData = hashData.Substring(0, hashData.LastIndexOf("|"));
                    }

                    HashedValue = Bildesk.GetParamHashValue(hashData, CheckSumKey);
                    string ResponseFieldHashedValue = strarr[25];
                    if (HashedValue.Equals(ResponseFieldHashedValue))
                    {
                        respcd = strarr[14];
                        if (respcd == "0300")
                        {
                            respmsg = "SUCCESS";
                        }
                        else
                        {
                            respmsg = strarr[24];
                        }
                    }
                    else
                    {
                        throw new InvalidExpressionException("Problem in parsing payment gatewway checksum response");
                    }
                }
                if (respcd != null)
                {
                    DataTable dtResponse = Bildesk.SaveResponse(ResponseCode: respcd,
                                                                        ResponseMessage: respmsg,
                                                                        MerchantID: strarr[0].Trim(),
                                                                        CustomerID: strarr[1].Trim(),
                                                                        TxnReferenceNo: strarr[2].Trim(),
                                                                        BankReferenceNo: strarr[3].Trim(),
                                                                        TxnAmount: (strarr[4].Trim()),
                                                                        BankID: strarr[5].Trim(),
                                                                        BankMerchantID: strarr[6].Trim(),
                                                                        TxnType: strarr[7].Trim(),
                                                                        CurrencyName: strarr[8].Trim(),
                                                                        ItemCode: strarr[9].Trim(),
                                                                        SecurityType: strarr[10].Trim(),
                                                                        SecurityID: strarr[11].Trim(),
                                                                        SecurityPassword: strarr[12].Trim(),
                                                                        TxnDate: strarr[13].Trim(),
                                                                        AuthStatus: strarr[14].Trim(),
                                                                        SettlementType: strarr[15].Trim(),
                                                                        AdditionalInfo1: strarr[16].Trim(),
                                                                        AdditionalInfo2: strarr[17].Trim(),
                                                                        AdditionalInfo3: strarr[18].Trim(),
                                                                        AdditionalInfo4: strarr[19].Trim(),
                                                                        AdditionalInfo5: strarr[20].Trim(),
                                                                        AdditionalInfo6: strarr[21].Trim(),
                                                                        AdditionalInfo7: strarr[22].Trim(),
                                                                        ErrorStatus: strarr[23].Trim(),
                                                                        ErrorDescription: strarr[24].Trim(),
                                                                        BankCheckSum: strarr[25].Trim(),
                                                                        CalculatedCheckSum: HashedValue.Trim(),
                                                                        UserID: Convert.ToInt64(strarr[22].Trim()),
                                                                        TransationType: "P",
                                                                        ResponseMode: "BrowserToServer",
                                                                        UserType: 1,
                                                                        ResponseMsg: msg
                                                                        );

                    if (respcd == "0300")
                    {
                        if (dtResponse.Rows.Count > 0)
                        {
                            if (dtResponse.Rows[0].Field<bool>("IsPaymentSuccessfull") == true)
                            {
                                var successObj = new { PaymentID = dtResponse.Rows[0].Field<long>("PaymentID"), TransactionID = dtResponse.Rows[0].Field<string>("TransactionID"), Message = "Payment Successfull" };
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                string json = js.Serialize(successObj);
                                return string.Format("/online/onlinepayment/PaymentSuccessfull?Data={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(json)));
                            }
                            else
                            {
                                throw new InvalidOperationException("Payment not successfull");
                            }

                        }
                        else
                        {
                            throw new InvalidOperationException("Payment not successfull");
                        }

                    }
                    else  //(respcd != "0300")
                    {
                        throw new InvalidOperationException(string.Format("Payment not successfull. Response Code: {0}, Message: {1}", respcd, respmsg));
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid response Code: Null");
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid response method[http]");
            }
        }
    }
}