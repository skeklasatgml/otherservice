﻿using CCA.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Valuation_Board.App_Codes.Extension;
using Valuation_Board.App_Codes.Utils;
using Valuation_Board.Models.Application;
using Valuation_Board.Utils.EncryptionDecrytion;

namespace Valuation_Board.PGI.Gateways.HDFC
{
    public class HDFC : PgiBase
    {
        private IEncryptDecrypt chiperAlgorithm;
        JavaScriptSerializer js = new JavaScriptSerializer();
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ConnectionString);
        public HDFC()
        {
            string key = ConfigurationManager.AppSettings["ChiperKey"] as string;
            chiperAlgorithm = new AES_Algorithm(key);
        }
        public void DoRedirectionToPaymentGateway(PG_Model pgObj)
        {

            try
            {
                HttpResponse Response = HttpContext.Current.Response;
                HdfcAccountBase _base = new HdfcAccountInfo_BLL(Convert.ToInt32(pgObj.MunicipalityID)).GetAccountInfo(pgObj.PG_Code);
                if (_base == null)
                {
                    throw new NullReferenceException("Payment Getway Not Found");
                }
                if (_base.IsActive == false)
                {
                    throw new InvalidOperationException("Payment Gateway is Deactivated");
                }


                //HDFC Legacy gateway. For old credential format
                if (string.Equals(_base.PGCode, "HF", StringComparison.OrdinalIgnoreCase))
                {
                    HdfcAccountInfo hdfcAccount = _base as HdfcAccountInfo;

                    string AccountID = hdfcAccount.AccountID;
                    string Channel = "10";
                    string Currency = "INR";
                    string CurrencyCode = "356";
                    string finalamount = Convert.ToDouble(pgObj.TxnAmount).ToString("0.00");

                    string ResponseFileName = ReplaceCurrHost(hdfcAccount.ResposeUrl);
                    System.Diagnostics.Debug.WriteLine("converted response url =>" + ResponseFileName);

                    String algo = hdfcAccount.Algorithm;
                    string skey = hdfcAccount.SecreteKey;
                    string CountryCode = "IND";


                    Regex re = new Regex("[;\\/:*?\"<>|&'%`~^_()]");
                    string assesseeName = (pgObj.AssesseeName.ToString().Trim() == "" ? "NA" : pgObj.AssesseeName.ToString().Trim());

                    NameValueCollection FormFields = new NameValueCollection();
                    FormFields.Add("account_id", AccountID);//^
                    FormFields.Add("channel", Channel);//^
                    FormFields.Add("currency", Currency);//^
                    FormFields.Add("currency_code", CurrencyCode);//^
                    FormFields.Add("reference_no", re.Replace(pgObj.PaymentID.Replace(" ", ""), String.Empty));//^ MstPaymentID
                    FormFields.Add("amount", finalamount);//^
                    FormFields.Add("description", pgObj.CustomerID);//^ Local TransactionID/ CustomarID
                    FormFields.Add("name", re.Replace(pgObj.AssesseeName.Replace(" ", ""), String.Empty));//^
                    FormFields.Add("address", re.Replace(pgObj.HoldingNo.Replace(" ", ""), "_"));//^Hodling No
                    FormFields.Add("city", pgObj.WardID);//^WardID
                    FormFields.Add("state", pgObj.MunicipalityID);//^MunicipalityID
                    FormFields.Add("postal_code", pgObj.LocationID);//^LocationID
                    FormFields.Add("country", CountryCode);
                    FormFields.Add("email", "nomail@mail.com");
                    FormFields.Add("phone", "0000000000");
                    FormFields.Add("mode", "LIVE");
                    FormFields.Add("return_url", ResponseFileName);
                    FormFields.Add("ship_name", pgObj.UserID.Trim());//^UserID
                    FormFields.Add("ship_address", re.Replace(pgObj.PaymentID.Replace(" ", ""), String.Empty));//paymentID
                    FormFields.Add("ship_city", "111111");
                    FormFields.Add("ship_state", "111111");
                    FormFields.Add("ship_country", CountryCode);
                    FormFields.Add("ship_phone", "111111");
                    //FormFields.Add("algo", algo);
                    FormFields.Add("ship_postal_code", "111111");
                    //FormFields.Add("name_on_card", "null");
                    //FormFields.Add("card_number", "null");
                    //FormFields.Add("card_expiry", "null");
                    //FormFields.Add("card_cvv", "null");

                    string hashedValue = GetParamHashValueHDFC(FormFields, algo, hdfcAccount);
                    System.Diagnostics.Debug.WriteLine(string.Format("HashedValue=> {0}", hashedValue));
                    //string formValue = BookingDataOnline.GetParamFormValue(FormFields, algo);
                    string V3URL = hdfcAccount.PaymentUrl;//"https://secure.ebs.in/pg/ma/payment/request";//https://secure.ebs.in/pg/ma/payment/request
                    string Method = "post";
                    string FormName = "payment";

                    //get string representation of html form
                    Func<string> getFormData = () =>
                    {
                        string frmData = "";
                        frmData += "<html><head>";
                        frmData += "<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"no-store, no-cache, must-revalidate\" />";
                        frmData += "<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"no-store, no-cache, must-revalidate\" />";
                        frmData += string.Format("</head><body onload=\"document.{0}.submit()\">", FormName);
                        frmData += string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, V3URL);
                        SortedDictionary<string, string> sortedDict1 = SortCommonHDFC.SortNameValueCollection(FormFields);
                        foreach (KeyValuePair<string, string> p in sortedDict1)
                        {
                            if (p.Key.ToString() != "secure_hash" && !p.Key.ToString().ToLower().StartsWith("__") && p.Key.ToString() != "secretkey" && p.Key.ToString() != "submitted")
                            {
                                frmData += string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", p.Key.ToString(), p.Value.ToString());

                            }
                        }
                        frmData += "<input type=\"hidden\" name=\"secure_hash\" value=" + hashedValue + " />";
                        frmData += "</form>";
                        frmData += "</body></html>";
                        return frmData;
                    };

                    string htmlFormedData = getFormData();

                    System.Diagnostics.Debug.WriteLine("========================Posting Form Data=================================");
                    System.Diagnostics.Debug.WriteLine(htmlFormedData);

                    Response.Clear();
                    Response.Write("<html><head>");
                    Response.Write("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"no-store, no-cache, must-revalidate\" />");
                    Response.Write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"no-store, no-cache, must-revalidate\" />");
                    Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
                    Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, V3URL));
                    SortedDictionary<string, string> sortedDict = SortCommonHDFC.SortNameValueCollection(FormFields);
                    foreach (KeyValuePair<string, string> p in sortedDict)
                    {
                        if (p.Key.ToString() != "secure_hash" && !p.Key.ToString().ToLower().StartsWith("__") && p.Key.ToString() != "secretkey" && p.Key.ToString() != "submitted")
                        {
                            Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", p.Key.ToString(), p.Value.ToString()));
                        }
                    }

                    Response.Write("<input type=\"hidden\" name=\"secure_hash\" value=" + hashedValue + " />");
                    Response.Write("</form>");
                    Response.Write("</body></html>");
                    //Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                }
                //HDFC new gatway provided by CC_Avenew
                else if (string.Equals(_base.PGCode, "Hc", StringComparison.OrdinalIgnoreCase))
                {
                    HdfcAccountInfo_CCAVNW hdfcAccount = _base as HdfcAccountInfo_CCAVNW;

                    string finalamount = Convert.ToDouble(pgObj.TxnAmount).ToString("0.00");
                    string ResponseUrl = ReplaceCurrHost(hdfcAccount.ResposeUrl);
                    string CancelUrl = ReplaceCurrHost(hdfcAccount.CancelUrl);
                    string CountryCode = "IND";


                    Regex re = new Regex("[;\\/:*?\"<>|&'%`~^_()]");
                    string assesseeName = (pgObj.AssesseeName.ToString().Trim() == "" ? "NA" : pgObj.AssesseeName.ToString().Trim());

                    NameValueCollection FormFields = new NameValueCollection();
                    //Compulsory information
                    FormFields.Add("merchant_id", hdfcAccount.MarchentId);
                    FormFields.Add("order_id", re.Replace(pgObj.PaymentID.Replace(" ", ""), String.Empty));
                    FormFields.Add("amount", finalamount);
                    FormFields.Add("currency", "INR");
                    FormFields.Add("redirect_url", ResponseUrl);
                    FormFields.Add("cancel_url", CancelUrl);
                    FormFields.Add("language", "EN");

                    //billing information
                    FormFields.Add("billing_name", re.Replace(pgObj.AssesseeName.Replace(" ", ""), String.Empty));
                    //hodling no
                    FormFields.Add("billing_address", "");
                    FormFields.Add("billing_city", "");
                    FormFields.Add("billing_state", "");
                    FormFields.Add("billing_zip", "");
                    FormFields.Add("billing_country", "");
                    FormFields.Add("billing_tel", "");
                    FormFields.Add("billing_email", "");

                    //shipping information
                    FormFields.Add("delivery_name", "");
                    FormFields.Add("delivery_address", "");
                    FormFields.Add("delivery_city", "");
                    FormFields.Add("delivery_state", "111");
                    FormFields.Add("delivery_zip", "111");
                    FormFields.Add("delivery_country", "");
                    FormFields.Add("delivery_tel", "0000000000");

                    FormFields.Add("merchant_param1", pgObj.CustomerID);
                    FormFields.Add("merchant_param2", pgObj.MunicipalityID);
                    //ward#location#holding
                    FormFields.Add("merchant_param3", string.Format("{0}#{1}#{2}",  pgObj.WardID, pgObj.LocationID, re.Replace(pgObj.HoldingNo.Replace(" ", ""), "_")));
                    FormFields.Add("merchant_param4", "");
                    FormFields.Add("merchant_param5", pgObj.UserID.Trim());

                    FormFields.Add("card_name", "");
                    FormFields.Add("card_number", "");
                    FormFields.Add("card_type", "");
                    FormFields.Add("cvv_number", "");
                    FormFields.Add("data_accept", "");
                    FormFields.Add("emi_plan_id", "");
                    FormFields.Add("emi_tenure_id", "");
                    FormFields.Add("expiry_month", "");
                    FormFields.Add("expiry_year", "");
                    FormFields.Add("issuing_bank", "");
                    FormFields.Add("mm_id", "");
                    FormFields.Add("mobile_number", "");
                    FormFields.Add("otp", "");
                    FormFields.Add("promo_code", "");

                    Func<NameValueCollection, long> makeRequestLog = (_FormFields) =>
                    {
                        cn.Open();
                        SqlCommand cmd = new SqlCommand("Insert_PgHDFC_OnlineTransactionRequestLogCC_Avenew", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //parameter bind
                        foreach (var key in _FormFields)
                        {
                            if (key != null)
                            {
                                if (!key.ToString().StartsWith("_"))
                                {
                                    cmd.Parameters.AddWithValue(string.Format("@{0}", key), _FormFields[key.ToString()].ReplaceDbNull());
                                }
                            }
                        }

                        return Convert.ToInt64(cmd.ExecuteScalar());

                    };
                    //add transaction id
                    FormFields.Add("tid", makeRequestLog(FormFields).ToString());

                    System.Diagnostics.Debug.WriteLine(string.Format("======Parameters==============="));
                    foreach (var key in FormFields)
                    {
                        if (key != null)
                        {
                            if (!key.ToString().StartsWith("_"))
                            {
                                System.Diagnostics.Debug.WriteLine(string.Format("{0}=>{1}", key, FormFields[key.ToString()].ReplaceDbNull()));
                            }
                        }
                    }
                    //serialize and encrypt sensitive payment information
                    Func<NameValueCollection, string> processStr = (_FormFields) =>
                        {
                            string strEncRequest = "";
                            string ccaRequest = "";
                            foreach (var key in _FormFields)
                            {
                                if (key != null)
                                {
                                    if (!key.ToString().StartsWith("_"))
                                    {
                                        ccaRequest = ccaRequest + key + "=" + HttpUtility.UrlEncode(_FormFields[key.ToString()]) + "&";
                                    }
                                }
                            }
                            strEncRequest = new CCACrypto().Encrypt(ccaRequest, hdfcAccount.WorkingKey);
                            return strEncRequest;
                        };

                    //string formValue = BookingDataOnline.GetParamFormValue(FormFields, algo);
                    string V3URL = hdfcAccount.PaymentUrl;//CC_Avenew payment url
                    string Method = "post";
                    string FormName = "redirect";

                    //get string representation of html form
                    Func<string> getFormData = () =>
                    {
                        string frmData = "";
                        frmData += "<html><head>";
                        //frmData += "<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"no-store, no-cache, must-revalidate\" />";
                        //frmData += "<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"no-store, no-cache, must-revalidate\" />";
                        frmData += string.Format("</head><body onload=\"document.{0}.submit()\">", FormName);
                        frmData += string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, V3URL);
                        frmData += string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "encRequest", processStr(FormFields));
                        frmData += "<input type=\"hidden\" name=\"access_code\" value=" + hdfcAccount.AccessCode + " />";
                        frmData += "</form>";
                        frmData += "</body></html>";
                        return frmData;
                    };

                    string htmlFormedData = getFormData();

                    System.Diagnostics.Debug.WriteLine("========================Posting Form Data=================================");
                    System.Diagnostics.Debug.WriteLine(htmlFormedData);

                    Response.Clear();
                    Response.Write(htmlFormedData);
                    //Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    throw new InvalidOperationException("Invalid HDFC Payment Gateway");
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public string SaveResponseNRedirect()
        {

            HttpRequest Request = HttpContext.Current.Request;
            HttpResponse Response = HttpContext.Current.Response;
            try
            {
                //PGResponse oPgResp = new PGResponse();
                //EncryptionUtil lEncUtil = new EncryptionUtil();
                string respcd = null;
                string respmsg = null;
                string astrResponseData = null;
                string strMerchantId, astrFileName = null;
                string strKey = null;
                string strDigest = null;
                string astrsfaDigest = null;
                string HashedValue = "";
                //string respon = "Success";

                String algo = "MD5";
                NameValueCollection ResponseFields = new NameValueCollection();
                if (Request.HttpMethod == "POST")
                {

                    System.Diagnostics.Debug.WriteLine("Response String=========");

                    for (int i = 0; i < Request.Form.Keys.Count; i++)
                    {
                        ResponseFields.Add(Request.Form.Keys[i], Request.Form[i]);
                        System.Diagnostics.Debug.WriteLine(string.Format("{0} => {1}", Request.Form.Keys[i], Request.Form[i]));
                    }
                    int municipalityID = Convert.ToInt32(ResponseFields["BillingState"]);
                    HdfcAccountInfo_BLL bll = new HdfcAccountInfo_BLL(municipalityID);
                    HdfcAccountInfo hdfcAccountInfo = bll.GetAccountInfo("HF") as HdfcAccountInfo;
                    if (hdfcAccountInfo == null)
                    {
                        throw new InvalidOperationException("Response Field Malformed or Tergate Municipality does not have HDFC Account Information");
                    }

                    HashedValue = GetParamHashValueHDFC(ResponseFields, algo, hdfcAccountInfo);
                    string ResponseFieldHashedValue = ResponseFields["SecureHash"].ToString();
                    if (HashedValue.Equals(ResponseFieldHashedValue))
                    {
                        respcd = ResponseFields["ResponseCode"];
                        respmsg = ResponseFields["ResponseMessage"];

                        if (respcd != null)
                        {
                            #region unused code comment by ranadeep
                            //int GuestAdminID = 0;
                            //int GuestID = 0;
                            //GuestLoginDetails GLD = null;
                            //BookingInfo bookingInfo = null;

                            //if (Session["BookingInfo"] != null && Session[SiteConstants.SSN_GUEST_LOGIN] != null)
                            //{
                            //    bookingInfo = (BookingInfo)Session["BookingInfo"];
                            //    GLD = (GuestLoginDetails)Session[SiteConstants.SSN_GUEST_LOGIN];
                            //    GuestAdminID = GLD.GuestAdminID;
                            //    GuestID = GLD.GuestID;
                            //}
                            //else
                            //{

                            //    if (respcd == "0")
                            //    {
                            //        if (ResponseFields["MerchantRefNo"] != null)
                            //        {
                            //            bookingInfo = new BookingInfo();
                            //            DataTable dtLocationRoom = (DataTable)HttpContext.Current.Session["EnquiryLocationRoom"];
                            //            if (dtLocationRoom == null)
                            //            {
                            //                dtLocationRoom = new DataTable();
                            //                dtLocationRoom = DataHandlingOnline.CreateLocationRoomTable(dtLocationRoom);
                            //                HttpContext.Current.Session["EnquiryLocationRoom"] = dtLocationRoom;
                            //            }

                            //            BookingDataOnline.FetchRecord(Convert.ToInt32(ResponseFields["MerchantRefNo"]), bookingInfo, "G", ref dtLocationRoom);
                            //            HttpContext.Current.Session["EnquiryLocationRoom"] = dtLocationRoom;

                            //            Session["BookingInfo"] = bookingInfo;

                            //            if (bookingInfo.enquiryMaster.EnquiryTokenID <= 0)
                            //            {
                            //                Response.Redirect("WelcomeGuest.aspx", false);
                            //            }
                            //            else
                            //            {
                            //                DataTable GetLoginDetails = DBHandler.GetResult("Get_LoginDetailsByEnquiryTokenID", bookingInfo.enquiryMaster.EnquiryTokenID);
                            //                if (GetLoginDetails.Rows.Count > 0)
                            //                {
                            //                    if (GetLoginDetails.Rows[0]["Status"].ToString() == "1")
                            //                    {
                            //                        GLD = new GuestLoginDetails();
                            //                        GLD.GuestAdminID = (int)GetLoginDetails.Rows[0]["Admin_ID"];
                            //                        GLD.GuestID = Convert.ToInt32(GetLoginDetails.Rows[0]["GuestID"]);
                            //                        GLD.GuestUserID = GetLoginDetails.Rows[0]["User_ID"].ToString();
                            //                        GLD.Name = GetLoginDetails.Rows[0]["Name"].ToString();
                            //                        GLD.Address = GetLoginDetails.Rows[0]["Address"].ToString();
                            //                        GLD.City = GetLoginDetails.Rows[0]["City"].ToString();
                            //                        GLD.State = GetLoginDetails.Rows[0]["State"].ToString();
                            //                        GLD.Country = GetLoginDetails.Rows[0]["Country"].ToString();
                            //                        GLD.Email = GetLoginDetails.Rows[0]["Email"].ToString();
                            //                        GLD.Mobile = GetLoginDetails.Rows[0]["Mobile"].ToString();
                            //                        GLD.Status = "Y";
                            //                        GLD.Message = "Successfully Logged In. Click Next to Proceed";
                            //                        HttpContext.Current.Session[SiteConstants.SSN_GUEST_LOGIN] = GLD;

                            //                        GuestAdminID = GLD.GuestAdminID;
                            //                        GuestID = GLD.GuestID;

                            //                    }
                            //                }
                            //            }
                            //        }
                            //        else
                            //        {
                            //            Response.Redirect("WelcomeGuest.aspx", false);
                            //        }

                            //    }
                            //    else
                            //    {
                            //        Response.Redirect("WelcomeGuest.aspx", false);
                            //    }
                            //}

                            #endregion

                            SqlCommand cmd = new SqlCommand("Insert_PgHDFC_OnlineTransactionResponse", cn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@ResponseCode", respcd);
                            cmd.Parameters.AddWithValue("@ResponseMessage", respmsg);
                            cmd.Parameters.AddWithValue("@DateCreated", ResponseFields["DateCreated"] == null ? DBNull.Value : (object)ResponseFields["DateCreated"]);
                            cmd.Parameters.AddWithValue("@PaymentID", ResponseFields["PaymentID"] == null ? DBNull.Value : (object)ResponseFields["PaymentID"]);
                            cmd.Parameters.AddWithValue("@MerchantRefNo", ResponseFields["MerchantRefNo"] == null ? DBNull.Value : (object)ResponseFields["MerchantRefNo"]); //--txnID / CustomarID
                            cmd.Parameters.AddWithValue("@Amount", ResponseFields["Amount"] == null ? DBNull.Value : (object)ResponseFields["Amount"]); //--net amount
                            cmd.Parameters.AddWithValue("@Mode", ResponseFields["Mode"] == null ? DBNull.Value : (object)ResponseFields["Mode"]);
                            cmd.Parameters.AddWithValue("@BillingName", ResponseFields["BillingName"] == null ? DBNull.Value : (object)ResponseFields["BillingName"]); //--assesseeName
                            cmd.Parameters.AddWithValue("@BillingAddress", ResponseFields["BillingAddress"] == null ? DBNull.Value : (object)ResponseFields["BillingAddress"]); //--HoldingNo
                            cmd.Parameters.AddWithValue("@BillingCity", ResponseFields["BillingCity"] == null ? DBNull.Value : (object)ResponseFields["BillingCity"]); //--city
                            cmd.Parameters.AddWithValue("@BillingState", ResponseFields["BillingState"] == null ? DBNull.Value : (object)ResponseFields["BillingState"]); //--municipalityID

                            cmd.Parameters.AddWithValue("@BillingPostalCode", ResponseFields["BillingPostalCode"] == null ? DBNull.Value : (object)ResponseFields["BillingPostalCode"]); //--LocationID
                            cmd.Parameters.AddWithValue("@BillingCountry", ResponseFields["BillingCountry"] == null ? DBNull.Value : (object)ResponseFields["BillingCountry"]);
                            cmd.Parameters.AddWithValue("@BillingPhone", ResponseFields["BillingPhone"] == null ? DBNull.Value : (object)ResponseFields["BillingPhone"]);
                            cmd.Parameters.AddWithValue("@BillingEmail", ResponseFields["BillingEmail"] == null ? DBNull.Value : (object)ResponseFields["BillingEmail"]);
                            cmd.Parameters.AddWithValue("@DeliveryName", ResponseFields["DeliveryName"] == null ? DBNull.Value : (object)ResponseFields["DeliveryName"]); //--userID
                            cmd.Parameters.AddWithValue("@DeliveryAddress", ResponseFields["DeliveryAddress"] == null ? DBNull.Value : (object)ResponseFields["DeliveryAddress"]); //--PaymentID
                            cmd.Parameters.AddWithValue("@DeliveryCity", ResponseFields["DeliveryCity"] == null ? DBNull.Value : (object)ResponseFields["DeliveryCity"]);
                            cmd.Parameters.AddWithValue("@DeliveryState", ResponseFields["DeliveryState"] == null ? DBNull.Value : (object)ResponseFields["DeliveryState"]);
                            cmd.Parameters.AddWithValue("@DeliveryPostalCode", ResponseFields["DeliveryPostalCode"] == null ? DBNull.Value : (object)ResponseFields["DeliveryPostalCode"]);
                            cmd.Parameters.AddWithValue("@DeliveryCountry", ResponseFields["DeliveryCountry"] == null ? DBNull.Value : (object)ResponseFields["DeliveryCountry"]);
                            cmd.Parameters.AddWithValue("@DeliveryPhone", ResponseFields["DeliveryPhone"] == null ? DBNull.Value : (object)ResponseFields["DeliveryPhone"]);
                            cmd.Parameters.AddWithValue("@IsFlagged", ResponseFields["IsFlagged"] == null ? DBNull.Value : (object)ResponseFields["IsFlagged"]);
                            cmd.Parameters.AddWithValue("@TransactionID", ResponseFields["TransactionID"] == null ? DBNull.Value : (object)ResponseFields["TransactionID"]);
                            cmd.Parameters.AddWithValue("@SecureHash", ResponseFields["SecureHash"] == null ? DBNull.Value : (object)ResponseFields["SecureHash"]);
                            cmd.Parameters.AddWithValue("@CalculatedSecureHash", HashedValue == null ? DBNull.Value : (object)HashedValue);
                            cmd.Parameters.AddWithValue("@AccountID", ResponseFields["AccountID"] == null ? DBNull.Value : (object)ResponseFields["AccountID"]);
                            cmd.Parameters.AddWithValue("@Description", ResponseFields["Description"] == null ? DBNull.Value : (object)ResponseFields["Description"]);
                            cmd.Parameters.AddWithValue("@RequestID", ResponseFields["RequestID"] == null ? DBNull.Value : (object)ResponseFields["RequestID"]);
                            cmd.Parameters.AddWithValue("@PaymentMethod", ResponseFields["PaymentMethod"] == null ? DBNull.Value : (object)ResponseFields["PaymentMethod"]);
                            cmd.Parameters.AddWithValue("@ApplicationUserID", (ResponseFields["DeliveryName"] == null ? DBNull.Value : (object)ResponseFields["DeliveryName"]));
                            cmd.Parameters.AddWithValue("@TransationType", "P");
                            cmd.Parameters.AddWithValue("@ResponseMode", "BrowserToServer");
                            cmd.Parameters.AddWithValue("@UserType", 1);
                            cn.Open();
                            DataTable dtResponse = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dtResponse);


                            if (respcd == "0")
                            {
                                if (dtResponse.Rows.Count > 0)
                                {
                                    if (dtResponse.Rows[0].Field<bool>("IsPaymentSuccessfull") == true)
                                    {
                                        //enquiry id to be replaced with response mtransid 
                                        var successObj = new { PaymentID = dtResponse.Rows[0].Field<long>("PaymentID"), TransactionID = ResponseFields["PaymentID"], Message = respmsg };

                                        string json = js.Serialize(successObj);
                                        return string.Format("/online/onlinepayment/PaymentSuccessfull?Data={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(json)));
                                    }
                                    else
                                    {
                                        throw new InvalidOperationException("Payment not successfull");
                                    }

                                }
                                else
                                {
                                    throw new InvalidOperationException("Payment not successfull");
                                }

                            }
                            else //(respcd != "0")
                            {
                                throw new InvalidOperationException(string.Format("Payment not successfull. Response Code: {0},Transaction ID: {1}, Message: {2}", respcd, ResponseFields["PaymentID"], respmsg));
                            }

                        }
                        else
                        {
                            throw new InvalidOperationException("Payment not successfull! Empty Payment Response");
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException(string.Format("RespCode:{0}, Message=Problem in parsing payment gatewway checksum response", respcd));
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid response method[http]");
                }


            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("Database error: Database faild to store payment getway response. Details: {0}", ex.Message));
            }
            finally
            {
                cn.Close();
            }

        }
        public string SaveResponseNRedirect_CCAVNW()
        {

            HttpRequest Request = HttpContext.Current.Request;
            HttpResponse Response = HttpContext.Current.Response;
            try
            {
                //string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
                if (Request.Form["encResp"] != null)
                {
                    string orderno = Request.Form["orderNo"];
                    var info = GetWorkingKeyByOrderId(orderno);
                    if (info==null)
                    {
                        throw new NullReferenceException("Working key not found respected to Order no");
                    }
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], info.WorkingKey);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("=======Response Parameter==========");
                    for (int i = 0; i < Params.Count; i++)
                    {
                        System.Diagnostics.Debug.WriteLine(string.Format("{0}=>{1}", Params.Keys[i], Params[i]));
                    }
                    HdfcAccountInfo_BLL bll = new HdfcAccountInfo_BLL(info.MunicipalityId);
                    HdfcAccountBase hdfcAccountInfo = bll.GetAccountInfo("HC");
                    if (hdfcAccountInfo == null || hdfcAccountInfo.PGCode != "HC")
                    {
                        throw new InvalidOperationException("Response Field Malformed or Tergate Municipality does not have HDFC Account Information");
                    }

                    SqlCommand cmd = new SqlCommand("Insert_PgHDFC_OnlineTransactionResponse_CC_Avenew", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //bind parameters
                    for (int i = 0; i < Params.Count; i++)
                    {
                        //Response.Write(Params.Keys[i] + " = " + Params[i] + "<br>");
                        cmd.Parameters.AddWithValue(string.Format("@{0}", Params.Keys[i]), Params[i].ReplaceDbNull());
                    }

                    cmd.Parameters.AddWithValue("@UserType", 1);
                    cn.Open();
                    DataTable dtResponse = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dtResponse);


                    if (Params["response_code"] == "0")
                    {
                        if (dtResponse.Rows.Count > 0)
                        {
                            if (dtResponse.Rows[0].Field<bool>("IsPaymentSuccessfull") == true)
                            {
                                //enquiry id to be replaced with response mtransid 
                                var successObj = new { PaymentID = dtResponse.Rows[0].Field<long>("PaymentID"), TransactionID = Params["bank_ref_no"], Message = "Payment Successfully Captured" };

                                string json = js.Serialize(successObj);
                                return string.Format("/online/onlinepayment/PaymentSuccessfull?Data={0}", HttpUtility.UrlEncode(chiperAlgorithm.Encrypt(json)));
                            }
                            else
                            {
                                throw new InvalidOperationException("Payment not successfull");
                            }

                        }
                        else
                        {
                            throw new InvalidOperationException("Payment not successfull");
                        }

                    }
                    else //(respcd != "0")
                    {
                        throw new InvalidOperationException(string.Format("Payment not successfull. Response Code: {0},Transaction ID: {1}, Message: {2}", Params["response_code"], Params["bank_ref_no"], Params["failure_message"]));
                    }

                }
                else
                {
                    throw new InvalidOperationException("Payment not successfull! Empty Payment Response");
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("Database error: Database faild to store payment getway response. Details: {0}", ex.Message));
            }
            finally
            {
                cn.Close();
            }

        }
        public void SaveResponseErrorResponseManuallyCCAvenue(string orderNo,string _encResponse)
        {
            try
            {
                if (string.IsNullOrEmpty(_encResponse) || string.IsNullOrEmpty(orderNo))
                {
                    throw new NullReferenceException("Order No or Response is null");
                }
                else
                {
                    {
                        string orderno = orderNo;
                        var info = GetWorkingKeyByOrderId(orderno);
                        if (info == null)
                        {
                            throw new NullReferenceException("Working key not found respected to Order no");
                        }
                        CCACrypto ccaCrypto = new CCACrypto();
                        string encResponse = ccaCrypto.Decrypt(_encResponse, info.WorkingKey);
                        NameValueCollection Params = new NameValueCollection();
                        string[] segments = encResponse.Split('&');
                        foreach (string seg in segments)
                        {
                            string[] parts = seg.Split('=');
                            if (parts.Length > 0)
                            {
                                string Key = parts[0].Trim();
                                string Value = parts[1].Trim();
                                Params.Add(Key, Value);
                            }
                        }
                        System.Diagnostics.Debug.WriteLine("=======Response Parameter==========");
                        for (int i = 0; i < Params.Count; i++)
                        {
                            System.Diagnostics.Debug.WriteLine(string.Format("{0}=>{1}", Params.Keys[i], Params[i]));
                        }
                        HdfcAccountInfo_BLL bll = new HdfcAccountInfo_BLL(info.MunicipalityId);
                        HdfcAccountBase hdfcAccountInfo = bll.GetAccountInfo("HC");
                        if (hdfcAccountInfo == null || hdfcAccountInfo.PGCode != "HC")
                        {
                            throw new InvalidOperationException("Response Field Malformed or Tergate Municipality does not have HDFC Account Information");
                        }
                        SqlCommand cmd = new SqlCommand("Insert_PgHDFC_ErrorOnlineTransactionResponseManually_CC_Avenew", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //bind parameters
                        for (int i = 0; i < Params.Count; i++)
                        {
                            //Response.Write(Params.Keys[i] + " = " + Params[i] + "<br>");
                            cmd.Parameters.AddWithValue(string.Format("@{0}", Params.Keys[i]), Params[i].ReplaceDbNull());
                        }

                        cmd.Parameters.AddWithValue("@UserType", 1);
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }catch (SqlException)
            {
                throw new Exception("Can not log payment cancel information due to database error");
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            }

        #region private methods
        string GetParamHashValueHDFC(NameValueCollection FormField, string algo, HdfcAccountInfo hdfcAccount)
        {
            string hashedvalue = "";
            try
            {
                string skey = hdfcAccount.SecreteKey;
                string hashValue = skey;
                //string V3URL = "https://secure.ebs.in/pg/ma/payment/request";
                string HashData = hashValue;
                SortedDictionary<string, string> sortedDict = SortCommonHDFC.SortNameValueCollection(FormField);

                foreach (KeyValuePair<string, string> p in sortedDict)
                {
                    if (p.Value != null && p.Value.ToString().Trim().ToLower() != "null" && p.Value.ToString().Length > 0 && p.Key.ToString().ToLower() != "secretkey" && p.Key.ToString() != "submitted" && p.Key.ToString() != "SecureHash" && !p.Key.ToString().ToLower().StartsWith("__"))
                    {
                        HashData += "|" + p.Value.ToString();
                    }
                }

                System.Diagnostics.Debug.WriteLine(string.Format("HashedData=>{0}", HashData));

                if (!string.IsNullOrEmpty(HashData) && HashData.Length > 0)
                {

                    hashedvalue = CryptoHDFC.GenerateHashString(HashData, SortCommonHDFC.GetConfigAlgorithm(hdfcAccount), CryptoHDFC.EncodingType.HEX).ToUpper();
                    //hashedvalue = HashData;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return hashedvalue;
        }
        static string GetMD5Hash(string name)
        {

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] ba = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(name));
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        private class HdfcAccountInfo_BLL
        {
            int MunicipalityID = 0;
            string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            SqlConnection cn;
            public HdfcAccountInfo_BLL(int municipalityID)
            {
                this.MunicipalityID = municipalityID;
                cn = new SqlConnection(conString);
            }
            public HdfcAccountBase GetAccountInfo(string type)
            {
                SqlCommand cmd = new SqlCommand("Get_PGAccountInfoHDFC", cn);
                cmd.Parameters.AddWithValue("@MunicipalityID", MunicipalityID);
                cmd.Parameters.AddWithValue("@Type", type);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    da.Fill(dt);
                    var objs = Convert(dt);
                    if (objs.Count > 0)
                    {
                        return objs[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
            List<HdfcAccountBase> Convert(DataTable dt)
            {
                List<HdfcAccountBase> l = new List<HdfcAccountBase>();
                foreach (DataRow dr in dt.Rows)
                {
                    if (string.Equals(dr.Field<string>("PG_Code"), "HF", StringComparison.OrdinalIgnoreCase))
                    {
                        HdfcAccountInfo a = new HdfcAccountInfo();
                        a.MappingID = dr.Field<int>("PgMunicipalityMapID");
                        a.MuncipalityID = dr.Field<int>("MunicipalityID");
                        a.PaymentUrl = dr.Field<string>("PAYMENT_URL");
                        a.RefundUrl = dr.Field<string>("Refund_URL");
                        a.IsActive = dr.Field<bool>("IsActive");
                        a.ResposeUrl = dr.Field<string>("ResponseUrl");
                        a.PGCode = dr.Field<string>("PG_Code");
                        a.AccountID = dr.Field<string>("ACCOUNT_ID");
                        a.Algorithm = dr.Field<string>("AlgorithmHDFC");
                        a.SecreteKey = dr.Field<string>("SECRET_KEY");
                        a.CancelUrl = dr.Field<string>("Cancel_URL");
                        l.Add(a);
                    }
                    else if (string.Equals(dr.Field<string>("PG_Code"), "HC", StringComparison.OrdinalIgnoreCase))
                    {
                        HdfcAccountInfo_CCAVNW a = new HdfcAccountInfo_CCAVNW();
                        a.MappingID = dr.Field<int>("PgMunicipalityMapID");
                        a.MuncipalityID = dr.Field<int>("MunicipalityID");
                        a.PaymentUrl = dr.Field<string>("PAYMENT_URL");
                        a.RefundUrl = dr.Field<string>("Refund_URL");
                        a.IsActive = dr.Field<bool>("IsActive");
                        a.ResposeUrl = dr.Field<string>("ResponseUrl");
                        a.PGCode = dr.Field<string>("PG_Code");
                        a.MarchentId = dr.Field<string>("MerchantId");
                        a.WorkingKey = dr.Field<string>("WorkingKey");
                        a.AccessCode = dr.Field<string>("AccessCode");
                        a.CancelUrl = dr.Field<string>("Cancel_URL");
                        l.Add(a);
                    }
                }
                return l;
            }
            /// <summary>
            /// this is for CC Avenew
            /// </summary>
            /// <param name="orderID"></param>
            /// <returns></returns>


        }
        #endregion
        public HDFCModels.CC_001 GetWorkingKeyByOrderId(string orderNo)
        {
            try
            {
                cn.Open();
                var cmd = new SqlCommand("Get_HDFC_WorkingKeyByOrderNo_CCAvenew", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNo", orderNo);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                  return  dt.AsEnumerable().Select(t =>
                    {
                        var obj= new HDFCModels.CC_001();
                        obj.WorkingKey = t.Field<string>("WorkingKey");
                        obj.MunicipalityId = t.Field<int>("MunicipalityId");
                        return obj;
                    }).ToList()[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}