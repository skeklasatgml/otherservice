﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace wbEcsc.Views.OpenReport
{
    public partial class OpenReport : System.Web.UI.Page
    {
        ExportFormatType formatType = ExportFormatType.NoFormat;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        //string conString_Inventory = ConfigurationManager.ConnectionStrings["constring_inventory"].ConnectionString;
        //string constring_sbstc_hr = ConfigurationManager.ConnectionStrings["constring_sbstc_hr"].ConnectionString;

        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        Dictionary<string, string> Params;

        protected void Page_Load(object sender, EventArgs e)
        {

            var keys = Request.QueryString["ReportName"];
            if (keys != null)
            {
                JObject obj = JObject.Parse(keys);
                var master = (JArray)obj.SelectToken("Master");
                var detal = (JArray)obj.SelectToken("Detail");

                GenerateReport(master, detal);
            }
        }
        private void GenerateReport(JArray master, JArray detal)
        {
            string server = "", database = "", userid = "", password = "";
            try
            {
                string reportName = master[0]["ReportName"].ToString();
                string FileName = master[0]["FileName"].ToString();
                string Database = master[0]["Database"].ToString();

                //return string.Format("~/Reporting/Viewer/ReportViewer.aspx?ReportCode={0}&Params={1}", ReportID, _params);
                crystalReport.Load(Server.MapPath("~/Reporting/Reports/" + reportName));
                crystalReport.Refresh();
                
              
                    builder.ConnectionString = conString;

                server = builder.DataSource;
                database = builder.InitialCatalog;
                userid = builder.UserID;
                password = builder.Password;

                //server = Connection_Details.ServerName();
                //database = Connection_Details.DatabaseName();
                //userid = Connection_Details.UserID();
                //password = Connection_Details.Password();

                crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(server, database, userid, password);

                for (int i = 0; i < crystalReport.Subreports.Count; i++)
                {
                    crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
                    crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
                    crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);
                }

                //crystalReport.VerifyDatabase();

                foreach (var item in detal)
                {
                    string a = item.ToString();
                    string noNewLines = a.Replace("\n", "");
                    Params = JsonConvert.DeserializeObject<Dictionary<string, string>>(noNewLines);
                }

                foreach (KeyValuePair<string, string> entry in Params)
                {
                    crystalReport.SetParameterValue("@" + entry.Key, entry.Value);
                }
               

                formatType = ExportFormatType.PortableDocFormat;
                crystalReport.ExportToHttpResponse(formatType, Response, false, FileName);
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
            finally
            {
                crystalReport.Close();
                crystalReport.Dispose();
                GC.Collect();
            }
        }
    }
}