﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.Logout = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Home/Logout",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var controls = {
            LogoutLink: {
                Control: $(".logout"),
                Id: null,
                Class: "logout"
            }
        };
        var LogIn = function () {
            throw "Function Not Implemented";
        };
        var Logout = function () {
            serverObject.Logout(function (response) {
                if (response.Status == 200) {
                    location.href = response.Data.RedirectUrl;
                } else {
                    alert("Error: cannot logout");
                }
            });
        };
        var BindEvent = function () {
            controls.LogoutLink.Control.off('click');
            controls.LogoutLink.Control.on('click', Logout);
        };
        (function () {
            BindEvent();
        }());
    };
    var INIT_PAGE = function () {
        var serverObject = new SERVER_OBJECT();
        var app_object = new APP_OBJECT(serverObject);
    };
    (function () {
        INIT_PAGE();
    }());
});