﻿$(document).ready(function () {
    var ScOrTCdoc = null;
    $(document).on("click", ".btn-search", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData();
        console.log(configureDetails);
        if (app.ConfigureForm().validate()) {
            var payload = {
                SurrenderInsAmt: app.ConfigureForm().getFormData()
            };
            $.ajax({
                type: "POST",
                url: "/Modules/DprExtension/GetProjectInstallmentDetails",
                data: JSON.stringify(payload.SurrenderInsAmt),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (Response) {
                    if (Response.Status === 200) {
                        app.bindTable(Response);
                    } else {
                        alertify.alert("Error!", Response.Message, function () { });
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }
    });
    $(document).on('change', '.ScOrTC', function () {
        ScOrTCdoc = {};
        if (this.files.length > 0) {
            window.StageFileByFormData(this, function (response) {
                ScOrTCdoc.StageId = response.Data;
                ScOrTCdoc.Result = null;
                ScOrTCdoc.Name = null;
                alertify.success(response.Message);
            }, function (response) {
                alertify.error(response.Message);
                $(this).val('');
            });
        }
    });
    $(document).on("click", ".ApplyReClame", function () {
        var tr = $(this).closest('tr');
        var payload = {
            SurrenderInsAmt: {
                InstallmentSubProjectTabId: tr.attr("data-InstallmentSubProjectTabId"),
                FundReleasedTabId: tr.attr("data-FundReleasedTabId"),
                SurrenderAmt: parseFloat(tr.find('.surrenderamt').val()),
                InstAmt: parseFloat(tr.attr('data-inst-amt')),
                ScOrTCdoc: ScOrTCdoc
            }
        };
        console.log(payload);
        if (payload.SurrenderInsAmt.SurrenderAmt > payload.SurrenderInsAmt.InstAmt) {
            alertify.error("Surrender Amount can not exceed Installment Amount.");
            return false;
        }
        if (payload.SurrenderInsAmt.ScOrTCdoc != null) {
            $.ajax({
                type: "POST",
                url: "/Modules/DprExtension/SaveReClameAmount",
                data: JSON.stringify(payload.SurrenderInsAmt),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (Response) {
                    if (Response.Status === 200) {
                        alertify.alert("Successfully Applied.");
                        location.reload();
                    } else {
                        alertify.alert("Error!", Response.Message, function () { });
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }
        else {
            alertify.error("Please select File.");
            return false;
        }

    });
});
var app = {
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#surrenderAmt"),
                txtProjNo: $(".projectNo"),
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().txtProjNo.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[_doms().txtProjNo.prop('name')] = { required: "Please Enter Project No" };
                //vldObj.messages[_doms().ddlMuni.prop('name')] = { required: "Select Municipality" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }
                }
                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                return {
                    ProjectNo: _doms().txtProjNo.val(),
                };
            }
        }
    },
    bindTable: function (result) {
        if (result.Data != null) {
            $('#myTable').find('>tbody').empty();
            for (i = 0; i < result.Data.length; i++) {
                var tempJson = result.Data[i];

                var tr = $('<tr data-inst-amt=' + (tempJson.InstAmt + tempJson.SurrenderAmt) + ' data-InstallmentSubProjectTabId=' + tempJson.InstallmentSubProjectTabId + ' data-FundReleasedTabId=' + tempJson.FundReleasedTabId + '/>');
                var td = $('<td />');
                tr.append(td.append(i + 1));

                var td = $('<td class="sub-proj-name" />');
                tr.append(td.append(tempJson.SubProjName));

                var td = $('<td class="inst-no" />');
                tr.append(td.append(tempJson.InstNo));

                var td = $('<td class="inst-amt" />');
                tr.append(td.append(tempJson.InstAmt + tempJson.SurrenderAmt));

                td = $('<td/>');
                tr.append(td.append(tempJson.SurrenderAmt));

                var td = $('<td class="inst-amt" />');
                tr.append(td.append(tempJson.InstAmt));

                var subst = tempJson.InstNo != null ? tempJson.InstNo.substring(0, 8) : tempJson.InstNo;
                if (subst != "Reclaim") {
                    td = $('<td/>');
                    tr.append(td.append('<input type="text" class="surrenderamt" />'));

                    td = $('<td/>');
                    tr.append(td.append('<input type="file" accept="application/pdf" class="ScOrTC" />'));

                    td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                    tr.append(td.append('<a class="ApplyReClame"><input type="hidden"  class="hdnJSN" value=' + 0 + '/>Apply</a>'));
                }
                else {
                    td = $('<td/>');
                    tr.append(td.append('<input type="text" disabled class="surrenderamtdisabled" />'));

                    td = $('<td/>');
                    tr.append(td.append('<input type="file" disabled accept="application/pdf" class="ScOrTC" />'));

                    td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                    tr.append(td.append('<input type="hidden"  class="hdnJSN" value=' + 0 + '/>'));
                }
                

                $('#myTable').find('>tbody').append(tr);
            }
        }
    },
}