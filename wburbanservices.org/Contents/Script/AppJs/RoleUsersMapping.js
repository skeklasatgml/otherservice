﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.Insert = function (data, callback) {
            console.log(data);
            $.ajax({
                type: "POST",
                url: "/Municipality/MnRoleUsersMapping/Insert",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.Get_Roles = function (callback) {
            $.ajax({
                type: "GET",
                url: "/Municipality/MnRoleUsersMapping/Get_Roles",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.UsersByRoleID = function (RoleID, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnRoleUsersMapping/Get_UsersByRoleID",
                data: "{RoleID:"+RoleID+"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };    
        this.UsersByMunicipalityID = function (callback) {
            $.ajax({
                type: "post",
                contentType: 'application/json;charset=utf-8',
                data: null,
                url: "/Municipality/MnUserCounterAssociate/GetUserByMunicipalityID",
                dataType: "json",
                success: function (response) {
                    //  alert(response);
                    callback(response);
                },
                failure: function (response) { alert(response.d); }
            })
        };
    };
    var APP_OBJECT = function (serverObject) {
        var RoleDataSource = [];
        var UsersDataSource = [];
        var UsersDataSourceByRoleID = [];        
        var ctrl_ddlRole = $("#ddlRole");     
        var cntr_divcheckboxcollection = $("#divcheckboxcollection");      
     //   var cntr_UserClass = "";
     //   var RoleUsers = [];        
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $("#btnCancel").on('click', ResetWindow);
            $("#ddlRole").on('change', RoleChange);
        };
        var ResetWindow = function () {
            location.reload(true);
        };
        var getDataFromView = function () {           
            var RoleID = $("#ddlRole").val();
            RoleUsers = [];
            //var getRole = function () {
            //    return [{
            //        RoleID: RoleID,
            //        RoleName: "",
            //        ControllerID: null,
            //        Users: [],
            //        Actions: []
            //    }];
            //};
            $.each($(".chckUsersMethod"), function () {
                if (this.checked) {
                    var chck = {
                        RoleID: RoleID,
                        UserID: this.value,                       
                        RoleName:'',
                        UserName:''
                    };
                    RoleUsers.push(chck);
                }
            })

            return RoleUsers;
        };
        var feedToForm = function (data) {
            ctrl_hdnMappingID.val(data.MappingID);
            ctrl_ddlRole.find("option[value='" + data.RoleID + "']").prop('selected', true);
            ctrl_ddlController.find("option[value='" + data.ControllerID + "']").prop('selected', true);
            serverObject.Action(data.ControllerID, function (response) {
                ActionDataSource = response.Data;
                PopulateActionData();
                ctrl_ddlAction.find("option[value='" + data.ActionID + "']").prop('selected', true);
            });
        };
        var PopulateRoleData = function () {
            ctrl_ddlRole.empty();
            ctrl_ddlRole.append("<option value='0'> select Role </option>");
            $.each(RoleDataSource, function (index, value) {
                var label = value.RoleName;
                var id = value.RoleID;
                ctrl_ddlRole.append("<option value='" + id + "'>" + label + "</option>");
            });
        };
        var PopulateUserData = function () {
            cntr_divcheckboxcollection.empty();
            $.each(UsersDataSource, function (index, value) {
                //  var label = value.FirstName +" "+ value.LastName +' ( '+ value.PhoneNo+' )';
                var label = value.UserName;
                var id = value.UserID;
                cntr_divcheckboxcollection.append("<label><input type='checkbox' class='chckUsersMethod' name='sports[]' value='" + id + "'/>  " + label + "</label><br/>");
            });
         //   cntr_UserClass = $(".chckUsersMethod");
        };        
        var CheckedUsersByRole = function () {
            if (UsersDataSourceByRoleID.length > 0) {
                $.each(UsersDataSourceByRoleID, function (index, value) {
                    var UserID = value.UserID;
                    $.each($(".chckUsersMethod"), function () {
                        if (this.value == UserID) {
                            this.checked = true;
                        }
                    })
                })
            }
        };
        var Validate = function (data) {           
            if (ctrl_ddlRole.val() == 0) {
                alert("Select a Role");
                ctrl_ddlRole.focus();
                return false;
            }
            if (data.length == 0) {
                alert("Select atleast one User !!");
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.Insert(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert('Selected record Mapping successfully!!')                       
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        var RoleChange = function () {            
            $.each($(".chckUsersMethod"), function () {
                this.checked = false;
            });
            if (ctrl_ddlRole.val() != "") {
                serverObject.UsersByRoleID(ctrl_ddlRole.val(), function (response) {
                    UsersDataSourceByRoleID = response.Data;
                    console.log(UsersDataSourceByRoleID);
                    CheckedUsersByRole();
                });
            }
        };      
        this.app_Initiate = function () {
            BindEvent();
            serverObject.Get_Roles(function (response) {
                RoleDataSource = response.Data;
                PopulateRoleData();
            });           
            serverObject.UsersByMunicipalityID(function (response) {
                UsersDataSource = response.Data;
                PopulateUserData();
            });
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});