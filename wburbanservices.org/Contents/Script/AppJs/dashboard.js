$(function () {
    'use strict';
    var currencyFormatter = OSREC.CurrencyFormatter;
    var formatCurrency = function (amount) {
        return currencyFormatter.format(amount, { currency: 'INR' });
    };
    var ajax = function (method, url, data, successCallbak) {
        var _method = 'post';
        var _url = url;
        var _data = null;
        if (method) {
            _method = method;
        }
        if (data) {
            _data = data;
        }
        $.ajax({
            type: _method,
            url: _url,
            data: JSON.stringify(_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                successCallbak(response);
            },
            failure: function (response) {
                alert(response);
            }
        });
    };
    var preaparedControl = function (selector, container) {
        var obj = {
            _selector: selector,
            _obj: null
        };
        if (container) {
            var container = $(container);
            obj._obj = container.find(selector);
        }
        return obj;
    };
    var _scopeContainer = preaparedControl('.dash-board', 'body');
    var searchComponent = new function () { };
    var municipalityFilter = new function () {
        var _self = this;
        var scopeContainer = $("#FB26FE2E-4474-4FB8-8835-6606CE06E060");
        this.Modal = scopeContainer;
        this.show = function () {
            scopeContainer.modal('show');
        };
        this.hide = function () {
            scopeContainer.modal('hide');
        }
        var uncheckAll = function () {
            var checkBoxes = scopeContainer.find('table tbody tr').find('input[type="checkbox"]:checked');
            checkBoxes.prop('checked', false);
        };
        this.selectedMunicipalities = function (ids) {
            uncheckAll();
            var trs = scopeContainer.find('table tbody tr');
            $.each(ids, function (i, v) {
                var check = trs.find('input[data-MunicipalityId="' + v + '"]');
                if (check) {
                    check.prop('checked', true)
                }
            });
        };
        this.applyFilter = function (callback) {

        };
        (function () {
            scopeContainer.find(".btn-apply-filter").on('click', function () {
                if (_self.applyFilter) {
                    var ids = [];
                    var checkBoxes = scopeContainer.find('table tbody tr').find('input[type="checkbox"]:checked');
                    $.each(checkBoxes, function (i, v) {
                        ids.push(Number($(v).attr('data-MunicipalityId')));
                    });
                    _self.applyFilter({ MunicipalityIds: ids },_self);
                }
            });
        }())
    };

    var dillAssesseeComponent = new function () {
        var _self = this;
        var _curRoute = null;
        var _availableRoutes = null;
        var _routeHistory = new function () {
            var histories = {};
            this.add = function (routeObj) {
                //add new property
                histories[routeObj.key] = routeObj;
                _eventHandlers.onRouteHistoryAdded(routeObj);
            };
            this.removeLast = function () {
                //remove last added property
                var lastDeletable = this.getLast();
                var isDeleted = delete histories[Object.keys(histories)[Object.keys(histories).length - 1]];
                if (isDeleted && lastDeletable) {
                    _eventHandlers.onRouteHistoryRemoved(lastDeletable);
                }
            };
            this.getLast = function () {
                if (Object.keys(histories).length > 0) {
                    //return last added property's value
                    return histories[Object.keys(histories)[Object.keys(histories).length - 1]];
                } else {
                    return null;
                }
            };
            this.clearAll = function () {
                //re-initialize histories
                histories = {};
            };
            this.count = function () {
                //current history lenght
                return Object.keys(histories).length;
            };
            this.histories = function () {
                return histories;
            };
        };
        var _domControl = {
            _tabDrilldownAssessee: preaparedControl("#drilldown-assessee", _scopeContainer._obj),
            _tblAssesseeGrid: preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj),
            _btnDrillAssessee: preaparedControl(".btn-door-drill-assessee", _scopeContainer._obj),
            _ddlTimeSpan: preaparedControl(".ddl-time-span", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj),
            _cntrCustomDateRange: preaparedControl(".custom-date-range", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj),
            _txtFromDate: preaparedControl(".from-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _txtToDate: preaparedControl(".to-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _btnGetDetails: preaparedControl(".btn-get-details", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj),
            _lnkDrillAssesseeByMuncipality: preaparedControl(".lnk-drill-by-municipality", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _lnkdrillbymunicipalitymonthwise: preaparedControl(".lnk-drill-by-municipality-month-wise", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _lnknewlyaddedassesseesmonthyear: preaparedControl(".lnk-new-assessee-list-by-ward-month-year", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _lnkassesseebymunicipalitywardlocation: preaparedControl(".lnk-assessee-by-municipality-ward-location", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _lnkdrillbywardmonthwise: preaparedControl(".lnk-drill-by-ward-month-wise", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _lnkdrillbywardwise: preaparedControl(".lnk-drill-by-ward-wise", preaparedControl(".tbl-drill-assessee", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)._obj),
            _btnShowHistory: preaparedControl(".btn-show-history", preaparedControl("#drilldown-assessee", _scopeContainer._obj)._obj)
        };

        var getRouteValueTemplate = function (key) {
            var getDateRangeModel = function () {
                var txtfromDate = _domControl._txtFromDate._obj.val();
                var txttoDate = _domControl._txtToDate._obj.val();
                if (txtfromDate.split("/").length !== 3) {
                    throw "Invalid From Date: " + txtfromDate;
                }
                if (txttoDate.split("/").length !== 3) {
                    throw "Invalid To Date: " + txttoDate;
                }
                var fromDate = CONVERTER.Date.convertToDate(
                    txtfromDate.split("/")[2],
                    txtfromDate.split("/")[1],
                    txtfromDate.split("/")[0]
                );
                var toDate = CONVERTER.Date.convertToDate(
                    txttoDate.split("/")[2],
                    txttoDate.split("/")[1],
                    txttoDate.split("/")[0]
                );
                return {
                    FromDate: fromDate,
                    ToDate: toDate
                };
            };
            var obj = {};
            var dateRange = getDateRangeModel();
            obj["district-wise"] = function () {
                return {
                    FromDate: dateRange.FromDate,
                    ToDate: dateRange.ToDate
                };
            };
            obj["municipality-wise"] = function (districtId) {
                return {
                    DistrictId: districtId,
                    FromDate: dateRange.FromDate,
                    ToDate: dateRange.ToDate
                };
            };
            obj["municipality-month-wise"] = function (MunicipalityId) {
                return {
                    MunicipalityId: MunicipalityId,
                    FromDate: dateRange.FromDate,
                    ToDate: dateRange.ToDate
                };
            };
            obj["new-assessee-list-by-ward-month-year"] = function (MunicipalityId,WardId, MonthYear) {
                return {
                    MunicipalityId: MunicipalityId,
                    WardId: WardId,
                    MonthYear: MonthYear
                };
            };
            obj["assessee-by-municipality-ward-location"] = function (MunicipalityId, decisionKey) {
                return {
                    MunicipalityId: MunicipalityId,
                    Key: decisionKey
                };
            };
            obj["ward-wise"] = function (MunicipalityId) {
                return { MunicipalityId: MunicipalityId, FromDate: dateRange.FromDate, ToDate: dateRange.ToDate };
            };
            obj["ward-month-wise"] = function (MunicipalityId,WardId) {
                return { MunicipalityId: MunicipalityId, WardId: WardId, FromDate: dateRange.FromDate, ToDate: dateRange.ToDate };
            };

            return obj[key];
        };
        var prepareRoutes = function () {
            var getRouteModel = function (key, url, payload) {
                return {
                    key: key,
                    url: url,
                    payload: payload,
                    updatePayload: function () {
                       this.payload=  getRouteValueTemplate(this.key)();
                    }
                };
            };
            if (!_availableRoutes) {
                _availableRoutes = {};
            }

            _availableRoutes["district-wise"] = getRouteModel("district-wise",
                '/Administration/DashBoard/DrillAssesseGroupByDistricts',
                null);
            _availableRoutes["municipality-wise"] = getRouteModel("municipality-wise",
                "/Administration/DashBoard/DrillAssesseGroupByMunicipalities",
                null
            );
            _availableRoutes["ward-month-wise"] = getRouteModel("ward-month-wise", "/Administration/DashBoard/DrillAssesseByWardMonthWise", null);
            _availableRoutes["ward-wise"] = getRouteModel("ward-wise", "/Administration/DashBoard/DrillAssesseesByWard", null);
            _availableRoutes["new-assessee-list-by-ward-month-year"] = getRouteModel("new-assessee-list-by-ward-month-year", "/Administration/DashBoard/AssesseListByWardMonthYear", null);
            //_availableRoutes["assessee-by-municipality-ward-location"] = getRouteModel("assessee-by-municipality-ward-location", "/Administration/DashBoard/DrillAssesseesByWordLocation", null);;
        };
        var setBoxTitle = function (key, routeValue) {
            var setTitle = function (title) {
                _domControl._tabDrilldownAssessee._obj.find(".box-title").html(title);
            };

            switch (key) {
                case "ward-wise":
                    {
                        setBoxTitle("Assessees Info Ward Wise");
                        break;
                    }
                case "district-wise":
                    {
                        setTitle("Assessees Info District Wise");
                        break;
                    }
                case "municipality-wise":
                    {
                        setTitle("Assessees Info Municipality Wise");
                        break;
                    }
                case "ward-month-wise":
                    {
                        setTitle("Newly Added Assessees Ward & Monthly Details");
                        break;
                    }
                case "new-assessee-list-by-month-year":
                    {
                        setTitle("Newly Added Assessee List");
                        break;
                    }
                //case "assessee-by-municipality-ward-location":
                //    {
                //        if (routeValue.Key == "all") {
                //            setTitle("All Assessee List by Ward Location");
                //        } else if (routeValue.Key == "new") {
                //            setTitle("Newly Added Assessee List by Ward Location");
                //        } else {
                //            setTitle(key);
                //        }

                //        break;
                //    }
                default:
                    setTitle(key);
                    break;
            }
        };
        var setCurRoute = function (key, routeValue) {
            setBoxTitle(key, routeValue);
            _availableRoutes[key].payload = routeValue;
            _curRoute = _availableRoutes[key];
            if (_curRoute) {
                _routeHistory.add(_curRoute);
            }
        };
        //prepare datatable datasource
        var prepareDatasource = function (key, data, callback) {
            //sample datasource
            var preparedSource = {
                data: [],
                columns: []
            };
            switch (key) {
                case "ward-wise":
                    {
                        var cols = [{
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "OldAssesseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                            }, {
                                mDataProp: "TotalAssesseeCount"
                            }]; 

                        //assign cols name
                        var colDef = [{
                                aTargets: [0],
                                sTitle: "Ward"
                            },
                            {
                                aTargets: [1],
                                sTitle: "Assessees @ Initiation"
                            },
                            {
                                aTargets: [2],
                                sTitle: "New Assessees Added",
                                render: function (data, type, full, meta) {
                                    return '<a class="lnk-drill-by-ward-month-wise" data-ward-id="'+full.WardId+'" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                                }
                            }, {
                                aTargets: [3],
                                sTitle: "Total Assessees"
                            }
                        ];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                Ward: value.Ward,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAssesseeCount: value.TotalAssesseeCount - value.NewAssesseeCount,
                                WardId: value.WardId,
                                MunicipalityId: value.MunicipalityId
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "ward-month-wise":
                    {
                        var cols = [{
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                                mDataProp: "MonthYear"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Ward"
                        },
                        {
                            aTargets: [1],
                            sTitle: "New Assessees Added",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-new-assessee-list-by-ward-month-year" data-ward-id="' + full.WardId + '" data-month-year="' + full.MonthYear+'" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [2],
                            sTitle: "Month-Year"
                        }
                        ];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                Ward: value.Ward,
                                NewAssesseeCount: value.NewAssesseeCount,
                                MonthYear: value.MonthYear,
                                WardId: value.WardId,
                                MunicipalityId: value.MunicipalityId
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "district-wise":
                    {
                        var cols = [{
                            mDataProp: "DistrictName"
                        }, {
                            mDataProp: "OldAsseseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "TotalAssesseeCount"
                        }]; //District //

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "District",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-municipality" data-DistrictId="' + full.DistrictId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [1],
                            sTitle: "Assessees @ Initiation"
                        }, {
                            aTargets: [2],
                            sTitle: "New Assessees Added"
                        }, {
                            aTargets: [3],
                            sTitle: "Total Assessees"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                DistrictId: value.DistrictId,
                                DistrictName: value.DistrictName,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAsseseeCount: value.TotalAssesseeCount - value.NewAssesseeCount
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "municipality-wise":
                    {
                        var cols = [{
                            mDataProp: "MunicipalityName"
                        }, {
                            mDataProp: "OldAsseseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "TotalAssesseeCount"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Municipality",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-ward-wise" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [1],
                            sTitle: "Assessees @ Initiation"
                        }, {
                            aTargets: [2],
                            sTitle: "New Assessees Added",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-ward-wise" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [3],
                            sTitle: "Total Assessees"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                MunicipalityId: value.MunicipalityId,
                                MunicipalityName: value.MunicipalityName,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAsseseeCount: value.TotalAssesseeCount - value.NewAssesseeCount
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                
                case "new-assessee-list-by-ward-month-year":
                    {
                        var cols = [{
                            mDataProp: "AssesseeNo"
                        }, {
                            mDataProp: "AssesseeName"
                        }, {
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "Location"
                        }, {
                            mDataProp: "Holding"
                        }, {
                            mDataProp: "AssesseeAddress"
                        }, {
                            mDataProp: "PropertyTax"
                        }, {
                            mDataProp: "Surcharge"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Assessee No"
                        }, {
                            aTargets: [1],
                            sTitle: "Assessee Name"
                        }, {
                            aTargets: [2],
                            sTitle: "Ward"
                        }, {
                            aTargets: [3],
                            sTitle: "Location"
                        }, {
                            aTargets: [4],
                            sTitle: "Holding"
                        }, {
                            aTargets: [5],
                            sTitle: "Address"
                        }, {
                            aTargets: [6],
                            sTitle: "Property Tax"
                        }, {
                            aTargets: [7],
                            sTitle: "Surcharge"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                AssesseeID: value.AssesseeID,
                                AssesseeName: value.Name,
                                AssesseeNo: value.AssesseeNo,
                                Ward: value.Ward,
                                Location: value.Location,
                                Holding: value.HoldingNo,
                                AssesseeAddress: value.AssesseeAddress,
                                PropertyTax: value.PropertyTax,
                                Surcharge: value.Surcharge
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                default:
                    {
                        alertify.error("invalid route");
                    }
            }
        };
        var chartOperation = new function () {
            var chartDivCtrl = document.getElementById("assessee-drill-chartdiv");
            this.populateChart = function (args) {
                if (args) {
                    var key = args.route.key;
                    switch (key) {
                        case "ward-month-wise":
                            {
                                google.charts.load('current', { 'packages': ['corechart'] });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var chartData = [];
                                    chartData.push(['Month-Year', 'New Assessees']);
                                    $.each(
                                        args.tblData.aaData,
                                        function (index, value) {
                                            chartData.push([
                                                value.MonthYear,
                                                value.NewAssesseeCount
                                            ]);
                                        }
                                    );
                                    var data = google.visualization.arrayToDataTable(chartData);

                                    var options = {
                                        title: 'Newly Added Assessees Ward & Monthly Details',
                                        height: 400,
                                        width: '100%',
                                        is3D: true,
                                        pieSliceText: 'value',
                                    };

                                    var chart = new google.visualization.PieChart(chartDivCtrl);

                                    chart.draw(data, options);
                                }
                                break;
                            }
                        case "ward-wise":
                            {
                                google.charts.load("current", {
                                    packages: ["bar"]
                                });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var chartData = [];
                                    chartData.push([
                                        "Ward",
                                        "Old Assessees",
                                        "New Assessees"
                                    ]);
                                    $.each(
                                        args.tblData.aaData,
                                        function (index, value) {
                                            chartData.push([
                                                value.Ward,
                                                value.OldAssesseeCount,
                                                value.NewAssesseeCount
                                            ]);
                                        }
                                    );
                                    var data = google.visualization.arrayToDataTable(chartData);

                                    var options = {
                                        chart: {
                                            title: "Ward Wise Assessee Bar Chart",
                                            subtitle: "Ward Wise Assessee Bar Chart"
                                        },
                                        bars: "vertical",
                                        vAxis: {
                                            format: "decimal"
                                        },
                                        height: 400,
                                        width: '100%',
                                        colors: ["#1b9e77", "#d95f02", "#7570b3"]
                                    };

                                    var chart = new google.charts.Bar(chartDivCtrl);
                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                }
                                break;
                                break;
                            }
                        case "district-wise":
                            {
                                google.charts.load("current", {
                                    packages: ["bar"]
                                });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var chartData = [];
                                    chartData.push([
                                        "District",
                                        "Old Assessees",
                                        "New Assessees"
                                    ]);
                                    $.each(
                                        args.tblData.aaData,
                                        function (index, value) {
                                            chartData.push([
                                                value.DistrictName,
                                                value.OldAsseseeCount,
                                                value.NewAssesseeCount
                                            ]);
                                        }
                                    );
                                    var data = google.visualization.arrayToDataTable(chartData);

                                    var options = {
                                        chart: {
                                            title: "District Wise Assessee Bar Chart",
                                            subtitle: "District Wise Assessee Bar Chart"
                                        },
                                        bars: "vertical",
                                        vAxis: {
                                            format: "decimal"
                                        },
                                        height: 400,
                                        width: '100%',
                                        colors: ["#1b9e77", "#d95f02", "#7570b3"]
                                    };

                                    var chart = new google.charts.Bar(chartDivCtrl);
                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                }
                                break;
                            }
                        case "municipality-wise":
                            {
                                google.charts.load("current", {
                                    packages: ["bar"]
                                });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var chartData = [];
                                    chartData.push([
                                        "Municipality",
                                        "Old Assessees",
                                        "New Assessees"
                                    ]);
                                    $.each(
                                        args.tblData.aaData,
                                        function (index, value) {
                                            chartData.push([
                                                value.MunicipalityName,
                                                value.OldAsseseeCount,
                                                value.NewAssesseeCount
                                            ]);
                                        }
                                    );
                                    var data = google.visualization.arrayToDataTable(chartData);

                                    var options = {
                                        bars: "horizontal",
                                        chart: {
                                            title: "Municipality Wise Assessee Bar Chart",
                                            subtitle: "Municipality Wise Assessee Bar Chart"
                                        },
                                        bars: "vertical",
                                        vAxis: {
                                            format: "decimal"
                                        },
                                        height: 400,
                                        width: "100%",
                                        colors: ["#1b9e77", "#d95f02", "#7570b3"]
                                    };

                                    var chart = new google.charts.Bar(chartDivCtrl);
                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                }
                                break;
                            }
                        default:
                            {
                                chartDivCtrl.innerHTML = "";
                                break;
                            }
                    };
                }

            };
        };
        //execute from here for drill down assessee
        var execAssesseDriller = function () {
            ajax('post', _curRoute.url, _curRoute.payload, function (r) {
                if (r.Status === 200) {
                    prepareDatasource(_curRoute.key, r.Data, function (rr) {
                        //rr is acutally datatable format
                        //clear if datatable previously attached with this table
                        if ($.fn.DataTable.isDataTable(_domControl._tblAssesseeGrid._selector)) {
                            _domControl._tblAssesseeGrid._obj
                                .DataTable()
                                .destroy();
                            _domControl._tblAssesseeGrid._obj.empty();
                        }
                        _domControl._tblAssesseeGrid._obj.DataTable(rr);
                        chartOperation.populateChart({
                            route: _curRoute,
                            tblData: rr
                        });
                    });
                } else {
                    alertify.error(r.Message);
                }
            });
        }
        var _eventHandlers = new function () {
            var _self = this;
            var getDefaultDateRange = function () {
                var timeSpanVal = _domControl._ddlTimeSpan._obj.val();
                switch (timeSpanVal) {
                    case 'last-6-months':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setMonth(toDate.getMonth() - 6))
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-1-year':
                        {
                            var fromDate1 = new Date();
                            var toDate1 = new Date();
                            fromDate1 = new Date(fromDate1.setMonth(toDate1.getMonth() - 12))
                            return {
                                FromDate: fromDate1,
                                ToDate: toDate1
                            };
                            break;
                        }
                    case 'date-range':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-month':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setDate(1));
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-7-days':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setDate(toDate.getDate() - 7));
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'to-day':
                    default:
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                }
            };
            this.onClickBtnDoorDrillAssessee = function () {
                _routeHistory.clearAll();
                _curRoute = null;
                setCurRoute("district-wise", getRouteValueTemplate("district-wise")());
                execAssesseDriller();
            }
            this.onChangeTodate = function (dateText, ctrl) {
                _domControl._txtFromDate._obj.datepicker('option', {
                    maxDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeFromdate = function (dateText, ctrl) {
                _domControl._txtToDate._obj.datepicker('option', {
                    minDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeDdlTimeSpan = function (e, r) {
                var dateRange = getDefaultDateRange();
                _domControl._txtFromDate._obj.datepicker("setDate", dateRange.FromDate);
                _domControl._txtToDate._obj.datepicker("setDate", dateRange.ToDate);
                _domControl._txtFromDate._obj.val(dateRange.FromDate.format('dd/mm/yyyy'))
                _domControl._txtToDate._obj.val(dateRange.ToDate.format('dd/mm/yyyy'))
                //$('.ui-datepicker-current-day').click(); //trigger onSelect on date picker, autometically track their won event handler

                //control custom date range visibility
                if (_domControl._ddlTimeSpan._obj.val() == 'date-range') {
                    _domControl._cntrCustomDateRange._obj.show();
                } else {
                    _domControl._cntrCustomDateRange._obj.hide();
                }
                _self.onDateRangeModelChanges(e, r);
            };
            this.onClickBtnGetDetails = function () {
                try {
                    _domControl._btnGetDetails._obj.hide();
                    _curRoute.updatePayload();
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
            this.onDateRangeModelChanges = function (control, args) {
                var key = null;
                if (args && args.hasOwnProperty("key")) {
                    key = args.key;
                }
                if (key !== "page_load") {
                    _domControl._btnGetDetails._obj.show();
                } else {
                    _domControl._btnGetDetails._obj.hide();
                }
            };
            this.onClickDrillAssesseebyMuncipality = function () {
                setCurRoute("municipality-wise", getRouteValueTemplate("municipality-wise")($(this).attr("data-DistrictId")));
                try {
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
            this.onClickDrillAssesseebyMuncipalityMonthWise = function () {
                setCurRoute("municipality-month-wise", getRouteValueTemplate("municipality-month-wise")($(this).attr("data-MuniciplityId")));
                try {
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
            this.onClickNewlyaddedassesseesmonthyear = function () {
                setCurRoute("new-assessee-list-by-ward-month-year", getRouteValueTemplate("new-assessee-list-by-ward-month-year")($(this).attr("data-municiplityid"), $(this).attr("data-ward-id"), $(this).attr("data-month-year")));
                try {
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
            this.onClickShowHistory = function () {
                if (_routeHistory.count() <= 1) {
                    return;
                }
                _routeHistory.removeLast();
                var routeVal = _routeHistory.getLast();
                if (routeVal) {
                    setCurRoute(routeVal.key, routeVal.payload);
                    //_routeHistory.removeLast();
                    var allRoutes = _routeHistory.histories();
                    try {
                        execAssesseDriller();
                    } catch (e) {
                        alertify.error(e);
                    }
                }
            };
            this.onRouteHistoryAdded = function (newRoute) {
                if (_routeHistory.count() > 1) {
                    _domControl._btnShowHistory._obj.show();
                }
            };
            this.onRouteHistoryRemoved = function (deletedRoute) {
                if (_routeHistory.count() <= 1) {
                    _domControl._btnShowHistory._obj.hide();
                }
            };
            //this.onClicklnkallassesseebymunicipalitywardlocation = function () {
            //    setCurRoute("assessee-by-municipality-ward-location", getRouteValueTemplate("assessee-by-municipality-ward-location")($(this).attr("data-municiplityid"), $(this).attr("data-decision-key")));
            //    try {
            //        execAssesseDriller();
            //    } catch (e) {
            //        alertify.error(e);
            //    }
            //};
            this.onClick_lnkdrillbywardwise=function(){
                setCurRoute("ward-wise", getRouteValueTemplate("ward-wise")($(this).attr("data-municiplityid")));
                try {
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
            this.onClick_lnkdrillbywardmonthwise = function () {
                setCurRoute("ward-month-wise", getRouteValueTemplate("ward-month-wise")($(this).attr("data-municiplityid"), $(this).attr("data-ward-id")));
                try {
                    execAssesseDriller();
                } catch (e) {
                    alertify.error(e);
                }
            };
        };
        var bindEvent = function () {
            _domControl._btnDrillAssessee._obj.on('click', _eventHandlers.onClickBtnDoorDrillAssessee);
            _domControl._ddlTimeSpan._obj.on('change', _eventHandlers.onChangeDdlTimeSpan);

            _domControl._txtFromDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeFromdate,
                maxDate: new Date()
            });
            _domControl._txtToDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeTodate,
                maxDate: new Date()
            });
            _domControl._btnGetDetails._obj.on('click', _eventHandlers.onClickBtnGetDetails);
            _domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnkDrillAssesseeByMuncipality._selector, _eventHandlers.onClickDrillAssesseebyMuncipality);
            _domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnkdrillbymunicipalitymonthwise._selector, _eventHandlers.onClickDrillAssesseebyMuncipalityMonthWise);
            _domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnknewlyaddedassesseesmonthyear._selector, _eventHandlers.onClickNewlyaddedassesseesmonthyear);
            //_domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnkassesseebymunicipalitywardlocation._selector, _eventHandlers.onClicklnkallassesseebymunicipalitywardlocation);
            _domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnkdrillbywardwise._selector, _eventHandlers.onClick_lnkdrillbywardwise);
            _domControl._tabDrilldownAssessee._obj.on("click", _domControl._lnkdrillbywardmonthwise._selector, _eventHandlers.onClick_lnkdrillbywardmonthwise)
            _domControl._btnShowHistory._obj.on("click", _eventHandlers.onClickShowHistory);
        };
        (function () {
            bindEvent();
            prepareRoutes();
            _domControl._ddlTimeSpan._obj.find("option:eq(1)").prop('selected', true);
            _domControl._ddlTimeSpan._obj.trigger('change', {
                key: "page_load"
            });
            _domControl._btnDrillAssessee._obj.trigger('click');
        }());
    };
    var dillMunicipalityComponent = new function () {
        var _self = this;
        var _curRoute = null;
        var _domControl = {
            _tabDrilldownMunicipality: preaparedControl("#drilldown-Municipalities", _scopeContainer._obj),
            _tblLast5MunicipalityGrid: preaparedControl(".tbl-last-5-municipality", preaparedControl("#drilldown-Municipalities", _scopeContainer._obj)._obj),
            _tblMunicipalityValuation: preaparedControl(".tbl-municipality-valuation", preaparedControl("#drilldown-Municipalities", _scopeContainer._obj)._obj),
            _btnDrillMunicipality: preaparedControl(".btn-door-drill-Municipalities", _scopeContainer._obj),
        };
        var pollyfill = new function () {
            _domControl._tblLast5MunicipalityGrid.setCaption = function (title) {
                _domControl._tabDrilldownMunicipality._obj.find('.box-title-right').html(title);
            };
            _domControl._tblMunicipalityValuation.setCaption = function (title) {
                _domControl._tabDrilldownMunicipality._obj.find('.box-title-left').html(title);
            };
        };
        var getRouteNPayload= function (key) {
            var getDateRangeModel = function () {
                var txtfromDate = _domControl._txtFromDate._obj.val();
                var txttoDate = _domControl._txtToDate._obj.val();
                if (txtfromDate.split("/").length !== 3) {
                    throw "Invalid From Date: " + txtfromDate;
                }
                if (txttoDate.split("/").length !== 3) {
                    throw "Invalid To Date: " + txttoDate;
                }
                var fromDate = CONVERTER.Date.convertToDate(
                    txtfromDate.split("/")[2],
                    txtfromDate.split("/")[1],
                    txtfromDate.split("/")[0]
                );
                var toDate = CONVERTER.Date.convertToDate(
                    txttoDate.split("/")[2],
                    txttoDate.split("/")[1],
                    txttoDate.split("/")[0]
                );
                return {
                    FromDate: fromDate,
                    ToDate: toDate
                };
            };
            var obj = {};
            var getRouteModel = function (key, url, payload) {
                return {
                    key: key,
                    url: url,
                    payload: payload
                };
            };
            obj["municipality-valuation"] = function () {
                return getRouteModel("municipality-valuation", '/Administration/DashBoard/GetMunicipalitiesValuations', null);
            };
            obj["last-5-municipality-added"] = function (rowCount) {
                return getRouteModel("last-5-municipality-added", '/Administration/DashBoard/LastCountMunicipalities', { CountRow: rowCount });
            };
            return obj[key];
        };
        var setTitle = function (title) {
                _domControl._tabDrilldownMunicipality._obj.find(".box-title").html(title);
        };
        //prepare datatable datasource
        var prepareDatasource = function (key, data, callback) {
            //sample datasource
            var preparedSource = {
                data: [],
                columns: []
            };
            switch (key) {
                case "ward-wise":
                    {
                        var cols = [{
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "OldAssesseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "TotalAssesseeCount"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Ward"
                        },
                        {
                            aTargets: [1],
                            sTitle: "Old Assessees"
                        },
                        {
                            aTargets: [2],
                            sTitle: "New Assessees",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-ward-month-wise" data-ward-id="' + full.WardId + '" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [3],
                            sTitle: "Total Assessees"
                        }
                        ];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                Ward: value.Ward,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAssesseeCount: value.TotalAssesseeCount - value.NewAssesseeCount,
                                WardId: value.WardId,
                                MunicipalityId: value.MunicipalityId
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "ward-month-wise":
                    {
                        var cols = [{
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "MonthYear"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Ward"
                        },
                        {
                            aTargets: [1],
                            sTitle: "New Assessees",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-new-assessee-list-by-ward-month-year" data-ward-id="' + full.WardId + '" data-month-year="' + full.MonthYear + '" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [2],
                            sTitle: "Month-Year"
                        }
                        ];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                Ward: value.Ward,
                                NewAssesseeCount: value.NewAssesseeCount,
                                MonthYear: value.MonthYear,
                                WardId: value.WardId,
                                MunicipalityId: value.MunicipalityId
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "district-wise":
                    {
                        var cols = [{
                            mDataProp: "DistrictName"
                        }, {
                            mDataProp: "OldAsseseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "TotalAssesseeCount"
                        }]; //District //

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "District",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-municipality" data-DistrictId="' + full.DistrictId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [1],
                            sTitle: "Old Assessees"
                        }, {
                            aTargets: [2],
                            sTitle: "New Assessees"
                        }, {
                            aTargets: [3],
                            sTitle: "Total Assessees"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                DistrictId: value.DistrictId,
                                DistrictName: value.DistrictName,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAsseseeCount: value.TotalAssesseeCount - value.NewAssesseeCount
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                case "municipality-wise":
                    {
                        var cols = [{
                            mDataProp: "MunicipalityName"
                        }, {
                            mDataProp: "OldAsseseeCount"
                        }, {
                            mDataProp: "NewAssesseeCount"
                        }, {
                            mDataProp: "TotalAssesseeCount"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Municipality",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-ward-wise" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [1],
                            sTitle: "Old Assessees"
                        }, {
                            aTargets: [2],
                            sTitle: "New Assessees",
                            render: function (data, type, full, meta) {
                                return '<a class="lnk-drill-by-ward-wise" data-MuniciplityId="' + full.MunicipalityId + '" href="javascript:void(0);">' + data + "</a>";
                            }
                        }, {
                            aTargets: [3],
                            sTitle: "Total Assessees"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                MunicipalityId: value.MunicipalityId,
                                MunicipalityName: value.MunicipalityName,
                                TotalAssesseeCount: value.TotalAssesseeCount,
                                NewAssesseeCount: value.NewAssesseeCount,
                                OldAsseseeCount: value.TotalAssesseeCount - value.NewAssesseeCount
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }

                case "new-assessee-list-by-ward-month-year":
                    {
                        var cols = [{
                            mDataProp: "AssesseeNo"
                        }, {
                            mDataProp: "AssesseeName"
                        }, {
                            mDataProp: "Ward"
                        }, {
                            mDataProp: "Location"
                        }, {
                            mDataProp: "Holding"
                        }, {
                            mDataProp: "AssesseeAddress"
                        }, {
                            mDataProp: "PropertyTax"
                        }, {
                            mDataProp: "Surcharge"
                        }];

                        //assign cols name
                        var colDef = [{
                            aTargets: [0],
                            sTitle: "Assessee No"
                        }, {
                            aTargets: [1],
                            sTitle: "Assessee Name"
                        }, {
                            aTargets: [2],
                            sTitle: "Ward"
                        }, {
                            aTargets: [3],
                            sTitle: "Location"
                        }, {
                            aTargets: [4],
                            sTitle: "Holding"
                        }, {
                            aTargets: [5],
                            sTitle: "Address"
                        }, {
                            aTargets: [6],
                            sTitle: "Property Tax"
                        }, {
                            aTargets: [7],
                            sTitle: "Surcharge"
                        }];

                        //Get stored data from HTML table element
                        var results = [];
                        $.each(data, function (index, value) {
                            results.push({
                                AssesseeID: value.AssesseeID,
                                AssesseeName: value.Name,
                                AssesseeNo: value.AssesseeNo,
                                Ward: value.Ward,
                                Location: value.Location,
                                Holding: value.HoldingNo,
                                AssesseeAddress: value.AssesseeAddress,
                                PropertyTax: value.PropertyTax,
                                Surcharge: value.Surcharge
                            });
                        });
                        callback({
                            searching: true,
                            ordering: true,
                            paging: true,
                            aaData: results,
                            aoColumns: cols,
                            aoColumnDefs: colDef,
                            destroy: true,
                            order: []
                        });
                        break;
                    }
                default:
                    {
                        alertify.error("invalid route");
                    }
            }
        };
        //execute from here for drill down assessee
        var post = function (payload, callback) {
            ajax('post', payload.url, payload.payload, function (r) {
                if (r.Status === 200) {
                    callback(r)
                } else {
                    alertify.error(r.Message);
                }
            });
        };
        var _loadMunicipalityValuation = function (data,title) {
            var cols = [{
                mDataProp: "MunicipalityName"
            }, {
                mDataProp: "TotalAssesseeCount"
            }, {
                mDataProp: "TotalValuation"
            }];

            //assign cols name
            var colDef = [{
                aTargets: [0],
                sTitle: "Municipality Name"
            },
            {
                aTargets: [1],
                sTitle: "Assessee Count",
                className: "text-right"
            },
            {
                aTargets: [2],
                sTitle: "Total Valuation",
                className: "text-right"
            }];

            //Get stored data from HTML table element
            var results = [];
            $.each(data, function (index, value) {
                results.push({
                    MunicipalityName: value.MunicipalityName,
                    TotalAssesseeCount: value.TotalAssesseeCount,
                    TotalValuation: value.TotalValuation.toFixed(2)
                    //MunicipalityId: value.MunicipalityId
                });
            });
            var dtTblObj={
                searching: true,
                ordering: true,
                paging: true,
                aaData: results,
                aoColumns: cols,
                aoColumnDefs: colDef,
                destroy: true,
                order: []
            };
            if ($.fn.DataTable.isDataTable(_domControl._tblMunicipalityValuation._selector)) {
                _domControl._tblMunicipalityValuation._obj
                    .DataTable()
                    .destroy();
                _domControl._tblMunicipalityValuation._obj.empty();
            }
            _domControl._tblMunicipalityValuation._obj.DataTable(dtTblObj);
            _domControl._tblMunicipalityValuation.setCaption(title);
        };
        var _loadLastAddedMunicipalities = function (data, title) {
            var cols = [{
                mDataProp: "sl"
            }, {
                mDataProp: "MunicipalityName"
            }];

            //assign cols name
            var colDef = [{
                aTargets: [0],
                sTitle: "#",
                className: "text-right"
            },{
                aTargets: [1],
                sTitle: "Municipality Name"
            }];

            //Get stored data from HTML table element
            var results = [];
            $.each(data, function (index, value) {
                results.push({
                    sl:(index+1),
                    MunicipalityName: value.MunicipalityName,
                    MunicipalityId: value.MunicipalityId
                });
            });
            var dtTblObj = {
                searching: false,
                ordering: true,
                paging: true,
                aaData: results,
                aoColumns: cols,
                aoColumnDefs: colDef,
                destroy: true,
                order: []
            };
            if ($.fn.DataTable.isDataTable(_domControl._tblLast5MunicipalityGrid._selector)) {
                _domControl._tblLast5MunicipalityGrid._obj
                    .DataTable()
                    .destroy();
                _domControl._tblLast5MunicipalityGrid._obj.empty();
            }
            _domControl._tblLast5MunicipalityGrid._obj.DataTable(dtTblObj);
            _domControl._tblLast5MunicipalityGrid.setCaption(title);
        };
        var _eventHandlers = new function () {
            var _self = this;
            this.onClickBtnDoorDrillMunicipality = function () {
                var payload1 = getRouteNPayload("municipality-valuation")();
                post(payload1, function (r) {
                    _loadMunicipalityValuation(r.Data, "Municipality Wise Valuations and Assessees");
                });

                //row count 5 last five added municipalities
                var rowCount = 10;
                var payload2 = getRouteNPayload("last-5-municipality-added")(rowCount);
                post(payload2, function (r) {
                    _loadLastAddedMunicipalities(r.Data, "Last " + rowCount + " Muncipalities Added");
                });
            }
        };
        var bindEvent = function () {
            _domControl._btnDrillMunicipality._obj.on('click', _eventHandlers.onClickBtnDoorDrillMunicipality);
        };
        (function () {
            bindEvent();
        }());
    };
    var dillMunicipalityCollectionComponent = new function () {
        var _self = this;
        var _curRoute = null;
        var MuniciaplityFilter = [];
        var MunicipalityWiseFilter = {RowCount:10,OrderBy:"DESC"}
        var _domControl = {
            _tabDrilldownMunicipality: preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj),
            _tblCollectionByCategories: preaparedControl(".tbl-collection-in-categories", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _btnDoorGetData: preaparedControl(".btn-door-Municipality-Collection", _scopeContainer._obj),
            _ddlTimeSpan: preaparedControl(".ddl-time-span", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrCustomDateRange: preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _txtFromDate: preaparedControl(".from-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj)._obj),
            _txtToDate: preaparedControl(".to-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj)._obj),
            _btnGetDetails: preaparedControl(".btn-get-details", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrPieCollectionByOrigine: preaparedControl(".pie-origine", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrPieCollectionPayHeardWise: preaparedControl(".pie-payhead-wise", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _filterBtnMuncipality: preaparedControl(".municipality-filter", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrBarChrtMunicipalityWiseCollection: preaparedControl(".bar-chrt-municipality-wise-collection", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            
        };
        var pollyfill = new function () {
            _domControl._tblCollectionByCategories.setCaption = function (title) {
                _domControl._tabDrilldownMunicipality._obj.find('.box-title-right').html(title);
            };
        };
        var getRouteNPayload = function (key) {
            var getDateRangeModel = function () {
                var timeSpanVal = _domControl._ddlTimeSpan._obj.val();
                switch (timeSpanVal) {
                    
                    case 'last-1-year':
                        {
                            var fromDate1 = new Date();
                            var toDate1 = new Date();
                            fromDate1 = new Date(fromDate1.setMonth(toDate1.getMonth() - 12))
                            return {
                                FromDate: fromDate1,
                                ToDate: toDate1
                            };
                            break;
                        }
                    case 'date-range':
                        {
                            var txtfromDate = _domControl._txtFromDate._obj.val();
                            var txttoDate = _domControl._txtToDate._obj.val();
                            if (txtfromDate.split("/").length !== 3) {
                                throw "Invalid From Date: " + txtfromDate;
                            }
                            if (txttoDate.split("/").length !== 3) {
                                throw "Invalid To Date: " + txttoDate;
                            }
                            var fromDate = CONVERTER.Date.convertToDate(
                                txtfromDate.split("/")[2],
                                txtfromDate.split("/")[1],
                                txtfromDate.split("/")[0]
                            );
                            var toDate = CONVERTER.Date.convertToDate(
                                txttoDate.split("/")[2],
                                txttoDate.split("/")[1],
                                txttoDate.split("/")[0]
                            );
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                        }
                    case 'last-6-months':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setMonth(toDate.getMonth() - 6));
                            if (timeSpanVal != 'last-6-months') {
                                _domControl._ddlTimeSpan._obj.find('option:eq(1)').prop('selected', true);
                            }
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-month':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setDate(1));
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-7-days':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setDate(toDate.getDate() - 7));
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'to-day':
                    default:
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                }
            };
            var dateRange = getDateRangeModel();
            var obj = {};
            var getRouteModel = function (key, url, payload) {
                return {
                    key: key,
                    url: url,
                    payload: payload
                };
            };
            obj["municipality-Collection-Summary"] = function () {
                return getRouteModel("municipality-Collection-Summary", '/Administration/DashBoard/GetCollectionSummary', Object.assign(dateRange, { MunicipalityIds: MuniciaplityFilter }));
            };
            obj["municipality-Wise-Collection"] = function () {
                var rowCount = 0;
                var orderby="DESC"
                if (MuniciaplityFilter.length == 0) {
                    rowCount = MunicipalityWiseFilter.RowCount;
                    orderby = MunicipalityWiseFilter.OrderBy;
                }

                var payload=Object.assign(dateRange, { RowCount: rowCount, OrderBy: orderby });
                payload = Object.assign(payload, { MunicipalityIds: MuniciaplityFilter });
                return getRouteModel("municipality-Wise-Collection", '/Administration/DashBoard/MunicipalityWiseCollection', payload);
            };
            return obj[key];
        };
        var setTitle = function (title) {
            _domControl._tabDrilldownMunicipality._obj.find(".box-title").html(title);
        };
        var getDefaultDateRange = function () {
            var timeSpanVal = _domControl._ddlTimeSpan._obj.val();
            switch (timeSpanVal) {
                case 'last-6-months':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setMonth(toDate.getMonth() - 6))
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'last-1-year':
                    {
                        var fromDate1 = new Date();
                        var toDate1 = new Date();
                        fromDate1 = new Date(fromDate1.setMonth(toDate1.getMonth() - 12))
                        return {
                            FromDate: fromDate1,
                            ToDate: toDate1
                        };
                        break;
                    }
                case 'date-range':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'last-month':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setDate(1));
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'last-7-days':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setDate(toDate.getDate() - 7));
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'to-day':
                default:
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
            }
        };
        var post = function (payload, callback) {
            ajax('post', payload.url, payload.payload, function (r) {
                if (r.Status === 200) {
                    callback(r)
                } else {
                    alertify.error(r.Message);
                }
            });
        };
        var _loadMuncipalityCollectionSummary = function () {
            try {
                _domControl._btnGetDetails._obj.hide();
                //summary load
                var payload = getRouteNPayload("municipality-Collection-Summary")();
                post(payload, function (r) {
                    _populateMunicipalityCollectionSummary(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };
        var _loadMunicipalityWiseCollectionData = function () {
            try {
                _domControl._btnGetDetails._obj.hide();
                //summary load
                var payload = getRouteNPayload("municipality-Wise-Collection")();
                post(payload, function (r) {
                    _populateMunicipalityWiseCollection(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };
        var _populateMunicipalityCollectionSummary = function (data, title) {

            var paidOnApp = 0;
            var paidOnWebPortal = 0;
            var paidOnCounter = 0;

            var getTotalAmountByOrigine=function(appOrigine,dtSource){
                try{
                    var rows = [];
                    if (appOrigine === "TOTAL")
                    {
                        rows = alasql("select sum(NetPaidAmount) as totAmount from ?", [dtSource])
                    } else {
                        rows = alasql("select sum(NetPaidAmount) as totAmount from ? where AppOrigine=? group by AppOrigine", [dtSource, appOrigine]);
                    }

                    if(rows.length>0)
                    {
                        return Number(rows[0].totAmount).toFixed(2);
                    }else{
                        throw 'no row found';
                    }
                }   catch(e){
                    return 0;
                }        
            };
            $(".collection-summary .amount-head-total").text(getTotalAmountByOrigine("TOTAL", data));
            $(".collection-summary .amount-head-counter").text(getTotalAmountByOrigine("COUNTER", data));
            $(".collection-summary .amount-head-App").text(getTotalAmountByOrigine("APP", data));
            $(".collection-summary .amount-head-web-portal").text(getTotalAmountByOrigine("WEB", data));




            var chartHeightRecomended = 400;
            var charPopulate1 = function (data1) {
                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    data1 = alasql("select sum(NetPaidAmount) as NetPaidAmount,AppOrigine from ? group by AppOrigine", [data1]);
                    var chartData = [];
                    chartData.push(['Collection Type', 'Collection Amount']);
                    $.each(
                        data1,
                        function (index, value) {
                            chartData.push([
                                 value.AppOrigine,
                                 (value.NetPaidAmount)//.toFixed(2)
                            ]);
                        }
                    );
                    var data2 = google.visualization.arrayToDataTable(chartData);

                    var options = {
                        title: 'Collection Graph',
                        height: chartHeightRecomended,
                        legend: { position: 'right' },
                        pieSliceText: 'value',
                    };

                    var ctrl = document.getElementById(_domControl._cntrPieCollectionByOrigine._obj.attr("id"));
                    var chart = new google.visualization.PieChart(ctrl);

                    chart.draw(data2, options);
                };
            }
            var charPopulate2 = function (data1) {
                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    data1 = alasql("select sum(CurPropTax) as CurPropTax, sum(ArrPropTax) as ArrPropTax,sum(CurSurcharge) as CurSurcharge, sum(ArrSurcharge) as ArrSurcharge, sum(OtherAdjustment) as OtherAdjustment, sum(Rebate) as Rebate, sum(Interest) as Interest from ?", [data1]);
                    var chartData = [];
                    chartData.push(['Property Tax', 'Amount']);
                    $.each(
                        data1,
                        function (index, value) {
                            chartData.push([
                                 "Curr. Prop. Tax: " + formatCurrency(value.CurPropTax), value.CurPropTax
                            ]);
                            chartData.push([
                                "Arr. Prop. Tax: " + formatCurrency(value.ArrPropTax), value.ArrPropTax
                            ]);
                            chartData.push([
                                "Curr. Sur.: " + formatCurrency(value.CurSurcharge), value.CurSurcharge
                            ]);

                            chartData.push([
                                 "Arr. Sur.: " + formatCurrency(value.ArrSurcharge), value.ArrSurcharge
                            ]);
                            chartData.push([
                                 "Interest: " + formatCurrency(value.Interest), value.Interest
                            ]);
                            chartData.push([
                                 "Rebate: " + formatCurrency(value.Rebate * -1), (value.Rebate * -1)
                            ]);
                            chartData.push([
                                 "Other Adjst.: " + formatCurrency(value.OtherAdjustment * -1), (value.OtherAdjustment * -1)
                            ]);
                        }
                    );
                    var data2 = google.visualization.arrayToDataTable(chartData);

                    var options = {
                        title: 'Collection Payhead Wise',
                        height: chartHeightRecomended,
                        pieSliceText: 'value',
                        legend: { position: 'right' }
                    };

                    var ctrl = document.getElementById(_domControl._cntrPieCollectionPayHeardWise._obj.attr("id"));
                    var chart = new google.visualization.PieChart(ctrl);

                    chart.draw(data2, options);
                };
            }

            charPopulate1(data);
            charPopulate2(data);
        };
        var _populateMunicipalityWiseCollection = function (data, title) {
            var chartDivCtrl = document.getElementById(_domControl._cntrBarChrtMunicipalityWiseCollection._obj.attr("id"));

            google.charts.load("current", {
                packages: ["bar"]
            });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var chartData = [];
                chartData.push([
                    "MunicipalityName",
                    "Total Collection",
                ]);

                data = alasql("select * from ? order by NetPaidAmount asc", [data])
                $.each(
                    data,
                    function (index, value) {
                        chartData.push([
                            value.MunicipalityName.replace("MUNICIPALITY", ""),
                            (value.NetPaidAmount).toFixed(2)
                        ]);
                    }
                );
                var data1 = google.visualization.arrayToDataTable(chartData);

                var options = {
                    chart: {
                        title: "Municipality Wise Collection",
                        //subtitle: "Ward Wise Assessee Bar Chart"
                    },
                    //bars: "vertical",
                    //vAxis: {
                    //    format: "decimal"
                    //},
                    height: 400,
                    width: '100%',
                    colors: ["#1b9e77", "#d95f02", "#7570b3"]
                };

                var chart = new google.charts.Bar(chartDivCtrl);
                chart.draw(data1, google.charts.Bar.convertOptions(options));
            }
            //_domControl._tblLast5MunicipalityGrid.setCaption(title);
        };
        var _eventHandlers = new function () {
            var _self = this;
            
            this.onClickBtnGetDetails = function () {
                _loadMuncipalityCollectionSummary();
                _loadMunicipalityWiseCollectionData();
            };
            this.onClick_btnDoorGetData = function () {
                _loadMuncipalityCollectionSummary();
                _loadMunicipalityWiseCollectionData();
            }
            this.onChangeTodate = function (dateText, ctrl) {
                _domControl._txtFromDate._obj.datepicker('option', {
                    maxDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeFromdate = function (dateText, ctrl) {
                _domControl._txtToDate._obj.datepicker('option', {
                    minDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeDdlTimeSpan = function (e, r) {
                var dateRange = getDefaultDateRange();
                _domControl._txtFromDate._obj.datepicker("setDate", dateRange.FromDate);
                _domControl._txtToDate._obj.datepicker("setDate", dateRange.ToDate);

                _domControl._txtFromDate._obj.val(dateRange.FromDate.format('dd/mm/yyyy'))
                _domControl._txtToDate._obj.val(dateRange.ToDate.format('dd/mm/yyyy'))
                //$('.ui-datepicker-current-day').click(); //trigger onSelect on date picker, autometically track their won event handler

                //control custom date range visibility
                if (_domControl._ddlTimeSpan._obj.val() == 'date-range') {
                    _domControl._cntrCustomDateRange._obj.show();
                } else {
                    _domControl._cntrCustomDateRange._obj.hide();
                }
                _self.onDateRangeModelChanges(e, r);
            };
            this.onDateRangeModelChanges = function (control, args) {
                var key = null;
                if (args && args.hasOwnProperty("key")) {
                    key = args.key;
                }
                if (key !== "page_load") {
                    _domControl._btnGetDetails._obj.show();
                } else {
                    _domControl._btnGetDetails._obj.hide();
                }
            };
        };
        var bindEvent = function () {
            _domControl._btnDoorGetData._obj.on('click', _eventHandlers.onClick_btnDoorGetData);
            _domControl._ddlTimeSpan._obj.on('change', _eventHandlers.onChangeDdlTimeSpan);
            _domControl._btnGetDetails._obj.on('click', _eventHandlers.onClickBtnGetDetails);
            _domControl._txtFromDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeFromdate,
                maxDate: new Date()
            });
            _domControl._txtToDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeTodate,
                maxDate: new Date()
            });
            _domControl._filterBtnMuncipality._obj.on('click', function () {
                municipalityFilter.selectedMunicipalities(MuniciaplityFilter);
                municipalityFilter.applyFilter = function (r) {
                    MuniciaplityFilter = r.MunicipalityIds;
                    municipalityFilter.hide();
                    if (MuniciaplityFilter.length > 0) {
                        $(".municipality-wise-collection-filter").hide();
                    } else {
                        $(".municipality-wise-collection-filter").show();
                    }
                    _loadMuncipalityCollectionSummary();
                    _loadMunicipalityWiseCollectionData();
                };
                municipalityFilter.show();
            })
            $(".ddl-order-by-municipality,.ddl-row-count-municipality").on('change', function () {
                MunicipalityWiseFilter.OrderBy = $(".ddl-order-by-municipality").val();
                MunicipalityWiseFilter.RowCount = $(".ddl-row-count-municipality").val();
                _loadMunicipalityWiseCollectionData();
            });
        };
        (function () {
            bindEvent();
            _domControl._ddlTimeSpan._obj.find('option:eq(1)').prop('selected', true);
            _domControl._ddlTimeSpan._obj.trigger('change', {
                key: "page_load"
            });
        }());
    };

    var assesseeSearchComponent =new function () {
        var _searchContainer = preaparedControl('#search-assessee', _scopeContainer);
        var basicSearchForm = function (assesseeForm) {
            var _basicSearchContainer = this;
            var constructor =new  function () {
                _basicSearchContainer.container = preaparedControl('#basic-search', _searchContainer);
            };
            var _domControls = {
                _txtMunicipality: preaparedControl(".txtMunicipality", _basicSearchContainer.container._obj),
                _txtWard: preaparedControl(".txtWard", _basicSearchContainer.container._obj),
                _txtLocation: preaparedControl(".txtLocation", _basicSearchContainer.container._obj),
                _txtHoldingName: preaparedControl(".txtHoldingName", _basicSearchContainer.container._obj),
                _hdnMunicipality: preaparedControl(".hdnMunicipality", _basicSearchContainer.container._obj),
                _hdnWard: preaparedControl(".hdnWard", _basicSearchContainer.container._obj),
                _hdnLocation: preaparedControl(".hdnLocation", _basicSearchContainer.container._obj),
                _hdnHoldingName: preaparedControl(".hdnHoldingName", _basicSearchContainer.container._obj),

                _btnSearch: preaparedControl(".btnSearch", _basicSearchContainer.container._obj),
            };
            var pollyFill =new  function () {
                //reset
                _domControls._txtMunicipality.reset = function () {
                    _domControls._txtMunicipality._obj.val('');
                    _domControls._hdnLocation._obj.val('');
                }
                _domControls._txtMunicipality.reset = function () {
                    _domControls._txtMunicipality._obj.val('');
                    _domControls._hdnLocation._obj.val('');
                }

                _domControls._txtMunicipality.reset = function () {
                    _domControls._txtMunicipality._obj.val('');
                    _domControls._hdnLocation._obj.val('');
                }
                _domControls._txtMunicipality.reset = function () {
                    _domControls._txtMunicipality._obj.val('');
                    _domControls._hdnLocation._obj.val('');
                }

            };
            
            var showAssessee = function (obj) {
                assesseeForm.show();
            };
            //(function () {
            //    constructor();
            //}())
        };
        var assesseeForm = function () {
            this.show = function (obj) {
                alert('assesseShown');
            };
            this.close = function () { };
            this.reset = function () { };
            this.noAssesseeFound = function () { };
        };
        (function () {
            new basicSearchForm(new assesseeForm());
        }());
    };
});