﻿$(document).ready(function () {
    app.ddlMunicipality();
    app.ddlBudgetCode();
   // app.getAllBudgetList();
    app.PopulateFinyear();
    $(document).on("click", ".btn-sbmt, .btn-edit", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData();
        console.log(configureDetails);
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirm Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData()
                };
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/SaveBudgetData",
                    data: JSON.stringify(payload.configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    $(document).on("click", ".btn-search", function () {
        //app.populateSearchedData();
        app.getAllBudgetList();
    });
    $(document).on('click', ".hdr", function () {
        var Bcode = $(this).attr('data-tergate-row');
        $(".myTable > tbody > tr:not(.hdr)." + Bcode).show();
        if ($(this).hasClass('expanded')) {
            $(".myTable > tbody > tr:not(.hdr)." + Bcode).hide();
            $(this).find('.minusSymbl').hide();
            $(this).find('.plusSymbl').show();
            $(this).removeClass('expanded');
        } else {
            $(".myTable > tbody > tr:not(.hdr)." + Bcode).show();
            $(this).addClass('expanded');
            $(this).find('.minusSymbl').show();
            $(this).find('.plusSymbl').hide();
        }
    });
    $(document).on("click", ".updateRow", function () {
        var jsnData = JSON.parse($(this).closest('tr').find('.hdnJSN').val());
        $(".budgetCode").val(jsnData.BCode).attr("disabled",true);
        $('#ddlMunicipality').val(jsnData.MnId == 0 ? "" : jsnData.MnId);
        $(".budgetDesc").val(jsnData.BDesc).attr("disabled", true);
        $(".finYear").val(jsnData.FinYr);
        $(".budgetAmt").val(jsnData.BAmt);
        $(".effDate").val(CONVERTER.Date.convertCsToJsDate(jsnData.EffDt).format('dd/mm/yyyy'));
        $(".ddl-fund-type").val(jsnData.FundType);
        $(".remarks").val(jsnData.Remarks);
    });
});
var app = {
    populateSearchedData: function () {
        if ($('.finYear1').length > 0 && $('#ddlMunicipality1').val()>0)
        {
            var data = { Mnid: $('#ddlMunicipality1').val(), BCode: $('#ddlBudgetCode').val(), FinYr: $('.finYear1').val() };
            $.ajax({
                url: '/Modules/DprExtension/GetBudgetDtl',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    //console.log(response.Data);
                    app.PopulateFormBySearchData(response.Data);
                },
                error: function () { }
            });
        }
        else
        {
            alertify.alert("Please select Municipality and Financial Year.");
        }
        
    },
    ddlMunicipality: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllMunicipalites',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
                $('#ddlMunicipality').val("")
            },
            error: function () { }
        });
    },
    ddlBudgetCode: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllBudgetCode',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.PopulateBudget(response.Data);
                $('#ddlBudgetCode').val("")
            },
            error: function () { }
        });
    },
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#configurebudget"),
                txtBudgetCode: $(".budgetCode"),
                ddlMuni: $(".ddlMunicipality1"),
                txtBudgetDesc: $(".budgetDesc"),
                txtFinyear: $(".finYear1"),
                txtBudgetAmt: $(".budgetAmt"),
                txtEffFrom: $(".effDate"),
                ddlFundType: $(".ddl-fund-type"),
                txtRemarks: $(".remarks"),
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().ddlMuni.prop('name')] = { required: true };
                vldObj.rules[_doms().txtBudgetCode.prop('name')] = { required: true };
                //vldObj.rules[_doms().ddlMuni.prop('name')] = { required: true };
                vldObj.rules[_doms().txtBudgetDesc.prop('name')] = { required: true }; 
                vldObj.rules[_doms().txtFinyear.prop('name')] = { required: true };
                vldObj.rules[_doms().txtBudgetAmt.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().txtEffFrom.prop('name')] = { required: true};
                vldObj.rules[_doms().ddlFundType.prop('name')] = { required: true };
                vldObj.rules[_doms().txtRemarks.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[_doms().ddlMuni.prop('name')] = { required: "Please Select Municipality" };
                vldObj.messages[_doms().txtBudgetCode.prop('name')] = { required: "Please enter Budget Code" };
                //vldObj.messages[_doms().ddlMuni.prop('name')] = { required: "Select Municipality" };
                vldObj.messages[_doms().txtBudgetDesc.prop('name')] = { required: "Please enter Budget Desc"};
                vldObj.messages[_doms().txtFinyear.prop('name')] = { required: "Please enter Fin Year" };
                vldObj.messages[_doms().txtBudgetAmt.prop('name')] = { required: "Please enter Amount", number: "It should be numeric." };
                vldObj.messages[_doms().txtEffFrom.prop('name')] = { required: "Please select a date"};
                vldObj.messages[_doms().ddlFundType.prop('name')] = { required: "Please select Fund Type" };
                vldObj.messages[_doms().txtRemarks.prop('name')] = { required: "Please enter Remarks" };
               
                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                function toDate(dateStr) {
                    var fromDate = dateStr.split("/");
                    var strJobDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
                    return strJobDate;
                }
                return {
                    BCode: _doms().txtBudgetCode.val(),
                    MnId: _doms().ddlMuni.val(),
                    BDesc: _doms().txtBudgetDesc.val(),
                    FinYr: _doms().txtFinyear.val(),
                    BAmt: _doms().txtBudgetAmt.val(),
                    EffDt: toDate(_doms().txtEffFrom.val()),
                    FundType: _doms().ddlFundType.val(),
                    Remarks: _doms().txtRemarks.val(),
                };
            }
        }
    },
    PopulateFormBySearchData: function (data)
    {
        if (data == null)
        {
            alertify.alert("Budget Code not exists for this municipality");
        }
        else {
            $(".budgetCode").val(data.BCode);
            $('#ddlMunicipality').val(data.MnId == 0 ? "" : data.MnId);
            $(".budgetDesc").val(data.BDesc);
            $(".finYear").val(data.FinYr);
            $(".budgetAmt").val(data.BAmt);
            $(".effDate").val(CONVERTER.Date.convertCsToJsDate(data.EffDt).format('dd/mm/yyyy'));
            $(".ddl-fund-type").val(data.FundType);
            $(".remarks").val(data.Remarks);
        }
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('.ddlMunicipalityCmmn');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
        var option = ctlr_ddlmunicipality.find("option[value='" + ctlr_ddlmunicipality.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlmunicipality.find('option:eq(0)').prop('selected', true);
        }
    },
    PopulateBudget: function (data) {
        var ctlr_ddlBcode = $('.ddlBudgetCode');
        ctlr_ddlBcode.empty();
        ctlr_ddlBcode.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            ctlr_ddlBcode.append("<option value='" + value.BCodeId + "' >" + value.BCode + "</option>")
        });
        var option = ctlr_ddlBcode.find("option[value='" + ctlr_ddlBcode.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlBcode.find('option:eq(0)').prop('selected', true);
        }
    },
    getAllBudgetList: function () {
        var mnId = $('.ddlMunicipality1').val();
        var finYr = $('.finYear1').val();
        var payload = {"mnId":mnId,"finYr":finYr};
        $.ajax({
            url: '/Modules/DprExtension/GetAllBudgets',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    //for (i = 0; i < result.Data.length; i++) {
                        //var tempJson = result.Data[i];

                        //var tr = $('<tr/>');
                        //var td = $('<td class="budget-code" />');
                        //tr.append(td.append(tempJson.MnName));

                        ////td = $('<td class="fundType"/>');
                        ////tr.append(td.append(tempJson.FundType));
                        ////floor control
                        //var td = $('<td class="budget-code" />');
                        //tr.append(td.append(tempJson.BCode));

                        ////add usage control
                        //td = $('<td class="budget-desc" />');
                        //tr.append(td.append(tempJson.BDesc));

                        ////add Year control
                        //td = $('<td class="fin-year" />');
                        //tr.append(td.append(tempJson.FinYr));

                        ////add covered area control
                        //td = $('<td class="budget-amt" />');
                        //tr.append(td.append(tempJson.BAmt));

                        ////add Occupent Name control
                        //td = $('<td class="eff-from" />');
                        //tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.EffDt).format('dd/mm/yyyy')));

                        //td = $('<td class="remarks" />');
                        //tr.append(td.append(tempJson.Remarks));

                        //td = $('<td/>');tempJson = JSON.stringify(tempJson);
                        //tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson +'\'/>Update</a>'));
                        
                        ////td = $('<td class="completeJson"><input type="hidden" class="hdnJSN" value="' + tempJson +'"/></td>');
                        ////tr.append(td);
                        //$('#myTable').find('>tbody').append(tr);


                        $('#myTable').find('>tbody').empty();
                        var i;
                        var totalBAmt = 0.0;
                        var cls = "";
                        var hdrtotalBAmt = 0.0;
                        for (i = 0; i < result.Data.length; ++i) {
                            //floor control  
                            var tempJson = result.Data[i];

                            if ($('.myTable tr > td:contains(' + tempJson.BCode + ')').length == 0) {
                                cls = tempJson.BCode;
                                var tr = $('<tr style="background: #add4f7;"class="hdr" data-tergate-row="' + tempJson.BCode + '"/>');
                                var td = $('<td colspan="2" />');
                                tr.append(td.append('<b><span class="plusSymbl">+ </span><span class="minusSymbl" style="display: none;">- </span></b>' + tempJson.BCode));

                                var td = $('<td style="text-align: right"  class=' + tempJson.BCode + "-bamt" + '/>');
                                tr.append(td.append(''));

                                var td = $('<td  class=' + tempJson.BCode + "-dt" + '/>');
                                tr.append(td.append(''));

                                var td = $('<td class=' + tempJson.BCode + "-rem" + '/>');
                                tr.append(td.append(''));

                                var td = $('<td class=' + tempJson.BCode + "-act" + '/>');
                                tr.append(td.append(''));

                                $('.myTable').find('>tbody').append(tr);
                            }
                            var tr = $('<tr style="display: none;" class=' + cls + '/>');

                            //var td = $('<td />');
                            //tr.append(td.append(tempJson.MnName));

                            var td = $('<td />');
                            tr.append(td.append(tempJson.BCode));

                            var td = $('<td />');
                            tr.append(td.append(tempJson.BDesc));

                            //var td = $('<td />');
                            //tr.append(td.append(tempJson.FinYr));

                            var td = $('<td style="text-align: right"/>');
                            tr.append(td.append(tempJson.BAmt));
                            totalBAmt = totalBAmt + tempJson.BAmt;

                            var td = $('<td />');
                            tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.EffDt).format('dd/mm/yyyy')));

                            var td = $('<td />');
                            tr.append(td.append(tempJson.Remarks));

                            var td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                            tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson +'\'/>Update</a>'));

                            $('.myTable').find('>tbody').append(tr);

                        }
                        for (i = 0; i < result.Data.length; ++i) {
                            var tempJson = result.Data[i];
                            hdrtotalBAmt = 0.0;
                            $.each($('.myTable tr > td:contains(' + tempJson.BCode + ')'), function (index, item) {
                                if (index != 0) {
                                    hdrtotalBAmt = hdrtotalBAmt + parseInt($('.' + tempJson.BCode + "-hamt").html(hdrtotalBAmt));
                                }
                            });
                            $('.' + tempJson.BCode + "-bamt").html(tempJson.SumBamt);
                        }
                        var tr = $('<tr/>');

                        var td = $('<th colspan="2" style="txt-align:right"/>');
                        tr.append(td.append("Summary"));

                        var td = $('<th style="text-align: right" />');
                        tr.append(td.append(tempJson.SumUlbAmt));

                        var td = $('<th style="text-align: right" colspan="3" />');
                        tr.append(td.append(""));

                        $('.myTable').find('>tbody').append(tr);
                    //}
                }
            },
            error: function () { }
        });
    },
    PopulateFinyear: function () {
        var ctlr_ddlfinYear = $('.finYear');
        ctlr_ddlfinYear.empty();
        ctlr_ddlfinYear.append('<option value="">--select--</option>');
        for (var i = new Date().getFullYear()+1; i >= 1950;i--)
        {
            ctlr_ddlfinYear.append("<option value='" + (i-1) + "-" + (i) + "' >" + (i-1) + "-" + (i) + "</option>")
        }

        var ctlr_ddlfinYear1 = $('.finYear1');
        ctlr_ddlfinYear1.empty();
        ctlr_ddlfinYear1.append('<option value="">--select--</option>');
        for (var i = new Date().getFullYear()+1; i >= 1950; i--) {
            ctlr_ddlfinYear1.append("<option value='" + (i - 1) + "-" + (i) + "' >" + (i - 1) + "-" + (i) + "</option>")
        }
       
    },
}