﻿$(document).ready(function () {
    var constants = function () {
        return {
            effectiveFinYearQtr: function () {
                return {
                    ddlEffectiveFinYear: $(".assmt-finyear-qtr .ddl-finyear-picker"),//
                    ddlEffectiveQtr: $(".assmt-finyear-qtr .ddl-qtr")
                };
            },
            getValTag: function () {
                return $(".setting-parameters").find('.val-tag').val();
            },
            getYearId: function () {
                //year-id
                return $(".setting-parameters").find('.year-id').val();
            },
            convertTodate: function (format, date) {
                switch (format) {
                    case 'dd/mm/yyyy':
                    case 'dd/mm/yy':
                        {
                            var array = date.split('/');
                            return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                            break;
                        }
                }
            }
        };
    }
    var formLoader = new function () {
        //this.Load = function (payload,callBack) {
        //    $.ajax({
        //        type: 'post',
        //        url: "/Municipality/Assessment/GetReviewOrImprovementForm",
        //        data: JSON.stringify(payload),
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "html",
        //        success: function (response) {
        //            callBack(response);
        //        },
        //        failure: function (response) {
        //            alert(response);
        //        },
        //        error: function (response) {
        //            alert(response);
        //        }
        //    });
        //};
        this.Load = function (payload, callBack) {
            $.ajax({
                type: 'post',
                url: "/Municipality/Assessment/GetReviewOrImprovementForm",
                data: JSON.stringify(payload),
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (response) {
                    callBack(response);
                    onSuccess(payload);
                    $.ajax({
                        type: 'post',
                        url: "/Municipality/Assessment/GetOtherTabDetails",
                        data: JSON.stringify(payload),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.Data._OtherTabDetails != null) {
                                if (response.Data._OtherTabDetails.ApplictnDt != null) {
                                    $('.txt-application-date').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.ApplictnDt));
                                }
                                $('.txt-application-no').val(response.Data._OtherTabDetails.ApplictnCaseNo);
                                if (response.Data._OtherTabDetails.DtOfOrder != null) {
                                    $('.txt-date-of-order').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.DtOfOrder));
                                }
                                $('.txt-primary-phone-no').val(response.Data._OtherTabDetails.PrimaryPhNo);
                                $('.txt-primary-email-id').val(response.Data._OtherTabDetails.PrimaryEmail);
                                $('.txt-pro-cosing-fees').val(response.Data._OtherTabDetails.ProCosingFees);
                                $('.txt-other-fees').val(response.Data._OtherTabDetails.OtherFees);
                            }
                        }
                    });
                },

                failure: function (response) {
                    alert(response);
                },
                error: function (response) {
                    alert(response);
                }
            });
            function onSuccess(payload) {
                //alert(payload);
                wardId = parseInt(payload.wardId);
                LocationId = parseInt(payload.LocationId);
                $.ajax({
                    type: 'POST',
                    url: "/Municipality/Assessment/GetValuationDetailsJsonData",
                    data: JSON.stringify({ ValTag: payload.ValTag, wardId: wardId, LocationId: LocationId, HoldingNo: payload.HoldingNo }),
                    //data: JSON.stringify(payload),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.Data != null) {
                            for (i = 0; i < result.Data.length; i++) {
                                var tempJson = result.Data[i];

                                var tr = $('<tr/>');
                                //floor control
                                var td = $('<td class="annual-val" />');
                                tr.append(td.append(tempJson.AnnualValuation));

                                //add usage control
                                td = $('<td class="qtr-ptax" />');
                                tr.append(td.append(tempJson.Qtr_Ptax));

                                //add Year control
                                td = $('<td class="qtr-schrg" />');
                                tr.append(td.append(tempJson.Qtr_Schrg));

                                //add covered area control
                                td = $('<td class="land-cost" />');
                                tr.append(td.append(tempJson.LandCost));

                                //add Occupent Name control
                                td = $('<td class="qtr-Edu-cess" />');
                                tr.append(td.append(tempJson.EduCessAmt));

                                td = $('<td class="hiddenAssmtID" hidden>' + tempJson.AssmtId + '</td>');
                                tr.append(td);

                                td = $('<td class="hiddenTabID" hidden>' + tempJson.AnulValDtlTabId + '</td>');
                                tr.append(td);

                                td = $('<td class="editMode" hidden></td>');
                                tr.append(td);

                                td = $('<td/>');
                                tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

                                tempJson = JSON.stringify(tempJson);
                                td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
                                tr.append(td);

                                $('#valuation_details_table').find('>tbody').append(tr);
                                totalAnnualVal();
                                totalEducess();
                                totalQtrPtax();
                                totalQtrScharge();
                                totalLandCost();
                                $('#valuation-details').modal('hide');
                                $('#valuationAddBtn').show();
                                $('#valuationSaveBtn').hide();
                            }
                        }

                    },
                });
            }
        };
    };

    var _doms = function () {
        return {
            cntrDownloadedForm: $(".downloaded-form"),
            searchHolding: {
                frm: $("#frm-holding-details"),
                txtWard: $(".txtWard"),
                hdnWard: $(".hddn-Ward"),
                txtLocation: $(".txtLocation"),
                hdnLocation: $(".hddn-Location"),
                txtHoldingNo: $(".txt-holding-no"),
                hddnHoldingNo: $(".hddn-holding-no"),
            },
            partialHoldingDtl: {
                frm: $(".frm-partial-holding-details"),
                // ddlHoldingType: $(".ddl-holding-type"),
                ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                listHoldingNames: $(".list-holding-names"),
                txtAddress: $(".txt-address"),
                txtOwnerName: $(".txt-assessee-name")
            },
            valuationDtl: {
                frm: $("#frm-valuation-details"),
                ddlHoldingType: $(".ddl-holding-type"),
                ddlZone: $(".ddl-zone"),
                txtLandAreaKt: $(".txt-land-area-kt"),
                txtLandAreaCh: $(".txt-land-area-ch"),
                txtLandAreaSft: $(".txt-land-area-sft"),
                ddlNatureOfUser: $(".ddl-nature-of-use"),
                txtBldgAreaSft: $(".txt-bldg-area-sft"),
                txtPlinthSft: $(".txt-plinth-sft"),
                ddlConstruction: $(".ddl-construction"),
                txtLandCost: $(".txt-land-cost"),
                txtAge: $(".txt-age"),
                txtReadOnlyAnnualValuation: $(".txt-readonly-annual-valuation"),
                txtReadOnlyQtrPtax: $(".txt-readonly-qtr-ptax"),
                txtReadOnlyQtrSchrg: $(".txt-readonly-qtr-schrg"),
                ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                SrchgTag: $(".chk-SrchgTag"),
                EduCessTag: $(".chk-EduCessTag"),
                txtReadonlyQtrEduCess: $(".txt-readonly-qtr-Edu-cess")
            },
            additionalDtl: {
                frm: $("#frm-sec-holding-details"),
                txtMouajaName: $(".txt-mauja-name"),
                txtKhatianNoRs: $(".txt-khatian-no-rs"),
                txtKhatianNoLr: $(".txt-khatian-no-lr"),
                txtDagNoRs: $(".txt-dag-no-rs"),
                txtDagNoLr: $(".txt-dag-no-lr"),
                ddlHoldingArea: $(".ddl-holding-area"),
                txtDeedNo: $(".txt-deed-no"),
                ddlDeedType: $(".ddl-deed-type"),
                txtDeedDate: $(".txt-deed-date"),
                chkLiftTag: $(".lst-tags .lift"),
                chkDrainage: $(".lst-tags .drainage"),
                chkToilets: $(".lst-tags .toilets"),
                chkElecticity: $(".lst-tags .electricity"),
                chkWater: $(".lst-tags .Water"),
                txtBoroughNo: $(".txt-borough-no")
            },
            otherDtl: {
                frm: $("#frm-other-details"),
                txtBuildingDtl: $(".txt-building-dtl")
            },
            otherTabDtl: {
                frm: $("#frm-new-holding-OtherTabdetails"),
                txtPhoneNo: $(".txt-primary-phone-no"),
                txtEmailId: $(".txt-primary-email-id"),
                txtPorCosingFees: $(".txt-pro-cosing-fees"),
                txtOtherFees: $(".txt-other-fees"),
                ApplictnDt: $(".txt-application-date"),
                ApplictnCaseNo: $(".txt-application-no"),
                DtOfOrder: $(".txt-date-of-order"),
            }
        }
    };
    var formDatas = function () {
        return {
            searchedHoldingData: function () {
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                }
            },
            partialHoldingData: function () {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    //HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                }
            },
            holdingDetails: function () {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                    // HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                };
            },
            valuationdata: function () {
                return {
                    HoldingTypeId: _doms().valuationDtl.ddlHoldingType.val(),
                    ZoneId: _doms().valuationDtl.ddlZone.val(),
                    NatureOfUseId: _doms().valuationDtl.ddlNatureOfUser.val(),
                    ConstructionId: _doms().valuationDtl.ddlConstruction.val(),
                    LandArea_KT: _doms().valuationDtl.txtLandAreaKt.val(),
                    LandArea_CH: _doms().valuationDtl.txtLandAreaCh.val(),
                    LandArea_SFT: _doms().valuationDtl.txtLandAreaSft.val(),
                    BldgArea_SFT: _doms().valuationDtl.txtBldgAreaSft.val(),
                    Plinth_SFT: _doms().valuationDtl.txtPlinthSft.val(),
                    LandCost: _doms().valuationDtl.txtLandCost.val(),
                    Age: _doms().valuationDtl.txtAge.val(),
                    HoldingTypeGroupId: _doms().valuationDtl.ddlHoldingTypeGroup.val(),
                    SrchgTag: _doms().valuationDtl.SrchgTag.prop('checked'),
                    EduCessTag: _doms().valuationDtl.EduCessTag.prop('checked')
                }
            },
            getValuationArrayData: function () {
                var rowDataJsonString = null;
                var rowDataJson = null;
                var tbl = $('.completeJson');
                var _ValuationDetailsArr = [];
                $.each(tbl, function (i, v) {
                    rowDataJsonString = v.innerText;
                    rowDataJson = JSON.parse(rowDataJsonString);
                    rowDataJson.HoldingTypeGroupId = _doms().valuationDtl.ddlHoldingTypeGroup.val();
                    // rowDataJson.EditMode=
                    _ValuationDetailsArr.push(rowDataJson);
                });
                return _ValuationDetailsArr;
            },
            additionalHodlingData: function () {
                return {
                    MaujaName: _doms().additionalDtl.txtMouajaName.val(),
                    KhatianNo_RS: _doms().additionalDtl.txtKhatianNoRs.val(),
                    KhatianNo_LR: _doms().additionalDtl.txtKhatianNoLr.val(),
                    DaagNo_RS: _doms().additionalDtl.txtDagNoRs.val(),
                    DaagNo_LR: _doms().additionalDtl.txtDagNoLr.val(),
                    DeedNo: _doms().additionalDtl.txtDeedNo.val(),
                    DeedDate: constants().convertTodate('dd/mm/yyyy', _doms().additionalDtl.txtDeedDate.val()),
                    DeedTypeId: _doms().additionalDtl.ddlDeedType.val(),
                    HoldingAreaCode: _doms().additionalDtl.ddlHoldingArea.val(),
                    Tag_Lift: _doms().additionalDtl.chkLiftTag.prop('checked'),
                    Tag_Drainage: _doms().additionalDtl.chkDrainage.prop('checked'),
                    Tag_Toilets: _doms().additionalDtl.chkToilets.prop('checked'),
                    Tag_Electricity: _doms().additionalDtl.chkElecticity.prop('checked'),
                    Tag_Water: _doms().additionalDtl.chkWater.prop('checked'),
                    BoroughNo: _doms().additionalDtl.txtBoroughNo.val(),
                }
            },
            otherDtl: function () {
                return {
                    BuildingDtl: _doms().otherDtl.txtBuildingDtl.val()
                }
            },
            otherTabDtl: function () {
                return {
                    PrimaryPhNo: _doms().otherTabDtl.txtPhoneNo.val(),
                    PrimaryEmail: _doms().otherTabDtl.txtEmailId.val(),
                    ProCosingFees: (isNaN(_doms().otherTabDtl.txtPorCosingFees.val()) ? 0 : _doms().otherTabDtl.txtPorCosingFees.val()),
                    OtherFees: (isNaN(_doms().otherTabDtl.txtOtherFees.val()) ? 0 : _doms().otherTabDtl.txtOtherFees.val()),
                    //BuildingDtl: _doms().otherDtl.txtBuildingDtl.val(),
                    ApplictnDt: _doms().otherTabDtl.ApplictnDt.val(),
                    ApplictnCaseNo: _doms().otherTabDtl.ApplictnCaseNo.val(),
                    DtOfOrder: _doms().otherTabDtl.DtOfOrder.val()
                }
            }
        }
    };
    var validators = {
        validateHoldingSearch: function () {

            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().searchHolding.hdnWard.prop('name')] = { required: true };
            vldObj.rules[_doms().searchHolding.hdnLocation.prop('name')] = { required: true };
            vldObj.rules[_doms().searchHolding.hddnHoldingNo.prop('name')] = { required: true };

            //set error messages
            vldObj.messages[_doms().searchHolding.hdnWard.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms().searchHolding.hdnLocation.prop('name')] = { required: "Please enter Location." };
            vldObj.messages[_doms().searchHolding.hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().searchHolding.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().searchHolding.frm.valid();
        },
        validatePartialHolding: function () {
            $.validator.addMethod(
                "OwnerList",
                function (value, element, params) {
                    // otherwise, use your rule
                    if ($(".txt-assessee-name").parent().siblings('ul').find('li').length > 0) {
                        return true;
                    }
                    return false;
                },
                "Enter Valid Date"
            );
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            // vldObj.rules[_doms().partialHoldingDtl.ddlHoldingType.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.txtAddress.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: true };

            // vldObj.messages[_doms().partialHoldingDtl.ddlHoldingType.prop('name')] = { required: "Please select holding type" };
            vldObj.messages[_doms().partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: "Please holding type group" };
            vldObj.messages[_doms().partialHoldingDtl.txtAddress.prop('name')] = { required: "Please enter address" };
            vldObj.messages[_doms().partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: "Please add owner list" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().partialHoldingDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().partialHoldingDtl.frm.valid();
        },
        validateValuation: function () {
            $("#frm-valuation-details").validate().resetForm();
            _doms().frm.removeData('validator');
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];
            var sdh = $(".ddl-hodling-type-group").val();
            //set rules
            vldObj.rules[_doms().valuationDtl.ddlZone.prop('name')] = { required: true };
            vldObj.rules[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: true, number: true, range: [0, 15] };
            vldObj.rules[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: true, number: true, range: [0, 44] };
            vldObj.rules[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
            vldObj.rules[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
            vldObj.rules[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
            vldObj.rules[_doms().valuationDtl.txtLandCost.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.txtAge.prop('name')] = { required: true, number: true };


            //set error messages
            vldObj.messages[_doms().valuationDtl.ddlZone.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: "Please enter land area kt.", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: "Please enter land area ch.", number: "Data should be numeric", range: "Value sould be between 0 to 15" };
            vldObj.messages[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: "Please enter land area sft.", number: "Data should be numeric", range: "Value sould be between 0 to 44" };
            vldObj.messages[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: "Please select nature of use" };
            vldObj.messages[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: "Please enter bldg. area sft", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: "Please enter plinth sft", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: "Please select construction" };
            vldObj.messages[_doms().valuationDtl.txtLandCost.prop('name')] = { required: "Please enter land cost", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtAge.prop('name')] = { required: "Please enter age", number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().valuationDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().valuationDtl.frm.valid();
        },
        validateAdditional: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().additionalDtl.txtMouajaName.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtKhatianNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtKhatianNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDagNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDagNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.ddlHoldingArea.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDeedNo.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.ddlDeedType.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDeedDate.prop('name')] = { required: true };


            //set error messages
            vldObj.messages[_doms().additionalDtl.txtMouajaName.prop('name')] = { required: "Please enter Mouja Name." };
            vldObj.messages[_doms().additionalDtl.txtKhatianNoRs.prop('name')] = { required: "Please enter Khatian No RS" };
            vldObj.messages[_doms().additionalDtl.txtKhatianNoLr.prop('name')] = { required: "Please enter Khatian No LR" };
            vldObj.messages[_doms().additionalDtl.txtDagNoRs.prop('name')] = { required: "Please enter Dag No RS" };
            vldObj.messages[_doms().additionalDtl.txtDagNoLr.prop('name')] = { required: "Please select Dag No LR" };
            vldObj.messages[_doms().additionalDtl.ddlHoldingArea.prop('name')] = { required: "Please enter Holding Area" };
            vldObj.messages[_doms().additionalDtl.txtDeedNo.prop('name')] = { required: "Please enter Deed no" };
            vldObj.messages[_doms().additionalDtl.ddlDeedType.prop('name')] = { required: "Please select Deed Type" };
            vldObj.messages[_doms().additionalDtl.txtDeedDate.prop('name')] = { required: "Please enter Deed Date" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().additionalDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().additionalDtl.frm.valid();
        },
        validateOtherDtl: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //push validator object to tergate form
            _doms().otherDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().otherDtl.frm.valid();
        },
        validateOtherTabDtl: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().otherTabDtl.txtPorCosingFees.prop('name')] = { number: true };
            vldObj.rules[_doms().otherTabDtl.txtOtherFees.prop('name')] = { number: true };

            //set error messages
            vldObj.messages[_doms().otherTabDtl.txtPorCosingFees.prop('name')] = { number: "Data should be numeric" };
            vldObj.messages[_doms().otherTabDtl.txtOtherFees.prop('name')] = { number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().otherTabDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().otherTabDtl.frm.valid();
        }
    };
    var evtHandlers = {
        onChangeValuation: function () {
            if (validators.validateHoldingSearch() && validators.validateValuation()) {
                var valuationData = formDatas().valuationdata();
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/CalCulateValuationDetails",
                    data: JSON.stringify(valuationData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        if (r.Status === 200) {
                            var data = r.Data;
                            _doms().valuationDtl.txtReadOnlyAnnualValuation.val(data.AnnualValuation);
                            _doms().valuationDtl.txtReadOnlyQtrPtax.val(data.Qtr_Ptax);
                            _doms().valuationDtl.txtReadOnlyQtrSchrg.val(data.Qtr_Schrg);
                            _doms().valuationDtl.txtLandCost.val(data.LandCost);
                            _doms().valuationDtl.txtReadonlyQtrEduCess.val(data.EduCessAmt);
                        } else {
                            alertify.error(r.Message);
                        }
                    },
                    failure: function (response) {
                        alertify.error(response);
                    }
                });
            }
        }
    };
    var bindEvents = function () {
        $(document).on("click", "#valuationAddBtn", function () {

            var tempJson = {
                "HoldingTypeId": $('.ddl-holding-type').val(),
                "ZoneId": $('.ddl-zone').val(),
                "ZoneVal": $('.zoneText').text(),
                "LandArea_KT": $('.txt-land-area-kt').val(),
                "LandArea_CH": $('.txt-land-area-ch').val(),
                "LandArea_SFT": $('.txt-land-area-sft').val(),
                "NatureOfUseId": $('.ddl-nature-of-use').val(),
                "NatureOfUseVal": $('.NOUVal').text(),
                "BldgArea_SFT": $('.txt-bldg-area-sft').val(),
                "Plinth_SFT": $('.txt-plinth-sft').val(),
                "Age": $('.txt-age').val(),
                "ConstructionId": $('.ddl-construction').val(),
                "ConstructionVal": $('.constText').text(),
                "SrchgTag": $('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": $('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": $('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": $('.txt-readonly-qtr-schrg').val(),
                "LandCost": $('.txt-land-cost').val(),
                "Qtr_EduCess": $('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": $(".ddl-hodling-type-group").val(),
                "AssmtId": null,
                "AnulValDtlTabId": null,
                "EditMode": "I"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append($('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append($('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append($('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append($('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append($('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td class="hiddenAssmtID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "I" + '</td>');
            tr.append(td);

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);

            $('#valuation_details_table').find('>tbody').append(tr);
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            $('#valuation-details').modal('hide');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        });
        $(document).on('click', '.updateRow', function () {
            var row_index = $(this).closest('tr').index() + 2;
            $('.hiddenFieldForUdtate').val(row_index);
            $('#valuationAddBtn').hide();
            $('#valuationSaveBtn').show();
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            var rowDataJsonString = $(this).closest('tr').find('.completeJson').text();
            var rowDataJson = JSON.parse(rowDataJsonString);
            $('#valuation-details').modal('show');
            $('.ddl-holding-type').val(rowDataJson.HoldingTypeId),
                $('.ddl-zone').val(rowDataJson.ZoneId),
                $('.zoneText').text(rowDataJson.ZoneVal),
                $('.txt-land-area-kt').val(rowDataJson.LandArea_KT),
                $('.txt-land-area-ch').val(rowDataJson.LandArea_CH),
                $('.txt-land-area-sft').val(rowDataJson.LandArea_SFT),
                $('.ddl-nature-of-use').val(rowDataJson.NatureOfUseId),
                $('.NOUVal').text(rowDataJson.NatureOfUseVal),
                $('.txt-bldg-area-sft').val(rowDataJson.BldgArea_SFT),
                $('.txt-plinth-sft').val(rowDataJson.Plinth_SFT),
                $('.txt-age').val(rowDataJson.Age),
                $('.ddl-construction').val(rowDataJson.ConstructionId),
                $('.constText').text(rowDataJson.ConstructionVal),
                $(".chk-SrchgTag").prop("checked", rowDataJson.SrchgTag);
            $('.txt-readonly-annual-valuation').val(rowDataJson.AnnualValuation),
                $('.txt-readonly-qtr-ptax').val(rowDataJson.Qtr_Ptax),
                $('.txt-readonly-qtr-schrg').val(rowDataJson.Qtr_Schrg),
                $('.txt-land-cost').val(rowDataJson.LandCost),
                $('.txt-readonly-qtr-Edu-cess').val(rowDataJson.Qtr_EduCess),
                $('.hidden_assmt_id').val(rowDataJson.AssmtId),
                $('.hidden_tab_id').val(rowDataJson.AnulValDtlTabId)
        });
        $(document).on('click', '.rmvRow', function () {
            var row_index = $(this).closest('tr').index() + 2;

            var xy = $(this).closest('tr');
            var rowDataJsonString = xy.find('.completeJson').text();
            var rowDataJson = JSON.parse(rowDataJsonString);
            rowDataJson.EditMode = "D";
            rowDataJsonString = JSON.stringify(rowDataJson);
            xy.find('.completeJson').text(rowDataJsonString);
            $("#valuation_details_table tr:eq(" + row_index + ")").remove();

            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            if ($('#valuation_details_table > tbody > tr:first').length == 0) {
                $('#valuation_details_table').find('>tbody').append(xy).hide();
                $('#valuation_details_table > tbody > tr:first').find('.editMode').text("D");
            }
            else if ($('#valuation_details_table > tbody > tr').length >= 1 && row_index < 3) {
                xy.prependTo('#valuation_details_table').hide();
                $('#valuation_details_table > tbody > tr').eq(0).find('.editMode').text("D");
            }
            else {
                $("#valuation_details_table tr:eq(" + (row_index - 1) + ")").after(xy);
                $("#valuation_details_table tr:eq(" + row_index + ")").hide();
                $("#valuation_details_table tr:eq(" + row_index + ")").find('.editMode').text("D");
            }
            // xy.hide();

        });
        $(document).on("click", "#valuationSaveBtn", function () {
            var rowIndex = parseInt($('.hiddenFieldForUdtate').val());
            $("#valuation_details_table tr:eq(" + rowIndex + ")").remove();
            var tempJson = {
                "HoldingTypeId": $('.ddl-holding-type').val(),
                "ZoneId": $('.ddl-zone').val(),
                "ZoneVal": $('.zoneText').text(),
                "LandArea_KT": $('.txt-land-area-kt').val(),
                "LandArea_CH": $('.txt-land-area-ch').val(),
                "LandArea_SFT": $('.txt-land-area-sft').val(),
                "NatureOfUseId": $('.ddl-nature-of-use').val(),
                "NatureOfUseVal": $('.NOUVal').text(),
                "BldgArea_SFT": $('.txt-bldg-area-sft').val(),
                "Plinth_SFT": $('.txt-plinth-sft').val(),
                "Age": $('.txt-age').val(),
                "ConstructionId": $('.ddl-construction').val(),
                "ConstructionVal": $('.constText').text(),
                "SrchgTag": $('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": $('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": $('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": $('.txt-readonly-qtr-schrg').val(),
                "LandCost": $('.txt-land-cost').val(),
                "Qtr_EduCess": $('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": $(".ddl-hodling-type-group").val(),
                "AssmtId": $(".hidden_assmt_id").val(),
                "AnulValDtlTabId": $(".hidden_tab_id").val(),
                "EditMode": "U"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append($('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append($('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append($('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append($('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append($('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="hiddenAssmtID" hidden>' + $(".hidden_assmt_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + $(".hidden_tab_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "U" + '</td>');
            tr.append(td);

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);

            if ($('#valuation_details_table > tbody > tr:first').length == 0) {
                $('#valuation_details_table').find('>tbody').append(tr);
                $('#valuation_details_table > tbody > tr:first').find('.editMode').text("U");
            }
            else if ($('#valuation_details_table > tbody > tr').length >= 1 && rowIndex < 3) {
                tr.prependTo('#valuation_details_table > tbody');
                $('#valuation_details_table > tbody > tr').eq(0).find('.editMode').text("U");
            }
            else {
                $("#valuation_details_table tr:eq(" + (rowIndex - 1) + ")").after(tr);
                $("#valuation_details_table tr:eq(" + rowIndex + ")").find('.editMode').text("U");
            }
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            $('#valuation-details').modal('hide');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        });
        $(document).on('hidden.bs.modal', '#valuation-details', function () {
            $('.ddl-construction,.ddl-nature-of-use,.ddl-zone').val('');
            $('.ddl-holding-type').val(0);
            $('.txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-age,.txt-readonly-annual-valuation,.txt-land-cost,.txt-readonly-qtr-ptax,.txt-readonly-qtr-Edu-cess,.txt-readonly-qtr-schrg').val('0');
            $(".chk-SrchgTag").prop("checked", false);
            $('.zoneText,.NOUVal,.constText').text('0');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        })
        $(document).on('click', ".btn-load-review-form", function (e) {
            e.preventDefault();

            _doms().cntrDownloadedForm.empty();
            if (!validators.validateHoldingSearch()) {
                return;
            }
            var frmData = formDatas().searchedHoldingData();
            var payload = { ValTag: constants().getValTag(), LocationId: frmData.LocationId, wardId: frmData.WardId, HoldingNo: frmData.HoldingNo, YearId: constants().getYearId() };
            formLoader.Load(payload, function (r) {
                _doms().cntrDownloadedForm.append(r);
            });
        });

        //holding details
        $(document).on('focus', ".txtWard", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { Ward: request.term };
                    _doms().searchHolding.hdnWard.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetWards",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WardID,
                                            label: item.WardName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms().searchHolding.hdnWard.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txtLocation", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { WardId: _doms().searchHolding.hdnWard.val(), Location: request.term };
                    _doms().searchHolding.hdnLocation.val('');
                    if (!request.term || !_doms().searchHolding.hdnWard.val()) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetLocations",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.LocationID,
                                            label: item.LocationName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms().searchHolding.txtLocation.val(i.item.label);
                    _doms().searchHolding.hdnLocation.val(i.item.value);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txt-holding-no", function () {
            $(this).off('keyup');
            $(this).on('keyup', function () {
                $(".hddn-holding-no").val($(this).val());
            });
        });

        //valudation details
        $(document).on('change', ".ddl-zone", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-nature-of-use", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-construction", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-ownerOrOccupier", function () {
            if ($('.ddl-ownerOrOccupier').val() == 2) {
                $('.ownerOccupier').show();
            }
            else {
                $('.ownerOccupier').hide();
            }
        });
        $(document).on('change', ".txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-land-cost,.txt-age,.chk-SrchgTag,.chk-EduCessTag", function () {
            evtHandlers.onChangeValuation();
        });

        //save details
        $(document).on('click', '.btn-save', function (e) {
            e.preventDefault();
            if (!(validators.validateHoldingSearch() && validators.validatePartialHolding() /*&& validators.validateValuation()*/)) {
                $('.review').trigger('click');
                return false;
            }
            if (validators.validateHoldingSearch() && validators.validatePartialHolding() /*&& validators.validateValuation()*/ && validators.validateOtherTabDtl()) {
                //prepear payload
                var holdingDtl = formDatas().holdingDetails();
                //var valuationDtl = formDatas().valuationdata();
                var valuationDtl = formDatas().getValuationArrayData();
                var additionalHodlingData = formDatas().additionalHodlingData();
                var otherDtl = formDatas().otherDtl();
                var otherTabDtl = formDatas().otherTabDtl();
                var payload = {
                    FinYear: constants().effectiveFinYearQtr().ddlEffectiveFinYear.val(),
                    Qtr: constants().effectiveFinYearQtr().ddlEffectiveQtr.val(),
                    _YearID: constants().getYearId(),
                    AssessmentValTag: constants().getValTag(),
                    _HoldingDetails: holdingDtl,
                    _ValuationDetails: valuationDtl,
                    _OtherDetails: additionalHodlingData,
                    _OtherTabDetails: otherTabDtl,
                    _AdditionalHoldingDetails: additionalHodlingData
                };
                alertify.confirm("Confirm us!", "Are you sure to save data?", function () {
                    $.ajax({
                        type: 'post',
                        url: "/Municipality/Assessment/SaveReviewOrImporvement",
                        data: JSON.stringify(payload),
                        contentType: "application/json; charset=utf-8",
                        dataType: "html",
                        success: function (response) {
                            _doms().cntrDownloadedForm.empty().append(response);
                            $('.review').trigger('click');
                        },
                        failure: function (response) {
                            alert(response);
                        },
                        error: function (response) {
                            alert(response);
                        }
                    });
                }, function () { });
            }
        });
    };
    (function () {
        bindEvents();
    }())
});
function totalAnnualVal() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.AnnualValuation);
        }
    });
    $('.lbl-total-annual-vl').text(totalVal);
}
function totalEducess() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_EduCess);
        }
    });
    $('.lbl-total-end-cess').text(totalVal);
}
function totalQtrPtax() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Ptax);
        }
    });
    $('.lbl-total-qtr-ptax').text(totalVal);
}
function totalQtrScharge() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Schrg);
        }
    });
    $('.lbl-total-qtr-scharge').text(totalVal);
}
function totalLandCost() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.LandCost);
        }
    });
    $('.lbl-total-last-cost').text(totalVal);
}


