﻿var SubpProjFiles = [];
var fileModel = { aaFs: null, subProjs: [], insts: [] };
function InitializeSubProjId(id) {
    SubpProjFiles.push({ RandId: id, file: null });
}
function InitializeInstlId(id) {
    fileModel.insts.push({ file: null, isReadComplete: true, id: id.toString() });
}
$(document).ready(function () {
    var Server = function () {
        this.submitForm = function (obj, callback) {
            console.log(obj);
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SubmitOldDpr',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        },
        this.saveForm = function (obj, callback) {
            console.log(obj);
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SaveAsDraftOldDpr',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
    };
    var App = function (srv) {
        $(".dev-auth").prop("disabled", true);
        if($(".dev-auth").val() == "")
        {
            $(".dev-auth").prop("disabled", false);
        }
        var moduleC = null;
        var getFileModel = function (id, file, isCompleted) {
            return { file: file, isReadComplete: isCompleted, id: id };
        }
        var doms = function () {
            return {
                devAuth: $(".dev-auth"),
                hdp_MstDprId: $(".hdn-mstDprId"),
                hdn_IdentityStamp: $(".hdn-IdentityStamp"),
                txt_projectDate: $(".txt-project-date"),
                txt_dprApprovalDate: $(".txt-approval-date"),
                txt_workName: $(".txt-work-name"),
                txt_exeSummary: $(".txt-exe-summary"),
                txt_Remarks: $(".txt-remarks"),
                txt_implAgency: $(".txt-impl-agency"),
                txt_estCost: $(".txt-estimated-cost"),
                txt_RevEstCost: $(".txt-revised-estimated-cost"),
                hdn_schemeTypes: $(".hdn-SchemeTypes"),
                hdn_SchemeCategories: $(".hdn-SchemeCategories"),
                ddl_SchemeType: $(".ddl-scheme-types"),
                ddl_schemeCategory: $(".ddl-scheme-category"),
                txt_email: $('.txt-email'),
                txt_engName: $(".txt-engineer-name"),
                txt_designation: $(".txt-designation"),
                txt_contactNumber: $(".txt-contact-number"),
                txt_MemoDate: $(".txt-memoDate"),
                txt_CommencementDate: $(".txt-commencementDate"),
                txt_CompletionDate: $(".txt-completionDate"),
                txt_memoNumber: $(".txt-memo-no"),
                btn_submitForm: $(".btn-form-submit"),
                btn_saveForm: $(".btn-save-draft"),
                btn_resetForm: $(".btn-reset"),
                txt_approvedAmount: $(".txt-approved-amount"),
                txt_sanctionMemoNo: $(".txt-sanction-memo-no"),
                txt_uoNo: $(".txt-uo-no"),
                txt_accountHead: $(".txt-account-head"),
                txt_uoDate: $(".txt-uo-date"),
                tbl_subPorjects: $(".tbl-sub-projects"),
                txt_receivedAmt: $(".txt-fund-received")
            };
        }
        var moduleContainer = function () {
            p$modules = this;
            this.$expendutures;
            this.$subPorjects;
            this.$installments;
            
            var expendutures = function () {
                p$expendutures = this;
                var c$expendtureContainer = $(".tbl-phasing-expenditure");
                this.getFormData = function () {
                    return c$expendtureContainer
                        .find("tbody tr")
                        .map(function (i, t) {
                            t = $(t);
                            return { FinYear: t.find('.hdn-phasing-finyear').val(), ExpenditureAmount: t.find('.txt-prop-expenture-amnt').val(), Id: t.attr('data-PhasingId') };
                        }).get()
                        .map(function (m) {
                            m.ExpenditureAmount = (window.isNaN(m.ExpenditureAmount) ? 0 : m.ExpenditureAmount.toNumber());
                            return m;
                        });
                };
                this.validateAmount = function (amount) {
                    var amn = p$expendutures.getFormData().reduce(function (a, b) { return a + b.ExpenditureAmount; }, 0);
                    if (amount != amn) {
                        throw "Valid Phase of Expenditure required: Esitmated Cost and Expenditure amount should be same";
                    }
                };
                var reCalculateExpentureSummary = function () {
                    c$expendtureContainer.find('tfoot').empty();
                    if (c$expendtureContainer.find('tbody tr').length == 0) {
                        return;
                    }
                    var tr = $('<tr/>');
                    var totalExpendature = p$expendutures.getFormData().reduce(function (a, b) { return a + Number(isNaN(b.ExpenditureAmount) ? 0 : b.ExpenditureAmount); }, 0);
                    tr.append('<td><label>Total Project Cost</label></td><td class="text-right"><lable>' + totalExpendature + '</label><td/>')
                    c$expendtureContainer.find('tfoot').append(tr);
                };
                var _onClickAddYear = function (e) {
                    e.preventDefault();
                    var validateFinYear = function (finYear) {
                        var errorMsg = 'Invalid Fin-Year: ' + finYear;
                        var p = /\b\d{4}-\d{4}\b/; //dddd-dddd format
                        if (!p.test(finYear))
                            throw errorMsg;

                        var arr = finYear.split('-');
                        if (Number(arr[0]) >= Number(arr[1])) {
                            throw errorMsg;
                        }
                        if (Number(arr[0]) + 1 !== Number(arr[1])) {
                            throw errorMsg;
                        }
                    }
                    var addYearToFinYear = function (finyear, addBy) {
                        validateFinYear(finyear);
                        var arr = finyear.split('-');
                        return (Number(arr[0]) + Number(addBy)) + '-' + (Number(arr[1]) + Number(addBy));
                    }

                    var curFinYear = c$expendtureContainer.find(".txt-phasing-fin-year").val();
                    try {
                        var addBy = c$expendtureContainer.find('.ddl-add-year') .val();
                        var afterAddFinYear = addYearToFinYear(curFinYear, addBy);
                        c$expendtureContainer.find('tbody').append(
                            "<tr><td>" +
                            "<label>" + afterAddFinYear + "</label><input type='hidden' class='hdn-phasing-finyear' value='" + afterAddFinYear + "'/>" +
                            "</td><td>" +
                            "<input type='text' class='form-control txt-prop-expenture-amnt money' name='txt-prop-expenture-amnt' placeholder='enter amount in Rs.'/>" +
                            "</td><td><a href='javascript:void(0);' class='del-phasing-row'><span class='glyphicon glyphicon-trash'></span></a></td>" +
                            "</tr>"
                        );
                        reCalculateExpentureSummary();
                        c$expendtureContainer.find('.ddl-add-year >option[value="' + (Number(addBy) + 1) + '"]').prop('selected', true);
                    } catch (e) {
                        alertify.error(e);
                        c$expendtureContainer.find(".txt-phasing-fin-year").focus();
                    }

                };
                var bindEvent = function () {
                    c$expendtureContainer.on('click', ".del-phasing-row", function () {
                        $(this).closest('tr').remove();
                        reCalculateExpentureSummary();
                    });
                    c$expendtureContainer.on('keyup', '.txt-prop-expenture-amnt', function () {
                        reCalculateExpentureSummary();
                    });
                    c$expendtureContainer.find(".btn-add-year").on('click', _onClickAddYear);
                };
                (function () {
                    bindEvent();
                }());
            };
            var subPorjects = function () {
                p$subPorjects = this;
                var c$subPorjectsContainer = $(".tbl-sub-projects");
                //var c$instaModal = $(".modal-inst");
                this.validate = function () {
                    //check all file read complete
                    if (fileModel.subProjs.firstOrDefault(function (t) { return t.isReadComplete === false })) {
                        alertify.error('File Reading is running on subprojects');
                        return false;
                    }
                };
                this.getFormData = function () {
                    return c$subPorjectsContainer
                        .find("tbody tr")
                        .map(function (i, t) { 
                            t = $(t);
                            return {
                                _SKey:t.attr('data-id'),
                                Name: t.find('.txt-sub-project-Name').val(),
                                Cost: isNaN(t.find('.txt-sub-project-cost').val()) ? "0" : t.find('.txt-sub-project-cost').val(),
                                Qty: (isNaN(t.find('.txt-sub-project-qty').val()) ? "0" : t.find('.txt-sub-project-qty').val()),
                                UnitId: (isNaN(t.find('.sub-project-unit').val()) ? "0" : t.find('.sub-project-unit').val()),
                                RandId: t.attr('row-id'),
                                file: {},//fileModel.subProjs.firstOrDefault(function (ts) { return ts.id === t.attr('data-id') }).file
                                id: t.attr('data-sbpId'),                                
                                //here
                            }
                        }).get()
                        .map(function (m) {
                            m.Cost = (window.isNaN(m.Cost) ? 0 : m.Cost.toNumber());
                            return m;
                        });
                };
                var reCalculateExpentureSummary = function () {
                    c$subPorjectsContainer.find('tfoot').empty();
                    if (c$subPorjectsContainer.find('tbody tr').length == 0) {
                        return;
                    }
                    var tr = $('<tr/>');
                    var totalExpendature = p$subPorjects.getFormData().reduce(function (a, b) { return a + Number(isNaN(b.Cost) ? 0 : b.Cost); }, 0);
                    tr.append('<td colspan="5"><label>Total Project Cost</label></td><td colspan="3" class="text-right"><lable>' + totalExpendature + '</label></td>')
                    c$subPorjectsContainer.find('tfoot').append(tr);
                };
                var bindEvent = function () {
                    c$subPorjectsContainer.on('click', ".del-row-sub-project", function () {
                        var id = $(this).closest('tr').attr('row-id');
                        SubpProjFiles = $.grep(SubpProjFiles, function (e) {
                            return e.RandId != id;
                        });
                        var uid = $(this).closest('tr').attr('data-id');
                        $(this).closest('tr').remove();
                        fileModel.subProjs.remove(function (t) { return t.id === uid });
                        reCalculateExpentureSummary();
                        
                    });
                    c$subPorjectsContainer.on('keyup', '.txt-sub-project-cost', function () {
                        reCalculateExpentureSummary();
                    });
                    //c$subPorjectsContainer.on('click', '.btn-addinstallmnt', function () {
                    //    var id = $(this).closest('tr').attr('data-id');
                    //    //c$instaModal.find(".subproj-ref").val(id);
                    //    //c$instaModal.modal('show');
                    //});
                    //c$subPorjectsContainer.on('change', '.file-subproject', function () {
                    //    var file = this;
                    //    var id = $(this).closest('tr').attr('data-id');
                    //    var modFile = fileModel.subProjs.firstOrDefault(function (t) { return t.id === id });
                    //    if (modFile) {
                    //        modFile.isReadComplete = false;
                    //        if (file.files.length > 0) {
                    //            try {
                    //                window.getFileBase64(file.files[0], file.files[0].name, function (d) {
                    //                    modFile.file = d;
                    //                    modFile.isReadComplete = true;
                    //                });
                    //            } catch {
                    //                modFile.file = null;
                    //                modFile.isReadComplete = true;
                    //            }
                    //        } else {
                    //            modFile.file = null;
                    //            modFile.isReadComplete = true;
                    //        }
                    //    }
                    //});
                    c$subPorjectsContainer.on('click', '.btn-add-subproject', function (e) {

                        var getUnitControl = function () {
                            var units = JSON.parse($(".hdn-units").val());
                            var select = "<select class='form-control sub-project-unit' name='sub-project-unit'>";
                            select += "<option value=''>--select--</option>";
                            units.forEach(function (v, i) {
                                select += "<option value='" + v.Id + "'>" + v.Name + "</option>";
                            });
                            select += "</select>";
                            return select;
                        };
                        c$subPorjectsContainer.find('tbody tr.no-row').remove();
                        var _uuid = uuid();
                        var randId = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                        var randId1 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                        var FinalRandId = randId + randId1;
                        SubpProjFiles.push({ RandId: FinalRandId, file: null });
                        var str = "<tr row-id=" + FinalRandId +" data-installments='[]' data-id='" + _uuid + "'><td>" + (c$subPorjectsContainer.find('tbody tr').length + 1) + "</td><td>";
                        str += "<input type='text' class='form-control txt-sub-project-Name' name='txt-sub-project-Name' value='' placeholder='Sub Porject Name' />";
                        str += "</td>";

                        str += "<td  style='width: 11 %;'><input type='text' class='form-control txt-sub-project-qty money' name='txt-sub-project-qty' value='0' style='text-align:right' placeholder='Quantity' /></td>";
                        str += "<td  style='width: 19 %;'>" + getUnitControl() + "</td>";
                        str += "<td ><input type='text' class='form-control txt-sub-project-cost money' name='txt-sub-project-cost' value='' style='text-align:right' placeholder='Estimated Cost' />";
                        str += "<td><input type='file' accept='application/pdf' class='form-control file-subproject'/></td>";
                        //str += '<td class="text-center"><a href="javascript:void(0);" class="btn-addinstallmnt green" title="Add or Show Installments" style="font-size: 25px;color: green;"><span class="fa fa-plus"></span></a></td>';
                        str += "</td><td  ><a href='javascript:void(0);' class='del-row-sub-project'><span class='glyphicon glyphicon-trash'></span></a></td>";
                        str += "</tr>";
                        c$subPorjectsContainer.find('tbody').append(str);
                        fileModel.subProjs.push(getFileModel(_uuid, null, true));
                        reCalculateExpentureSummary();
                        
                    });
                };
                (function () {
                    bindEvent();
                }());
            };
            
            var installments = function () {
                p$Installment = this;
                var _tbl = $(".tbl-released-installments");
                var _tblSub = _tbl.find(".tbl-sub-inst");
                this.validate = function () {
                    //check all file read complete
                    if (fileModel.insts.firstOrDefault(function (t) { return t.isReadComplete === false })) {
                        alertify.error('File Reading is prending on Installments');
                        return false;
                    }
                };
                this.getFormData = function () {
                    return _tbl
                        .children("tbody").children('tr:not(.no-row)')
                        .map(function (i, t) {
                            t = $(t);
                            return {
                                InstallmentId: t.find('.ddl-installment').val(),
                                OrderNo: t.find('.txt-orderno').val(),
                                OderDate: t.find('.txt-orderdate').val() ? t.find('.txt-orderdate').val().toDate('dd/mm/yyyy'):null ,
                                SancDate: t.find('.txt-sanc-dt').val() ? t.find('.txt-sanc-dt').val().toDate('dd/mm/yyyy') : null,
                                SancNo: t.find('.txt-sanc-no').val(),
                                InstAmnt: t.find('.txt-sanc-amnt').val().toNumber(0),
                                //File: fileModel.insts.firstOrDefault(function (ts) { return ts.id === t.attr('data-id') }).file,
                                StageId: fileModel.insts.firstOrDefault(function (ts) { return ts.id === t.attr('data-id') })==null?0:fileModel.insts.firstOrDefault(function (ts) { return ts.id === t.attr('data-id') }).StageId,
                                SubProjects: t.find(".tbl-sub-inst tbody tr:not(.no-row)")
                                    .map(function (ii, tt) {
                                        return {
                                            _SKey: $(tt).find(".ddl-int-sub").val(),
                                            Amount: $(tt).find('.txt-amnt').val().toNumber(0),
                                            SurrenderAmount: $(tt).find('.txt-surrender-amnt').val().toNumber(0),
                                            sbpId: $(tt).attr('data-inssbpId'),
                                            TabId: $(tt).attr('data-TabId'),
                                            StageId: $(tt).attr('data-stageid')
                                            //here
                                        }
                                    }).get(),
                                FundReleaseId: t.attr('data-FundReleaseId')
                            }
                        }).get()
                        .map(function (m) {
                            return m;
                        });
                    };
                var reCalculateInstallmentSummary = function () {
                    console.log(_tbl.find('tfoot.tff'));
                    _tbl.find('tfoot.tff').empty();
                    if (_tbl.find('tbody tr').length == 1) {
                        return;
                    }
                    var tr = $('<tr/>');
                    var totalExpendature = p$Installment.getFormData().reduce(function (a, b) { return a + Number(isNaN(b.InstAmnt) ? 0 : b.InstAmnt); }, 0);
                    tr.append('<td colspan="5"><label>Total Released Amount</label></td><td colspan="3" class="text-right"><lable>' + totalExpendature + '</label></td>')
                    _tbl.find('tfoot.tff').append(tr);
                };
                var reCalculateInstallmentSubSummary = function (th) {
                    var _subtable = th.parent().parent().parent().parent();
                    var sum = 0;
                    var surrendersum = 0;
                    $.each(_subtable.find('.txt-amnt'), function (i, v) {
                        sum = sum + parseFloat(v.value);
                    });
                    $.each(_subtable.find('.txt-surrender-amnt'), function (i, v) {
                        surrendersum = surrendersum + parseFloat(v.value);
                    });
                    console.log(sum);
                    _subtable.find('tfoot').empty();
                    _subtable.append('<tfoot><tr><td>Total Amount</td><td>' + sum + '</td><td>' + surrendersum + '</td></tr></tfoot>')
                };
                _tbl.on('keyup', '.txt-sanc-amnt', function () {
                    reCalculateInstallmentSummary();
                });
                $(document).on('keyup', '.txt-amnt, .txt-surrender-amnt', function () {
                    reCalculateInstallmentSubSummary($(this));
                });
                
                _tbl.on('click', '.btn-add', function () {
                    _tbl.children('tbody').children('tr.no-row').remove();
                    var uId = uuid();
                    _tbl.children('tbody').append(getNewRow(uId));
                    fileModel.insts.push(getFileModel(uId, null, true));
                    reCalculateInstallmentSummary();
                });
                _tbl.on('click', '.del-row', function () {
                    var uid = $(this).closest('tr').attr('data-id');
                    $(this).closest('tr').remove();
                    if (_tbl.children('tbody').children('tr:not(.no-row)').length == 0) {
                        _tbl.children('tbody').append(getEmptyRow());
                    };
                    fileModel.insts.remove(function (t) { return t.id === uid });
                    reCalculateInstallmentSummary();
                });
                _tbl.on('change', '.file-upload', function () {
                    var xyz = $(this);
                    var file = this;
                    var id = $(this).closest('tr').attr('data-id');
                    var modFile = fileModel.insts.firstOrDefault(function (t) { return t.id === id });
                    if (modFile == undefined)
                        modFile = {};
                    if (modFile) {
                        if (this.files.length > 0 ) {
                            window.StageFileByFormData(this, function (response) {
                                modFile.StageId = response.Data;
                                modFile.Result = null;
                                modFile.Name = null;
                                alertify.success(response.Message);
                                //$(xyz).val('');
                                //$(xyz).css('border-color', '#5aef0d');
                            }, function (response) {
                                alertify.error(response.Message);
                                $(this).val('');
                            });
                        } 
                    }
                });

                var getInstallmentControl_ddl = function () {
                    var str = $("<select class='form-control ddl-installment input-xs'/>");
                    str.append("<option value='1'>1st Installment</option>");
                    str.append("<option value='2'>2nd Installment</option>");
                    str.append("<option value='3'>3rd Installment</option>");
                    str.append("<option value='4'>4th Installment</option>");
                    str.append("<option value='5'>5th Installment</option>");
                    str.append("<option value='6'>6th Installment</option>");
                    str.append("<option value='7'>7th Installment</option>");
                    str.append("<option value='8'>8th Installment</option>");
                    str.append("<option value='9'>9th Installment</option>");
                    str.append("<option value='10'>10th Installment</option>");
                    str.append("<option value='11'>11th Installment</option>");
                    str.append("<option value='12'>12th Installment</option>");
                    str.append("<option value='13'>13th Installment</option>");
                    str.append("<option value='14'>14th Installment</option>");
                    str.append("<option value='15'>15th Installment</option>");
                    str.append("<option value='16'>16th Installment</option>");
                    str.append("<option value='17'>17th Installment</option>");
                    str.append("<option value='18'>18th Installment</option>");
                    str.append("<option value='19'>19th Installment</option>");
                    str.append("<option value='20'>20th Installment</option>");
                    str.append("<option value='21'>21th Installment</option>");
                    str.append("<option value='22'>22th Installment</option>");
                    str.append("<option value='23'>23th Installment</option>");
                    str.append("<option value='24'>24th Installment</option>");
                    str.append("<option value='25'>25th Installment</option>");
                    return str;
                };
                
                var getEmptyRow = function () {
                    return '<tr class="no-row"><td class="text-center" colspan="10">Please add Installments, If Available!</td></tr>';
                }
                var getNewRow = function (uId) {
                    
                    var str = $("<tr />");
                    str.attr('data-id', uId);

                    var td_srl = $("<td/>");
                    td_srl.text((_tbl.children('tbody').children('tr').length + 1));
                    str.append(td_srl);

                    var td_installmentNo = $("<td/>");
                    td_installmentNo.append(getInstallmentControl_ddl());
                    str.append(td_installmentNo);

                    var td_orderNo = $("<td/>");
                    td_orderNo.append("<input type='text' class='form-control txt-orderno input-xs' placeholder='order no'/>");
                    str.append(td_orderNo);

                    var td_orderdate = $("<td/>");
                    td_orderdate.append("<input type='text' class='form-control txt-orderdate input-xs datepicker' readonly placeholder='order date'/>");
                    str.append(td_orderdate);

                    var td_sanctionedno = $("<td/>");
                    td_sanctionedno.append("<input type='text' class='form-control input-xs txt-sanc-no' placeholder='release order no.'/>");
                    str.append(td_sanctionedno);

                    var td_sanctioneddt = $("<td/>");
                    td_sanctioneddt.append("<input type='text' class='form-control input-xs txt-sanc-dt datepicker' readonly placeholder='release order date'/>");
                    str.append(td_sanctioneddt);

                    var td_sanctionedAmnt = $("<td/>");
                    td_sanctionedAmnt.append("<input type='text' class='form-control input-xs txt-sanc-amnt money' placeholder='sanction amount'/>");
                    str.append(td_sanctionedAmnt);

                    var td_fileUpload = $("<td/>");
                    td_fileUpload.append("<input type='file' accept='application/pdf' class='form-control input-xs file-upload'/>")
                    str.append(td_fileUpload);

                    var td_sub = $("<td style='width: 40%;'/>");
                    td_sub.append(getNewSubprojtable(uId));
                    str.append(td_sub);

                    var td_delrow = $("<td/>");
                    td_delrow.append('<a href="javascript:void(0);" class="del-row"><span class="glyphicon glyphicon-trash"></span></a>')
                    str.append(td_delrow);

                    return str;
                };

                /***
                    Subprojects in installments
                ****/
                var getSubPorjectsDdl = function (selectedId)//returns array
                {
                    var dd = $("<select class='form-control ddl-int-sub input-xs'><option value=''>-select-</option>");
                    $(".tbl-sub-projects tbody tr:not(.no-row)").map(function (i, v) {
                        return {
                            id: ($(v).attr("data-id") != "0" && $(v).attr("data-id") != "undefined") ? $(v).attr("data-id") : $(v).attr("row-id"),
                            value: $(v).find(".txt-sub-project-Name").val()
                        }

                    }).get()
                        .forEach(function (v,i) {
                            if (v.id == selectedId) {
                                dd.append("<option selected value='" + v.id + "'>" + v.value + "</option>");
                            } else {
                                dd.append("<option value='" + v.id + "'>" + v.value + "</option>");
                            }

                        });
                    return dd;
                };
                var subprojectRow = function () {
                    var obj = $("<tr/>");

                    var td1 = $('<td/>');
                    td1.append(getSubPorjectsDdl());
                    obj.append(td1);

                    var td2 =$('<td/>');
                    td2.append("<input type='text' class='form-control input-xs txt-amnt money' placeholder='amount'/>");
                    obj.append(td2);

                    var td2 = $('<td/>');
                    td2.append("<input type='text' class='form-control input-xs txt-surrender-amnt money' placeholder='surrender amount'/>");
                    obj.append(td2);

                    var td2 = $('<td/>');
                    td2.append("<input type='file' accept='application / pdf' class='form-control input-xs surrender-file-upload' />");
                    obj.append(td2);

                    var td2 = $('<td/>');
                    td2.append('<a href="javascript:void(0);" class="del-row1"><span class="glyphicon glyphicon-trash"></span></a>');
                    obj.append(td2);
                    return obj;
                };
                var getEmptyRow1 = function () {
                    return "<tr class='no-row'><td colspan='3' class='text-center'>please add subprojects</td></tr>";;
                };
                var getNewSubprojtable = function (uId) {
                    var str = "<table class='table table-responsive table-bordered table-condensed tbl-sub-inst mb-0 " + uId+"'><thead><tr class='bg-aqua'>";
                    str += "<th style='width: 27%;'>Project</th>";
                    str += "<th>Amount</th>";
                    str += "<th>Surrender Amount (If any)</th>";
                    str += "<th style='width: 24%;'>SC/TC(PDF)</th>";
                    str += '<th><a href="javascript:void(0);" class="btn-add1 white" style="font-size: 25px;"><span class="fa fa-plus"></span></a></th></tr>';
                    str += "</thead><body>";
                    str += getEmptyRow1();
                    str += "</tbody>";
                    str += "</table>";
                    return str;
                };

                _tbl.on('click', '.tbl-sub-inst .btn-add1', function () {
                    $(this).closest('table').find('tbody tr.no-row').remove();
                    //if (_tbl.find('tbody tr:not(.no-row)').length == 0) {
                    //    _tbl.find('tbody').append(getEmptyRow());
                    //};
                    $(this).closest('table').find('tbody').append(subprojectRow());
                   // reCalculateInstallmentSubSummary();
                });
                _tbl.on('click', '.tbl-sub-inst .del-row1', function () {
                    var th = $(this);
                    var v = $(this).closest('tr').find('.txt-amnt')[0].value;
                    var v1 = $(this).closest('tr').find('.txt-surrender-amnt')[0].value;
                    var _subtable = $(this).closest('table');
                    var sum = 0;
                    var surrendersum = 0;
                    $.each(_subtable.find('.txt-amnt'), function (i, v) {
                        sum = sum + parseFloat(v.value);
                    });
                    $.each(_subtable.find('.txt-surrender-amnt'), function (i, v) {
                        surrendersum = surrendersum + parseFloat(v.value);
                    });
                    console.log(sum);
                    _subtable.find('tfoot').empty();                    
                    _subtable.append('<tfoot><tr><td>Total Amount</td><td>' + (sum - parseFloat(v)) + '</td><td>' + (surrendersum - parseFloat(v1)) + '</td></tr></tfoot>')
                    $(this).closest('tr').remove();
                    if (_subtable.find('tbody tr').length == 0) {
                        _subtable.find('tfoot').empty();
                    }
                    if ($(this).closest('table').find('tbody tr:not(.no-row)').length == 0) {
                        $(this).closest('table').find('tbody').append(getEmptyRow1());
                    };
                    
                });
            };
            
            this.register = function () {
                p$modules.$expendutures = new expendutures();
                p$modules.$subPorjects = new subPorjects();
                p$modules.$installments = new installments();
            };
        };
        var _eventHandlers = {
            _onSubmitForm: function (e) {
                e.preventDefault();
                if (!$('form').valid()) {
                    var scrollTo = $(".error:nth(0)");
                    if (scrollTo.length > 0) {
                        $([document.documentElement, document.body]).animate({
                            scrollTop: scrollTo.offset().top
                        }, 200);
                    }
                    alertify.error('please enter data on mandetory fields');
                    return;
                }
                console.log('form data gathered')
                var formdata = getFormData();
                //if (formdata.ReceivedAmt > 0 && $('.file-utilization-certificate')[0].files.length == 0) {
                //    alertify.alert("Please upload utilization certificate.");
                //    return false;
                //}
                if (formdata.EstimatedCost < formdata.RevisedEstimatedCost) {
                    alertify.error("Revised estimated cost can not be less than estimated cost.");
                    return false;
                }
                alertify.confirm("Confirm us!", "Are you sure to submit?", function () {
                    console.log('Confirm Yes')
                    try {
                        moduleC.$expendutures.validateAmount(formdata.EstimatedCost);
                    } catch (e) {
                        alertify.error(e);
                        return;
                    }
                    console.log('exp validated')
                    formdata.PhasingOfExpenditures = moduleC.$expendutures.getFormData();
                    console.log('exp added')
                    formdata.SubProjects = moduleC.$subPorjects.getFormData();
                    console.log('subproject added')
                    console.log(formdata);
                    console.log('preparing for fire ajax')
                    srv.submitForm(formdata, function (e) {
                        if (e.Status == 200) {
                            console.log('success ajax fired')
                            alertify.alert('Successful', e.Message, function () {
                                if (formdata.DPChkSlId != 0) {
                                    location.reload();
                                } else {
                                    location.href = "/Modules/Dpr/OldDprEntryForm";
                                }
                                
                            });
                        } else {
                            alertify.error(e.Message);
                        }
                    });
                }, function () { });
            },
            _onSaveForm: function (e) {
                e.preventDefault();
                console.log('form data gathered')
                var formdata = getFormData();
                alertify.confirm("Confirm us!", "Are you sure to save as draft?", function () {
                    console.log('Confirm Yes')
                    formdata.PhasingOfExpenditures = moduleC.$expendutures.getFormData();
                    formdata.SubProjects = moduleC.$subPorjects.getFormData();
                    srv.saveForm(formdata, function (e) {
                        if (e.Status == 200) {
                            alertify.alert('Successful', e.Message, function () {
                                if (formdata.DPChkSlId != 0) {
                                    location.reload();
                                } else {
                                    location.href = "/Modules/Dpr/OldDprEntryForm";
                                }

                            });
                        } else {
                            alertify.error(e.Message);
                        }
                    });
                }, function () { });
            },
            _onResetForm: function (e) {
                location.reload();
            },
            _onChangeSchemeType: function () {
                var schemeCategories = JSON.parse(doms().hdn_SchemeCategories.val());
                console.log(schemeCategories);
                doms().ddl_schemeCategory.find("option:not(:first-child)").remove();
                schemeCategories.filter(t => t.SchemeTypeId == $(this).val()).forEach(function (val) {
                    doms().ddl_schemeCategory.append('<option value="' + val.Id + '">' + val.Name + '</option>')
                })
            }

        };
        var doc = null;
        var moduleC = null;
        var AAFSFile = null;
        var bindEvents = function () {
           
            function getBase64(file, name, callback) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    callback({ Result: reader.result, Name: name });
                };
                reader.onerror = function (error) {
                    alertify.error('Unable to read file');
                };
            }; 
            doms().btn_submitForm.on('click', _eventHandlers._onSubmitForm);
            doms().btn_saveForm.on('click', _eventHandlers._onSaveForm);
            doms().btn_resetForm.on('click', _eventHandlers._onResetForm);
            doms().ddl_SchemeType.on('change', _eventHandlers._onChangeSchemeType);
            $(document).on('click', ".date-remove", function () {
                var target = $(this).siblings("input.datepicker");
                if (target.length > 0) {
                    target.val('');
                }
            });
            $(document).on('change', ".hasSubProj", function () {
                if ($(this).is(":checked")) {
                    $('.subProjTbl').css("display", "block");
                }
                else {
                    $('.subProjTbl').css("display", "none");
                }
            });
            $(".file-utilization-certificate").on('change', function () {
                doc = null;
                if (this.files.length > 0) {
                    var file = getBase64(this.files[0], this.files[0].name, function (data) {
                        doc = data;
                    });
                } 
            });
            $(document).on('change', ".file-aa-fs", function () {
                var xyz = $(this);
                AAFSFile = {};
                if (this.files.length > 0 ) {
                    window.StageFileByFormData(this, function (response) {
                        AAFSFile.StageId = response.Data;
                        AAFSFile.Result = null;
                        AAFSFile.Name = null;
                        alertify.success(response.Message);
                        //$(xyz).val('');
                        //$(xyz).css('border-color', '#5aef0d');
                       
                    }, function (response) {
                        alertify.error(response.Message);
                        $(this).val('');
                    });
                } 
                //if (this.files.length > 0) {
                //    var file = getBase64(this.files[0], this.files[0].name, function (data) {
                //        AAFSFile = data;
                //        $.ajax({
                //            type: 'post',
                //            url: '/Modules/dpr/UploadImage',
                //            data: JSON.stringify(AAFSFile),
                //            contentType: "application/json; charset=utf-8",
                //            dataType: "json",
                //            success: function (response) {
                //                if (response.Status == 200) {
                //                    AAFSFile.StageId = response.Data;
                //                    AAFSFile.Result = null;
                //                    AAFSFile.Name = null;
                //                }
                //                else {
                //                    alertify.alert(response.Message);
                //                }
                //            },
                //            error: function (xhr, ajaxOptions, thrownError) {

                //            }
                //        });
                //    });
                //}
            });
            $(document).on('change', ".file-subproject", function () {
                var $tr = $(this).closest('tr');
                $tr.attr('data-StageId', null);
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        $tr.attr('data-StageId', response.Data);
                        alertify.success(response.Message);
                    }, function (response) {
                        alertify.error(response.Message);
                        (this).val('');
                    });
                } 

                //var xyz = $(this);
                //var id = $(this).closest('tr').attr('row-id');                
                //var checkFile = SubpProjFiles.filter(function (v) {
                //    return v.RandId == id;
                //});
                //if (checkFile.length == 0)
                //    checkFile[0] = {};
                //if (this.files.length > 0) {
                //    window.StageFileByFormData(this, function (response) {
                //        checkFile[0].StageId = response.Data;
                //        checkFile[0].Result = null;
                //        checkFile[0].Name = null;
                //        alertify.success(response.Message);
                //        //$(xyz).val('');
                //        //$(xyz).css('border-color', '#5aef0d');
                //    }, function (response) {
                //        alertify.error(response.Message);
                //        $(this).val('');
                //    });
                //} 
            });
            $(document).on('change', ".surrender-file-upload", function () {
                var $tr = $(this).closest('tr');
                $tr.attr('data-StageId', null);
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        $tr.attr('data-StageId', response.Data);
                        alertify.success(response.Message);
                    }, function (response) {
                        alertify.error(response.Message);
                        (this).val('');
                    });
                }
            });
            $(document).on('input', '.txt-revised-estimated-cost', function () {
                $('.txt-estimated-cost').val($('.txt-revised-estimated-cost').val());
            });
            $(document).on('mouseover', '.txt-sanc-no, .txt-orderno, .txt-sanc-amnt, .txt-amnt, .txt-surrender-amnt, .txt-sub-project-Name, .txt-sub-project-cost', function () {
                $(this).css('cursor', 'pointer').attr('title', $(this).val());
                $(this).css('cursor', 'auto');
            });
            $(document).on('mouseover', '.ddl-int-sub', function () {
                $(this).css('cursor', 'pointer').attr('title', $(this).find("option:selected").text());
                $(this).css('cursor', 'auto');
            });
        }
        var registerValidator = function () {
            var status = $(".hdn-status").val();
            //add validate method
            $.validator.addMethod('greaterThan', function (value, el, param) {
                return value > param;
            });
            $.validator.addMethod('greaterThanEqual', function (value, el, param) {
                return value >= param;
            });
            $.validator.addMethod('lessThanEqual', function (value, el, param) {
                if (param.apply == true)
                    return value <= param.value;
                return true;
            });
            $.validator.addMethod('mobile', function (value, el, param) {
                if (param === false)
                    return true;
                var r = /^(\+\d{1,3}[- ]?)?\d{10}$/;
                return r.test(value);
            });

            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    if (element.siblings(".input-group-addon").length > 0) {
                        error.insertAfter(element.parent());
                        error.css('color', 'red');
                        return;
                    }
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            }

            //impl rules
            vld.rules[doms().txt_contactNumber.attr('name')] = {
                mobile: function () {
                    return (doms().txt_contactNumber.val().trim().length > 0);
                }
            };
            vld.rules[doms().devAuth.attr('name')] = { required: true };
            //vld.rules[doms().txt_workName.attr('name')] = { required: true };
            //vld.rules[doms().txt_implAgency.attr('name')] = { required: true };
            vld.rules[doms().txt_estCost.attr('name')] = { required: true, number: true, min: 0, greaterThan: 0 };
            vld.rules[doms().txt_RevEstCost.attr('name')] = { required: true, number: true, min: 0, greaterThan: 0 };
            //vld.rules[doms().txt_engName.attr('name')] = { required: true };
            //vld.rules[doms().txt_designation.attr('name')] = { required: true };
            vld.rules[doms().txt_MemoDate.attr('name')] = { required: true };
            //vld.rules[doms().txt_memoNumber.attr('name')] = { required: true };
            vld.rules[doms().ddl_schemeCategory.attr('name')] = { required: true };
            vld.rules[doms().ddl_SchemeType.attr('name')] = { required: true };
            vld.rules[doms().txt_receivedAmt.attr('name')] = { number: true };
            vld.rules[doms().txt_sanctionMemoNo.attr('name')] = { required: true };
            vld.rules[doms().txt_email.attr('name')] = { email: function () { return doms().txt_email.val().trim().length > 0; } };
            //vld.rules[doms().txt_projectNo.attr('name')] = { required: true };
            vld.rules[doms().txt_projectDate.attr('name')] = { required: true };
            //vld.rules[doms().txt_dprVerificationDate.attr('name')] = { required: true };
            vld.rules[doms().txt_dprApprovalDate.attr('name')] = { required: function () { return status == "A"; } };
            vld.rules[doms().txt_approvedAmount.attr('name')] = {
                required: function () { return status == "A"; }, lessThanEqual: function ()
                {
                    return { apply: status == "A", value: (doms().txt_estCost.val().toNumber() || 0) };
                }, min: 0
            };

            //impl mssgs
            vld.messages[doms().txt_contactNumber.attr('name')] = { mobile: "Invalid contact no" };
            vld.messages[doms().txt_estCost.attr('name')] = { greaterThan: "Invalid estimated cost" };
            vld.messages[doms().txt_RevEstCost.attr('name')] = { greaterThan: "Invalid revised estimated cost" };
            vld.messages[doms().txt_approvedAmount.attr('name')] = { greaterThan: "Invalid estimated cost", greaterThanEqual: "Invalid estimated cost", lessThanEqual:"Invalid estimated cost" };
            $('form').validate(vld);
        }
        var getFormData = function () {
            var getSubprojects = function () {
                var $cntr = $(".subProjDiv .tbl-sub-projects");
                return $cntr.find('tbody tr').map(function (i, v) {
                    var $tr = $(v);
                    return { RandId: $tr.attr('row-id'), StageId: $tr.attr('data-stageid') };

                }).get();
            };
            var convertToDate = function (strDate) {
                if (!window.isNullOrEmpty(strDate, null)) {
                    return null;
                }
                var arr = strDate.split('/');
                return new Date(arr[2], arr[1] - 1, arr[0]);
            }
            return {
                DPChkSlId: doms().hdp_MstDprId.val(),
                MunicipalityId: doms().devAuth.val(),
                IdentityStamp: doms().hdn_IdentityStamp.val(), 
                NameOfWork: doms().txt_workName.val().trim() || null,
                ExecutiveSummary: doms().txt_exeSummary.val().trim() || null,
                Remarks: doms().txt_Remarks.val().trim() || null, 
                AgencyName: doms().txt_implAgency.val().trim() || null,
                EstimatedCost: Number(isNaN(doms().txt_estCost.val()) ? 0 : doms().txt_estCost.val()),
                RevisedEstimatedCost: Number(isNaN(doms().txt_RevEstCost.val()) ? 0 : doms().txt_RevEstCost.val()),
                MemoDate: convertToDate(doms().txt_MemoDate.val()),
                CommencementDate: convertToDate(doms().txt_CommencementDate.val()),
                CompletionDate: convertToDate(doms().txt_CompletionDate.val()),
                MemoNo: doms().txt_memoNumber.val().trim() || null,
                SchemeTypeId: doms().ddl_SchemeType.val().isNanThenZero(),
                SchemeCategoryId: doms().ddl_schemeCategory.val().isNanThenZero(),
                EmailId: doms().txt_email.val().trim() || null,
                NameOfEngineer: doms().txt_engName.val().trim() || null,
                Designation: doms().txt_designation.val().trim() || null,
                ContactNo: doms().txt_contactNumber.val().trim() || null,
                ProjectNo:  null,
                Projectdate: convertToDate(doms().txt_projectDate.val()),
                DprVerifiedOn: convertToDate(doms().txt_projectDate.val()), //convertToDate(doms().txt_dprVerificationDate.val()),
                DprApprovalDate: convertToDate(doms().txt_dprApprovalDate.val()),
                ApprovedAmount: doms().txt_approvedAmount.val().toNumber() || 0,
               // DprSubmissionDate: convertToDate(doms().txt_dprSubmissionDate.val()),
                SanctionMemoNo: doms().txt_sanctionMemoNo.val(),
                AccountHeadNo: doms().txt_accountHead.val(),
                UONo: doms().txt_uoNo.val(),
                UODate: convertToDate(doms().txt_uoDate.val()),
                HasSubProj: true,
                ReceivedAmt: 0,
                file: doc,
                AAFSFile: AAFSFile,
                SubProjects: moduleC.$subPorjects.getFormData(),
                Installments: moduleC.$installments.getFormData(),
                SubpProjFiles: getSubprojects() //SubpProjFiles
            }
        }
        this.Init = function () {
            bindEvents();
            registerValidator();
            moduleC = new moduleContainer();
            moduleC.register();
        };
    };
    (function () {
        var srv = new Server();
        var app = new App(srv);
        app.Init();
    }());
});
