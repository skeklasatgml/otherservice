﻿$(document).ready(function(){
    var populateScheCats = function () {
        var populateTable = function (rows) {
            var $table = $(".table-scheme-cat");
            $table.find('tbody').empty();
            if (rows.length > 0) {
                rows.forEach(function (v,i) {
                    var $tr = $("<tr/>");
                    var $tdDscr = $('<td class="pd-3"/>');
                    $tdDscr.text(v.Name);
                    $tr.append($tdDscr);

                    var $check = $('<div class="checkbox m-0" ><label><input type="checkbox" value="' + v.Id + '" class="check"></label></div>');
                    $check.find('input').prop('checked', v.IsMapped);
                    var $td = $('<td class="pd-3"/>');
                    $td.append($check);
                    $tr.append($td);
                    $table.find('tbody').append($tr);
                })
            } else {
                $table.find('tbody').append("<tr><td colspan='2' class='text-center'>No data available</td></tr>");
            }
        };
        var schemeTypeId = $(".ddl-scheme-type").val();
        var groupId = $(".ddl-group").val();
        var data = { schemeTypeId: schemeTypeId, groupId: groupId };
        $.ajax({
            url: "/Modules/DprExtension/GetMileStoneGroupedMappedSchemeCats",
            method: "Post",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (e) {
                if (e.Status === 200) {
                    populateTable(e.Data);
                } else {
                    alertify.error(e.Message);
                }
            },
            error: function (e) {
                alertify.error(e.responseText || e.statusText);
            }
        });
    };

    $(document).on('change', '.ddl-scheme-type,.ddl-group', function () {
        populateScheCats();
    });
    $(document).on('change', '.table-scheme-cat tbody tr .check', function () {
        var checkbx = $(this);//int? scemeCatId, int? groupId,bool isMapped
        var scemeCatId = checkbx.val();
        var groupId = $(".ddl-group").val();
        var data = { scemeCatId: scemeCatId, groupId: groupId, isMapped: checkbx.prop('checked') };
        $.ajax({
            url: "/Modules/DprExtension/updateMappingSchemeteMilestoneGrp",
            method: "Post",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (e) {
                if (e.Status === 200) {
                    populateScheCats();
                    alertify.success(e.Message);
                } else {
                    alertify.error(e.Message);
                }
            },
            error: function (e) {
                alertify.error(e.responseText || e.statusText);
            }
        });
    });
    $(".ddl-group").trigger('change');
});