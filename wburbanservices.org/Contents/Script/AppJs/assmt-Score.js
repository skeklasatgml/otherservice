﻿


$(function () {
    app.ddlMunicipality();
    app.getYear();
    app.belTypeValue();
    //app.GetAssmtScoreSheet();
    $('#btnGo').click(function () {
        app.showEffecdate();
        app.GetAssmtScoreSheet();
        app.GetAssmtBeltSheet();

    });
    $('#btnSubmit').click(function () {
        app.saveAssmtScoreSheet();
        app.saveAssmtBeltSheet();
    });
    $('#ddlyear').change(function () {
        //alert('hI')
        //app.showEffecdate();
    });
    app.accordion();
})

var app = {
    options: [],
    optionResponse:[],
    belTypeValue: function () {
        $.ajax({
            url: '/Municipality/Score/GetBeltTypevalue',
            type: 'POST',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.optionResponse = response.Data;

                $(response.Data).each(function (i, element) {
                    app.options.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
                });
                //console.log(app.options);
            },
            error: function () { }
        });
    },
    showEffecdate: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Municipality/Score/GetEffectiveDate',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(JSON.stringify(response));
                //console.log(response.Data)
                //console.log(JSON.stringify(response.Data));
                //console.log(JSON.parse(response.Data));
                //app.getScoreSheet(response.Data);
                //var fdate = response.Data[0][element.EffectiveDateTo]
                if (response.Data.length > 0) {
                    console.log(response.Data);
                    console.log(response.Data[0]["EffectiveDateForm"]);
                    console.log(response.Data[0]["EffectiveDateTo"]);
                    $('#fromdate').html(CONVERTER.Date.convertCsToJsDate(response.Data[0]["EffectiveDateForm"]).format('dd/mm/yy'));
                    $('#todate').html(new Date(parseInt((response.Data[0]["EffectiveDateTo"]).replace(/[^0-9 +]/g, ''))).format('dd/mm/yy'));
                }
                
            },
            error: function () { }
        });
    },
    GetAssmtBeltSheet: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Municipality/Score/GetAssmtBeltSheet',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                app.beltData(response.Data);
                //app.BeltDataDyn(response.Data);
                //app.BeltdataDynsDirectResponse(response.Data);
            },
            error: function () { }
        });
    },
    beltData: function (data) {
        $('#AssmtBeltSheet').empty();
        $('#AssmtBeltSheet').append('<thead><tr>'
            + '<th style=display:none;>BeltSheetID </th>'
            + '<th>Score Belt Type </th>'
            + '<th>From(>) K </th>'
            + '<th>Upto(<=) K </th>'
            +' <th>% Consider </th>'
            +' </tr></thead>');
        $(data).each(function (i, element) {
                $('#AssmtBeltSheet').append('<tbody><tr>'
                + '<td style=display:none;>' + element.BeltSheetID + '</td>'
                + '<td>' + element.ScoreBeltTypeDesc + '</td>'
                + '<td> <input type=text class=txtbeltfrom value=' + element.FromRange + '></td>'
                + '<td> <input type=text class=txtbeltto value=' + element.ToRange + '></td>'
                + '<td><select id=per_' + element.BeltSheetID +' class=txtbeltval></td>'
                    + '</tr></tbody>');
            $('#per_' + element.BeltSheetID).html(app.options.join(''));
            $('#per_' + element.BeltSheetID).val(element.BeltValue == '' ? 0 : element.BeltValue);
        });
        
    },
    BeltDataDyn: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>BeltSheetID </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>From(>) K </th>');
        Html.push('<th>Upto(<=) K </th>');
        Html.push('<th>% Consider </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        var optHtmlObj = [];
        $(data).each(function (i, element) {

            Html.push('<tbody><tr>')
            Html.push('<td style=display:none;>' + element.BeltSheetID + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            //Html.push('<td>' + element.FromRange+' ' +element.BeltUnit + '</td>');
            //Html.push('<td>' + element.ToRange + ' ' + element.BeltUnit + '</td>');
            Html.push('<td> <input type=text class=txtbeltfrom value=' + element.FromRange + ' ></td>');
            Html.push('<td> <input type=text class=txtbeltto value=' + element.ToRange + ' ></td>');
            //Html.push('<td>' + element.BeltValue + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            //Html.push('<td> <input type=text class=txtbeltval value=' + element.BeltValue + '  ></td>');


            var optHTML = [];
            optHtmlObj = $.parseHTML(app.options.join(''));
            $(optHtmlObj).each(function (x, opt) {
             //   alert($(opt).val());
                if ($(opt).val() == element.BeltValue) {
                   // $(opt).attr('selected', 'true');
                    optHTML.push('<option value=' + element.BeltValue + ' selected>' + $(opt).html() +'</option>')
                } else {
                    optHTML.push('<option value=' + element.BeltValue + '>' + $(opt).html() + '</option>')
                }
            })

            Html.push('<td><select  id=per_' + element.BeltSheetID + ' class=txtbeltval>' + optHTML.join('') +'</select></td>');
            Html.push('</tr></tbody>');
           
            //alert(element.BeltValue == '' ? 0 : element.BeltValue);
          //  $('#per_' + element.BeltSheetID).html();

            //var ddlId = '#per_' + element.BeltSheetID;
            //ddlId = $(ddlId);
            //$.each(app.options, function (ind, val) {
            //    $(ddlId).append(val);
            //})

            //$(ddlId).html(app.options.join(''));
            //$(ddlId).val(element.BeltValue);
            //Html.clone();
        });
        //$('#AssmtBeltSheet').html(Html.join(''));
        $('#AssmtBeltSheet').html(Html.join(''));
    },
    BeltdataDynsDirectResponse: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>BeltSheetID </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>From(>) K </th>');
        Html.push('<th>Upto(<=) K </th>');
        Html.push('<th>% Consider </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        $(data).each(function (i, element) {
            Html.push('<tbody><tr>')
            Html.push('<td style=display:none;>' + element.BeltSheetID + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            //Html.push('<td>' + element.FromRange+' ' +element.BeltUnit + '</td>');
            //Html.push('<td>' + element.ToRange + ' ' + element.BeltUnit + '</td>');
            Html.push('<td> <input type=text class=txtbeltfrom value=' + element.FromRange + ' ></td>');
            Html.push('<td> <input type=text class=txtbeltto value=' + element.ToRange + ' ></td>');
            //Html.push('<td>' + element.BeltValue + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            //Html.push('<td> <input type=text class=txtbeltval value=' + element.BeltValue + '  ></td>');

            var optHTML = [];
            //app.options.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
            $(app.optionResponse).each(function (i, elem) {
                if (elem.ValTag == element.BeltValue) {
                    optHTML.push('<option selected value=' + element.BeltValue + '>' + elem.ValDesc + '</option>');
                } else {
                    optHTML.push('<option value=' + elem.ValTag + '>' + elem.ValDesc + '</option>');
                }
            })

            Html.push('<td><select  id=per_' + element.BeltSheetID + ' class=txtbeltval>' + optHTML.join('') + '</select></td>');
            Html.push('</tr></tbody>');

            //alert(element.BeltValue == '' ? 0 : element.BeltValue);
            //  $('#per_' + element.BeltSheetID).html();

            //var ddlId = '#per_' + element.BeltSheetID;
            //ddlId = $(ddlId);
            //$.each(app.options, function (ind, val) {
            //    $(ddlId).append(val);
            //})

            //$(ddlId).html(app.options.join(''));
            //$(ddlId).val(element.BeltValue);
            //Html.clone();
        });
        //$('#AssmtBeltSheet').html(Html.join(''));
        $('#AssmtBeltSheet').html(Html.join(''));
    },
    GetAssmtScoreSheet: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Municipality/Score/GetAssmtScoreSheet',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year+ "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(JSON.stringify(response));
                //console.log(response.Data)
                //console.log(JSON.stringify(response.Data));
                //console.log(JSON.parse(response.Data));
                app.getScoreSheet(response.Data);
            },
            error: function () { }
        });
    },
    saveAssmtScoreSheet: function () {
        var arr = [];
        var data = $('#AssmtScorewSheet tr').not(":first")
        //console.log(data);
        $(data).each(function (i, item) {
            var obj = {};

            //obj.ScoreSheetID = item.find('td').eq(0).text();
            obj.ScoreSheetID = $(item).children('td').eq(0).html();
            //obj.MunicipalityName = $(item).children('td').eq(1).html();
            //obj.YearId = $(item).children('td').eq(2).html();
            //obj.ScoreBeltTypeDesc = $(item).children('td').eq(3).html();
            //obj.ScoreCode = $(item).children('td').eq(4).html();
            //obj.ScoreDesc = $(item).children('td').eq(5).html();
            //obj.ScoreValue = $(item).children('td').eq(6).html();
            
            obj.ScoreValue = $(item).find('td').find('.txtscoreval').val();



            //obj.ScoreValue = $(item).find('td:eq(6) input[type="text"]').val();
            //alert($(item).find('td').find('.txtvai').val());

            //alert(i);
            arr.push(obj);
            //arr[i] = obj;
        });
        //console.log(arr);

        $.ajax({
            url: '/Municipality/Score/SaveAssmtScoreSheet',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType:'json',
            success: function (response) {
                console.log(response);
            },
            error: function () { }
        });
    },
    saveAssmtBeltSheet: function () {
        
        var arr = [];
        var data = $('#AssmtBeltSheet tr').not(":first")
        $(data).each(function (i, element) {
            var obj = {};
            
            obj.BeltSheetID = $(element).children('td').eq(0).html();
            obj.From = $(element).find('td').find('.txtbeltfrom').val();
            obj.To = $(element).find('td').find('.txtbeltto').val();
            obj.BeltValue = $(element).find('td').find('.txtbeltval').val();
            arr.push(obj);
        });
        console.log(arr);
        $.ajax({
            url: '/Municipality/Score/SaveAssmtBeltSheet',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response)
            },
            error: function () { }
        });

    },
    getScoreSheet: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>ScoreSheetId </th>');
        //Html.push('<th>EffectiveDateForm</th>');
        //Html.push('<th>EffectiveDateTo</th>');
        Html.push('<th style=display:none;>MunicipalityName </th>');
        Html.push('<th style=display:none;>YearId </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>Score </th>');
        Html.push('<th>Description </th>');
        Html.push('<th>Value </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        $(data).each(function (i, element) {
            //CONVERTER.Date.convertCsToJsDate("/Date(1522519200000)/").format('dd/mm/yy')
            //element.EffectiveDateTo
            Html.push('<tr>')
            Html.push('<td style=display:none;>' + element.ScoreSheetID + '</td>');
            //Html.push('<td>' + CONVERTER.Date.convertCsToJsDate(element.EffectiveDateForm).format('dd/mm/yy') + '</td>');
            //Html.push('<td>' + new Date( parseInt((element.EffectiveDateTo).replace(/[^0-9 +]/g, ''))).format('dd/mm/yy') + '</td>');
            Html.push('<td style=display:none;>' + element.MunicipalityName + '</td>');
            Html.push('<td style=display:none;>' + element.YearId + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            Html.push('<td>' + element.ScoreCode + '</td>');
            Html.push('<td>' + element.ScoreDesc + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            Html.push('<td> <input type=text class=txtscoreval value=' + element.ScoreValue + '  ></td>');
            Html.push('</tr>');
        });
        $('#AssmtScorewSheet').html(Html.join(''));
    },
    ddlMunicipality: function () {
        $.ajax({
            url: '/Municipality/Score/GetAllMunicipalites',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
            },
            error: function () { }
        });
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('#ddlMunicipality');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
        //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
        var option = ctlr_ddlmunicipality.find("option[value='" + ctlr_ddlmunicipality.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected',true);
        } else {
            ctlr_ddlmunicipality.find('option:eq(0)').prop('selected',true);
        }
    },
    getYear: function () {
        $.ajax({
            url: '/Municipality/Score/GetYear',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: '{}',
            dataType: 'json',
            success: function (response) { app.populateYear(response.Data) },
            error: function () { }
        });
    },
    populateYear: function (data) {
        var ctlr_ddlYear = $('#ddlyear');
        ctlr_ddlYear.empty();
        ctlr_ddlYear.append('<option value="">--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlYear.append('<option value=' + item.YearDesc + '>' + item.YearDesc + '</option>');
        })
    },
    accordion: function () {
       
            $("#accordion").accordion({
                collapsible: true
            });
        
    }
}




