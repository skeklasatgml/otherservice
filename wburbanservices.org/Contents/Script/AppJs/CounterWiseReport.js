﻿$(document).ready(function () {
    var SERVER_OBJ = function () {
    };

    var APP_OBJ = function () {
        var BindEvent = function () {
            $('#btnSave').on('click', Insert);

            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateTo,#DateFrom').blur(function () {
                if ($(this).val().trim() == '') {
                    return false;
                }
            });
        };
        var Validate = function (data) {

            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                $('#frmCounterWiseReport').submit();
                //   print report here...
            }
        };
        var getDataFromView = function () {
            var DateFrom = $('#DateFrom').val().trim();
            var DateTo = $('#DateTo').val().trim();
            //var WardName = $('#txtWard').val().trim();
            //var WardID = $('#WardID').val();
            //var LocationName = $('#txtLocation').val().trim();
            //var LocationID = $('#LocationID').val();
            //var CounterID = $('#CounterID').val();

            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }

            var data = { DateFrom: DateFrom, DateTo: DateTo};
            return data;
        };
        this.app_Initiate = function () {
            BindEvent();
            //Bind_GridOnPageLoad();
            //serverObject.GetCountersByMunicipalityID(function (response) {
            //    ddlCounter_data = response.Data;
            //    PopulateCounters();
            //});
        };
    };
    var INIT = function () {
        var serverObject = null;//new SERVER_OBJ();
        var appObject = new APP_OBJ(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});