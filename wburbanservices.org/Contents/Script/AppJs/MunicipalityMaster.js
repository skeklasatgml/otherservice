﻿


$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.SaveMuniciPality = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/MunicipalityMaster/SaveMunicipality",
                data:JSON.stringify( { obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.Delete = function (data, callback) {

        };
        this.GetDistricts = function (callback) {
            $.ajax({
                type: "POST",
                url: "/DistrictMaster/Get_AllDistrict",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });

        };
        this.GetMunicipalities = function (callback) {
            $.ajax({
                type: "POST",
                url: "/MunicipalityMaster/GetAllMunicipalities",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var DistictDataSource = [];
        var ctrl_DrpDistrict = $("#drpDistrict");
        var ctrl_TxtMunicipalityName = $("#txtMunicipalityName");
        var cntr_TblMunicipalities = $("#tblMnicipalities");
        var cntr_txtMObileNo = $("#txtMobileNo");
        var cntr_txtEmail = $("#txtEmail");
        var cntr_txtAreaAddress = $("#txtAreaAddress");

        var BindEvent = function () {
            $("#btnInsert").on('click', OnInsertMunicipalityDetailas);
            $("#btnCancel").on('click', ResetWIndow);

        };
        var ResetWIndow = function () {
            location.reload(true);
        };
        var getDataFromView = function () {
            var MunicipalityID = $("#btnInsert").attr('MunicipalityId');
            var MunicipalityName = $("#txtMunicipalityName").val();
            var Address = $("#txtAreaAddress").val();
            var DistrictID = $("#drpDistrict").val();
            var DistrictName = $("#drpDistrict").text();
            var Email = $("#txtEmail").val();
            var Mobile = $("#txtMobileNo").val();
            var InsertedBy = null;
            var InsertedOn = null;
            var UpdatedBy = null;
            var UpdatedOn = null;
            var data = { MunicipalityID: MunicipalityID, MunicipalityName: MunicipalityName, Address: Address, DistrictID: DistrictID,DistrictName:DistrictName, Email: Email, Mobile: Mobile, InsertedBy: InsertedBy, InsertedOn: InsertedOn, UpdatedBy: UpdatedBy, UpdatedOn: UpdatedOn };
            return data;
        };
        var feedToForm = function (data) {
            var DistrictID = data.DistrictID;
            ctrl_DrpDistrict.find("option[value='" + DistrictID + "']").prop('selected', true);
            ctrl_TxtMunicipalityName.val(data.MunicipalityName);
            cntr_txtMObileNo.val(data.Mobile);
            cntr_txtEmail.val(data.Email);
            cntr_txtAreaAddress.val(data.Address);
        };
        var PopulateDistrictControlToDataSource = function () {
            ctrl_DrpDistrict.empty();
            ctrl_DrpDistrict.append("<option>--select--</option>");
            $.each(DistictDataSource, function (index,value) {
                var label = value.DistrictName;
                var id = value.DistrictID;
                ctrl_DrpDistrict.append("<option value='"+id+"'>" + label + "</option>");
            });
        };
        var PopulateTable = function () {
            var ctr_btnRowDel = $(".btnRowDel");
            var btnRowEdit =$(".btnRowEdit");
            cntr_TblMunicipalities.find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td class='MunicipalityName'>" + rowData.MunicipalityName + "</td>";
                rowstr += "<td style='display:none' class='MunicipalityID'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td class='Address'>" + rowData.Address + "</td>";
                rowstr += "<td style='display:none' class='DistrictID'>" + rowData.DistrictID + "</td>";
                rowstr += "<td class='DistrictName'>" + rowData.DistrictName + "</td>";
                rowstr += "<td class='Email'>" + rowData.Email + "</td>";
                rowstr += "<td class='Mobile'>" + rowData.Mobile + "</td>";
                rowstr += "<td><a class='btnRowEdit'>Edit</a></td>";
                rowstr += "<td><a class='btnRowDel'>Delete</a></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onDeleteRow = function () {
                var ctrl = $(this);
                //find row
                alert("Sorry!! Delete permission not given to you.");
            };
            var onEditRow = function () {
                var ctrl = $(this);
                //find row
                //construct model
                var currentrow = ctrl.parent().parent();
                var MunicipalityName = currentrow.find(".MunicipalityName").text();
                var MunicipalityId = currentrow.find(".MunicipalityID").text();
                var Address = currentrow.find(".Address").text();
                var DistrictID = currentrow.find(".DistrictID").text();
                var Email = currentrow.find(".Email").text();
                var Mobile = currentrow.find(".Mobile").text();
                $("#btnInsert").attr('MunicipalityId', MunicipalityId);
                var model = { MunicipalityName: MunicipalityName, Address: Address, DistrictID: DistrictID, Email: Email, Mobile: Mobile };
                feedToForm(model);
            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowDel', onDeleteRow);
                $(document).on('click', '.btnRowDel', onDeleteRow);

                $(document).off('click', '.btnRowEdit', onEditRow);
                $(document).on('click', '.btnRowEdit', onEditRow);
            };
            var appendToBody = function (rowString) {
                cntr_TblMunicipalities.find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.GetMunicipalities(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index,value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        };
        var Validate = function (data) {
            if (data.MunicipalityName == "") {
                alert("Enter Municipality Name");
                return false;
            }
            if(data.DistrictID=="")
            {
                alert("Select District");
                return false;
            }
            return true;
        };
        var OnInsertMunicipalityDetailas = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.SaveMuniciPality(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert('Municipality details successfully saved!!')
                        PopulateTable();
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        this.app_Initiate = function () {
            PopulateTable();
            BindEvent();
            serverObject.GetDistricts(function (response) {
                DistictDataSource = response.Data;
                PopulateDistrictControlToDataSource();
            });
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});