﻿var PAYMENT_PROCESSOR = function () {
    var thisProcessor = this;
    //setter
    this.Set_PaymentType = function (_paymentType) {
        if (_paymentType == "O" || _paymentType == "F") {
            paymentType = _paymentType;
            if (thisProcessor.wellKnownPaymentObject.hasOwnProperty("PaymentType")) {
                thisProcessor.wellKnownPaymentObject.PaymentType = _paymentType;
            }
        } else {
            throw { Exception: "InvalidArgumentException", Message: "PaymentType should be in ['O=>Online','F=>Offline']" };
        }
    };
    this.Set_PaymentMode = function (_paymentMode) {
        if (['O', 'C', 'Q', 'D', 'R', 'N', 'T'].indexOf(_paymentMode) > -1) {
            paymentMode = _paymentMode;
            if (thisProcessor.wellKnownPaymentObject.hasOwnProperty("PayMode")) {
                thisProcessor.wellKnownPaymentObject.PayMode = _paymentMode;
            }
        } else {
            throw { Exception: "InvalidArgumentException", Message: "PayMode should be in [Online = O, Cash=C, Cheque=Q, Debit Card=D, Credit Card=R, NEFT = N , RTGS=T]" };
        }
    };
    this.Set_PaymentCollectionDate = function (_CollectionDate) {
        if (_CollectionDate instanceof Date) {
            collectionDate = _CollectionDate;
            if (thisProcessor.wellKnownPaymentObject.hasOwnProperty("PaymentCollectionDate")) {
                thisProcessor.wellKnownPaymentObject.PaymentCollectionDate = _CollectionDate;
            }
        } else {
            throw "Invalid Collection Date. Tergate object is not a Date Object";
        }

    };
    this.Set_PaymentHead = function (_paymentHead) {
        paymentHead = _paymentHead;
    };

    var outStandignDataSource = [];
    this.wellKnownPaymentObject = {};
    var paymentType = "F";
    var paymentMode = "C";
    var paymentHead = {};
    var collectionDate = new Date();
    this.DataBind = function (dataSource) {
        if (dataSource.length > 0) {
            outStandignDataSource = dataSource;
            init_WellKnownObject();
        }
    };
    var init_WellKnownObject = function () {
        //convert outStandingDataSourceTo WellknownPaymentObject
        var createMstPaymentObject = function () {
            return {
                PaymentID: null,
                PaymentType: paymentType,
                AssesseeID: outStandignDataSource[0].AssesseeID,
                MunicipalityID: outStandignDataSource[0].MunicipalityID,
                BillID: outStandignDataSource[0].DemandTypeID,
                FinYear: outStandignDataSource[0].FinYear,
                PaymentCollectionDate: collectionDate,
                PaymentReceiveDate: new Date(),
                NetAmount: 0,
                RoundOffAdjust: 0,
                PayMode: paymentMode,
                InstrumentNo: null,
                InstrumentDate: null,
                PaidAmount: 0,
                CollectorID: null,
                InsertedBy: null,
                InsertedOn: null,
                UpdatedBy: null,
                UpdatedOn: null,
                PropertyTaxPaymentDetails: []
            };
        };
        var createDetailspaymentObject = function () {
            var array = [];
            $.each(outStandignDataSource, function (index, value) {
                var billDueDate = (CONVERTER.Date.isDateObject(value.BillDueDate) == false ? CONVERTER.Date.convertCsToJsDate(value.BillDueDate) : value.BillDueDate);
                array.push({
                    PaymentDetailID: null,
                    PaymentID: null,
                    PayArrrearBill: value.DemandType,
                    DemandTypeID: value.DemandTypeID,
                    QtrNo: value.QtrNo,
                    TaxValue: value.OSPropertyTaxAmt,
                    SurchargeValue: value.OSSurchargeAmt,
                    CalculatedValue: 0,
                    PaidTaxAmt: 0,
                    PaidSurchargeAmt: 0,
                    PaidCalculatedAmt: 0,
                    PayHeadTypeID: null,
                    InsertedBy: null,
                    InsertedOn: null,
                    UpdatedBy: null,
                    UpdatedOn: null,
                    FinYear: value.FinYear,
                    _Rank:index+1,
                    //extra helper properties
                    BillDueDate: billDueDate,
                    _IsPayable:false
                });
            });
            return array;
        };
        var masterObject = createMstPaymentObject();
        masterObject.PropertyTaxPaymentDetails = createDetailspaymentObject();
        thisProcessor.wellKnownPaymentObject = masterObject;
        //attach oustanding search details for future requirement
        thisProcessor.wellKnownPaymentObject.OutStandindSearchDetails = outStandignDataSource;
    };
    this.Calculate = function (amnt) {
        if (outStandignDataSource.length > 0) {
            calculateDemand(amnt);
            updateMasterObject();
            EventBus.publish(thisProcessor.eventPaths.OnCompleteCalculation, thisProcessor.wellKnownPaymentObject);
        }
    };
    var calculateDemand = function (amnt) {
        var welKnownObject = thisProcessor.wellKnownPaymentObject;
        var demandDetails = JSLINQ(welKnownObject.PropertyTaxPaymentDetails).Where(function (item) { return item.PayArrrearBill == 'B' }).OrderBy(function (item) { return item.QtrNo }).items;

        var demandDetailsForRebate = JSLINQ(demandDetails).Where(function (item) { return item.BillDueDate >= welKnownObject.PaymentCollectionDate }).items;
        var demandDetailsForInterest = JSLINQ(demandDetails).Where(function (item) { return item.BillDueDate < welKnownObject.PaymentCollectionDate }).items;

        //calculate interest first then rebate
        //calculate interest
        var GetPayHeadAmount = function (billDueDate, CollectionDate, rate, rawAmount) {
            var days = CONVERTER.DateDiff.CountDays(billDueDate, CollectionDate);

            var payHeadAmount = (rate * rawAmount * days / (100 * 30));
            return payHeadAmount.toFixed(2);
        }
        $.each(demandDetailsForInterest, function (index, value) {

            var interestPayHead = JSLINQ(paymentHead).Where(function (item) { return item.PayHeadBehave == "A" }).items;
            if (interestPayHead.length > 0) {
                //value.CalculatedValue = value.TaxValue * interestPayHead[0].PayRate / 100;
                value.CalculatedValue = Number(GetPayHeadAmount(value.BillDueDate, welKnownObject.PaymentCollectionDate, interestPayHead[0].PayRate, value.TaxValue));
                value.PayHeadTypeID = interestPayHead[0].PayHeadID;
                value.PayHeadObject = interestPayHead[0];
            } else {
                throw "Invalid Operation: PayHead Not Configured (Hint: Addition Behaviour)";
            }


            //deduct Interst first
            if (amnt > value.CalculatedValue) {
                value.PaidCalculatedAmt = value.CalculatedValue;
                amnt = amnt - value.CalculatedValue;
            } else if (amnt == value.CalculatedValue) {
                value.PaidCalculatedAmt = value.CalculatedValue;
                amnt = 0;
            } else if (amnt > 0 && amnt < value.CalculatedValue) {
                value.PaidCalculatedAmt = amnt;
                amnt = 0;
            }

            //deduct surcharg 
            if (amnt > value.SurchargeValue) {
                value.PaidSurchargeAmt = value.SurchargeValue;
                amnt = amnt - value.SurchargeValue;
            } else if (amnt == value.SurchargeValue) {
                value.PaidSurchargeAmt = value.SurchargeValue;
                amnt = 0;
            } else if (amnt > 0 && amnt < value.SurchargeValue) {
                value.PaidSurchargeAmt = amnt;
                amnt = 0;
            }

            //deduct Property tax 
            if (amnt > value.TaxValue) {
                value.PaidTaxAmt = value.TaxValue;
                amnt = amnt - value.TaxValue;
            } else if (amnt == value.TaxValue) {
                value.PaidTaxAmt = value.TaxValue;
                amnt = 0;
            } else if (amnt > 0 && amnt < value.TaxValue) {
                value.PaidTaxAmt = amnt;
                amnt = 0;
            }
        });

        //calculate rebate
        $.each(demandDetailsForRebate, function (index, value) {
            var rebatePayHead = JSLINQ(paymentHead).Where(function (item) { return item.PayHeadBehave == "D" }).items;
            if (rebatePayHead.length > 0) {
                value.CalculatedValue = Number(((-1) * value.TaxValue * rebatePayHead[0].PayRate / 100).toFixed(2));
                //var x= Number(GetPayHeadAmount(value.BillDueDate, welKnownObject.PaymentCollectionDate, rebatePayHead[0].PayRate, value.TaxValue));
                value.PayHeadTypeID = rebatePayHead[0].PayHeadID;
                value.PayHeadObject = rebatePayHead[0];
            } else {
                throw "Invalid Operation: PayHead Not Configured. (Hint: Deduction Behaviour)";
            }

            if (amnt > 0) {
                //amnt = amnt + ((-1) * value.CalculatedValue);
                //value.PaidCalculatedAmt = value.CalculatedValue;

                //deduct surcharg 
                if (amnt > value.SurchargeValue) {

                    value.PaidSurchargeAmt = value.SurchargeValue;
                    amnt = amnt - value.SurchargeValue;
                } else if (amnt == value.SurchargeValue) {
                    value.PaidSurchargeAmt = value.SurchargeValue;
                    amnt = 0;
                } else if (amnt > 0 && amnt < value.SurchargeValue) {
                    value.PaidSurchargeAmt = amnt;
                    amnt = 0;
                }

                //deduct Property tax 
                if ((amnt + ((-1) * value.CalculatedValue)) > value.TaxValue) {
                    amnt =(amnt + ((-1) * value.CalculatedValue));
                    value.PaidCalculatedAmt = value.CalculatedValue;

                    value.PaidTaxAmt = value.TaxValue;
                    amnt = amnt - value.TaxValue;
                } else if (((amnt + ((-1) * value.CalculatedValue))) == value.TaxValue) {
                    amnt = (amnt + ((-1) * value.CalculatedValue));
                    value.PaidCalculatedAmt = value.CalculatedValue;

                    value.PaidTaxAmt = value.TaxValue;
                    amnt = 0;
                } else if (amnt > 0 && amnt < value.TaxValue) {
                    value.PaidTaxAmt = amnt;
                    amnt = 0;
                }

                //value.PaidTaxAmt =Number( Number(value.PaidTaxAmt).toFixed());
                //value.PaidCalculatedAmt =Number( Number(value.PaidCalculatedAmt).toFixed());
                //value.PaidSurchargeAmt =Number( Number(value.PaidSurchargeAmt).toFixed());
            }
        });
        return amnt;
    };
    var updateMasterObject = function () {
        var netAmount = 0;
        var paidAmount = 0;
        $.each(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails, function (index, value) {
            netAmount += (value.TaxValue + value.SurchargeValue + value.CalculatedValue)
            paidAmount += (value.PaidSurchargeAmt + value.PaidTaxAmt + value.PaidCalculatedAmt)
        });
        thisProcessor.wellKnownPaymentObject.NetAmount = netAmount;
        thisProcessor.wellKnownPaymentObject.PaidAmount = paidAmount;
    };

    var updateMasterObjectByPayableCheckState = function () {
        var netAmount = 0;
        var paidAmount = 0;
        $.each(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails, function (index, value) {
            if (value._IsPayable == true) {
                netAmount += (value.TaxValue + value.SurchargeValue + value.CalculatedValue);
            }

        });
        paidAmount = getTotalPaidAmountByPayableCheckState();

        var RoundOffAdjust = 0;
        var totalNetAmount = netAmount;
        var round = Math.round(totalNetAmount, 0);
        //if (round > totalNetAmount) {
            RoundOffAdjust = round - totalNetAmount;
        //}
            thisProcessor.wellKnownPaymentObject.RoundOffAdjust =Number(RoundOffAdjust.toFixed(2));
            thisProcessor.wellKnownPaymentObject.NetAmount = round;
            thisProcessor.wellKnownPaymentObject.PaidAmount = round;//RoundOffAdjust
    };
    this.CalculateByPayableCheckState = function () {
        if (outStandignDataSource.length > 0) {
           thisProcessor. CalculateDemandByPayableState();
            updateMasterObjectByPayableCheckState();
            EventBus.publish(thisProcessor.eventPaths.OnCompleteCalculation, thisProcessor.wellKnownPaymentObject);
        }
    };

    //set true false isPayble property
    this.CheckUncheckQtrPaybleDemand = function (_qtrNo, isPayble) {
        if ([true, false].indexOf(isPayble) > -1) {
            if ([1, 2, 3, 4].indexOf(_qtrNo) > -1) {
                if (isPayble == true) {
                    for (var i = _qtrNo - 1; i > 0; i--) {
                        var y = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item.QtrNo == i }).items[0];
                        if (y._IsPayable == false) {
                            throw "Invalid Operation. Please Maintain Sequence to Check Qtr.  Hint Check [top to bottom]";
                        }
                    }
                } else {
                    for (var i = _qtrNo + 1; i <= thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails.length; i++) {
                        var y = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item.QtrNo == i }).items[0];
                        if (y._IsPayable == true) {
                            throw "Invalid Operation. Please Maintain Sequence to Check Qtr. Hint Uncheck [bottom to top]";
                        }
                    }
                }
                var x = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item.QtrNo == _qtrNo }).items[0];
                x._IsPayable = isPayble;
            } else {
                throw "Invalid value _qtrNo. should be in [1,2,3,4]";
            }
        } else {
            throw "Invalid value _IsPayable. should be in [true,false]";
        }
    };
    this.CheckUncheckByRankPaybleDemand = function (_rank, isPayble) {
        if ([true, false].indexOf(isPayble) > -1) {
            if (_rank > 0) {
                if (isPayble == true) {
                    for (var i = _rank - 1; i > 0; i--) {
                        var y = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item._Rank == i }).items[0];
                        if (y._IsPayable == false) {
                            throw "Invalid Operation. Please Maintain Sequence to Check Qtr.  Hint Check [top to bottom]";
                        }
                    }
                } else {
                    for (var i = _rank + 1; i <= thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails.length; i++) {
                        var y = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item._Rank == i }).items[0];
                        if (y._IsPayable == true) {
                            throw "Invalid Operation. Please Maintain Sequence to Check Qtr. Hint Uncheck [bottom to top]";
                        }
                    }
                }
                var x = JSLINQ(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails).Where(function (item) { return item._Rank == _rank }).items[0];
                x._IsPayable = isPayble;
            } else {
                throw "Invalid Rank. should be greater than 0";
            }
        } else {
            throw "Invalid value _IsPayable. should be in [true,false]";
        }
    };

    this.CalculateDemandByPayableState = function () {
        var welKnownObject = thisProcessor.wellKnownPaymentObject;
        var demandDetails = JSLINQ(welKnownObject.PropertyTaxPaymentDetails).Where(function (item) { return (item.PayArrrearBill == 'B' || item.PayArrrearBill == 'A') }).OrderBy(function (item) { return item.PayArrrearBill }).OrderBy(function (item) { return item.QtrNo }).items;
        //$.each(welKnownObject.PropertyTaxPaymentDetails, function (index, value) {
        //    value._Rank = index + 1;
            
        //});
        console.log(welKnownObject.PropertyTaxPaymentDetails);
        var demandDetailsForRebate = JSLINQ(demandDetails).Where(function (item) { return item.BillDueDate >= welKnownObject.PaymentCollectionDate }).items;
        var demandDetailsForInterest = JSLINQ(demandDetails).Where(function (item) { return item.BillDueDate < welKnownObject.PaymentCollectionDate }).items;

        //calculate interest first then rebate
        //calculate interest
        var GetPayHeadAmount = function (billDueDate, CollectionDate, rate, rawAmount, GracePeriodInMonth) {
            var monthsCount = CONVERTER.DateDiff.CountMonths(CollectionDate, billDueDate);
            monthsCount -= GracePeriodInMonth;
            monthsCount = (monthsCount <= 0 ? 0 : monthsCount);

            var payHeadAmount = (rate * rawAmount * monthsCount / (100 * 12));
            return payHeadAmount.toFixed(2);
        }
        $.each(demandDetailsForInterest, function (index, value) {

            var interestPayHead = JSLINQ(paymentHead).Where(function (item) { return item.PayHeadBehave == "A" }).items;
            if (interestPayHead.length > 0) {
                value.PayHeadTypeID = interestPayHead[0].PayHeadID;
                value.PayHeadObject = interestPayHead[0];
                if (value._IsPayable == true) {
                    //value.CalculatedValue = value.TaxValue * interestPayHead[0].PayRate / 100;
                    value.CalculatedValue = Number(GetPayHeadAmount(value.BillDueDate, welKnownObject.PaymentCollectionDate, interestPayHead[0].PayRate, value.TaxValue, interestPayHead[0].GracePeriodInMonth));
                    

                    value.PaidCalculatedAmt = value.CalculatedValue;
                    value.PaidSurchargeAmt = value.SurchargeValue;
                    value.PaidTaxAmt = value.TaxValue;
                }
                else {
                    value.PaidSurchargeAmt = 0;
                    value.PaidCalculatedAmt = 0;
                    value.PaidTaxAmt = 0;
                    value.CalculatedValue = 0;
                }
            } else {
                throw "Invalid Operation: PayHead Not Configured (Hint: Addition Behaviour)";
            }
                
        });

        //calculate rebate
        $.each(demandDetailsForRebate, function (index, value) {
            var rebatePayHead = JSLINQ(paymentHead).Where(function (item) { return item.PayHeadBehave == "D" }).items;
            if (rebatePayHead.length > 0) {
                value.PayHeadTypeID = rebatePayHead[0].PayHeadID;
                value.PayHeadObject = rebatePayHead[0];
                if (value._IsPayable == true) {
                    value.CalculatedValue = Number(((-1) * value.TaxValue * rebatePayHead[0].PayRate / 100).toFixed(2));
                    //var x= Number(GetPayHeadAmount(value.BillDueDate, welKnownObject.PaymentCollectionDate, rebatePayHead[0].PayRate, value.TaxValue));
                    

                    value.PaidSurchargeAmt = value.SurchargeValue;
                    value.PaidCalculatedAmt = value.CalculatedValue;
                    value.PaidTaxAmt = value.TaxValue;
                } else {
                    value.PaidSurchargeAmt = 0;
                    value.PaidCalculatedAmt = 0;
                    value.PaidTaxAmt = 0;
                    value.CalculatedValue = 0;
                }
            } else {
                throw "Invalid Operation: PayHead Not Configured. (Hint: Deduction Behaviour)";
            }
            
        });
    };
    var getTotalPaidAmountByPayableCheckState = function () {
        //var totalOutstanding = 0;
        var totalPaid = 0;
        $.each(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails, function (index, value) {
            if (value._IsPayable == true) {
                totalPaid += (value.PaidTaxAmt + value.PaidCalculatedAmt + value.PaidSurchargeAmt);
            }
        });
        return totalPaid;
    };

    var getTotalPaidAmount = function () {
        //var totalOutstanding = 0;
        var totalPaid = 0;
        $.each(thisProcessor.wellKnownPaymentObject.PropertyTaxPaymentDetails, function (index, value) {
            totalPaid += (value.PaidTaxAmt + value.PaidCalculatedAmt + value.PaidSurchargeAmt);
        });
    };
    this.eventPaths = {
        OnCompleteCalculation: { EventPath: "PAYMENT_PROCESSOR/{D8A0A29C-9A3C-4DC1-9168-DE81E1E7AFD7}", Description: "After databind, putting amount and on Completting calculation. this event fired. Return An object in understandable format following mst_payment, dat_payment" }
    };
    
};
var PAYMENT_GRID = function () {
    this. DataBind = function (data, container) {
        var gridId = "tbl-5152536D-84F2-401A-93DD-E47C6B8D0D9B";
        var ctrGrid = $("#" + gridId);

        var getRowString = function (rowData) {

            var billDate = "";

            var paymentHeadDesc = "<br/><span style='color:" + (rowData.PayHeadObject.PayHeadBehave == "D" ? "Green" : "Red") + ";'>[" + rowData.PayHeadObject.PayHeadDesc + " : " + rowData.PayHeadObject.PayRate + "%]</span>";
            var rowstr = "<tr>";
            rowstr += "<td class='FinYear'>" + rowData.FinYear + "</td>";
            rowstr += "<td class='QtrNo'>" + rowData.QtrNo + "</td>";
            rowstr += "<td class='BillDueDate'>" + rowData.BillDueDate.format('dd/mm/yyyy') + "</td>";
            rowstr += "<td class='PropertyTaxValue' >" + rowData.PropertyTaxValue + "</td>";
            rowstr += "<td class='SurchargeValue'>" + rowData.SurchargeValue + "</td>";
            rowstr += "<td class='osTaxValue' >" + rowData.osTaxValue + "</td>";
            rowstr += "<td class='osSurchargeValue' >" + rowData.osSurchargeValue + "</td>";
            rowstr += "<td class='osCalculatedValue' >" + rowData.osCalculatedValue + paymentHeadDesc + "</td>";
            rowstr += "<td class='PaidTaxAmt' >" + rowData.PaidTaxAmt + "</td>";
            rowstr += "<td class='PaidSurchargeAmt' >" + rowData.PaidSurchargeAmt + "</td>";
            rowstr += "<td class='PaidCalculatedAmt' >" + rowData.PaidCalculatedAmt + "</td>";
            var chkBox = "<input data-qtrNo='" + rowData.QtrNo + "' data-Rank='" + rowData._Rank + "' type='checkbox' class='chkIsPayable' " + (rowData._IsPayable == true ? " checked " : "") + "/> ";
            rowstr += "<td class='chkSelect' >" + chkBox + "</td>";
            rowstr += "</tr>";
            return rowstr;
        };
        var getNotFoundRowString = function () {
            return "<tr><td colspan='6' style='text-align:center'>No Data Found<td></tr>";
        };
        var appendToBody = function (rowString) {
            ctrGrid.find("tbody").append(rowString);
        };
        var poulateSummaryField = function () {
            var totalTaxValue = 0;
            var totalSurcharge = 0;
            var totalCalculated = 0;

            var totalTaxValuePaid = 0;
            var totalSurchargePaid = 0;
            var totalCalculatedPaid = 0;
            $.each(data.PropertyTaxPaymentDetails, function (index, value) {
                totalTaxValue += value.TaxValue;
                totalSurcharge += value.SurchargeValue;
                totalCalculated += value.CalculatedValue;

                totalTaxValuePaid += value.PaidTaxAmt;
                totalSurchargePaid += value.PaidSurchargeAmt;
                totalCalculatedPaid += value.PaidCalculatedAmt;
            });
            //var getCalculatedDescription = function () {
                
            //    var PayHeadIDs = JSLINQ(data.PropertyTaxPaymentDetails)
            //           .GroupBy(function (i) { return i.PayHeadObject.PayHeadID; })
            //           .Select(function (g) {
            //               return {
            //                   key: g.key,
            //                   items: jslinq(g.elements).Select(function (i) { return { PayHeadID: i.PayHeadObject.PayHeadID } }).items
            //               };
            //           })
            //    .toList();
            //    console.log(PayHeadIDs);
            //};
            //getCalculatedDescription();
            //var 
            var getPaidCalcualtedDescription = function () { };
            var foorterSummaryString = "<tr class='vb-table-footer-bgcolor'>";
            foorterSummaryString += "<th colspan='5' class='summaryFieldCommon' style='text-align:right'>Gross</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalTaxValue+"</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalSurcharge+"</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalCalculated+"</th>";

            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalTaxValuePaid+"</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalSurchargePaid+"</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'>"+totalCalculatedPaid+"</th>";
            foorterSummaryString += "<th  class='summaryFieldCommon' style='text-align:right'></th>";
            //appendToBody(foorterSummaryString);

            var footerString = "<tr class='vb-table-footer-bgcolor'>";
            footerString += "<th colspan='5' class='summaryFieldCommon' style='text-align:right'>Summary</th>";
            footerString += "<th colspan='3' class='summaryFieldOutStanding'><table style='border-collapse: collapse;width: 100%;'><tr><td>Total OutStanding: </td><td style='text-align:right'>Rs. " + (totalTaxValue+totalCalculated+totalSurcharge) + "</td></tr></table></th>";
            footerString += "<th colspan='4' class='summaryFieldPaid'><table style='border-collapse: collapse;width: 100%;'><tr><td>Total paid:</td><td style='text-align:right'>Rs. " + (totalTaxValuePaid+totalCalculatedPaid+totalSurchargePaid) + "</td></tr></table> </th>";
            footerString += "</tr>";
            appendToBody(footerString);
        };
        var dataBind = function (data) {
            var source = JSLINQ(data.PropertyTaxPaymentDetails).OrderBy(function (item) { return item._Rank }).items;
               

            $.each(source, function (index, value) {
                var extraInfo = JSLINQ(data.OutStandindSearchDetails)
               .Where(function (item) { return ((item.QtrNo == value.QtrNo) && (item.FinYear == value.FinYear)); }).Select(function (item) {
                   return {
                       QtrNo: item.QtrNo,
                       PropertyTaxValue: item.PropertyTaxValue,
                       SurchargeValue: item.SurchargeValue,
                       FinYear: item.FinYear,
                       BillDueDate: value.BillDueDate,
                       osCalculatedValue: value.CalculatedValue,
                       osTaxValue: value.TaxValue,
                       osSurchargeValue: value.SurchargeValue,
                       PaidCalculatedAmt: value.PaidCalculatedAmt,
                       PaidTaxAmt: value.PaidTaxAmt,
                       PaidSurchargeAmt: value.PaidSurchargeAmt,
                       PayHeadObject: value.PayHeadObject,
                       _IsPayable: value._IsPayable,
                       _Rank: value._Rank
                   };
               }).FirstOrDefault();
                var rowstr = getRowString(extraInfo);
                appendToBody(rowstr);
            });
            if (data.length <= 0) {
                appendToBody(getNotFoundRowString());
            } else {
                poulateSummaryField();
            }
        };
        var createGridFrame = function () {
            var grid = "<table class='table table-hover table-bordered  table-responsive' id='" + gridId + "'>" +
            "<thead>"+
            "<tr class='vb-table-header-bgcolor'>"+
            								"<th rowspan='2'>Fin Year</th>"+
											"<th rowspan='2'>Qtr No</th>"+
											"<th rowspan='2'>Due Date</th>"+
											"<th rowspan='2'>Property Tax</th>"+
											"<th rowspan='2'>Surcharge</th>"+
											"<th colspan='3'>Outstanding</th>"+
											"<th colspan='3'>Paid Amount</th>"+
											"<th rowspan='2'>Select Qtr</th>"+
										"</tr>"+
										"<tr class='vb-table-header-bgcolor'>"+
											"<th>Tax</th>"+
											"<th>Surcharge</th>"+
											"<th>Calculated Value</th>"+
											"<th>Tax</th>"+
											"<th>Surcharge</th>"+
											"<th>Calculated Value</th>"+
										"</tr>"+
									"</thead>"+
            "<tbody>" +
            "</tbody>" +
            "</table>";
            container.append(grid);
        };
        var init = function () {
            if (ctrGrid.length <= 0) {
                createGridFrame();
                ctrGrid = $("#" + gridId);
            }
            dataBind(data);
        };
        init();
    };
};