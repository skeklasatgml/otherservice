﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {

        this.PayHead = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/GetPayHeadDesc",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };

        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        this.ViewAdjustAllData = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/GetAllAdjustData",
                data: JSON.stringify({ AllAdjData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.ViewAdjustIndData = function (EffectFromDT, EffectToDT, AssesseeID, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/GetIndAdjustData",
                data: JSON.stringify({ EffectFromDT: EffectFromDT, EffectToDT: EffectToDT, AssesseeID: AssesseeID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.UpdateAdjustAllData = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/UpdateAllAdjustData",
                data: JSON.stringify({ AllAdjData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.UpdateAdjustIndData = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/UpdateIndAdjustData",
                data: JSON.stringify({ IndAdjData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.EntryAdjustAllData = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/EntryAdjustAllData",
                data: JSON.stringify({ AllAdjData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });

        };

        this.EntryAdjustIndData = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/OtherAdjustment/EntryAdjustIndData",
                data: JSON.stringify({ AllAdjData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });

        };
    }

    var APP_OBJECT = function (serverObject) {
        var modelData = [];
        var modelDataIndividual = [];
        var MstOthAdjID = "";
        var DtlID = "";
        var AssesseeID = "";
        var MunicipalityID = "";

        var BindEvent = function () {
            $('#txtWard').autocomplete(AutocompleteSource.Wards());
            $('#txtLocation').autocomplete(AutocompleteSource.Locations());
            $('#txtAssessee').autocomplete(AutocompleteSource.Assessees());

            $('#txtWardEntry').autocomplete(AutocompleteSource2.Wards());
            $('#txtLocationEntry').autocomplete(AutocompleteSource2.Locations());
            $('#txtAssesseeEntry').autocomplete(AutocompleteSource2.Assessees());


            $("#btnView").on('click', OnViewOtherAdjustAll);
            $("#btnViewInd").on('click', OnViewOtherAdjustInd);
            $("#btnEntryAll").on('click', OnEntryOtherAdjustAll);
            $("#btnEntryInd").on('click', OnEntryOtherAdjustInd);

            PopulateFinYear();

            $('#txtRateValue, #txtRateValueEntry, #txtRateValueInd, #txtRateValueEntryInd').keypress(function (event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $("#txtGracePeriod, #txtGracePeriodEntry, #txtGracePeriodInd, #txtGracePeriodEntryInd").keypress(function (event) {
                if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
                    event.preventDefault();
                }
            });

            $('#ddlApplicableTypeRangeEntry').change(function () {

                if ($('#ddlApplicableTypeRangeEntry').val() == 'R') {
                    $("#ddlFromFinYearEntry").prop("disabled", false);
                    $("#ddlFromQtrEntry").prop("disabled", false);

                    $("#ddlToFinYearEntry").prop("disabled", false);
                    $("#ddlToQtrEntry").prop("disabled", false);
                }
                else
                {
                    $("#ddlFromFinYearEntry").prop("disabled", true);
                    $("#ddlFromQtrEntry").prop("disabled", true);

                    $("#ddlToFinYearEntry").prop("disabled", true);
                    $("#ddlToQtrEntry").prop("disabled", true);
                }

            });
            
            $('#ddlApplicableTypeRange').change(function () {

                if ($('#ddlApplicableTypeRange').val() == 'R') {
                    $("#ddlFromFinYear").prop("disabled", false);
                    $("#ddlFromQtr").prop("disabled", false);

                    $("#ddlToFinYear").prop("disabled", false);
                    $("#ddlToQtr").prop("disabled", false);
                }
                else {
                    $("#ddlFromFinYear").prop("disabled", true);
                    $("#ddlFromQtr").prop("disabled", true);

                    $("#ddlToFinYear").prop("disabled", true);
                    $("#ddlToQtr").prop("disabled", true);
                }

            });

            $('#ddlApplicableTypeRangeEntryInd').change(function () {

                if ($('#ddlApplicableTypeRangeEntryInd').val() == 'R') {
                    $("#ddlFromFinYearEntryInd").prop("disabled", false);
                    $("#ddlFromQtrEntryInd").prop("disabled", false);

                    $("#ddlToFinYearEntryInd").prop("disabled", false);
                    $("#ddlToQtrEntryInd").prop("disabled", false);
                }
                else {
                    $("#ddlFromFinYearEntryInd").prop("disabled", true);
                    $("#ddlFromQtrEntryInd").prop("disabled", true);

                    $("#ddlToFinYearEntryInd").prop("disabled", true);
                    $("#ddlToQtrEntryInd").prop("disabled", true);
                }

            });

            $('#ddlApplicableTypeRangeInd').change(function () {

                if ($('#ddlApplicableTypeRangeInd').val() == 'R') {
                    $("#ddlFromFinYearInd").prop("disabled", false);
                    $("#ddlFromQtrInd").prop("disabled", false);

                    $("#ddlToFinYearInd").prop("disabled", false);
                    $("#ddlToQtrInd").prop("disabled", false);
                }
                else {
                    $("#ddlFromFinYearInd").prop("disabled", true);
                    $("#ddlFromQtrInd").prop("disabled", true);

                    $("#ddlToFinYearInd").prop("disabled", true);
                    $("#ddlToQtrInd").prop("disabled", true);
                }

            });


            var dateFormat = "dd/mm/yy",
                from = $("#dtFrom, #txtFromDateEdit, #dtFromInd, #txtFromDateEntry, #txtFromDateEntryInd, #txtFromDateEditInd")
                    .datepicker({
                        //defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat: "dd/mm/yy"
                    })
                    .on("change", function () {
                        to.datepicker("option", "minDate", getDate(this));
                    }),
                to = $("#dtTo, #txtToDateEdit, #dtToInd, #txtToDateEntry, #txtToDateEntryInd, #txtToDateEditInd").datepicker({
                    //defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: "dd/mm/yy"
                })
                    .on("change", function () {
                        from.datepicker("option", "maxDate", getDate(this));
                    });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        };

        var PopulateFinYear = function () {
            var currDT = new Date();
            var currYear = '';
            var currMonth = currDT.getMonth();
            if (currMonth >= 0 && currMonth < 3) {
                currYear = currDT.getFullYear() - 1;
            }
            else {
                currYear = currDT.getFullYear();
            }
            var firstYear = 1950;
            var finyear = '';
            $('<option value=""> -- Select -- </option>').appendTo('#ddlFromFinYearEntry, #ddlToFinYearEntry, #ddlFromFinYear, #ddlToFinYear, #ddlFromFinYearEntryInd, #ddlToFinYearEntryInd, #ddlFromFinYearInd, #ddlToFinYearInd');
            for (var i = currYear; i >= firstYear; i--) {
                j = i + 1;
                finyear = i + '-' + j;                
                $('<option value="' + finyear + '">' + finyear + '</option>').appendTo('#ddlFromFinYearEntry, #ddlToFinYearEntry, #ddlFromFinYear, #ddlToFinYear, #ddlFromFinYearEntryInd, #ddlToFinYearEntryInd, #ddlFromFinYearInd, #ddlToFinYearInd');
                //$('<option value="' + finyear + '">' + finyear + '</option>').appendTo('#ddlFinyearInterest');
            }
        }

        var PopulatePayHeadData = function (data) {
            $("#ddlPayDescEntry, #ddlPayDescEntryInd").empty();
            $("#ddlPayDescEntry, #ddlPayDescEntryInd").append('<option value="">-- Select --</option>');
            $.each(data, function (index, value) {
                var label = value.PayHeadDesc;
                var id = value.PayHeadID;
                $("#ddlPayDescEntry, #ddlPayDescEntryInd").append("<option value='" + id + "'>" + label + "</option>");
            });
        };

        var RegistertabAllAdjEvent = function () {
            $("#tabAllAdj tbody").off('click', 'span[id="spanEdit"]', onEditRow);
            $("#tabAllAdj tbody").on('click', 'span[id="spanEdit"]', onEditRow);
        };

        var RegistertabIndAdjEvent = function () {
            $("#tabIndAdj tbody").off('click', 'span[id="spanEditInd"]', onEditRowInd);
            $("#tabIndAdj tbody").on('click', 'span[id="spanEditInd"]', onEditRowInd);
        };

        var OnEntryOtherAdjustAll = function () {
            if (From_ValidateForAdjustAllEntry()) {
                var AdjAllData = {
                    PayHeadID: $('#ddlPayDescEntry').val(),
                    ApplicableTaxRangeType: $('#ddlApplicableTypeRangeEntry').val(),
                    FromFinYear: ($('#ddlApplicableTypeRangeEntry').val() == 'R' ? $('#ddlFromFinYearEntry').val() : " "),
                    FromFinYearQtr: ($('#ddlApplicableTypeRangeEntry').val() == 'R' ? $('#ddlFromQtrEntry').val() : " "),
                    ToFinYear: ($('#ddlApplicableTypeRangeEntry').val() == 'R' ? $('#ddlToFinYearEntry').val() : " "),
                    ToFinYearQtr: ($('#ddlApplicableTypeRangeEntry').val() == 'R' ? $('#ddlToQtrEntry').val() : " "),
                    RateValueType: $('#ddlRateTypeEntry').val(),
                    RateValueWithoutPercent: $('#txtRateValueEntry').val(),
                    IsPropertyTax: $('#ckhPtaxEntry').is(":checked") ? 1 : 0,
                    IsSurcharge: $('#ckhSurchargeEntry').is(":checked") ? 1 : 0,
                    IsPropertyTaxInt: $('#ckhPtaxIntEntry').is(":checked") ? 1 : 0,
                    IsSurchargeInt: $('#ckhSurchargeIntEntry').is(":checked") ? 1 : 0,
                    GracePeriodInMonth: $('#txtGracePeriodEntry').val(),
                    EffectFromDT: $('#txtFromDateEntry').val(),
                    EffectToDT: $('#txtToDateEntry').val(),
                    Remarks: $('#txtRemarksEntry').val()
                };               
                serverObject.EntryAdjustAllData(AdjAllData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(response.Message);
                        $('#frmAdjAllEntry').closest('form').find("input[type=text], select").val("");
                        $('#myModalEntry').modal('hide');
                       // OnViewOtherAdjustAll();
                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var OnEntryOtherAdjustInd = function () {
            if (From_ValidateForAdjustIndividualEntry()) {
            var AdjIndData = {
                AssesseeID: $('#hdnAssesseeEntry').val(),
                PayHeadID: $('#ddlPayDescEntryInd').val(),
                ApplicableTaxRangeType: $('#ddlApplicableTypeRangeEntryInd').val(),
                FromFinYear: ($('#ddlApplicableTypeRangeEntryInd').val() == 'R' ? $('#ddlFromFinYearEntryInd').val() : " "),
                FromFinYearQtr: ($('#ddlApplicableTypeRangeEntryInd').val() == 'R' ? $('#ddlFromQtrEntryInd').val() : " "),
                ToFinYear: ($('#ddlApplicableTypeRangeEntryInd').val() == 'R' ? $('#ddlToFinYearEntryInd').val() : " "),
                ToFinYearQtr: ($('#ddlApplicableTypeRangeEntryInd').val() == 'R' ? $('#ddlToQtrEntryInd').val() : " "),
                RateValueType: $('#ddlRateTypeEntryInd').val(),
                RateValueWithoutPercent: $('#txtRateValueEntryInd').val(),
                IsPropertyTax: $('#ckhPtaxEntryInd').is(":checked") ? 1 : 0,
                IsSurcharge: $('#ckhSurchargeEntryInd').is(":checked") ? 1 : 0,
                IsPropertyTaxInt: $('#ckhPtaxIntEntryInd').is(":checked") ? 1 : 0,
                IsSurchargeInt: $('#ckhSurchargeIntEntryInd').is(":checked") ? 1 : 0,
                GracePeriodInMonth: $('#txtGracePeriodEntryInd').val(),
                EffectFromDT: $('#txtFromDateEntryInd').val(),
                EffectToDT: $('#txtToDateEntryInd').val(),
                Remarks: $('#txtRemarksEntryInd').val()
                };
                serverObject.EntryAdjustIndData(AdjIndData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(response.Message);
                        $('#frmAdjIndiEntry').closest('form').find("input[type=text], select").val("");
                        $('#myModalEntryInd').modal('hide');
                       // OnViewOtherAdjustInd();
                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var onEditRowInd = function () {

           // $("#txtFinYearInd").prop("disabled", true);
            $("#txtPayDescInd").prop("disabled", true);
            $("#txtStatusInd").prop("disabled", true);
            $("#txtApprovedOnInd").prop("disabled", true);
            $("#txtAdjustedInd").prop("disabled", true);
            $("#txtAdjustedOnInd").prop("disabled", true);
            

            var CurrentTR = $(this).closest('tr');
            var RowUnqIDInd = $(CurrentTR).find("td:eq(4)").find('span[id$="spanEditInd"]').attr('data-ID');
            var arrRowDataInd = alasql('SELECT * FROM ?  where UnqID="' + RowUnqIDInd + '"', [modelDataIndividual]);
            if (arrRowDataInd.length = 1) {

                var objRowDataInd = arrRowDataInd[0];
                console.log(objRowDataInd);
                AssesseeID = objRowDataInd.AssesseeID;
                DtlID = objRowDataInd.DtlID;
                MstOthAdjID = objRowDataInd.MstOthrAdjID;
                MunicipalityID = objRowDataInd.MunicipalityID;
                $('#txtPayDescInd').val(objRowDataInd.PayHeadDesc);
                $('#ddlApplicableTypeRangeInd').val(objRowDataInd.ApplicableTaxRangeType);
                $('#ddlFromFinYearInd').val(objRowDataInd.FromFinYear);
                $('#ddlFromQtrInd').val(objRowDataInd.FromFinYearQtr == 0 ? "" : (objRowDataInd.FromFinYearQtr));
                $('#ddlToFinYearInd').val(objRowDataInd.ToFinYear);
                $('#ddlToQtrInd').val(objRowDataInd.ToFinYearQtr == 0 ? "" : (objRowDataInd.ToFinYearQtr));
                if (objRowDataInd.IsPropertyTax == 1) {
                    $('#ckhPtaxInd').prop('checked', true);
                }
                else {
                    $('#ckhPtaxInd').prop('checked', false);
                }
                if (objRowDataInd.IsSurcharge == 1) {
                    $('#ckhSurchargeInd').prop('checked', true);
                }
                else {
                    $('#ckhSurchargeInd').prop('checked', false);
                }
                if (objRowDataInd.IsPropertyTaxInt == 1) {
                    $('#ckhPtaxIntInd').prop('checked', true);
                }
                else {
                    $('#ckhPtaxIntInd').prop('checked', false);
                }
                if (objRowDataInd.IsSurchargeInt == 1) {
                    $('#ckhSurchargeIntInd').prop('checked', true);
                }
                else {
                    $('#ckhSurchargeIntInd').prop('checked', false);
                }

                $('#ddlRateTypeInd').val(objRowDataInd.RateValueType);
                $('#txtRateValueInd').val(objRowDataInd.RateValueWithoutPercent);
                $('#txtGracePeriodInd').val(objRowDataInd.GracePeriodInMonth);
                $('#txtFromDateEditInd').val(objRowDataInd.EffectFromDT);
                $('#txtToDateEditInd').val(objRowDataInd.EffectToDT);
                $('#txtStatusInd').val(objRowDataInd.IsApproved);
                $('#txtApprovedOnInd').val(objRowDataInd.ApprovedOn);
                $('#txtAdjustedInd').val(objRowDataInd.IsPayAdjusted);
                $('#txtAdjustedOnInd').val(objRowDataInd.PayAdjustedOn);

                $('#txtRemarksInd').val(objRowDataInd.Remarks);

                if (objRowDataInd.IsApprovedFlag == true) {
                    $("#ddlApplicableTypeRangeInd").prop("disabled", true);
                    $("#ddlFromFinYearInd").prop("disabled", true);
                    $("#ddlFromQtrInd").prop("disabled", true);
                    $("#ddlToFinYearInd").prop("disabled", true);
                    $("#ddlToQtrInd").prop("disabled", true);

                    $("#ckhPtaxInd").prop("disabled", true);
                    $("#ckhSurchargeInd").prop("disabled", true);
                    $("#ckhPtaxIntInd").prop("disabled", true);
                    $("#ckhSurchargeIntInd").prop("disabled", true);

                    $("#ddlRateTypeInd").prop("disabled", true);
                    $("#txtRateValueInd").prop("disabled", true);
                    $("#txtGracePeriodInd").prop("disabled", true);
                    $("#txtFromDateEditInd").prop("disabled", true);
                    $("#txtToDateEditInd").prop("disabled", true);
                    $("#btnEditInd").prop("disabled", true);

                    $("#txtRemarksInd").prop("disabled", true);
                }
                else {
                    $("#ddlApplicableTypeRangeInd").prop("disabled", false);

                    if ($('#ddlApplicableTypeRangeInd').val() == 'R') {
                        $("#ddlFromFinYearInd").prop("disabled", false);
                        $("#ddlFromQtrInd").prop("disabled", false);

                        $("#ddlToFinYearInd").prop("disabled", false);
                        $("#ddlToQtrInd").prop("disabled", false);
                    }
                    else {
                        $("#ddlFromFinYearInd").prop("disabled", true);
                        $("#ddlFromQtrInd").prop("disabled", true);

                        $("#ddlToFinYearInd").prop("disabled", true);
                        $("#ddlToQtrInd").prop("disabled", true);
                    }                    

                    $("#ckhPtaxInd").prop("disabled", false);
                    $("#ckhSurchargeInd").prop("disabled", false);
                    $("#ckhPtaxIntInd").prop("disabled", false);
                    $("#ckhSurchargeIntInd").prop("disabled", false);

                    $("#ddlRateTypeInd").prop("disabled", false);
                    $("#txtRateValueInd").prop("disabled", false);
                    $("#txtGracePeriodInd").prop("disabled", false);
                    $("#txtFromDateEditInd").prop("disabled", false);
                    $("#txtToDateEditInd").prop("disabled", false);
                    $("#btnEditInd").prop("disabled", false);

                    $("#txtRemarksInd").prop("disabled", false);

                    $("#btnEditInd").off('click', OnUpdateOtherAdjustInd);
                    $("#btnEditInd").on('click', OnUpdateOtherAdjustInd);
                }
            }
            else {
                alert("No record found!!!");
            }
        }

        var onEditRow = function () {
            //$("#txtFinYear").prop("disabled", true);
            $("#txtPayDesc").prop("disabled", true);
            $("#txtStatus").prop("disabled", true);
            $("#txtApprovedOn").prop("disabled", true);
            

            var CurrentTR = $(this).closest('tr');
            var RowUnqID = $(CurrentTR).find("td:eq(4)").find('span[id$="spanEdit"]').attr('data-ID');
            var arrRowData = alasql('SELECT * FROM ?  where UnqID="' + RowUnqID + '"', [modelData]);
            if (arrRowData.length = 1) {

                var objRowData = arrRowData[0];
                //console.log(objRowData);
                MstOthAdjID = objRowData.MstOthrAdjID;
                MunicipalityID = objRowData.MunicipalityID;
                $('#txtPayDesc').val(objRowData.PayHeadDesc);
                $('#ddlApplicableTypeRange').val(objRowData.ApplicableTaxRangeType);
                $('#ddlFromFinYear').val(objRowData.FromFinYear);
                $('#ddlFromQtr').val(objRowData.FromFinYearQtr == 0 ? "" : (objRowData.FromFinYearQtr)) ;
                $('#ddlToFinYear').val(objRowData.ToFinYear);
                $('#ddlToQtr').val(objRowData.ToFinYearQtr == 0 ? "" : (objRowData.ToFinYearQtr));

                if (objRowData.IsPropertyTax == 1) {
                    $('#ckhPtax').prop('checked', true);
                }
                else {
                    $('#ckhPtax').prop('checked', false);
                }
                if (objRowData.IsSurcharge == 1) {
                    $('#ckhSurcharge').prop('checked', true);
                }
                else {
                    $('#ckhSurcharge').prop('checked', false);
                }
                if (objRowData.IsPropertyTaxInt == 1) {
                    $('#ckhPtaxInt').prop('checked', true);
                }
                else {
                    $('#ckhPtaxInt').prop('checked', false);
                }
                if (objRowData.IsSurchargeInt == 1) {
                    $('#ckhSurchargeInt').prop('checked', true);
                }
                else {
                    $('#ckhSurchargeInt').prop('checked', false);
                }
                
                $('#ddlRateType').val(objRowData.RateValueType);
                $('#txtRateValue').val(objRowData.RateValueWithoutPercent);
                $('#txtGracePeriod').val(objRowData.GracePeriodInMonth);
                $('#txtFromDateEdit').val(objRowData.EffectFromDT);
                $('#txtToDateEdit').val(objRowData.EffectToDT);
                $('#txtStatus').val(objRowData.IsApproved);
                $('#txtApprovedOn').val(objRowData.ApprovedOn);

                $('#txtRemarks').val(objRowData.Remarks);

                if (objRowData.IsApprovedFlag == true) {
                    $("#ddlApplicableTypeRange").prop("disabled", true);
                    $("#ddlFromFinYear").prop("disabled", true);
                    $("#ddlFromQtr").prop("disabled", true);
                    $("#ddlToFinYear").prop("disabled", true);
                    $("#ddlToQtr").prop("disabled", true);

                    $("#ckhPtax").prop("disabled", true);
                    $("#ckhSurcharge").prop("disabled", true);
                    $("#ckhPtaxInt").prop("disabled", true);
                    $("#ckhSurchargeInt").prop("disabled", true);

                    $("#ddlRateType").prop("disabled", true);
                    $("#txtRateValue").prop("disabled", true);
                    $("#txtGracePeriod").prop("disabled", true);
                    $("#txtFromDateEdit").prop("disabled", true);
                    $("#txtToDateEdit").prop("disabled", true);
                    $("#btnEditAll").prop("disabled", true);

                    $("#txtRemarks").prop("disabled", true);
                }
                else {
                    $("#ddlApplicableTypeRange").prop("disabled", false);

                    if ($('#ddlApplicableTypeRange').val() == 'R') {
                        $("#ddlFromFinYear").prop("disabled", false);
                        $("#ddlFromQtr").prop("disabled", false);

                        $("#ddlToFinYear").prop("disabled", false);
                        $("#ddlToQtr").prop("disabled", false);
                    }
                    else {
                        $("#ddlFromFinYear").prop("disabled", true);
                        $("#ddlFromQtr").prop("disabled", true);

                        $("#ddlToFinYear").prop("disabled", true);
                        $("#ddlToQtr").prop("disabled", true);
                    }

                    $("#ckhPtax").prop("disabled", false);
                    $("#ckhSurcharge").prop("disabled", false);
                    $("#ckhPtaxInt").prop("disabled", false);
                    $("#ckhSurchargeInt").prop("disabled", false);

                    $("#ddlRateType").prop("disabled", false);
                    $("#txtRateValue").prop("disabled", false);
                    $("#txtGracePeriod").prop("disabled", false);
                    $("#txtFromDateEdit").prop("disabled", false);
                    $("#txtToDateEdit").prop("disabled", false);
                    $("#btnEditAll").prop("disabled", false);

                    $("#txtRemarks").prop("disabled", false);

                    $("#btnEditAll").off('click', OnUpdateOtherAdjustAll);
                    $("#btnEditAll").on('click', OnUpdateOtherAdjustAll);
                }
            }
            else {
                alert("No record found!!!");
            }

        }

        var OnUpdateOtherAdjustAll = function () {
            if (From_ValidateForAdjustAllUpdate()) {
                var AdjAllData = {
                    EffectFromDT: $('#txtFromDateEdit').val(), EffectToDT: $('#txtToDateEdit').val(), RateValueType: $('#ddlRateType').val(),
                    RateValueWithoutPercent: $('#txtRateValue').val(), GracePeriodInMonth: $('#txtGracePeriod').val(),
                    ApplicableTaxRangeType: $('#ddlApplicableTypeRange').val(),
                    FromFinYear: ($('#ddlApplicableTypeRange').val() == 'R' ? $('#ddlFromFinYear').val() : " "),
                    FromFinYearQtr: ($('#ddlApplicableTypeRange').val() == 'R' ? $('#ddlFromQtr').val() : " "),
                    ToFinYear: ($('#ddlApplicableTypeRange').val() == 'R' ? $('#ddlToFinYear').val() : " "),
                    ToFinYearQtr: ($('#ddlApplicableTypeRange').val() == 'R' ? $('#ddlToQtr').val() : " "),

                    IsPropertyTax: $('#ckhPtax').is(":checked") ? 1 : 0,
                    IsSurcharge: $('#ckhSurcharge').is(":checked") ? 1 : 0,
                    IsPropertyTaxInt: $('#ckhPtaxInt').is(":checked") ? 1 : 0,
                    IsSurchargeInt: $('#ckhSurchargeInt').is(":checked") ? 1 : 0,
                    Remarks: $('#txtRemarks').val(),
                    MstOthrAdjID: MstOthAdjID, MunicipalityID: MunicipalityID
                };
                serverObject.UpdateAdjustAllData(AdjAllData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(response.Message);
                        $('#frmAdjAllEDit').closest('form').find("input[type=text], select").val("");
                        $('#myModal').modal('hide');
                        OnViewOtherAdjustAll();

                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var OnUpdateOtherAdjustInd = function () {           
            if (From_ValidateForAdjustIndUpdate()) {
                var AdjIndData = {
                    EffectFromDT: $('#txtFromDateEditInd').val(), EffectToDT: $('#txtToDateEditInd').val(), RateValueType: $('#ddlRateTypeInd').val(),
                    RateValueWithoutPercent: $('#txtRateValueInd').val(), GracePeriodInMonth: $('#txtGracePeriodInd').val(),
                    ApplicableTaxRangeType: $('#ddlApplicableTypeRangeInd').val(),
                    FromFinYear: ($('#ddlApplicableTypeRangeInd').val() == 'R' ? $('#ddlFromFinYearInd').val() : " "),
                    FromFinYearQtr: ($('#ddlApplicableTypeRangeInd').val() == 'R' ? $('#ddlFromQtrInd').val() : " "),
                    ToFinYear: ($('#ddlApplicableTypeRangeInd').val() == 'R' ? $('#ddlToFinYearInd').val() : " "),
                    ToFinYearQtr: ($('#ddlApplicableTypeRangeInd').val() == 'R' ? $('#ddlToQtrInd').val() : " "),

                    IsPropertyTax: $('#ckhPtaxInd').is(":checked") ? 1 : 0,
                    IsSurcharge: $('#ckhSurchargeInd').is(":checked") ? 1 : 0,
                    IsPropertyTaxInt: $('#ckhPtaxIntInd').is(":checked") ? 1 : 0,
                    IsSurchargeInt: $('#ckhSurchargeIntInd').is(":checked") ? 1 : 0,
                    Remarks: $('#txtRemarksInd').val(),
                    MstOthrAdjID: MstOthAdjID, MunicipalityID: MunicipalityID, DtlID: DtlID, AssesseeID: AssesseeID
                };
                serverObject.UpdateAdjustIndData(AdjIndData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(response.Message);
                        $('#frmAdjIndEDit').closest('form').find("input[type=text], select").val("");
                        $('#myModalIndividual').modal('hide');
                        OnViewOtherAdjustInd();

                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var From_ValidateForAdjustAllEntry = function () {
            $("#frmAdjAllEntry").validate({
                rules: {
                    ddlPayDescEntry: {
                        required: true
                    },
                    ddlApplicableTypeRangeEntry: {
                        required: true
                    },
                    ddlFromFinYearEntry: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntry").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlFromQtrEntry: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntry").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToFinYearEntry: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntry").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToQtrEntry: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntry").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlRateTypeEntry: {
                        required: true
                    },
                    txtRateValueEntry: {
                        required: true,
                        max: function (element) {
                            if ($("#ddlRateTypeEntry").val() == 'P') {
                                return 100;
                            }
                        }
                    },
                    'test[]': {
                        required: true,
                        //maxlength: 1
                    },
                    txtGracePeriodEntry: {
                        required: true
                    },
                    txtFromDateEntry: {
                        required: true
                    },
                    txtToDateEntry: {
                        required: true
                    },
                    txtRemarksEntry: {
                        required: true
                    }
                },
                messages: {
                    ddlPayDescEntry: {
                        required: "Please Select Pay Head."
                    },
                    ddlApplicableTypeRangeEntry: {
                        required: "Please Select."
                    },
                    ddlFromFinYearEntry: {
                        required: "Please Select From FinYear."
                    },
                    ddlFromQtrEntry: {
                        required: "Please Select From Quarter."
                    },
                    ddlToFinYearEntry: {
                        required: "Please Select To FinYear."
                    },
                    ddlToQtrEntry: {
                        required: "Please Select To Quarter."
                    },
                    ddlRateTypeEntry: {
                        required: "Please Select."
                    },
                    txtRateValueEntry: {
                        required: "Please Enter Rate Value.",
                        max: "Percent value must be between 0 to 100"
                    },
                    'test[]': {
                        required: "You must check at least 1 Applicable Type",
                        //maxlength: "Check no more than {0} boxes"
                    },
                    txtGracePeriodEntry: {
                        required: "Please Enter Grace Period."
                    },
                    txtFromDateEntry: {
                        required: "Please Enter From Date."
                    },
                    txtToDateEntry: {
                        required: "Please Enter To Date."
                    },
                    txtRemarksEntry: {
                        required: "Please Enter Proper Remarks for Adjustment."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") === "checkbox") {
                        error.insertAfter($(element).closest('div'));
                    } 
                    else {
                        error.insertAfter($(element)); // standard behaviour
                    }
                }
            });
            return $('#frmAdjAllEntry').valid();
        }

        var From_ValidateForAdjustAllUpdate = function () {
            $("#frmAdjAllEDit").validate({
                rules: {
                    ddlApplicableTypeRange: {
                        required: true
                    },
                    ddlFromFinYear: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRange").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlFromQtr: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRange").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToFinYear: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRange").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToQtr: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRange").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlRateType: {
                        required: true
                    },
                    txtRateValue: {
                        required: true,
                        max: function (element) {
                            if ($("#ddlRateType").val() == 'P') {
                                return 100;
                            }
                        }
                    },
                    'chkAll[]': {
                        required: true,                        
                    },
                    txtGracePeriod: {
                        required: true
                    },
                    txtFromDateEdit: {
                        required: true
                    },
                    txtToDateEdit: {
                        required: true
                    },
                    txtRemarks: {
                        required: true
                    }
                },
                messages: {
                    ddlApplicableTypeRange: {
                        required: "Please Select."
                    },
                    ddlFromFinYear: {
                        required: "Please Select From FinYear."
                    },
                    ddlFromQtr: {
                        required: "Please Select From Quarter."
                    },
                    ddlToFinYear: {
                        required: "Please Select To FinYear."
                    },
                    ddlToQtr: {
                        required: "Please Select To Quarter."
                    },
                    ddlRateType: {
                        required: "Please Select Rate Type."
                    },
                    txtRateValue: {
                        required: "Please Enter RateValue.",
                        max: "Percent value must be between 0 to 100"
                    },
                    'chkAll[]': {
                        required: "You must check at least 1 Applicable Type",
                        //maxlength: "Check no more than {0} boxes"
                    },
                    txtGracePeriod: {
                        required: "Please Enter Grace Period"
                    },
                    txtFromDateEdit: {
                        required: "Please Select Date From"
                    },
                    txtToDateEdit: {
                        required: "Please Select Date To"
                    },
                    txtRemarks: {
                        required: "Please Enter Proper Remarks for Adjustment."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") === "checkbox") {
                        error.insertAfter($(element).closest('div'));
                    }
                    else {
                        error.insertAfter($(element)); // standard behaviour
                    }
                }
            });
            return $('#frmAdjAllEDit').valid();
        }

        var From_ValidateForAdjustIndUpdate = function () {
            $("#frmAdjIndEDit").validate({
                rules: {
                    ddlApplicableTypeRangeInd: {
                        required: true
                    },
                    ddlFromFinYearInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlFromQtrInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToFinYearInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToQtrInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlRateTypeInd: {
                        required: true
                    },
                    txtRateValueInd: {
                        required: true,
                        max: function (element) {
                            if ($("#ddlRateTypeInd").val() == 'P') {
                                return 100;
                            }
                        }
                    },
                    'chkInd[]': {
                        required: true,
                    },
                    txtGracePeriodInd: {
                        required: true
                    },
                    txtFromDateEditInd: {
                        required: true
                    },
                    txtToDateEditInd: {
                        required: true
                    },
                    txtRemarksInd: {
                        required: true
                    }
                },
                messages: {
                    ddlApplicableTypeRangeInd: {
                        required: "Please Select."
                    },
                    ddlFromFinYearInd: {
                        required: "Please Select From FinYear."
                    },
                    ddlFromQtrInd: {
                        required: "Please Select From Quarter."
                    },
                    ddlToFinYearInd: {
                        required: "Please Select To FinYear."
                    },
                    ddlToQtrInd: {
                        required: "Please Select To Quarter."
                    },
                    ddlRateTypeInd: {
                        required: "Please Select Rate Type."
                    },
                    txtRateValueInd: {
                        required: "Please Enter RateValue.",
                        max: "Percent value must be between 0 to 100"
                    },
                    'chkInd[]': {
                        required: "You must check at least 1 Applicable Type",
                        //maxlength: "Check no more than {0} boxes"
                    },
                    txtGracePeriodInd: {
                        required: "Please Enter Grace Period"
                    },
                    txtFromDateEditInd: {
                        required: "Please Select Date From"
                    },
                    txtToDateEditInd: {
                        required: "Please Select Date To"
                    },
                    txtRemarksInd: {
                        required: "Please Enter Proper Remarks for Adjustment."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") === "checkbox") {
                        error.insertAfter($(element).closest('div'));
                    }
                    else {
                        error.insertAfter($(element)); // standard behaviour
                    }
                }
            });
            return $('#frmAdjIndEDit').valid();
        }

        var From_ValidateForAdjustAll = function () {
            $("#frmAdjAll").validate({
                rules: {
                    dtFrom: {
                        required: true
                    },
                    dtTo: {
                        required: true
                    }
                },
                messages: {
                    dtFrom: {
                        required: "Please select Date From."
                    },
                    dtTo: {
                        required: "Please select Date To."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                }
            });
            return $('#frmAdjAll').valid();
        }

        var From_ValidateForAdjustIndividual = function () {
            var objIndividual = {
                rules: {
                    dtFromInd: {
                        required: true
                    },
                    dtToInd: {
                        required: true
                    },
                    hdnWard: {
                        required: true
                    },
                    hdnLocation: {
                        required: true
                    },
                    hdnAssessee: {
                        required: true
                    }
                },
                messages: {
                    dtFromInd: {
                        required: "Please select Date From."
                    },
                    dtToInd: {
                        required: "Please select Date To."
                    },
                    hdnWard: {
                        required: "Please select Ward."
                    },
                    hdnLocation: {
                        required: "Please select Location."
                    },
                    hdnAssessee: {
                        required: "Please select Assessee."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                }
            };
            objIndividual.ignore = [];
            $("#frmAdjInd").validate(objIndividual);
            return $('#frmAdjInd').valid();
        }

        var From_ValidateForAdjustIndividualEntry = function () {
            var objIndividualEntry = {
                rules: {
                    txtFromDateEntryInd: {
                        required: true
                    },
                    txtToDateEntryInd: {
                        required: true
                    },
                    hdnWardEntry: {
                        required: true
                    },
                    hdnLocationEntry: {
                        required: true
                    },
                    hdnAssesseeEntry: {
                        required: true
                    },
                    ddlPayDescEntryInd: {
                        required: true
                    },
                    ddlApplicableTypeRangeEntryInd: {
                        required: true
                    },
                    ddlFromFinYearEntryInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntryInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlFromQtrEntryInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntryInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToFinYearEntryInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntryInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlToQtrEntryInd: {
                        required: function (element) {
                            if ($("#ddlApplicableTypeRangeEntryInd").val() == 'R') {
                                return true;
                            }
                        }
                    },
                    ddlRateTypeEntryInd: {
                        required: true
                    },
                    txtRateValueEntryInd: {
                        required: true,
                        max: function (element) {
                            if ($("#ddlRateTypeEntryInd").val() == 'P') {
                                return 100;
                            }
                        }
                    },
                    'chkEntryInd[]': {
                        required: true,
                        //maxlength: 1
                    },
                    txtGracePeriodEntryInd: {
                        required: true
                    },
                    txtRemarksEntryInd: {
                        required: true
                    }
                },
                messages: {
                    txtFromDateEntryInd: {
                        required: "Please select Date From."
                    },
                    txtToDateEntryInd: {
                        required: "Please select Date To."
                    },
                    hdnWardEntry: {
                        required: "Please select Ward."
                    },
                    hdnLocationEntry: {
                        required: "Please select Location."
                    },
                    hdnAssesseeEntry: {
                        required: "Please select Assessee."
                    },
                    ddlPayDescEntryInd: {
                        required: "Please select Pay Head."
                    },
                    ddlApplicableTypeRangeEntryInd: {
                        required: "Please Select."
                    },
                    ddlFromFinYearEntryInd: {
                        required: "Please Select From FinYear."
                    },
                    ddlFromQtrEntryInd: {
                        required: "Please Select From Quarter."
                    },
                    ddlToFinYearEntryInd: {
                        required: "Please Select To FinYear."
                    },
                    ddlToQtrEntryInd: {
                        required: "Please Select To Quarter."
                    },
                    ddlRateTypeEntryInd: {
                        required: "Please select Pay Rate Type."
                    },
                    txtRateValueEntryInd: {
                        required: "Please Enter Pay Rate Value.",
                        max: "Percent value must be between 0 to 100"
                    },
                    'chkEntryInd[]': {
                        required: "You must check at least 1 Applicable Type",
                        //maxlength: "Check no more than {0} boxes"
                    },
                    txtGracePeriodEntryInd: {
                        required: "Please Enter Grace Period Value."
                    },
                    txtRemarksEntryInd: {
                        required: "Please Enter Proper Remarks for Adjustment."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") === "checkbox") {
                        error.insertAfter($(element).closest('div'));
                    }
                    else {
                        error.insertAfter($(element)); // standard behaviour
                    }
                }
            };
            objIndividualEntry.ignore = [];
            $("#frmAdjIndiEntry").validate(objIndividualEntry);
            return $('#frmAdjIndiEntry').valid();
        }

        var uuidv4 = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        var OnViewOtherAdjustAll = function () {
            if (From_ValidateForAdjustAll()) {
                var AdjAllData = { EffectFromDT: $('#dtFrom').val(), EffectToDT: $('#dtTo').val() };
                serverObject.ViewAdjustAllData(AdjAllData, function (response) {
                    var status = response.Status;
                    if (status == 200) {

                        modelData = [];
                        MstOthAdjID = "";
                        MunicipalityID = "";
                        if (response.Data.length > 0) {
                            $.each(response.Data, function (index, value) {
                                var obj = value;
                                obj.UnqID = uuidv4();
                                modelData.push(obj);
                            });
                            // console.log(modelData);
                            PopulateAllAdjTable(modelData);
                        }
                        else {
                            alert("No Record found!!!");
                            var tabAllAdj = $("#tabAllAdj");
                            tabAllAdj.find("tbody").empty();
                        }

                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var PopulateAllAdjTable = function (responseData) {
            var tabAllAdj = $("#tabAllAdj");
            tabAllAdj.find("tbody").empty();

            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {

                    var rowstr = "<tr>";
                    rowstr += "<td align='center' class='FinYear'>" + value.EffectFromDT + "</td>";
                    rowstr += "<td align='center' class='FinYear'>" + value.EffectToDT + "</td>";
                    rowstr += "<td align='center' class='QtrNo'>" + value.PayHeadDesc + "</td>";
                    rowstr += "<td align='center' class='EffectiveDateForm'>" + value.RateValue + "</td>";
                    rowstr += "<td align='center'><span data-ID=" + value.UnqID + " id='spanEdit' style='cursor: pointer' data-toggle='modal' data-target='#myModal'><img src='/Contents/image/edit.png'/></span></td>"
                    rowstr += "</tr>";

                    tabAllAdj.find("tbody").append(rowstr);
                });
                RegistertabAllAdjEvent();

            }
        }

        var OnViewOtherAdjustInd = function () {
            if (From_ValidateForAdjustIndividual()) {

                var EffectFromDT = $('#dtFromInd').val()
                var EffectToDT = $('#dtToInd').val();
                var AssesseeID = $('#hdnAssessee').val();
                serverObject.ViewAdjustIndData(EffectFromDT, EffectToDT, AssesseeID, function (response) {
                    var status = response.Status;
                    if (status == 200) {

                        modelDataIndividual = [];
                        MstOthAdjID = "";
                        DtlID = "";
                        AssesseeID = "";
                        MunicipalityID = "";
                        if (response.Data.length > 0) {
                            $.each(response.Data, function (index, value) {
                                var obj = value;
                                obj.UnqID = uuidv4();
                                modelDataIndividual.push(obj);
                            });
                            // console.log(modelDataIndividual);
                            PopulateIndAdjTable(modelDataIndividual);
                        }
                        else {
                            alert("No Record found!!!");
                            var tabIndAdj = $("#tabIndAdj");
                            tabIndAdj.find("tbody").empty();
                        }

                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var PopulateIndAdjTable = function (responseData) {
            var tabIndAdj = $("#tabIndAdj");
            tabIndAdj.find("tbody").empty();

            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {

                    var rowstr = "<tr>";
                    rowstr += "<td align='center' class='FinYear'>" + value.EffectFromDT + "</td>";
                    rowstr += "<td align='center' class='FinYear'>" + value.EffectToDT + "</td>";
                    rowstr += "<td align='center' class='QtrNo'>" + value.PayHeadDesc + "</td>";
                    rowstr += "<td align='center' class='EffectiveDateForm'>" + value.RateValue + "</td>";
                    rowstr += "<td align='center'><span data-ID=" + value.UnqID + " id='spanEditInd' style='cursor: pointer' data-toggle='modal' data-target='#myModalIndividual'><img src='/Contents/image/edit.png'/></span></td>"
                    rowstr += "</tr>";

                    tabIndAdj.find("tbody").append(rowstr);
                });
                RegistertabIndAdjEvent();
            }
        }

        this.app_Initiate = function () {

            serverObject.PayHead(function (response) {
               // DistrictDataSource = response.Data;
                PopulatePayHeadData(response.Data);
            });
            BindEvent();
        };


        var AutocompleteSource = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnWard').val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        $('#hdnWard').val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnLocation').val('');
                        Resetors.ResetAssesse();
                        //reset area
                        var wardID = $('#hdnWard').val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        $('#hdnLocation').val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnAssessee').val('');
                        //reset area
                        var locationID = $('#hdnLocation').val();
                        var wardID = $('#hdnWard').val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName + '<' + item.HoldingNo + '>',
                                            AssesseeID: item.AssesseeID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        $('#hdnAssessee').val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            }
        };

        var AutocompleteSource2 = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnWardEntry').val('');
                        Resetors2.ResetLocation();
                        Resetors2.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        $('#hdnWardEntry').val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors2.ResetLocation();
                            Resetors2.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnLocationEntry').val('');
                        Resetors2.ResetAssesse();
                        //reset area
                        var wardID = $('#hdnWardEntry').val();
                       // alert(wardID);
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        $('#hdnLocationEntry').val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors2.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnAssesseeEntry').val('');
                        //reset area
                        var locationID = $('#hdnLocationEntry').val();
                        var wardID = $('#hdnWardEntry').val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName + '<' + item.HoldingNo + '>',
                                            AssesseeID: item.AssesseeID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        $('#hdnAssesseeEntry').val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors2.ResetAssesse();
                        }
                    }
                };
            }
        };

        var Resetors = {
            ResetWard: function () {
                $('#hdnWard').val('');
                $('#txtWard').val('');
            },
            ResetLocation: function () {
                $('#hdnLocation').val('');
                $('#txtLocation').val('');
            },
            ResetAssesse: function () {
                $('#hdnAssessee').val('');
                $('#txtAssessee').val('');
            }
        };

        var Resetors2 = {
            ResetWard: function () {
                $('#hdnWardEntry').val('');
                $('#txtWardEntry').val('');
            },
            ResetLocation: function () {
                $('#hdnLocationEntry').val('');
                $('#txtLocationEntry').val('');
            },
            ResetAssesse: function () {
                $('#hdnAssesseeEntry').val('');
                $('#txtAssesseeEntry').val('');
            }
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();


    };

    (function () {
        INIT();
    }());
});