﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetPaymentDetails = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/online/onlinepayment/Get_PropertyTaxPaymentDetails",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetCounter = function (callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/CurSession/GetCurrentPaymentCounter",
                data: null,
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetPaymentModes = function (callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/CurSession/GetPaymentModes",
                data: null,
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.MakePaymentOnline = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/online/onlinepayment/MakePaymentTemp",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }; 
        this.GetLast20Payments = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/GetLast20PaymentsSummary",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        
    };
    var APP_OBJECT = function (serverObject) {
        var appObj = this;
        var GlobalVariables = {
            IsSearchEnabled: true,
            PaymentObject: {},
            PaymentModes: [],
            PaymentCounter: {},
            NetAmount:0
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                                
                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnWard.val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        Resetors.ResetAssesse();
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });
                               
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnAssessee.val('');
                        //reset area
                        var locationID = DomControls.ctrl_hdnLocation.val();
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName,
                                            AssesseeID: item.AssesseeID
                                        });
                                    });
                                    
                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            }
        };

        //control ids changed for online payment
        var DomControls = {
            ctrl_btnPay: $("#btnPay"),
            ctrl_btnSearch: $("#btnSearch"),

            ctrl_hdnMunicipality: $("#hdnMunicipality"),
            ctrl_txtMunicipality: $("#txtMunicipality"),

            ctrl_hdnWard: $("#hdnWard"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#hdnLocation"),
            ctrl_txtLocation: $("#txtLocation"),


            ctrl_txtAssessee: $("#txtAssessee"),
            ctrl_hdnAssessee: $("#hdnAssessee"),
            ctnr_gridContainer: $("#gridContainer"),

            ctrl_CollectionDate: $("#txtCollectionDate"),
            ctrl_PaidAmnt: $("#txtPaidAmnt"),
            ctrl_txtCollectionDate: $("#txtCollectionDate")
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            },
            ResetAssesse: function () {
                DomControls.ctrl_hdnAssessee.val('');
                DomControls.ctrl_txtAssessee.val('');
            },
            ResetCollectionDt: function () {
                DomControls.ctrl_txtCollectionDate.val('');
            }
        };
        var OnSearchClick = function () {
            if (GlobalVariables.IsSearchEnabled === false) {
                ToggleSearchField();
                return;
            }
            var data = searchableObject();
            PopulateData(data, function (response) {
                ToggleSearchField();
               
            });

        };

        //[chagned on online payment,]
        var searchableObject = function () {
            //online payment
            return { MunicipalityID: DomControls.ctrl_hdnMunicipality.val(), WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val(), AssesseeID: DomControls.ctrl_hdnAssessee.val(), PaymentDate: DomControls.ctrl_txtCollectionDate.val(), TaxPaymentCheckParameters: Grid.GetSelectedRowObjs() };
            //offline payment
            //return { WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val(), AssesseeID: DomControls.ctrl_hdnAssessee.val(), PaymentDate: DomControls.ctrl_txtCollectionDate.val(), TaxPaymentCheckParameters: Grid.GetSelectedRowObjs() };
        };
        var PaymentCompletionModal = new function () {
            var modalParent = this;
            var modalControl = $('#modal_MakePaymentComplete');
            this.GetPaymentInstrumentInfo = function () {
                var paymentInfo = [];
                GlobalVariables.PaymentModes.forEach(function (value, index) {
                    var paymntTemplate = $("#modal_MakePaymentComplete .modal-body .tabContainer .tab-content").find('#' + value.PaymentModeCode);
                    if (value.HasInstrument == true) {
                        var GetInstruments = function () {
                            var rows = paymntTemplate.find('table').find('tbody').find('tr');
                            var values = [];
                            rows.each(function (indexRow, valueRow) {
                                values.push({
                                    InstrumentNo: $(valueRow).find('.instrument-no').val(),
                                    InstrumentDate: $(valueRow).find('.instrument-date').val(),
                                    InstrumentAmount: Number(isNaN($(valueRow).find('.diposit-amount').val()) ? 0 : $(valueRow).find('.diposit-amount').val()),
                                    BankName: $(valueRow).find('.instrument-BankName').val(),
                                    BankCode: null
                                });
                            });
                            return values;
                        };
                        var instruments = GetInstruments();
                        var totalAmount = 0;
                        $.each(instruments, function (index,value) {
                            totalAmount += value.InstrumentAmount;
                        });
                        totalAmount = (typeof (totalAmount) === 'undefined' ? 0 : totalAmount);
                        paymentInfo.push({
                            PaymentModeCode: value.PaymentModeCode,
                            PaymentModeDescription: value.PaymentModeDescription,
                            TotalAmount: totalAmount,
                            HasInstrument: value.HasInstrument,
                            PaymentInstruments: instruments
                        });
                    } else {
                        var totAmount = Number(isNaN($(paymntTemplate).find('.diposit-amount').val()) ? 0 : $(paymntTemplate).find('.diposit-amount').val());
                        paymentInfo.push({
                            PaymentModeCode: value.PaymentModeCode,
                            PaymentModeDescription: value.PaymentModeDescription,
                            TotalAmount: totAmount,
                            HasInstrument: value.HasInstrument,
                            PaymentInstruments: []
                        });
                    }
                });
                return paymentInfo;
            };
            this.ShowModal = function () {
                modalControl.modal('show');
            };
            this.ResetModal = function () { };
            var validate = function () {
                //field validator
                
                    modalControl.find(".modal-footer .field-error").hide();//hide all error fields
                    var selectedPGVal = modalControl.find('.ddl-payment-gateway').val();
                    if (selectedPGVal == "" || selectedPGVal == null || selectedPGVal== undefined)
                    {
                        modalControl.find(".modal-footer .field-error").html('Please select payment gateway');
                        modalControl.find(".modal-footer .field-error").show();
                        return false;
                    }
                    return true;
            };
            var OnClickCompeletePayment = function () {
                if (validate() && confirm("Are you sure to confirm payment?")) {
                    var payLoad = searchableObject();
                    if (typeof (payLoad) != 'undefined')
                    {
                        payLoad.PG = modalControl.find('.ddl-payment-gateway').val();
                        payLoad.Email = modalControl.find('#txtEmail').val();
                        payLoad.Mobile = modalControl.find('#txtPhno').val();
                        serverObject.MakePaymentOnline(payLoad, function (response) {
                            if (response.Status === 200)
                            {
                                var data = response.Data;
                                if (data.hasOwnProperty('RedirectTo'))
                                {
                                    location.href = data.RedirectTo;
                                }
                            } else {
                                alert('Error: ' + response.Message);
                            }
                        });
                    } else {
                        alert('Invalid Payload');
                    }
                }
            };
            var BindEvents = function () {
                $(document).off('click', '.complete-payment', OnClickCompeletePayment);
                $(document).on('click', '.complete-payment', OnClickCompeletePayment);
            };
            (function () {
                BindEvents();
            }());
        };
        //[obsolete]
        var Last20PaymentControl = function () {
            var cntr_table = $(".table-last20payments")
            this.Load = function () {
                serverObject.GetLast20Payments(null, function (response) {
                    if (response.Status === 200)
                    {
                        loadTableBody(response.Data);
                    } else {
                        alert("Error: " + response.Message);
                    }
                });
            };
            var emptyBody = function () {
                cntr_table.find('tbody').empty();
            };
            var loadTableBody = function (Datasource) {
                emptyBody();
                $.each(Datasource, function (index, value) {
                    cntr_table.find('tbody').append(getRowstring( value, ++index));;
                });
            };
            var getRowstring = function (rowObject,rowNumber) {
                var tr = "<tr>";
                tr += "<td>" + rowNumber + "</td>";
                tr += "<td>" + rowObject.AssesseeName+"</td>";
                tr += "<td>" + rowObject.HoldingNo+"</td>";
                tr += "<td>" + CONVERTER.Date.convertCsToJsDate(rowObject.PaymentReceiveDate).format('dd/mm/yyyy')+"</td>";
                tr += "<td>" + rowObject.NetAmount+"</td>";
                tr += "<td><a target='_blank' href='/Municipality/MnReports/PaymentReciept?PaymentId=" + rowObject.PaymentID +"'>Print Reciept</a></td>";
                tr += "</tr>";
                return tr;
            };
        };
        var Grid = new function () {
            var ExpandexGroupList = [];
            var totalColNumber = 11;
            var DataSet = [];
            var GiantObject = {};
            var tablContainer = $("#gridContainer");
            var TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
            if (TblCtrl.length === 0) {
                tablContainer.append("<table id='B98E8A35-C901-45A4-8AFF-B486BC58E199' style='width: 100%' class='table table-condensed table-bordered table-responsive'><thead></thead><tbody></tbody><tfoot></tfoot></table>");
                TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
            }
            var tableHeader = function () {
                var hr = "<tr >" +
                    "<th rowspan='2' style=' text-align: center;'>Fin Year</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Qtr</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Property Tax</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Surcharge</th>" +
                    "<th  colspan='2' style=' text-align: center;'>Outstainding</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Rebate/ Interest</th>" +
                    "<th  colspan='2' style=' text-align: center;'>Paying</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Action<br/><input type='checkbox' class='checkall'/></th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Line Total</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Payment Flow</th>" +
                    "</tr>" +
                    "<tr >" +
                    "<th style=' text-align: center;'>P. Tax</th>" +
                    "<th style=' text-align: center;'>Surcharge</th>" +
                    "<th style=' text-align: center;'>P. Tax</th>" +
                    "<th style=' text-align: center;'>Surcharge</th>" +
                    "</tr>";

                return hr;
            };
            var tableBody = function () {
                var rowFormattedObjects = function () {
                    if (GiantObject === null || typeof (GiantObject) == 'undefined' || GiantObject.hasOwnProperty("propertyTaxOutstadings") == false || GiantObject.hasOwnProperty("propertyTaxPaymentMaster") == false) {
                        throw "Invalid DataSource";
                    }
                    var lst = [];
                    $.each(GiantObject.propertyTaxOutstadings, function (index, value) {
                        var _calculatedDetail = GiantObject.propertyTaxPaymentMaster.PropertyTaxPaymentDetails[index];
                        var viewRow = {
                            FinYear: value.FinYear,
                            PropertyTaxValue: value.PropertyTaxValue,
                            SurchargeValue: value.SurchargeValue,
                            QtrNo: value.QtrNo,
                            Rank: value.Rank,
                            OSPropertyTaxAmt: value.OSPropertyTaxAmt,
                            OSSurchargeAmt: value.OSSurchargeAmt,
                            OSCalculatedValue: _calculatedDetail.CalculatedValue,
                            PaidSurchargeAmt: _calculatedDetail.PaidSurchargeAmt,
                            PaidPropertyTaxAmt: _calculatedDetail.PaidTaxAmt,
                            PaidCalculatedAmt: _calculatedDetail.PaidCalculatedAmt,
                            PayheadDetailLvl: _calculatedDetail.payHead,
                            IsChecked: _calculatedDetail.taxPaymentCheckParameter.IsChecked,
                            Rank: _calculatedDetail.taxPaymentCheckParameter.Rank
                        };
                        lst.push(viewRow);
                    });
                    return lst;
                };
                var lstRowStrings = "";

                var lstFormattedDatasource = rowFormattedObjects();
                DataSet = lstFormattedDatasource;

                if (lstFormattedDatasource.length > 0) {
                    window.groupbyTestSource = lstFormattedDatasource;
                    //group by finyear
                    alasql('SELECT FinYear FROM ? GROUP BY FinYear order by CAST(SUBSTRING(FinYear, 1, 4) AS NUMBER) asc', [groupbyTestSource]).forEach(function (valuep, index) {
                        var getColorCode = randomColor({
                            luminosity: 'light',
                            hue: 'blue'
                        });;
                        //add header row
                        var headerDiv = "<div data-isExpanded='false'><span class='state-symbol'>+</span>&nbsp;&nbsp;<span class='headerTitle'>" + valuep.FinYear + "</span></div>";
                        var summaryParameters = { osPtax: 0, osSurcharge: 0, calvalue: 0, paidPtax: 0, paidSurcharge: 0, groupCheck: "<input type='checkbox' class='groupCheck' data-finyear='" + valuep.FinYear + "'/>", tillTotal: 0 };
                        lstRowStrings += "<tr  class='data-header' data-FinYear='" + valuep.FinYear + "' style='text-align: center;background-color:" + getColorCode + ";cursor:pointer;'>" +
                            "<td colspan='4' style='text-align: left;' class='Summary'>" + headerDiv + "</td>" +
                            "<td class='OSPropertyTaxAmt'><span>" + summaryParameters.osPtax + "</span></td>" +
                            "<td  class='OSSurchargeAmt'><span>" + summaryParameters.osSurcharge + "</span></td>" +
                            "<td class='PaidCalculatedAmt'><span>" + summaryParameters.paidPtax + "</span></td>" +
                            "<td class='PaidPropertyTaxAmt'><span>" + summaryParameters.paidSurcharge + "</span></td>" +
                            "<td class='PaidSurchargeAmt'><span>" + summaryParameters.calvalue + "</span></td>" +
                            "<td><span>" + summaryParameters.groupCheck + "</span></td>" +
                            "<td class='lineTotal'><span>0</span></td>" +
                            "<td class='lineTotalFlow'><span></span></td></tr>";

                        var elementsByGroup = alasql("SELECT * FROM ? where FinYear='" + valuep.FinYear + "' order by Rank", [window.groupbyTestSource]);
                        $.each(elementsByGroup, function (index, value) {
                            var tr = "<tr class='data-row' data-rank='" + value.Rank + "' data-parentGroup='" + value.FinYear + "' style=' text-align: center;display:none;'>";
                            tr += "<td class='FinYear'><span>" + value.FinYear + "</span></td>";
                            tr += "<td class='QtrNo'>" + value.QtrNo + "</td>";
                            tr += "<td class='PropertyTaxValue'>" + value.PropertyTaxValue + "</td>";
                            tr += "<td class='SurchargeValue'>" + value.SurchargeValue + "</td>";
                            tr += "<td class='OSPropertyTaxAmt'>" + value.OSPropertyTaxAmt + "</td>";
                            tr += "<td class='OSSurchargeAmt'>" + value.OSSurchargeAmt + "</td>";
                            tr += "<td class='PaidCalculatedAmt'>" + value.PaidCalculatedAmt + "</td>";
                            tr += "<td class='PaidPropertyTaxAmt'>" + value.PaidPropertyTaxAmt + "</td>";
                            tr += "<td class='PaidSurchargeAmt'>" + value.PaidSurchargeAmt + "</td>";
                            tr += "<td class='IsChecked'><input class='chkPay' type='checkbox' " + (value.IsChecked === true ? " checked " : "") + " data-FinYear='" + value.FinYear + "' data-QtrNo='" + value.QtrNo + "' data-rank='" + value.Rank + "'/></td>";
                            tr += "<td class='lineTotal'><span>" + (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt).toFixed(2) + "</span></td>";
                            tr += "<td class='lineTotalFlow'><span >0</span></td>";
                            tr += "</tr>";
                            lstRowStrings += tr;
                        });
                    });


                } else {
                    lstRowStrings = "<tr><td coldspan='11'>No Record found</td></tr>";
                }
                return lstRowStrings;
            };
            var tableFooter = function () {
                var getTotalPtaxSurch = function (totalPtaxSurchg) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total PTax./SChrg.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalPtaxSurch'>" + totalPtaxSurchg + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getTotalRebateInt = function (totalRbtInt) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Rbt./Int.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalRebateInt'>" + totalRbtInt + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getTotalOtherAdjAmount = function (totalOthrAdjAmnt) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Adjst. Amnt.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalOtherAdjAmount'>" + totalOthrAdjAmnt + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getRoundOff = function (roundOff) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Rounded Off</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='RoundOff'>" + roundOff + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };

                var getNetPayable = function (netPayable) {
                    var x = "<tr style='font-size: 18px;'>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Net Payable</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='NetPayable' >" + netPayable + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };

                var getMainFooterSummary = function () {
                    var x = "<div class='container-fluid'>" +
                        "<div class='row'>" +
                        "<div class='col-md-6'><table><tbody>" + getTotalPtaxSurch(0.00) + getTotalRebateInt(0.00) + getTotalOtherAdjAmount(0.00) + getRoundOff(0.00) + "</tbody></table></div>" +
                        "<div class='col-md-6'><table><tbody>" + getNetPayable(0.00) + "</tbody></table></div>" +
                        "</div>" +
                        "</div>";
                    return x;
                };

                var tf = "<tr class='data-summary' style=' text-align: center;'>";
                tf += "<td class='Summary' colspan='4'>Summary</td>";
                tf += "<td class='OSPropertyTaxAmt'><span>0</span></td>";
                tf += "<td class='OSSurchargeAmt'><span>0</span></td>";
                tf += "<td class='PaidCalculatedAmt'><span>0</span></td>";
                tf += "<td class='PaidPropertyTaxAmt'><span>0</span></td>";
                tf += "<td class='PaidSurchargeAmt'><span>0</span></td>";
                tf += "<td class='check'><span>0</span></td>";
                tf += "<td class='lineTotal'><span>0</span></td>";
                tf += "<td class='lineTotalFlow'><span></span></td>";
                tf += "</tr>";

                tf += "<tr class='data-summary-payment' style=' text-align: center;'>";
                tf += "<td class='Payment-Summary' colspan='4'></td>";
                tf += "<td class='' colspan='6'>" + getMainFooterSummary() + "</td>";
                //tf += "<td class='roundOf' colspan='3'><span style='font-weight: bolder;font-size: 17px'>Round Off: 0 Rs/-</span></td>";
                tf += "<td class='payButton' colspan='2' style='vertical-align: middle;'><input type='button' class='btn btn-primary btn-block pull-right proceed-payment ' value='Proceed' id='btnProceedPayment' ></td>";
                tf += "</tr>";
                return tf;
            };
            this.DataBind = function () {
                TblCtrl.find('thead').empty();
                TblCtrl.find('thead').append(tableHeader());

                TblCtrl.find('tbody').empty();
                TblCtrl.find('tbody').append(tableBody());

                TblCtrl.find('tfoot').empty();
                TblCtrl.find('tfoot').append(tableFooter());
                SetLineTotalFlow();
                SetTableFooter();
                ManageHeaderCheckBoxes();
            };
            var SetLineTotalFlow = function () {
                var lineTotal = 0;
                $.each(DataSet, function (index, value) {
                    if (value.IsChecked == true) {
                        lineTotal += (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt);
                        lineTotal = Number(lineTotal.toFixed(2));
                        var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentgroup='" + value.FinYear + "'][data-rank='" + value.Rank + "'] .lineTotalFlow span");
                        tergateCtrl.html(lineTotal);
                    }
                });

                var groupSums = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                    'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true GROUP BY FinYear ', [DataSet]);
                $.each(groupSums, function (index, value) {
                    var tergateRow = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + value.FinYear + "']");
                    var lineTotalFlow = tergateRow.find(" .lineTotal span");
                    var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                    var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                    var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                    var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                    var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");

                    lineTotalFlow.html(value.lineTotal.toFixed(2));
                    PaidCalculatedAmt.html(value.PaidCalculatedAmt.toFixed(2));
                    PaidPropertyTaxAmt.html(value.PaidPropertyTaxAmt.toFixed(2));
                    PaidSurchargeAmt.html(value.PaidSurchargeAmt.toFixed(2));
                    OSPropertyTaxAmt.html(value.OSPropertyTaxAmt.toFixed(2));
                    OSSurchargeAmt.html(value.OSSurchargeAmt.toFixed(2));
                });
            };
            var SetTableFooter = function () {

                var summary = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                    'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true', [DataSet]);

                if (GiantObject.hasOwnProperty("propertyTaxPaymentMaster") && GiantObject.propertyTaxPaymentMaster) {
                    var PTaxmasterObj = GiantObject.propertyTaxPaymentMaster;

                    var tergateRow = $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary");
                    var lineTotalFlow = tergateRow.find(" .lineTotal span");
                    var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                    var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                    var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                    var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                    var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");
                    var totalChecked = tergateRow.find(" .check");

                    var groupByFinYearChecked = alasql('SELECT Count(*)as checkCounts FROM ?  where IsChecked=true', [DataSet]);

                    if (summary.length > 0) {
                        summary = summary[0];
                        lineTotalFlow.html(summary.lineTotal.toFixed(2));
                        PaidCalculatedAmt.html(summary.PaidCalculatedAmt.toFixed(2));
                        PaidPropertyTaxAmt.html(summary.PaidPropertyTaxAmt.toFixed(2));
                        PaidSurchargeAmt.html(summary.PaidSurchargeAmt.toFixed(2));
                        OSPropertyTaxAmt.html(summary.OSPropertyTaxAmt.toFixed(2));
                        OSSurchargeAmt.html(summary.OSSurchargeAmt.toFixed(2));
                        totalChecked.html(groupByFinYearChecked.length > 0 ? groupByFinYearChecked[0].checkCounts : 0);
                    }
                    //summary value gather data

                    var TotalPtaxSurch = PTaxmasterObj.GrossAmount;
                    var TotalRebateInt = summary.PaidCalculatedAmt.toFixed(2);
                    var TotalOtherAdjAmount = PTaxmasterObj.AdjustedAmount;
                    var RoundOff = PTaxmasterObj.RoundOffAdjust;
                    var NetPayable = PTaxmasterObj.NetAmount ;
                    ///end gathering data

                    //summary description
                    var containerTable = ("<table><tbody>");
                    var paymentSummay = "";
                    $.each(PTaxmasterObj.AdjustmentSummaries, function (index, value) {
                        paymentSummay += "<tr><td style='text-align:left;'>" + value.WellKnownName + "</td><td>:</td><td> &nbsp;" + value.AdjustedAmount + " Rs/-</td></tr>";
                    });
                    containerTable += paymentSummay;
                    containerTable += "</tbody></table>";



                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalPtaxSurch').text(TotalPtaxSurch);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalRebateInt').text(TotalRebateInt);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalOtherAdjAmount').text(TotalOtherAdjAmount);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.RoundOff').text(RoundOff);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.NetPayable').text(NetPayable);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.Payment-Summary').html(containerTable);

                    GlobalVariables.NetAmount = NetPayable;
                    $("#modal_MakePaymentComplete .modal-header .total-Payable-amount").html(GlobalVariables.NetAmount);
                }
            };
            var OnClickProceedButton = function () {
                if (GlobalVariables.NetAmount == 0)
                {
                    alert('net amount can not be 0'); return;
                }
                PaymentCompletionModal.ShowModal();
            };
            //manage check uncheck state group header check boxes and Select All Check box
            var ManageHeaderCheckBoxes = function () {
                var groupByFinYearAll = alasql('SELECT count(*) as rowCounts,FinYear FROM ?   group by FinYear', [DataSet]);
                var groupByFinYearChecked = alasql('SELECT sum(case when IsChecked=true then 1 else 0 end)as rowCounts ,FinYear FROM ?  group by FinYear', [DataSet]);

                var joinedList = alasql("select A.rowCounts as TotalRowCount,B.rowCounts as TotalCheckedRowCount,A.FinYear  from  ? as A  join  ? as B on A.FinYear=B.FinYear", [groupByFinYearAll, groupByFinYearChecked]);
                //manage check box from group header
                $.each(joinedList, function (index, value) {
                    var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header .groupCheck[data-finyear='" + value.FinYear + "']");
                    if (tergateCtrl.length > 0) {
                        tergateCtrl.prop('checked', (value.TotalCheckedRowCount === value.TotalRowCount))

                    }
                });

                //manage all select check box
                var totalListSummary = alasql("select SUM(TotalRowCount) as TotalRowCount,SUM(TotalCheckedRowCount) as TotalCheckedRowCount  from  ? ", [joinedList]);
                if (totalListSummary.length > 0) {
                    var tergateCtrl = $("#" + TblCtrl.attr('id') + " > thead >tr .checkall");
                    if (tergateCtrl.length > 0) {
                        tergateCtrl.prop('checked', (totalListSummary[0].TotalCheckedRowCount === totalListSummary[0].TotalRowCount))

                    }
                }



            };
            this.GetDataSource = function () {
                return DataSet;
            };

            var OnCheckBoxClick = function () {

                var ctrl = $(this);
                var classes = ctrl.attr('class').split(' ');

                //identify data row checkboxes
                if (classes.indexOf("chkPay") > -1) {
                    var curRank = ctrl.attr('data-Rank');

                    //check all previous check box
                    var totalRank = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                    for (var i = 1; i <= totalRank; i++) {
                        var chkContrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row .chkPay[data-rank='" + i + "']");
                        var nearByGroup = $(".groupCheck[data-finyear='" + chkContrl.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                        if (ctrl.prop('checked') == true && i <= curRank) {
                            chkContrl.prop('checked', true);
                            if (nearByGroup.prop("checked") == false) {
                                nearByGroup.prop('checked', true);
                            }
                        } else if (i > curRank) {
                            if (nearByGroup.prop("checked") == true) {
                                nearByGroup.prop('checked', false);
                            }
                            chkContrl.prop('checked', false);
                        }
                    }
                }
                //identify group check boxes
                else if (classes.indexOf("groupCheck") > -1) {
                    var curFinYear = ctrl.attr("data-finyear");
                    var lowerCheckbxes = $(".chkPay[data-finyear='" + curFinYear + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                    var totalRowsCount = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                    var lastRankOfCheckboxList = Number($(lowerCheckbxes[lowerCheckbxes.length - 1]).attr("data-rank"));
                    var firstRankOfCheckboxList = Number($(lowerCheckbxes[0]).attr("data-rank"));
                    //var uncheckArrayforGroupCheck = [];
                    for (var i = 1; i <= totalRowsCount; i++) {
                        var trgateCheckBox = $(".chkPay[data-rank='" + i + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                        var nearByGroup = $(".groupCheck[data-finyear='" + trgateCheckBox.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                        if (ctrl.prop('checked') == true && i <= lastRankOfCheckboxList) {
                            trgateCheckBox.prop('checked', true);
                            if (nearByGroup.prop("checked") == false) {
                                nearByGroup.prop('checked', true);
                            }
                        } else if (ctrl.prop('checked') == false && i >= firstRankOfCheckboxList) {
                            if (nearByGroup.prop("checked") == true) {
                                nearByGroup.prop('checked', false);
                                $(".checkall", "#" + TblCtrl.attr('id')).prop('checked', false);
                            }
                            trgateCheckBox.prop('checked', false);
                        }
                    };

                }
                //identify header check boxes
                else if (classes.indexOf("checkall") > -1) {
                    var trgateCheckBoxes = $(".chkPay, .groupCheck", "#" + TblCtrl.attr('id') + " > tbody >tr");
                    trgateCheckBoxes.prop('checked', ctrl.prop('checked'));
                }
                var data = searchableObject();

                PopulateData(data, function (resp) {
                    ManageGroupVisibility();
                });
            };
            var ManageGroupVisibility = function () {
                $.each(ExpandexGroupList, function (index, value) {
                    ExpandGroup(value);
                });
            };
            var BindEvent = function () {
                $(document).off('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
                $(document).on('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
                $(document).off('click', "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div');
                $(document).on("click", "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div', function () {
                    var curDiv = $(this);
                    var currGroup = curDiv.closest('tr').attr('data-finyear');
                    var isExpanded = curDiv.attr('data-isExpanded');
                    if (isExpanded == 'false') {
                        ExpandGroup(currGroup);
                    } else {
                        CollapseGroup(currGroup);
                    }

                });

                $(document).off('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);
                $(document).on('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);
            };
            var ExpandGroup = function (finYear) {
                var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
                var headerDiv = headerTr.find('div');
                var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

                headerDiv.find('.state-symbol').html('-');
                headerDiv.attr('data-isExpanded', 'true');
                relatedDataRows.each(function (indx, elemnt) {
                    $(elemnt).show('fast');
                });
                if ((ExpandexGroupList.indexOf(finYear) > -1) == false)//if not exist
                {
                    ExpandexGroupList.push(finYear);
                }
            };
            var CollapseGroup = function (finYear) {
                var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
                var headerDiv = headerTr.find('div');
                var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

                headerDiv.find('.state-symbol').html('+');
                headerDiv.attr('data-isExpanded', 'false');
                relatedDataRows.each(function (indx, elemnt) {
                    $(elemnt).hide('fast');
                });
                if ((ExpandexGroupList.indexOf(finYear) > -1))//if not exist
                {
                    ExpandexGroupList.splice(ExpandexGroupList.indexOf(finYear), 1);
                }
            };
            //paramter type PTaxCollectionGiantObject which is downloaded from ~/Municipality/Collection/Get_PropertyTaxPaymentDetails
            this.SetDataSource = function (PTaxCollectionGiantObject) {
                GiantObject = PTaxCollectionGiantObject;
            };
            this.Reset = function () {
                DataSet = [];
                GiantObject = {};
                TblCtrl.find('thead').empty();
                TblCtrl.find('tbody').empty();
                TblCtrl.find('tfoot').empty();
            };
            this.GetSelectedRowObjs = function () {
                var inputs = $(".chkPay:checked", "#B98E8A35-C901-45A4-8AFF-B486BC58E199 > tbody >tr.data-row");
                var objs = [];// data-finyear, 2: data-qtrno, 3: data-rank,
                $.each(inputs, function (index, value) {
                    objs.push({
                        FinYear: $(value).attr("data-finyear"),
                        QtrNo: $(value).attr("data-qtrno"),
                        IsChecked: true,
                        Rank: $(value).attr("data-rank")
                    });
                });
                return objs;
            };
            (function () {
                BindEvent();
            }());
        };
        var Validator = {
            ValidateSearchFields: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControls.ctrl_hdnWard.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_hdnLocation.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_hdnAssessee.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[DomControls.ctrl_hdnWard.prop('name')] = { required: "Please select Ward." };
                vldObj.messages[DomControls.ctrl_hdnLocation.prop('name')] = { required: "Please select Location." };
                vldObj.messages[DomControls.ctrl_hdnAssessee.prop('name')] = { required: "Please select Assessee" };
                vldObj.messages[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: "Please select Collection Date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                $("#frmPropertyTaxPaymentSearch").validate(vldObj);
                //validate form and return true and false
                return $('#frmPropertyTaxPaymentSearch').valid();
            }
        };
        var ToggleSearchField = function () {
            if (GlobalVariables.IsSearchEnabled === true) {
                DomControls.ctrl_txtAssessee.attr('disabled', 'disabled');
                DomControls.ctrl_txtLocation.attr('disabled', 'disabled');
                DomControls.ctrl_txtWard.attr('disabled', 'disabled');
                DomControls.ctrl_btnSearch.prop('value', 'Re-Search');
                DomControls.ctrl_txtCollectionDate.attr('disabled', 'disabled');

                GlobalVariables.IsSearchEnabled = false;


            } else {//reset search form
                DomControls.ctrl_txtAssessee.removeAttr('disabled');
                DomControls.ctrl_txtLocation.removeAttr('disabled');
                DomControls.ctrl_txtWard.removeAttr('disabled');
                DomControls.ctrl_txtCollectionDate.removeAttr('disabled');
                DomControls.ctrl_btnSearch.prop('value', 'Search');

                GlobalVariables.IsSearchEnabled = true;

                Resetors.ResetWard();
                Resetors.ResetLocation();
                Resetors.ResetAssesse();
                Resetors.ResetCollectionDt();
                GlobalVariables.PaymentObject = {};
                GlobalVariables.SelectedGridRows = [];
                Grid.Reset();
            }
        };
        //[obsolete]
        var LoadCounter = function () {
            serverObject.GetCounter(function (response) {
                if (response.Status === 200) {
                    if (response.Data !== null) {
                        GlobalVariables.PaymentCounter = response.Data;
                        $(".payment-collection-panel > .panel-heading  .counteDesc").html(response.Data.CounterCode).attr('title', "Counter code " + response.Data.CounterCode);
                    } else {
                        alert("Error: Counter Not Configured for current user");
                    }
                } else {
                    alert("Error: " + response.Message);
                }
            });
        };
        //[obsolete]
        var LoadPaymentModes = function () {
            serverObject.GetPaymentModes(function (response) {
                if (response.Status === 200) {
                    if (response.Data.length > 0) {
                        GlobalVariables.PaymentModes = response.Data;
                        window.ss = response.Data;
                        var paymentModes = "";
                        alasql('select PaymentModeDescription from ?', [response.Data]).forEach(function (value, index) {
                            paymentModes += value.PaymentModeDescription + (index == response.Data.length - 1 ? "" : ", ");
                        });

                        $(".payment-collection-panel > .panel-heading  .payment-mode-desc").html(paymentModes).attr('title', "Available payment modes " + paymentModes);

                    } else {
                        alert("Error: Payment Mode yet Not Configured for Counter");
                    }
                } else {
                    alert("Error: " + response.Message);
                }
            });
        };
        var PopulateData = function (payload, callback) {
            //disabled validator for online payment
            //if (Validator.ValidateSearchFields()) {
                GlobalVariables.PaymentObject = {};

                serverObject.GetPaymentDetails(payload, function (response) {
                    if (response.Status === 200) {
                        var Data = response.Data;
                        if (Data === null) {
                            callback(null);
                            return;
                        }
                        GlobalVariables.PaymentObject = Data;
                        Grid.SetDataSource(GlobalVariables.PaymentObject);
                        Grid.DataBind();
                        callback(Data);

                    } else {
                        alert('Error: ' + response.Message);
                    }
                });
            //}//disabled validator for online payment
        };
        var BindEvents = function () {
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            DomControls.ctrl_txtAssessee.autocomplete(AutocompleteSorces.Assessees());
            DomControls.ctrl_CollectionDate.datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                maxDate: new Date()
            });
            DomControls.ctrl_btnPay.on('click', function () {

            });
            DomControls.ctrl_btnSearch.on('click', OnSearchClick);
        };


        (function () {
            window.OverLay.Show()
            BindEvents();
            //this block added on online, populate data on page load
            $(".payment-collection-panel").find('.panel-body').find('.No-pending').remove();
            PopulateData(searchableObject(), function (response) {
                //seache complete
                window.OverLay.Hide();
                if (response == null)
                {
                    $(".payment-collection-panel").find('.panel-body').append('<h3 class="No-pending" style="text-align:center">No Pendings Found</h3>');
                } else {
                    $(".checkall").prop('checked', true); $(".checkall").trigger('change');
                }
            });
        }());
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    new INIT();
});