﻿$(document).ready(function () {
    $('#txtCollectionDate').datepicker({
        dateFormat: 'yy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'medium'
    });

    var SERVER_OBJECT = function () {
        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        callback(rowDatas);
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
        this.GetpayheadByMncpltyID = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/MnPayHead/Get_Payheads",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        callback(rowDatas);
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.Get_Municipality = function (data, callback) {

            $.ajax({
                type: "POST",
                url: "/Assessee/AutoMunicipality",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        callback(rowDatas);
                    }

                }
            });
        };
        this.GetWard = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocation = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetPropertyTaxDetails = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/Get_PropertyTaxPaymentDetails",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callback(data.Data);
                        
                    } else {
                        alert("Error on loding Property Tax Details");
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.MakePayment = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/MakePayment",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                        callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
    };

   
    var APP_OBJECT = function (serverObject) {
        var OutStandindSearchDetails = [];
        var processor = new PAYMENT_PROCESSOR();
        var PaymentGrid = new PAYMENT_GRID();

        var ctrl_btnPay = $("#btnPay");
        var ctrl_btnSearch = $("#btnSearch");

        var ctrl_hdnMunicipality = $("#hdnMunicipality");
        var ctrl_txtMunicipality = $("#txtMunicipality");

        var ctrl_hdnWard = $("#hdnWard");
        var ctrl_txtWard = $("#txtWard");

        var ctrl_hdnLocation = $("#hdnLocation");
        var ctrl_txtLocation = $("#txtLocation");


        var ctrl_txtAssessee = $("#txtAssessee");
        var ctrl_hdnAssessee = $("#hdnAssessee");
        var ctnr_gridContainer = $("#gridContainer");

        var ctrl_CollectionDate = $("#txtCollectionDate")
        var ctrl_PaidAmnt=$("#txtPaidAmnt");

        var convertCsToJsDate = function (date) {
            if (date != '' || date != null || date != "null") {
                var ToDate = new Date(parseInt((date).substr(6)));
                return ToDate.format("dd/mm/yyyy")

            } else {
                return '';
            }
        };
        var BindEvent = function () {
            ctrl_btnPay.off('click', OnSave);
            ctrl_btnPay.on('click', OnSave);

            ctrl_btnSearch.off('click', SearchPaymentDetails);
            ctrl_btnSearch.on('click', SearchPaymentDetails);
            //$("#btnCalculate").on('click',onClickCalculate);
           
            //$("#txtMunicipality").autocomplete(PopulateMunicipalityAutoComplete());
            $("#txtWard").autocomplete(PopulateWardAutoComplete());
            $("#txtLocation").autocomplete(PopulateLocationAutoComplete());
            $("#txtAssessee").autocomplete(PopulateAssesseesAutoComplete());
            $("#txtCollectionDate,#txtPaidAmnt").on('change', function () {
                onPaymentCalculate();//MunicipalityID
            });
            EventBus.subscribe(processor.eventPaths.OnCompleteCalculation, function (response) {
                ctnr_gridContainer.empty();
                PaymentGrid.DataBind(response, ctnr_gridContainer);
                //Bind_Grid(response, ctnr_gridContainer);
            });
            EventBus.subscribe(processor.eventPaths.OnCompleteCalculation, function (response) {
                var totalPaid = 0;
                $.each(response.PropertyTaxPaymentDetails, function (index, value) {
                    totalPaid += (value.PaidTaxAmt + value.PaidCalculatedAmt + value.PaidSurchargeAmt);
                });
                if (totalPaid > 0) {
                    ctrl_btnPay.attr('disabled', false);
                } else {
                    ctrl_btnPay.attr('disabled', true);
                }
            });
            $(document).off("change", ".chkIsPayable");
            $(document).on("change", ".chkIsPayable", function (event) {
                try {
                    var rank = Number($(this).attr("data-Rank"));
                    processor.CheckUncheckByRankPaybleDemand(rank, $(this).is(":checked"));
                    processor.CalculateByPayableCheckState();
                } catch (e) {
                    //throw e;
                    $(this).prop("checked", !$(this).is(":checked"));
                }
            });
        };
        var OnSave = function () {
            if (confirm("Are you sure to pay?") == true) {
                serverObject.MakePayment({ PropertyTaxPaymentMaster: processor.wellKnownPaymentObject }, function (response) {
                    if (response.Status == 200 && response.Data.hasOwnProperty("MstPaymentID")) {

                        ResetWholePage();
                        $("<a>").attr("href", "/Aspxes/Report.aspx?MstPaymentID=" + response.Data.MstPaymentID).attr("target", "_blank")[0].click();
                        //window.open("/Reporting/PTax_Reciept?MstPaymentID=" + response.Data.MstPaymentID, "_blank");
                    } else {
                        alert("Error: "+response.Message);
                    }
                });
            }
        }

        var onPaymentCalculate = function () {
            var collectionDateString = $("#txtCollectionDate").val();
            var paidAmountString = $("#txtPaidAmnt").val();
            var collectionDate = new Date();
            var paidAmount = 0;
            if (CONVERTER.Date.isDateString(collectionDateString) == true) {
                var dtParts = (collectionDateString).split("-");
                collectionDate = CONVERTER.Date.convertToDate(dtParts[0], dtParts[1], dtParts[2]);
            }
            if (!isNaN(paidAmountString)) {
                paidAmount = Number(paidAmountString);
            }

            serverObject.GetpayheadByMncpltyID({ CollectionDate: collectionDate }, function (payheadResponse) {
                if (payheadResponse.length <= 0) {
                    alert("Error: Payheads are not found. System can not initiate");
                    return;
                }
                processor.Set_PaymentMode("C");
                processor.Set_PaymentType("F");


                processor.Set_PaymentCollectionDate(collectionDate);
                processor.Set_PaymentHead(payheadResponse);
                processor.DataBind(OutStandindSearchDetails);
                processor.CalculateByPayableCheckState();
            });
        };

        var PopulateMunicipalityAutoComplete = function () {
            
            return {
                source: function (request, response) {
                    //reset area
                    ctrl_hdnMunicipality.val('');
                    ResetWard();
                    ResetLocation();
                    ResetAssesse();
                    //end reset area
                    var data = { municipalityParam: $.trim(request.term) };
                    serverObject.Get_Municipality(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        
                    });
                },
                select: function (e, i) {
                    ctrl_hdnMunicipality.val(i.item.MunicipalityID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        ResetWard();
                        ResetLocation();
                        ResetAssesse();
                    }
                }
            };
        };

        var PopulateWardAutoComplete = function () {
            
            return {
                source: function (request, response) {
                    //reset area
                    ctrl_hdnWard.val('');
                    ResetLocation();
                    ResetAssesse();
                    //reset area
                    var municipalityID = ctrl_hdnMunicipality.val();
                    var data = { municipalityID: municipalityID, wardName: $.trim(request.term) };
                    serverObject.GetWard(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.WardName,
                                    WardID: item.WardID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        

                    });
                },
                select: function (e, i) {
                    $("#hdnWard").val(i.item.WardID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        ResetLocation();
                        ResetAssesse();
                    }
                }
            };


        };
        var PopulateLocationAutoComplete = function () {
            
            return {
                source: function (request, response) {
                    //reset area
                    ctrl_hdnLocation.val('');
                    ResetAssesse();
                    //reset area
                    var municipalityID = ctrl_hdnMunicipality.val();
                    var wardID = ctrl_hdnWard.val();
                    var data = { municipalityID: municipalityID, wardID: wardID, locationName: $.trim(request.term) };
                    serverObject.GetLocation(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.LocationName,
                                    LocationID: item.LocationID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                    });
                },
                select: function (e, i) {
                    $("#hdnLocation").val(i.item.LocationID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        ResetAssesse();
                    }
                }
            };
        };
        var PopulateAssesseesAutoComplete = function () {
            
            return {
                source: function (request, response) {
                    //reset area
                    ctrl_hdnAssessee.val('');
                    //reset area
                    var municipalityID = ctrl_hdnMunicipality.val();
                    var locationID = ctrl_hdnLocation.val();
                    var wardID = ctrl_hdnWard.val();
                    var data = { MunicipalityID: municipalityID, wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                    serverObject.GetAssesseebyName(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.AssesseeName,
                                    AssesseeID: item.AssesseeID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        

                    });
                },
                select: function (e, i) {
                    $("#hdnAssessee").val(i.item.AssesseeID);
                    //$("#txtAssessee").val(i.item.label);
                },
                change: function (e, i) {
                    if (!i.item) {
                        ResetAssesse();
                    }
                }
            };
        };
        
        var feedToForm = function (data) {
            $("#hdnAssesseID").val(data.AssesseeID);
            $("#txtMunicipality").val(data.Municipality);
            $("#hdnMunicipality").val(data.MunicipalityID);
            $("#txtWard").val(data.Ward);
            $("#hdnWard").val(data.WardID);

            $("#txtLocation").val(data.Location);
            $("#hdnLocation").val(data.LocationID);

            $("#txtHoldingType").val(data.HoldingType);
            $("#hdnHoldingType").val(data.HoldingTypeID);

            $("#txtHoldingNo").val(data.HoldingNo);
            $("#txtAssesseeName").val(data.AssesseeName);
            $("#txtAssesseAddress").val(data.Address == "null" ? '' : data.Address);

            //var myDate = new Date((data.EffectiveTo).match(/\d+/)[0] * 1);
            if (data.EffectiveTo != '' || data.EffectiveTo != null || data.EffectiveTo != "null") {

            } else {
                var ToDate = new Date(parseInt((data.EffectiveTo).substr(6)));
                var Tresult = ToDate.format("dd/mm/yyyy")
                $("#txtEffectiveTo").val(Tresult == "null" ? '' : Tresult);
            }

            if (data.EffectiveFrom != '' || data.EffectiveFrom != null || data.EffectiveFrom != "null") {

            } else {
                var FromDate = new Date(parseInt((data.EffectiveFrom).substr(6)));
                var Fresult = FromDate.format("dd/mm/yyyy")
                $("#txtEffectiveFrom").val(Fresult == "null" ? '' : Fresult);
            }

            $("#btnInsert").val("Update");
            //ctrl_DrpDistrict.find("option[value='" + DistrictID + "']").prop('selected', true);

        };
        var GetSearchableFormData = function () {
            return {  WardID: ctrl_hdnWard.val(), LocationID: ctrl_hdnLocation.val(), AssesseeID: ctrl_hdnAssessee.val() };
        };
        var validateSearchFormData = function () {
            $("#frmPropertyTaxPaymentSearch").validate({
                ignore: [],
                rules: {
                    hdnWard: { required: true },
                    hdnLocation: { required: true },
                    hdnAssessee: { required: true }
                },
                messages: {
                    
                    hdnWard: {
                        required: "Please select correct ward."
                    },
                    hdnLocation: {
                        required: "Please select correct location."
                    },
                    hdnAssessee: {
                        required: "Please select correct Assessee."
                    }
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                    error.css('color','red');
                }
               
            });
            return $('#frmPropertyTaxPaymentSearch').valid();
        };

        var SearchPaymentDetails = function () {
            ResetePayementArea();
            if (validateSearchFormData() == true) {
                var data = GetSearchableFormData();
                serverObject.GetPropertyTaxDetails(data, function (response) {
                    var rowDatas = response;
                    OutStandindSearchDetails = rowDatas;
                    onPaymentCalculate();
                });
            } 
        };
        var Bind_Grid = function (data, container) {
            var gridId = "tbl-5152536D-84F2-401A-93DD-E47C6B8D0D9B";
            var ctrGrid = $("#" + gridId);
            
            var getRowString = function (rowData) {

                var billDate = "";

                var paymentHeadDesc = "<br/><span style='color:" + (rowData.PayHeadObject.PayHeadBehave=="D"?"Green":"Red") + ";'>[" + rowData.PayHeadObject.PayHeadDesc + " : " + rowData.PayHeadObject.PayRate + "%]</span>";
                var rowstr = "<tr>";
                rowstr += "<td class='FinYear'>" + rowData.FinYear + "</td>";
                rowstr += "<td class='QtrNo'>" + rowData.QtrNo + "</td>";
                rowstr += "<td class='BillDueDate'>" + rowData.BillDueDate.format('yyyy-mm-dd') + "</td>";
                rowstr += "<td class='PropertyTaxValue' >" + rowData.PropertyTaxValue + "</td>";
                rowstr += "<td class='SurchargeValue'>" + rowData.SurchargeValue + "</td>";
                rowstr += "<td class='osTaxValue' >" + rowData.osTaxValue + "</td>";
                rowstr += "<td class='osSurchargeValue' >" + rowData.osSurchargeValue + "</td>";
                rowstr += "<td class='osCalculatedValue' >" + rowData.osCalculatedValue + paymentHeadDesc + "</td>";
                rowstr += "<td class='PaidTaxAmt' >" + rowData.PaidTaxAmt + "</td>";
                rowstr += "<td class='PaidSurchargeAmt' >" + rowData.PaidSurchargeAmt + "</td>";
                rowstr += "<td class='PaidCalculatedAmt' >" + rowData.PaidCalculatedAmt + "</td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var getNotFoundRowString = function () {
                return "<tr><td colspan='6' style='text-align:center'>No Data Found<td></tr>";
            };
            var appendToBody = function (rowString) {
                ctrGrid.find("tbody").append(rowString);
            };
            var poulateSummaryField = function () {
                var totalOutstanding=0;
                var totalPaid=0;
                $.each(data.PropertyTaxPaymentDetails, function (index, value) {
                    totalOutstanding+=(value.TaxValue+value.CalculatedValue+value.SurchargeValue);
                    totalPaid+=(value.PaidTaxAmt+ value.PaidCalculatedAmt+value.PaidSurchargeAmt);
                   });

                var footerString = "<tr class='vb-table-footer-bgcolor'>";
                footerString += "<th colspan='5' class='summaryFieldCommon' style='text-align:right'>Summary</th>";
                footerString += "<th colspan='3' class='summaryFieldOutStanding'><table style='border-collapse: collapse;width: 100%;'><tr><td>Total OutStanding: </td><td style='text-align:right'>Rs. " + totalOutstanding + "</td></tr></table></th>";
                footerString += "<th colspan='3' class='summaryFieldPaid'><table style='border-collapse: collapse;width: 100%;'><tr><td>Total paid:</td><td style='text-align:right'>Rs. " + totalPaid + "</td></tr></table> </th>";
                footerString += "</tr>";
                appendToBody(footerString);
            };
            var dataBind = function (data) {
                $.each(data.PropertyTaxPaymentDetails, function (index, value) {
                    var extraInfo = JSLINQ(data.OutStandindSearchDetails)
                   .Where(function (item) { return item.QtrNo == value.QtrNo; }).Select(function (item) {
                       return {
                           QtrNo: item.QtrNo,
                           PropertyTaxValue: item.PropertyTaxValue,
                           SurchargeValue: item.SurchargeValue,
                           FinYear: item.FinYear,
                           BillDueDate: value.BillDueDate,
                           osCalculatedValue: value.CalculatedValue,
                           osTaxValue: value.TaxValue,
                           osSurchargeValue: value.SurchargeValue,
                           PaidCalculatedAmt: value.PaidCalculatedAmt,
                           PaidTaxAmt: value.PaidTaxAmt,
                           PaidSurchargeAmt: value.PaidSurchargeAmt,
                           PayHeadObject: value.PayHeadObject
                       };
                   }).FirstOrDefault();
                    var rowstr = getRowString(extraInfo);
                    appendToBody(rowstr);
                });
                if (data.length <= 0) {
                    appendToBody(getNotFoundRowString());
                } else {
                    poulateSummaryField();
                }
            };
            var createGridFrame = function () {
                var grid = "<table class='table table-hover table-bordered  table-responsive' id='" + gridId + "'>"+
                "<thead>" +
                    "<tr class='vb-table-header-bgcolor' >" +
                        "<th>Fin Year</th>" +
                        "<th>Qtr No</th>" +
                        "<th>Due Date</th>" +
                        "<th>Property Tax</th>" +
                        "<th>Surcharge</th>" +
                        "<th>osTaxValue</th>" +
                        "<th>osSurchargeValue</th>" +
                        "<th>osCalculatedValue</th>" +
                        "<th>PaidTaxAmt</th>" +
                        "<th>PaidSurchargeAmt</th>" +
                        "<th>PaidCalculatedAmt</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody>" +
                "</tbody>" +
                "</table>";
                container.append(grid);
            };
            var init = function () {
                if (ctrGrid.length <= 0) {
                    createGridFrame();
                    ctrGrid = $("#" + gridId);
                }
                dataBind(data);
            };
            init();
        }
        var ResetMunicipality = function () {
            ctrl_txtMunicipality.val('');
            ctrl_hdnMunicipality.val('');
        }
        var ResetWard = function () {
            ctrl_hdnWard.val('');
            ctrl_txtWard.val('');
        }
        var ResetLocation = function () {
            ctrl_hdnLocation.val('');
            ctrl_txtLocation.val('');
        }
        var ResetAssesse = function () {
            ctrl_hdnAssessee.val('');
            ctrl_txtAssessee.val('');
        };
        var ResetePayementArea = function () {
            processor.wellKnownPaymentObject = {};
            ctnr_gridContainer.empty();
            ctrl_btnPay.attr('disabled', true);
            ctrl_CollectionDate.val('');
            ctrl_PaidAmnt.val('');
        };
        var ReseteSearchFields = function () {
            ResetMunicipality();
            ResetWard();
            ResetLocation();
            ResetAssesse();
            OutStandindSearchDetails = [];
        };
        var ResetWholePage = function () {
            ResetePayementArea();
            ReseteSearchFields();
        }

        this.app_Initiate = function () {
            BindEvent();
            ResetWholePage();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
        $('#txtCollectionDate').datepicker('setDate', (new Date()).getDate());
    };

    (function () {
        INIT();
    }());


});