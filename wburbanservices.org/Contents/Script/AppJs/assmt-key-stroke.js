﻿$(document).ready(function () {
    var ctrlType = function () {
        var getNodName = function (ctrl) {
            return ctrl.prop('nodeName').toLowerCase();
            
        };
        var getType = function (ctrl) {
            return ctrl.attr("type").toLowerCase();
        };
        return {
            isButton: function (ctrl) {
                if (getNodName(ctrl) == "button" || (getNodName(ctrl) == "input" && (getType(ctrl) == "submit" || getType(ctrl) == "button"))) {
                    return true;
                } else {
                    return false;
                }
            },
            isTextBox: function (ctrl) {
                if((getNodName(ctrl) == "input" && getType(ctrl) == "text") )
                {
                    return true;
                } else {
                    return false;
                }
            },
            isTextArea: function (ctrl) {
                if ((getNodName(ctrl) == "textarea"))
                    return true
                else
                    return false;
            },
            isRadio: function (ctrl) {
                if ((getNodName(ctrl) == "input" && getType(ctrl) == "radio")) {
                    return true;
                } else {
                    return false;
                }
            },
            isCheckBox: function (ctrl) {
                if ((getNodName(ctrl) == "input" && getType(ctrl) == "checkbox")) {
                    return true;
                } else {
                    return false;
                }
            },
            isSelect: function (ctrl) {
                if ((getNodName(ctrl) == "select")) {
                    return true;
                } else {
                    return false;
                }
            },
        }
    };
    $(document).on('keyup', function (event) {
        var ctrl_source = $(event.target);//jquery ctrl event fired from
        if (event.keyCode == 13) {//only press enter button
            
            var x = ctrlType().isTextBox(ctrl_source);
            if (ctrlType().isTextBox(ctrl_source)  || ctrlType().isRadio(ctrl_source) || ctrlType().isCheckBox(ctrl_source) || ctrlType().isSelect(ctrl_source)) {
                $.tabNext();
            } else if (ctrlType().isTextArea(ctrl_source)) {
                if (!event.shiftKey) {
                    $.tabNext();
                }
            } else if (ctrlType().isButton(ctrl_source)) {
                $.tabNext();
            }






            
        } else if (event.keyCode == 9) {
            console.log(ctrl_source.attr("class"))
        }
        else if (event.which === 32) {
            if (ctrlType().isCheckBox(ctrl_source)) {
                ctrl_source.prop('checked', !ctrl_source.is(":checked"));
            }
        }
        event.preventDefault();
        return false;
    });
});