﻿$(document).ready(function () {
    var formLoader = new function () {
        this.Load = function (payload, callBack) {
            $.ajax({
                type: 'post',
                url: "/Municipality/Assessment/GetAssesseeUpdationForm",
                data: JSON.stringify(payload),
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (response) {
                    callBack(response);
                },
                failure: function (response) {
                    alert(response);
                },
                function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        };
    };
    var _doms = function () {
        return {
            cntrDownloadedForm: $(".downloaded-form"),
            searchHolding: {
                frm: $("#frm-holding-details"),
                txtWard: $(".txtWard"),
                hdnWard: $(".hddn-Ward"),
                txtLocation: $(".txtLocation"),
                hdnLocation: $(".hddn-Location"),
                txtHoldingNo: $(".txt-holding-no"),
                hddnHoldingNo: $(".hddn-holding-no"),
            },
            partialHoldingDtl: {
                frm: $(".frm-partial-holding-details"),
                ddlHoldingType: $(".ddl-holding-type"),
                ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                listHoldingNames: $(".list-holding-names"),
                txtAddress: $(".txt-address"),
                txtOwnerName: $(".txt-assessee-name")
            },
            valuationDtl: {
                frm: $("#frm-valuation-details"),
                ddlZone: $(".ddl-zone"),
                txtLandAreaKt: $(".txt-land-area-kt"),
                txtLandAreaCh: $(".txt-land-area-ch"),
                txtLandAreaSft: $(".txt-land-area-sft"),
                ddlNatureOfUser: $(".ddl-nature-of-use"),
                txtBldgAreaSft: $(".txt-bldg-area-sft"),
                txtPlinthSft: $(".txt-plinth-sft"),
                ddlConstruction: $(".ddl-construction"),
                txtLandCost: $(".txt-land-cost"),
                txtAge: $(".txt-age"),
                txtReadOnlyAnnualValuation: $(".txt-readonly-annual-valuation"),
                txtReadOnlyQtrPtax: $(".txt-readonly-qtr-ptax"),
                txtReadOnlyQtrSchrg: $(".txt-readonly-qtr-schrg"),
                ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                SrchgTag: $(".chk-SrchgTag"),
                EduCessTag: $(".chk-EduCessTag"),
                txtReadonlyQtrEduCess: $(".txt-readonly-qtr-Edu-cess")
            },
            additionalDtl: {
                frm: $("#frm-sec-holding-details"),
                txtMouajaName: $(".txt-mauja-name"),
                txtKhatianNoRs: $(".txt-khatian-no-rs"),
                txtKhatianNoLr: $(".txt-khatian-no-lr"),
                txtDagNoRs: $(".txt-dag-no-rs"),
                txtDagNoLr: $(".txt-dag-no-lr"),
                ddlHoldingArea: $(".ddl-holding-area"),
                txtDeedNo: $(".txt-deed-no"),
                ddlDeedType: $(".ddl-deed-type"),
                txtDeedDate: $(".txt-deed-date"),
                chkLiftTag: $(".lst-tags .lift"),
                chkDrainage: $(".lst-tags .drainage"),
                chkToilets: $(".lst-tags .toilets"),
                chkElecticity: $(".lst-tags .electricity"),
                chkWater: $(".lst-tags .Water"),
                txtBoroughNo: $(".txt-borough-no")
            },
            otherDtl: {
                frm: $("#frm-other-details"),
                txtPhoneNo: $(".txt-primary-phone-no"),
                txtEmailId: $(".txt-primary-email-id"),
                txtPorCosingFees: $(".txt-pro-cosing-fees"),
                txtOtherFees: $(".txt-other-fees"),
                txtBuildingDtl: $(".txt-building-dtl")
            }
        }
    };
    var formDatas = function () {
        return {
            searchedHoldingData: function () {
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                }
            },
            partialHoldingData: function () {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                }
            },
            holdingDetails: function () {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                    HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                };
            },
            valuationdata: function () {
                return {
                    ZoneId: _doms().valuationDtl.ddlZone.val(),
                    NatureOfUseId: _doms().valuationDtl.ddlNatureOfUser.val(),
                    ConstructionId: _doms().valuationDtl.ddlConstruction.val(),
                    LandArea_KT: _doms().valuationDtl.txtLandAreaKt.val(),
                    LandArea_CH: _doms().valuationDtl.txtLandAreaCh.val(),
                    LandArea_SFT: _doms().valuationDtl.txtLandAreaSft.val(),
                    BldgArea_SFT: _doms().valuationDtl.txtBldgAreaSft.val(),
                    Plinth_SFT: _doms().valuationDtl.txtPlinthSft.val(),
                    LandCost: _doms().valuationDtl.txtLandCost.val(),
                    Age: _doms().valuationDtl.txtAge.val(),
                    HoldingTypeGroupId: _doms().valuationDtl.ddlHoldingTypeGroup.val(),
                    SrchgTag: _doms().valuationDtl.SrchgTag.prop('checked'),
                    EduCessTag: _doms().valuationDtl.EduCessTag.prop('checked')
                }
            },
            additionalHodlingData: function () {
                return {
                    MaujaName: _doms().additionalDtl.txtMouajaName.val(),
                    KhatianNo_RS: _doms().additionalDtl.txtKhatianNoRs.val(),
                    KhatianNo_LR: _doms().additionalDtl.txtKhatianNoLr.val(),
                    DaagNo_RS: _doms().additionalDtl.txtDagNoRs.val(),
                    DaagNo_LR: _doms().additionalDtl.txtDagNoLr.val(),
                    DeedNo: _doms().additionalDtl.txtDeedNo.val(),
                    DeedDate: constants().convertTodate('dd/mm/yyyy', _doms().additionalDtl.txtDeedDate.val()),
                    DeedTypeId: _doms().additionalDtl.ddlDeedType.val(),
                    HoldingAreaCode: _doms().additionalDtl.ddlHoldingArea.val(),
                    Tag_Lift: _doms().additionalDtl.chkLiftTag.prop('checked'),
                    Tag_Drainage: _doms().additionalDtl.chkDrainage.prop('checked'),
                    Tag_Toilets: _doms().additionalDtl.chkToilets.prop('checked'),
                    Tag_Electricity: _doms().additionalDtl.chkElecticity.prop('checked'),
                    Tag_Water: _doms().additionalDtl.chkWater.prop('checked'),
                    BoroughNo: _doms().additionalDtl.txtBoroughNo.val(),
                }
            },
            otherDtl: function () {
                return {
                    PrimaryPhNo: _doms().otherDtl.txtPhoneNo.val(),
                    PrimaryEmail: _doms().otherDtl.txtEmailId.val(),
                    ProCosingFees: (isNaN(_doms().otherDtl.txtPorCosingFees.val()) ? 0 : _doms().otherDtl.txtPorCosingFees.val()),
                    OtherFees: (isNaN(_doms().otherDtl.txtOtherFees.val()) ? 0 : _doms().otherDtl.txtOtherFees.val()),
                    BuildingDtl: _doms().otherDtl.txtBuildingDtl.val()
                }
            }
        }
    };
    var validators = {
        validateHoldingSearch: function () {

            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().searchHolding.hdnWard.prop('name')] = { required: true };
            vldObj.rules[_doms().searchHolding.hdnLocation.prop('name')] = { required: true };
            vldObj.rules[_doms().searchHolding.hddnHoldingNo.prop('name')] = { required: true };

            //set error messages
            vldObj.messages[_doms().searchHolding.hdnWard.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms().searchHolding.hdnLocation.prop('name')] = { required: "Please enter Location." };
            vldObj.messages[_doms().searchHolding.hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().searchHolding.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().searchHolding.frm.valid();
        },
        validatePartialHolding: function () {
            $.validator.addMethod(
            "OwnerList",
            function (value, element, params) {
                // otherwise, use your rule
                if ($(".txt-assessee-name").parent().siblings('ul').find('li').length > 0) {
                    return true;
                }
                return false;
            },
            "Enter Valid Date"
            );
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            vldObj.rules[_doms().partialHoldingDtl.ddlHoldingType.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.txtAddress.prop('name')] = { required: true };
            vldObj.rules[_doms().partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: true };

            vldObj.messages[_doms().partialHoldingDtl.ddlHoldingType.prop('name')] = { required: "Please select holding type" };
            vldObj.messages[_doms().partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: "Please holding type group" };
            vldObj.messages[_doms().partialHoldingDtl.txtAddress.prop('name')] = { required: "Please enter address" };
            vldObj.messages[_doms().partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: "Please add owner list" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().partialHoldingDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().partialHoldingDtl.frm.valid();
        },
        validateValuation: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().valuationDtl.ddlZone.prop('name')] = { required: true };
            vldObj.rules[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: true, number: true, range: [0, 15] };
            vldObj.rules[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: true, number: true, range: [0, 44] };
            vldObj.rules[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: true };
            vldObj.rules[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: true };
            vldObj.rules[_doms().valuationDtl.txtLandCost.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms().valuationDtl.txtAge.prop('name')] = { required: true, number: true };


            //set error messages
            vldObj.messages[_doms().valuationDtl.ddlZone.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: "Please enter land area kt.", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: "Please enter land area ch.", number: "Data should be numeric", range: "Value sould be between 0 to 15" };
            vldObj.messages[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: "Please enter land area sft.", number: "Data should be numeric", range: "Value sould be between 0 to 44" };
            vldObj.messages[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: "Please select nature of use" };
            vldObj.messages[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: "Please enter bldg. area sft", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: "Please enter plinth sft", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: "Please select construction" };
            vldObj.messages[_doms().valuationDtl.txtLandCost.prop('name')] = { required: "Please enter land cost", number: "Data should be numeric" };
            vldObj.messages[_doms().valuationDtl.txtAge.prop('name')] = { required: "Please enter age", number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().valuationDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().valuationDtl.frm.valid();
        },
        validateAdditional: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().additionalDtl.txtMouajaName.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtKhatianNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtKhatianNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDagNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDagNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.ddlHoldingArea.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDeedNo.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.ddlDeedType.prop('name')] = { required: true };
            vldObj.rules[_doms().additionalDtl.txtDeedDate.prop('name')] = { required: true };


            //set error messages
            vldObj.messages[_doms().additionalDtl.txtMouajaName.prop('name')] = { required: "Please enter Mouja Name." };
            vldObj.messages[_doms().additionalDtl.txtKhatianNoRs.prop('name')] = { required: "Please enter Khatian No RS" };
            vldObj.messages[_doms().additionalDtl.txtKhatianNoLr.prop('name')] = { required: "Please enter Khatian No LR" };
            vldObj.messages[_doms().additionalDtl.txtDagNoRs.prop('name')] = { required: "Please enter Dag No RS" };
            vldObj.messages[_doms().additionalDtl.txtDagNoLr.prop('name')] = { required: "Please select Dag No LR" };
            vldObj.messages[_doms().additionalDtl.ddlHoldingArea.prop('name')] = { required: "Please enter Holding Area" };
            vldObj.messages[_doms().additionalDtl.txtDeedNo.prop('name')] = { required: "Please enter Deed no" };
            vldObj.messages[_doms().additionalDtl.ddlDeedType.prop('name')] = { required: "Please select Deed Type" };
            vldObj.messages[_doms().additionalDtl.txtDeedDate.prop('name')] = { required: "Please enter Deed Date" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().additionalDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().additionalDtl.frm.valid();
        },
        validateOtherDtl: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().otherDtl.txtPorCosingFees.prop('name')] = { number: true };
            vldObj.rules[_doms().otherDtl.txtOtherFees.prop('name')] = { number: true };

            //set error messages
            vldObj.messages[_doms().otherDtl.txtPorCosingFees.prop('name')] = { number: "Data should be numeric" };
            vldObj.messages[_doms().otherDtl.txtOtherFees.prop('name')] = { number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().otherDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().otherDtl.frm.valid();
        }
    };
    var bindEvents = function () {
        $(document).on('click', ".btn-load-review-form", function (e) {
            e.preventDefault();

            _doms().cntrDownloadedForm.empty();
            if (!validators.validateHoldingSearch()) {
                return;
            }
            var frmData = formDatas().searchedHoldingData();
            var payload = { LocationId: frmData.LocationId, WardId: frmData.WardId, HoldingNo: frmData.HoldingNo};
            formLoader.Load(payload, function (r) {
                _doms().cntrDownloadedForm.append(r);
            });
        });
        $(document).on('focus', ".txtWard", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { Ward: request.term };
                    _doms().searchHolding.hdnWard.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetWards",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WardID,
                                            label: item.WardName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms().searchHolding.hdnWard.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txtLocation", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { WardId: _doms().searchHolding.hdnWard.val(), Location: request.term };
                    _doms().searchHolding.hdnLocation.val('');
                    if (!request.term || !_doms().searchHolding.hdnWard.val()) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetLocations",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.LocationID,
                                            label: item.LocationName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms().searchHolding.txtLocation.val(i.item.label);
                    _doms().searchHolding.hdnLocation.val(i.item.value);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txt-holding-no", function () {
            $(this).off('keyup');
            $(this).on('keyup', function () {
                $(".hddn-holding-no").val($(this).val());
            });
        });
    };
    (function () {
        bindEvents();
    }());
});