﻿$(document).ready(function () {
    //$('#taableid').DataTable();
    LoadCalender();
    GetMunicipality();
    LoadData();
    $('#btnSave').click(SaveUpdate); 
    $('#Refresh').click(ClearAllFields);
});
function LoadCalender()
{
    $('#txtFromDate,#txtTodate').datepicker({      
        selectOtherMonths: true,
        //showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
    });
}

function GetMunicipality() {
    var DataAutoComplete = [];

    $.ajax({
        type: "POST",
        url: "/Administration/SchedulrIII/GetMunicipality",
        data:"",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.MMunicipalityList;

            if (t.length > 0) {
                $.each(t, function (index, item) {
                    DataAutoComplete.push({
                        label: item.MMunicipalityName,
                        ShiftId: item.MunicipalityID
                    });
                });

            }
        }
    });

    $("#txtMunicipality").autocomplete({

        source: function (request, response) {
            var results = $.ui.autocomplete.filter(DataAutoComplete, request.term);
            response(results.slice(0, 15));
        }, minLength: 0, 
        select: function (e, i) {
            $("#MunicipalityId").val(i.item.ShiftId);
        }
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function LoadData() {
    $.ajax({
        type: "POST",
        url: "/Administration/SchedulrIII/Get_MstScheduleIII",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {

                //$("#taableid tbody").empty();
                //var i = 1;
                var arr = [];
                $.each(D.ScheduleIIIList, function (index, value) {
                    arr.push([index+1, value.AppliWindowId, value.MunicipalityID, value.MMunicipalityName, value.ScheIIIStartDt, value.ScheIIIDt]);
                });
                $('#taableid').DataTable({
                    data: arr,
                    columnDefs: [
                        //{ "visible": false, "targets": 1 },
                        //{ "visible": false, "targets": 2 },
                        { "className": "hide_column", "targets": 1 },
                        { "className": "hide_column", "targets": 2 },
                        {
                            "targets": 6,
                            "data": "download_link",
                            "render": function (data, type, row, meta) {
                                return '<a herf="" onclick="EditScheduleIII(this);">Edit</a>';
                            }
                        }
                    ]});
            }
            else {
                alert('No data found');
            }
        }
    });
    
}

function SaveUpdate() {
    if (Validation()) {
        var S = "{AppliWindowId:'" + $.trim($('#hdnScheduleIIIId').val()) +
            "', MunicipalityID:'" + $.trim($('#MunicipalityId').val()) +
            "', FromDate:'" + $.trim($('#txtFromDate').val()) +
            "', ToDate:'" + $.trim($('#txtTodate').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/SchedulrIII/SaveUpdate_SchedulrIIIr",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status == 200) {
                    if (parseInt(D.ErrorCode) ==1)
                    {
                        alert(D.Message);
                        ClearAllFields();
                        GetMunicipality();
                        LoadData();
                    }   
                    else {
                        alert(D.Message);
                    }
                }
                else {
                    alert(D.Message);
                }
            }
        });
    }
}

function EditScheduleIII(t) {

    $('#btnSave').html('Update');
    $('#hdnScheduleIIIId').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#MunicipalityId').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#txtMunicipality').val($(t).closest('tr').find('td:eq(3)').text().trim());
    $('#txtFromDate').val($(t).closest('tr').find('td:eq(4)').text().trim());
    $('#txtTodate').val($(t).closest('tr').find('td:eq(5)').text().trim());
   
}

function ClearAllFields() {

    $("#txtMunicipality").val('');
    $("#txtFromDate").val('');
    $("#txtTodate").val('');
    $('#btnSave').html('Save');
}

function Validation() {
    var txtMunicipality = $('#txtMunicipality').val();
    var txtFromDate = $('#txtFromDate').val();
    var txtTodate = $('#txtTodate').val();
    if (txtMunicipality == "") {
        alert("Select Municipality!");
        return false;
    }
    if (txtFromDate == "") {
        alert("Select FromDate!");
        return false;
    }
    if (txtTodate == "") {
        alert("Select ToDate!");
        return false;
    }
    return true;
}
