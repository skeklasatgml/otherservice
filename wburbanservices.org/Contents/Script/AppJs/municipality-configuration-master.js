﻿$(document).ready(function () {
    var SERVER = function () {
        this.SaveInterestRebateTextDemandBill = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/municipality/MunicipalityConfigMaster/SaveDemandBillConfig",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
        };
        this.SaveBasicConfig = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/municipality/MunicipalityConfigMaster/SaveBasicConfig",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
        };
        this.SavePaymentConfig = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/municipality/MunicipalityConfigMaster/SavePaymentConfig",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
        };
    };
    var APP = function (srv) {
        var getControlInterface = function (selector, container) {
            if (container) {
                return {
                    _selector: selector, _control: container._control.find(selector)
                };
            } else {
                return {
                    _selector: selector, _control: $(selector)
                };
            }
        };
        var demandBillComponent = new function () {
            var _scopeContainer = getControlInterface('#tab-pane-demand-bill');
            var _domControls = {
                btnSaveChanges: getControlInterface('.btn-save-changes', _scopeContainer),
                txtText: getControlInterface('.txtText', _scopeContainer)
            };
            var bindEvent = function () {
                _domControls.btnSaveChanges._control.on('click', function (e) {
                    e.preventDefault();
                    alertify.confirm("Please Confirm!", "Are you sure to save?", function () {
                        srv.SaveInterestRebateTextDemandBill({
                            DemandBillConfig: { DemandBillRebateIntrstText: _domControls.txtText._control.val().trim() }
                        }, function (r) {
                            if (r.Status === 200) {
                                alertify.success(r.Message);
                            } else {
                                alertify.error(r.Message);
                            }
                        });
                    }, function () { });

                });
            };
            (function () {
                bindEvent();
            }());
        };
        var basicInfoComponent = new function () {
            var _scopeContainer = getControlInterface('#tab-pane-basic-configuration');
            var _domControls = {
                btnSaveChanges: getControlInterface('.btn-save-changes', _scopeContainer),
                txtMunicipalityName: getControlInterface('.txtMunicipalityName', _scopeContainer),
                txtAddress: getControlInterface('.txtAddress', _scopeContainer),
                txtwebsite: getControlInterface('.txtwebsite', _scopeContainer),
                txtEmail: getControlInterface('.txtEmail', _scopeContainer),
                txtMobile: getControlInterface('.txtMobile', _scopeContainer),
                txtFax: getControlInterface('.txtFax', _scopeContainer),
                txtTelephone: getControlInterface('.txtTelephone', _scopeContainer),
                form: getControlInterface('form', _scopeContainer)
            };
            var initValidator = function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_domControls.txtMunicipalityName._control.prop('name')] = { required: true };
                vldObj.rules[_domControls.txtAddress._control.prop('name')] = { required: true };
                vldObj.rules[_domControls.txtwebsite._control.prop('name')] = { url: true };
                vldObj.rules[_domControls.txtMobile._control.prop('name')] = {validMobile: true };


                vldObj.messages[_domControls.txtMunicipalityName._control.prop('name')] = { required: "Please Enter Municipality Name." };
                vldObj.messages[_domControls.txtAddress._control.prop('name')] = { required: "Please Enter Address." };
                vldObj.messages[_domControls.txtwebsite._control.prop('name')] = { required: "Please Enter Valid URL." };

                //inject more validator
                jQuery.validator.addMethod("validMobile", function (value, element) {
                    if (element.required === false) {
                        if (value.trim() !== '') {
                            return RegxValidators.MobileNo10CharValidate(value);
                        }
                        return true;
                    } else {
                        return true;
                    }
                }, "Enter Valid Mobile No");

                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
                _domControls.form._control.validate(vldObj);
            };
            var pollyFills = function () {
                _domControls.form._control.getFormData = function () {
                    return {
                        MunicipalityName: _domControls.txtMunicipalityName._control.val(),
                        Address: _domControls.txtAddress._control.val(),
                        Email: _domControls.txtEmail._control.val(),
                        Mobile: _domControls.txtMobile._control.val(),
                        TelePhone: _domControls.txtTelephone._control.val(),
                        Fax: _domControls.txtFax._control.val(),
                        WebSite: _domControls.txtwebsite._control.val(),
                    }
                };
            };

            var bindEvents = function () {
                _domControls.btnSaveChanges._control.on('click', function (e) {
                    e.preventDefault();
                    if (_domControls.form._control.valid()) {
                        alertify.confirm("Please Confirm!", "Are you sure to save?", function () {
                            var payLoad = _domControls.form._control.getFormData();
                            srv.SaveBasicConfig({ BasicInfoConfig: payLoad }, function (r) {
                                if (r.Status === 200) {
                                    alertify.success(r.Message);
                                } else {
                                    alertify.error(r.Messages);
                                }
                            });
                        }, function () { });
                    }
                });
            };

            (function () {
                initValidator();
                pollyFills();
                bindEvents();
            }());
        };
    };

    (function () {
        new APP(new SERVER());
    }())
});