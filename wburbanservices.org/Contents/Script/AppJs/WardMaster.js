﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetAllWard = function (callback) {

            $.ajax({
                type: "POST",
                url: "/WardMaster/Get_AllWard",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.Insert = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/WardMaster/InsertWard",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetMunicipality = function (callback) {
            $.ajax({
                type: "POST",
                url: "/MunicipalityMaster/GetAllMunicipalities",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        
        this.Delete = function (data, callback) {

        };
    };
    var APP_OBJECT = function (serverObject) {
        var ddlMunicipal_data = [];
        var ddlMunicipal = $('#ddlMunicipal');

        var Bind_GridOnPageLoad = function () {

            var ctr_btnRowDel = $(".btnRowDel");
            var btnRowEdit = $(".btnRowEdit");
            $("#tabDtl").find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td class='Ward' style='text-align:center'>" + rowData.WardName + "</td>";
                rowstr += "<td class='WardID' style='display:none;'>" + rowData.WardID + "</td>";
                rowstr += "<td class='Municipality'>" + rowData.MunicipalityName + "</td>";
                rowstr += "<td class='MunicipalityID' style='display:none;'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td style='text-align:center'><a class='btnRowEdit' href='javascript:void(0);'><img src='/Contents/image/edit.png'  /></a></td>";
                rowstr += "<td style='text-align:center'><a class='btnRowDel'><img src='/Contents/image/delete.png'  /></a></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onDeleteRow = function () {
                var ctrl = $(this);
                //find row

            };
            var onEditRow = function () {
                var ctrl = $(this);
                var currentrow = ctrl.parent().parent();

                var Ward = currentrow.find(".Ward").text();
                var WardID = currentrow.find(".WardID").text();
                var Municipality = currentrow.find(".Municipality").text();
                var MunicipalityID = currentrow.find(".MunicipalityID").text();

                $("#btnSave").attr('WardID', WardID);
                $("#btnSave").attr('value', 'Update');

                var model = { WardID:WardID, Ward: Ward, MunicipalityID: MunicipalityID };
                feedToForm(model);
            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowDel', onDeleteRow);
                $(document).on('click', '.btnRowDel', onDeleteRow);

                $(document).off('click', '.btnRowEdit', onEditRow);
                $(document).on('click', '.btnRowEdit', onEditRow);
            };
            var appendToBody = function (rowString) {
                $("#tabDtl").find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.GetAllWard(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        }
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $('#btnCancel').on('click', Reset_Field);
        };
        var Reset_Field = function () {
            //window.location.reload(true);
            $("#hdnWardID").val('');
            $("#txtWard").val('');
            $("#ddlMunicipal").val(0);
            $("#btnSave").attr('value', 'Save');
        }
        var getDataFromView = function () {
            var txtWard = $('#txtWard').val();
            var ddlMunicipal = $('#ddlMunicipal').val();
            var hdnWardID = $('#hdnWardID').val();
            var data = { WardID:hdnWardID, WardName: txtWard, MunicipalityID: ddlMunicipal };
            return data;
        };
        var feedToForm = function (data) {
            $("#txtWard").val(data.Ward);//
            $("#hdnWardID").val(data.WardID);
            $("#ddlMunicipal").val(data.MunicipalityID);
        };
        var PopulateMunicipality = function () {
            ddlMunicipal.append("<option value='0'>--(Select Municipality)--</option>");
            $.each(ddlMunicipal_data, function (index, value) {
                var label = value.MunicipalityName;
                var val = value.MunicipalityID;
                ddlMunicipal.append("<option value=" + val + ">" + label + "</option>");
            });
        }

        var Validate = function (data) {
            if (data.WardName == '') {
                alert("Fill Ward Name");
                return false;
            }
            if (data.MunicipalityID == 0) {
                alert("Select Municipality");
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.Insert(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert('Record Save Successfully !!');
                        window.location.reload(true);
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            Bind_GridOnPageLoad();
            serverObject.GetMunicipality(function (response) {
                ddlMunicipal_data = response.Data;
                PopulateMunicipality();
            });
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});