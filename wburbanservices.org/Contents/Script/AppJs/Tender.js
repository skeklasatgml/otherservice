﻿$(document).ready(function () {
    app.ddlMunicipality();
    app.ddlBudgetCode();
    app.PopulateProjectNo();
    app.PopulateSubProjectNo();
    app.PopulateVendors();
    app.getAllTenderDetails();
    $(".disablCntrl").prop("disabled", true);
    $(document).on("click", ".btn-sbmt", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData()
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirm Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData()
                };
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/SaveTenderData",
                    data: JSON.stringify(payload.configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    $("#workStrtDt").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
       // minDate: 0,
        onSelect: function (date) {
            $("#workCmpltnDt").datepicker('option', 'minDate', date);
        }
    });
    $("#workCmpltnDt").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
    });
    $(document).on("click", ".editRow", function () {
        var tempJsn = JSON.parse($(this).closest('tr').find('.hdnRow').text());
        console.log(tempJsn);
        $('.tenderPrjctNo').val(tempJsn.ProjectNo);
        $('.SubPrjctNo').val(tempJsn.SubProjName);
        $('.tenderNo').val(tempJsn.TenderNo);
        $('.tenderCallNo').val(tempJsn.TenderCallNo);
        $('.tndrStatus').val(2).change();
        $('.tOpenDate').val(CONVERTER.Date.convertCsToJsDate(tempJsn.TndrOpenDt).format('dd/mm/yyyy'));
        $('.tCloseDate').val(CONVERTER.Date.convertCsToJsDate(tempJsn.TndrCloseDt).format('dd/mm/yyyy'));

        $('.awardedTo').val(tempJsn.AwardedTo);
        $('.wOrderNo').val(tempJsn.WrkOrdrNo); 
        $('.workOdrDt').val(CONVERTER.Date.convertCsToJsDate(tempJsn.WrkOrdrDt).format('dd/mm/yyyy'));
        $('.workStrtDt').val(CONVERTER.Date.convertCsToJsDate(tempJsn.WrkStrtDt).format('dd/mm/yyyy'));
        $('.workCmpltnDt').val(CONVERTER.Date.convertCsToJsDate(tempJsn.WrkCmpltnDt).format('dd/mm/yyyy'));
        $('.engName').val(tempJsn.EngName);
        $('.engDesig').val(tempJsn.EngDesig);
        $('.engEmail').val(tempJsn.EngEmail);
        $('.engPhone').val(tempJsn.EngPhone);
    });
    $(document).on("change", ".tndrStatus", function () {
        if ($(this).val() == 1)
        {
            $(".disablCntrl").prop("disabled", true);
        }
        if ($(this).val() == 2) {
            $(".disablCntrl").prop("disabled", false);
        }
    });
});
var app = {
    ddlMunicipality: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllMunicipalites',
            type: 'POST',
            data: JSON.stringify({ considerMnId: true }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
                $('#ddlMunicipality').val($("#ddlMunicipality option:eq(1)").val());
                $('#ddlMunicipality').prop('disabled', true);
            },
            error: function () { }
        });
    },
    ddlBudgetCode: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllBudgetCode',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.PopulateBudget(response.Data);
                $('#ddlBudgetCode').val("")
            },
            error: function () { }
        });
    },
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#configureTender"),
                ddlMuni: $(".ddlMunicipality"),
                txtProjectNo: $(".tenderPrjctNo"),
                txtTenderNo: $(".tenderNo"),
                txtTenderCallNo: $(".tenderCallNo"),
                txtTndrOpenDt: $(".tOpenDate"),
                txtTndrCloseDt: $(".tCloseDate"),
                txtAwardedTo: $(".awardedTo"),
                txtWrkOrdrNo: $(".wOrderNo"),
                txtWrkOrdrDt: $(".workOdrDt"),
                txtWrkStrtDt: $(".workStrtDt"),
                txtWrkCmpltnDt: $(".workCmpltnDt"),
                txtEngName: $(".engName"),
                txtEngEmail: $(".engEmail"), 
                txtEngDesig: $(".engDesig"),
                txtEngPhone: $(".engPhone"),
                txtSubProjId: $(".SubPrjctNo"),
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];
                var chk = $(".tndrStatus").val();
                //set rules
                vldObj.rules[_doms().ddlMuni.prop('name')] = { required: true };
                vldObj.rules[_doms().txtProjectNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtTenderNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtTenderCallNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtTndrOpenDt.prop('name')] = { required: true };
                vldObj.rules[_doms().txtTndrCloseDt.prop('name')] = { required: true };
                vldObj.rules[_doms().txtAwardedTo.prop('name')] = { required: !(chk==1) };
                vldObj.rules[_doms().txtWrkOrdrNo.prop('name')] = { required: !(chk == 1) };
                vldObj.rules[_doms().txtWrkOrdrDt.prop('name')] = { required: !(chk == 1) };
                vldObj.rules[_doms().txtWrkStrtDt.prop('name')] = { required: !(chk == 1) };
                vldObj.rules[_doms().txtWrkCmpltnDt.prop('name')] = { required: !(chk == 1) };
                vldObj.rules[_doms().txtEngName.prop('name')] = { required: !(chk == 1) };
                vldObj.rules[_doms().txtEngEmail.prop('name')] = { email: true };

                //set error messages
                vldObj.messages[_doms().ddlMuni.prop('name')] = { required: "Select municipality" };
                vldObj.messages[_doms().txtProjectNo.prop('name')] = { required: "Please select Scheme ID" };
                vldObj.messages[_doms().txtTenderNo.prop('name')] = { required: "Please enter tender no" };
                vldObj.messages[_doms().txtTenderCallNo.prop('name')] = { required: "Please enter tender call no"};
                vldObj.messages[_doms().txtTndrOpenDt.prop('name')] = { required: "Please select a date"};
                vldObj.messages[_doms().txtTndrCloseDt.prop('name')] = { required: "Please select a date" };
                vldObj.messages[_doms().txtAwardedTo.prop('name')] = { required: "Please enter vendor name" };
                vldObj.messages[_doms().txtWrkOrdrNo.prop('name')] = { required: "Please enter work order No" };
                vldObj.messages[_doms().txtWrkOrdrDt.prop('name')] = { required: "Please select date" };
                vldObj.messages[_doms().txtWrkStrtDt.prop('name')] = { required: "Please select date" };
                vldObj.messages[_doms().txtWrkCmpltnDt.prop('name')] = { required: "Please select date" };
                vldObj.messages[_doms().txtEngName.prop('name')] = { required: "Please enter engineer name" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                function toDate(dateStr) {
                    if (dateStr == null)
                        return null;
                    var fromDate = dateStr.split("/");
                    var strJobDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
                    return strJobDate;
                }
                return {
                    MnId: _doms().ddlMuni.val(),
                    ProjectNo: _doms().txtProjectNo.val(),
                    TenderNo: _doms().txtTenderNo.val(),
                    TenderCallNo: _doms().txtTenderCallNo.val(),
                    TndrOpenDt: toDate(_doms().txtTndrOpenDt.val()),
                    TndrCloseDt: toDate(_doms().txtTndrCloseDt.val()),
                    AwardedTo: _doms().txtAwardedTo.val(),
                    WrkOrdrNo: _doms().txtWrkOrdrNo.val(),
                    WrkOrdrDt: toDate(_doms().txtWrkOrdrDt.val()),
                    WrkStrtDt: toDate(_doms().txtWrkStrtDt.val()),
                    WrkCmpltnDt: toDate(_doms().txtWrkCmpltnDt.val()),
                    EngName: _doms().txtEngName.val(),
                    EngEmail: _doms().txtEngEmail.val(),
                    EngDesig: _doms().txtEngDesig.val(),
                    EngPhone: _doms().txtEngPhone.val(),
                    SubProjName: _doms().txtSubProjId.val(),
                };
            }
        }
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('.ddlMunicipalityCmmn');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
        var option = ctlr_ddlmunicipality.find("option[value='" + ctlr_ddlmunicipality.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlmunicipality.find('option:eq(0)').prop('selected', true);
        }
    },
    PopulateBudget: function (data) {
        var ctlr_ddlBcode = $('.ddlBudgetCode');
        ctlr_ddlBcode.empty();
        ctlr_ddlBcode.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            ctlr_ddlBcode.append("<option value='" + value.BCodeId + "' >" + value.BCode + "</option>")
        });
        var option = ctlr_ddlBcode.find("option[value='" + ctlr_ddlBcode.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlBcode.find('option:eq(0)').prop('selected', true);
        }
    },
    PopulateProjectNo: function () {
        $(document).on('focus', ".tenderPrjctNo", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { ProjNo: request.term, considerMnId: true };
                    //$(".tenderPrjctNo").val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/Modules/DprExtension/GetAllProjectNos',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.ProjectNoVal,
                                            label: item.ProjectNoTxt
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    $(".tenderPrjctNo").val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
    },
    PopulateSubProjectNo: function () {
        $(document).on('focus', ".SubPrjctNo", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { SubProjNo: request.term, ProjNo: $(".tenderPrjctNo").val(), considerMnId: true };
                    //$(".SubPrjctNo").val('');
                    if (!request.term) {
                        return;
                    }
                    if ($(".tenderPrjctNo").val() == "")
                    {
                        alertify.alert("Please Select Scheme Id");
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/Modules/DprExtension/GetAllSubProjectNos',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.SubProjNoTxt,
                                            label: item.SubProjNoTxt
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    $(".SubPrjctNo").val(i.item.label);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
    },
    PopulateVendors: function () {
        $(document).on('focus', ".awardedTo", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { VendorName: request.term };
                    //$(".awardedTo").val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/Modules/DprExtension/GetAllVendors',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.VName,
                                            label: item.VName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    $(".awardedTo").val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
    },
    getAllTenderDetails: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllTenders',
            type: 'POST',
            //data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');

                        var td = $('<td hidden class="hdnRow"/>');
                        tr.append(td.append(JSON.stringify(tempJson)));
                        //floor control
                        var td = $('<td />');
                        tr.append(td.append(tempJson.ProjectNo));

                        var td = $('<td />');
                        tr.append(td.append(tempJson.SubProjName)); 

                        //add usage control
                        td = $('<td  />');
                        tr.append(td.append(tempJson.TenderNo));

                        //add usage control
                        td = $('<td  />');
                        tr.append(td.append(tempJson.TenderCallNo));

                        //add Year control
                        td = $('<td  />');
                        tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.TndrOpenDt).format('dd/mm/yyyy')));

                        //add covered area control
                        td = $('<td />');
                        tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.TndrCloseDt).format('dd/mm/yyyy')));
                        
                        //add Occupent Name control
                        td = $('<td  />');
                        tr.append(td.append(tempJson.AwardedTo));

                        td = $('<td  />');
                        tr.append(td.append(tempJson.WrkOrdrNo));

                        td = $('<td />');
                        tr.append(td.append(tempJson.WrkOrdrDt==null?"":CONVERTER.Date.convertCsToJsDate(tempJson.WrkOrdrDt).format('dd/mm/yyyy')));

                        td = $('<td  />');
                        tr.append(td.append('<input type="button" value="Edit" class="editRow">'));

                        $('#myTable').find('>tbody').append(tr);
                    }
                }
            },
            error: function () { }
        });
    },
}