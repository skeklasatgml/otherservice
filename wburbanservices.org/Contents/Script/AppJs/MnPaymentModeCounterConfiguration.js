﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetCounters = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/GetAllCounterMunicipalityWise",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.SavePaymentMode_Counter_Assocation = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/SavePaymentMode_Counter_Assocation",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetPaymentMode = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/GetOffLinePaymentModes",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetPaymentModesCounerWise = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/GetPaymentModeCounterWise",
                data: "{CounterId: '" + data + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.UpdatePaymentModeCounterAssoc = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/UpdatePaymentMode_Counter_Assocation",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.DeletePaymentModeCounterWise = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/DeletePaymentMode_Counter_Assocation",
                data: "{ RowId:'" + data + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        Window.test = function (date) {
            
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPaymentModeConfiguration/test",
                data: JSON.stringify({ dt: date}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    //location.reload(true);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var CounterDataSource = [];
        var DomControls = {
            ctrl_FromToDate: $("#txtFormDate,#txtToDate"),
            ctrl_BtnInsert: $("#btnInsert"),
            ctrl_DrpCounter: $("#drpCounter"),
            ctrl_FormDate: $("#txtFormDate"),
            ctrl_ToDate: $("#txtToDate"),
            ctrl_tblPaymentModes: $("#tblPaymentModes"),
            ctrl_tblAddRow: $("#tblAddRow"),
            ctrl_btnAdd: $("#btnAdd"),
            ctrl_tblPaymentModeCounterWise: $("#tblPaymentModeCounterWise")  
        };
        var ValidateBeforeAddToTheList = function () {
            var Status = true;
            var Count = 0;
            $.each($("#tblPaymentModes tbody tr"), function (index, value) {
                var checked = $(value).find(".chkSelect:checked");

                if (checked.length > 0) {
                    Count = 1
                }
            });
            var s = $("#drpCounter").val();
            if ($("#drpCounter").val() == "0") {
                alert("Select Counter From List!");
                $("#drpCounter").focus();
                Status= false;
            }
            else if ($("#txtFormDate").val() == "") {
                alert("Select From date");
                $("#txtFormDate").focus();
                Status= false;
            }
            else if ($("#txtToDate").val() == "") {
                alert("Select To date!");
                $("#txtToDate").focus();
                Status= false;
            }
            else if (Count == 0) {
                alert("Select atleast one PaymentMode!");
                Status = false;

            }
            return Status;
        };
        var getDataFromView = function () {
            var CounterId = $("#drpCounter").val();
            var Rows = []; 
            var tblrows = $("#tblAddRow tbody tr");
            var RowId = $("#btnInsert").attr("RowId");

            var i = 0;
            var ConvertToDate = function (strDate) {
                //Supported Format yyyy-mm-dd
                if (CONVERTER.Date.isDateString(strDate) == true) {
                    var dtParts = (strDate).split("-");
                    return CONVERTER.Date.convertToDate(dtParts[0], dtParts[1], dtParts[2]);
                }
                else {
                        throw "Invalid Date Formate!"
                }
            };
            try{
                $.each(tblrows, function (index, value) {
                    var obj = {};
                    var CurrentRow = $(value);
                    var PaymentModeCode = CurrentRow.find('.PaymentModeCode').text();
                    var DateFrom = CurrentRow.find('.FormDate').text();
                    var DateTo = CurrentRow.find('.Todate').text();

                
                

                    obj.RowId = RowId;
                    obj.counterId = CounterId;
                    obj.MunicipalityId = null;
                    obj.FromDate =ConvertToDate( DateFrom);
                    obj.Todate =ConvertToDate( DateTo);
                    obj.paymentModeCode = PaymentModeCode;
                    obj.MunicipalityPaymentCounter = null;
                    obj.PaymentMode = null;
                    obj.Municipality = null;

                    Rows[i] = obj;
                    i++;
                });
            }
            catch (ex) {
                alert("Error: "+ex);
            }
            console.log(Rows);
            return Rows;
        };
        var Validate = function (data) {
            var len = $("#tblAddRow tbody tr").length;
            if ($("#tblAddRow tbody tr").length <= 0) {
                alert("No row is added for Save/Update");
                return false;
            }
            else { return true;}
        };
        var InsertPaymentMOdeCounterAssociation = function () {
            var formData = getDataFromView();
            if (Validate()) {
                serverObject.SavePaymentMode_Counter_Assocation(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(' Details successfully saved!!')
                    }
                    else if (status == 422) {
                        alert(' Date Range already exists for this Payment type and Counter!!')

                    }
                    else {
                        alert(response.Message);
                    }
                });
            }
        };
        var UpdatePaymentMOdeCounterAssociation = function () {
            var formData = getDataFromView();
            if (Validate()) {
                serverObject.UpdatePaymentModeCounterAssoc(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert(' Details Updated successfully!!')
                    }
                    else if (status == 422) {
                        alert(' Date Range already exists for this Payment type and Counter!!')

                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        var ResetWIndow = function () {
            location.reload(true);
        };
        var Save_Update_PaymentModes_CounterAssociation = function () {
            var RowId = $("#btnInsert").attr("RowId");
            if (RowId == "0") {

                InsertPaymentMOdeCounterAssociation();
            }
            else {
                
                UpdatePaymentMOdeCounterAssociation();

            }
        };

        var BindEvents = function () {

            $("#btnInsert").on('click', Save_Update_PaymentModes_CounterAssociation);
            $("#btnAdd").on('click', AddRowPaymentModeDateBetweenWise);
            $("#drpCounter").on('change', PopulatePaymentModeCounterAssociationTable);
            $("#btnCancel").on('click', ResetWIndow);

            DomControls.ctrl_FormDate.datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                minDate: 'today',
                onSelect: function (date, b) {
                    var dts = date.split('/');
                    DomControls.ctrl_ToDate.datepicker('option', 'minDate', new Date(date));

                }
            });
            DomControls.ctrl_ToDate.datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                minDate: 'today',
            });
        };
        var PopulateCounterControlToDataSource = function () {
            DomControls.ctrl_DrpCounter.empty();
            DomControls.ctrl_DrpCounter.append("<option value='0'>--select--</option>");
            $.each(CounterDataSource, function (index, value) {
                var label = value.CounterCode;
                var id = value.CounterID;
                DomControls.ctrl_DrpCounter.append("<option value='" + id + "'>" + label + "</option>");
            });
        };
        var PopulatePaymentMode = function () {
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td class='PaymentModeCode' style='display:none'>" + rowData.PaymentModeCode + "</td>";
                rowstr += "<td class='PaymentModeDescription' style='align:center'>" + rowData.PaymentModeDescription + "</td>";
                rowstr += "<td  style='align:center'><input type='checkbox' class='chkSelect' name='select' value='select' data-payment-code='" + rowData.PaymentModeCode + "'/></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var appendToBody = function (rowString) {
                DomControls.ctrl_tblPaymentModes.find("tbody").append(rowString);
            };
            (function () {
                serverObject.GetPaymentMode(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        };
        var AddRowPaymentModeDateBetweenWise = function () {
            var status = ValidateBeforeAddToTheList();
            if (status == false) {
                return false;
            }
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowDelete', onDeleteRow);
                $(document).on('click', '.btnRowDelete', onDeleteRow);

            };
            $.each($("#tblPaymentModes tbody tr"), function (index, value) {
                var checked = $(value).find(".chkSelect:checked");
                if (checked.length > 0) {
                    var currentrow = $(this);
                    var PaymentModeCode = currentrow.find(".PaymentModeCode").text();
                    var PaymentModeDescription = currentrow.find(".PaymentModeDescription").text();
                    var FormDate = $("#txtFormDate").val();
                    var Todate = $("#txtToDate").val();

                    if($("#tblAddRow tbody tr").length > 0)
                    {
                        var Flags = 0;
                        $.each($("#tblAddRow tbody tr"), function (index, value) {
                            var currentrowtblAddRow = $(this);
                            var PaymentModeCodetblAddRow = currentrowtblAddRow.find(".PaymentModeCode").text();
                            if (PaymentModeCodetblAddRow == PaymentModeCode) {
                                Flags=1;
                            }
                        });
                        if (Flags == 0) {
                            var rowstr = "<tr>";
                            rowstr += "<td class='PaymentModeCode' style='display:none'>" + PaymentModeCode + "</td>";
                            rowstr += "<td class='PaymentModeDescription' style='align:center'>" + PaymentModeDescription + "</td>";
                            rowstr += "<td class='FormDate' style='align:center'>" + FormDate + "</td>";
                            rowstr += "<td class='Todate' style='align:center'>" + Todate + "</td>";
                            rowstr += "<td class='chkSelect' style='align:center'><input type='button' name='Delete' value='Delete' class='btnRowDelete'/></td>";
                            rowstr += "</tr>";
                            DomControls.ctrl_tblAddRow.find("tbody").append(rowstr);
                            
                        }
                           
                      
                    }
                    else
                    {
                        var rowstr = "<tr>";
                        rowstr += "<td class='PaymentModeCode' style='display:none'>" + PaymentModeCode + "</td>";
                        rowstr += "<td class='PaymentModeDescription' style='align:center'>" + PaymentModeDescription + "</td>";
                        rowstr += "<td class='FormDate' style='align:center'>" + FormDate + "</td>";
                        rowstr += "<td class='Todate' style='align:center'>" + Todate + "</td>";
                        rowstr += "<td class='chkSelect' style='align:center'><input type='button' name='Delete' value='Delete' class='btnRowDelete'/></td>";
                        rowstr += "</tr>";
                        DomControls.ctrl_tblAddRow.find("tbody").append(rowstr);
                       
                    }

                    
                }
                
            });
            $.each($("#tblPaymentModes tbody tr"), function (index, value) {
                $(value).find('.chkSelect').prop("checked", false);
            });
            (function () {
                RegisterEvent();
            }());
        };
        var onDeleteRow = function () {
                $(this).parent().parent().remove();
        };
        this.app_Initiate = function () {
            BindEvents();
            PopulatePaymentMode();
            serverObject.GetCounters(function (response) {
                CounterDataSource = response.Data;
                PopulateCounterControlToDataSource();
            });
        };
        var PopulatePaymentModeCounterAssociationTable = function () {
            var CounterId = DomControls.ctrl_DrpCounter.val();

            var ctr_btnRowDel = $(".btnRowDel");
            var btnRowEdit = $(".btnRowEdit");
            DomControls.ctrl_tblPaymentModeCounterWise.find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td style='display:none' class='RowId'>" + rowData.RowId + "</td>";
                rowstr += "<td style='display:none' class='PaymentModeCode'>" + rowData.PaymentModeCode + "</td>";
                rowstr += "<td style='display:none' class='CounterId'>" + rowData.CounterId + "</td>";
                rowstr += "<td  class='PaymentMode'>" + rowData.PaymentMode + "</td>";
                rowstr += "<td class='FromDate'>" + CONVERTER.Date.convertCsToJsDate(rowData.FromDate).format("yyyy-mm-dd") + "</td>";
                rowstr += "<td  class='Todate'>" + CONVERTER.Date.convertCsToJsDate(rowData.Todate).format("yyyy-mm-dd") + "</td>";
                rowstr += "<td><a class='btnRowEdit'>Edit</a></td>";
                rowstr += "<td><a class='btnRowDel'>Delete</a></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onDeleteRow = function () {
                var ctrl = $(this);
                var currentrow = ctrl.parent().parent();

                //find row
                var RowId = currentrow.find(".RowId").text();
                serverObject.DeletePaymentModeCounterWise(RowId, function (response) {
                    if (response.Status == 200) {
                        alert("Record Deleted Sucessfully!!");
                    }
                });
           };
            var onEditRow = function () {
                var ctrl = $(this);
                //find row
                //construct model
                var currentrow = ctrl.parent().parent();
                var PaymentModeCode = currentrow.find(".PaymentModeCode").text();
                var CounterId = currentrow.find(".CounterId").text();
                var FromDate = currentrow.find(".FromDate").text();
                var Todate = currentrow.find(".Todate").text();
                var RowId = currentrow.find(".RowId").text();
                $("#btnInsert").attr('RowId', RowId);

                var model = { RowId: RowId, PaymentModeCode: PaymentModeCode, CounterId: CounterId, FromDate: FromDate, Todate: Todate };
                feedToForm(model);
            };
            var feedToForm = function (data) {

                DomControls.ctrl_DrpCounter.find("option[value='" + CounterId + "']").prop('selected', true);
                DomControls.ctrl_FormDate.val(data.FromDate);
                DomControls.ctrl_ToDate.val(data.Todate);
                $("#tblPaymentModes > tbody > tr .chkSelect[data-payment-code !='" + data.PaymentModeCode + "']").prop("checked", false);
                $("#tblPaymentModes > tbody > tr .chkSelect[data-payment-code='" + data.PaymentModeCode + "']").prop("checked", true);

            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowDel', onDeleteRow);
                $(document).on('click', '.btnRowDel', onDeleteRow);

                $(document).off('click', '.btnRowEdit', onEditRow);
                $(document).on('click', '.btnRowEdit', onEditRow);
            };
            var appendToBody = function (rowString) {
                DomControls.ctrl_tblPaymentModeCounterWise.find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.GetPaymentModesCounerWise(CounterId, function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());

            ///////////////
        };
        
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});