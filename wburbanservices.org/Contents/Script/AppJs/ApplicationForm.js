﻿var mode = '', ApplicationId = '', UserType = '';
var SpecificationRWidth = '';
var SpecificationRDepth = '';
var SpecificationPNo = '';
var SpecificationPLength = '';
var SpecificationPWidth = '';
var SpecificationPDepth = '';
var SpecificationTDiameter = ''; 

$(document).ready(function () {

    Bind_Date();
    AutoComplete_Municipality();

    $("#ddlTypeofSurface").change(function (evt) {
        Bind_TypeofRoad('');
    });
    $("#ddlSpecification").change(function (evt) {
        Show_ddlSpecification();
    });
    $("#ddlSpecification_P").change(function (evt) {
        Show_ddlSpecification_P();
    });
    //$("#ddlMunicipality").change(function (evt) {
    //    Bind_Ward();
    //});
    $("#ddlWard").change(function (evt) {
        Bind_Location('');
    });
    $("#ddlAction").change(function (evt) {
        var actionID = $("#ddlAction").val();
        if ((actionID == 'F' && mode == 'N') || (actionID == 'Z' && mode == 'G'))
        {
            alert('Sorry ! Invalid selection !!'); $("#ddlAction").val(''); return false;
        }
        Bind_Designation();
    });
    $("#btnAdd").click(function (evt) {
        Add();
    });
    $("#btnPopUpAdd").click(function (evt) {
        Add_PopUp();
    });
    $("#btnOtherChargeAdd").click(function (evt) {
        AddOtherCharge();
    });
    $("#btnSave").click(function (evt) {
        Save();
    });
    $("#btnRefresh").click(function (evt) {
        Refresh();
    });
    $("#btnBack").click(function (evt) {
        window.location.href = '/OtherServices/OtherServices/RedirectFromModule?modcode=RC';
    });
    
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //ApplicationId = getParameterByName("ApplicationId", window.location.href); 
    //mode = getParameterByName("mode", window.location.href); 
    ApplicationId = $('.clsVariables').attr('ApplicationId'); 
    mode = $('.clsVariables').attr('UrlMode'); 

    if (ApplicationId > 0) {
      
        if (mode != "V" )
            Execute();
        else
            Load(ApplicationId, mode);
        
        $(".clsBackButton").css('display', '');
        UserType = $('.clsVariables').attr('usertype'); 
    }
    else {
        GetCredentials();
    }
       
    
    //ApplyOption(mode);
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function AllowDecimal()
{
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}
function Bind_Date() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear(); 

    $('#txtStartofWork, #txtEndofWork,#txtVerificationDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            //$("#txtEndofWork").datepicker("option", "maxDate", $("#txtStartofWork").val());
            $("#txtEndofWork").datepicker("option", "minDate", $("#txtStartofWork").val());
        }
    });


    $("#txtVerificationDate").val(output);
    $("#txtStartofWork").datepicker("option", "minDate", output);
}
function AutoComplete_Municipality()
{
    $('#ddlMunicipality').autocomplete({
        source: function (request, response) {

            var GLS = "S";

            var S = "{Desc:'" + $('#ddlMunicipality').val() + "'}"; //alert(S);
            $.ajax({
                url: '/OtherServices/ApplicationForm/AutoComplete_Municipality',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = []; //alert(JSON.stringify(serverResponse.Data));

                    if (serverResponse.Data == null) {
                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.MunicipalityName,
                                MunicipalityID: item.MunicipalityID
                            });
                        });

                        response(AutoComplete);
                    }
                    else {
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });

                            response(AutoComplete);
                        }
                    }

                }
            });
        },
        select: function (e, i) {
            $('.clsVariables').attr('automunicipalityid', i.item.MunicipalityID);
            Bind_Ward();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#ddlMunicipality').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#ddlMunicipality").val('');
            $('.clsVariables').attr('automunicipalityid', '');
        }
        if (iKeyCode == 46) {
            $("#ddlMunicipality").val('');
            $('.clsVariables').attr('automunicipalityid', '');
        }
    });
}
function Bind_TypeofRoad(RoadTypeId)
{
    if ($("#ddlTypeofSurface").val() != "")
    {
        var E = "{SurfaceID: '" + $("#ddlTypeofSurface").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_TypeofRoad',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                $('#ddlTypeofRoad').empty();
                $('#ddlTypeofRoad').append('<option value="">Select</option');

                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlTypeofRoad').append('<option value=' + item.RoadTypeID + '>' + item.RoadTypeName + '</option>');
                        });
                        if (RoadTypeId != "") { $('#ddlTypeofRoad').val(RoadTypeId); } 
                    }
                }
            }
        });
    }
}
function Bind_Ward() {
    var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
    if (MunicipalityID != "") {
        var E = "{MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_Ward',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlWard').empty();
                $('#ddlWard').append('<option value="">Select</option');

                $('#ddlWard_P').empty();
                $('#ddlWard_P').append('<option value="">Select</option');

                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlWard, #ddlWard_P').append('<option value=' + item.WardID + '>' + item.WardName + '</option>');
                        });
                    }
                }
            }
        });
    }
}
function Bind_Location(LocationId) {
    if ($("#ddlWard").val() != "") {
        var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
        var E = "{WardID: '" + $("#ddlWard").val() + "', MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_LocationByWard',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlLocation').empty();
                $('#ddlLocation').append('<option value="">Select</option');
                if (t != null)
                {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlLocation').append('<option value=' + item.LocationID + '>' + item.LocationName + '</option>');
                        });
                    }
                    if (LocationId != "") $('#ddlLocation').val(LocationId);
                }
            }
        });
    }
}


function Show_ddlSpecification()
{
    if ($("#ddlSpecification").val() != "") {
        var E = "{SpecificationID: '" + $("#ddlSpecification").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Show_Specification',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        SpecificationRWidth = item.SpecificationRWidth; 
                        SpecificationRDepth = item.SpecificationRDepth; 
                        SpecificationPNo = item.SpecificationPNo; 
                        SpecificationPLength = item.SpecificationPLength; 
                        SpecificationPWidth = item.SpecificationPWidth; 
                        SpecificationPDepth = item.SpecificationPDepth; 
                        SpecificationTDiameter = item.SpecificationTDiameter; 

                        $("#RWidth").val(''); $("#RDepth").val(''); $("#PNo").val(''); $("#PLength").val('');
                        $("#PWidth").val(''); $("#PDepth").val(''); $("#TDiameter").val('');

                        if (SpecificationRWidth == true) $("#RWidth").css('display', ''); else $("#RWidth").css('display', 'none');
                        if (SpecificationRDepth == true) $("#RDepth").css('display', ''); else $("#RDepth").css('display', 'none');
                        if (SpecificationPNo == true) $("#PNo").css('display', ''); else $("#PNo").css('display', 'none');
                        if (SpecificationPLength == true) $("#PLength").css('display', ''); else $("#PLength").css('display', 'none');
                        if (SpecificationPWidth == true) $("#PWidth").css('display', ''); else $("#PWidth").css('display', 'none');
                        if (SpecificationPDepth == true) $("#PDepth").css('display', ''); else $("#PDepth").css('display', 'none');
                        if (SpecificationTDiameter == true) $("#TDiameter").css('display', ''); else $("#TDiameter").css('display', 'none');
                    });
                }
            }
        });
    }
}
function Add()
{
    var chkSpection = Check_Specification(); if (chkSpection == false) { return false; } 
    var reChk = Check_LatLong();  if (reChk == false) { return false;}
    if ($("#ddlTypeofSurface").val() == "") { $("#ddlTypeofSurface").focus(); return false; }
    if ($("#ddlTypeofRoad").val() == "") { $("#ddlTypeofRoad").focus(); return false; }
    if ($("#txtLongitudeStart").val() == "") { $("#txtLongitudeStart").focus(); return false; }
    if ($("#txtLongitudeEnd").val() == "") { $("#txtLongitudeEnd").focus(); return false; }
    if ($("#txtLatitudeStart").val() == "") { $("#txtLatitudeStart").focus(); return false; }
    if ($("#txtLatitudeEnd").val() == "") { $("#txtLatitudeEnd").focus(); return false; }

    if ($("#ddlWard").val() == "") { $("#ddlWard").focus(); return false; }
    if ($("#ddlLocation").val() == "") { $("#ddlLocation").focus(); return false; }
    if ($("#txtStartofWork").val() == "") { $("#txtStartofWork").focus(); return false; }
    if ($("#txtEndofWork").val() == "") { $("#txtEndofWork").focus(); return false; }
    if ($("#txtEscavation").val() == "") { $("#txtEscavation").focus(); return false; }
    if ($("#ddlSpecification").val() == "") { $("#ddlSpecification").focus(); return false; }

    var html = ""
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsDetailID' >" + $("#hdnDetailID").val() + "</td>"
        + "<td style='display:none;' class='clsStatus' chk-pservid='" + $(".clsVariables").attr('hdnParentID')+ "' chk-insertmode='A' chk-dtl='" + $("#hdnDetailID").val() + "'>" + 1 + "</td>"

        + "<td style='display:none;' class='clsTypService'  >" + $("#ddlTypeofSurface").val() + "</td>"
        + "<td>" + $("#ddlTypeofSurface option:selected").text() + "</td>"

        + "<td style='display:none;' class='clsTypRoad' chk-data='" + $("#ddlTypeofRoad").val() + "' >" + $("#ddlTypeofRoad").val() + "</td>"
        + "<td>" + $("#ddlTypeofRoad option:selected").text() + "</td>"

        + "<td>LS: <span class='clsLongStart'>" + $("#txtLongitudeStart").val() + "</span><br> LE: <span class='clsLongEnd'>" + $("#txtLongitudeEnd").val() + "</span></td>"
        + "<td>LS: <span class='clsLatStart'>" + $("#txtLatitudeStart").val() + "</span><br> LE: <span class='clsLatEnd'>" + $("#txtLatitudeEnd").val() + "</span></td>"

        + "<td style='display:none;' class='clsWard'  >" + $("#ddlWard").val() + "</td>"
        + "<td>" + ($("#ddlWard").val() == "" ? "" : $("#ddlWard option:selected").text()) + "</td>"

        + "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation").val() + "' >" + $("#ddlLocation").val() + "</td>"
        + "<td>" + ($("#ddlLocation").val() == "" ? "" : $("#ddlLocation option:selected").text()) + "</td>"

        + "<td class='clsLandMark'>" + $("#txtLandMark").val() + "</td>"
        + "<td class='clsBorough'>" + $("#txtBorough").val() + "</td>"
        + "<td class='clsStartofWork'>" + $("#txtStartofWork").val() + "</td>"
        + "<td class='clsEndofWork'>" + $("#txtEndofWork").val() + "</td>"
        + "<td class='clsEscavation'>" + $("#txtEscavation").val() + "</td>"

        + "<td style='display:none;' class='clsSpecification' chk-data='" + $("#ddlSpecification").val() + "' >" + $("#ddlSpecification").val() + "</td>"
        + "<td class='clsSpecificationText'>" + SpecificationString() + "</td>"

        + "<td class='cls mode_E' style='text-align:center'><input type='text' class='clsEstimatedCost allownumericwithdecimal' style='width:80px' value='" + $("#hdnEstimationCost").val() + "'   " + ($("#hdnEstimationCost").val() > 0 ? "" : "disabled") +" /><span class='glyphicon glyphicon-edit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;display:none;'  onClick='EditApplicantDetail(this);'></span></td>"
        +"<td class='clsApprove cls mode_R' style='text-align:center; display:none;'><input type='checkbox' checked disabled class='chk cls mode_R' onClick='ButtonAction()' /></td>"
        //+"<td class='cls mode_G'  ><input type='text'  style='width:80px' class='allownumericwithdecimal clsRate' onkeyup='Calculate(this)' /></td>"
        +"<td class='clsCalAmount cls mode_G' ><input type='text' onkeyup='Calculate(this)' style='width:80px' class='allownumericwithdecimal clsAmount' /></td>"


        + "<td style='text-align:center' class='clsEditOption'>"
        + "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditDetail(this);'></span>"
        + "</br></br><span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center;'  onClick='DeleteDetail(this);'></span>"
        + "</td>"+
    html + "</tr>";


    var dtlID = $("#hdnDetailID").val(); 
    if (dtlID != "") {
        //$('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").text(0);
        //$('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").closest('tr').css('display', 'none');
        $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").closest('tr').remove();
    }
    $parentTR.after(html);

    $("#ddlTypeofSurface").val(''); $("#ddlTypeofRoad").val(''); $("#hdnEstimationCost").val('');
    $("#ddlTypeofRoad").val(''); $("#ddlTypeofRoad").val('');
    $("#txtLongitudeStart").val(''); $("#txtLongitudeEnd").val(''); $("#txtLatitudeStart").val(''); $("#txtLatitudeEnd").val('');
    $("#ddlWard").val(''); $("#ddlLocation").val(''); $("#txtStartofWork").val('');
    $("#txtLandMark").val(''); $("#txtBorough").val('');
    $("#txtEndofWork").val(''); $("#txtEscavation").val(''); $("#ddlSpecification").val(''); 

    $("#RWidth").val('');$("#RDepth").val('');$("#PNo").val('');$("#PLength").val('');
    $("#PWidth").val(''); $("#PDepth").val(''); $("#TDiameter").val('');
    $(".clsVariables").attr('hdnParentID', '');

    $("#hdnDetailID").val('');$("#hdnStatus").val('');

    $('.dtl').css('display', 'none');
    ApplyOption(mode);
}
function AddOtherCharge()
{
    if ($("#ddlOtherCharge").val() == "") { $("#ddlOtherCharge").focus(); return false; }
    if ($("#txtOtherAmount").val() == "") { $("#txtOtherAmount").focus(); return false; }

    var l = $('#tblOtherCharges tr.myData').find("td[chk-data='" + $("#ddlOtherCharge").val() + "']").length;
    if (l > 0) {
        alert('Sorry ! This Charge Type is Already Available.'); $("#ddlOtherCharge").val(''); $("#ddlOtherCharge").focus(); return false;
    }

    var html = ""
    var $this = $('#tblOtherCharges .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsID' chk-data='" + $("#ddlOtherCharge").val() +"'>" + $(".clsVariables").attr('otherchargeMainID') + "</td>"

        + "<td style='display:none;' class='clsChargeID'  >" + $("#ddlOtherCharge").val() + "</td>"
        + "<td>" + $("#ddlOtherCharge option:selected").text() + "</td>"

        + "<td class='clsOtherAmt' style='text-align:right'>" + $("#txtOtherAmount").val() + "</td>"

        + "<td style='text-align:center' class='clsEditOption'>"
        + "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; display:none; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditOtherChargeDetail(this);'></span>"
        + "<span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center; padding-left:10px'  onClick='DeleteOtherChargeDetail(this, 0);'></span>"
        + "</td>" +
        html + "</tr>";
    $parentTR.after(html);
    $("#ddlOtherCharge").val(''); $("#txtOtherAmount").val('');
    CalculateFinal();
}
function EditDetail(ID)
{
    $("#hdnDetailID").val($(ID).closest('tr').find('.clsDetailID').text());
    $("#hdnStatus").val($(ID).closest('tr').find('.clsStatus').text());

    $("#ddlTypeofSurface").val($(ID).closest('tr').find('.clsTypService').text()); Bind_TypeofRoad($(ID).closest('tr').find('.clsTypRoad').text());
    $("#ddlTypeofRoad").val($(ID).closest('tr').find('.clsTypRoad').text());

    $("#txtLongitudeStart").val($(ID).closest('tr').find('.clsLongStart').text());
    $("#txtLongitudeEnd").val($(ID).closest('tr').find('.clsLongEnd').text());
    $("#txtLatitudeStart").val($(ID).closest('tr').find('.clsLatStart').text());
    $("#txtLatitudeEnd").val($(ID).closest('tr').find('.clsLatEnd').text());

    $("#ddlWard").val($(ID).closest('tr').find('.clsWard').text()); Bind_Location($(ID).closest('tr').find('.clsLocation').text());
    $("#ddlLocation").val($(ID).closest('tr').find('.clsLocation').text());
    $("#txtLandMark").val($(ID).closest('tr').find('.clsLandMark').text());
    $("#txtBorough").val($(ID).closest('tr').find('.clsBorough').text());

    $("#txtStartofWork").val($(ID).closest('tr').find('.clsStartofWork').text());
    $("#txtEndofWork").val($(ID).closest('tr').find('.clsEndofWork').text());
    $("#txtEscavation").val($(ID).closest('tr').find('.clsEscavation').text());
    $("#ddlSpecification").val($(ID).closest('tr').find('.clsSpecification').text());

    var clsSpecificationText = $(ID).closest('tr').find('.clsSpecificationText').text(); //alert(clsSpecificationText);
    var RWidth = '', RDepth = '', PNo = '', PLength = '', PWidth = '', PDepth = '', TDiameter = '';
    if (clsSpecificationText != "") {
        var ls = clsSpecificationText.split(','); //alert(ls); alert(ls.length);
        if (ls.length > 0) {
            //alert(ls);
            for (var i = 0; i < ls.length; i++) {
                var keyValue = ls[i].split(":");
                if (keyValue[0].trim() == 'RWidth') { RWidth = keyValue[1].trim(); $("#RWidth").val(RWidth); (RWidth == "" ? $("#RWidth").css('display', 'none') : $("#RWidth").css('display', '') ) }
                    
                if (keyValue[0].trim() == 'RDepth') { RDepth = keyValue[1].trim(); $("#RDepth").val(RDepth); (RDepth == "" ? $("#RDepth").css('display', 'none') : $("#RDepth").css('display', '')) }

                if (keyValue[0].trim() == 'PNo') { PNo = keyValue[1].trim(); $("#PNo").val(PNo); (PNo == "" ? $("#PNo").css('display', 'none') : $("#PNo").css('display', '')) }

                if (keyValue[0].trim() == 'PLength') { PLength = keyValue[1].trim(); $("#PLength").val(PLength); (PLength == "" ? $("#PLength").css('display', 'none') : $("#PLength").css('display', '')) }
                  
                if (keyValue[0].trim() == 'PWidth') { PWidth = keyValue[1].trim(); $("#PWidth").val(PWidth); (PWidth == "" ? $("#PWidth").css('display', 'none') : $("#PWidth").css('display', '')) }
                   
                if (keyValue[0].trim() == 'PDepth') { PDepth = keyValue[1].trim(); $("#PDepth").val(PDepth); (PDepth == "" ? $("#PDepth").css('display', 'none') : $("#PDepth").css('display', '')) }
                   
                if (keyValue[0].trim() == 'TDiameter') { TDiameter = keyValue[1].trim(); $("#TDiameter").val(TDiameter); (TDiameter == "" ? $("#TDiameter").css('display', 'none') : $("#TDiameter").css('display', '')) }
                    
            }
        }
    }

    $("#hdnEstimationCost").val($(ID).closest('tr').find('.clsEstimatedCost').val());

    $(".clsVariables").attr('hdnParentID', $(ID).closest('tr').find('.clsStatus').attr('chk-pservid'));

    //var detailID = $(ID).closest('tr').find('.clsDetailID').text();
    //if (detailID == "")
        //$(ID).closest('tr').remove();
}
function DeleteDetail(ID)
{
    var res = confirm('Are you sure want to delete ?');
    if (res == true) {
        //$(ID).closest('tr').remove();
        var dtlID = $(ID).closest('tr').find('.clsDetailID').text();
        $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").text(0);
        $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").closest('tr').css('display', 'none');
    }
       

}
function EditOtherChargeDetail(ID) {
    $("#ddlOtherCharge").val($(ID).closest('tr').find('.clsChargeID').text());
    $("#txtOtherAmount").val($(ID).closest('tr').find('.clsOtherAmt').text());
    $(".clsVariables").attr('otherchargeMainID', $(ID).closest('tr').find('.clsID').text())
    $(ID).closest('tr').remove();
    CalculateFinal();
}
function DeleteOtherChargeDetail(ID, detlID) {
    var res = confirm('Are you sure want to delete ?');
    if (res == true) {

        if (detlID > 0) {
            var E = "{OtherChargeID: '" + detlID + "'} ";
            $.ajax({
                type: "POST",
                url: '/OtherServices/ApplicationForm/Delete_OtherCharge',
                contentType: "application/json; charset=utf-8",
                data: E,
                dataType: "json",
                success: function (responce) {
                    var xmlDoc = $.parseXML(responce.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    var Result = $.trim($(Table).find("Result").text());
                    if (Result == 1) {
                        $(ID).closest('tr').remove();
                        CalculateFinal();
                    }
                    else
                        alert('Sorry ! There is some problem.');
                }
            });
        }
        else {
            $(ID).closest('tr').remove();
            CalculateFinal();
        }
    }
}

function SpecificationString()
{
    var retString = "";
    var spectiontxt = $("#ddlSpecification option:selected").text();

    //var SpecificationRWidth = $.trim($("#RWidth").val() == "" ? "0" : $("#RWidth").val());
    //var SpecificationRDepth = $.trim($("#RDepth").val() == "" ? "0" : $("#RDepth").val());
    //var SpecificationPNo = $.trim($("#PNo").val() == "" ? "0" : $("#PNo").val());
    //var SpecificationPLength = $.trim($("#PLength").val() == "" ? "0" : $("#PLength").val());
    //var SpecificationPWidth = $.trim($("#PWidth").val() == "" ? "0" : $("#PWidth").val());
    //var SpecificationPDepth = $.trim($("#PDepth").val() == "" ? "0" : $("#PDepth").val());
    //var SpecificationTDiameter = $.trim($("#TDiameter").val() == "" ? "0" : $("#TDiameter").val());
    var SpecificationRWidth = $.trim($("#RWidth").val());
    var SpecificationRDepth = $.trim($("#RDepth").val());
    var SpecificationPNo = $.trim($("#PNo").val());
    var SpecificationPLength = $.trim($("#PLength").val());
    var SpecificationPWidth = $.trim($("#PWidth").val());
    var SpecificationPDepth = $.trim($("#PDepth").val());
    var SpecificationTDiameter = $.trim($("#TDiameter").val());

    if (SpecificationRWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RWidth: " + SpecificationRWidth
    }
    if (SpecificationRDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RDepth: " + SpecificationRDepth
    }
    if (SpecificationPNo != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PNo: " + SpecificationPNo
    }
    if (SpecificationPLength != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PLength: " + SpecificationPLength
    }
    if (SpecificationPWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PWidth: " + SpecificationPWidth
    }
    if (SpecificationPDepth !="") {
        retString = (retString == "" ? "" : retString + ", ") + "PDepth: " + SpecificationPDepth
    }
    if (SpecificationTDiameter !="") {
        retString = (retString == "" ? "" : retString + ", ") + "TDiameter: " + SpecificationTDiameter
    }

    return spectiontxt + ", " + retString;
}
function Check_Specification()
{
    var ret = true;

    if (SpecificationRWidth == true && $("#RWidth").val() == "") { $("#RWidth").focus(); ret = false; }
    if (SpecificationRDepth == true && $("#RDepth").val() == "") { $("#RDepth").focus(); ret = false; };
    if (SpecificationPNo == true && $("#PNo").val() == "") { $("#PNo").focus(); ret = false; };
    if (SpecificationPLength == true && $("#PLength").val() == "") { $("#PLength").focus(); ret = false; }
    if (SpecificationPWidth == true && $("#PWidth").val() == "") { $("#PWidth").focus(); ret = false; }
    if (SpecificationPDepth == true && $("#PDepth").val() == "") { $("#PDepth").focus(); ret = false; }
    if (SpecificationTDiameter == true && $("#TDiameter").val() == "") { $("#TDiameter").focus(); ret = false; }

    return ret;
}

function Save() {

    var ArrList = [], ArrOtherCrgList = [];
    var userType = $('.clsVariables').attr('usertype');
    var municipalityID = $('.clsVariables').attr('automunicipalityid'); //$("#ddlMunicipality").val();
    var purposeID = $("#ddlPurpose").val();
    var projectName = $("#txtProjectName").val();

    if (municipalityID == "") { alert('Please Select Project Municipality/Corporation.'); $("#ddlMunicipality").focus(); return false; }
    if (purposeID == "") { alert('Please Select Purpose of Road Cutting.'); $("#ddlPurpose").focus(); return false; }
    if (projectName == "") { alert('Please Enter Name of project.'); $("#txtProjectName").focus(); return false; }

    //DETAIL DATA
    var l1 = $('#tbl tbody tr.myData').length; var isEstimationCost = 0;
    if (l1 == 0) { alert('Please add Project details.'); return false; }
    $('#tbl tbody tr.myData').each(function () {

        var clsDetailID = $(this).find('.clsDetailID').text();
        var clsStatus = $(this).find('.clsStatus').text();

        var clsInsertMode = $(this).find('.clsStatus').attr('chk-insertmode');
        var clsParentID = $(this).find('.clsStatus').attr('chk-pservid');

        var clsTypService = $(this).find('.clsTypService').text();
        var clsTypRoad = $(this).find('.clsTypRoad').text();

        var clsLongStart = $(this).find('.clsLongStart').text();
        var clsLongEnd = $(this).find('.clsLongEnd').text();
        var clsLatStart = $(this).find('.clsLatStart').text();
        var clsLatEnd = $(this).find('.clsLatEnd').text();

        var clsWard = $(this).find('.clsWard').text();
        var clsLocation = $(this).find('.clsLocation').text();
        var clsLandMark = $(this).find('.clsLandMark').text();
        var clsBorough = $(this).find('.clsBorough').text();
        var clsStartofWork = $(this).find('.clsStartofWork').text();
        var clsEndofWork = $(this).find('.clsEndofWork').text();

        var clsEscavation = $(this).find('.clsEscavation').text();
        var clsSpecification = $(this).find('.clsSpecification').text();
        var clsSpecificationText = $(this).find('.clsSpecificationText').text(); //alert(clsSpecificationText);
        var RWidth = '', RDepth = '', PNo = '', PLength = '', PWidth = '', PDepth = '', TDiameter = '';
        if (clsSpecificationText != "") {
            var ls = clsSpecificationText.split(',');
            if (ls.length > 0) {
                //alert(ls);
                for (var i = 0; i < ls.length; i++) {
                    var keyValue = ls[i].split(":");
                    if (keyValue[0].trim() == 'RWidth')
                        RWidth = keyValue[1];
                    if (keyValue[0].trim() == 'RDepth')
                        RDepth = keyValue[1];
                    if (keyValue[0].trim() == 'PNo')
                        PNo = keyValue[1];
                    if (keyValue[0].trim() == 'PLength')
                        PLength = keyValue[1];
                    if (keyValue[0].trim() == 'PWidth')
                        PWidth = keyValue[1];
                    if (keyValue[0].trim() == 'PDepth')
                        PDepth = keyValue[1];
                    if (keyValue[0].trim() == 'TDiameter')
                        TDiameter = keyValue[1];
                }
            }
        }

        var clsEstimatedCost = $(this).find('.clsEstimatedCost').val();
        var ischk = $(this).find('.chk').prop('checked');
        var clsApprove = (ischk == true ? 1 : 0); //$(this).find('.clsApprove').text();
        var clsRate = $(this).find('.clsRate').val();
        var clsCalAmount = $(this).find('.clsAmount').val();

        if (mode == 'E') { if (clsEstimatedCost == '' || clsEstimatedCost == '0') { isEstimationCost = 1; }  }

        ArrList.push({
            'DetailID': clsDetailID, 'Status': clsStatus,
            'TypSurface': clsTypService, 'TypRoad': clsTypRoad,
            'LongStart': clsLongStart, 'LongEnd': clsLongEnd, 'LatStart': clsLatStart, 'LatEnd': clsLatEnd,

            'Ward': clsWard, 'Location': clsLocation, 'LandMark': clsLandMark, 'BoroughNo': clsBorough, 'StartofWork': clsStartofWork, 'EndofWork': clsEndofWork,

            'Escavation': clsEscavation, 'Specification': clsSpecification,
            'RWidth': RWidth, 'RDepth': RDepth, 'PNo': PNo,
            'PLength': PLength, 'PWidth': PWidth, 'PDepth': PDepth, 'TDiameter': TDiameter,
            'EstimatedAmount': clsEstimatedCost, 'VerifiedStatus': clsApprove, 'ApprovedRate': clsRate, 'ApprovedAmount': clsCalAmount,
            'ParentServDatId': clsParentID, 'insertMode': clsInsertMode
        });
    });
    if (isEstimationCost > 0) { alert('Please enter Estimation Amount for project detail.'); return false; }


    ////FILE UPLOAD VALIDATION
    var imgCnt = 0, ismaxCnt = 0, qty = 0, docName = "";
    $('#tblimg tbody tr.myData').each(function () {
        var Id = $(this).find('.flimg').attr('id');
        var isMand = $(this).find('.flimg').attr('mandatory'); 
        var maxqty = $(this).find('.flimg').attr('maxQuantity'); var restmaxqty = $(this).find('.flimg').attr('maxQuantity');
        var doctName = $(this).find('.flimg').attr('docName');
        var isvisible = $(this).find('.flimg').attr('isvisible');
        var isEnable = $(this).find('.flimg').attr('isEnable');
        var doctid = $(this).find('.flimg').attr('doctypeid'); //alert(doctid);
        var isMand = $(this).find('.flimg').attr('mandatory'); 
        var filerKit = $('#' + Id).prop("jFiler");
       
        var l1 = $('#tblimg tr.img_' + doctid).find('tr').length; //alert(l); //return false;
        var l2 = filerKit.files_list.length;
        var l3 = l1 + l2;
        //maxqty = maxqty - l3;
       

        if (isMand == 1 && (isEnable == "True" || isEnable == true)) {
            if (l3 > maxqty)
            {
                ismaxCnt = (doctid == 32 ? 0 : 1);
                docName = doctName;
            }
            if (l3 == 0) {
                imgCnt = (doctid == 32 ? 0 : 1);
                docName = doctName;
            }
        }
    });
    if (imgCnt == 1) {   //here N is for update inspector resedule
        alert('Sorry ! Please Confirm File/Image for ' + docName); return false;
    }
    if (ismaxCnt == 1) {
        alert('Sorry ! Please Complete ' + qty + ' File/Image ' +  'for ' + docName); return false;
    }


    ////OTHER CHARGES DATA
    //var l2 = $('#tblOtherCharges tbody tr.myData').length;
    //if (l2 == 0) { alert('Please add Estimation Other Charge details.'); return false; }
    $('#tblOtherCharges tbody tr.myData').each(function () {
        var clsChargeID = $(this).find('.clsChargeID').text();
        var clsOtherAmt = $(this).find('.clsOtherAmt').text();

        ArrOtherCrgList.push({
            'OtherChargeID': clsChargeID, 'OtherAmt': clsOtherAmt,
        });
    });
    

    //FORWARD/ASSIGN VALIDATION
    if (userType == 'MunicipalityUser')
    {
        if ($('#ddlAction').val() == "") { alert('Please Select Action.'); $("#ddlAction").focus(); return false; }
        if ($('#ddlDesignation').val() == "") { alert('Please Select Designation.'); $("#ddlDesignation").focus(); return false; }
        if ($('#txtRemarks').val() == "") { alert('Please Enter Remarks.'); $("#txtRemarks").focus(); return false; }
    }

    //alert(JSON.stringify(ArrList));
    //return false;
    var E = "{ApplicationID: '" + $('#hdnApplicationID').val() + "' , municipalityID: '" + municipalityID + "', purposeID: '" + purposeID + "', " +
        "projectName: '" + projectName + "', Action: '" + $('#ddlAction').val() + "', DesignationID: '" + $('#ddlDesignation').val() + "'," +
        "VerificationDate: '" + $('#txtVerificationDate').val() + "', Remarks: '" + $('#txtRemarks').val() + "', GrossEstimationAmount: '" + $('.clsGrossEstChargeAmt').text() + "', " +
        "obj: " + JSON.stringify(ArrList) + ", objOtherCharge: " + JSON.stringify(ArrOtherCrgList) + "}";

    $.ajax({
        url: '/OtherServices/ApplicationForm/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");

            var MSGID = $.trim($(Table).find("MSGID").text());
            var MSG = $.trim($(Table).find("MSG").text()); 

            var ApplicationID = $.trim($(Table1).find("ApplicationID").text()); 
            var SubModuleID = $.trim($(Table1).find("SubModuleID").text()); 

            if (MSGID == 1)
            {
                Upload_Doc(ApplicationID, SubModuleID);
                alert(MSG);

                var modcode = $('#btnBack').attr('modcode');
                var href = $('#btnBack').attr('href');
                if (modcode == "" || modcode == null)
                    location.reload();
                else
                    window.location.href = href;
            }
            else {
                alert('Sorry ! There is some Problem.');
                return false;
            }
        }
    });
}
function Upload_Doc(ApplicationID, SubModuleID) {
   
    $('#tblimg tbody tr.myData').each(function () {

        var formData = new FormData();
        var Id = $(this).find('.flimg').attr('id');
        var filerKit = $('#' + Id).prop("jFiler");
        //console.log(filerKit.files_list);

        //alert(filerKit.files_list.length);

        //var Id = $(this).find('.flimg').attr('id');
        var isMand = $(this).find('.flimg').attr('mandatory'); 
        var maxqty = $(this).find('.flimg').attr('maxQuantity');
        var doctName = $(this).find('.flimg').attr('docName');
        var docTypeId = $(this).find('.flimg').attr('docTypeId'); 


        //var file = filerKit.files_list[0].file; console.log(file);
        var totalFiles = filerKit.files_list.length; //console.log(totalFiles);
        if (totalFiles > 0) {
            for (var i = 0; i < totalFiles; i++) {
                var formData = new FormData();
                var file = filerKit.files_list[i].file; //console.log(file);
                var fileName = file.name; //console.log(fileName);
                //fileName = $('#' + Id).val().substring(12); console.log(fileName);
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.')); //console.log(fileNameExt);
                formData.append(Id, file, docTypeId + "#" + ApplicationID + "#" + SubModuleID + "#" + (parseInt(i) + 1) + fileNameExt);

                $.ajax({
                    type: "POST",
                    url: '/OtherServices/ApplicationForm/Upload',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {
                    }
                });
            }
        }
    });
}
function Refresh()
{
    location.reload();
}

function Load(ApplicationId, mode)
{
    var E = "{ApplicationID: '" + ApplicationId + "'}";
    $.ajax({
        url: '/OtherServices/ApplicationForm/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table"); 
            var Table1 = xml.find("Table1");
            var Table2 = xml.find("Table2");

            var ApplicationID = $.trim($(Table).find("ServMstId").text()); 
            var ApplicationNo = $.trim($(Table).find("ApplicationNo").text()); 
            var MunicipalityID = $.trim($(Table).find("MunicipalityDevAuthoId").text()); 
            var MunicipalityName = $.trim($(Table).find("MunicipalityName").text()); 
            var PurposeID = $.trim($(Table).find("PurposeOfService").text());
            var ProjectName = $.trim($(Table).find("NameOfProject").text());
            var GrossEstimationAmt = $.trim($(Table).find("GrossEstimationAmount").text());

            //MASTER 
            $("#hdnApplicationID").val(ApplicationID); $("#txtApplicationNo").val(ApplicationNo); 
            $("#ddlMunicipality").val(MunicipalityName); $('.clsVariables').attr('automunicipalityid', MunicipalityID); Bind_Ward();
            $("#ddlPurpose").val(PurposeID); $("#txtProjectName").val(ProjectName); $(".clsGrossEstChargeAmt").text(GrossEstimationAmt == "" ? 0 : GrossEstimationAmt);
            //DETAIL
            Detail(xml, mode);
            //DOCUMENT
            Document(xml, mode);
            //OTHER CHARGE
            OtherChargeDetail(xml, mode);
        }
    });
}
function Detail(xml, mode)
{
    $("#tbl tr.myData").remove();
    var html = "", Amt=0;
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');
    $(xml).find("Table1").each(function () {

        var isActive = $.trim($(this).find("isActive").text()); 
        var Amts = $.trim($(this).find("ApprovedAmount").text()) == "" ? 0 : $.trim($(this).find("ApprovedAmount").text());
        Amt = parseFloat(Amt) + parseFloat(Amts);
      
        $parentTR.after("<tr class='myData' >" +  //style='display:" + (isActive == 1 ? "" : "none") +"'

            "<td style='display:none;' class='clsDetailID' >" + $.trim($(this).find("ServDatId").text()) + "</td>" +
            "<td style='display:none;' class='clsStatus' chk-pservid='" + $.trim($(this).find("ParentServDatId").text()) + "' chk-insertmode='" + $.trim($(this).find("insertMode").text()) + "' chk-dtl='" + $.trim($(this).find("ServDatId").text()) + "'>" + $.trim($(this).find("isActive").text()) + "</td>" + 

            "<td style='display:none;' class='clsTypService'>" + $.trim($(this).find("SurfaceType").text()) + "</td>" +
            "<td>" + $.trim($(this).find("SurfaceName").text()) + "</td>" +
            "<td style='display:none;' class='clsTypRoad' chk-data='" + $("#ddlTypeofRoad").val() + "'>" + $.trim($(this).find("RoadType").text()) + "</td>" +
            "<td>" + $.trim($(this).find("RoadTypeName").text()) + "</td>" +

            "<td>LS: <span class='clsLongStart'>" + $.trim($(this).find("LongStart").text()) + "</span><br> LE: <span class='clsLongEnd'>" + $.trim($(this).find("LongEnd").text()) + "</span></td>"+
            "<td>LS: <span class='clsLatStart'>" + $.trim($(this).find("LatStart").text()) + "</span><br> LE: <span class='clsLatEnd'>" + $.trim($(this).find("LatEnd").text()) + "</span></td>"+


            "<td style='display:none;' class='clsWard' >" + $.trim($(this).find("WardNo").text()) + "</td>" +
            "<td>" + $.trim($(this).find("WardName").text()) + "</td>" +

            "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation").val() + "'>" + $.trim($(this).find("LocationId").text()) + "</td>" +
            "<td>" + $.trim($(this).find("LocationName").text()) + "</td>" +

            "<td class='clsLandMark'>" + $.trim($(this).find("LandMark").text()) + "</td>"+
            "<td class='clsBorough'>" + $.trim($(this).find("BoroughNo").text()) + "</td>"+
            "<td class='clsStartofWork'>" + $.trim($(this).find("WorkStratDate").text()) + "</td>"+
            "<td class='clsEndofWork'>" + $.trim($(this).find("WorkEndDate").text()) + "</td>"+
            "<td class='clsEscavation'>" + $.trim($(this).find("LengthOfEsca").text()) + "</td>" +

            "<td style='display:none;' class='clsSpecification' chk-data='" + $("#ddlSpecification").val() + "' >" + $.trim($(this).find("SpecififationType").text()) + "</td>"+
            "<td class='clsSpecificationText'>" + BindSpecificationString($.trim($(this).find("SpecificationRWidth").text()), $.trim($(this).find("SpecificationRDepth").text()),
            $.trim($(this).find("SpecificationPNo").text()), $.trim($(this).find("SpecificationPLength").text()), $.trim($(this).find("SpecificationPWidth").text()),
            $.trim($(this).find("SpecificationPDepth").text()), $.trim($(this).find("SpecificationTDiameter").text()), $.trim($(this).find("Specification").text())
            ) + "</td>" +

          
            "<td class='cls mode_E' style='text-align:center;' ><input type='text' class='clsEstimatedCost allownumericwithdecimal'  style='width:80px' value='" + $.trim($(this).find("EstimatedAmount").text()) + "'   /><span class='glyphicon glyphicon-check imgEditEstimation' style='color:blue; font-size:15px; cursor:pointer; text-align:center; display:none'  onClick='EditApplicantDetail(this);'></span></td>" +
            "<td class='clsApprove cls mode_R' style='text-align:center; display:none;'><input checked disabled type='checkbox' style='margin-top:15px'  class='chk cls mode_R' onClick='ButtonAction()'  /></td>" +
           // "<td class='cls mode_G' ><input type='text'  style='width:80px' class='allownumericwithdecimal clsRate' onkeyup='Calculate(this)' " + ($.trim($(this).find("ApprovedRate").text()) > 0 ? "disabled" : "") +" value='" + $.trim($(this).find("ApprovedRate").text()) + "' /></td>" +
            "<td class='clsCalAmount cls mode_G' ><input type='text' style='width:80px' class='allownumericwithdecimal clsAmount' onkeyup='Calculate(this)' value='" + $.trim($(this).find("ApprovedAmount").text()) + "' /></td>" +

            "<td style='text-align:center' class='clsEditOption'>"+
            "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditDetail(this);'></span>"+
            "</br></br><span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center;'  onClick='DeleteDetail(this);'></span> " +
            "</td> "+
            "</tr > ");
        //Estimation($.trim($(this).find("ServiceCurrStatus").text()));
    });
   
    //var fhtml = "";
    //fhtml = "<tr>" +
    //    "<td colspan='14' style='text-align:right; font-weight:bold;'>Total (&#x20B9;)</td>" +
    //    "<td style='text-align:right; font-weight:bold;' class='clsTotal'>" + Amt + "</td>" +
    //    "<td></td>" +
    //    "</tr > ";
    //$('#tbl tfoot').append(fhtml);

    AllowDecimal();
    //ApplyOption(mode);
    GetCredentials();
}
function DetailPopup(xml, mode) {
    $("#tblpopup tr.myData").remove();
    var html = "", Amt = 0;
    var $this = $('#tblpopup .test_0');
    $parentTR = $this.closest('tr');
    $(xml).find("Table1").each(function () {

        var isActive = $.trim($(this).find("isActive").text());
        var Amts = $.trim($(this).find("ApprovedAmount").text()) == "" ? 0 : $.trim($(this).find("ApprovedAmount").text());
        Amt = parseFloat(Amt) + parseFloat(Amts);

        $parentTR.after("<tr class='myData' style='display:" + (isActive == 1 ? "" : "none") + "'>" +
            "<td style='display:none;' class='clsDetailID' >" + $.trim($(this).find("ServDatId").text()) + "</td>" +
            "<td style='display:none;' class='clsStatus'  chk-dtl='" + $.trim($(this).find("ServDatId").text()) + "'>" + $.trim($(this).find("isActive").text()) + "</td>" +
            "<td style='display:none;' class='clsTypService'>" + $.trim($(this).find("SurfaceType").text()) + "</td>" +
            "<td>" + $.trim($(this).find("SurfaceName").text()) + "</td>" +
            "<td style='display:none;' class='clsTypRoad' chk-data='" + $("#ddlTypeofRoad").val() + "'>" + $.trim($(this).find("RoadType").text()) + "</td>" +
            "<td>" + $.trim($(this).find("RoadTypeName").text()) + "</td>" +

            "<td>LS: <span class='clsLongStart'>" + $.trim($(this).find("LongStart").text()) + "</span><br> LE: <span class='clsLongEnd'>" + $.trim($(this).find("LongEnd").text()) + "</span></td>" +
            "<td>LS: <span class='clsLatStart'>" + $.trim($(this).find("LatStart").text()) + "</span><br> LE: <span class='clsLatEnd'>" + $.trim($(this).find("LatEnd").text()) + "</span></td>" +


            "<td style='display:none;' class='clsWard' >" + $.trim($(this).find("WardNo").text()) + "</td>" +
            "<td>" + $.trim($(this).find("WardName").text()) + "</td>" +

            "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation").val() + "'>" + $.trim($(this).find("LocationId").text()) + "</td>" +
            "<td>" + $.trim($(this).find("LocationName").text()) + "</td>" +

            "<td class='clsLandMark'>" + $.trim($(this).find("LandMark").text()) + "</td>" +
            "<td class='clsBorough'>" + $.trim($(this).find("BoroughNo").text()) + "</td>" +
            "<td class='clsStartofWork'>" + $.trim($(this).find("WorkStratDate").text()) + "</td>" +
            "<td class='clsEndofWork'>" + $.trim($(this).find("WorkEndDate").text()) + "</td>" +
            "<td class='clsEscavation'>" + $.trim($(this).find("LengthOfEsca").text()) + "</td>" +

            "<td style='display:none;' class='clsSpecification' chk-data='" + $("#ddlSpecification").val() + "' >" + $.trim($(this).find("SpecififationType").text()) + "</td>" +
            "<td class='clsSpecificationText'>" + BindSpecificationString($.trim($(this).find("SpecificationRWidth").text()), $.trim($(this).find("SpecificationRDepth").text()),
                $.trim($(this).find("SpecificationPNo").text()), $.trim($(this).find("SpecificationPLength").text()), $.trim($(this).find("SpecificationPWidth").text()),
                $.trim($(this).find("SpecificationPDepth").text()), $.trim($(this).find("SpecificationTDiameter").text()), $.trim($(this).find("Specification").text())
            ) + "</td>" +


            "<td class='cls mode_E' style='text-align:center;' ><input type='text' class='clsEstimatedCost allownumericwithdecimal'  style='width:80px' " + ($.trim($(this).find("EstimatedAmount").text()) > 0 ? "disabled" : "") + " value='" + $.trim($(this).find("EstimatedAmount").text()) + "'  onkeyup='CalculateEstimation()' /><span class='glyphicon glyphicon-check imgEditEstimation' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditApplicantDetail(this);'></span></td>" +
            "<td class='clsApprove cls mode_R' style='text-align:center; display:none;'><input checked disabled type='checkbox' style='margin-top:15px'  class='chk cls mode_R' onClick='ButtonAction()'  /></td>" +
            // "<td class='cls mode_G' ><input type='text'  style='width:80px' class='allownumericwithdecimal clsRate' onkeyup='Calculate(this)' " + ($.trim($(this).find("ApprovedRate").text()) > 0 ? "disabled" : "") +" value='" + $.trim($(this).find("ApprovedRate").text()) + "' /></td>" +
            "<td class='clsCalAmount cls mode_G' ><input type='text'  style='width:80px' class='allownumericwithdecimal clsAmount' onkeyup='Calculate(this)' value='" + $.trim($(this).find("ApprovedAmount").text()) + "' /></td>" +

            "<td style='text-align:center' >" +
            "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditDetail(this);'></span>" +
            "</br></br><span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center;'  onClick='DeleteDetail(this);'></span> " +
            "</td> " +
            "</tr > ");
        //Estimation($.trim($(this).find("ServiceCurrStatus").text()));
    });

    AllowDecimal();
    //ApplyOption(mode);

}
function Document(xml, mode) {

    var html = "", imgCnt=0;
    $("#tblimg tr.docImg td").remove();

    $(xml).find("Table2").each(function () {

        var doctypeid = $.trim($(this).find("DocTypeID").text()); //alert(doctypeid);
        var docid = $.trim($(this).find("DocId").text());

        var isDelete = $("#flPic_" + doctypeid).attr('isDelete');  
        var isVisible = $("#flPic_" + doctypeid).attr('isVisible'); 

        if (mode == "N") {
                $("#flPic_32").prop('disabled', true);
        }

        html += "<tr><td style='width:80%; padding:5px 0px 0px 5px'>" + $.trim($(this).find("DocPath").text()) + "</td>";
        html += "<td style='display:none;' class='cslUrl'>" + $.trim($(this).find("MainUrl").text()) + "</td>";
        html += "<td style='display:none;' class='clsdocid'>" + $.trim($(this).find("ServDocId").text()) + "</td>";
        html += "<td style='width:30%; padding:5px 0px 5px 5px'>&nbsp;<img src='/Contents/image/paperclip.png' style='width:17px;height:15px; cursor:pointer; font-size:15px; top:5px !important;' onClick= 'ViewImg(this);' />"; //<img class='imgView' style= 'width:15px;height:15px;cursor:pointer; margin-left:10px; text-align:center;'  />
        html += "<span class='glyphicon glyphicon-trash imgDel' style='color:red; margin-left:10px; font-size:15px; cursor:pointer; text-align:center; top:5px !important; display:" + (isDelete == "False" ? "none" : "") + "'  onClick='DeleteImg(this);'></span></td ></tr>";
        $("#tblimg tr.img_" + doctypeid).append(html);
        html = "";

    });
    //ApplyOption(mode);
}
function OtherChargeDetail(xml, mode) {
    $("#tblOtherCharges tr.myData").remove();
    var html = "", Amt = 0;
    var $this = $('#tblOtherCharges .test_0');
    $parentTR = $this.closest('tr');
    $(xml).find("Table3").each(function () {

        $parentTR.after("<tr class='myData'>" +

            "<td style='display:none;' class='clsID' >" + $.trim($(this).find("ID").text()) + "</td>" +
           
            "<td style='display:none;' class='clsChargeID' >" + $.trim($(this).find("OtherChargeID").text()) + "</td>" +
            "<td>" + $.trim($(this).find("ChargeName").text()) + "</td>" +

            "<td class='clsOtherAmt' style='text-align:right'>" + $.trim($(this).find("Amount").text()) + "</td>" +

            "<td style='text-align:center' class='clstblEditOtherCharge'>" +
            "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; display:none; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditOtherChargeDetail(this);'></span>" +
            "<span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center; padding-left:10px'  onClick='DeleteOtherChargeDetail(this, " + $.trim($(this).find("ID").text()) +");'></span> " +
            "</td> " +
            "</tr> ");
    });
    GetCredentials();
    //ApplyOption(mode);
}

function BIND()
{
    if (UserType == 'MunicipalityUser')
    {

    }
}
function BindSpecificationString(RWidth, RDepth, PNo, PLength, PWidth, PDepth, TDiameter, Specification) {
    var retString = "";
    var spectiontxt = Specification;

    var SpecificationRWidth = RWidth;
    var SpecificationRDepth = RDepth;
    var SpecificationPNo = PNo;
    var SpecificationPLength =PLength;
    var SpecificationPWidth = PWidth;
    var SpecificationPDepth = PDepth;
    var SpecificationTDiameter = TDiameter;


    if (SpecificationRWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RWidth: " + SpecificationRWidth
    }
    if (SpecificationRDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RDepth: " + SpecificationRDepth
    }
    if (SpecificationPNo != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PNo: " + SpecificationPNo
    }
    if (SpecificationPLength != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PLength: " + SpecificationPLength
    }
    if (SpecificationPWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PWidth: " + SpecificationPWidth
    }
    if (SpecificationPDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PDepth: " + SpecificationPDepth
    }
    if (SpecificationTDiameter != "") {
        retString = (retString == "" ? "" : retString + ", ") + "TDiameter: " + SpecificationTDiameter
    }

    return spectiontxt + ", " + retString;
}
function DeleteImg(ID)
{
    var res = confirm('Are you sure want to delete image ?');
    if (res == true) {
        var docId = $(ID).closest('tr').find('.clsdocid').text();
        
        var E = "{DocID: '" + docId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/DeleteImg',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (responce) {
                var xmlDoc = $.parseXML(responce.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");

                var Result = $.trim($(Table).find("Result").text()); 
                if (Result == 1) $(ID).closest('tr').remove();
            }
        });
    }
}
function ViewImg(ID)
{
    var url = $(ID).closest('tr').find('.cslUrl').text(); //alert(url);

    var filename = url.substring(url.lastIndexOf('/') + 1);
    var fileExtension = filename.replace(/^.*\./, '');
    //alert(filename); alert(fileExtension);
    if (fileExtension == 'dwg')
    {
        var result = url.replace(/(^\w+:|^)\/\//, '');
        $('.clsDownload').attr('href', "http://"+result);
        $(".clsDownload")[0].click();
        //return false;
        //window.open(url, '_blank'); return false;
    }
    else {
        $('#imgview').attr('src', url);
        $('#dialog').modal('show');
    }

}
function download() {
    var filename = "SOMEONE.pdf";
    var text = "http://stage.wburbanservices.org/ApplicationFormExcel/RoadCutting/58_1_10_3.dwg";
    var element = document.createElement('a');
    element.setAttribute('href', text);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

//POPUP PROJECT DETAIL
function Bind_TypeofRoad_P(RoadTypeId) {
    if ($("#ddlTypeofSurface_P").val() != "") {
        var E = "{SurfaceID: '" + $("#ddlTypeofSurface_P").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_TypeofRoad',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                $('#ddlTypeofRoad_P').empty();
                $('#ddlTypeofRoad_P').append('<option value="">Select</option');

                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlTypeofRoad_P').append('<option value=' + item.RoadTypeID + '>' + item.RoadTypeName + '</option>');
                        });
                        if (RoadTypeId != "") { $('#ddlTypeofRoad_P').val(RoadTypeId); }
                    }
                }
            }
        });
    }
}
function Bind_Ward_P(WardId, LocationId) {
    var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
    if (MunicipalityID != "") {
        var E = "{MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_Ward',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlWard_P').empty();
                $('#ddlWard_P').append('<option value="">Select</option');

                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlWard_P').append('<option value=' + item.WardID + '>' + item.WardName + '</option>');
                        });
                        if (WardId != "") { $('#ddlWard_P').val(WardId); Bind_Location_P(LocationId); }
                    }
                }
            }
        });
    }
}
function Bind_Location_P(LocationId) {
    if ($("#ddlWard_P").val() != "") {
        var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
        var E = "{WardID: '" + $("#ddlWard_P").val() + "', MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_LocationByWard',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlLocation_P').empty();
                $('#ddlLocation_P').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlLocation_P').append('<option value=' + item.LocationID + '>' + item.LocationName + '</option>');
                        });
                    }
                    if (LocationId != "") $('#ddlLocation_P').val(LocationId);
                }
            }
        });
    }
}
function Add_PopUp() {
    //var reChk = Check_LatLong();  if (reChk == false) { return false;}
    //if ($("#ddlTypeofSurface").val() == "") { $("#ddlTypeofSurface").focus(); return false; }
    //if ($("#ddlTypeofRoad").val() == "") { $("#ddlTypeofRoad").focus(); return false; }
    //if ($("#txtLongitudeStart").val() == "") { $("#txtLongitudeStart").focus(); return false; }
    //if ($("#txtLongitudeEnd").val() == "") { $("#txtLongitudeEnd").focus(); return false; }
    //if ($("#txtLatitudeStart").val() == "") { $("#txtLatitudeStart").focus(); return false; }
    //if ($("#txtLatitudeEnd").val() == "") { $("#txtLatitudeEnd").focus(); return false; }

    //if ($("#ddlWard").val() == "") { $("#ddlWard").focus(); return false; }
    //if ($("#ddlLocation").val() == "") { $("#ddlLocation").focus(); return false; }
    //if ($("#txtStartofWork").val() == "") { $("#txtStartofWork").focus(); return false; }
    //if ($("#txtEndofWork").val() == "") { $("#txtEndofWork").focus(); return false; }
    //if ($("#txtEscavation").val() == "") { $("#txtEscavation").focus(); return false; }
    //if ($("#ddlSpecification").val() == "") { $("#ddlSpecification").focus(); return false; }

    var html = ""
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsDetailID' >" + $("#hdnDetailID_P").val() + "</td>"
        + "<td style='display:none;' class='clsStatus' chk-pservid='" + $("#hdnDetailID_P").val() + "' chk-insertmode='I'  chk-dtl='" + $("#hdnDetailID_P").val() + "'>" + 1 + "</td>"

        + "<td style='display:none;' class='clsTypService'  >" + $("#ddlTypeofSurface_P").val() + "</td>"
        + "<td>" + $("#ddlTypeofSurface_P option:selected").text() + "</td>"

        + "<td style='display:none;' class='clsTypRoad' chk-data='" + $("#ddlTypeofRoad_P").val() + "' >" + $("#ddlTypeofRoad_P").val() + "</td>"
        + "<td>" + $("#ddlTypeofRoad_P option:selected").text() + "</td>"

        + "<td>LS: <span class='clsLongStart'>" + $("#txtLongitudeStart_P").val() + "</span><br> LE: <span class='clsLongEnd'>" + $("#txtLongitudeEnd_P").val() + "</span></td>"
        + "<td>LS: <span class='clsLatStart'>" + $("#txtLatitudeStart_P").val() + "</span><br> LE: <span class='clsLatEnd'>" + $("#txtLatitudeEnd_P").val() + "</span></td>"

        + "<td style='display:none;' class='clsWard'  >" + $("#ddlWard_P").val() + "</td>"
        + "<td>" + ($("#ddlWard_P").val() == "" ? "" : $("#ddlWard_P option:selected").text()) + "</td>"

        + "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation_P").val() + "' >" + $("#ddlLocation_P").val() + "</td>"
        + "<td>" + ($("#ddlLocation_P").val() == "" ? "" : $("#ddlLocation_P option:selected").text()) + "</td>"

        + "<td class='clsLandMark'>" + $("#txtLandMark_P").val() + "</td>"
        + "<td class='clsBorough'>" + $("#txtBorough_P").val() + "</td>"
        + "<td class='clsStartofWork'>" + $("#txtStartofWork_P").val() + "</td>"
        + "<td class='clsEndofWork'>" + $("#txtEndofWork_P").val() + "</td>"
        + "<td class='clsEscavation'>" + $("#txtEscavation_P").val() + "</td>"

        + "<td style='display:none;' class='clsSpecification' chk-data='" + $("#ddlSpecification_P").val() + "' >" + $("#ddlSpecification_P").val() + "</td>"
        + "<td class='clsSpecificationText'>" + SpecificationString_P() + "</td>"

        + "<td class='cls mode_E' style='text-align:center'><input type='text' class='clsEstimatedCost allownumericwithdecimal' style='width:80px' onkeyup='CalculateEstimation()' " + ($("#txtEstimationCost_P").val() > 0 ? "disabled" : "") + " value='" + $("#txtEstimationCost_P").val() + "'  onkeyup='CalculateEstimation()' /></td>"
        + "<td class='clsApprove cls mode_R' style='text-align:center; display:none;'><input type='checkbox' checked disabled class='chk cls mode_R' onClick='ButtonAction()' /></td>"
        //+"<td class='cls mode_G'  ><input type='text'  style='width:80px' class='allownumericwithdecimal clsRate' onkeyup='Calculate(this)' /></td>"
        + "<td class='clsCalAmount cls mode_G' ><input type='text' onkeyup='Calculate(this)' style='width:80px' class='allownumericwithdecimal clsAmount' disabled value='" + $("#txtCalAmount_P").val() + "' /></td>"


        + "<td style='text-align:center' class='clsEditOption'>"
        + "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditDetail(this);'></span>"
        + "</br></br><span class='glyphicon glyphicon-trash' style='color:red; font-size:15px; cursor:pointer; text-align:center;'  onClick='DeleteDetail(this);'></span>"
        + "</td>" +
        html + "</tr>";


    $parentTR.after(html);

    $("#ddlTypeofSurface_P").val(''); $("#ddlTypeofRoad_P").val('');
    $("#txtLongitudeStart_P").val(''); $("#txtLongitudeEnd_P").val(''); $("#txtLatitudeStart_P").val(''); $("#txtLatitudeEnd_P").val('');
    $("#ddlWard_P").val(''); $("#ddlLocation_P").val(''); $("#txtStartofWork_P").val('');
    $("#txtLandMark_P").val(''); $("#txtBorough_P").val('');
    $("#txtEndofWork_P").val(''); $("#txtEscavation_P").val(''); $("#ddlSpecification_P").val('');

    $("#RWidth_P").val(''); $("#RDepth_P").val(''); $("#PNo_P").val(''); $("#PLength_P").val('');
    $("#PWidth_P").val(''); $("#PDepth_P").val(''); $("#TDiameter_P").val('');

    $("#hdnDetailID_P").val(''); $("#hdnStatus_P").val('');

    $('.dtl').css('display', 'none');
    $('#dialog_detail').modal('hide');
    ApplyOption(mode);
}
function EditApplicantDetail(ID)
{
     $("#hdnDetailID_P").val($(ID).closest('tr').find('.clsDetailID').text());
     $("#hdnStatus_P").val($(ID).closest('tr').find('.clsStatus').text());

     $("#ddlTypeofSurface_P").val($(ID).closest('tr').find('.clsTypService').text()); Bind_TypeofRoad_P($(ID).closest('tr').find('.clsTypRoad').text());
     $("#ddlTypeofRoad_P").val($(ID).closest('tr').find('.clsTypRoad').text());

     $("#txtLongitudeStart_P").val($(ID).closest('tr').find('.clsLongStart').text());
     $("#txtLongitudeEnd_P").val($(ID).closest('tr').find('.clsLongEnd').text());
     $("#txtLatitudeStart_P").val($(ID).closest('tr').find('.clsLatStart').text());
     $("#txtLatitudeEnd_P").val($(ID).closest('tr').find('.clsLatEnd').text());

     Bind_Ward_P($(ID).closest('tr').find('.clsWard').text(), $(ID).closest('tr').find('.clsLocation').text()); //Bind_Location_P($(ID).closest('tr').find('.clsLocation').text());
     $("#txtLandMark_P").val($(ID).closest('tr').find('.clsLandMark').text());
     $("#txtBorough_P").val($(ID).closest('tr').find('.clsBorough').text());

     $("#txtStartofWork_P").val($(ID).closest('tr').find('.clsStartofWork').text());
     $("#txtEndofWork_P").val($(ID).closest('tr').find('.clsEndofWork').text());
     $("#txtEscavation_P").val($(ID).closest('tr').find('.clsEscavation').text());
     $("#ddlSpecification_P").val($(ID).closest('tr').find('.clsSpecification').text());

     var clsSpecificationText = $(ID).closest('tr').find('.clsSpecificationText').text(); //alert(clsSpecificationText);
     var RWidth = '', RDepth = '', PNo = '', PLength = '', PWidth = '', PDepth = '', TDiameter = '';
     if (clsSpecificationText != "") {
         var ls = clsSpecificationText.split(','); //alert(ls); alert(ls.length);
         if (ls.length > 0) {
             //alert(ls);
             for (var i = 0; i < ls.length; i++) {
                 var keyValue = ls[i].split(":");
                 if (keyValue[0].trim() == 'RWidth') { RWidth = keyValue[1].trim(); $("#RWidth_P").val(RWidth); (RWidth == "" ? $("#RWidth_P").css('display', 'none') : $("#RWidth_P").css('display', '')) }

                 if (keyValue[0].trim() == 'RDepth') { RDepth = keyValue[1].trim(); $("#RDepth_P").val(RDepth); (RDepth == "" ? $("#RDepth_P").css('display', 'none') : $("#RDepth_P").css('display', '')) }

                 if (keyValue[0].trim() == 'PNo') { PNo = keyValue[1].trim(); $("#PNo_P").val(PNo); (PNo == "" ? $("#PNo_P").css('display', 'none') : $("#PNo_P").css('display', '')) }

                 if (keyValue[0].trim() == 'PLength') { PLength = keyValue[1].trim(); $("#PLength_P").val(PLength); (PLength == "" ? $("#PLength_P").css('display', 'none') : $("#PLength_P").css('display', '')) }

                 if (keyValue[0].trim() == 'PWidth') { PWidth = keyValue[1].trim(); $("#PWidth_P").val(PWidth); (PWidth == "" ? $("#PWidth_P").css('display', 'none') : $("#PWidth_P").css('display', '')) }

                 if (keyValue[0].trim() == 'PDepth') { PDepth = keyValue[1].trim(); $("#PDepth_P").val(PDepth); (PDepth == "" ? $("#PDepth_P").css('display', 'none') : $("#PDepth_P").css('display', '')) }

                 if (keyValue[0].trim() == 'TDiameter') { TDiameter = keyValue[1].trim(); $("#TDiameter_P").val(TDiameter); (TDiameter == "" ? $("#TDiameter_P").css('display', 'none') : $("#TDiameter_P").css('display', '')) }

             }
         }
     }

     $("#txtEstimationCost_P").val($(ID).closest('tr').find('.clsEstimatedCost').val());
     $("#txtCalAmount_P").val($(ID).closest('tr').find('.clsAmount').val());

     var dtlID = $("#hdnDetailID_P").val(); 
     if (dtlID != "") {
         //$('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").find("td[chk-insertmode='I']").closest('tr').remove();
         //var l = $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").closest('tr').find("td[chk-insertmode='I']").length; alert(l);
         $('#tbl tr.myData').find("td[chk-pservid='" + dtlID + "']").closest('tr').find("td[chk-insertmode='I']").closest('tr').remove();
     }


     $('#dialog_detail').modal('show');
}
function Show_ddlSpecification_P() {
    if ($("#ddlSpecification_P").val() != "") {
        var E = "{SpecificationID: '" + $("#ddlSpecification_P").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Show_Specification',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        var SpecificationRWidth = item.SpecificationRWidth;
                        var SpecificationRDepth = item.SpecificationRDepth;
                        var SpecificationPNo = item.SpecificationPNo;
                        var SpecificationPLength = item.SpecificationPLength;
                        var SpecificationPWidth = item.SpecificationPWidth;
                        var SpecificationPDepth = item.SpecificationPDepth;
                        var SpecificationTDiameter = item.SpecificationTDiameter;

                        if (SpecificationRWidth == true) $("#RWidth_P").css('display', ''); else $("#RWidth_P").css('display', 'none');
                        if (SpecificationRDepth == true) $("#RDepth_P").css('display', ''); else $("#RDepth_P").css('display', 'none');
                        if (SpecificationPNo == true) $("#PNo_P").css('display', ''); else $("#PNo_P").css('display', 'none');
                        if (SpecificationPLength == true) $("#PLength_P").css('display', ''); else $("#PLength_P").css('display', 'none');
                        if (SpecificationPWidth == true) $("#PWidth_P").css('display', ''); else $("#PWidth_P").css('display', 'none');
                        if (SpecificationPDepth == true) $("#PDepth_P").css('display', ''); else $("#PDepth_P").css('display', 'none');
                        if (SpecificationTDiameter == true) $("#TDiameter_P").css('display', ''); else $("#TDiameter_P").css('display', 'none');
                    });
                }
            }
        });
    }
}
function SpecificationString_P() {
    var retString = "";
    var spectiontxt = $("#ddlSpecification_P option:selected").text();

    var SpecificationRWidth = $.trim($("#RWidth_P").val() == "" ? "0" : $("#RWidth_P").val());
    var SpecificationRDepth = $.trim($("#RDepth_P").val() == "" ? "0" : $("#RDepth_P").val());
    var SpecificationPNo = $.trim($("#PNo_P").val() == "" ? "0" : $("#PNo_P").val());
    var SpecificationPLength = $.trim($("#PLength_P").val() == "" ? "0" : $("#PLength_P").val());
    var SpecificationPWidth = $.trim($("#PWidth_P").val() == "" ? "0" : $("#PWidth_P").val());
    var SpecificationPDepth = $.trim($("#PDepth_P").val() == "" ? "0" : $("#PDepth_P").val());
    var SpecificationTDiameter = $.trim($("#TDiameter_P").val() == "" ? "0" : $("#TDiameter_P").val());

    if (SpecificationRWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RWidth: " + SpecificationRWidth
    }
    if (SpecificationRDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RDepth: " + SpecificationRDepth
    }
    if (SpecificationPNo != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PNo: " + SpecificationPNo
    }
    if (SpecificationPLength != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PLength: " + SpecificationPLength
    }
    if (SpecificationPWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PWidth: " + SpecificationPWidth
    }
    if (SpecificationPDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PDepth: " + SpecificationPDepth
    }
    if (SpecificationTDiameter != "") {
        retString = (retString == "" ? "" : retString + ", ") + "TDiameter: " + SpecificationTDiameter
    }

    return spectiontxt + ", " + retString;
}

//MODE WORK
function ApplyOption(mode)
{
    if (mode != null && mode != "") {

        if (mode == 'R') {

            $(".mode_G").addClass('hide');
            $(".mode_E").css('display', '');

            //ButtonAction();
        }
        else if (mode == 'V') {
            $(".mode_G").css('display', ''); $(".imgEditEstimation").css('display', 'none'); $(".clstblOtherCharge").css('display', 'none');
            $("#btnSave").css('display', 'none'); $("#btnRefresh").css('display', 'none');
            $("#btnAdd").addClass('hide');
            $(".mode_E").css('display', '');

            $("#btnReject").css('display', '');
            $(".chk").prop('disabled', true);
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".clsOption").addClass('hide');
            $(".imgView").addClass('hide');

        }
        else if (mode == 'G') {
            Execute_G();
            $("#ddlMunicipality").prop('disabled', true); $("#ddlPurpose").prop('disabled', true); $("#txtProjectName").prop('disabled', true);
            $(".mode_G").css('display', '');
            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');
            $(".mode_E").css('display', '');

            $("#btnReject").css('display', '');
            $(".chk").prop('disabled', true);
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").addClass('hide');
        }
        else if (mode == 'B') {
            $("#ddlMunicipality").prop('disabled', true); $("#ddlPurpose").prop('disabled', true); $("#txtProjectName").prop('disabled', true);
            $(".mode_G").css('display', ''); $(".imgEditEstimation").css('display', 'none');
            $("#btnSave").css('display', ''); 
            $("#btnAdd").addClass('hide');
            $(".mode_E").css('display', '');

            $(".clstblEditOtherCharge").css('display', 'none');
            var estOherchrgAmt = $(".clsGrossEstChargeAmt").text();
            parseFloat(estOherchrgAmt) > 0 ? $(".clstblOtherCharge").css('display', '') : $(".clstblOtherCharge").css('display', 'none');

            $("#btnReject").css('display', '');
            $(".chk").prop('disabled', true);
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").addClass('hide');
        }
        else if (mode == 'A') {
            $(".mode_G").css('display', 'none');
            $(".mode_E").css('display', 'none');

            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');

            $(".chk").addClass('hide');
            $(".imgDel").addClass('hide'); $(".imgEdit").addClass('hide'); $(".imgView").addClass('hide');
        }
        else if (mode == 'C') {
            $("#ddlMunicipality").prop('disabled', true); $("#ddlPurpose").prop('disabled', true); $("#txtProjectName").prop('disabled', true);
            $(".flimg").prop('disabled', true); 
            $(".imgEditEstimation").css('display', 'none');
            var estOherchrgAmt = $(".clsGrossEstChargeAmt").text();


            $(".clstblEditOtherCharge").css('display', 'none');
            parseFloat(estOherchrgAmt) > 0 ? $(".clstblOtherCharge").css('display', '') : $(".clstblOtherCharge").css('display', 'none');

            $(".mode_G").css('display', 'none');

            //$(".mode_R").css('display', 'none');
            //$(".mode_E").css('display', 'none');
            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');


            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").css('display', '');
            ButtonAction();
        }
        else if (mode == 'E') {
            $("#ddlMunicipality").prop('disabled', true); $("#ddlPurpose").prop('disabled', true); $("#txtProjectName").prop('disabled', true);
            $(".clstblOtherCharge").css('display', ''); $(".imgEditEstimation").css('display', 'none');
            $("#btnSave").css('display', '');
            $(".mode_E").css('display', '');

            $(".mode_G").css('display', 'none');

            $("#btnAdd").addClass('hide');
            //$(".chk").addClass('hide');
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").css('display', '');
        }
        else if (mode == 'I') {
            $(".mode_G").css('display', '');
            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');
            $(".mode_E").css('display', '');

            $("#btnReject").css('display', '');
            $(".chk").prop('disabled', true);
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").addClass('hide');
        }
        else if (mode == 'T') {
            $(".mode_G").addClass('hide'); $(".mode_E").css('display', 'none');
            $("#btnSave").css('display', ''); $("#btnRefresh").css('display', '');
            $(".clsOption").addClass('hide');
        }
        else { $(".cls").addClass('hide'); $(".clstblOtherCharge").addClass('hide');}
    } 
    else {
        $(".cls").addClass('hide');
        $(".clstblOtherCharge").addClass('hide');
    }
}
function Estimation(ServiceCurrStatus)
{
    if (ServiceCurrStatus == 1 || ServiceCurrStatus == 2)
    {
        $(".mode_E").css('display', 'none');
    }

}
function Calculate(ID)
{
    //var length = $(ID).closest('tr').find('.clsEscavation').text();
    //var rate = $(ID).val() == "" ? 0 : $(ID).val();
    //var cal = parseFloat(rate) * parseFloat(length == "" ? 0 : length);

    //$(ID).closest('tr').find('.clsAmount').val(cal);

    var Total = 0, isCnt=0;
    $(".clsAmount").each(function () {
        var amt = $(this).val() == "" ? 0 : $(this).val();
        //Total = parseFloat(Total) + parseFloat(amt);
        if (amt == 0)
            isCnt = 1;
    });
    if (isCnt > 0)
    {
        $("#btnSave").prop('disabled', true);
    }
    else
    {
        $("#btnSave").prop('disabled', false);
    }
    //$('.clsTotal').text(Total);

    CalculateFinal();
}
function ButtonAction()
{
    var total = 0;
    $('#tbl tbody tr.myData').each(function () {
        var clsStatus = $(this).find('.clsStatus').text();
        if (clsStatus > 0)
        {
            var isChk = $(this).find('.chk').prop('checked'); //alert(isChk);
            if (isChk == false)
                total = 1;
        }

    });
    //alert(total);
    if (total > 0)
        $('#btnSave').prop('disabled', true);
    else
        $('#btnSave').prop('disabled', false);

}
function Check_LatLong()
{
    //WEST BENGAL LONG & LAT
    //LOG START 21.25 TO 27.13
    //LAT START 85.5 TO + 89.5
   
        var longst = $('#txtLongitudeStart').val();
        var longed = $('#txtLongitudeEnd').val(); 
        if (parseFloat(longst) < 21.25 || parseFloat(longst) > 27.13)
        {
            alert('Sorry ! Longitude Start should be between ' + 21.25 + ' and ' + 27.13); return false;
        }
        else if (parseFloat(longed) < 21.25 || parseFloat(longed) > 27.13)
        {
            alert('Sorry ! Longitude End should be between ' + 21.25 + ' and ' + 27.13); return false;
        }

        var latst = $('#txtLatitudeStart').val();
        var lated = $('#txtLatitudeEnd').val();

        if (parseFloat(latst) < 85.5 || parseFloat(latst) > 89.5) 
        {
            alert('Sorry ! Latitude Start should be between ' + 85.5 + ' and ' + 89.5); return false;
        }
        if (parseFloat(lated) < 85.5 || parseFloat(lated) > 89.5) 
        {
            alert('Sorry ! Latitude End should be between ' + 85.5 + ' and ' + 89.5); return false;
        }
        return true;
    
}
function Bind_Designation()
{
    if ($("#ddlAction").val() != "") {
        var E = "{ActionID: '" + $("#ddlAction").val() + "', ApplicationID: '" + ApplicationId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_Designation',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlDesignation').empty();
                $('#ddlDesignation').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlDesignation').append('<option value=' + item.DesignationID + '>' + item.DesignationName + '</option>');
                        });
                    }
                }
            }
        });
    }
    else
    {
        $('#ddlDesignation').empty();
        $('#ddlDesignation').append('<option value="">Select</option');
    }
}
function CalculateFinal()
{
    var Total = 0;
    $(".clsAmount").each(function () {
        var amt = ($(this).val() == "" || $(this).val() == ".") ? 0 : $(this).val();
        Total = parseFloat(Total) + parseFloat(amt);
    });

    $('#tblOtherCharges').find('.clsOtherAmt').each(function () {
        var amt = ($(this).text() == "" || $(this).text() == ".") ? 0 : $(this).text(); 
        Total = parseFloat(Total) + parseFloat(amt);
    });

    $('.clsGrossEstChargeAmt').text(Total.toFixed(2));
}

function GetCredentials() {

    if (mode != null && mode != "") {
       
        var E = "{Mode: '" + mode + "', ApplicationID: '" + ApplicationId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/GetCredentials',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            async: false,
            success: function (responce) {
                var xmlDoc = $.parseXML(responce.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");

                var EstimateAmount = $.trim($(Table).find("EstimateAmount").text());
                var AddEstimateExtraCharge = $.trim($(Table).find("AddEstimateExtraCharge").text());
                var FinalAmount = $.trim($(Table).find("FinalAmount").text());
                var ProjDtlEditButton = $.trim($(Table).find("ProjDtlEditButton").text());
                var EditBtnEstimateExtraChrg = $.trim($(Table).find("EditBtnEstimateExtraChrg").text());
                var EstimatedCostOnOff = $.trim($(Table).find("EstimatedCostOnOff").text());
                var FinalCostOnOff = $.trim($(Table).find("FinalCostOnOff").text());
                var SaveBtnOnOff = $.trim($(Table).find("SaveBtnOnOff").text());
                var ForwardAssign = $.trim($(Table).find("ForwardAssign").text());

                $("#ddlMunicipality").prop('disabled', true); $("#ddlPurpose").prop('disabled', true); $("#txtProjectName").prop('disabled', true);

                if (parseInt(EstimateAmount) == 0) {
                    $(".mode_E").css('display', 'none');
                }
                if (parseInt(AddEstimateExtraCharge) == 0) {
                    $(".clstblOtherCharge").css('display', 'none');
                }
                if (parseInt(FinalAmount) == 0) {
                    $(".mode_G").css('display', 'none');
                }
                if (parseInt(ProjDtlEditButton) == 0) {
                    $(".clsEditOption").css('display', 'none');
                }
                if (parseInt(EditBtnEstimateExtraChrg) == 0) {
                    $(".clstblEditOtherCharge").css('display', 'none');
                }
                if (parseInt(EstimatedCostOnOff) == 0) {
                    $(".clsEstimatedCost").prop('disabled', true);
                }
                if (parseInt(FinalCostOnOff) == 0) {
                    $(".clsAmount").prop('disabled', true);
                }
                if (parseInt(SaveBtnOnOff) == 0) {
                    $("#btnSave").prop('disabled', true);
                }
                if (parseInt(ForwardAssign) == 0) {
                    $(".clsOption").css('display', 'none');
                }
            }

        });
    }
    else {
      
        $(".mode_E").css('display', 'none');
        $(".clstblOtherCharge").css('display', 'none');
        $(".mode_G").css('display', 'none');
        $(".clsEditOption").css('display', '');
        $(".clsOption").css('display', 'none');
    }
}
function Execute() {
    var E = "{ApplicationID: '" + ApplicationId + "'} ";

    $.ajax({
        type: "POST",
        url: '/OtherServices/ApplicationForm/Execute',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            Load(ApplicationId, mode);
        }
    });
}

//PHOTO
function Photo(ID)
{
    var Id = $(ID).attr('id');
    var maxqty = $(ID).attr('maxQuantity'); var restmaxqty = $(ID).attr('maxQuantity');
    var doctid = $(ID).attr('doctypeid'); 
    var l = $('#tblimg tr.img_' + doctid).find('tr').length; 
    maxqty = maxqty - l;

    if (maxqty > 0) {
        var totalFiles = document.getElementById(Id).files.length;
        if (totalFiles > maxqty) {
            alert('Sorry ! You can select only ' + restmaxqty + " files.");
            $('#' + Id).val("");
            return false;
        }
    }
    else {
        alert('Sorry ! You can not select more than ' + restmaxqty + " files.");
        $('#' + Id).val("");
        return false;
    }


}

