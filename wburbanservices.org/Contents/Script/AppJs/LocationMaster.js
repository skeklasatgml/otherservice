﻿
 var app = angular.module("myapp", []);
app.controller("mycontroller", function ($scope, $window,$http) {
    angular.element("#txtLocationName").focus();
    $scope.btnSave = "Save";
    $scope.locations = [];
    $scope.municipalities = [];
    $scope.wards = [];

    $scope.Load = function () {
        Get_AllLocation();
        dll_Municipality();
    }

    $scope.Save_Update = function () {
        var LocationID = $scope.hdnLocationID;
        var LocationName = $scope.txtLocationName;
        var MunicipalityID = $scope.ddlMunicipality;
        var WardID = $scope.ddlWard;
        //alert(MunicipalityID);
        //return false;
        if (LocationName == "")
        { alert("Please write Location Name"); angular.element("#txtLocationName").focus(); return false; }
        if (MunicipalityID == "" || MunicipalityID == undefined)
        { alert("Please Select Municipality Name"); angular.element("#ddlMunicipality").focus(); return false; }
        if (WardID == "" || WardID == undefined)
        { alert("Please Select Ward Number"); angular.element("#ddlWard").focus(); return false; }
        if (LocationID == "")
        { Save(LocationName, WardID, MunicipalityID); } else { Update(LocationID, LocationName, WardID, MunicipalityID); }
    }

    $scope.Municipality_Change = function () {
        var MunicipalityID = $scope.ddlMunicipality;
        // alert(MunicipalityID);
        if (MunicipalityID !== 0) {
            dll_Ward(MunicipalityID);
        } else { alert("select municipality"); }

    }

    $scope.Edit = function (location) {
        $scope.hdnLocationID = location.MunicipalityID;
        $scope.txtLocationName = location.LocationName;
        $scope.ddlMunicipality = location.MunicipalityID;
        dll_Ward(location.MunicipalityID);
        $scope.ddlWard = location.WardID;
        $scope.ddlWard.label = location.WardName;
        $scope.btnSave = "Update";
    }

    $scope.Delete = function (LocationID) {
        if (confirm("Are you sure want to Delete This ???")) {

            $http({
                method: "post",
                url: "/LocationMaster/Delete_Location"
            }).then(function (response) {
                alert(response.data.Message);
                if (response.data.Status == 200)
                { Get_AllLocation(); }
            }, function () {
                alert("Error occur");
            })
        }
    }

    $scope.Cancel = function () {
        $window.location.reload();
    }

    function Save(LocationName, WardID, MunicipalityID) {
        $http({
            method: "post",
            url: "/LocationMaster/Save_Location",
            data: { LocationName: LocationName, WardID: WardID, MunicipalityID: MunicipalityID }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200)
            { Get_AllLocation(); }
        }, function () {
            alert("Error occur");
        })
    }
    function Update(LocationID, LocationName, WardID, MunicipalityID) {
        $http({
            method: "post",
            url: "/LocationMaster/Update_Location",
            data: { LocationID: LocationID, LocationName: LocationName, WardID: WardID, MunicipalityID: MunicipalityID }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200)
            { Get_AllLocation(); }
        }, function () {
            alert("Error occur");
        })
    }
    function Get_AllLocation() {
        $scope.hdnLocationID = "";
        $scope.txtLocationName = "";
        $scope.btnSave = "Save";
        angular.element("#txtLocationName").focus();
        $http({
            method: "post",
            url: "/LocationMaster/Get_AllLocation"
        }).then(function (response) {
            $scope.locations = response.data.Data;
            //  alert(response.data.Message);
        }, function () {
            alert("Error occur");
        })
    }
    function dll_Municipality() {
        $http({
            method: "post",
            url: "/LocationMaster/ddl_Municipality"
        }).then(function (response) {
            $scope.municipalities = response.data.Data;
            //  alert(response.data.Message);
        }, function () {
            alert("Error occur");
        })
    }
    function dll_Ward(MunicipalityID) {
        if (MunicipalityID != null) {
            $http({
                method: "post",
                url: "/LocationMaster/ddl_Ward",
                data: { MunicipalityID: MunicipalityID }
            }).then(function (response) {
                $scope.wards = response.data.Data;
            }, function () {
                alert("Error occur");
            })
        }
    }
})
