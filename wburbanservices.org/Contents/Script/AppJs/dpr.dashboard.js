﻿$(document).ready(function () {
    var rClone = [];
    var constants = {
        colors: []
    };
    var dashBoard = function ($$filterBox, $$constants) {
        var reportPayloads = [
            {
                reportCode: "Rpt_ProposalDetailsReport",
                payload: null,
                url:"/Modules/Dpr/geturl_Rpt_ProposalDetailsReport"
            }, {
                reportCode: "Rpt_AverageTimeDpr",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_AverageTimeDpr"
            }, {
                reportCode: "Rpt_ProjectStatusSummary",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_ProjectStatusSummary"
            }, {
                reportCode: "Rpt_FundreleaseFinYearWise",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_FundreleaseFinYearWise"
            }, {
                reportCode: "Rpt_YearWiseFundReq",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_YearWiseFundReq"
            }, {
                reportCode: "Rpt_SchemeCatgoryWiseProjectDtls",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_SchemeCatgoryWiseProjectDtls"
            }, {
                reportCode: "Rpt_FundReleaseSummeryReport",
                payload: null,
                url: "/Modules/Dpr/geturl_Rpt_FundReleaseSummeryReport"
            },
            {
                reportCode: "RPT_SynopsisOfRSP",
                payload: null,
                url: "/Modules/dpr/SynopsisOfRSP"
            },
            {
                reportCode: "RPT_RSPDraft",
                payload: null,
                url: "/Modules/dpr/RSPDraft"
            },
            {
                reportCode: "RPT_SchemeWiseNoOfUC",
                payload: null,
                url: "/Modules/dpr/SchemeWiseUC"
            },
            {
                reportCode: "RPT_YearWiseNoOfUC",
                payload: null,
                url: "/Modules/dpr/YearWiseUC"
            },
            {
                reportCode: "RPT_TotalNoOfPondRoadLightPark",
                payload: null,
                url: "/Modules/dpr/CategoryWiseCount"
            },
            {
                reportCode: "RPT_PhysicalProgressOfProj",
                payload: null,
                url: "/Modules/dpr/PhysicalProgressOfProject"
            },
            {
                reportCode: "RPT_ProjApprvInFavOfMuni",
                payload: null,
                url: "/Modules/dpr/RPT_ProjApprvInFavOfMuni"
            },
            {
                reportCode: "RPT_ULBOrDAWiseAAFS",
                payload: null,
                url: "/Modules/dpr/RPT_ULBOrDAWiseAAFS"
            },
            {
                reportCode: "RPT_FundReleaseSummary",
                payload: null,
                url: "/Modules/dpr/RPT_FundReleaseSummary"
            },
            {
                reportCode: "RPT_ProjWiseFundReleased",
                payload: null,
                url: "/Modules/dpr/RPT_ProjWiseFundReleased"
            },
            {
                reportCode: "RPT_ProjWiseFundReleaseDetail",
                payload: null,
                url: "/Modules/dpr/RPT_ProjWiseFundReleaseDetail"
            }, 
            {
                reportCode: "RPT_AAFSSummary",
                payload: null,
                url: "/Modules/dpr/RPT_AAFSSummary"
            },
            {
                reportCode: "RPT_PhasingSummary",
                payload: null,
                url: "/Modules/dpr/RPT_PhasingSummary"
            },
            {
                reportCode: "RPT_DPRSubmittedButAAFSNotIssued",
                payload: null,
                url: "/Modules/dpr/RPT_DPRSubmittedButAAFSNotIssued"
            },
            {
                reportCode: "RPT_DPRSubmittedButAAFSNotIssuedExcel1",
                payload: null,
                url: "/Modules/dpr/RPT_DPRSubmittedButAAFSNotIssuedExcel1"
            },
            {
                reportCode: "RPT_DPRSubmittedWithoutAnySubScheme",
                payload: null,
                url: "/Modules/dpr/RPT_DPRSubmittedWithoutAnySubScheme"
            },
            {
                reportCode: "RPT_DPRSubmittedWithoutAnySubSchemeExcel",
                payload: null,
                url: "/Modules/dpr/RPT_DPRSubmittedWithoutAnySubSchemeExcel"
            },
            {
                reportCode: "RPT_InstallmentClaimedButFundNotReleased",
                payload: null,
                url: "/Modules/dpr/RPT_InstallmentClaimedButFundNotReleased"
            },
        ];

        var getdataFromServer = function (objct, callback) {
            $.ajax({
                url: "/Modules/Dpr/GetDashboardDataMain",
                method: "Post",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(objct),
                success: function (e) {
                    callback(e);
                },
                error: function (e) {
                    callback({
                        Data: null,
                        Message: e.responseText || e.statusText,
                        Status: e.status
                    });
                }
            });
        };
        var bindEvents = function () {
            $$filterBox.$onChangeFilterData = function (filterData) {
                var payload = formatDataForServePayload(filterData);

                //apply report payload
                reportPayloads.forEach(function (t) {
                    if (["Rpt_ProposalDetailsReport", "Rpt_AverageTimeDpr", "Rpt_ProjectStatusSummary", "Rpt_SchemeCatgoryWiseProjectDtls", "Rpt_FundReleaseSummeryReport", "RPT_SynopsisOfRSP", "RPT_RSPDraft", "RPT_SchemeWiseNoOfUC", "RPT_YearWiseNoOfUC", "RPT_TotalNoOfPondRoadLightPark", "RPT_PhysicalProgressOfProj", "RPT_ProjApprvInFavOfMuni", "RPT_ULBOrDAWiseAAFS", "RPT_FundReleaseSummary", "RPT_ProjWiseFundReleased", "RPT_ProjWiseFundReleaseDetail", "RPT_AAFSSummary", "RPT_PhasingSummary", "RPT_DPRSubmittedButAAFSNotIssued", "RPT_DPRSubmittedButAAFSNotIssuedExcel1", "RPT_DPRSubmittedButAAFSNotIssuedExcel", "RPT_DPRSubmittedWithoutAnySubScheme","RPT_DPRSubmittedWithoutAnySubSchemeExcel", "RPT_InstallmentClaimedButFundNotReleased"].indexOf(t.reportCode) != -1) {
                        t.payload = payload;
                    }
                });
                getdataFromServer(payload, function (r) {
                    if (r.Status === 200) {
                        DataAnalysis.onDataLoaded(r.Data);
                        rClone = r.Data;
                    } else {
                        alertify.error(r.Message);
                    }
                });
                FinancialYearWiseData.onChangeFilter(filterData);
            };
            $(".municipality-wise-data").on('change', ".top-no-of-ulb", function () {
                $$filterBox.$reloadFilter();
            });
            $(".municipality-wise-data").on('change', ".data-by-radio", function () {
                $$filterBox.$reloadFilter();
            });
            $(".sceme-wise-pie-box").on('change', ".data-by-radio", function () {
                $$filterBox.$reloadFilter();
            });
            $(".dash-reports-modal-box .dash-reports .report").on('click', function (ee) {
                var evObject = $(this);
                var reportKey = $(this).attr("data-key");
                var report = reportPayloads.filter(function (r) {
                    return r.reportCode == reportKey
                });
                evObject.siblings().removeClass('active');
                evObject.addClass('active');
                if (reportKey == 'UnReleasedInstallmentsExcel') {
                    window.location.href = "/Modules/Dpr/Excel";
                    //return false;
                }
                else if (reportKey == 'RPT_DPRSubmittedButAAFSNotIssuedExcel1') {
                    window.location.href = "/Modules/Dpr/RPT_DPRSubmittedButAAFSNotIssuedExcel?MunicipalityIds=" + report[0].payload.MunicipalityIds + "&TimeSpanFlag=" + report[0].payload.TimeSpanFlag + "&FromDate=" + report[0].payload.FromDate + "&ToDate=" + report[0].payload.ToDate + "&SchemeTypeId=" + report[0].payload.ScemeTypeId;
                }
                else if (reportKey == 'RPT_DPRSubmittedWithoutAnySubSchemeExcel') {
                    window.location.href = "/Modules/Dpr/RPT_DPRSubmittedWithoutAnySubSchemeExcel?MunicipalityIds=" + report[0].payload.MunicipalityIds + "&TimeSpanFlag=" + report[0].payload.TimeSpanFlag + "&FromDate=" + report[0].payload.FromDate + "&ToDate=" + report[0].payload.ToDate + "&SchemeTypeId=" + report[0].payload.ScemeTypeId;
                }
                else {
                    if (report.length <= 0) {
                        alertify.error('Report not found');
                        return;
                    } else {
                        report = report[0];
                    }

                    $.ajax({
                        url: report.url,
                        method: 'post',
                        data: JSON.stringify(report.payload),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (e) {
                            if (e.Status === 200 && e.Data) {
                                var target = evObject.siblings(".redirect-report-link");
                                target.attr('href', e.Data.substring(1, e.Data.length));
                                target[0].click();
                            } else {
                                alertify.error(e.Message);
                            }
                        }, error: function (e) { }
                    });
                }
            });
            $(".dash-block").on('click', function () {
                var dataVal = $(this).attr("data-val");
                var DateType = "";
                if (dataVal == "submitted") {
                    DateType = "S";
                }
                if (dataVal == "approved") {
                    DateType = "A";
                }
                if (dataVal == "returned") {
                    DateType = "RT";
                }
                if (dataVal == "released") {
                    DateType = "RL";
                }
                var payload = $$filterBox._filteredData;
                var obj = {
                    status: dataVal,
                    municipalities: payload.municipalityFilter.municipalities,
                    timeSpan: payload.timeSpanFilter.timeSpan.val,
                    fromDate: payload.timeSpanFilter.fromDate,
                    toDate: payload.timeSpanFilter.toDate
                };
                var base64 = btoa(JSON.stringify(obj));
                var urlEncoded = encodeURIComponent(base64);
                var url = "/Modules/Dpr/DprListUi?filter=" + urlEncoded + "&isCalledFromDashBoard=1&DateType=" + DateType;
                var target = $(this).siblings(".redirect-report-link").attr('href', url);
                target[0].click();
            });
            $(".filtr-modal-close").on('click', function () {
                $(".change-filter-modal").modal('hide');
            });
        };
        var formatDataForServePayload = function (filterData) {
            return {
                MunicipalityIds: filterData.municipalityFilter.municipalities.map(function (t) {
                    return t.id;
                }),
                TimeSpanFlag: filterData.timeSpanFilter.timeSpan.val,
                FromDate: filterData.timeSpanFilter.fromDate,
                ToDate: filterData.timeSpanFilter.toDate,
                AuthorityTypeId: filterData.municipalityFilter.AuthorityTypeId,
                SchemeTypeId: filterData.SchemeTypeId,
                SchemeCategoryId: filterData.SchemeCategoryId
            }
        };
        var DataAnalysis = {
            populateCategoryWisePieChart: function (r, SchemeId)
            {
                var parentContainer = ".category-wise-pie-box";
                var showBy = $(parentContainer).find(".data-by-radio:checked").val();
                var source = r
                    .Apporveds
                    .map(function (v) {
                        return {
                            DprId: v.DprId,
                            ApprovedAmount: v.ApprovedAmount,
                            SchemeId: v.CategoryId,
                            SchemeName: v.CategoryName,
                            OriginalSchemeId: v.ScemeTypeId
                        };
                    });
                var array = [];
                if (source.length > 0) {
                   // array = alasql('Select * from ? WHERE OriginalSchemeId=' + SchemeId, [source]);
                    array = alasql('Select sum(ApprovedAmount) as ApprovedAmount,SchemeName from ? WHERE OriginalSchemeId=' + SchemeId+' group by SchemeId,SchemeName', [source]);
                   // array = alasql('Select sum(ApprovedAmount) as ApprovedAmount,SchemeId,OriginalSchemeId,SchemeName,count(1) as RowCount from ? group by SchemeId,SchemeName where OriginalSchemeId=' + SchemeId, [source]);
                    var totalAmount = array.reduce(function (a, b) { return a + b.ApprovedAmount; }, 0)
                    array = array.map(function (t, i) {
                        console.log(t.SchemeName);
                        return {
                            label: t.SchemeName + " " + (t.ApprovedAmount * 100 / totalAmount).toFixed(2) + "%",
                            SchemeId: t.SchemeId,
                            data: (t.ApprovedAmount * 100 / totalAmount),
                            color: $$constants.colors[i],
                            amount: t.ApprovedAmount,
                            count: t.RowCount
                        };
                    });
                    
                    //apply on chart
                    var plotContainer = parentContainer + ' .category-wise-pie';
                    var toolTipId = "tooltip1C130E65-4ECF-42EF-A9AA-4F7AC0A06Q1";
                    var previousPoint = null;
                    var showTooltip = function (x, y, contents) {
                        $("<div id='" + toolTipId + "' class='chart-tooltipCategory'>" + contents + '</div>').css({ position: 'absolute', display: 'none', top: y + 5, left: x + 5, border: '1px solid #fdd', padding: '2px', 'background-color': '#fee', opacity: 0.80 }).appendTo("body").show();
                    };
                    $(plotContainer).bind("plothover", function (event, pos, item) {
                        event.stopPropagation();
                        $("#x").text(pos.pageX.toFixed(2));
                        $("#y").text(pos.pageY.toFixed(2));
                        if (item) {
                            if (previousPoint != item.datapoint) {
                                previousPoint = item.datapoint;
                                $("#" + toolTipId).remove();
                                var _showBy = $(parentContainer).find(".data-by-radio:checked").val();
                                var value = "";
                                if (_showBy == "C") {
                                    value = " Count- <span>" + item.series.count + "</span>";
                                } else if (_showBy == "A") {
                                    value = +"<span class='fa fa-inr'> " + item.series.amount.toFixed(2) + "</span>";
                                } else if (_showBy == 'P') {
                                    value = "<span>" + item.series.percent.toFixed(2) + " %</span>";
                                }
                                value = item.series.label + " " + value
                                //var _showType = $(parentContainer).find(".data-by-radio:checked").val()
                                showTooltip(pos.pageX, pos.pageY, value);
                            }
                        }
                        else {
                            $("#" + toolTipId).remove();
                            previousPoint = null;
                        }
                    });
                    $.plot(plotContainer, array, {
                        series: {
                            pie: {
                                show: true,
                                radius: 3 / 4,
                                //innerRadius: 0.2,
                                label: {
                                    show: false
                                }
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true
                        }, legend: {
                            show: true,
                            labelFormatter: function (label, series) {
                                var _showBy = $(parentContainer).find(".data-by-radio:checked").val();
                                var value = "";
                                if (_showBy == "C") {
                                    value = "Count- <span style='color:" + series.color + "'>" + series.count + "</span>";
                                } else if (_showBy == "A") {
                                    value = "<span class='fa fa-inr' style='color:" + series.color + "'>" + series.amount.toFixed(2) + "</span>";
                                } else if (_showBy == 'P') {
                                    value = "<span style='color:" + series.color + "'>" + Math.round(series.percent) + "%</span>";
                                }
                                return '<div style="padding:2px;">'
                                    + label
                                    + '&nbsp;'
                                    + value + '</div>'
                            }
                        }
                    });
                } else {
                    $(".category-wise-pie").empty().append('<h3 class="text-center">No Data Effected</h3>');
                }
            },
            populateBlocks: function (r) {
                function numDifferentiation(val) {
                    if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
                    else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
                    else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
                    else val ='Rs. '+ (val).toFixed(2);
                    return val;
                }
                var blockSubmitted = $(".dash-block-submitted");
                var blockApproved = $(".dash-block-approved");
                var blockReleased = $(".dash-block-released");
                var blockReturned = $(".dash-block-returned");

                var countSubmitted = r.Submitteds.length;
                var amountSubmitted = r.Submitteds.reduce(function (a, b) { return a + b.EstimatedAmount; }, 0);

                var countApproved = r.Apporveds.length;;
                var amountApproved = r.Apporveds.reduce(function (a, b) { return a + b.ApprovedAmount; }, 0);

                var countReleased = r.Releaseds.length; //r.Releaseds.map(function (t) { return t.DprId; }).filter((v, i, a) => a.indexOf(v) === i).length;//unique by dprId
                var amountReleased = r.Releaseds.reduce(function (a, b) { return a + b.ApprovedAmount; }, 0);

                var countReturned = r.Returneds.length;;
                var amountReturned = r.Returneds.reduce(function (a, b) { return a + b.EstimatedAmount; }, 0);

                blockSubmitted.find(".dpr-count").text(countSubmitted);
                blockSubmitted.find('.amount').text(numDifferentiation(amountSubmitted));

                blockApproved.find(".dpr-count").text(countApproved);
                blockApproved.find('.amount').text(numDifferentiation(amountApproved));

                blockReleased.find(".dpr-count").text(countReleased);
                blockReleased.find('.amount').text(numDifferentiation(amountReleased));

                blockReturned.find(".dpr-count").text(countReturned);
                blockReturned.find('.amount').text(numDifferentiation(amountReturned));
            },
            populateScemeWisePieChart: function (r) {
                var parentContainer = ".sceme-wise-pie-box";
                var showBy = $(parentContainer).find(".data-by-radio:checked").val();
                var source = r
                    .Apporveds
                    .map(function (v) {
                        return {
                            DprId: v.DprId,
                            ApprovedAmount: v.ApprovedAmount,
                            SchemeId: v.ScemeTypeId,
                            SchemeName: v.ScemeTypeName
                        };
                    });
                var array = [];
                if (source.length > 0) {
                    array = alasql('Select sum(ApprovedAmount) as ApprovedAmount,SchemeId,SchemeName,count(1) as RowCount from ? group by SchemeId,SchemeName', [source]);
                    var totalAmount = array.reduce(function (a, b) { return a + b.ApprovedAmount; }, 0)
                    array = array.map(function (t, i) {
                        return {
                            label: t.SchemeName,
                            SchemeId: t.SchemeId,
                            data: (t.ApprovedAmount * 100 / totalAmount),
                            color: $$constants.colors[i],
                            amount: t.ApprovedAmount,
                            count: t.RowCount
                        };
                    });

                    //apply on chart
                    var plotContainer = parentContainer + ' .sceme-wise-pie';
                    var toolTipId = "tooltip1C130E65-4ECF-42EF-A9AA-4F7AC0A06Q";
                    var previousPoint = null;
                    var showTooltip = function (x, y, contents) {
                        $("<div id='" + toolTipId + "' class='chart-tooltip'>" + contents + '</div>').css({ position: 'absolute', display: 'none', top: y + 5, left: x + 5, border: '1px solid #fdd', padding: '2px', 'background-color': '#fee', opacity: 0.80 }).appendTo("body").show();
                    };
                    $(plotContainer).bind("plothover", function (event, pos, item) {
                        event.stopPropagation();
                        $("#x").text(pos.pageX.toFixed(2));
                        $("#y").text(pos.pageY.toFixed(2));
                        if (item) {
                            if (previousPoint != item.datapoint) {
                                previousPoint = item.datapoint;
                                $("#" + toolTipId).remove();
                                var _showBy = $(parentContainer).find(".data-by-radio:checked").val();
                                var value = "";
                                if (_showBy == "C") {
                                    value = " Count- <span>" + item.series.count + "</span>";
                                } else if (_showBy == "A") {
                                    value = +"<span class='fa fa-inr'> " + item.series.amount.toFixed(2) + "</span>";
                                } else if (_showBy == 'P') {
                                    value = "<span>" + item.series.percent.toFixed(2) + " %</span>";
                                }
                                value = item.series.label+" "+value
                                //var _showType = $(parentContainer).find(".data-by-radio:checked").val()
                                showTooltip(pos.pageX, pos.pageY, value);
                            }
                        }
                        else {
                            $("#" + toolTipId).remove();
                            previousPoint = null;
                        }
                    });
                    $(plotContainer).bind("plotclick", function (event, pos, obj) {
                        console.log(pos);
                        DataAnalysis.populateCategoryWisePieChart(rClone, obj.series.SchemeId); 
                        //populateCategoryWisePieChart(r);
                        $('.scheme-category-for-scheme-type').modal('show');

                    });
                    $.plot(plotContainer, array, {
                        series: {
                            pie: {
                                show: true,
                                radius: 3 / 4,
                                //innerRadius: 0.2,
                                label: {
                                    show: false
                                }
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true
                        }, legend: {
                            show: true,
                            labelFormatter: function (label, series) {
                                var _showBy = $(parentContainer).find(".data-by-radio:checked").val();
                                var value = "";
                                if (_showBy == "C") {
                                    value = "Count- <span style='color:" + series.color + "'>" + series.count + "</span>";
                                } else if (_showBy == "A") {
                                    value = "<span class='fa fa-inr' style='color:" + series.color+"'>" + series.amount.toFixed(2) + "</span>";
                                } else if (_showBy == 'P') {
                                    value = "<span style='color:" + series.color +"'>" + Math.round(series.percent) + "%</span>";
                                }
                                return '<div style="padding:2px;">'
                                    + label
                                    + '&nbsp;'
                                    + value + '</div>'
                            }
                        }
                    });
                } else {
                    $(".sceme-wise-pie").empty().append('<h3 class="text-center">No Data Effected</h3>');
                }


            },
            populateAverageApprovalTime: function (r) {
                var source = r
                    .Apporveds
                    .map(function (v) {
                        return {
                            DprId: v.DprId,
                            SubmissionDate: CONVERTER.Date.convertCsToJsDate(v.SubmissionDate),
                            ApprovalDate: CONVERTER.Date.convertCsToJsDate(v.approvedOn)
                        };
                    })
                    .map(function (v) {
                        var timeDiff = Math.abs(v.ApprovalDate.getTime() - v.SubmissionDate.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        return {
                            DprId: v.DprId,
                            DiffDays: diffDays
                        };
                    });
                var avg = (source.reduce(function (a, b) { return a + b.DiffDays }, 0) / source.length).toString();
                avg = avg.isNumber() ? Math.ceil(avg) : 0;
                $(".average-approval-time .value").text(avg + " Days on " + source.length + " DPR/DPRs");
            },
            populateMunicipalityWiseGroupColumnChart: function (r) {
                var parentContainer = ".municipality-wise-data";
                var container = ".municipality-wise-grouped-Column-chart";
                var showType = $(parentContainer).find(".data-by-radio:checked").val();

                //merge all props in one array and fetch distinct ulbs
                var array = [];
                array = array.concat(r.Submitteds)
                array = array.concat(r.Returneds)
                array = array.concat(r.Apporveds)
                array = array.concat(r.Releaseds)
                if (array.length == 0) {
                    $(container).empty().append("<h3 class='text-center'>No Data Found</h3>")
                    return;
                }
                var sourceData = [
                    { "label": "Returned", "data": [] },
                    { "label": "Submited", "data": [] },
                    { "label": "Approved", "data": [] },
                    { "label": "Released", "data": [] }];
                var sorceTicks = [];

                var noOfULB = $('.top-no-of-ulb :selected').val();
                var municipalities = alasql("select top " + noOfULB +" UlbID, UlbName,sum(EstimatedAmount) as EsimatedCost  from ? group by UlbID,UlbName  order by  sum(EstimatedAmount) desc", [array])
                    .map(function (v) { return { MunicipalityId: v.UlbID, Name: v.UlbName }; });

                municipalities.forEach(function (v, i) {
                    if (v.MunicipalityId != undefined) {

                        //var filteredResults = r.filter(function (q) { return q.MunicipalityId == v.MunicipalityId; });

                        var filteredReturned = sourceData.filter(function (q) { return q.label == "Returned" })[0].data;
                        var filteredApplied = sourceData.filter(function (q) { return q.label == "Submited" })[0].data;
                        var filteredApproved = sourceData.filter(function (q) { return q.label == "Approved" })[0].data;
                        var filteredReleased = sourceData.filter(function (q) { return q.label == "Released" })[0].data;

                        //r.Apporveds.length == 0 && r.Releaseds.length == 0 && r.Returneds.length == 0 && r.Submitteds.length == 0
                        var appliedVal = r
                            .Submitteds
                            .filter(function (q) { return q.UlbID == v.MunicipalityId; })
                            .map(function (q) { return showType == "C" ? 1 : q.EstimatedAmount; })
                            .reduce(function (a, b) { return a + b; }, 0)
                        filteredApplied.push([filteredApplied.length, appliedVal]);

                        var approvedVal = r
                            .Apporveds
                            .filter(function (q) { return q.UlbID == v.MunicipalityId; })
                            .map(function (q) { return showType == "C" ? 1 : q.ApprovedAmount })
                            .reduce(function (a, b) { return a + b; }, 0)
                        filteredApproved.push([filteredApproved.length, approvedVal]);

                        var returnedVal = r
                            .Returneds
                            .filter(function (q) { return q.UlbID == v.MunicipalityId; })
                            .map(function (q) { return showType == "C" ? 1 : q.EstimatedAmount })
                            .reduce(function (a, b) { return a + b; }, 0)
                        filteredReturned.push([filteredReturned.length, returnedVal]);

                        var releasedVals = r
                            .Releaseds
                            .filter(function (q) { return q.UlbID == v.MunicipalityId; })
                            .map(function (q) { return { ApprovedAmount: q.ApprovedAmount, DprId: q.DprId }; });

                        //count release
                        var releasedVal = 0;
                        if (showType == "C") {
                            releasedVal = releasedVals
                                .map(function (t) {return t.DprId;})
                                .filter((v, i, a) => a.indexOf(v) === i)//distinct DprId
                                .reduce(function (a, b) {
                                    return a + b;
                                }, 0);
                        } else {
                            releasedVal = releasedVals
                                .map(function (t) { return t.ApprovedAmount; })
                                .reduce(function (a, b) {
                                    return a + b;
                                }, 0);
                        }
                        filteredReleased.push([filteredReleased.length, releasedVal]);

                        //register municipality
                        sorceTicks.push([i, v.Name]);
                    }

                });
                var showTooltip = function (x, y, contents) {
                    $("<div id='tooltip1C130E65-4ECF-42EF-A9AA-B54F7AC0A06F' class='chart-tooltip'>" + contents + '</div>').css({ position: 'absolute', display: 'none', top: y + 5, left: x + 5, border: '1px solid #fdd', padding: '2px', 'background-color': '#fee', opacity: 0.80 }).appendTo("body").show();
                };
                $.plot($(container), sourceData, {
                    bars: { show: true, barWidth: 0.15, order: 1 },
                    xaxis: { ticks: sorceTicks },
                    grid: { hoverable: true, clickable: true }
                });
                $(container).bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.datapoint) {
                            previousPoint = item.datapoint;

                            $("#tooltip1C130E65-4ECF-42EF-A9AA-B54F7AC0A06F").remove();
                            //var x = item.datapoint[0].toFixed(2),
                            var y = item.datapoint[1].toFixed(2);
                            var _showType = $(parentContainer).find(".data-by-radio:checked").val()
                            showTooltip(item.pageX, item.pageY, item.series.label+" "+(_showType == "C" ? "Count " : "Amount ") + " = " + y);
                        }
                    }
                    else {
                        $("#tooltip1C130E65-4ECF-42EF-A9AA-B54F7AC0A06F").remove();
                        previousPoint = null;
                    }
                });

                $(container).bind("plotclick", function (event, pos, item) {
                    console.log('clicked on bar');
                });
            },
            searchByprojectNo: function () {
                var container = $(".search-project");
                container.find('.txt-search-project').autocomplete({
                    //minLength: 0,
                    source: function (request, response) {
                        container.find('.txt-search-project').attr('data-val', '');
                        var term = request.term;
                        //if (term.trim() == "") {
                        //    return;
                        //}
                        var arr = [];
                        $.post("/Modules/Dpr/FindProject", { projectNo: term, status:'A' })
                            .done(function (d) {
                                if (d.Status === 200) {
                                    arr = d.Data.map(function (v) {
                                        return {
                                            label: v.PorjectNo,
                                            value: v.MstDprId
                                        };
                                    });
                                } else {
                                    alertify.error(d.Message);
                                }
                                response(arr);
                            })
                            .always(function (d) {
                                //finished
                            });
                    },
                    select: function (event, ui) {
                        this.value = ui.item.label;
                        container.find('.txt-search-project').attr('data-val', ui.item.value);
                        $.post("/Modules/Dpr/GetProjectDetails", { MstDprId: ui.item.value })
                            .done(function (d) {
                                if (d.Status === 200) {
                                    //Applied(Node1) -> Approved(Node2) -> Tender(Node3) -> PhysicalProg(Node4) -> FundRelease(Node5)
                                    populateSearchedProjectDetails(d.Data);
                                } else {
                                    alertify.error(d.Message);
                                }
                            })
                            .fail(function (e) { })
                            .always(function () { })
                        event.preventDefault();
                    }
                });
                container.find('.txt-search-projectName').autocomplete({
                    //minLength: 0,
                    source: function (request, response) {
                        container.find('.txt-search-projectName').attr('data-val', '');
                        var term = request.term;
                        //if (term.trim() == "") {
                        //    return;
                        //}
                        var arr = [];
                        $.post("/Modules/Dpr/FindProjectByName", { projectName: term, status: 'A' })
                            .done(function (d) {
                                if (d.Status === 200) {
                                    arr = d.Data.map(function (v) {
                                        return {
                                            label: v.PorjectNo,
                                            value: v.MstDprId
                                        };
                                    });
                                } else {
                                    alertify.error(d.Message);
                                }
                                response(arr);
                            })
                            .always(function (d) {
                                //finished
                            });
                    },
                    select: function (event, ui) {
                        this.value = ui.item.label;
                        container.find('.txt-search-projectName').attr('data-val', ui.item.value);
                        $.post("/Modules/Dpr/GetProjectDetails", { MstDprId: ui.item.value })
                            .done(function (d) {
                                if (d.Status === 200) {
                                    populateSearchedProjectDetails(d.Data);
                                } else {
                                    alertify.error(d.Message);
                                }
                            })
                            .fail(function (e) { })
                            .always(function () { })
                        event.preventDefault();
                    }
                });
                var populateSearchedProjectDetails = function (r) {
                    var getStackedBarData = function (rr) {
                        var apprvedAmount = rr.ApprovedAmount;
                        var releasedInstallments = rr.InstallmentReleases.filter(function (a) { return a.Status == "A"; }).map(function (ab, ix) {
                            return {
                                text: ab.InstallmentName,
                                amount: ab.ReleasedAmount,
                                color: colors[(ix + 1)],
                                percent: Math.round(ab.ReleasedAmount * 100 / apprvedAmount),
                                status: ab.Status,
                                statusDscr: ab.StatusDscr
                            };
                        });

                        var pendingAmount = apprvedAmount - releasedInstallments.reduce(function (a, b) { return a + b.amount }, 0);
                        var pendingPercentage = 100 - releasedInstallments.reduce(function (a, b) { return a + b.percent }, 0)
                        return {
                            installments: releasedInstallments,
                            pendingInstallmnt: {
                                text: "Pending",
                                amount: pendingAmount,
                                color: colors[0],
                                percent: pendingPercentage
                            }
                        };
                    };

                    var colors = ["#e6194B", "#3cb44b", "#4363d8", "#f58231", "#911eb4", "#42d4f4", "#f032e6", "#bfef45", "#fabebe", "#469990", "#9A6324", "#800000", "#808000", "#000075", "#a9a9a9", "#000000", "#ffe119"];
                    container.find(".searched-project-chart").empty().append('<h3 class="text-center error">No Project Selected</h3>');
                   
                    var str = "";
                    var str1 = "";

                    if (r.Status == "A") {

                        var formattedData = getStackedBarData(r);
                        var v = formattedData.pendingInstallmnt;
                        var strList = "<ul class='list-inline'>";
                        str += "<div class='progress' style='margin-bottom: 0px;'>";
                        strList += "<li class='list-inline-item'><span class='color-label white' title='" + v.text + "' style='background-color:" + v.color + "'><span class='fa fa-rupee'></span>" + v.amount.toFixed(2) + "-" + v.percent + "%</span></li>";
                        formattedData.installments.forEach(function (vv) {
                            str += "<div class='progress-bar' role='progressbar' title='" + vv.percent + "%' style='width:" + vv.percent + "%;background-color:" + vv.color + "'>" + vv.text + "</div>";
                            strList += "<li class='list-inline-item'><span class='color-label white' title='" + vv.text + "' style='background-color:" + vv.color + "'><span class='fa fa-rupee'></span>" + vv.amount.toFixed(2) + "-" + vv.percent + "%</span></li>";
                        });


                        if (v.percent > 0) {
                            str += "<div class='progress-bar' role='progressbar' title='" + v.percent + "%' style='width:" + v.percent + "%;background-color:" + v.color + "'>" + v.text + "</div>";
                        }
                        str += "</div>";
                        str += "<span class='fa fa-long-arrow-right'>0%</span><span class='fa fa-long-arrow-left pull-right'>100%</span>";
                        strList += "</ul>";

                        str = strList + str;

                        var strProjectStatus = "";
                        strProjectStatus += "<ul class='products-list product-list-in-box'>";
                        //submitted
                        strProjectStatus += "<li class='item'>";
                        strProjectStatus += "<div class=''>";
                        strProjectStatus += "<a href='javascript:void(0)' class='product-title'>Submitted On</a>";
                        strProjectStatus += "<span class='product-description'>" + r.SubmissionDate.toJsFromCsDate().format('dd/mm/yy')+"</span>";
                        strProjectStatus += "</div>";
                        strProjectStatus += "</li>";
                        //Approved
                        strProjectStatus += "<li class='item'>";
                        strProjectStatus += "<div class=''>";
                        strProjectStatus += "<a href='javascript:void(0)' class='product-title'>Approved On</a>";
                        strProjectStatus += "<span class='product-description'>" + (r.ApprovalDate ? r.ApprovalDate.toJsFromCsDate().format('dd/mm/yy'):"Not Available") + "</span>";
                        strProjectStatus += "</div>";
                        strProjectStatus += "</li>";

                        //Released
                        strProjectStatus += "<li class='item'>";
                        strProjectStatus += "<div class=''>";
                        strProjectStatus += "<a href='javascript:void(0)' class='product-title'>Released On</a>";
                        var releaseDates = r.InstallmentReleases
                            .filter(function (t) { return t.Status == 'A'; })
                            .map(function (t) { return (t.ReleaseDate ? t.ReleaseDate.toJsFromCsDate().format('dd/mm/yy') : "Not Available"); })
                            .join(", ");
                        strProjectStatus += "<span class='product-description'>" + releaseDates + "</span>";
                        strProjectStatus += "</div>";
                        strProjectStatus += "</li>";

                        //Name of Workd
                        strProjectStatus += "<li class='item'>";
                        strProjectStatus += "<div class=''>";
                        strProjectStatus += "<a href='javascript:void(0)' class='product-title'>Name Of Work</a>";
                        strProjectStatus += "<span class='product-description'>" + r.NameOfWork + "</span>";
                        strProjectStatus += "</div>";
                        strProjectStatus += "</li>";

                        strProjectStatus += "</ul>"; 

                        str += strProjectStatus;
                        
                    } else {
                        str += "<h3 class='text-center error'>Dpr not Approved or Sanctioned</h3>";
                    }
                   
                    container.find(".searched-project-chart").empty().append(str);
                    container.find(".searched-road-map").find(".container").find(".wrapper").find(".arrow-steps").css("display", "block");
                    container.find(".searched-road-map").find(".container").find(".wrapper").find(".text-center").css("display", "none");
                    if (r.SubmissionDate == null) {
                        $(".next").trigger("click");
                        container.find(".searched-road-map").find(".container").find(".wrapper").empty().append('<h3 class="text-center error">DPR not found</h3>');
                    }
                    else {
                        container.find(".searched-road-map").find(".container").find(".wrapper").find(".applied").attr("title", " Applied On:" + r.SubmissionDate.toJsFromCsDate().format('dd/mm/yy'));
                        if (r.ApprovalDate != null) {
                            $(".next").trigger("click");
                            container.find(".searched-road-map").find(".container").find(".wrapper").find(".approved").attr("title", " Approved On:" + r.ApprovalDate.toJsFromCsDate().format('dd/mm/yy') + " ,Sanction Memo No:" + r.SanctionMemoNo)
                        }
                        if (r.AwardedTo != null) {
                            $(".next").trigger("click");
                            container.find(".searched-road-map").find(".container").find(".wrapper").find(".tender").attr("title", " ,Tender No:" + r.TenderNo + " Awarded To:" + r.AwardedTo )
                        }
                        if (r.PercentageOfWorkCompleted != null) {
                            $(".next").trigger("click");
                            container.find(".searched-road-map").find(".container").find(".wrapper").find(".phyProg").attr("title", " ,Percentage Of Work Completed:" + r.PercentageOfWorkCompleted)
                        }
                    }

                };
            },
            onDataLoaded: function (r) {
                DataAnalysis.populateBlocks(r); 
                DataAnalysis.populateScemeWisePieChart(r); 
                DataAnalysis.populateAverageApprovalTime(r); 
                DataAnalysis.populateMunicipalityWiseGroupColumnChart(r);
            }

        };
        var FinancialYearWiseData = {
            populateReleaseVsApprovedLastNYears: function (r) {
                var toolTipId = "tooltip1C130E65-4ECF-42EF-A9AA-B54F7AC0A06Q";
                var container = "#financial-year-wise-data .financial-year-wise-data-bar-chart-last-n-years";

                var sorceTicks = [];
                var series = [{
                    data: [],
                    label: "Submitted"
                },{
                    data: [],
                    label: "Approved"
                },
                {
                    data: [],
                    label: "Released"
                }, {
                    data: [],
                    label: "Returned"
                }];

                var array = [];
                array = array.concat(r.Submitteds)
                array = array.concat(r.Returneds)
                array = array.concat(r.Apporveds)
                array = array.concat(r.Releaseds)

                var finYears = alasql('select FinYear from ? group by FinYear', [array]);
                if (array.length > 0) {
                    finYears.forEach(function (_finYear, i) {
                        var finYear = _finYear.FinYear;
                        var approvedData = r
                            .Apporveds
                            .filter(function (t) { return t.FinYear == finYear})
                            .reduce(function (a, b) { return a + b.ApprovedAmount; }, 0);

                        series.filter(function (t) { return t.label == "Approved" })[0].data.push([i, approvedData]);

                        var returnedData = r
                            .Returneds
                            .filter(function (t) { return t.FinYear == finYear; })
                            .reduce(function (a, b) { return a + b.EstimatedAmount; }, 0);

                        series.filter(function (t) { return t.label == "Returned" })[0].data.push([i, returnedData]);

                        var releasedData = r
                            .Releaseds
                            .filter(function (t) { return t.FinYear == finYear; })
                            .reduce(function (a, b) { return a + b.ApprovedAmount; }, 0);
                        series.filter(function (t) { return t.label == "Released" })[0].data.push([i, releasedData]);

                        var submittedData = r
                            .Submitteds
                            .filter(function (t) { return t.FinYear == finYear })
                            .reduce(function (a, b) { return a + b.EstimatedAmount; }, 0);
                        series.filter(function (t) { return t.label == "Submitted" })[0].data.push([i, submittedData]);
                        sorceTicks.push([i, finYear]);
                    });
                    

                    var showTooltip = function (x, y, contents) {
                        $("<div id='" + toolTipId + "' class='chart-tooltip'>" + contents + '</div>').css({ position: 'absolute', display: 'none', top: y + 5, left: x + 5, border: '1px solid #fdd', padding: '2px', 'background-color': '#fee', opacity: 0.80 }).appendTo("body").show();
                    };
                    $(container).bind("plothover", function (event, pos, item) {
                        event.stopPropagation();
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint != item.datapoint) {
                                previousPoint = item.datapoint;
                                $("#" + toolTipId).remove();
                                var amount = item.datapoint[1].toFixed(2);
                                //var _showType = $(parentContainer).find(".data-by-radio:checked").val()
                                showTooltip(item.pageX, item.pageY, ("Total " + item.series.label + " Amount ") + " = " + amount);
                            }
                        }
                        else {
                            $("#" + toolTipId).remove();
                            previousPoint = null;
                        }
                    });

                    $(container).bind("plotclick", function (event, pos, item) {
                        console.log('clicked on bar');
                    });
                    $.plot(container, series, {
                        bars: { show: true, barWidth: 0.15, order: 1 },
                        xaxis: { ticks: sorceTicks },
                        grid: { hoverable: true, clickable: true }
                    });
                } else {
                    $(container).empty().append('<h3 class="text-center error">No data effected</h3>')
                }
            },
            populateReleaseVsApprovedNextNYears: function (r) {
                var toolTipId = "tooltip1C130E65-4ECF-42EF-A9AA-B54F7AC0A06Q1";
                var container = "#financial-year-wise-data .financial-year-wise-data-bar-chart-next-n-years-expenditure";

                if (r.length > 0) {
                    var data = [
                        //[0, 11], //London, UK
                        //[1, 15], //New York, USA
                        //[2, 25], //New Delhi, India
                        //[3, 24], //Taipei, Taiwan
                        //[4, 13], //Beijing, China
                        //[5, 18]  //Sydney, AU
                    ];
                    var ticks = [
                        //[0, "London"], [1, "New York"], [2, "New Delhi"], [3, "Taipei"], [4, "Beijing"], [5, "Sydney"]
                    ];
                    r.forEach(function (v, i) {
                        data.push([i, v.Value]);
                        ticks.push([i, v.Key])
                            ;                    });
                    var dataset = [
                        { label: "Next Five Yeas Expenditures", data: data, color: "#5482FF" }
                    ];
                    var option = {
                        xaxis: {
                            ticks: ticks
                        }, series: {
                            bars: {
                                show: true
                            }
                        },
                        bars: {
                            align: "center",
                            barWidth: 0.5
                        },
                        grid: { hoverable: true, clickable: true }
                    };
                    var showTooltip = function (x, y, contents) {
                        $("<div id='" + toolTipId + "' class='chart-tooltip'>" + contents + '</div>').css({ position: 'absolute', display: 'none', top: y + 5, left: x + 5, border: '1px solid #fdd', padding: '2px', 'background-color': '#fee', opacity: 0.80 }).appendTo("body").show();
                    };
                    $(container).bind("plothover", function (event, pos, item) {
                        event.stopPropagation();
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint != item.datapoint) {
                                previousPoint = item.datapoint;
                                $("#" + toolTipId).remove();
                                var amount = item.datapoint[1].toFixed(2);
                                showTooltip(item.pageX, item.pageY, ("Amount ") + " = " + amount);
                            }
                        }
                        else {
                            $("#" + toolTipId).remove();
                            previousPoint = null;
                        }
                    });

                    $(container).bind("plotclick", function (event, pos, item) {
                        console.log('clicked on bar');
                    });
                    $.plot(container, dataset, option);
                } else {
                    $(container).empty().append('<h3 class="text-center error">No data effected</h3>')
                }
            },

            pullServerData: function (url,payload,callback) {
                $.ajax({
                    url: url,
                    method: "Post",
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify(payload),
                    success: function (e) {
                        if (e.Status == 200) {
                            callback(e.Data);
                        } else {
                            alertify.error(e.Message);
                        }
                    },
                    error: function (e) {
                        callback({
                            Data: null,
                            Message: e.responseText || e.statusText,
                            Status: e.status
                        });
                    }
                });
                
            },
            onChangeFilter: function (filterData) {
                var getPayload = function (adjucentRange, finyearCount) {
                    var getLastNFinYear = function (_adjucentRange) {
                        var curFinyear = _SERVER.getCurFinYear();
                        var curFinLeftPart = (curFinyear.split('-')[0]).toNumber();
                        var curFinRightPart = (curFinyear.split('-')[1]).toNumber();
                        var arr = [];
                        if (curFinyear) {
                            _adjucentRange.sort(function (a, b) { return a-b }).forEach(function (i) {
                                arr.push((curFinLeftPart + i) + '-' + (curFinRightPart + i));
                            });
                        }
                        return arr;
                    };

                    var finyears = getLastNFinYear(adjucentRange);
                    var payload = {
                        AuthorityTypeId: filterData.municipalityFilter.AuthorityTypeId,
                        FromFinYear: finyears[0],
                        ToFinYear: finyears[finyears.length - 1],
                        MunicipalityIds: filterData.municipalityFilter.municipalities.map(function (v) { return v.id; }),
                        SchemeTypeId: filterData.SchemeTypeId,
                        SchemeCategoryId: filterData.SchemeCategoryId
                    };
                    
                    return payload;
                }
                var lastFiveYearPayload = getPayload([0, -1, -2, -3, -4]);
                reportPayloads.forEach(function (t) {
                    if (["Rpt_FundreleaseFinYearWise"].indexOf(t.reportCode) != -1) {
                        t.payload = lastFiveYearPayload;
                    }
                });
                FinancialYearWiseData.pullServerData("/Modules/Dpr/GetDashboardFinYearWiseData",lastFiveYearPayload, function (e) {
                    FinancialYearWiseData.populateReleaseVsApprovedLastNYears(e);
                });

                var nextFiveYearExpenditurePayload = {
                    date: new Date(),
                    MunicipalityIds: filterData.municipalityFilter.municipalities.map(function (v) { return v.id; }),
                    SchemeTypeId: filterData.SchemeTypeId,
                    SchemeCategoryId: filterData.SchemeCategoryId
                };
                reportPayloads.forEach(function (t) {
                    if (["Rpt_YearWiseFundReq"].indexOf(t.reportCode) != -1) {
                        t.payload = nextFiveYearExpenditurePayload;
                    }
                });
                FinancialYearWiseData.pullServerData("/Modules/Dpr/GetDashboardFinYearExpenditure",nextFiveYearExpenditurePayload, function (e) {
                    FinancialYearWiseData.populateReleaseVsApprovedNextNYears(e);
                });
            }
        };
        (function () {
            bindEvents();
            $$filterBox.$reloadFilter();
            DataAnalysis.searchByprojectNo();
        }());
    };
    var modalChangeFilter = function () {
        this._filteredData = null;
        this.$onFilterDataChanged = null;
        var _modalChangeFilter = this;
        var modalFilterBox = $(".change-filter-modal")
        var bindEvents = function () {
            var filterList = function () {
                var txtSearch = modalFilterBox.find(".txt-search-municipality");
                var authorityType = modalFilterBox.find('.ddl-authority');
                var authorityTypeId = window.isNullOrEmpty(authorityType.val().trim(), null)
                var authFilter = (authorityTypeId ? "[data-auth-id='" + authorityTypeId + "']" : "");
                if (txtSearch.val().trim() == "") {
                    modalFilterBox.find(".list-container input[type=checkbox]:not(" + authFilter + "):not(.all)").closest('li').hide();
                    modalFilterBox.find(".list-container input[type=checkbox]" + authFilter + ":not(.all)").closest('li').show();
                } else {
                    //non matched list
                    var nonMatched = modalFilterBox.find(".list-container input[type=checkbox]" + authFilter+":not(.all)").filter(function (a, t) {
                        var x = $(t).parent().text().toLowerCase().indexOf(txtSearch.val().toLowerCase()) !== -1;
                        return !x;
                    });

                    nonMatched.closest('li').hide();
                    //matched list
                    var matched = modalFilterBox.find(".list-container input[type=checkbox]" + authFilter+":not(.all)").filter(function (a, t) {
                        var x = $(t).parent().text().toLowerCase().indexOf(txtSearch.val().toLowerCase()) !== -1;
                        return x;
                    });
                    matched.closest('li').show();
                }
            };
            //check box only select all wala
            modalFilterBox.on('change', ".list-container input[type=checkbox].all", function () {
                modalFilterBox.find(".list-container input[type=checkbox]:not(.all)").prop('checked', $(this).prop('checked'));
            });
            //all check box except (select all)
            modalFilterBox.on('change', ".list-container input[type=checkbox]:not(.all)", function () {
                var countOfMunicipality = modalFilterBox.find(".list-container input[type=checkbox]:not(.all)").length;
                var selectedCount = modalFilterBox.find(".list-container input[type=checkbox]:not(.all):checked").length;
                modalFilterBox.find(".list-container input[type=checkbox].all").prop('checked', (countOfMunicipality === selectedCount));
            });
            modalFilterBox.on('keyup', ".txt-search-municipality", function () {
                filterList();
            });
            modalFilterBox.on('change', ".ddl-authority", function () {
                filterList();
            });
            modalFilterBox.on('change', ".ddl-time-span", function () {
                if ($(this).val() == "date-range") {
                    modalFilterBox.find(".custom-date-range").show();
                } else {
                    modalFilterBox.find(".custom-date-range").hide();
                }
            });
            modalFilterBox.on('hidden.bs.modal', function () {
                _modalChangeFilter._filteredData = getFormData();
                if (_modalChangeFilter.$onFilterDataChanged) {
                    _modalChangeFilter.$onFilterDataChanged(_modalChangeFilter._filteredData);
                }
            });
            //make disable all date, on to-date control
            modalFilterBox.find(".to-date").datepicker('option', 'beforeShowDay', function (date) {
                return [false, ''];
            });
            modalFilterBox.find(".from-date").datepicker('option', 'onSelect', function (e) {
                //set minimum date 
                modalFilterBox.find(".to-date").datepicker('option', 'minDate', e.toDate("dd/mm/yyyy"));
                //enable relative dates
                modalFilterBox.find(".to-date").datepicker('option',
                    'beforeShowDay', function (date) {
                        return [true, ''];
                    }
                );
            });
            modalFilterBox.on('change', '.ddl-schemetypes', function () {
                var schemeid = $(this).val().toNumber();
                var categoryCtrl = modalFilterBox.find('.ddl-scheme-categories');
                categoryCtrl.find('option:not(:first-child)').remove();
                if (schemeid) {
                    var categoriesRaw = JSON.parse(categoryCtrl.attr('data-all-categories'));
                    categoriesRaw = categoriesRaw.filter(function (v) {
                        return v.SchemeTypeId === schemeid;
                    });
                    categoriesRaw.forEach(function (v, i) {
                        categoryCtrl.append('<option value="' + v.Id + '">' + v.Name + '</option>');
                    });
                }
            });
        };
        var getFormData = function () {
            var authorityType = modalFilterBox.find('.ddl-authority');
            var authorityTypeId = window.isNullOrEmpty(authorityType.val().trim(), null)
            var authFilter = (authorityTypeId ? "[data-auth-id='" + authorityTypeId + "']" : "");

            var obj = {
                municipalityFilter: {
                    municipalities: modalFilterBox
                        .find(".list-container input[type=checkbox]" + authFilter+":not(.all):checked")
                        .map(function (i, e) {
                            return {
                                id: $(e).val(),
                                name: $(e).attr('data-name')
                            }
                        }).get(),
                    isAllSelected: modalFilterBox
                        .find(".list-container input[type=checkbox].all").prop('checked'),
                    AuthorityTypeId: authorityTypeId
                },
                timeSpanFilter: {
                    timeSpan: {
                        val: modalFilterBox.find(".ddl-time-span").val(),
                        text: modalFilterBox.find(".ddl-time-span option:selected").text().trim()
                    }
                },
                SchemeTypeId: window.isNullOrEmpty(modalFilterBox.find('.ddl-schemetypes').val()),
                SchemeCategoryId: window.isNullOrEmpty(modalFilterBox.find('.ddl-scheme-categories').val())

            };

            if (modalFilterBox.find(".ddl-time-span").val() == "date-range") {
                var fromDate = modalFilterBox.find(".from-date").val();
                var toDate = modalFilterBox.find(".to-date").val();
                obj.timeSpanFilter.fromDate = fromDate.toDate("dd/mm/yyyy") == "Invalid Date" ? new Date() : fromDate.toDate("dd/mm/yyyy");
                obj.timeSpanFilter.toDate = toDate.toDate("dd/mm/yyyy") == "Invalid Date" ? new Date() : toDate.toDate("dd/mm/yyyy");
            }
            return obj
        };
        this.removeMunicipality = function (id) {
            var ctrl = modalFilterBox.find(".list-container input[type=checkbox][value=" + id + "]");
            if (ctrl.length > 0 && ctrl.is(":disabled") === false) {
                ctrl.prop('checked', false).trigger('change');
                _modalChangeFilter._filteredData = getFormData();
                _modalChangeFilter.$onFilterDataChanged(_modalChangeFilter._filteredData);
            }
        };
        this.reloadFilterData = function () {
            modalFilterBox.trigger('hidden.bs.modal');
        };
        (function () {
            bindEvents();
        }());
    };
    var filterBox = function ($changeFilterModal) {
        
        var _filterBox = this;
        this._filteredData = null;
        var p$filterBox = this;
        this.$onChangeFilterData = null;
        this.$reloadFilter = function () {
            $changeFilterModal.reloadFilterData();
        };
        var filterListBox = $(".filterBox .list-filters");
        var populateFilterBox = function (filterData) {
            filterListBox.empty();
            var strFilterList = "";
            //populate time span
            if (filterData.timeSpanFilter.timeSpan.val === "date-range") {
                strFilterList += "<div class='filter-chip bg-green' data-key='" + filterData.timeSpanFilter.timeSpan.val + "'>" + filterData.timeSpanFilter.fromDate.toStringDate('dd/mm/yyyy') + " - " + filterData.timeSpanFilter.toDate.toStringDate('dd/mm/yyyy') + "</div>";
            } else {
                strFilterList += "<div class='filter-chip bg-green' data-key='" + filterData.timeSpanFilter.timeSpan.val + "'>" + filterData.timeSpanFilter.timeSpan.text + "</div>";
            }
            if (filterData.municipalityFilter.isAllSelected) {
                strFilterList += "<div class='filter-chip bg-olive-active municipality-chip' data-val=''>All ULBs</div>";
            } else {
                var needAddMore = false;
                var maxCountShowMunicipalityChip = 6;
                var toolTipTextFromMoreChip = "";
                filterData.municipalityFilter.municipalities.forEach(function (v, i) {
                    if (i <= maxCountShowMunicipalityChip) {
                        strFilterList += "<div class='filter-chip bg-olive-active municipality-chip' title='" + v.name + "' data-val='" + v.id + "'>" + (v.name.length > 17 ? v.name.substring(0, 15) + ".." : v.name) + "&nbsp;<a class='remove-chip cursor-pointer'><i class='fa  fa-remove white'></i></a></div>";
                    } else {
                        needAddMore = true;
                        strFilterList += "<div class='filter-chip bg-olive-active municipality-chip' title='" + v.name + "' data-val='" + v.id + "' style='display:none;'>" + (v.name.length > 17 ? v.name.substring(0, 15) + ".." : v.name) + "<a class='remove-chip cursor-pointer'><i class='fa  fa-remove white'></i></a></div>";
                        toolTipTextFromMoreChip += (((maxCountShowMunicipalityChip + 1) == i ? "" : "<br/>") + ("<code>" + (i - maxCountShowMunicipalityChip) + ". " + v.name + "</code>"));
                    }
                });
                if (needAddMore) {
                    strFilterList += "<div class='filter-chip bg-olive-active municipality-chip exclude' data-toggle='popover' data-placement='left' data-original-title='Municipalities' data-content='" + toolTipTextFromMoreChip + "'>" + (filterData.municipalityFilter.municipalities.length - (maxCountShowMunicipalityChip + 1)) + " More..</div>";
                }
            }
            filterListBox.append(strFilterList);
        };
        var bindEvents = function () {
            $changeFilterModal.$onFilterDataChanged = function (filterData) {
                populateFilterBox(filterData);
                if (p$filterBox.$onChangeFilterData) {
                    p$filterBox._filteredData = filterData;
                    p$filterBox.$onChangeFilterData(filterData);
                }
            }
            filterListBox.on('click', ".municipality-chip>.remove-chip", function () {
                $changeFilterModal.removeMunicipality($(this).parent().attr('data-val'));
            });
            
        };

        (function () {
            bindEvents();
        }())
    };
    var initConstants = function () {
        constants.colors = randomColor({
            count: 100,
            luminosity: 'dark'
        });
    };
    var dashBoardExtended = function ($$filterBox, $$constants) {
        $(".top-no-of-ulb").change(function () {
            $$filterBox.$reloadFilter();
        });
    };
    (function () {
        var $$initConstants = new initConstants();
        var $$modalChangeFilter = new modalChangeFilter();
        var $$filterBox = new filterBox($$modalChangeFilter);
        var $$dashBoard = new dashBoard($$filterBox, constants);
        //var $$dashBoardExtended = new dashBoard($$filterBox, constants);
        $(".dash-tab a[data-toggle='tab']").on('shown.bs.tab', function (e) {
            $(e.target).siblings().removeClass('active');
            $(e.target).addClass('active');
            var target = $(e.target).attr("href") // activated tab
            var tabPane = $(".dash-tab").find(target);
            tabPane.siblings().removeClass('active');
            tabPane.addClass('active');
        });
    }())
});
