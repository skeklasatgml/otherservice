﻿$(document).ready(function () {
    var helper = {
        ifNaN: function (val, replaceBy) {
            if (isNaN(val)) {
                return replaceBy;
            } else {
               return Number(val);
            }
        },
        ifNullEmptyOrUndefinded: function (val, replace) {
            if (val)
                return val;
            else
                return replace;
        },
         convertTodate : function (format, date) {
            switch (format) {
                case 'dd/mm/yyyy':
                case 'dd/mm/yy':
                    {
                        var array = date.split('/');
                        return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                        break;
                    }
            }
        }
    }
    var globalVariables = {
        masterData: null,
        deletableFileRequest:[]
    };
    var doms = function () {
        return {
            _container: function () {
                var container_selector = (".cntr-schedule-three");
                return {
                    _parent: function () {
                        var parentSelect = $(".down-loaded-form");
                        return {
                            _obj: $(parentSelect),
                            _selector:$(parentSelect)
                        };
                    },
                    _selector: container_selector,
                    _obj: $(container_selector),
                    _ctrl_Name: function () {
                        var ctrl_name = container_selector + " .name";
                        return {
                            _obj: $(ctrl_name),
                            _selector: ctrl_name
                        }
                    },
                    _ctrl_email: function () {
                        var ctrl_email = container_selector + " .email";
                        return {
                            _obj: $(ctrl_email),
                            _selector: ctrl_email
                        }
                    },
                    _ctrl_phone: function () {
                        var ctrl_phone = container_selector + " .mobile";
                        return {
                            _obj: $(ctrl_phone),
                            _selector: ctrl_phone
                        }
                    },
                    _ctrl_applyDate: function () {
                        var ctrl_ApplyDate = container_selector + " .apply-date";
                        return {
                            _obj: $(ctrl_ApplyDate),
                            _selector: ctrl_ApplyDate
                        }
                    },
                    _ctrl_Address: function () {
                        var ctrl_address = container_selector + " .address";
                        return {
                            _obj: $(ctrl_address),
                            _selector: ctrl_address
                        }
                    },
                    _ctrl_Municiapality: function () {
                        var ctrl_Municiapality = container_selector + " .ddl-municipality";
                        return {
                            _obj: $(ctrl_Municiapality),
                            _selector: ctrl_Municiapality
                        }
                    },
                    _ctrl_assmtId: function () {
                        var ctrl_assmtId = container_selector + " .assmt-id";
                        return {
                            _obj: $(ctrl_assmtId),
                            _selector: ctrl_assmtId
                        }
                    },
                    _tabs: function () {
                        var tabContainer = container_selector + (" .tab-content-schedule-three");
                        return {
                            _selector: tabContainer,
                            _obj: $(tabContainer),
                            _tab_location: function () {
                                var tab_location = tabContainer + " .tab-location";
                                return {
                                    _selector: tab_location,
                                    _obj: $(tab_location),
                                    _ctrl_borough: function () {
                                        var borough = tab_location + " .borough";
                                        return {
                                            _selector: borough,
                                            _obj: $(borough),
                                        };
                                    },
                                    _ctrl_ward: function () {
                                        var ward = tab_location + " .ward";
                                        return {
                                            _selector: ward,
                                            _obj: $(ward),
                                        };
                                    },
                                    _ctrl_hdnWard: function () {
                                        var hdnWard = tab_location + " .hdn-ward";
                                        return {
                                            _selector: hdnWard,
                                            _obj: $(hdnWard),
                                        };
                                    },
                                    _ctrl_holding: function () {
                                        var holding = tab_location + " .holding";
                                        return {
                                            _selector: holding,
                                            _obj: $(holding),
                                        };
                                    },
                                    _ctrl_roadlane: function () {
                                        var roadlane = tab_location + " .roadlane";
                                        return {
                                            _selector: roadlane,
                                            _obj: $(roadlane),
                                        };
                                    },
                                    _ctrl_hdnRoadlane: function () {
                                        var hdnRoadlane = tab_location + " .hdn-roadlane";
                                        return {
                                            _selector: hdnRoadlane,
                                            _obj: $(hdnRoadlane),
                                        };
                                    },

                                    _ctrl_complex: function () {
                                        var complex = tab_location + " .complex";
                                        return {
                                            _selector: complex,
                                            _obj: $(complex),
                                        };
                                    },
                                    _ctrl_landType: function () {
                                        var category = tab_location + " .land-type";
                                        return {
                                            _selector: category,
                                            _obj: $(category),
                                        };
                                    },
                                    _ctrl_landarea_dec: function () {
                                        var landarea_dec = tab_location + " .landarea-dec";
                                        return {
                                            _selector: landarea_dec,
                                            _obj: $(landarea_dec),
                                        };
                                    },
                                    _ctrl_landarea_sft: function () {
                                        var landarea_sft = tab_location + " .landarea-sft";
                                        return {
                                            _selector: landarea_sft,
                                            _obj: $(landarea_sft),
                                        };
                                    },
                                    _ctrl_mouzaname: function () {
                                        var mouzaname = tab_location + " .mouzaname";
                                        return {
                                            _selector: mouzaname,
                                            _obj: $(mouzaname),
                                        };
                                    },
                                    _ctrl_jlno: function () {
                                        var jlno = tab_location + " .jlno";
                                        return {
                                            _selector: jlno,
                                            _obj: $(jlno),
                                        };
                                    },
                                    _ctrl_ps: function () {
                                        var ps = tab_location + " .ps";
                                        return {
                                            _selector: ps,
                                            _obj: $(ps),
                                        };
                                    },
                                    _ctrl_dag_rs: function () {
                                        var dag_rs = tab_location + " .dag-rs";
                                        return {
                                            _selector: dag_rs,
                                            _obj: $(dag_rs),
                                        };
                                    },
                                    _ctrl_dag_lr: function () {
                                        var dag_lr = tab_location + " .dag-lr";
                                        return {
                                            _selector: dag_lr,
                                            _obj: $(dag_lr),
                                        };
                                    },
                                    _ctrl_khatian_rs: function () {
                                        var khatian_rs = tab_location + " .khatian-rs";
                                        return {
                                            _selector: khatian_rs,
                                            _obj: $(khatian_rs),
                                        };
                                    },
                                    _ctrl_khatian_lr: function () {
                                        var khatian_lr = tab_location + " .khatian-lr";
                                        return {
                                            _selector: khatian_lr,
                                            _obj: $(khatian_lr),
                                        };
                                    },
                                    _ctrl_rornature: function () {
                                        var rornature = tab_location + " .rornature";
                                        return {
                                            _selector: rornature,
                                            _obj: $(rornature),
                                        };
                                    },
                                    _ctrl_deedNo: function () {
                                        var deedNo = tab_location + " .deed-no";
                                        return {
                                            _selector: deedNo,
                                            _obj: $(deedNo),
                                        };
                                    },
                                    _ctrl_deedDate: function () {
                                        var deedDate = tab_location + " .deed-date";
                                        return {
                                            _selector: deedDate,
                                            _obj: $(deedDate),
                                        };
                                    },
                                    _ctrl_deedType: function () {
                                        var deedType = tab_location + " .deed-type";
                                        return {
                                            _selector: deedType,
                                            _obj: $(deedType),
                                        };
                                    },
                                    _btn_next: function () {
                                        var next = tab_location + " .btn-next";
                                        return {
                                            _selector: next,
                                            _obj: $(next),
                                        };
                                    }

                                }
                            },
                            _tab_usage: function () {

                                var tab_usage = tabContainer + " .tab-usage";
                                var getNewUsageControl = function () {
                                    var data = globalVariables.masterData.Usage;
                                    var select = $('<select class="form-control usage data-last-selected=""/>').append('<option value="">--select--</option>');
                                    if (data) {
                                        $.each(data, function (ind, val) {
                                            select.append('<option value="' + val.Key + '">' + val.Value + '</option>');
                                        });
                                    }
                                    return select;
                                };
                                return {
                                    _selector: tab_usage,
                                    _obj: $(tab_usage),
                                    _ctrl_holdingTypeGroup: function () {
                                        var _holdingTypeGroup = tab_usage + " .holding-type-group";
                                        return {
                                            _selector: _holdingTypeGroup,
                                            _obj: $(_holdingTypeGroup),
                                        };
                                    },
                                    _ctrl_vacantLand: function () {
                                        var ctrlVacantland = tab_usage.concat(" .cntr-vacant-land-details");
                                        return {
                                            _ctrl_vacantLandDec:function(){
                                                var _vacantLandDec = ctrlVacantland + " .vacant-land-dec";
                                                return {
                                                    _selector: _vacantLandDec,
                                                    _obj: $(_vacantLandDec),
                                                }
                                            },
                                            _ctrl_vacantLandSft: function () {
                                                var _vacantLandSft = ctrlVacantland + " .vacant-land-sft";
                                                return {
                                                    _selector: _vacantLandSft,
                                                    _obj: $(_vacantLandSft),
                                                }
                                            }
                                        }
                                        
                                    },
                                    _cntr_vacantLandDetails: function () {
                                        var vacantLandDetails = tab_usage + " .cntr-vacant-land-details";
                                        return {
                                            _obj: $(vacantLandDetails),
                                            _selector: vacantLandDetails
                                        };
                                    },
                                    _cntr_occupencyStructDetails: function () {
                                        var occupencyStructDetails = tab_usage + " .cntr-occupency-struct-details";
                                        return {
                                            _obj: $(occupencyStructDetails),
                                            _selector: occupencyStructDetails
                                        };
                                    },
                                    hasOccupencyData: function () {
                                        var curTab = doms()._container()._tabs()._tab_usage();
                                        if (curTab._ctrl_holdingTypeGroup()._obj.val() == 4) {
                                            return false;
                                        } else if (!curTab._ctrl_holdingTypeGroup()._obj.val()) {
                                            return undefined;
                                        } else {
                                            return true;
                                        }
                                    },
                                    LiveOccupencyForm: function () {
                                        var parent = this;
                                        var curTab = function () { return doms()._container()._tabs()._tab_usage(); };
                                        var parentTable = function () { return curTab()._obj.find(".cntr-occupency-struct-details .usage-parent-table"); };
                                        var liveEvents = function () {
                                            var evntHndlers = {
                                                updateTotalCoveredArea: function (ctrl) {
                                                    var totalArea = 0;
                                                    $.each(ctrl.closest('table').find('tbody>tr'), function (ind,val) {
                                                        totalArea += helper.ifNaN($(val).find(".covered-area").val(), 0);
                                                    })
                                                    var footRow = ctrl.closest('table').find('tfoot>tr');
                                                    footRow.find(".lbl-total-area-sft").text(totalArea);
                                                    footRow.find(".hdn-total-area-sft").val(totalArea);
                                                }
                                            };
                                            doms()._container()._parent()._obj.off('click', ".usage-parent-table >thead .add-row");
                                            doms()._container()._parent()._obj.on('click', ".usage-parent-table >thead .add-row", function () {
                                                if (doms()._container()._parent()._obj.find(".usage-parent-table >tbody>tr").length > 0) {
                                                    doms()._container()._parent()._obj.find(".usage-parent-table >tbody>tr:last").clone().appendTo(doms()._container()._parent()._obj.find(".usage-parent-table >tbody"));
                                                    return;
                                                }
                                                
                                                var tr = $('<tr/>');
                                                //floor control
                                                var td = $('<td/>');
                                                tr.append(td.append(getFloorCtrl()));

                                                //add usage control
                                                td = $('<td/>');
                                                tr.append(td.append(getNewUsageControl()));

                                                //add Year control
                                                td = $('<td/>');
                                                tr.append(td.append(getYearCtrl()));

                                                //add covered area control
                                                td = $('<td/>');
                                                tr.append(td.append(getCoveredAreaCtrl()));

                                                //add Occupent Name control
                                                td = $('<td/>');
                                                tr.append(td.append(getOccupentNameCtrl())); 

                                                //add Occupent Name control
                                                td = $('<td/>');
                                                tr.append(td.append(getOccuDetailsCtrl()));

                                                //add monthly rent control
                                                td = $('<td/>');
                                                tr.append(td.append(getMonthlyRentCtrl()));

                                                //add monthly srv. charge control
                                                td = $('<td/>');
                                                tr.append(td.append(getMonthlyServiceChrgCtrl()));

                                                //add monthly srv. charge control
                                                td = $('<td/>');
                                                tr.append(td.append(getDetailsRoomBtnCtrl()));

                                                //add delete button
                                                td = $('<td/>');
                                                tr.append(td.append(getDeleteRowBtnCtrl()));

                                                parentTable().find('>tbody').append(tr);
                                            });

                                            doms()._container()._parent()._obj.off('click', ".usage-parent-table >tbody>tr .btn-room-details");
                                            doms()._container()._parent()._obj.on('click', ".usage-parent-table >tbody>tr .btn-room-details", function (e) {
                                                e.preventDefault();
                                                if (helper.ifNullEmptyOrUndefinded($(this).closest('tr').find('.usage').val(), 0) == 0) {
                                                    alertify.alert("Error!", "please select usage type on current floor", function (e) { });
                                                    return;
                                                }
                                                var btn = $(this);
                                                $.ajax({
                                                    type: "POST",
                                                    url: '/Administration/ScheduleThree/GetRoomTypes',
                                                    data: JSON.stringify({ UsageId: helper.ifNullEmptyOrUndefinded($(this).closest('tr').find('.usage').val(), 0) }),
                                                    dataType: 'json',
                                                    contentType: 'application/json',
                                                    success: function (r) {
                                                        if (r.Status == 200) {
                                                            globalVariables.masterData.RoomTypes = r.Data;
                                                            var modal = parentTable().siblings('.modal-room-details');
                                                            setTimeout(function () {
                                                                modal
                                                               .off('hidden.bs.modal')
                                                               .on('hidden.bs.modal', function () {
                                                                   //on close modal
                                                                   var modifiedModalContent = modal.find('.modal-body').find('div.table-responsive').has(".table-room-details");
                                                                   modifiedModalContent.hide().insertAfter(btn);
                                                               });
                                                                var tergateContent = btn.siblings('div.table-responsive').has(".table-room-details").show();
                                                                modal.find('.modal-body').append(tergateContent);
                                                                modal.modal('show');
                                                            }, 50);
                                                        } else {
                                                            alertify.alert('error', r.Message, function () { });
                                                        }
                                                    },
                                                    error: function (error) {
                                                        alertify.error(error.responseText);
                                                    }
                                                });

                                                
                                            });

                                            //room details
                                            doms()._container()._parent()._obj.off('click', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>thead .add-row-child");
                                            doms()._container()._parent()._obj.on('click', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>thead .add-row-child", function () {
                                                //if more that 0 rows available then clone and add last row
                                                if (doms()._container()._parent()._obj.find(".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody>tr").length > 0) {
                                                    doms()._container()._parent()._obj.find(".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody>tr:last").clone().appendTo(doms()._container()._parent()._obj.find(".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody"));
                                                    return;
                                                }
                                                //Room Type	Width	Lenght	Covered Area	Roof Type	Wall Type	Floor Type
                                                var locHelper = {};
                                                locHelper.getRoomTypeCtrl = function (selectedId) {
                                                    var data = globalVariables.masterData.RoomTypes;
                                                    var select = $('<select class="form-control room-type"/>').append('<option value="">--select--</option>');
                                                    $.each(data, function (ind, val) {
                                                        var selected = '';
                                                        if (selectedId && val.Key == selectedId) {
                                                            selected = 'selected';
                                                        }
                                                        select.append('<option ' + selected + ' value="' + val.Key + '">' + val.Value + '</option>');
                                                    })
                                                    return select;
                                                };
                                                locHelper.getWitdhCtrl = function (value) {
                                                    return $('<input type="text" class="form-control width" placeholder="width" value="' + (value ? value : "") + '"/>')
                                                };
                                                locHelper.getLengthCtrl = function (value) {
                                                    return $('<input type="text" class="form-control length" placeholder="length" value="' + (value ? value : "") + '"/>')
                                                };
                                                locHelper.getCoveredArearCtrl = function (value) {
                                                    return $('<input type="text" class="form-control covered-area" disabled placeholder="Floor area" value="' + (value ? value : "") + '"/>')
                                                };
                                                locHelper.getRoofTypeCtrl = function (selectedId) {
                                                    var data = globalVariables.masterData.RoofTypes;
                                                    var select = $('<select class="form-control roof-type"/>').append('<option value="">--select--</option>');
                                                    $.each(data, function (ind, val) {
                                                        var selected = '';
                                                        if (selectedId && val.Key == selectedId) {
                                                            selected = 'selected';
                                                        }
                                                        select.append('<option ' + selected + ' value="' + val.Key + '">' + val.Value + '</option>');
                                                    })
                                                    return select;
                                                };
                                                locHelper.getWallTypeCtrl = function (selectedId) {
                                                    var data = globalVariables.masterData.WallTypes;
                                                    var select = $('<select class="form-control wall-type"/>').append('<option value="">--select--</option>');
                                                    $.each(data, function (ind, val) {
                                                        var selected = '';
                                                        if (selectedId && val.Key == selectedId) {
                                                            selected = 'selected';
                                                        }
                                                        select.append('<option ' + selected + ' value="' + val.Key + '">' + val.Value + '</option>');
                                                    })
                                                    return select;
                                                };
                                                locHelper.getFloorTypeCtrl = function (selectedId) {
                                                    var data = globalVariables.masterData.FloorTypes;
                                                    var select = $('<select class="form-control floor-type"/>').append('<option value="">--select--</option>');
                                                    $.each(data, function (ind, val) {
                                                        var selected = '';
                                                        if (selectedId && val.Key == selectedId) {
                                                            selected = 'selected';
                                                        }
                                                        select.append('<option ' + selected + ' value="' + val.Key + '">' + val.Value + '</option>');
                                                    })
                                                    return select;
                                                };
                                                locHelper.getDeleteRowBtnCtrl = function (selectedId) {
                                                    return $('<a href="javascript:void(0)" class="glyphicon glyphicon-minus del-row"></a>');
                                                };

                                                var getNewRow = function () {
                                                    var tr = $('<tr/>');
                                                    //room type added
                                                    var td = $('<td/>')
                                                    td.append(locHelper.getRoomTypeCtrl());
                                                    tr.append(td)
                                                    //width added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getWitdhCtrl());
                                                    tr.append(td)
                                                    //length added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getLengthCtrl());
                                                    tr.append(td)
                                                    //covered area added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getCoveredArearCtrl());
                                                    tr.append(td)
                                                    //roof type added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getRoofTypeCtrl());
                                                    tr.append(td)
                                                    //wall type added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getWallTypeCtrl());
                                                    tr.append(td)
                                                    //floor type added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getFloorTypeCtrl());
                                                    tr.append(td)
                                                    //delete row buttom added
                                                    td = $('<td/>')
                                                    td.append(locHelper.getDeleteRowBtnCtrl());
                                                    tr.append(td);
                                                    return tr;
                                                };
                                                $(this).closest('table').find('tbody').append(getNewRow());
                                            });

                                            var scop1 = ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody>tr";
                                            doms()._container()._parent()._obj.off('change', scop1.concat(" .width").concat(",").concat(scop1).concat(" .length"));
                                            doms()._container()._parent()._obj.on('change', scop1.concat(" .width").concat(",").concat(scop1).concat(" .length"), function () {
                                                evntHndlers.updateTotalCoveredArea($(this));
                                            });

                                            doms()._container()._parent()._obj.off('click', '.usage-parent-table >tbody>tr .del-row');
                                            doms()._container()._parent()._obj.on('click', '.usage-parent-table >tbody>tr .del-row', function () {
                                                $(this).closest('tr').remove();
                                            });

                                            doms()._container()._parent()._obj.off('click', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody .del-row");
                                            doms()._container()._parent()._obj.on('click', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details>tbody .del-row", function () {
                                                $(this).closest('tr').remove();
                                            });

                                            doms()._container()._parent()._obj.off('keyup', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details .width,.usage-parent-table ~.modal-room-details .modal-body table.table-room-details .length");
                                            doms()._container()._parent()._obj.on('keyup', ".usage-parent-table ~.modal-room-details .modal-body table.table-room-details .width,.usage-parent-table ~.modal-room-details .modal-body table.table-room-details .length", function () {
                                                var tr = $(this).closest('tr');
                                                var length =helper.ifNaN( tr.find('.length').val(),0);
                                                var width =helper.ifNaN( tr.find('.width').val(),0);
                                                tr.find('.covered-area').val(length*width);
                                            });

                                            //on change usage on occupency and structure detail
                                            doms()._container()._parent()._obj.off('change', ".usage-parent-table >tbody .usage");
                                            doms()._container()._parent()._obj.on('change', ".usage-parent-table >tbody .usage", function (e,t,s) {
                                                var curTab = $(tab_usage);
                                                var usageDdl = $(this);
                                                var curRow = usageDdl.closest('tr');
                                                if (t && t.evt === "page_load") {
                                                    //skip
                                                }
                                                else{
                                                    if (curRow.find("table.table-room-details>tbody>tr").length > 0) {
                                                        alertify.alert("Error! Changing usage type", "Please delete rooms on current floor", function () { });
                                                        e.preventDefault();
                                                        $(this).find('option[value="' + helper.ifNullEmptyOrUndefinded($(this).attr('data-last-selected'), "") + '"]').prop('selected', true);
                                                        return;
                                                    }
                                                }

                                                $(this).attr('data-last-selected', $(this).val());
                                                var holdingTypeGroupCtrl = curTab.find(".holding-type-group");
                                                
                                                var ownerName = doms()._container()._ctrl_Name()._obj.val();
                                                if ([1, 11, 47].indexOf(helper.ifNaN(usageDdl.val()))>=0) {
                                                    curRow.find(".occu-detail").prop('disabled', true);
                                                    curRow.find(".monthly-rent").prop('disabled', true);
                                                    curRow.find(".monthly-rent").prop('disabled', true);
                                                    curRow.find(".monthly-srv-chrg").prop('disabled', true);
                                                    if (helper.ifNullEmptyOrUndefinded(curRow.find(".occupent-name").val(), 0) == 0) {
                                                        curRow.find(".occupent-name").val(ownerName);
                                                    }
                                                } else {
                                                    curRow.find(".occu-detail").prop('disabled', false);
                                                    curRow.find(".monthly-rent").prop('disabled', false);
                                                    curRow.find(".monthly-rent").prop('disabled', false);
                                                    curRow.find(".monthly-srv-chrg").prop('disabled', false);
                                                }
                                            });
                                        };

                                        var getFloorCtrl = function (selectedId) {
                                            var data = globalVariables.masterData.Floors;
                                            var select = $('<select class="form-control floor"/>').append('<option value="">--select--</option>');
                                            $.each(data, function (ind, val) {
                                                var selected = '';
                                                if (selectedId && val.Key == selectedId) {
                                                    selected = 'selected';
                                                }
                                                select.append('<option ' + selected + ' value="' + val.Key + '">' + val.Value + '</option>');
                                            })
                                            return select;
                                        };
                                        var getYearCtrl = function (value) {
                                            return $('<input type="text" class="form-control year" placeholder="Year" value="' + (value ? value : "") + '"/>')
                                        }
                                        var getCoveredAreaCtrl = function (value) {
                                            return $('<input type="text" class="form-control covered-area" placeholder="covered area" value="' + (value ? value : "") + '"/>')
                                        }
                                        var getOccupentNameCtrl = function (value) {
                                            return $('<input type="text" class="form-control occupent-name" placeholder="occupent name" value="' + (value ? value : "") + '"/>')
                                        }
                                        var getOccuDetailsCtrl = function (value) {
                                            return $('<input type="text" class="form-control occu-detail" placeholder="occu detail" value="' + (value ? value : "") + '"/>')
                                        };
                                        var getMonthlyRentCtrl = function (value) {
                                            return $('<input type="text" class="form-control monthly-rent" placeholder="monthly rent" value="' + (value ? value : "") + '"/>')
                                        }

                                        var getMonthlyServiceChrgCtrl = function (value) {
                                            return $('<input type="text" class="form-control monthly-srv-chrg" placeholder="monthly service charge" value="' + (value ? value : "") + '"/>')
                                        };
                                        var getDetailsRoomBtnCtrl = function () {
                                            var guid = 'M' + uuid().replace(/-/g, "").substring(0, 10);//replace '-' by '' and take 30 character
                                            var div = $('<div/>');
                                            div.append('<input type="button" value="Room Details" class="btn btn-danger btn-room-details" >');
                                            div.append($("<div class='table-responsive'>"
                                                                    + "<table class='table table-striped table-bordered table-hover table-condensed table-room-details'>"
                                                                       + "<thead class='bg-aqua'>"
                                                                          + "<tr>"
                                                                             + "<th style='width: 154px;'>Room Type</th>"
                                                                             + "<th style='width: 100px;'>Width (ft.)</th>"
                                                                             + "<th style='width: 100px;'>Lenght (ft.)</th>"
                                                                             + "<th style='width: 100px;'>Floor Area (sft.)</th>"
                                                                             + "<th>Roof Type</th>"
                                                                             + "<th>Wall Type</th>"
                                                                             + "<th>Floor Type</th>"
                                                                             + "<th><a href='javascript:void(0)' class='glyphicon glyphicon-plus add-row-child white'></a></th>"
                                                                          + "</tr>"
                                                                       + "</thead>"
                                                                       + "<tbody></tbody>"
                                                                    + "</table>"
                                                        + "</div>").hide());
                                            return div;
                                        };
                                        var getDeleteRowBtnCtrl = function (selectedId) {
                                            return $('<a href="javascript:void(0)" class="glyphicon glyphicon-minus del-row"></a>');
                                        };
                                        (function () {
                                            liveEvents();
                                        }());
                                    },
                                    repopulateUsageOnParentTable: function () {
                                        var parentTable = doms()._container()._tabs()._tab_usage()._obj.find(".cntr-occupency-struct-details .usage-parent-table");

                                        $.each(parentTable.find('tbody tr'), function (ind, val) {
                                            var newUsageControl = getNewUsageControl();
                                            var tr = $(val);
                                            tr.find('td:has(select.usage)').empty().append(newUsageControl);
                                        });
                                    },
                                    _countOccupencyRows: function () {
                                        var curTab = function () { return doms()._container()._tabs()._tab_usage(); };
                                        var parentTable = function () { return curTab()._obj.find(".cntr-occupency-struct-details .usage-parent-table"); };
                                       return parentTable().find('tbody tr').length;
                                    },
                                };
                            },
                            _tab_others: function () {
                                var tabothers = tabContainer + " .tab-others";
                                return {
                                    _obj: $(tabothers),
                                    _selector: tabothers,
                                    _ctrl_whetherAssessed: function () {
                                        var whetherAssessed = tabothers + " .is-assessed:checked";
                                        return {
                                            _obj: $(whetherAssessed),
                                            _selector: whetherAssessed,
                                        };
                                    },
                                    _ctrl_assmtYear: function () {
                                        var assmtYear = tabothers + " .assessment-year";
                                        return {
                                            _obj: $(assmtYear),
                                            _selector: assmtYear,
                                        };
                                    },
                                    _ctrl_AnnualValuation: function () {
                                        var AnnualValuation = tabothers + " .annual-valuation";
                                        return {
                                            _obj: $(AnnualValuation),
                                            _selector: AnnualValuation,
                                        };
                                    },
                                    _ctrl_QtrlyPropTax: function () {
                                        var QtrlyPropTax = tabothers + " .qtrly-prop-tax";
                                        return {
                                            _obj: $(QtrlyPropTax),
                                            _selector: QtrlyPropTax,
                                        };
                                    },
                                    _ctrl_isOwnerShipChanged: function () {
                                        var isOwnershipChanged = tabothers + " .is-ownershp-changed:checked";
                                        return {
                                            _obj: $(isOwnershipChanged),
                                            _selector: isOwnershipChanged
                                        }
                                    },
                                    _ctrl_OwnershpChangedDetails: function () {
                                        var OwnershpChangedDetails = tabothers + " .ownershp-changed-details";
                                        return {
                                            _obj: $(OwnershpChangedDetails),
                                            _selector: OwnershpChangedDetails,
                                        };
                                    },
                                    _ctrl_isNatureOfUsedChange: function () {
                                        var isNatureOfUsedChange = tabothers + " .is-natureOfUsed-changed:checked";
                                        return {
                                            _obj: $(isNatureOfUsedChange),
                                            _selector: isNatureOfUsedChange
                                        }
                                    },
                                    _ctrl_natureOfUsedChangedDetails: function () {
                                        var natureOfUsedChangedDetails = tabothers + " .natureOfUsed-changed-details";
                                        return {
                                            _obj: $(natureOfUsedChangedDetails),
                                            _selector: natureOfUsedChangedDetails,
                                        };
                                    },
                                    _ctrl_isAddAlteredModed: function () {
                                        var isAddAlteredModed = tabothers + " .is-add-altr-moded:checked";
                                        return {
                                            _obj: $(isAddAlteredModed),
                                            _selector: isAddAlteredModed
                                        }
                                    },
                                    _ctrl_addAltrModedDetails: function () {
                                        var addAltrModedDetails = tabothers + " .add-altr-moded-details";
                                        return {
                                            _obj: $(addAltrModedDetails),
                                            _selector: addAltrModedDetails,
                                        };
                                    },
                                    _ctrl_totalAreaOfApertment: function () {
                                        var totalArea = tabothers + " .total-area-apartment";
                                        return {
                                            _obj: $(totalArea),
                                            _selector: totalArea,
                                        };
                                    },
                                    _ctrl_otherArea: function () {
                                        var otherArea = tabothers + " .other-area";
                                        return {
                                            _obj: $(otherArea),
                                            _selector: otherArea,
                                        };
                                    }
                                };
                            },
                            _tab_images: function () {
                                var tabImages = tabContainer + " .tab-images";
                                return {
                                    _obj: $(tabImages),
                                    _selector: tabImages,
                                    _ctrl_lat: function () {
                                        var lat = tabImages + " .geo-loc-lat";
                                        return {
                                            _obj: $(lat),
                                            _selector: lat,
                                        };
                                    },
                                    _ctrl_long: function () {
                                        var long = tabImages + " .geo-loc-long";
                                        return {
                                            _obj: $(long),
                                            _selector: long,
                                        };
                                    },
                                    _ctrl_imageUploadCtrls: function () {
                                        var long = tabImages + " .img-upload";
                                        return {
                                            _obj: $(long),
                                            _selector: long,
                                            getViewer: function (uploadControl) {
                                                if (uploadControl) {
                                                    return $(uploadControl).parent().siblings('img');
                                                }
                                            }
                                        };
                                    }
                                };
                            }
                        }
                    }
                };
            },
        };
    };
    var getFormData = function () {
        var obj = {
            Address: doms()._container()._ctrl_Address()._obj.val(),
            Name: doms()._container()._ctrl_Name()._obj.val(),
            Email: helper.ifNullEmptyOrUndefinded( doms()._container()._ctrl_email()._obj.val(),null),
            Phone: helper.ifNullEmptyOrUndefinded( doms()._container()._ctrl_phone()._obj.val(),null),
            ApplicationDate: helper.convertTodate("dd/mm/yy",helper.ifNullEmptyOrUndefinded( doms()._container()._ctrl_applyDate()._obj.val(),null)),
            MunicipalityId: helper.ifNaN($(".ddl-municipality").val(), null),
            AssmtId:helper.ifNaN(doms()._container()._ctrl_assmtId()._obj.val(),null),
            LocationTab: function () {
                var tabRef = doms()._container()._tabs()._tab_location();
                var otherInfos = function () {
                    return tabRef._obj.find(".location-other-info").map(function (i, v) {
                        return {
                            DatId: null,
                            DataType: $(v).attr("data-datatype"),
                            InfoType: $(v).attr("data-infotype"),
                            InfoTypeId: $(v).attr("data-infotypeid"),
                            TypeCode: $(v).attr('data-typecode'),
                            TypeDesc: $(v).attr('data-typedesc'),
                            Value: helper.ifNaN( $(v).val(),0)
                        }
                    }).get();
                }
                var deeddate = tabRef._ctrl_deedDate()._obj.val();
                if (deeddate) {
                    deeddate= helper.convertTodate("dd/mm/yy", deeddate);
                }
                return {
                    Borough: tabRef._ctrl_borough()._obj.val(),
                    Complex: tabRef._ctrl_complex()._obj.val(),
                    DagNo_Lr: tabRef._ctrl_dag_lr()._obj.val(),
                    DagNo_Rs: tabRef._ctrl_dag_rs()._obj.val(),
                    DeedDate: deeddate,
                    DeedNo: tabRef._ctrl_deedNo()._obj.val(),
                    DeetType: tabRef._ctrl_deedType()._obj.val(),
                    Holding: tabRef._ctrl_holding()._obj.val(),
                    JlNo: tabRef._ctrl_jlno()._obj.val(),
                    Khatian_Lr: helper.ifNaN(tabRef._ctrl_khatian_lr()._obj.val(),0),
                    Khatian_Rs: helper.ifNaN(tabRef._ctrl_khatian_rs()._obj.val(),0),
                    LandArea_dec: helper.ifNaN(tabRef._ctrl_landarea_dec()._obj.val(),0),
                    LandArea_sft: helper.ifNaN(tabRef._ctrl_landarea_sft()._obj.val(),0),
                    LandType: tabRef._ctrl_landType()._obj.val(),
                    MouzaName: tabRef._ctrl_mouzaname()._obj.val(),
                    OtherInfos: otherInfos(),
                    PS: tabRef._ctrl_ps()._obj.val(),
                    RoadLane: tabRef._ctrl_roadlane()._obj.val(),
                    RoadLaneId:helper.ifNaN( tabRef._ctrl_hdnRoadlane()._obj.val(),0),
                    RorNature: tabRef._ctrl_rornature()._obj.val(),
                    Ward: tabRef._ctrl_ward()._obj.val(),
                    WardID: helper.ifNaN(tabRef._ctrl_hdnWard()._obj.val(), 0)
                }
            },
            UsageTab: function () {
                var tabRef=doms()._container()._tabs()._tab_usage();
                var getFloorData = function () {
                    var getRoomDetails = function (table,occuId) {
                        return table.find('tbody tr').map(function (i, v) {
                            return {
                                TabId: null,
                                TabIdOccu: occuId,
                                RoomTypeId: $(v).find('.room-type').val(),
                                WSft: helper.ifNaN( $(v).find('.width').val(),0),
                                LSft:helper.ifNaN(  $(v).find('.length').val(),0),
                                HSft: 0,//not in used
                                CoverdAreaSft: (helper.ifNaN($(v).find('.width').val(), 0) * helper.ifNaN($(v).find('.length').val(), 0)),
                                RoomDetails: null,
                                RoofType: $(v).find('.roof-type').val(),
                                WallType: $(v).find('.wall-type').val(),
                                FloorType: $(v).find('.floor-type').val(),
                                MunicipalityID: null
                            }
                        }).get();
                    };
                   return tabRef._obj.find("table.usage-parent-table >tbody >tr").map(function (i, v) {
                       return {
                           TabId: null,
                           FloorID: $(v).find('.floor').val(),
                           ConstYear: $(v).find('.year').val(),
                           UseId: $(v).find('.usage').val(),
                           NameOfOccupant: $(v).find('.occupent-name').val(),
                           OccuDetails: $(v).find('.occu-detail').val(),
                           TotCoverdAreaSft: $(v).find('.covered-area').val(),
                           MonRent: $(v).find('.monthly-rent').val(),
                           MonSc: $(v).find('.monthly-srv-chrg').val(),
                           MunicipalityID: null,
                           Rooms: getRoomDetails($(v).find('.table-room-details'),null)
                       }
                    }).get();
                };
                var getVacantLand = function () {
                    var landType = function () {
                        var ctrlLandType = tabRef._obj.find(".ddl-vacant-land-type");
                        return {
                            DatId: null,
                            DataType: $(ctrlLandType).attr("data-datatype"),
                            InfoType: $(ctrlLandType).attr("data-infotype"),
                            InfoTypeId: $(ctrlLandType).attr("data-infotypeid"),
                            TypeCode: $(ctrlLandType).attr('data-typecode'),
                            TypeDesc: $(ctrlLandType).attr('data-typedesc'),
                            Value: helper.ifNaN($(ctrlLandType).val(),0)
                        };
                    };
                    return { Area_dec: helper.ifNaN(tabRef ._ctrl_vacantLand()._ctrl_vacantLandDec()._obj.val(), 0), Area_sft: helper.ifNaN(tabRef._ctrl_vacantLand()._ctrl_vacantLandSft()._obj.val(), 0), LandType: landType() };
                };
                var hasOccupencyData=tabRef.hasOccupencyData();
                return {
                    HoldingTypeGroupId:tabRef._ctrl_holdingTypeGroup()._obj.val(),
                    Floors:(hasOccupencyData==true?getFloorData():[]) ,
                    VacantLand: (hasOccupencyData == false ? getVacantLand() : null)
                }
            },
            OtherTab: function () {
                var tabRef = doms()._container()._tabs()._tab_others();

                var isAssessed = tabRef._ctrl_whetherAssessed()._obj.val();
                isAssessed = (isAssessed === "Y" ? true : false);

                var IsAddAltrModed = tabRef._ctrl_isAddAlteredModed()._obj.val();
                IsAddAltrModed = (IsAddAltrModed === "Y" ? true : false);

                var IsOwnershpChanged = tabRef._ctrl_isOwnerShipChanged()._obj.val();
                IsOwnershpChanged = (IsOwnershpChanged === "Y" ? true : false);

                var IsNatureOfUsedChanged = tabRef._ctrl_isNatureOfUsedChange()._obj.val();
                IsNatureOfUsedChanged = (IsNatureOfUsedChanged === "Y" ? true : false);

                var ctrlAssmtYear = tabRef._ctrl_assmtYear()._obj;
                var AssessmentYear = null;
                if (ctrlAssmtYear.length > 0) {
                    AssessmentYear = {
                        DatId: null,
                        DataType: (ctrlAssmtYear).attr("data-datatype"),
                        InfoType: (ctrlAssmtYear).attr("data-infotype"),
                        InfoTypeId: (ctrlAssmtYear).attr("data-infotypeid"),
                        TypeCode: (ctrlAssmtYear).attr('data-typecode'),
                        TypeDesc: (ctrlAssmtYear).attr('data-typedesc'),
                        Value: (ctrlAssmtYear).val()
                    }
                }
                

                var obj = {
                    WheatherAssessed: isAssessed,
                    AssessmentYear: ((isAssessed && AssessmentYear) ? AssessmentYear : null),
                    AnnualValuation:helper.ifNaN( isAssessed? tabRef._ctrl_AnnualValuation()._obj.val():null,0),
                    QtrlyPropTax:helper.ifNaN( isAssessed? tabRef._ctrl_QtrlyPropTax()._obj.val():null,0),
                    IsOwnershpChanged: IsOwnershpChanged,
                    OwnershpChanged_Desc: IsOwnershpChanged ? tabRef._ctrl_OwnershpChangedDetails()._obj.val() : null,
                    IsNatureOfUsedChanged: IsNatureOfUsedChanged,
                    NatureOfUsedChanged_Desc:IsNatureOfUsedChanged? tabRef._ctrl_natureOfUsedChangedDetails()._obj.val():null,
                    IsAddAltrModed: IsAddAltrModed,
                    AddAltrModed_Desc: IsAddAltrModed?tabRef._ctrl_addAltrModedDetails()._obj.val():null,
                    OtherArea_sft: helper.ifNaN(tabRef._ctrl_otherArea()._obj.val(),0),
                    TotalAreaApartment_sft: helper.ifNaN(tabRef._ctrl_totalAreaOfApertment()._obj.val(),0),
                }
                return obj;
            },
            ImageTab: function () {
                var tabRef = doms()._container()._tabs()._tab_images();
                var getImageDatas = function () {
                    return tabRef._obj.find(".img-upload").map(function (i, v) {
                        if (v.files.length > 0) {
                            return {
                                DatId: null,
                                DataType: $(v).attr("data-datatype"),
                                InfoType: $(v).attr("data-infotype"),
                                InfoTypeId: $(v).attr("data-infotypeid"),
                                TypeCode: $(v).attr('data-typecode'),
                                TypeDesc: $(v).attr('data-typedesc'),
                                Value: null
                            };
                        }
                    }).get();
                }
                var obj1 = {
                    GeoLocation: {
                        TabId: null,
                        Geo_Lat: helper.ifNaN( tabRef._ctrl_lat()._obj.val(),null),
                        Geo_Long: helper.ifNaN(tabRef._ctrl_long()._obj.val(), null)
                    },
                    Images:[]
                }
                return obj1;
            },
            UserVerificationData: function () {
                var authenticationModal = $("#modal-authenticate-n-save");
                var ddlAuthenticationType = authenticationModal.find(".ddl-authentication-type");
                var txtOtp=authenticationModal.find(".txt-otp");
                var authenticationType = 2;//otp
                var reslt = {};
                if (ddlAuthenticationType.val() == "SIG") {
                    authenticationType = 1;
                } else if (ddlAuthenticationType.val() == "OTP") {
                    authenticationType = 2;
                    reslt.VerificationData = { OTP: txtOtp.val() }
                    
                }
                reslt.VerificationType = authenticationType;
                return reslt;
            }
        };
        return {
            Address: obj.Address,
            Name: obj.Name,
            AssmtId: obj.AssmtId,
            Email: obj.Email,
            Phone: obj.Phone,
            ApplicationDate:obj.ApplicationDate,
            MunicipalityId:obj.MunicipalityId,
            LocationTab: obj.LocationTab(),
            UsageTab: obj.UsageTab(),
            OtherTab: obj.OtherTab(),
            ImageTab: obj.ImageTab(),
            UserVerificationData: obj.UserVerificationData()
        }
    };
    var formValidator = function () {
        var config = {
            ignore: [],
            rules: {
                name: { required: true },
                email: { email: true },
                mobile: { required: true, digits: true, minlength: 10, maxlength: 10 },
                address: { required: true },

            },
            messages: {},
            errorPlacement: function (error, element) {
                if (element.closest(".input-group").length > 0) {
                    error.insertAfter(element.closest(".input-group"));
                }
                else {
                    error.insertAfter(element);
                }
                error.css('color', 'red');
            }

        };
        config.rules["hdn-ward"] = {
            required: true, min: 1
        };
        config.rules["holding"] = {
            required: true
        };
        config.rules["hdn-roadlane"] = { required: true };
        config.rules["land-type"] = { required: true };
        //config.rules["mouzaname"] = { required: true };
        //config.rules["jlno"] = { required: true };
        //config.rules["ps"] = { required: true };
        config.rules["holding-type-group"] = { required: true };
        //config.rules["dag-rs"] = { required: true };
        //config.rules["dag-lr"] = { required: true};


        config.messages["mobile"] = { digit: "Invalid phone no", minlength: "Invalid phone no", maxlength: "Invalid phone no" };
        config.messages["hdn-ward"] = { min: "Invalid Ward" };
        //config.messages["dag-rs"] = { required: "Dag No Rs Required." };
        //config.messages["dag-lr"] = { required: "Dag No LR Required."};

       var validate= $('form').validate(config);
        return {
            isValid: function () {
               return $('form').valid();
            },
            getErrors: function () {
                return validate.errorList;
            }
        };
    };
    var processWithFormData = function (grabbedFormData) {
        var formDataObj = new FormData();
        $.each($(".img-upload"), function (ind, val) {
            var fileUpload = $(this).get(0);
            var files = fileUpload.files;
            if (files.length > 0) {
                formDataObj.append($(val).attr('data-typecode'), files[0]);
                grabbedFormData.ImageTab.Images.push(
                    {
                        InfoTypeId: $(val).attr('data-infotypeid'),
                        TypeCode: $(val).attr('data-typecode'),
                        TypeDesc: $(val).attr('data-typedesc'),
                        DataType: $(val).attr('data-datatype'),
                        InfoType: $(val).attr('data-infotype'),
                        TabId: null,
                        Value:null,
                    }
                    );
            }
        });
        //for signature
        if (grabbedFormData.UserVerificationData.VerificationType === 1) {
            var fileUpload = $("#modal-authenticate-n-save .img-upload-signature").get(0);
            var files = fileUpload.files;
            if (files.length > 0) {
                formDataObj.append("SIG", files[0]);
            } else {
                throw "Please upload signature file";
            }
        }
        //if otp empty
        else if (grabbedFormData.UserVerificationData.VerificationType == 2) {
            if (helper.ifNaN(grabbedFormData.UserVerificationData.VerificationData['OTP'], 0) === 0) {
                throw "Please enter OTP";
            }
        } else {
            throw 'Invalid Authentication Type Selected';
        }

        formDataObj.append('FormData',JSON.stringify( grabbedFormData));
        return formDataObj;
    }
    var _evtHndlers = function () {
        return {
            //on click next or previous button of tab footer
            onClickFooterTabNav: function () {
                var x = $(this).attr('href');
                var ctrl = $("a[href='" + x + "'");
                if (ctrl.parent('li').length > 0) {
                    ctrl.parent('li').siblings('li').removeClass('active');
                    ctrl.parent('li').addClass('active');
                }
            },
            //on change usage.holding-type-group dropdown
            
            //on grab geolocation
            onGrabGeoLocation: function (ctrl, location) {
                if (location) {
                    var scope = doms()._container()._tabs()._tab_images();
                    scope._ctrl_lat()._obj.val(location.lat);
                    scope._ctrl_long()._obj.val(location.long);
                    ctrl.closest(".modal").modal('hide');
                } else {
                    alertify.error("Please select location on map");
                }
            },
            afterPopulateForm: function () {
                if ($(".Is-citizen").length > 0 && $(".Is-citizen").val() === 'yes' && $(".citizen-data").length > 0) {
                    var citizenData = JSON.parse($(".citizen-data").val());
                    if (citizenData) {

                        doms()._container()._ctrl_Name()._obj.val(helper.ifNullEmptyOrUndefinded( citizenData.AssesseeName,""));
                        doms()._container()._ctrl_email()._obj.val(helper.ifNullEmptyOrUndefinded( citizenData.PrimaryEmail,""));
                        doms()._container()._ctrl_phone()._obj.val(helper.ifNullEmptyOrUndefinded( citizenData.PrimaryPhNo,""));
                        doms()._container()._ctrl_Address()._obj.val(helper.ifNullEmptyOrUndefinded(citizenData.AssesseeAddress, ""));
                        var tab = doms()._container()._tabs()._tab_location();
                        tab._ctrl_ward()._obj.val(citizenData.WardName);
                        tab._ctrl_hdnWard()._obj.val(citizenData.WardID);
                        tab._ctrl_holding()._obj.val(citizenData.HoldingNo);
                        tab._ctrl_roadlane()._obj.val(citizenData.LocationName);
                        tab._ctrl_hdnRoadlane()._obj.val(citizenData.LocationID);

                        tab._ctrl_ward()._obj.prop('disabled',true);
                        tab._ctrl_hdnWard()._obj.prop('disabled', true);
                        tab._ctrl_holding()._obj.prop('disabled', true);
                        tab._ctrl_roadlane()._obj.prop('disabled', true);


                        //{"AssesseeID":86266,"ParentAssesseeID":null,"AssesseeNo":"1200100086266","MunicipalityID":1,"MunicipalityName":null,"WardID":1,"WardName":null,"LocationID":1,"LocationName":null,"HoldingTypeID":4,"HoldingTypeDesc":"RESIDENTIAL","HoldingNo":"1/1","AssesseeName":"SANTI RANJAN SARKAR","AssesseeAddress":null,"UserType":1,"UserID":86266};
                    }
                    var mobile = doms()._container()._ctrl_phone()._obj;
                    mobile.val(mobile.attr('value'));
                }

                    doms()._container()._tabs()._tab_location()._ctrl_landType()._obj.trigger('change');
                    //Plot/Dag no, in lr and rs one filed is given another is disabled
                    doms()._container()._tabs()._tab_location()._ctrl_dag_lr()._obj.keyup();
                    doms()._container()._tabs()._tab_location()._ctrl_dag_rs()._obj.keyup();
         
                    //Khatian no, in lr and rs one filed is given another is disabled
                    doms()._container()._tabs()._tab_location()._ctrl_khatian_rs()._obj.keyup();
                    doms()._container()._tabs()._tab_location()._ctrl_khatian_lr()._obj.keyup();
                    doms()._container()._tabs()._tab_usage()._obj.find(".usage-parent-table >tbody .usage").trigger('change', { evt: 'page_load' });
                    doms()._container()._tabs()._tab_usage()._ctrl_holdingTypeGroup()._obj.trigger('change', {evt:'page_load'});
            }
        };
    };
    var registerEvents = function () {
        $(document).on('click',"a[data-toggle='tab'].btn", _evtHndlers().onClickFooterTabNav);
       // $(document).on('change', doms()._container()._tabs()._tab_usage()._ctrl_holdingTypeGroup()._selector, _evtHndlers().onChangeHoldingTypeGroup);
        $(document).on('change', doms()._container()._tabs()._tab_usage()._ctrl_holdingTypeGroup()._selector, function (ev, arg) {
            var usageTab = function () { return doms()._container()._tabs()._tab_usage(); };
            var evt = '';
            if (arg && arg.evt) {
                evt = arg.evt;
            }
            if (usageTab()._countOccupencyRows() > 0 && evt != 'page_load') {
                alertify.alert("Warning!", "Please delete all occupency data from table then change holding type group", function () { });
                var lastSelected = $(this).attr('data-last-selected');
                $(this).find('option[value="' + (lastSelected ? lastSelected : '') + '"]').prop('selected', true);
                ev.preventDefault();
                return false;
            }
            if ($(this).val() == "") {
                doms()._container()._tabs()._tab_usage()._cntr_occupencyStructDetails()._obj.find('table:eq(0) >thead .add-row').hide();
            } else {
                doms()._container()._tabs()._tab_usage()._cntr_occupencyStructDetails()._obj.find('table:eq(0) >thead .add-row').show();
            }
            if ($(this).val() && $(this).val() == 4)//vacant land
            {
                doms()._container()._tabs()._tab_usage()._cntr_vacantLandDetails()._obj.show();
                doms()._container()._tabs()._tab_usage()._cntr_occupencyStructDetails()._obj.hide();

            } else {//occupency and structure details
                doms()._container()._tabs()._tab_usage()._cntr_vacantLandDetails()._obj.hide();
                doms()._container()._tabs()._tab_usage()._cntr_occupencyStructDetails()._obj.show();
                $.post('/Administration/ScheduleThree/Usages', { HldngTypGrpId: $(this).val() ? $(this).val() : 0 })
                .done(function (r) {
                    if (r.Status === 200) {
                        globalVariables.masterData.Usage = r.Data;
                        //doms()._container()._tabs()._tab_usage().repopulateUsageOnParentTable();
                    }
                }).fail(function (r) {
                    console.log(r);
                });
            }
            $(this).attr('data-last-selected', $(this).val());
        });
        $(document).on('click',".btn-apply-geo", function () {
            var location = {
                lat: Number($("#us3-lat").val()),
                long: Number($("#us3-lon").val())
            };
            if (location.lat && location.long) {
                _evtHndlers().onGrabGeoLocation($(this), location);
            } else {
                _evtHndlers().onGrabGeoLocation($(this), null);
            }
        });
        $(document).on('change', doms()._container()._tabs()._tab_others()._selector + " .is-assessed", function () {
            var scope = doms()._container()._tabs()._tab_others();
            var isEnabled = (scope._obj.find(".is-assessed:checked").val() == "Y" ? true : false);
            scope._ctrl_assmtYear()._obj.prop('disabled', !isEnabled)
            scope._ctrl_AnnualValuation()._obj.prop('disabled', !isEnabled)
            scope._ctrl_QtrlyPropTax()._obj.prop('disabled', !isEnabled)
            if (!isEnabled) {
                scope._ctrl_assmtYear()._obj.find("option:eq(0)").prop('selected', true);
                scope._ctrl_AnnualValuation()._obj.val(0);
                scope._ctrl_QtrlyPropTax()._obj.val(0);
            }
        });

        $(document).on('change', doms()._container()._tabs()._tab_others()._selector + " .is-natureOfUsed-changed", function () {
            var scope = doms()._container()._tabs()._tab_others();
            var isEnabled = (scope._obj.find(".is-natureOfUsed-changed:checked").val() == "Y" ? true : false);
            scope._ctrl_natureOfUsedChangedDetails()._obj.prop('disabled', !isEnabled);
            if (!isEnabled) {
                scope._ctrl_natureOfUsedChangedDetails()._obj.val('');
            }
        });

        $(document).on('change', doms()._container()._tabs()._tab_others()._selector + " .is-ownershp-changed", function () {
            var scope = doms()._container()._tabs()._tab_others();
            var isEnabled = (scope._obj.find(".is-ownershp-changed:checked").val() == "Y" ? true : false);
            scope._ctrl_OwnershpChangedDetails()._obj.prop('disabled', !isEnabled);
            if (!isEnabled) {
                scope._ctrl_OwnershpChangedDetails()._obj.val('');
            }
        });

        $(document).on('change', doms()._container()._tabs()._tab_others()._selector + " .is-add-altr-moded", function () {
            var scope = doms()._container()._tabs()._tab_others();
            var isEnabled = (scope._obj.find(".is-add-altr-moded:checked").val() == "Y" ? true : false);
            scope._ctrl_addAltrModedDetails()._obj.prop('disabled', !isEnabled);
            if (!isEnabled) {
                scope._ctrl_addAltrModedDetails()._obj.val('');
            }
        });
        //image upload control
        $(document).on('change', doms()._container()._tabs()._tab_images()._ctrl_imageUploadCtrls()._selector, function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var viewer = doms()._container()._tabs()._tab_images()._ctrl_imageUploadCtrls().getViewer($(input));
                    viewer.attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        });
        $(document).on('focus',doms()._container()._tabs()._tab_location()._ctrl_deedDate()._selector, function () {
            $(this).datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
        });
        
        //live occupency ui interaction
        doms()._container()._tabs()._tab_usage().LiveOccupencyForm();
        $(".ddl-municipality").on('change', function (e,arg) {
            if (helper.ifNaN($(this).val(), null) !== null) {
                var payoad = { MunicipalityID: $(this).val() };
                if (arg && arg.hasOwnProperty('action') && arg.action === 'page_load') {
                    payoad = arg.payload;
                }
                $.post('/Administration/ScheduleThree/GetForm', payoad, 'text/html')
                .done(function (e) {
                    doms()._container()._parent()._obj.empty().append(e);
                    doms()._container()._obj.find('.btn-draft').show();
                    doms()._container()._parent()._obj.show();
                    if (!(arg && arg.hasOwnProperty('action') && arg.action === 'page_load')) {
                        _evtHndlers().afterPopulateForm();
                    }
                    (new landCalculator()).live();
                    (new authenticateBeforeSave()).live();

                    //show hide draft
                    if (helper.ifNaN(doms()._container()._ctrl_assmtId()._obj.val(), 0) === 0) {
                        $(".btn-draft").show();
                    } else {
                        $(".btn-draft").hide();
                    }

                    //show hide print form
                    var header = $(".cntr-schedule-three .box-header .box-tools");
                    header.find(".print-form").remove();
                    if (helper.ifNaN(doms()._container()._ctrl_assmtId()._obj.val(), 0) !== 0) {
                        header.prepend("<a href='/Administration/ScheduleThree/ShowScheduleIIIFormReadOnly?TabId=" + doms()._container()._ctrl_assmtId()._obj.val() + "' target='_blank' class='btn btn-success print-form'>print form</a>");
                    }
                })
                .fail(function (e) {
                    alertify.alert("<b class='red'>Error</b>", e.responseText);
                });
                
            } else {
                doms()._container()._parent()._obj.hide();
                doms()._container()._obj.find('.btn-draft').hide();
            }
            
        });
        $(document).on('keyup', doms()._container()._tabs()._tab_location()._ctrl_ward()._selector, function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { Ward: request.term, MunicipalityID: $(".ddl-municipality").val() };
                    doms()._container()._tabs()._tab_location()._ctrl_hdnWard()._obj.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Administration/ScheduleThree/GetWards",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WardID,
                                            label: item.WardName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    doms()._container()._tabs()._tab_location()._ctrl_hdnWard()._obj.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });//_ctrl_hdnRoadlane
        $(document).on('keyup', doms()._container()._tabs()._tab_location()._ctrl_roadlane()._selector, function () {
            var scope = doms()._container()._tabs()._tab_location();
            $(this).autocomplete({
                source: function (request, response) {
                    var data = {locationName:request.term,wardID: scope._ctrl_hdnWard()._obj.val(), MunicipalityID: $(".ddl-municipality").val() };
                    doms()._container()._tabs()._tab_location()._ctrl_hdnRoadlane()._obj.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Shared/Location/GetLocations",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.LocationID,
                                            label: item.LocationName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    doms()._container()._tabs()._tab_location()._ctrl_hdnRoadlane()._obj.val(i.item.value);
                    doms()._container()._tabs()._tab_location()._ctrl_roadlane()._obj.val(i.item.label);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('click', ".img-upload:disabled~.input-group-addon", function () {
            var delBtn = $(this);
            alertify.confirm("Confirm us!", "Are you sure to delete this image", function () {
                var imgUploadCtrl = delBtn.siblings('.img-upload');
                imgUploadCtrl.prop('disabled', false);
                delBtn.hide();
                alertify.warning("Now image is re-uploadable")
                globalVariables.deletableFileRequest.push({
                    DatId: null,
                    DataType: imgUploadCtrl.attr("data-datatype"),
                    InfoType: imgUploadCtrl.attr("data-infotype"),
                    InfoTypeId: imgUploadCtrl.attr("data-infotypeid"),
                    TypeCode: imgUploadCtrl.attr('data-typecode'),
                    TypeDesc: imgUploadCtrl.attr('data-typedesc'),
                    Value: imgUploadCtrl.parent().siblings(".img-view").attr("src")
                });
                
                var noImagePath = imgUploadCtrl.parent().siblings(".img-view").attr("data-no-image-src");
                imgUploadCtrl.parent().siblings(".img-view").attr("src", noImagePath);
            }, function () { });
        });
        $(document).on('click',".btn-draft", function () {
            alertify.confirm('Confirm us!', 'Draft will be expired with in 72 hrs from first save.', function () {
                var formData = getFormData();
                if (formData.ImageTab.hasOwnProperty('Images')) {
                    formData.ImageTab.Images = [];
                }
                $.ajax({
                    type: "POST",
                    url: '/Administration/ScheduleThree/SaveAsDraft',
                    data: JSON.stringify(formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (response) {
                        alertify.success('Data saved as draft')
                    },
                    error: function (error) {
                        alertify.error(error.responseText);
                    }
                });
            }, function () { })
            
        });
        
        $(document).on('click', ".land-calculator", function () {
            $("#land-calculator").modal('show');
            var container = $(this).closest(".input-group").attr("data-use-cal-id");
            $("#land-calculator").attr("cal-container", container);
            return false;
        });

        //clone helper for select element
        $(document).on("change", "select", function () {
            var val = $(this).val(); //get new value
            //find selected option
            $("option", this).removeAttr("selected").filter(function () {
                return $(this).attr("value") == val;
            }).first().attr("selected", "selected"); //add selected attribute to selected option
        });

        $(document).on('change', doms()._container()._tabs()._tab_location()._ctrl_landType()._selector, function () {
            var landAreaDC = doms()._container()._tabs()._tab_location()._ctrl_landarea_dec()._obj;
            var landAreaSF = doms()._container()._tabs()._tab_location()._ctrl_landarea_sft()._obj;
            if ($(this).val() == 'F') {
                landAreaDC.prop('disabled', true);
                landAreaSF.prop('disabled', true);
            } else {
                landAreaDC.prop('disabled', false);
                landAreaSF.prop('disabled', false);
            }
        });
        //Plot/Dag no, in lr and rs one filed is given another is disabled
        $(document).on('keyup', doms()._container()._tabs()._tab_location()._ctrl_dag_lr()._selector.concat(',').concat(doms()._container()._tabs()._tab_location()._ctrl_dag_rs()._selector), function () {
            var lr = doms()._container()._tabs()._tab_location()._ctrl_dag_lr()._obj;
            var rs = doms()._container()._tabs()._tab_location()._ctrl_dag_rs()._obj;
            if (helper.ifNullEmptyOrUndefinded(lr.val(),0)!=0 || lr.val()!=0) {
                lr.prop('disabled', false);
                rs.prop('disabled', true);
            } else if (helper.ifNullEmptyOrUndefinded(rs.val(),0)!=0 || rs.val()!=0) {
                lr.prop('disabled', true);
                rs.prop('disabled', false);
            } else {
                lr.prop('disabled', false);
                rs.prop('disabled', false);
            }
        });
        //Khatian no, in lr and rs one filed is given another is disabled
        $(document).on('keyup', doms()._container()._tabs()._tab_location()._ctrl_khatian_rs()._selector.concat(',').concat(doms()._container()._tabs()._tab_location()._ctrl_khatian_lr()._selector), function () {
            var lr = doms()._container()._tabs()._tab_location()._ctrl_khatian_lr()._obj;
            var rs = doms()._container()._tabs()._tab_location()._ctrl_khatian_rs()._obj;
            if (helper.ifNullEmptyOrUndefinded(lr.val(), 0) != 0 || lr.val() != 0) {
                lr.prop('disabled', false);
                rs.prop('disabled', true);
            } else if (helper.ifNullEmptyOrUndefinded(rs.val(), 0) != 0 || rs.val() != 0) {
                lr.prop('disabled', true);
                rs.prop('disabled', false);
            } else {
                lr.prop('disabled', false);
                rs.prop('disabled', false);
            }
        });
    };
    var initiateMasterData = function () {
        var msterData = $(".cntr-schedule-three .hidden-params .master-data");
        if (msterData.val() && msterData.val().length > 0) {
            globalVariables.masterData = JSON.parse(msterData.val());
            msterData.remove();
        }
    };
    var preRunIfAssmtIdAvailable = function () {
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var assmtId = getUrlVars().id;//query string id
        var mnId = getUrlVars().mn;//query string municipality id
        if ((assmtId && isNaN(assmtId) === false) && (mnId && isNaN(mnId)==false)) {
            $(".ddl-municipality").prop('disabled', true);
            $(".ddl-municipality").find('option[value="' + mnId + '"]').prop('selected', true);
            $(".ddl-municipality").trigger('change', { action: 'page_load', payload: { MunicipalityID: mnId, assmtId: assmtId } });
            //remove draft saving role from next button
            $('.btn-next').removeClass('btn-draft').each(function (i, v) { $(v).text('next') });
        } else if ($(".Is-citizen").length > 0 && $(".Is-citizen").val() === 'yes' && $(".citizen-data").length>0) {
            var citizenData = JSON.parse($(".citizen-data").val());
            if (citizenData) {
                $(".ddl-municipality").prop('disabled', true);
                $(".ddl-municipality").find('option[value="' + citizenData.MunicipalityID + '"]').prop('selected', true);
                $(".ddl-municipality").trigger('change');
            }
        }
       
    };
    var landCalculator = function () {
        var socpe1 = function () { return $("#land-calculator") };
        var resSft = function () { return socpe1().find(".calc-sft-res") };
        var resDec = function () { return socpe1().find(".calc-dec-res") };
        var calculate = function (katha, chatak) {
            katha = helper.ifNaN(katha, 0);
            chatak = helper.ifNaN(chatak, 0);

            var sft = 0;
            var dec = 0;
            var tmpsft = 0;
            tmpsft = (720 * katha + 45 * chatak);
            dec = parseInt(tmpsft / 435.6);
            //dec = (720 * katha + 45 * chatak) / 435.6;
            sft = Math.round((tmpsft - (dec * 435.6)));
            return { dec: dec, sft: sft };
        };
        var bindEvent = function () {
            var x = socpe1();
            socpe1().off('keyup', ".calc-katha,.calc-chatak");
            socpe1().on('keyup', ".calc-katha,.calc-chatak", function () {
                var data = calculate(socpe1().find(".calc-katha").val(), socpe1().find(".calc-chatak").val());
                resDec().val(data.dec);
                resSft().val(data.sft);
                
                var usedContainer = socpe1().attr("cal-container");
                $("[data-use-cal-id='" + usedContainer+"']").find(".cal-dec").val(data.dec);
                $("[data-use-cal-id='" + usedContainer+"']").find(".cal-sft").val(data.sft);
            });
            socpe1().on('shown.bs.modal', function (e,q) {
                // do something...
                socpe1().find(".calc-katha").focus();
                socpe1().find(".calc-katha,.calc-chatak").val('');
                resDec().val('');
                resSft().val('');
            })
        };
        this.live = function () {
            bindEvent();
        };
    };
    var authenticateBeforeSave = function () {
        var scope_sctr = "#modal-authenticate-n-save";
        var scope = $(scope_sctr);
        
        this.live = function () {
            bindEvent();
        };
        var bindEvent = function () {
            $(document).off('click', scope_sctr.concat(" .btn-authenticate"));
            $(document).on('click', scope_sctr.concat(" .btn-authenticate"), function () {
                scope.modal('show');
            });

            $(document).off('change', scope_sctr.concat(" .ddl-authentication-type"));
            $(document).on('change', scope_sctr.concat(" .ddl-authentication-type"), function () {
                scope.find("ul.nav-tabs>li>a[href='#"+$(this).val()+"']").trigger('click');
            });

            $(document).off('change', scope_sctr.concat(" .img-upload-signature"));
            $(document).on('change', scope_sctr.concat(" .img-upload-signature"), function () {
                var input = this;
                if (input.files && input.files[0]) {
                    var filerdr = new FileReader();
                    filerdr.onload = function (e) {
                        scope.find('.img-sinature').attr('src', e.target.result);
                    }
                    filerdr.readAsDataURL(input.files[0]);
                }
            });

            $(document).off('click', scope_sctr.concat(" .btn-success"));
            $(document).on('click', scope_sctr.concat(" .btn-success"), function (e) {
                e.preventDefault();
                try{
                var formData = getFormData();
                formData.DelImgRequests = globalVariables.deletableFileRequest;
                formData = processWithFormData(formData);
                var formValidatr = formValidator();
                if (!formValidatr.isValid()) {
                    alert("Validation Error! Some input fields have invalid value, Please correct them");
                    var errors = formValidatr.getErrors();
                    errors.forEach(function (a, b) {
                        if (b === 0) {
                            var elemnt = $(a.element);
                            var closestTab = elemnt.closest('.tab-pane')
                            if (closestTab.length > 0) {
                                closestTab.parent().siblings('ul').find('li>a[href="#' + closestTab.attr('id') + '"]').trigger('click');
                            }
                            $('html, body').animate({
                                scrollTop: elemnt.offset().top
                            }, 100);
                            elemnt.focus();
                        }
                    });
                    return;
                }
                alertify.confirm('Please Confirm us!', "Are you sure to save data?", function () {
                    $.ajax({
                        type: "POST",
                        url: '/Administration/ScheduleThree/SaveForm',
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (r) {
                            if (r.Status === 200) {
                                alertify.alert("Success", r.Message, function () {
                                    
                                    location.reload();
                                });
                            } else {
                                alertify.alert("<b class='red'>Error</b>", r.Message, function () {

                                });
                            }
                        },
                        error: function (error) {
                            alertify.error(error.responseText);
                        }
                    });
                }, function () { })
                } catch (e) {
                    alertify.alert("Error", e, function () { });
                }
            });

            $(document).off('click', scope_sctr.concat(" .btn-otp"));
            $(document).on('click', scope_sctr.concat(" .btn-otp"), function () {
                var btnRqstOtp = $(this);
                var mobileNo = doms()._container()._ctrl_phone()._obj.val();
                var lblOtpMsg = $(scope_sctr.concat(" .lbl-otp-msg"));
                lblOtpMsg.text('');
                $.post("/Shared/OTPServer/SenOtp", { MobileNo: mobileNo }, function (e) {
                    if (e.Status === 200) {
                        lblOtpMsg.removeClass('red').addClass('green');
                        var curtime = new Date();// get cur date time
                        var nextSend=new Date();
                        nextSend.setMinutes(nextSend.getMinutes()+1);

                        var timer = function () {
                            curtime.setSeconds(curtime.getSeconds() + 1);
                            if (curtime > nextSend) {
                                btnRqstOtp.prop('disabled', false).val('Request OTP');
                                clearInterval(setInterVal);
                            } else {
                                btnRqstOtp.prop('disabled', true).val('Next Otp Request after ' + Math.abs((nextSend - curtime)/1000));
                            }
                        };
                        var setInterVal = setInterval(timer, 1000);
                        
                    } else {
                        lblOtpMsg.removeClass('green').addClass('red');
                    }
                    lblOtpMsg.removeClass('green').addClass('red').text(e.Message);
                }, 'json').fail(function (e) { });
            });

        
        };
    };
    (function () {
        initiateMasterData();
        registerEvents();
        preRunIfAssmtIdAvailable();
        
    }());
});

