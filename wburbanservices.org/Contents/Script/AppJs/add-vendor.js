﻿$(document).ready(function () {
    app.getAllVendors();
    $(document).on("click", ".btn-sbmt", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData()
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirm Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData()
                };
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/AddVendorData",
                    data: JSON.stringify(payload.configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
});
var app = {
    getAllVendors: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllVendorsList',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');
                        //floor control
                        var td = $('<td class="vendor-name" />');
                        tr.append(td.append(tempJson.VendorName));

                        td = $('<td class="vendor-add" />');
                        tr.append(td.append(tempJson.VendorAdd));

                        //add usage control
                        td = $('<td class="vendor-regno" />');
                        tr.append(td.append(tempJson.VendorReg));

                        //add covered area control
                        td = $('<td class="vendor-PAN" />');
                        tr.append(td.append(tempJson.VendorPAN));

                        //add Occupent Name control
                        td = $('<td class="vendor-SGT" />');
                        tr.append(td.append(tempJson.VendorGST));

                        td = $('<td class="PcontactNo" />');
                        tr.append(td.append(tempJson.PrimaryCntctNo));

                        td = $('<td class="fundType"/>');
                        tr.append(td.append(tempJson.PrimaryCntctNm));

                        td = $('<td class="fundType"/>');
                        tr.append(td.append(tempJson.PrimaryEmail));

                        td = $('<td class="fundType"/>');
                        tr.append(td.append(tempJson.OtherCntctNo));

                        //td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                        //tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson + '\'/>Update</a>'));


                        //td = $('<td class="completeJson"><input type="hidden" class="hdnJSN" value="' + tempJson +'"/></td>');
                        //tr.append(td);
                        $('#myTable').find('>tbody').append(tr);
                    }
                }
            },
            error: function () { }
        });
    },
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#addVendor"),
                txtVendorNm: $(".vendorNm"),
                txtVendorRegNo: $(".vendorRegNo"),
                txtVendorAdd: $(".vendorAdd"),
                txtVendorPAN: $(".vendorPAN"),
                txtGSTNo: $(".gstNo"),
                txtPcontactNo: $(".pcontactNo"),
                txtCntctPrsnName: $(".cntctPrsnName"),
                txtPrimaryEmail: $(".primaryEmail"),
                txtOthrCntctNo: $(".othrCntctNo"),
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().txtVendorNm.prop('name')] = { required: true };
                vldObj.rules[_doms().txtVendorRegNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtVendorAdd.prop('name')] = { required: true };
                vldObj.rules[_doms().txtVendorPAN.prop('name')] = { required: true };
                vldObj.rules[_doms().txtPcontactNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtCntctPrsnName.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[_doms().txtVendorNm.prop('name')] = { required: "Enter vendor name" };
                vldObj.messages[_doms().txtVendorRegNo.prop('name')] = { required: "Enter vendor registration no" };
                vldObj.messages[_doms().txtVendorAdd.prop('name')] = { required: "Enter vendor address" };
                vldObj.messages[_doms().txtVendorPAN.prop('name')] = { required: "Enter vendor PAN no" };
                vldObj.messages[_doms().txtPcontactNo.prop('name')] = { required: "Enter primary contact no" };
                vldObj.messages[_doms().txtCntctPrsnName.prop('name')] = { required: "Enter contact person name" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                return {
                    VendorName: _doms().txtVendorNm.val(),
                    VendorReg: _doms().txtVendorRegNo.val(),
                    VendorAdd: _doms().txtVendorAdd.val(),
                    VendorPAN: _doms().txtVendorPAN.val(),
                    VendorGST: _doms().txtGSTNo.val(),
                    PrimaryCntctNo: _doms().txtPcontactNo.val(),
                    PrimaryCntctNm: _doms().txtCntctPrsnName.val(),
                    PrimaryEmail: _doms().txtPrimaryEmail.val(),
                    OtherCntctNo: _doms().txtOthrCntctNo.val(),
                };
            }
        }
    },
}