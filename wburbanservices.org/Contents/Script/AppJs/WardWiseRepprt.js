﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetCountersByMunicipalityID = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnCounter/GetActiveCountersForReports",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var BindEvent = function () {
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            $('#btnSave').on('click', Insert);
            PopulateCounters();
            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateTo,#DateFrom').blur(function () {
                if ($(this).val().trim() == '') {
                    return false;
                }
            });
            $('#divCounter').on('change', '#chkAll', function () {
                $("#tabCounter tbody tr").find(".select-counter").prop('checked', $("#chkAll").prop("checked"));
            });
        };
        var Bind_GridOnPageLoad = function () {
            var RegisterEvent = function () {
            };
            (function () {
                RegisterEvent();
            }());
        };
        var Validate = function (data) {

            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            if ($("#hdnCounter").val().length == 0) {
                alert("Select atleast one Counter");
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                $('#frmWardWiseReport').submit();
            }
        };
        var getDataFromView = function () {
            GetCounters();
            var DateFrom = $('#DateFrom').val().trim();
            var DateTo = $('#DateTo').val().trim();
            var CounterID = $('#hdnCounter').val();
            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }

            var data = { DateFrom: DateFrom, DateTo: DateTo };
            return data;
        };
        var GetCounters = function () {
            var counterID = "";
            $.each($(".select-counter").find("table tbody").find(".select-counter:checked"), function (idx, counter) {
                counterID = counterID + $(counter).val() + ",";
            });
            counterID = counterID.substring(1 - counterID.length, counterID.length - 1);
            $("#hdnCounter").val(counterID);
        };
        var PopulateCounters = function () {
            serverObject.GetCountersByMunicipalityID(function (response) {
                $(".select-counter").find("table tbody").empty();
                if (response.Status == 200 && (response.Data).length > 0) {
                    $.each(response.Data, function (idx, counter) {
                        var str = "<tr>";
                        str += "<td><input type='checkbox' class='select-counter' value='" + counter.CounterID + "'/></td><td><label>&nbsp;&nbsp;" + counter.CounterCode + "</label></td>";
                        str += "</tr>";
                        $(".select-counter").find("table tbody").append(str);
                    });
                }
            });
        };
        this.app_initiate = function () {
            BindEvent();
            Bind_GridOnPageLoad();
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        //Resetors.ResetLocation();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            if ((serverResponse).length > 0) {
                                var userDataAutoComplete = [];
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                                response(userDataAutoComplete);
                            }
                        });
                    },
                    select: function (e, i) {
                        $("#WardID").val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            //Resetors.ResetLocation();
                        }
                    }
                };
            }
        };
        var DomControls = {
            ctrl_hdnWard: $("#WardID"),
            ctrl_txtWard: $("#txtWard")
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            }
        };
    };

    var INIT = function () {
        var serverObj = new SERVER_OBJECT();
        var appObj = new APP_OBJECT(serverObj);
        appObj.app_initiate();
    };
    (function () { INIT(); })();
});