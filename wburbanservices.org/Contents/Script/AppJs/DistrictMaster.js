﻿var app = angular.module('myApp', []);
app.controller('myController', function ($scope, $window, $http) {

    $scope.Districts = [];
    $scope.btnSave2 = "Save";
    angular.element("#txtDistrictName").focus();

    $scope.onload = function () {
        GetAllDistrict();
    }

    $scope.Reset = function () {
        $scope.hdnDistrictID = "";
        $scope.txtDistrictName = "";
        $scope.btnSave2 = "Save";
        angular.element("#txtDistrictName").focus();
    }

    $scope.Edit = function (ID) {
        angular.forEach($scope.Districts, function (value, key) {
            if (value.DistrictID == ID) {
                $scope.hdnDistrictID = value.DistrictID;
                $scope.txtDistrictName = value.DistrictName;
                $scope.btnSave2 = "Update";
            }
        })
    }

    $scope.Save_Update = function () {
        if ($scope.txtDistrictName == "") {
            alert("Please enter a District Name");
            return false;
        }
        var ID = $scope.hdnDistrictID;
        var name = $scope.txtDistrictName;
        if (ID == "")
        { Save(name); }
        else { Update(ID, name); }
    }

    function Save(txt) {
        $http({
            method: "post",
            url: "/DistrictMaster/Save_District",
            data: { DistrictName: txt }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200) {
                GetAllDistrict();
            }
        }, function (error) { alert("Error Occur"); })
    }

    function Update(ID, txt) {
        $http({
            method: "post",
            url: "/DistrictMaster/Update_District",
            data: { DistrictID: ID, DistrictName: txt }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200) {
                GetAllDistrict();
            }
        }, function (error) { alert("Error Occur"); })
    }

    function GetAllDistrict() {
        $scope.hdnDistrictID = "";
        $scope.txtDistrictName = "";
        $scope.btnSave2 = "Save";
        angular.element("#txtDistrictName").focus();
        $http({
            method: "post",
            url: "/DistrictMaster/Get_AllDistrict"
        }).then(function (response) {
            $scope.Districts = response.data.Data;
        }, function () {
            alert("Error Occur");
        })
    }

    $scope.Delete = function (ID) {
        if ($window.confirm("Are you sure want to delete this")) {
            $http({
                method: "post",
                url: "/DistrictMaster/Delete_District",
                data: { DistrictID: ID }
            }).then(function (response) {
                alert(response.data.Message);
            }, function (error) {
                alert("Error Occur");
            })
        }
    }
})