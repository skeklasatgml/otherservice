﻿$(document).ready(function () {
    window.StageFileByFormData = function (input,successCallBack,errorCallBack) {
        var fileData = new FormData();
        if (input.files.length <= 0) {
            return;
        }
        for (var i = 0; i < input.files.length; i++) {
            fileData.append(input.files[i].name, input.files[i]);
        }
        $.ajax({
            url: '/Modules/dpr/StageFileByFormData',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (r) {
                if (r.Status === 200 && successCallBack && typeof (successCallBack) === 'function') {
                    successCallBack(r);
                } else {
                    errorCallBack(r);
                }
            },
            error: function (r) {
                if (errorCallBack && typeof (errorCallBack) === 'function') {
                    var obj = { Data: r.responseText, Message: r.statusText, Status: r.statusCode };
                    errorCallBack(obj);
                }
            }
        });  
    };
    
});