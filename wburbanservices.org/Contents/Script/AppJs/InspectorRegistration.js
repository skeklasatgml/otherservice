﻿var mobileGlobal = '';


$(document).ready(function () {
    createCaptcha();

    $(document).on('click', '.btn-Register', function (e) {
        validateCaptcha();
        mobileGlobal = '';
        e.preventDefault();
        registerValidator();
        if (!validateFrm()) { return; }
        var inspectorname = $('.txtinspecName').val();
        var inspectormobile = $('.txtmobile').val();
        var inspectoremail = $('.txtinspectemail').val();
        var inspectorusername = $('.txtuserName').val();
        var inspectorpassword = $('.txtpass').val();

        var InspectorReg = {
            InspectorName: inspectorname, InspectorMobile: inspectormobile, InspectorEmail: inspectoremail, InspectorUserName: inspectorusername,
            InspectorPassword: inspectorpassword
        }

        if (validateCaptcha()) {
            $.ajax({
                type: "POST",
                url: "/OtherServices/OtherServices/SaveInspectorReg",
                data: JSON.stringify(InspectorReg),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.Status === 200) {
                        if ((r.Data.Status === 1 && r.Data.isExist === 0 && r.Data.isVerified === 0)) {
                            // $('#ModelInspectorServices').show();
                            $('#ModelInspectorServices').modal('show');
                            $('#optmsg').html(r.Data.Msg);
                            mobileGlobal = r.Data.Mobile;
                            $('#lblMobile').text(r.Data.Mobile);
                            $('#inspector-OTP-form').show();
                            $('#inspector-MobileVerfication-form').hide();
                        }
                        else {
                            //alert(r.Data.Msg);
                            swal("", r.Data.Msg, "error");
                        }
                    } else {
                        alert("Error!", r.Message, function () { });
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });
        }
    })

    //-sumit-//
    $(document).on('click', '.btn-SendOTP', function (e) {
        var otptext = $(".form_otp");
        var otpdata = { Otpdata: otptext.val().trim(), MobileNo: mobileGlobal };
        if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal && otptext.val().trim() != '') {
            $.ajax({
                type: "POST",
                url: "/OtherServices/OtherServices/otpVerify_inspector",
                data: JSON.stringify(otpdata),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.Status === 200) {
                        if ((r.Data.Status === 1)) {
                            //alert(r.Data.Msg);
                            swal("", r.Data.Msg, "success");
                            $('#ModelInspectorServices').modal('hide');
                            swal("", "Sucessfully Registered", "success");
                            $('#Inspector-Registration-form').find("input[type=text],input[type=password],input[type=tel],input[type=email],textarea").val("");
                            $('#inspector-OTP-form').find("input[type=text],input[type=password],input[type=tel],input[type=email],textarea").val("");
                        } else {
                            // alert(r.Data.Msg);
                            swal("", r.Data.Msg, "error");
                        }

                    } else if (r.Status == 401) {

                        alert('Unsucessful');
                    } else {
                        //alertify.error(r.Message);
                        swal("", r.Message, "error");
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });
        }
        else {
            alert('Entered mobile no is not correct');
        };
    })

    $(document).on('click', '.Btn-resendotp', function (e) {
        // var otptext = $(".form_otp");
        var Resendotpdata = { Type: "R", MobileNo: mobileGlobal };
        if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal) {
            $.ajax({
                type: "POST",
                url: "/OtherServices/OtherServices/ResendOtp_inspector",
                data: JSON.stringify(Resendotpdata),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.Status === 200) {
                        if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
                            alert(r.Data.Msg);
                        } else {
                            alert(r.Data.Msg);
                        }
                    } else if (r.Status == 401) {
                        alert('Unsucessful');
                    } else {
                        alertify.error(r.Message);
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });

        }
        else {
            alert('Entered mobile no is not correct');
        };
    })

    $(document).on('click', '.btn-verifymobile', function (e) {
        // var otptext = $(".form_otp");
        var otpdata = { Otpdata: $('.form_otp_ver').val().trim(), MobileNo: $('.form_mobile_verify').val().trim() };
        if ($('.form_otp_ver').val().trim() != '' && $('.form_mobile_verify').val().trim() != '') {
            $.ajax({
                type: "POST",
                url: "/OtherServices/OtherServices/otpVerify_inspector",
                data: JSON.stringify(otpdata),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.Status === 200) {
                        if ((r.Data.Status === 1)) {
                            alert(r.Data.Msg);
                            $('#ModelInspectorServices').modal('hide');
                        } else {
                            alert(r.Data.Msg);
                        }

                    } else if (r.Status == 401) {

                        alert('Unsucessful');
                    } else {
                        alertify.error(r.Message);
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });
        }
        else {
            alert('Please Enter the Correct Mobile No');
        };
    })

    $(document).on('click', '.Btn-mobileresendverify', function (e) {
        // var otptext = $(".form_otp");
        var Resendotpdata = { Type: "R", MobileNo: $('.form_mobile_verify').val().trim() };
        if ($('.form_mobile_verify').val().trim() != '') {
            $.ajax({
                type: "POST",
                url: "/OtherServices/OtherServices/ResendOtp_inspector",
                data: JSON.stringify(Resendotpdata),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.Status === 200) {
                        if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
                            alert('Your Otp has been Sent to your Mobile No');
                        } else {
                            alert(r.Data.Msg);
                        }
                    } else if (r.Status == 401) {
                        alert('Unsucessful');
                    } else {
                        alertify.error(r.Message);
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });

        }
        else {
            alert('Please Enter the Correct Mobile No');
        };
    })

})

var validateFrm = function () {
    var vldObj = {
        rules: {},
        messages: {},
        errorPlacement: {}
    };
    vldObj.ignore = [];
    vldObj.rules[$('.txtinspecName').prop('name')] = { required: true };
    vldObj.rules[$('.txtmobile').prop('name')] = { required: true, regex: "^[0-9]{10}$" };
    vldObj.rules[$('.txtinspectemail').prop('name')] = { required: true, regex: /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/ };
    //vldObj.rules[$('.profpic').prop('name')] = { required: true };
    vldObj.rules[$('.txtuserName').prop('name')] = { required: true };
    vldObj.rules[$('.txtpass').prop('name')] = { required: true };
    vldObj.rules[$('.txtconfpass').prop('name')] = { required: true, equalTo: '#txtpass' };



    vldObj.messages[$('.txtinspecName').prop('name')] = { required: "Please Enter The Inspector Name" };
    vldObj.messages[$(".txtmobile").prop('name')] = { regex: "Invalid Phone number", required: "Please Enter Phone number" };
    vldObj.messages[$(".txtinspectemail").prop('name')] = { regex: "Invalid Email", required: "Please Enter Email" };
    vldObj.messages[$('.txtuserName').prop('name')] = { required: "Please Enter User Name" };
    vldObj.messages[$('.txtpass').prop('name')] = { required: "Please Enter Password" };
    vldObj.messages[$('.txtconfpass').prop('name')] = { required: "Please Enter Confirm Password", equalTo: "Password doesn't Match!!" };


    vldObj.errorPlacement = function (error, element) {
        if ($(element).parent(".input-group").length > 0) {
            error.insertAfter($(element).parent(".input-group"));
            error.css('color', 'red');
        } else {
            error.insertAfter(element);
            error.css('color', 'red');
        }
    };
    $('#Inspector-Registration-form').validate(vldObj);
    return $('#Inspector-Registration-form').valid();
};
var registerValidator = function () {
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
};

$(document).on('click', '.btn-mverify', function (e) {
    e.preventDefault();
    $('.modal-title').html('Mobile Verfication');
    $('#ModelInspectorServices').modal('show');
    $('#inspector-OTP-form').hide();
    $('#inspector-MobileVerfication-form').show();
});

var code;
function createCaptcha() {
    //clear the contents of captcha div first
    document.getElementById('captcha').innerHTML = "";
    var charsArray = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$%^&*";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
        //below code will not allow Repetition of Characters
        var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
        if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
        else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captcha";
    canv.width = 120;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    code = captcha.join("");
    document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
}
function validateCaptcha() {
    event.preventDefault();
    debugger
    if (document.getElementById("cpatchaTextBox").value == code) {
        // alert("Valid Captcha")
        $(".error").html("");
        return true;
    } else {
        $(".error").html("Invalid Captcha. try Again");
        createCaptcha();
        return false;
    }
}