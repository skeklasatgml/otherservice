﻿
$(document).ready(function () {

    getgriddata();

    var modalControl = $('#modal_MakePaymentComplete');
    var ServiceID = '';
    var MuncipalityID = '';
    
    //$(document).on('click', '.complete-payment', function (e) {
    //    e.preventDefault();
    //    clickOnPayment(ServiceID, MuncipalityID, $('.ddl-payment-gateway').val());
    //});    


    function getgriddata() {
        var url = '';
        var RedirectURL = '';
        var submoduleid = $('#submoduleid').val();
        var userid = $('#userid').val();

        var E = "{SubModuleid:'" + submoduleid + "',UserId:'" + userid + "'}";
        $.ajax({
            type: "POST",
            url: "/OtherServices/OtherServices/GetGridDet",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $(".aplilist tr.trclass").empty();

                    if (submoduleid == 1) {
                        RedirectURL = "ApplicationForm";
                    }
                    if (submoduleid == 2) {
                        RedirectURL = "Stacking";
                    }
                    if (submoduleid == 3) {
                        RedirectURL = "Movie";
                    }
                    if (submoduleid == 4) {
                        RedirectURL = "Signage";
                    }
                    if (submoduleid == 5) {
                        RedirectURL = "MobileTower";
                    }

                    $.each(D.Data, function (index, value) {
                        var str = "";
                        $.each(value.StatusActions, function (ii, vv) {
                            if (vv.urlmode == "T") {
                                url = "/OtherServices/" + RedirectURL + "/Index?Data=" + vv.Data;
                              
                               // url = "/OtherServices/" + RedirectURL + "/Index?ApplicationId=" + vv.ServMstId + "&mode=" + vv.urlmode;
                                str = str + "<option  value ='" + vv.urlmode + "' > " + vv.Actions + "</option >";
                                //str = str + "<option value ='/OtherServices/" + RedirectURL + "/Index?ApplicationId=" + value.ServMstId + "&mode=" + vv.urlmode + "' > " + vv.Actions + "</option >";
                            }
                            else if (vv.urlmode == "P" ) {
                                str = str + "<option value ='" + vv.urlmode + "' > " + vv.Actions + "</option >";
                            }
                            else if (vv.urlmode == "A" || vv.urlmode == "B") {
                                str = str + "<option value ='" + vv.urlmode + "' > " + vv.Actions + "</option >";
                            }
                            else if(vv.urlmode == "W") {
                                url = "/Municipality/MnReports/RoadCuttingAnnexure1?ApplicationID=" + value.ServMstId;
                                str = str + "<option  value ='" + vv.urlmode + "' > " + vv.Actions + "</option >";
                            }
                        });
                        if (str != "" && str.length > 1) {
                            str = str + "</select>";
                        }

                        var abc = "";
                        if (url.length > 0)
                        {
                            abc = "<span data-MID=" + value.MunicipalityDevAuthoIdEncrypt + " data-SID=" + value.ServMstIdEncrypted + " data-url=" + url + " id='spanView'></span></td></tr> ";
                        }
                        else {
                            abc = "<span data-MID=" + value.MunicipalityDevAuthoIdEncrypt + " data-SID=" + value.ServMstIdEncrypted + " id='spanView'></span></td></tr> ";
                        }

                        $(".aplilist tbody").append("<tr class='trclass'>" +
                            "<td  style='text-align:center'>" +   // style='display:none;'
                            value.slno + "</td>" +
                            "<td  style='text-align:center'>" +
                            value.MunicipalityName + "</td>" +
                            "<td style='display:none;'>" +
                            value.ServMstId + "</td><td style='text-align:center'>" +
                            value.ApplicationNo + "</td><td style='text-align:center'>" +
                            value.ApplicationDate + "</td ><td style='text-align:center;'>" + value.ProjectDescription + "</td><td style='text-align:center;display:none;'>" +
                            value.ServiceCurrStatus + "</td><td style='text-align:center;'>" +
                            value.DESCRIPTION + "</td><td style='text-align:center;'>" +
                            "<select id='cmbAction' style='width:100%'><option>Option</option>" +
                            str +
                            abc);
                    });
                    GetActionTabEvent();
                    $('.aplilist').DataTable({
                        "bSort": false,
                        "bPaginate": true,
                        "bInfo": true,
                        "bFilter": true,
                        "bLengthChange": false,
                        "iDisplayLength": 8,
                        "searchPlaceholder": "search",
                    });
                }
            }
        });
    }
    var Redirect = function (ID)
    {
        var url = $(ID).val();
        window.location.href = url
    }
    var GetActionTabEvent = function () {
        $(".aplilist").find("tbody").off('change', 'select[id="cmbAction"]', OnActionChange);
        $(".aplilist").find("tbody").on('change', 'select[id="cmbAction"]', OnActionChange);
    };

    var OnActionChange = function () {
        ServiceID = '';
        MuncipalityID = '';
        
        var CurrentTR = $(this).closest('tr');
        var ActionType = $(CurrentTR).find("td:eq(8)").find('select[id$="cmbAction"]').val(); 
        var ApplicationId = $(CurrentTR).find("td:eq(2)").text();

        if (ActionType == 'P') {

            ServiceID = $(CurrentTR).find("td:eq(8)").find('span[id$="spanView"]').attr('data-SID');
            MuncipalityID = $(CurrentTR).find("td:eq(8)").find('span[id$="spanView"]').attr('data-MID');
            var submoduleid = $('#submoduleid').val();
           
            if (submoduleid == 1)
                window.location.href = "/OtherServices/OtherServices/ServiceReport?ApplicationId=" + ApplicationId + "&MuncipalityID=" + MuncipalityID + "&ServiceID=" + ServiceID.toString();
            if (submoduleid == 2)
                window.location.href = "/OtherServices/OtherServices/ServiceReport_Stacking?ApplicationId=" + ApplicationId + "&MuncipalityID=" + MuncipalityID + "&ServiceID=" + ServiceID.toString();
            if (submoduleid == 3)
                window.location.href = "/OtherServices/OtherServices/ServiceReport_Movie?ApplicationId=" + ApplicationId + "&MuncipalityID=" + MuncipalityID + "&ServiceID=" + ServiceID.toString();

           // window.location.href = "/OtherServices/OtherServices/ServiceReport?ApplicationId=" + ApplicationId + "&MuncipalityID=" + MuncipalityID + "&ServiceID=" + ServiceID.toString();
            return false;

            var PayLoadPG = "{MunicipalityID:'" + MuncipalityID + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/OtherServices/MakePayment/GetPGList",
                data: PayLoadPG,
                dataType: "json",
                success: function (data) {
                    var ResponseData = data.Data;
                    if (ResponseData != null) {
                        $('.ddl-payment-gateway').empty();
                        $('.ddl-payment-gateway').append('<option value="">--select--</option>');
                        $.each(ResponseData, function (index, value) {
                            var label = value.PG_Name;
                            var id = value.PG_Code;
                            $('.ddl-payment-gateway').append("<option value='" + id + "'>" + label + "</option>");
                        });
                        modalControl.modal('show');
                        $('.complete-payment').off('click', clickOnPayment);
                        $('.complete-payment').on('click', clickOnPayment);
                    } else {
                        alert(data.Message);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        else if (ActionType == 'T' || ActionType == 'W') {

            var urls = $(CurrentTR).find("td:eq(8)").find('span[id$="spanView"]').attr('data-url');
            window.location.href = urls;
        }
        else if (ActionType == 'A') {
          
            var ReportNames = "", FileNames='';
            var submoduleid = $('#submoduleid').val(); 
            var final = {}; var master = []; var detail = [];
            if (submoduleid == 1) {
                ReportNames = "RoadCuttingAnnex1.rpt";
                FileNames = "RoadCutting_Annexure-I";
            }
            if (submoduleid == 2) {
                ReportNames = "StorageStackingAnnex1.rpt";
                FileNames = "StorageStacking_Annexure-I";
            }
            if (submoduleid == 3) {
                ReportNames = "MovieShootingAnnex1.rpt";
                FileNames = "MovieShooting_Annexure-I";
            }

            master.push({
                ReportName: ReportNames,
                FileName: FileNames,
                Database: ''
            });

            detail.push({
                ApplicationID: ApplicationId
            });

            final = {
                Master: master,
                Detail: detail
            }


            var left = ($(window).width() / 2) - (950 / 2),
                top = ($(window).height() / 2) - (650 / 2),
                popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
            popup.focus();

        }
        else if (ActionType == 'B') {
           
            var ReportNames = "", FileNames = '';
            var submoduleid = $('#submoduleid').val(); 
            var final = {}; var master = []; var detail = [];
            if (submoduleid == 1) {
                ReportNames = "RoadCuttingAnnex2.rpt";
                FileNames = "RoadCutting_Annexure-II";
            }
            if (submoduleid == 2) {
                ReportNames = "StorageStackingAnnex2.rpt";
                FileNames = "StorageStacking_Annexure-II";
            }
            if (submoduleid == 3) {
                ReportNames = "MovieShootingAnnex2.rpt";
                FileNames = "MovieShooting_Annexure-II";
            }

            master.push({
                ReportName: ReportNames,
                FileName: "Annexure-I",
                Database: ''
            });

            detail.push({
                ApplicationID: ApplicationId
            });

            final = {
                Master: master,
                Detail: detail
            }


            var left = ($(window).width() / 2) - (950 / 2),
                top = ($(window).height() / 2) - (650 / 2),
                popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
            popup.focus();
        }
    };

    var validate = function () {
        modalControl.find(".modal-footer .field-error").hide();//hide all error fields
        var selectedPGVal = modalControl.find('.ddl-payment-gateway').val();
        if (selectedPGVal == "" || selectedPGVal == null || selectedPGVal == undefined) {
            modalControl.find(".modal-footer .field-error").html('Please select payment gateway');
            modalControl.find(".modal-footer .field-error").show();
            return false;
        }
        return true;
    };

    var clickOnPayment = function () {        
        if (validate() && confirm("Are you sure to confirm payment?")) {
            if (ServiceID != '' && MuncipalityID != '') {
                MakePayment(ServiceID, MuncipalityID, $('.ddl-payment-gateway').val());
                //alert(ServiceID+' '+ MuncipalityID+'  ' + $('.ddl-payment-gateway').val());
            }
        }
        else {
            alert('ERROR on id mapping.');
        }
    };

    var MakePayment = function (ServiceID, MuncipalityID, PGCode) {
        var PayLoadMP = "{MunicipalityID:'" + MuncipalityID + "',ServiceID:'" + ServiceID + "',PGCode:'" + PGCode + "'}";
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/OtherServices/MakePayment/MakePaymentTemp",
            data: PayLoadMP,
            dataType: "json",
            success: function (data) {
                var ResponseData = data;
                if (ResponseData != null) {
                    var rData = ResponseData.Data;
                    if (rData.hasOwnProperty('RedirectTo')) {
                        location.href = rData.RedirectTo;
                    }
                } else {
                    alert('Error: ' + response.Message);
                }
            },
            error: function (result) {
                alert("Error");
            }
        });
    };
});


