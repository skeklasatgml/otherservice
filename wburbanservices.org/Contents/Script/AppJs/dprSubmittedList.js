﻿$(document).ready(function () {
    var AAFSFile = null;
    var Server = function () {
        this.GetDprList = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/DprList',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        },
            this.GetApprovalUI = function (data, callback) {
                $.ajax({
                    type: 'post',
                    url: '/Modules/dpr/Approval_GetDprApporvalUI',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    success: function (response) {
                        callback(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                    }
                });
            }
        this.ChangeDprApprovalStatus = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/Approval_ChangeDprAllocationSatus',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
        this.GenerateAAFS = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/Approval_GenerateAAFS',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
        this.SvInternalMessage = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SaveInternalMessage',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
    };
    var App = function (srv) {
       
        var applyUlrFilter = function () {
            var urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('filter')) {
                var param = urlParams.get('filter');
                var decodedUrlParam = decodeURIComponent(param);
                var dataStr = atob(decodedUrlParam);
                var obj = JSON.parse(dataStr);
                var setDateToDatePicker = function (fromDate, toDate) {
                    var strFromDt = fromDate.format('dd/mm/yyyy');
                    var strToDt = toDate.format('dd/mm/yyyy');
                    $(".txt-from-date").val(strFromDt);
                    $(".txt-to-date").val(strToDt);
                };
                
                if (obj.timeSpan == "date-range") {
                    setDateToDatePicker(new Date(obj.fromDate), new Date(obj.toDate));
                } else if (obj.timeSpan == "to-day") {
                    setDateToDatePicker(new Date(), new Date());
                } else if (obj.timeSpan == "last-7-days") {
                    var date = new Date();
                    date.setDate(date.getDate() - 7);
                    setDateToDatePicker(date, new Date());
                } else if (obj.timeSpan == "last-month") {
                    var date = new Date();
                    date.setMonth(date.getMonth() - 1);
                    setDateToDatePicker(date, new Date());
                } else if (obj.timeSpan == "last-6-months") {
                    var date = new Date();
                    date.setMonth(date.getMonth() - 6);
                    setDateToDatePicker(date, new Date());
                }
                else if (obj.timeSpan == "last-1-year") {
                    var date = new Date();
                    date.setFullYear(date.getFullYear() - 6);
                    setDateToDatePicker(date, new Date());
                }


                if (obj.status == "approved") {
                    $(".rdo_status[value='A']").prop('checked', true);
                } else if (obj.status == "submitted") {
                    $(".rdo_status[value='S']").prop('checked', true);
                } else if (obj.status == "returned") {
                    $(".rdo_status[value='RT']").prop('checked', true);
                } else if (obj.status == "released") {
                    $(".rdo_status[value='RL']").prop('checked', true);
                }


                
               var opt= $('.ulbs').find('option[value="0"]');
               if (obj.municipalities && $.isArray(obj.municipalities) && obj.municipalities.length > 0) {
                   opt.prop('selected', false);
                   obj.municipalities.forEach(function (v, i) {
                       var a = $('.ulbs').find('option[value="' + v.id + '"]');
                       if (a && a.length > 0) {
                           a.prop('selected', true);
                       }
                   })
                } else {
                    if (opt && opt.length > 0) {
                        opt.prop('selected', true);
                    }
                }
                
            }
        };
        var constans = {};
        var loadConstants = function () {
            //constans.config = JSON.parse(doms().hdnConfig.val());
            constans.uType = $(".hdn-user-type").val();
            constans.uId = $(".hdn-user-id").val();
            constans.roleId = $(".hdn-user-roleid").val();
            $(".hdn-user-type").remove();
            $(".hdn-user-id").remove();
        };
        var convertToDate = function (strDate) {
            var arr = strDate.split('/');
            return new Date(arr[2], arr[1] - 1, arr[0]);
        }
        var doms = function () {
            return {
                btnShowAll: $(".btn-show-all"),
                btnShowByDate: $(".btn-search-by-date"),
                txtFromDate: $(".txt-from-date"),
                txtToDate: $(".txt-to-date"),
                frm: $("form"),
                table: $(".tbl-dpr-list"),
                rdoStatus: $(".rdo_status:checked"),
                //hdnConfig: $(".hdn-auth-report-actions")
                ulbId: $(".ulbs ")
            }
        };
        var dataTable = function () {
            return {
                configure: function () {
                    doms().table.DataTable({
                        
                        //scrollX: true,
                        //scrollCollapse: false,
                        //"autoWidth": true,
                        columnDefs: [
                            { width: '50px', targets: 0 },
                            { width: '111px', targets: 1 },
                            { width: '141px', targets: 2 },
                            { width: '77px', targets: 3 },
                            //{ width: '400px', targets: 4 },//name of work
                            //{ width: '400px', targets: 5 },//name of project
                            { width: '77px', targets: 6 },//v-date
                            { width: '77px', targets: 7 },
                            { width: '77px', targets: 8 },
                            { width: '136px', targets: 9 },//status
                            { width: '115px', targets: 10 },//released-approved
                            { width: '150px', targets: 11 }
                        ],
                        columns: [
                            { data: "sl" },
                            { data: "DevAuth" },
                            //{ data: "SubmittedOn" },
                            { data: "MemoNumber" },
                            { data: "MemoDate" },
                            {
                                data: null,
                                render: function (data, type, row) {
                                    return '<span href="#" data-toggle="tooltip" data-placement="top" title="' + row.NameOfWork + '">' + (row.NameOfWork || "").substring(0, 45).concat((row.NameOfWork || "").length > 45 ? ".." : "") + '</span>';
                                }
                            },
                            { data: "ProjectNo" },
                            { data: "ProjectDate" },
                            { data: "InsertedOn" },
                            { data: "InstDate" },
                            { data: "dprProgStat" },

                            {
                                data: null,
                                //width:'500px',
                                'className': 'ReleasedVsApprovedAmnt text-right',
                                render: function (data, type, row) {
                                    return row.TotalReleasedAmnt + ' /' + row.TotalApprovedAmount
                                }
                            },
                            {
                                data: null,
                                className: "center",
                                render: function (data, type, row) {
                                    var str1 = "<div class='dropdown'>";
                                    str1 += "<button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>Options<span class='caret'></span></button>";
                                    str1 += "<ul class='dropdown-menu'>";
                                    if (constans.roleId != 11) {
                                        str1 += "<li class='dropdown-header bg-gray'>Actions</li>";
                                    }
                                    var anchors1 = "";
                                  
                                    if (constans.uType == '3' && constans.roleId != 11) {
                                        if (row.Status === null) {
                                            //verification held
                                            anchors1 += "<li><a href='/Modules/Dpr/DprApproveOrReturn?DPChkSlId=" + row.MstDprId + "'>DPR Verification</a></li>";
                                        } else if (row.Status == "V") {
                                            //approval held
                                            anchors1 += "<li><a data-mstDprId='" + row.MstDprId + "' class='a-dpr-approval-modal' href='javascript:void(0);'>DPR Apporval</a></li>";
                                        }
                                        //edit dpr
                                        if (constans.uId == 136) {
                                            anchors1 += "<li><a href='/Modules/dpr/OldDprEntryForm?DprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Edit DPR</a></li>";
                                        }
                                       
                                        if (row.FundReleaseStatus && row.FundReleaseStatus.length > 0) {
                                            anchors1 += "<li><a href='javascript:void(0);' class='pop-edit-installments' data-installments='" + JSON.stringify(row.FundReleaseStatus) + "'>Edit Installments</a></li>";
                                        }
                                        if (row.Status === 'A') {
                                            //verify
                                            anchors1 += "<li><a href='/Modules/dpr/GetUIFundReleaseVerification?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Fund Release Verification</a></li>";
                                            anchors1 += "<li><a href='/Modules/dpr/GetUIInstallmentApproval?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Fund Release Approval</a></li>";
                                        }
                                        //apply to str1


                                    }
                                    var anchors2 = "";
                                    if (constans.uType == '2') {

                                        if (row.Status === 'A') {
                                            anchors2 += "<li><a href='/Modules/Dpr/GetUIApplyFundRelease?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Apply Installment</a></li>";
                                        }
                                        if (row.Status === 'R' || row.Status === null || row.Status === 'D') {
                                            anchors2 += "<li><a href='/Modules/Dpr/EditDprUI?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Edit DPR</a></li>";
                                        }
                                        if (row.Status === 'D' || row.Status === 'd') {
                                            anchors2 += "<li><a href='/Modules/Dpr/DprFinalSubmitionUI?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Upload CheckList Form</a></li>";
                                        }                                        
                                    }

                                    if ((anchors1 + anchors2) !== "") {
                                        str1 += (anchors1 + anchors2);
                                        str1 += "<li class='divider'></li>";
                                    }

                                    str1 += "<li class='dropdown-header bg-gray'>Reports</li>";
                                    str1 += "<li><a href='/Modules/dpr/DprReport?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Print Form</a></li>";

                                    if (constans.uType == '3' )
                                    {
                                        //str1 += "<li><a href='/Modules/dpr/SynopsisOfRSP' class='a-dpr-report' target='_blank'>Synopsis Of RSP</a></li>";
                                        //str1 += "<li><a href='/Modules/dpr/RSPDraft' class='a-dpr-report' target='_blank'>RSP Draft</a></li>";
                                        //str1 += "<li><a href='/Modules/dpr/SchemeWiseUC' class='a-dpr-report' target='_blank'>Scheme wise UC</a></li>";
                                        //str1 += "<li><a href='/Modules/dpr/YearWiseUC' class='a-dpr-report' target='_blank'>Year wise UC</a></li>";
                                        //str1 += "<li><a href='/Modules/dpr/CategoryWiseCount' class='a-dpr-report' target='_blank'>Category wise count</a></li>";
                                        //str1 += "<li><a href='/Modules/dpr/PhysicalProgressOfProject' class='a-dpr-report' target='_blank'>Physical Progress Of Projects</a></li>";
                                    }
                                    if (constans.uType == '3' && row.Status === 'A' && constans.roleId != 11) {//AA
                                        str1 += "<li><a href='/Modules/dpr/RptProjectDetails?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Project Details</a></li>";
                                        str1 += "<li><a href='/Modules/dpr/RptFileCover?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>File Cover Details</a></li>";
                                    }
                                    if (constans.uType == '3' && row.Status === 'R' && constans.roleId != 11) {
                                        str1 += "<li><a href='/Modules/dpr/RptReturnMemo?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Return Memo</a></li>";
                                    }
                                    if (constans.roleId != 11 && (constans.uType == '3' || constans.uType == '2') && (row.Status === 'V' || row.Status === 'A')) {
                                        str1 += "<li><a href='/Modules/dpr/RptAcknowledgement?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Acknowledgement</a></li>";
                                    } 
                                    //if (row.Status === 'A') {
                                    //    str1 += "<li><a href='/Modules/dpr/Rpt_AA_And_FS?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>AA & FS</a></li>";
                                    //}
                                    if (row.Status === 'A' && constans.roleId != 11 ) {
                                        str1 += "<li><a href='" + row.AAandFSDocUrl + "' class='a-dpr-report' target='_blank'>AA & FS</a></li>";
                                    }
                                    if (row.Status === 'V' && (constans.uType == '3') && constans.roleId != 11) {
                                        str1 += "<li><a href='/Modules/dpr/Rpt_NoteSheet?mstDprId=" + row.MstDprId + "' class='a-dpr-report' target='_blank'>Note Sheet</a></li>";
                                    }
                                    if (row.FundReleaseStatus && row.FundReleaseStatus.length > 0 && constans.roleId != 11) {
                                        //str1 += "<li class='divider'></li>";
                                        //str1 += "<li class='dropdown-submenu pull-left'> <a tabindex='-1' href='#'>Fund Release</a>";
                                        //str1 += "<ul class='dropdown-menu'>";
                                        str1 += "<li class='dropdown-header bg-gray'>Acknowledgement Reports</li>";
                                        row.FundReleaseStatus.forEach(function (v) {
                                            if (v.Status) {
                                                str1 += "<li><a href='/Modules/dpr/RptAcknowledgementFundRelease?mstDprId=" + row.MstDprId + "&fundReleaseId=" + v.FundReleaseId + "' class='a-dpr-report' target='_blank'>" + v.InstallmentName + "</a></li>";
                                            }
                                        });
                                        //str1 += "</ul>";
                                        //str1 += "</li>";
                                    }
                                    if (row.FundReleaseStatus && row.FundReleaseStatus.length > 0 && constans.roleId != 11) {
                                        str1 += "<li class='dropdown-header bg-gray'>Release Order</li>";
                                        row.FundReleaseStatus.forEach(function (v) {
                                            if (v.Status) {
                                                str1 += "<li><a href='" + v.ReleaseFile + "' class='a-dpr-report' target='_blank'>" + v.InstallmentName +"</a></li>";
                                            }
                                        });
                                       
                                    }
                                    str1 += "</ul>";
                                    str1 += "</div>";
                                    str1 += "</div>";
                                    return str1;
                                }
                            },
                            {
                                data: "TotalReleasedAmnt",
                                'className': 'TotalReleasedAmnt',
                                visible: false,
                            },
                            {
                                data: "TotalApprovedAmount",
                                'className': 'TotalApprovedAmount',
                                visible: false,
                            }
                        ],
                        "footerCallback": function (row, data, start, end, display) {
                            var api = this.api(), data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function (i) {
                                return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                        i : 0;
                            };

                            // Total over this page
                            var totalReleased = api
                                .column('.TotalReleasedAmnt')
                                .data()
                                .reduce(function (a, b) {
                                    return Number(a) + Number( b);
                                }, 0);

                            var totalApprovedAmount = api
                                .column('.TotalApprovedAmount')
                                .data()
                                .reduce(function (a, b) {
                                    return Number(a) + Number(b);
                                }, 0);

                            // Update footer
                            var footerVal = totalReleased + " /" + totalApprovedAmount
                            var footer = '<span href="#" data-toggle="tooltip" data-placement="top" title="' + footerVal + '">' + (footerVal || "").substring(0, 20).concat((footerVal || "").length > 10 ? ".." : "") + '</span>';
                            $(api.column('.ReleasedVsApprovedAmnt').footer()).html(footer);
                        }
                    });
                    //$('.tbl-dpr-list').DataTable({
                    //    buttons: [
                    //        {
                    //            extend: 'excel',
                    //            text: 'Save current page',
                    //            exportOptions: {
                    //                modifier: {
                    //                    page: 'current'
                    //                }
                    //            }
                    //        }
                    //    ]
                    //});
                },
                loadData: function (payload) {
                    //var data = { dateFrom: fromdate, dateTo: todate, status: status, UlbId: UlbId };
                    srv.GetDprList(payload, function (e) {
                        if (e.Status === 200) {
                            var datatable = doms().table.DataTable();
                            datatable.clear();
                            datatable.rows.add(e.Data.map(function (val, ind) {
                                return {
                                    sl: ++ind,
                                    MstDprId: val.DPChkSlId,
                                    DevAuth: val.MunicipalityDevAuthoName,
                                    SubmittedOn: val.InsertedOn ? CONVERTER.Date.convertCsToJsDate(val.InsertedOn).format('dd/mm/yyyy') : '',
                                    MemoNumber: val.MemoNumber,
                                    MemoDate: val.MemoDate ? CONVERTER.Date.convertCsToJsDate(val.MemoDate).format('dd/mm/yyyy') : '',
                                    ProjectNo: val.ProjectNo,
                                    ProjectDate: val.ProjectDate ? CONVERTER.Date.convertCsToJsDate(val.ProjectDate).format('dd/mm/yyyy') : '',
                                    InsertedOn: val.InsertedOn ? CONVERTER.Date.convertCsToJsDate(val.InsertedOn).format('dd/mm/yyyy') : '',
                                    InstDate: val.InstDate ? CONVERTER.Date.convertCsToJsDate(val.InstDate).format('dd/mm/yyyy') : '',
                                    Status: val.DPChkStatus,
                                    dprProgStat: val.dprProgStat,
                                    NameOfWork: val.NameOfWork,
                                    TotalReleasedAmnt: val.TotalReleasedAmnt,
                                    TotalApprovedAmount: val.TotalApprovedAmount,
                                    FundReleaseStatus: val.FundReleaseStatus,
                                    AAandFSDocUrl: val.AAandFSDocUrl
                                }
                            }));
                            datatable.draw();
                            if (e.Data.length <= 0) {
                                $(".download-excel").hide().attr('href', "#");
                            }
                        } else {
                            $(".download-excel").hide().attr('href', "#");
                            alertify.error(e.Message);
                        }
                    });
                }
            }
        }
        this.Init = function () {
            applyUlrFilter();
            loadConstants();
            bindEnvents();
            registerValidator();
            dataTable().configure();
            doms().btnShowByDate.trigger('click');
        };
        var bindEnvents = function () {
            if ($(".hdn-user-roleid").val() == 11) {
                $('.ddl-dpr-mylist').val('N');
            }
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
            doms().txtFromDate.datepicker({
                "dateFormat": 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                onSelect: function (selectedDate) {
                    doms().txtToDate.datepicker("option", "minDate", selectedDate);
                    doms().txtToDate.datepicker("option", "beforeShowDay", function (date) {
                        return [true, ''];
                    });
                }
            });
            doms().txtToDate.datepicker({
                changeMonth: true,
                changeYear: true,
                "dateFormat": 'dd/mm/yy', beforeShowDay: function (date) {
                    return [false, ''];
                }
            });
            doms().btnShowByDate.on('click', _eventHandlers._onClickShowByDate);
            $(".remove-date").on('click', function () {
                var clearTergate = $(this).attr("data-clear-tergate");
                $("." + clearTergate).val('');
                //$(this).siblings('input[type=text]').val('');
                if (clearTergate == "txt-from-date") {
                    doms().txtToDate.datepicker("option", "beforeShowDay", function (date) {
                        return [false, ''];
                    });
                }
            });
            function getBase64(file, name, callback) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    callback({ Result: reader.result, Name: name });
                };
                reader.onerror = function (error) {
                    alertify.error('Unable to read file');
                };
            };  
            $(document).on('change', ".file-aa-fs", function () {
                //AAFSFile = null;
                //if (this.files.length > 0) {
                //    var file = getBase64(this.files[0], this.files[0].name, function (data) {
                //        AAFSFile = data;
                //    });
                //} 
                var xyz = $(this);
                AAFSFile = {};
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        AAFSFile.StageId = response.Data;
                        AAFSFile.Result = null;
                        AAFSFile.Name = null;
                        alertify.success(response.Message);
                        $('.viewFile').css("display", "block");
                        $('.viewFile').attr("href", response.FileLink);
                        //$(xyz).val('');
                        //$(xyz).css('border-color', '#5aef0d');

                    }, function (response) {
                        alertify.error(response.Message);
                        $(this).val('');
                    });
                } 
            });
            $(document).on('click', ".pop-edit-installments", function () {
                var strInstallments = $(this).attr('data-installments');
                if (strInstallments) {
                    var installments = JSON.parse(strInstallments);
                    var rowString = "";
                    installments.forEach(function (v) {
                        rowString += "<tr>";
                        rowString += "<td>" + v.InstallmentName+"</td>"; 
                        rowString += "<td>" + v.StatusDscr+"</td>"; 
                        rowString += "<td><a target='_blank' href='/Modules/dpr/oldFundReleaseEntryForm?DprId=" + v.MstDprId + "&fundReleaseId=" + v.FundReleaseId+"'>Edit</a></td>"; 
                        rowString += "</tr>";
                    });
                    if (!rowString)
                    {
                        rowString += "<tr><td colspan='3' class='text-center'>no installment found</td></tr>";
                    }
                    $(".edit-installment-modal table tbody").empty().append(rowString);
                    $(".edit-installment-modal").modal('show');
                }
            });
            $('.ulbs').fSelect();
            //$(".fs-dropdown .fs-options> .fs-option[data-value='0']").on('click', function (e) {
            //    console.log($(e.target).closest('.fs-option').hasClass('selected'));
            //});
            $(document).on('fs:changed', function (e, ctrl) {
                if ($(ctrl).attr('data-value') == 0) {
                    if ($(ctrl).hasClass('selected')) {
                        $(ctrl).parent().find(".fs-option:not([data-value='0']):not(.selected)").trigger('click');
                    } else {
                        $(ctrl).parent().find(".fs-option:not([data-value='0']).selected").trigger('click');
                    }
                }
            });
            $(".ddl-schemetypes").on('change', function () {
                var schemeid = $(this).val().toNumber();
                var categoryCtrl = $('.ddl-scheme-categories');
                categoryCtrl.find('option:not(:first-child)').remove();
                if (schemeid) {
                    var categoriesRaw = JSON.parse(categoryCtrl.attr('data-all-categories'));
                    categoriesRaw = categoriesRaw.filter(function (v) {
                        return v.SchemeTypeId === schemeid;
                    });
                    categoriesRaw.forEach(function (v, i) {
                        categoryCtrl.append('<option value="' + v.Id + '">' + v.Name + '</option>');
                    });
                }
            });
            
           
        };
        var _eventHandlers = {
            _onClickShowByDate: function (e) {
                e.preventDefault();
                var getUlbIds = function () {
                    var arr = doms().ulbId.val();
                    var index = arr.indexOf("0");
                    if (index !== -1) arr.splice(index, 1);
                    return {
                        MunicipalityIds: arr,
                        TimeSpanFlag: "date-range",
                        FromDate: convertToDate(doms().txtFromDate.val()),
                        ToDate: convertToDate(doms().txtToDate.val()),
                        //Status: doms().rdoStatus.val(),
                        Status: $(".ddl-rdo_status").val(),
                        SchemeTypeId: window.isNullOrEmpty($('.ddl-schemetypes').val()),
                        SchemeCategoryId: window.isNullOrEmpty($('.ddl-scheme-categories').val()),
                        DprType: $(".ddl-dpr-type").val(),
                        MyList: $(".ddl-dpr-mylist").val()
                    };
                };
                
                if (doms().frm.valid()) {
                    var payload = getUlbIds();
                    var payloadStr64 = btoa(JSON.stringify(payload));
                    payloadStr64 = encodeURIComponent(payloadStr64);
                    $(".download-excel").show().attr('href', "/Modules/Dpr/ExportDprList?_payload=" + payloadStr64);
                    dataTable().loadData(payload);//convertToDate(doms().txtFromDate.val()), convertToDate(doms().txtToDate.val()), doms().rdoStatus.val(), getUlbIds());
                }
            }
        };
        var registerValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    var inputGroup = element.parent(".input-group");
                    if (inputGroup.length > 0) {
                        error.insertAfter(inputGroup);
                    } else {
                        error.insertAfter(element);
                    }
                    error.css('color', 'red');
                }
            }
            vld.rules[doms().txtFromDate.attr('name')] = { required: true };
            vld.rules[doms().txtToDate.attr('name')] = { required: true };
            doms().frm.validate(vld);
        }
    };

    var ApprovalApp = function (srv) {
        var doms = function () {
            return {
                parentModal: ".dpr-approval-modal",
                finalModal:".final-approval-modal"
            }
        };
        function SobProjNotSelectedFun() {
            var arr = [];
            $(".check-sbp").each(function () {
                var ischecked = $(this).is(':checked');
                if (!ischecked) {
                    var ob = {
                        sbpId: $(this).attr("data-sbpId"),
                        DprId: $(this).attr("data-dprId"),
                    }
                    arr.push(ob);
                }
            })
            return arr;
        };
        var getFormdataFirstModal = function () {
            return {
                mstDprId: $(doms().parentModal).find("#mstDprId").val(),
                status: $(doms().parentModal).find("#status").val(),
                remarks: $(doms().parentModal).find("#msg").val(),
                targetDesignationId: $(doms().parentModal).find("#designation").val(),
                approvedProjectAmount: (isNaN($(doms().parentModal).find("#approved-amount").val()) ? 0 : Number($(doms().parentModal).find("#approved-amount").val())),
                Subprojects: $(".tbl-sub-projects tbody tr.data")
                    .map(function (i, v) { return { Cost: $(v).find(".txt-subp-cost").val().toNumber(), Id: $(v).attr('data-id').toNumber() }; }).get(),
                SobProjNotSelected: SobProjNotSelectedFun()
            };
        };
        var getInternalMsgFirstModal = function () {
            return {
                mstDprId: $(doms().parentModal).find("#mstDprId").val(),
                InternalMessage: $(doms().parentModal).find("#Internalmsg").val()
            };
        };
        var getFormdataSecondModal = function () {
            return {
                AccountHead: $(doms().finalModal).find(".txt-account-head").val(),
                UoDate: window.toDate($(doms().finalModal).find(".txt-uo-date").val()),
                SanctionNo: $(doms().finalModal).find(".txt-sanction-number").val(),
                UoNo: $(doms().finalModal).find(".txt-uo-no").val(),
                //ApprovalDate: window.toDate($(doms().finalModal).find(".txt-approval-date").val()),
                //BudgetId: $(".ddl-budget-code").val().toNumber(),
                SanctionDate: window.toDate($(doms().finalModal).find(".txt-approval-date").val()),
                AccordedBy: $(doms().finalModal).find(".ddl-accorded-by").val(),
                AAFSFile: AAFSFile,
                //BudgetHeads: $(doms().finalModal).find(".tbl-account-heads tbody tr:not(.no-row)")
                //    .map(function (i, v){
                //        return {
                //            DeductedAmount: ($(v).find(".txt-amount").val().isNumber() ? $(v).find(".txt-amount").val().toNumber() : "0"),
                //            Id: $(v).find(".ddl-budget-code").val()
                //        }
                //    }).get()
            };
        };
        var validateFirstModalForm = function () {
            var HasSubProj = $("#HasSubProj").val();
            registerFirstModalValidator();
            var totalSubPrjectAmount = $(".tbl-sub-projects tbody tr.data").map(function (i, v) { return $(v).find(".txt-subp-cost").val().toNumber() }).get().reduce(function (a, b) { return a + b; }, 0);
            var approvedAmount = $("#approved-amount").val().toNumber();
            if (HasSubProj == true)
            {
                if (approvedAmount !== totalSubPrjectAmount) {
                    alertify.error("Sum of sub project cost and total approved amount should be equal");
                    return false;
                }
            }
            return $(doms().parentModal).find('form').valid();
        };
        var validateSecondModalForm = function () {
            registerSecondModalValidator();
            return $(doms().finalModal).find('form').valid();;
        };

        var ChangeDprApprovalStatusExceptApproval = function () {
            var parent = $(doms().parentModal).find('form');
            if (validateFirstModalForm()) {
                alertify.confirm('Confirm Us!', 'Are you sure to change status?', function () {
                    var data = getFormdataFirstModal();
                    srv.ChangeDprApprovalStatus(data, function (r) {
                        if (r.Status === 200) {
                            alertify.alert('Successful', r.Message, function () {
                                LoadDprWithProgress(data.mstDprId);
                            });
                        } else {
                            alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                        }
                    });
                }, function () { })

            }
        };
        var SaveInternalMessage = function () {
            var data = getInternalMsgFirstModal();
            srv.SvInternalMessage(data, function (r) {
                if (r.Status === 200) {
                    alertify.alert('Successful', r.Message, function () {
                        LoadDprWithProgress(data.mstDprId);
                    });
                } else {
                    alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                }
            });
        };
        var FinalApprove = function (successcallback) {
            var parent = $(doms().parentModal).find('form');
            if (validateFirstModalForm() && validateSecondModalForm()) {
                alertify.confirm('Confirm Us!', 'Are you sure to Approve Finally?', function () {
                    var data = getFormdataFirstModal();
                    var data2 = getFormdataSecondModal();
                    var payload = Object.assign(data, data2);
                    srv.ChangeDprApprovalStatus(data, function (r) {
                        if (r.Status === 200) {
                            alertify.alert('Successful', r.Message, function () {
                                LoadDprWithProgress(data.mstDprId);
                                if (successcallback)
                                    successcallback();
                            });
                        } else {
                            alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                        }
                    });
                }, function () { })

            }
        };
        var GenerateAAFS = function (successcallback) {
            var parent = $(doms().parentModal).find('form');
            if (validateFirstModalForm() && validateSecondModalForm() /*&& validateBudgetHeads()*/) {
                alertify.confirm('Confirm us!', 'Are you sure to Generate AA & FS?', function () {
                    var data = getFormdataFirstModal();
                    var data2 = getFormdataSecondModal();
                    var payload = Object.assign(data, data2);
                    srv.GenerateAAFS(payload, function (r) {
                        if (r.Status === 200) {
                            alertify.alert('Successful', r.Message, function () {
                                LoadDprWithProgress(data.mstDprId);
                                if (successcallback)
                                    successcallback();
                            });
                        } else {
                            alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                        }
                    });
                }, function () { })

            }
        };
        var LoadDprWithProgress = function (mstDprId) {
            srv.GetApprovalUI({ mstDprId: mstDprId }, function (r) {
                $(doms().parentModal).modal('show');
                $(doms().parentModal).find('.modal-body').empty().append(r);
                //make visble scrollbar
                $(".box-dpr-messages .box-body").slimScroll({
                    height: $(".box-dpr-messages  .box-body").css('height'),
                    allowPageScroll: true,
                    alwaysVisible: true
                })
                $('.btn-search-by-date').trigger('click');
                $(doms().parentModal).find("#approved-amount").prop('disabled', !($(doms().parentModal).find("#canApprovalAmtChangble").val() == 1 ? true : false));
                $(doms().parentModal).find(".check-sbp").prop('disabled', !($(doms().parentModal).find("#canApprovalAmtChangble").val() == 1 ? true : false));
                $(doms().parentModal).find("#status").trigger('change');
                
                $(".btn-gen-aafs").hide();
                $(doms().parentModal).find('.modal-title').text('Final Approval')
                if ($(".CanGenerateAAFS").val() == 1) {
                    $(doms().finalModal).find('.modal-title').text('Generate AA & FS')
                    $(".btn-gen-aafs").show();
                }
                $(".btn-final-approve").addClass('hidden');
                if ($("#status").val() == "A") {
                    $(".btn-final-approve").removeClass('hidden');
                }
                fillAaFsOnModal();
                makeModalReadyOnlyOnLogic();
                
            });
        };
        var fillAaFsOnModal = function () {
            var aafs = $(doms().parentModal).find(".hdn-aafs").val();
            if (aafs && aafs != "null") {
                aafs = JSON.parse(aafs);
                $(doms().finalModal).find(".txt-account-head").val(aafs.AccountHead);
                $(doms().finalModal).find(".txt-uo-date").val(aafs.UoDate ? new Date(aafs.UoDate).format('dd/mm/yyyy'):'');
                $(doms().finalModal).find(".txt-sanction-number").val(aafs.SanctionNo);
                $(doms().finalModal).find(".txt-uo-no").val(aafs.UoNo);
                $(doms().finalModal).find(".ddl-budget-code option[value='" + aafs.BudgetId + "']").prop('selected', true);
                $(doms().finalModal).find(".txt-approval-date").val(aafs.SanctionDate ? new Date(aafs.SanctionDate).format('dd/mm/yyyy') : '');
                $(doms().finalModal).find(".ddl-accorded-by").val(aafs.AccordedBy);
                $('.viewFile').css("display", "block");
                $('.viewFile').attr("href", aafs.AAFSFilePath);
            }

            //var table = $(doms().finalModal).find(".tbl-account-heads");
           // fillBudgetsIfExistOld();
            //var totalDed = table.find('tbody tr:not(.no-row)')
            //    .map(function (i, v) { return $(v).find(".txt-amount").val().isNumber() ? $(v).find(".txt-amount").val().toNumber() : "0" })
            //    .get()
            //    .reduce(function (a, b) { return a + b; }, 0);
            //table.find('tfoot tr .tol-ded-amt').text(totalDed + "/" + $("#approved-amount").val());
        };
        //var fillBudgetsIfExistOld = function () {
        //    var table = $(doms().finalModal).find(".tbl-account-heads");
            

        //    var existingStr = $(".hdn-applied-budgets").val();
        //    if (existingStr) {
        //        var existingData = JSON.parse(existingStr);
        //        if (existingData.length > 0) {
        //            table.find('tbody tr:not(.no-row)').remove();
        //        }
        //        existingData.forEach(function (v, i) {
        //            var ddlBudget = $("<select class='form-control ddl-budget-code' name='ddl-budget-code'/>");
        //            ddlBudget.append('<option value="" data-avamt="">--select--</option>');
        //            if ($("#hdn-budgets").val()) {
        //                var budgets = JSON.parse($("#hdn-budgets").val());
        //                budgets.forEach(function (v, i) {
        //                    ddlBudget.append("<option value='" + v.Id + "' data-avamt='" + v.AvAmnt + "'>" + v.BCode + "-" + v.Dscr + "</option>");
        //                });
        //            };

        //            ddlBudget.find('option[value="' + v.Id + '"]').prop('selected', true);
        //            var curBalanse = ddlBudget.find('option:selected').attr('data-avamt').toNumber();
        //            var dedAmount = v.DeductedAmount;
        //            var restAmount = curBalanse - dedAmount;
        //            var tr = $("<tr/>");
        //            var td = $("<td/>");
        //            td.append(table.find('tbody tr:not(.no-row)').length + 1);
        //            tr.append(td);
        //            td = $("<td/>");
        //            td.append(ddlBudget);
        //            tr.append(td);

        //            td = $("<td class='row-av-amt'/>");//current balnace
        //            td.append(curBalanse);
        //            tr.append(td);
        //            td = $("<td />");//deductable amount
        //            td.append('<input type="text" class="form-control txt-amount" disabled placeholder="amount" value="' + dedAmount+'"/>');
        //            tr.append(td);
        //            td = $("<td class='row-rest-amt'/>");
        //            td.append(restAmount)
        //            tr.append(td);
        //            td = $("<td/>");
        //            td.append("<a href='javascript:void(0);' class='del-row'><span class='glyphicon glyphicon-trash'></span></a>");
        //            tr.append(td);
        //            table.find('tbody').append(tr);
        //        });
        //    }
        //};
        var makeModalReadyOnlyOnLogic = function () {
            var CanGenerateAAFS =   ($(".CanGenerateAAFS").val() == 1);
            var isReadonly = !CanGenerateAAFS;
            $(doms().finalModal).find(".txt-account-head").prop('readonly', isReadonly);
            $(doms().finalModal).find(".txt-uo-date").datepicker("option", "disabled", isReadonly);
            $(doms().finalModal).find(".txt-sanction-number").prop('readonly', isReadonly);
            $(doms().finalModal).find(".txt-uo-no").prop('readonly', isReadonly);
            $(doms().finalModal).find(".ddl-budget-code").prop('readonly', isReadonly);
            $(doms().finalModal).find(".txt-approval-date").datepicker("option", "disabled", isReadonly);
            $(doms().finalModal).find(".ddl-accorded-by").prop('disabled', isReadonly);
            $(doms().finalModal).find(".file-aa-fs").prop('disabled', isReadonly);

            //var tableBudget = $(doms().finalModal).find(".tbl-account-heads");
            //tableBudget.find('.ddl-budget-code').prop('readonly', isReadonly);
            //tableBudget.find('.txt-amount').prop('readonly', isReadonly);
            //if (isReadonly) {
            //    tableBudget.find('.add-row').hide();
            //    tableBudget.find('.del-row').hide();
            //} else {
            //    tableBudget.find('.add-row').show();
            //    tableBudget.find('.del-row').show();
            //}
            
        };
        var registerFirstModalValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    if (element.siblings(".input-group-addon").length > 0) {
                        error.insertAfter(element.parent());
                        error.css('color', 'red');
                        return;
                    }
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            };
            $.validator.addMethod('positiveNumber', function (value) {
                return Number(value) > 0;
            }, 'Enter a positive decimal.');

            vld.rules["msg"] = { required: true };
            vld.rules["status"] = { required: true };
            vld.rules["designation"] = { required: (($(doms().parentModal).find("#status").val() !== "N") || ($(doms().parentModal).find("#status").val() !== "A" && $("#isFinallAprovalPossible").val() == 0)) };
            vld.rules["approved-amount"] = { required: ($(doms().parentModal).find("#approved-amount").prop('disabled') === false), positiveNumber: ($(doms().parentModal).find("#approved-amount").prop('disabled') === false) }
            $(doms().parentModal).find('form').validate(vld);
        };
        var registerSecondModalValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    if (element.siblings(".input-group-addon").length > 0) {
                        error.insertAfter(element.parent());
                        error.css('color', 'red');
                        return;
                    }
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            };
            $.validator.addMethod('positiveNumber', function (value) {
                return Number(value) > 0;
            }, 'Enter a positive decimal.');

            vld.rules["txt-approval-date"] = { required: true };
            vld.rules["file-aa-fs"] = { required: true };
            $(doms().finalModal).find('form').validate(vld);
        };
        //var validateBudgetHeads = function () {
        //    var flag = true;
        //    var table = $(doms().finalModal).find(".tbl-account-heads");
        //    if (table.find('tbody tr:not(.no-row)').length == 0) {
        //        flag = false;
        //        alertify.error('Please at least on budget head');
        //        return flag;
        //    }
        //    $(doms().finalModal).find(".tbl-account-heads tbody tr:not(.no-row)").removeClass('bg-red');
        //    $.each($(doms().finalModal).find(".tbl-account-heads tbody tr:not(.no-row)"), function (i, v) {
        //        var budgetCode = $(v).find('.ddl-budget-code').val();
        //        if (!budgetCode) {
        //            alertify.error('Invalid Selection: Head of account');
        //            flag = false;
        //        }
        //        var amount = $(v).find('.txt-amount').val().toNumber();
        //        if (isNaN(amount) || amount <= 0) {
        //            alertify.error('Amount can not be less than zero');;
        //            $(v).find('.txt-amount').focus();
        //            flag = false;
        //        }


        //        if (!flag) {
        //            $(v).addClass('bg-red');
        //        }
        //        //validate totality
        //        var totalDed = $(this).closest('table tbody').find('tr:not(.no-row)')
        //            .map(function (i, v) { return $(v).find(".txt-amount").val().isNumber() ? $(v).find(".txt-amount").val().toNumber() : "0" })
        //            .get()
        //            .reduce(function (a, b) { return a + b; }, 0);
        //        var totApproveAmt = $("#approved-amount").val().toNumber();
        //        if (totApproveAmt != totalDed) {
        //            alertify.error('Approved amount and total budget amount should be equal');
        //            flag = false;
        //        }
        //    });
        //    return flag;
        //};
        
        (function () {
            $(doms().parentModal).find('.modal-dialog').css('width', '95%');
            $(document).on('click', ".a-dpr-approval-modal", function () {
                LoadDprWithProgress($(this).attr('data-mstdprid'));
            });
            $(".ddl-budget-code").on('change', function () {
                var avAmt = $(this).find('option:selected').attr('data-avamt');
                $(".budget-amt-show").empty();
                if (avAmt) {
                    $(".budget-amt-show").append("Rs. " + avAmt);
                }
            });
            $(doms().parentModal).on('click', "#btn-submit-msg", function () {
                if ($(doms().parentModal).find("#status").val() == "A" || $(".CanGenerateAAFS").val() == 1) {
                    if (validateFirstModalForm())
                    $(".final-approval-modal").modal('show');
                    return;
                }
                ChangeDprApprovalStatusExceptApproval();
            });
            $(doms().parentModal).on('click', "#btn-submit-Internalmsg", function () {
                SaveInternalMessage();
            });
            $(doms().parentModal).on('change', ".check-sbp", function () {
                var SumOfAppliedAmt = parseFloat("0.0");
                $(".check-sbp").each(function () {
                    var ischecked = $(this).is(':checked');
                    if (ischecked) {
                        SumOfAppliedAmt = parseFloat(SumOfAppliedAmt) + parseFloat($(this).attr("data-cost"));
                    }
                });
                $('#approved-amount').val(SumOfAppliedAmt.toFixed(2));
            }); 
            $(".final-approval-modal").on('click', ".btn-gen-aafs", function () {
                GenerateAAFS(function () {
                    $(".final-approval-modal").modal('hide');
                    $(".final-approval-modal").find('form').trigger("reset");
                });

            });
            $(doms().finalModal).on('click', '.btn-final-approve', function (e) {
                e.preventDefault();
                FinalApprove(function () {
                    $(".final-approval-modal").modal('hide');
                    $(".final-approval-modal").find('form').trigger("reset");
                });
            });
            $(document).on('hidden.bs.modal', function (event) {
                if ($('.modal:visible').length) {
                    $('body').addClass('modal-open');
                }
            });
            $(doms().parentModal).on('change', "#status", function () {
                if ($(this).val() == "A" || $("#installmentStatus").val() == "A") {
                    $(doms().parentModal).find("#designation>option:first").prop('selected', true);
                    $(doms().parentModal).find("#designation").prop('disabled', true);
                    $(".btn-final-approve").removeClass('hidden');
                } else {
                    $(doms().parentModal).find("#designation").prop('disabled', false);
                    $(".btn-final-approve").addClass('hidden');
                }
            });
            $(doms().parentModal).on('keyup', ".txt-subp-cost", function () {
                $(this).closest('tr').find('.check-sbp').attr('data-cost', $(this).closest('tr').find(".txt-subp-cost").val());
                $(".tbl-sub-projects tfoot .lbl-sb-totalCost").text($(".tbl-sub-projects tbody tr.data").map(function (i, v) { return $(v).find(".txt-subp-cost").val().toNumber() }).get().reduce(function (a, b) { return a + b; }, 0));
                $(".check-sbp").trigger("change");
            });
            //$(doms().finalModal).find(".tbl-account-heads thead tr").on('click', '.add-row', function (e) {
            //    e.preventDefault();
            //    var table = $(this).closest('table');
                
            //    var getRowTemplate = function () {
            //        var ddlBudget = $("<select class='form-control ddl-budget-code' name='ddl-budget-code'/>");
            //        ddlBudget.append('<option value="" data-avamt="">--select--</option>');
            //        if ($("#hdn-budgets").val()) {
            //            var budgets = JSON.parse($("#hdn-budgets").val());
            //            budgets.forEach(function (v, i) {
            //                ddlBudget.append("<option value='" + v.Id + "' data-avamt='" + v.AvAmnt + "'>" + v.BCode + "-" + v.Dscr + "</option>");
            //            });
            //        };

            //        var tr = $("<tr/>");
            //        var td = $("<td/>");
            //        td.append(table.find('tbody tr:not(.no-row)').length + 1);
            //        tr.append(td);
            //        td = $("<td/>");
            //        td.append(ddlBudget);
            //        tr.append(td);
                    
            //        td = $("<td class='row-av-amt'/>");//current balnace
            //        td.append(0);
            //        tr.append(td);
            //        td = $("<td />");//deductable amount
            //        td.append('<input type="text" class="form-control txt-amount" disabled placeholder="amount" value="0"/>');
            //        tr.append(td);
            //        td = $("<td class='row-rest-amt'/>");
            //        td.append(0)
            //        tr.append(td);
            //        td = $("<td/>");
            //        td.append("<a href='javascript:void(0);' class='del-row'><span class='glyphicon glyphicon-trash'></span></a>");
            //        tr.append(td);
            //        return $('<div/>').append(tr).html();
            //    };
            //    table.find('tbody').append(getRowTemplate());
            //    table.find('tbody tr.no-row').remove();
            //});
            //$(doms().finalModal).find(".tbl-account-heads tbody").on('change', '.ddl-budget-code,.txt-amount', function () {
            //    var tr = $(this).closest('tr');
            //    var ctrl = $(this);
            //    if ($(this).hasClass('ddl-budget-code')) {
            //        if (!$(this).val()) {
            //            tr.find(".txt-amount").prop('disabled', true);
            //            tr.find(".txt-amount").val(0);
            //            tr.find('.row-rest-amt').text(0);
            //            tr.find('.row-av-amt').text(0);
            //        } else {
            //            tr.find(".txt-amount").prop('disabled', false);
            //        }
            //        var idAlreadySelected = $(this).closest('tr:not(.no-row)').siblings()
            //            .map(function (i, v) { return $(v).find(".ddl-budget-code").val(); })
            //            .get()
            //            .filter(function (v) { return v == ctrl.val() });
            //        if (idAlreadySelected.length > 0) {
            //            alertify.error('Head of account already selected. Please select another account');
            //            $(this).find('option:first-child').prop('selected', true);
            //        }
            //    }
                
            //    var avAmt = tr.find('.ddl-budget-code option:selected').attr("data-avamt").toNumber();
            //    tr.find('.row-av-amt').text(avAmt);
            //    var dedAmt = tr.find(".txt-amount").val().isNumber() ? tr.find(".txt-amount").val().toNumber() : "0";
            //    var upcomAmt = avAmt - dedAmt;
                
            //    if (upcomAmt < 0) {
            //        tr.find(".txt-amount").val(0);
            //        tr.find('.row-rest-amt').text(avAmt);
            //        alertify.error('Budget Amount exceeded');
            //    } else {
            //        tr.find('.row-rest-amt').text(upcomAmt);
            //    }

            //    var totalDed = $(this).closest('table tbody').find('tr:not(.no-row)')
            //        .map(function (i, v) { return $(v).find(".txt-amount").val().isNumber() ? $(v).find(".txt-amount").val().toNumber() : "0" })
            //        .get()
            //        .reduce(function (a, b) { return a + b; }, 0);
            //    $(this).closest('table').find('tfoot tr .tol-ded-amt').text(totalDed + "/" + $("#approved-amount").val());

            //});
            //$(doms().finalModal).find(".tbl-account-heads tbody").on('click', '.del-row', function () {
            //    var table = $(doms().finalModal).find(".tbl-account-heads");
            //    $(this).closest('tr').remove();
            //    var totalDed = table.find('tbody').find('tr:not(.no-row)')
            //        .map(function (i, tr) { return tr.find(".txt-amount").val().isNumber() ? tr.find(".txt-amount").val().toNumber() : "0" })
            //        .get()
            //        .reduce(function (a, b) { return a + b; }, 0);
            //    table.find('tfoot tr .tol-ded-amt').text(totalDed + "/" + $("#approved-amount").val());
            //    if (table.find('tbody tr').length == 0) {
            //        table.find('tbody').append("<tr class='no-row'><td colspan='6' class='text-center'>No data added</td></tr>");
            //    }
            //});
        }());
    };
    (function () {
        var srv = new Server();
        var app = new App(srv);
        var approvalApp = new ApprovalApp(srv);
        app.Init();
    }())
});