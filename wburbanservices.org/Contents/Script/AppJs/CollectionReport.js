﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {        
        this.GetCountersByMunicipalityID = function (callback) {        
            $.ajax({
                type: "POST",
                url: "/Municipality/MnCounter/GetActiveCountersForReports",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };       
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.ReportCalling = function (data, callback) {
            alert('calling..');
            $.ajax({
                type:'post',
                contentType: 'application/json;cursor=utf-8',
                data: JSON.stringify(data),
                dataType: 'json',
                url: "/Municipality/MnReports/CollectionReport",
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    alert('error occur');
                }
            });
        };
        this.ExportReport = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: 'application/json;cursor=utf-8',
                data: JSON.stringify(data),
                dataType: 'json',
                url: "/Municipality/MnReports/CollectionReportExcel",
                success: function (response) {
                    callback(response.Data);
                },
                error: function (err) {
                    alert(err.Message);
                }
                });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            }           
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        Resetors.ResetLocation();                       
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            if ((serverResponse).length > 0) {
                                var userDataAutoComplete = [];
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                                response(userDataAutoComplete);
                            }
                        });
                    },
                    select: function (e, i) {
                        $("#WardID").val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();                          
                        }
                    }
                };
            },
            Locations: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');                     
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            if ((serverResponse).length > 0) {
                                var userDataAutoComplete = [];
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });
                                response(userDataAutoComplete);
                            }
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {                           
                        }
                    }
                };
            }          
        };

        var DomControls = {
            ctrl_hdnWard: $("#WardID"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#LocationID"),
            ctrl_txtLocation: $("#txtLocation")        
        };
        var Bind_GridOnPageLoad = function () {
            var RegisterEvent = function () {
            };
            (function () {
                RegisterEvent();
            }());
        };
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $("#btnExport").on('click', Export);
            $("#btnPDF").on('click', Insert);
            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
                //onSelect: function () {
                //    $('#DateTo').datepicker('option', 'maxDate',setMaxDate());
                //}
            });
            $('#DateTo,#DateFrom').blur(function () {
                if ($(this).val().trim() == '') {
                    return false;
                }
            });
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            PopulateCounters();

            $('#divCounter').on('change', '#chkAll', function () {
                $("#tabCounter tbody tr").find(".chkCnt").prop('checked', $("#chkAll").prop("checked"));
            });
        };
        var setMaxDate = function () {
            var arr = $('#DateFrom').val().split("/");
                var date = new Date(arr[2] + "-" + arr[1] + "-" + arr[0]);
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
                var minDate = new Date(y, m, d + 6);
                return minDate;
        };
        var getDataFromView = function () {
            GetCounters();
            var DateFrom = $('#DateFrom').val().trim();
            var DateTo = $('#DateTo').val().trim();
            var WardName = $('#txtWard').val().trim();
            var WardID = $('#WardID').val();
            var LocationName = $('#txtLocation').val().trim();
            var LocationID = $('#LocationID').val();
            var CounterID = $('#hdnCounter').val();
            var paymentTyp = $('#PaymentTyp').val();
            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }

            var data = { DateFrom: DateFrom, DateTo: DateTo, WardID: WardID, LocationID: LocationID, CounterID: CounterID, PaymentTyp: paymentTyp };
            return data;
        };
        var Pollyfills = function () {
            jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                        || (Number(value) > Number($(params).val()));
                }, 'Must be greater than {0}.');
        };
        var GetCounters = function () {
            var counterID = "";
            $.each($(".select-counter").find("table tbody").find(".chkCnt:checked"), function (idx, counter) {
                counterID = counterID + $(counter).val() + ",";
            });
            counterID = counterID.substring(1 - counterID.length, counterID.length - 1);
            $("#hdnCounter").val(counterID);
        };
        var PopulateCounters = function () {
            serverObject.GetCountersByMunicipalityID(function (response) {
                $(".select-counter").find("table tbody").empty();
                if (response.Status == 200 && (response.Data).length > 0) {
                    $.each(response.Data, function (idx, counter) {
                        var str = "<tr>";
                        str += "<td><input type='checkbox' class='chkCnt' value='" + counter.CounterID + "'/></td><td><label>&nbsp;&nbsp;" + counter.CounterCode + "</label></td>";
                        str += "</tr>";
                        $(".select-counter").find("table tbody").append(str);
                    });
                }
            }); 

        };
        var GetDaysValidate = function () {
            var startDT = new Date($("#DateFrom").val().split('/')[2] + '-' + $("#DateFrom").val().split('/')[1] + '-' + $("#DateFrom").val().split('/')[0]);
            var endDT = new Date($("#DateTo").val().split('/')[2] + '-' + $("#DateTo").val().split('/')[1] + '-' + $("#DateTo").val().split('/')[0]);
            var diffday = (((((endDT - startDT) / 1000) / 60) / 60) / 24) + 1;
            if (Math.floor(diffday) > 7) {
                return false;
            }
            else
            {
                return true;
            }
        };
        var Validate = function (data) { 
            
            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];           
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            if ($('#PaymentTyp').val() == 'F')
            {
                if ($("#hdnCounter").val().length == 0) {
                    alert("Select atleast one Counter");
                    return false;
                }
            }
           
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                var cmd = $(this).attr('data-command');
                if (cmd == 'print') {
                    $('#frmCollectionReport').attr('action', '/Municipality/MnReports/CollectionReport');
                } else if (cmd == 'pdf') {
                    $('#frmCollectionReport').attr('action', '/Municipality/MnReports/CollectionReportPDF');
                }
                if (GetDaysValidate()) {
                    $('#frmCollectionReport').submit();
                } else {
                    if (cmd == 'pdf') {
                        $('#frmCollectionReport').submit();
                    } else {
                        alert("Not more then 7 days to print the report...");
                    }
                }
                //   print report here...
            }
        };
        var Export = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.ExportReport(formData, function (respons) {
                        if (respons.length > 0) {
                            console.log(respons);
                            respons = respons.replace(/\\/g, "/");
                            location.href = respons;
                        }
                    });
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            Bind_GridOnPageLoad();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});