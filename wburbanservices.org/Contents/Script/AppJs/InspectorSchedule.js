﻿var municipalityid = '', ApplicationId = '', mode='';

$(document).ready(function () {
    Bind_Date();

    $("#btnAdd").click(function (evt) {
        Add();
    });
    $("#btnRefresh").click(function (evt) {
        Refresh();
    });
    $("#btnSave").click(function (evt) {
        Save();
    });


    //ApplicationId = getParameterByName("ApplicationId", window.location.href);
    //mode = getParameterByName("mode", window.location.href); 

    ApplicationId = $('.clsVariables').attr('ApplicationId');
    mode = $('.clsVariables').attr('UrlMode'); 
    municipalityid = $('.clsVariables').attr('municipalityid'); //alert(UserType);

    var minDate = $('.clsVariables').attr('mindate');
    var maxDate = $('.clsVariables').attr('maxdate');
    $("#txtinfrmdate").datepicker("option", "minDate", minDate);
    $("#txtinfrmdate").datepicker("option", "maxDate", maxDate);

    $("#txtintodate").datepicker("option", "minDate", minDate);
    $("#txtintodate").datepicker("option", "maxDate", maxDate);
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function Bind_Date() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();


    $('#txtinfrmdate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $("#txtintodate").datepicker("option", "minDate", $("#txtinfrmdate").val());
            if ($('#txtinfrmdate').val() != '' && $('#txtintodate').val() != '' && $('#txtinfrmdatetime').val() != '' && $('#txtintodatetime').val() != '')
                Bind_Inspector();
        }
    });
    $('#txtintodate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {

            $("#txtintodate").datepicker("option", "minDate", $("#txtinfrmdate").val());
            if ($('#txtinfrmdate').val() != '' && $('#txtintodate').val() != '' && $('#txtinfrmdatetime').val() != '' && $('#txtintodatetime').val() != '')
                Bind_Inspector();
        }
    });

    //$('#txtinfrmdatetime, #txtintodatetime').timepicker({
    //    timeFormat: 'h:mm p',
    //    interval: 05,
    //    minTime: '06',
    //    //maxTime: '6:00pm',
    //    startTime: '06:00',
    //    dynamic: false,
    //    dropdown: true,
    //    scrollbar: true
    //});

    $('#txtinfrmdatetime').timepicker({
        timeFormat: 'h:mm p',
        interval: 05,
        minTime: '6:00am',
        maxTime: '6:00pm',
        startTime: '06:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: FromTimeChange
    });
    $('#txtintodatetime').timepicker({
        timeFormat: 'h:mm p',
        interval: 05,
        minTime: '6:00am',
        //maxTime: $('#txtinfrmdatetime').val(),
        startTime: '06:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: ToTimeChange
    });

    $("#txtVerificationDate").val(output);
  
}

function FromTimeChange()
{
    var ftime = $('#txtinfrmdatetime').val(); 
    //var a = ftime.split(' ')[0];
    //var b = ftime.split(' ')[1];
    //var c = a + b; 

    $("#txtintodatetime").val('');
    $("#txtintodatetime").timepicker("option", "minTime", ftime);

    if ($('#txtinfrmdate').val() != '' && $('#txtintodate').val() != '' && $('#txtinfrmdatetime').val() != '' && $('#txtintodatetime').val() != '')
        Bind_Inspector();
}
function ToTimeChange() {
    var ftime = $('#txtintodatetime').val(); //alert(ftime);
    //var a = ftime.split(' ')[0];
    //var b = ftime.split(' ')[1];
    //var c = a + b;

    //$("#txtintodatetime").val('');
    //$("#txtinfrmdatetime").timepicker("option", "minTime", c);
    //alert($('#txtinfrmdate').val()); alert($('#txtintodate').val()); alert($('#txtinfrmdatetime').val()); alert($('#txtintodatetime').val());
    if ($('#txtinfrmdate').val() != '' && $('#txtintodate').val() != '' && $('#txtinfrmdatetime').val() != '' && $('#txtintodatetime').val() != '')
        Bind_Inspector();
}

function Bind_Inspector() {
    //var E = "{MunicipalityID: '" + municipalityid + "', FromDate: '" + $("#txtinfrmdate").val() + "', ToDate: '" + $("#txtintodate").val() + "'} ";

    var fd = $('#txtinfrmdate').val(); //alert(ftime);
    var ad = fd.split('/');
    var bd = ad[1] + "/" + ad[0] + "/" + ad[2] + " " + $('#txtinfrmdatetime').val();

    var td = $('#txtintodate').val(); //alert(ftime);
    var at = td.split('/');
    var bt = at[1] + "/" + at[0] + "/" + at[2] + " " + $('#txtintodatetime').val();

    var E = "{MunicipalityID: '" + municipalityid + "', FromDate: '" + bd + "', ToDate: '" + bt + "'} ";
    //var E = "{MunicipalityID: '" + municipalityid + "', FromDate: '" + $('#txtinfrmdate').val() + " " + $('#txtinfrmdatetime').val() + "', ToDate: '" + $('#txtintodate').val() + " " + $('#txtintodatetime').val() + "'} ";
    //var E = "{MunicipalityID: '" + municipalityid + "', FromDate: '" + "11/13/2020 6:00 AM" + "', ToDate: '" + "11/15/2020 6:10 AM" + "'} ";
    //alert(E);
    $.ajax({
        type: "POST",
        url: '/OtherServices/ApplicationForm/Bind_Inspector',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            $('#inspdet').empty();
            $('#inspdet').append('<option value="">Select</option');
            if (t != null) {
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('#inspdet').append('<option value=' + item.InspectorID + '>' + item.InspectorName + '</option>');
                    });
                }
            }
        }
    });
}

function Add() {
  
    if ($("#txtinfrmdate").val() == "") { $("#txtinfrmdate").focus(); return false; }
    if ($("#txtinfrmdatetime").val() == "") { $("#txtinfrmdatetime").focus(); return false; }

    if ($("#txtintodate").val() == "") { $("#txtintodate").focus(); return false; }
    if ($("#txtintodatetime").val() == "") { $("#txtintodatetime").focus(); return false; }

    if ($("#inspdet").val() == "") { $("#inspdet").focus(); return false; } ;
    //if ($("#notify").val() == "") { $("#notify").focus(); return false; }
   
    var html = ""
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"

        //+ "<td class='clsdetailid'>" + $("#txtinfrmdate").val() + "</td>"
        + "<td class='clsFromdt'>" + $('#txtinfrmdate').val() + " " + $('#txtinfrmdatetime').val() + "</td>"
        + "<td class='clsTodt'>" + $("#txtintodate").val() + " " + $('#txtintodatetime').val() + "</td>"

        + "<td style='display:none;' class='clsInsptId' chk-data='" + $("#inspdet").val() + "' >" + $("#inspdet").val() + "</td>"
        + "<td class='clsInsptName'>" + $("#inspdet option:selected").text() + "</td>"

        //+ "<td style='display:none;' class='clsWard'  >" + $("#notify").val() + "</td>"
        //+ "<td>" + ($("#notify").val() == "" ? "" : $("#notify option:selected").text()) + "</td>"

        + "<td style='text-align:center'>"
        + "<span class='glyphicon glyphicon-edit imgEdit' style='color:blue; font-size:15px; cursor:pointer; text-align:center;'  onClick='EditDetail(this);'></span>"
        + " &nbsp;<span class='glyphicon glyphicon-trash' class='clsDelButton imgDel' style='color:red; font-size:15px; cursor:pointer; text-align:center;' onClick='DeleteDetail(this, 0);'></span>"

        //+ "<img src= '/Contents/image/edit.png' title='Edit' class='imgEdit' style='width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'EditDetail(this);' />"
        //+ "</br><img src= '/Contents/image/delete.png' class='clsDelButton imgDel' title= 'Delete' style= 'width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'DeleteDetail(this, 0);' />"
        + "</td>" +
        html + "</tr>";

    
    $parentTR.after(html);

    $("#txtinfrmdate").val(''); $("#txtintodate").val(''); $("#txtinfrmdatetime").val(''); $("#txtintodatetime").val('');
    $("#inspdet").val(''); $("#notify").val('');
}

function Refresh() {
    location.reload();
}

function Save() {

    var ArrList = [];
    var remarks = $("#txtRemarks").val();

    var l = $('#tbl tr.myData').length; 
    if (l == 0) {
        alert('Please Add Inspector Schedule details.'); return false;
    }

    if (remarks == "") { $("#txtRemarks").focus(); return false; }

    $('#tbl tbody tr.myData').each(function () {

        var clsFromdt = $(this).find('.clsFromdt').text();
        var clsTodt = $(this).find('.clsTodt').text();

        var clsInsptId = $(this).find('.clsInsptId').text();

        ArrList.push({
            'ServMstId': ApplicationId, 
            'InspectionOnFrom': clsFromdt, 'InspectionOnTo': clsTodt,'InspectorID': clsInsptId
        });
    });

    //alert(JSON.stringify(ArrList));
    //return false;
    var E = "{ApplicationID: '" + ApplicationId + "' , municipalityID: '" + municipalityid + "', Remarks: '" + $('#txtRemarks').val() + "', ResponseDate: '" + $('#txtVerificationDate').val() + "', obj: " + JSON.stringify(ArrList) + "}";

    $.ajax({
        url: '/OtherServices/OtherServices/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            var MSGID = $.trim($(Table).find("MSGCODE").text());
            var MSG = $.trim($(Table).find("MSG").text());

            if (MSGID == 1) {
                alert(MSG);

                var href = $('#btnBack').attr('href');
                window.location.href = href;
            }
            else {
                alert('Sorry ! There is some Problem.');
                return false;
            }
        }
    });
}

function DeleteDetail(ID, SCHEDULEID) {
    var res = confirm('Are you sure want to delete ?');
    if (res == true) {
      
        if (SCHEDULEID > 0)
        {
            var E = "{SCHEDULEID: '" + SCHEDULEID + "'} ";
            $.ajax({
                type: "POST",
                url: '/OtherServices/OtherServices/DeleteInspector',
                contentType: "application/json; charset=utf-8",
                data: E,
                dataType: "json",
                success: function (data) {
                    var t = data.Data; //alert(JSON.stringify(t));

                    $(ID).closest('tr').remove();
                }
            });
        }
        else {
            $(ID).closest('tr').remove();
        }

    }
}

function EditDetail(ID) {

    var ft = $(ID).closest('tr').find('.clsFromdt').text();
    var tt = $(ID).closest('tr').find('.clsTodt').text();

    $("#txtinfrmdate").val(ft.split(' ')[0]);
    $("#txtintodate").val(tt.split(' ')[0]);

    $("#txtinfrmdatetime").val(ft.split(' ')[1] + " " + ft.split(' ')[2]); 
    $("#txtintodatetime").val(tt.split(' ')[1] + " " + tt.split(' ')[2]); 
   
    $("#inspdet").val($(ID).closest('tr').find('.clsInsptId').text()); 
    $(ID).closest('tr').remove();
}
