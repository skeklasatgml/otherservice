﻿var MutationType;
var IdProofType;
var Holdingtype;
var Block;
var Mauzaname;
var Devolvedtype;
var Roof;
var Flor;
$(document).ready(function () {
    
    MutationType = $('.hdnMutationType').val();
    IdProofType = $('.hdnproofType').val();
    Holdingtype = $('.hdnholdingtype').val();
    Block = $('.hdnblock').val();
    Mauzaname = $('.hdnmauzaname').val();
    Devolvedtype = $('.hdndevolvedtype').val();
    Roof = $('.hdnRoof').val();
    Flor = $('.hdnFlor').val();
    $('.ddlroof').val(Roof);
    $('.ddlFlor').val(Flor);

    GetMutationType();
    GetproofType();
    //Getholdingtype();
    //GetBlock();
    GetDevolvedType();
    AutoComplete_Municipality();
    $('input[type=radio][name=assessed]').change(function () {
        if (this.value == '1') {
            $('.isAssessedNoDiv').css('display', 'block');
        }
        else if (this.value == '0') {
            $('.isAssessedNoDiv').css('display', 'none');
        }
    });
    $('input[type=radio][name=Certificate]').change(function () {
        if (this.value == '1') {
            $('.CertificateYes').css('display', 'block');
        }
        else if (this.value == '0') {
            $('.CertificateYes').css('display', 'none');
        }
    });
    $('input[type=radio][name=isAssessee]').change(function () {
        if (this.value == '1') {
            $('.isAssesseeYesDiv').css('display', 'block');
            $('.isAssesseeNoDiv').css('display', 'none');
        }
        else if (this.value == '0') {
            $('.isAssesseeYesDiv').css('display', 'none');
            $('.isAssesseeNoDiv').css('display', 'block');
        }
    });
    $(".applicantName").keyup(function () {
        $(".applicantName").val($(".applicantName").val().toUpperCase());
    });
    $(document).on('click', '.btnSave', function (e) {
        e.preventDefault();
        // if (!validate_basic_detail()) { return; }
        //var TabId = $('.').val();
        var MutationTypeId = $('#ddlMutationType').val();
        var ApplicantsName = $('.applicantName').val();
        var ApplicantsFatherHusband = $('.fatherOrHusband').val();
        var AddressNo = $('.houseno').val();
        var AddressRdLn = $('.road').val();
        var AddressViTn = $('.Village').val();
        var AddressPs = $('.ps').val();
        var AddressPO = $('.postoffice').val();
        var AddressDt = $('.district').val();
        var AddressPin = $('.pin').val();
        var AddressSt = $('.State').val();
        //var AddressLandMark = $('.').val();
        var CommunicationDetailsPh = $('.phone').val();
        var CommunicationDetailsMn = $('.mobile').val();
        var CommunicationDetailsEmailIdn = $('.email').val();
        var IdProofType = $('#ddlproofType').val();
        var IdProofPath = document.getElementById("idproof").files[0];
        var WardNo = $('.hddn-Ward').val();
        var HoldingDtlsRoad = $('.roadname').val();
        var HoldingDtlsLoMo = $('.locality').val();
        var HoldingDtlsHoldingNo = $('.holdingno').val();
        var HoldingType = $('#holdingtype').val();
        var LandRecBlock = $('.ddl-block').val();
        var LandRecMouza = $('.mauzaname').val();
        var JlNo = $('.jlno').val();
        var PlotNoRS = $('.plotnoRS').val();
        var PlotNoLR = $('.plotnoLR').val();
        var KhatianNoRs = $('.KhatiannoRS').val();
        var KhatianNoLs = $('.KhatiannoLR').val();
        var QuanTumLandDc = $('.quantamoflandDeci').val();
        var QuanTumLandKt = $('.quantamoflandKhata').val();
        var LandTypeRor = $('.classificationofLandROR').val();
        var LandOwOcRor = $('.occupierNameAsPerROR').val();
        var RorPath = document.getElementById("fileROR").files[0];
        var ele = document.getElementsByName('assessed');
        var ass = 1;
        for (i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                ass = ele[i].value;
        }
        var Assessed = ass == 1 ? true : false; //change it
        var nameTaxPayer = $('.nameTaxPayer').val();
        var presentOwnerName = $('.presentOwnerName').val();
        var presentOwnerAddress = $('.presentOwnerAddress').val();
        var lastQuaterPaymentReceipt = $('.lastQuaterPaymentReceipt').val();
        var NameTrandVend = $('.NameTrandVend').val();
        var ele1 = document.getElementsByName('isAssessee');
        var ass1 = 1;
        for (i = 0; i < ele1.length; i++) {
            if (ele1[i].checked)
                ass1 = ele1[i].value;
        }
        var TrandVendAssessee = ass1 == 1 ? true : false; //change it
        var TrandVendTaxPaidUpto = $('.uptoWhichTaxPaid').val();
        var TrandVendNoRemarks = $('.isAssesseeNoRemarks').val();
        var ele2 = document.getElementsByName('deedType');
        var ass2 = 1;
        for (i = 0; i < ele2.length; i++) {
            if (ele2[i].checked)
                ass2 = ele2[i].value;
        }
        var DeedTypeId = ass2 == 1 ? true : false; //change it
        var DeedTypeNo = $('.deedNo').val();
        var DeedTypeDate = $('.deedRegDt').val();
        var DeedTypeRegOff = $('.deedRegOffice').val();
        var DeedTypeVal = $('.deedRegAmt').val();
        var DeedPath = document.getElementById("deedCopy").files[0];
        var DevolvedType = $('.devolvedtype').val();
        var DevolvedPath = document.getElementById("devolvedFile").files[0];;
        var AreaDtlsTla = $('.Totallandarea').val();
        var AreaDtlsTvla = $('.Totalvacantlandarea').val();
        var AreaDtlsTp = $('.tankpond').val();
        var AreaDtlsLabf = $('.areabuilding').val();
        var AreaDtlsFa = $('.areaflat').val();
        var parkingspace = $('.parkingspace').val();
        var exclusivegarden = $('.exclusivegarden').val();
        var RoofTypeId = $('.ddlroof').val();
        var FloorTypeId = $('.ddlFlor').val();
        var ele3 = document.getElementsByName('Certificate');
        var ass3 = 1;
        for (i = 0; i < ele3.length; i++) {
            if (ele3[i].checked)
                ass3 = ele3[i].value;
        }
        var CompleCert = ass3 == 1 ? true : false;
        var CompleCertNo = $('.Certificatenumber').val();
        var CompleCertDt = $('.CertificateDate').val();
        var AppliSigPath = document.getElementById("SignatureFile").files[0];

        var formData = new FormData();
        formData.append("MutationTypeId", MutationTypeId);
        formData.append("ApplicantsName", ApplicantsName);
        formData.append("ApplicantsFatherHusband", ApplicantsFatherHusband);
        formData.append("AddressNo", AddressNo);
        formData.append("AddressRdLn", AddressRdLn);
        formData.append("AddressViTn", AddressViTn);
        formData.append("AddressPs", AddressPs);
        formData.append("AddressPO", AddressPO);
        formData.append("AddressDt", AddressDt);
        formData.append("AddressPin", AddressPin);
        formData.append("AddressSt", AddressSt);
        //formData.append("AddressLandMark", AddressLandMark);
        formData.append("CommunicationDetailsPh", CommunicationDetailsPh);
        formData.append("CommunicationDetailsMn", CommunicationDetailsMn);
        formData.append("CommunicationDetailsEmailIdn", CommunicationDetailsEmailIdn);
        formData.append("IdProofType", IdProofType);
        formData.append("IdProofData", IdProofPath);
        formData.append("WardNo", WardNo);
        formData.append("HoldingDtlsRoad", HoldingDtlsRoad);
        formData.append("HoldingDtlsLoMo", HoldingDtlsLoMo);
        formData.append("HoldingDtlsHoldingNo", HoldingDtlsHoldingNo);
        formData.append("HoldingType", HoldingType);
        formData.append("LandRecBlock", LandRecBlock);
        formData.append("LandRecMouza", LandRecMouza);
        formData.append("JlNo", JlNo);
        formData.append("PlotNoRS", PlotNoRS);
        formData.append("PlotNoLR", PlotNoLR);
        formData.append("KhatianNoRs", KhatianNoRs);
        formData.append("KhatianNoLs", KhatianNoLs);
        formData.append("QuanTumLandDc", QuanTumLandDc);
        formData.append("QuanTumLandKt", QuanTumLandKt);
        formData.append("LandTypeRor", LandTypeRor);
        formData.append("LandOwOcRor", LandOwOcRor);
        formData.append("RorData", RorPath);
        formData.append("Assessed", Assessed);
        formData.append("nameTaxPayer", nameTaxPayer);
        formData.append("NameTrandVend", NameTrandVend);
        formData.append("presentOwnerName", presentOwnerName);
        formData.append("presentOwnerAddress", presentOwnerAddress);
        formData.append("lastQuaterPaymentReceipt", lastQuaterPaymentReceipt);
        formData.append("TrandVendAssessee", TrandVendAssessee);
        formData.append("TrandVendTaxPaidUpto", TrandVendTaxPaidUpto);
        formData.append("TrandVendNoRemarks", TrandVendNoRemarks);
        formData.append("DeedTypeId", DeedTypeId);
        formData.append("DeedTypeNo", DeedTypeNo);
        formData.append("DeedTypeDate", DeedTypeDate);
        formData.append("DeedTypeRegOff", DeedTypeRegOff);
        formData.append("DeedTypeVal", DeedTypeVal);
        formData.append("DeedData", DeedPath);
        formData.append("DevolvedType", DevolvedType);
        formData.append("DevolvedData", DevolvedPath);
        formData.append("AreaDtlsTla", AreaDtlsTla);
        formData.append("AreaDtlsTvla", AreaDtlsTvla);
        formData.append("AreaDtlsTp", AreaDtlsTp);
        formData.append("AreaDtlsLabf", AreaDtlsLabf);
        formData.append("AreaDtlsFa", AreaDtlsFa);
        formData.append("parkingspace", parkingspace);
        formData.append("exclusivegarden", exclusivegarden);
        formData.append("RoofTypeId", RoofTypeId);
        formData.append("FloorTypeId", FloorTypeId);
        formData.append("CompleCert", CompleCert);
        formData.append("CompleCertNo", CompleCertNo);
        formData.append("CompleCertDt", CompleCertDt);
        formData.append("AppliSigData", AppliSigPath);
        if (confirm('Are you sure to Save', function () { }) == true) {
            $.ajax({
                type: "POST",
                contentType: false,
                processData: false,
                url: "/Municipality/Assessment/SaveMutationForm",
                data: formData,
                success: function (D) {
                    if (D.Status === 200) {
                        alert("Successful! " + D.Message, function () {});
                        location.reload(true);
                        //window.location.href = '/Municipality/Assessment/MutationList';
                    } else {
                        alert("Error! " + D.Message, function () { });
                    }
                }, error: function (err) {
                    alert("Error: Failed to save data.");
                }
            });
        }
    });
    $(document).on('focus', ".wardno", function () {
        $(this).autocomplete({
            source: function (request, response) {
                var data = { Ward: request.term };
                $(".hddn-Ward").val('');
                if (!request.term) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/GetWards",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            var serverResponse = Response.Data;
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        value: item.WardID,
                                        label: item.WardName
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        } else {
                            alertify.error("Error: " + Response.Message);
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            select: function (e, i) {
                $(".hddn-Ward").val(i.item.value);
            },
            change: function (e, i) {
                if (!i.item) {
                    //Resetors.ResetAssesse();
                }
            }
        });
    });
    $(document).on('focus', ".roadname", function () {
        var ctrl = $(this);
        $(this).autocomplete({
            source: function (request, response) {
                var data = { WardId: $(".hddn-Ward").val(), Location: request.term };
                $(".hddn-Location").val('');
                if (!request.term || !$(".hddn-Ward").val()) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/GetLocations",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            var serverResponse = Response.Data;
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        value: item.LocationID,
                                        label: item.LocationName
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        } else {
                            alertify.error("Error: " + Response.Message);
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            select: function (e, i) {
                $(".hddn-Location").val(i.item.value);
                ctrl.val(i.item.label);
                return false;
            },
            change: function (e, i) {
                if (!i.item) {
                    //Resetors.ResetAssesse();
                }
            }
        });
    });
    $(document).on('focus', ".holdingno", function () {
        $(this).autocomplete({
            source: function (request, response) {
                var data = { WardId: $(".hddn-Ward").val(), LocationID: $(".hddn-Location").val(), Holding: request.term };
                $(".hddn-holding-no").val('');
                if (!request.term || !$(".hddn-Ward").val() || !$(".hddn-Location").val()) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/GetHoldings",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            var serverResponse = Response.Data;
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        value: item.HoldingNo,
                                        label: item.HoldingNo + " (" + item.AssesseeName + ")"
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        } else {
                            alertify.error("Error: " + Response.Message);
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            select: function (e, i) {
                //$(".hddn-Location").val(i.item.value);
                $(".hddn-holding-no").val(i.item.value);
            },
            change: function (e, i) {
                if (!i.item) {
                    //Resetors.ResetAssesse();
                }
            }
        });
    });
    $(document).on('change', '.ddl-block', function () {
        $(".mauzaname").not(':eq(0)').remove();
        if ($(this).val() == '') {
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetMouzas",
            data: JSON.stringify({ BlockCode: $(this).val() }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                if (r.Status == 200) {

                    $.each(r.Data, function (index, value) {
                        $(".mauzaname").append('<option value="' + value.MouzaCode + '">' + value.MouzaName + '</option>')
                    });
                }
            },
            failure: function (response) {
                alert(response);
            }
        });
    });

    function GetMutationType() {
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetMutationType",
            dataType: "json",
            //data: JSON.stringify({ IsToday: true }),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status === 200) {
                    var t = "";
                    t = '<option value="">--Select--</option>';
                    $.each(D.Data, function (index, value) {
                        t = t + '<option value="' + value.MutationTypeId + '">' + value.MutationTypeName + '</option>'
                    })
                    $('#ddlMutationType').append(t);
                    $('#ddlMutationType').val(MutationType);
                } else {
                    alert("Error", D.Message, "error");
                }
            }, error: function (err) {
                alert("Error", "Failed to save data.", "error");
            }
        });
    }
    function GetproofType() {
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetIdProofType",
            dataType: "json",
            //data: JSON.stringify({ IsToday: true }),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status === 200) {
                    var t = "";
                    t = '<option value="">--Select--</option>';
                    $.each(D.Data, function (index, value) {
                        t = t + '<option value="' + value.IdProofTypeId + '">' + value.IdProofTypeName + '</option>'
                    })
                    $('#ddlproofType').append(t);
                    $('#ddlproofType').val(IdProofType);
                } else {
                    alert("Error", D.Message, "error");
                }
            }, error: function (err) {
                alert("Error", "Failed to save data.", "error");
            }
        });
    }
    function Getholdingtype(MunicipalityID) {
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetHoldingTypes",
            dataType: "json",
            data: JSON.stringify({ HoldingGroupTypeId: null, MunicipalityID: MunicipalityID }),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status === 200) {
                    var t = "";
                    t = '<option value="">--Select--</option>';
                    $.each(D.Data, function (index, value) {
                        t = t + '<option value="' + value.HoldingTypeId + '">' + value.HoldingTypeDesc + '</option>'
                    })
                    $('#holdingtype').append(t);
                    $('#holdingtype').val(Holdingtype);
                } else {
                    alert("Error", D.Message, "error");
                }
            }, error: function (err) {
                alert("Error", "Failed to save data.", "error");
            }
        });
    }
    function GetBlock(MunicipalityID) {
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetBllock",
            dataType: "json",
            data: JSON.stringify({ BlockId: null, MunicipalityID: MunicipalityID }),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status === 200) {
                    var t = "";
                    t = '<option value="">--Select--</option>';
                    $.each(D.Data, function (index, value) {
                        t = t + '<option value="' + value.BlockCode + '">' + value.BlockName + '</option>'
                    })
                    $('.ddl-block').append(t);
                    $('.ddl-block').val(Block);
                } else {
                    alert("Error", D.Message, "error");
                }
            }, error: function (err) {
                alert("Error", "Failed to save data.", "error");
            }
        });
    }
    function GetDevolvedType() {
        $.ajax({
            type: "POST",
            url: "/Municipality/Assessment/GetDevolvedType",
            dataType: "json",
            //data: JSON.stringify({ IsToday: true }),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                if (D.Status === 200) {
                    var t = "";
                    t = '<option value="">--Select--</option>';
                    $.each(D.Data, function (index, value) {
                        t = t + '<option value="' + value.DevolvedTypeId + '">' + value.DevolvedTypeName + '</option>'
                    })
                    $('.devolvedtype').append(t);
                    $('.devolvedtype').val(Devolvedtype);
                } else {
                    alert("Error", D.Message, "error");
                }
            }, error: function (err) {
                alert("Error", "Failed to save data.", "error");
            }
        });
    }

    function AutoComplete_Municipality() {
        $('#ddlMunicipality').autocomplete({
            source: function (request, response) {

                var GLS = "S";

                var S = "{Desc:'" + $('#ddlMunicipality').val() + "'}"; //alert(S);
                $.ajax({
                    url: '/OtherServices/ApplicationForm/AutoComplete_Municipality',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = []; //alert(JSON.stringify(serverResponse.Data));

                        if (serverResponse.Data == null) {
                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });

                            response(AutoComplete);
                        }
                        else {
                            if ((serverResponse.Data).length > 0) {

                                $.each(serverResponse.Data, function (index, item) {
                                    AutoComplete.push({
                                        label: item.MunicipalityName,
                                        MunicipalityID: item.MunicipalityID
                                    });
                                });

                                response(AutoComplete);
                            }
                        }

                    }
                });
            },
            select: function (e, i) {
                $('.clsVariables').attr('automunicipalityid', i.item.MunicipalityID);
                CallfunctionRelatedtoMuniID(i.item.MunicipalityID);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

        $('#ddlMunicipality').keydown(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 8) {
                $("#ddlMunicipality").val('');
                $('.clsVariables').attr('automunicipalityid', '');
            }
            if (iKeyCode == 46) {
                $("#ddlMunicipality").val('');
                $('.clsVariables').attr('automunicipalityid', '');
            }
        });
    }
    function CallfunctionRelatedtoMuniID(MunicipalityID) {
        Getholdingtype(MunicipalityID);
        GetBlock(MunicipalityID);
    }
});
var validate_basic_detail = function () {
    var vldObj = {
        rules: {},
        messages: {},
        errorPlacement: {}
    };
    vldObj.ignore = [];
    vldObj.rules[$('.Title ').prop('name')] = { required: true };
    vldObj.rules[$('.CourseDuration').prop('name')] = { required: true };
    vldObj.rules[$('.Scope').prop('name')] = { required: true };
    vldObj.rules[$('.WhatWillLearn').prop('name')] = { required: true };
    if ($('.custom-file-label').attr('value') == "0") {
        vldObj.rules[$('.custom-file-input').prop('name')] = { required: true };
    }

    vldObj.messages[$(".Title ").prop('name')] = { required: "Please Enter Course Title" };
    vldObj.messages[$(".Scope ").prop('name')] = { required: "Please Enter Course Scope" };
    vldObj.messages[$(".WhatWillLearn ").prop('name')] = { required: "Please Enter What Will Learn" };
    vldObj.messages[$(".CourseDuration").prop('name')] = { required: "Please Enter Course Duration" };
    if ($('.custom-file-label').attr('value') == "0") {
        vldObj.messages[$(".custom-file-input").prop('name')] = { required: "Please upload Course Image" };
    }

    vldObj.errorPlacement = function (error, element) {
        if ($(element).parent(".input-group").length > 0) {
            error.insertAfter($(element).parent(".input-group"));
            error.css('color', 'red');
        } else {
            error.insertAfter(element);
            error.css('color', 'red');
        }
    };
    $('.basic-details-Form').validate(vldObj);
    return $('.basic-details-Form').valid();
};


