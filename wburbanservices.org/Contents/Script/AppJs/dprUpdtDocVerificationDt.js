﻿$(document).ready(function () {
    var Server = function () {
        this.GetDprList = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/GetDprListByDocVrifyDtOrMemoNo',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
    };
    var App = function (srv) {
        var convertToDate = function (strDate) {
            try {
                if (!strDate)
                    return null;
                var arr = strDate.split('/');
                return new Date(arr[2], arr[1] - 1, arr[0]);
            } catch (e) {
                alert('Invalid date format')
            }
        }

        var doms = function () {
            return {
                btnShowAll: $(".btn-show-all"),
                btnShowByDate: $(".btn-search-by-date"),
                txtFromDate: $(".txt-from-date"),
                txtToDate: $(".txt-to-date"),
                txtVerifyDate: $(".txt-verify-date"),
                txtMemoNo: $(".memo_no"),
                frm: $("form"),
                table: $(".tbl-dpr-list")
            }
        };
        var dataTable = function () {
            return {
                configure: function () {
                    doms().table.DataTable({
                        columns: [
                            { data: "sl" },
                            { data: "DevAuth" },
                            { data: "ProjectName" },
                            { data: "EstimatedAmt" },
                            { data: "MemoNumber" },
                            { data: "MemoDate" },
                            {
                                data: null,
                                className: "center",
                                render: function (data, type, row) {
                                    var str2 = "<div class='input-group'>";
                                    str2 += "<input type='text' class='form-control txt-verify-date' name='txt-verify-date' readonly value='" + row.VerificationDate + "'/>";
                                    str2 += "<a href='javascript:void(0)' class='input-group-addon remove-date' title='clear input' data-clear-tergate='txt-verify-date'>";
                                    str2 += "<span class='glyphicon glyphicon-remove'></span>";
                                    str2 += "</a>";
                                    str2 += "</div>";
                                    return str2;
                                }
                            },
                        {
                            data: null,
                            className: "center",
                            render: function (data, type, row) {
                                var str2 = "";
                                str2 += "<a class='btn-update-ver-date btn btn-primary' data-mst-dpr-id='" + row.MstDprId + "'> update</a>";
                                return str2;
                            }
                        }
                        ]
                    });
                },
                loadData: function (fromdate, todate, memono) {
                    var data = { dateFrom: fromdate, dateTo: todate, memoNo: memono };
                    srv.GetDprList(data, function (e) {
                        if (e.Status === 200) {
                            var datatable = doms().table.DataTable();
                            datatable.clear();
                            datatable.rows.add(e.Data.map(function (val, ind) {
                                return {
                                    sl: ++ind,
                                    MstDprId: val.DPChkSlId,
                                    DevAuth: val.MunicipalityDevAuthoName,
                                    ProjectName: val.NameOfWork,
                                    EstimatedAmt: val.EstimatedCost,
                                    MemoNumber: val.MemoNumber,
                                    MemoDate: val.MemoDate ? CONVERTER.Date.convertCsToJsDate(val.MemoDate).format('dd/mm/yyyy') : '',
                                    VerificationDate: val.DocVerificationDate ? CONVERTER.Date.convertCsToJsDate(val.DocVerificationDate).format('dd/mm/yyyy') : ''
                                }
                            }));
                            datatable.draw();
                            $(document).on('focus', '.txt-verify-date', function () {
                                $(this).datepicker({ "dateFormat": "dd/mm/yy", 'minDate': new Date() });
                            });

                        } else {
                            alertify.error(e.Message);
                        }
                    });
                }
            }
        }
        this.Init = function () {
            bindEnvents();
            registerValidator();
            dataTable().configure();
            dataTable().loadData(null, null, null);
        };
        var bindEnvents = function () {
            doms().txtFromDate.datepicker({
                "dateFormat": 'dd/mm/yy',
                onSelect: function (selectedDate) {
                    doms().txtToDate.datepicker("option", "minDate", selectedDate);
                    doms().txtToDate.datepicker("option", "beforeShowDay", function (date) {
                        return [true, ''];
                    });
                }
            });

            doms().txtToDate.datepicker({
                "dateFormat": 'dd/mm/yy', beforeShowDay: function (date) {
                    return [false, ''];
                }
            });
            doms().btnShowAll.on('click', _eventHandlers._onClickShowAll);
            doms().btnShowByDate.on('click', _eventHandlers._onClickShowByDate);
            $(document).on('click', ".remove-date", function () {
                var clearTergate = $(this).attr("data-clear-tergate");
                $("." + clearTergate).val('');
                //$(this).siblings('input[type=text]').val('');
                if (clearTergate == "txt-from-date") {
                    doms().txtToDate.datepicker("option", "beforeShowDay", function (date) {
                        return [false, ''];
                    });
                }
            })
            $(document).on('click', '.btn-update-ver-date', function () {
                var date = $(this).closest('tr').find(".txt-verify-date").val();
                var data = { mstDprId: $(this).attr('data-mst-dpr-id'), uvDate: convertToDate(date) };
                $.ajax({
                    type: 'post',
                    url: '/Modules/dpr/UpdateVerifyDate',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert(response.Message);
                    },
                    error: function () {
                        alert(response.Message);
                    }
                });
            })
        };
        var _eventHandlers = {
            _onClickShowAll: function (e) {
                e.preventDefault();
                dataTable().loadData(null, null, null);
            },
            _onClickShowByDate: function (e) {
                e.preventDefault();
                //if (doms().frm.valid() || (doms().txtMemoNo.val().length>0)) {
                dataTable().loadData(convertToDate(doms().txtFromDate.val()), convertToDate(doms().txtToDate.val()), doms().txtMemoNo.val());
                // }
            }
        };
        var registerValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    var inputGroup = element.parent(".input-group");
                    if (inputGroup.length > 0) {
                        error.insertAfter(inputGroup);
                    } else {
                        error.insertAfter(element);
                    }
                    error.css('color', 'red');
                }
            }
            vld.rules[doms().txtFromDate.attr('name')] = { required: true };
            vld.rules[doms().txtToDate.attr('name')] = { required: true };
            vld.rules[doms().txtMemoNo.attr('name')] = { required: true };
            doms().frm.validate(vld);
        }
    };
    (function () {
        var srv = new Server();
        var app = new App(srv);
        app.Init();
    }())
});
