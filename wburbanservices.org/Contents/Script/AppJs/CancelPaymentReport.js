﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetLocation = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    alert("Error");
                }
            });
        };

        this.GetWard = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };

        this.GetCounter = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnCounter/GetActiveCountersForReports",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function () {
                    alert("Error");
                }
            });
        };

        this.ShowReport = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnReports/CancelPaymentReport",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function () {
                    alert("Error");
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var BindEvent = function () {
            $("#btnSave").on('click', ReportShow);          
            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            PopulateCounter();
            $('#divCounter').on('change', '#chkAll', function () {
                $("#tabCounter tbody tr").find(".select-counter").prop('checked', $("#chkAll").prop("checked"));
            });
        };
        var ReportShow = function () {
            var data = GetDataFromView();
            if (ValidateData(data)) {
                $('#frmCancelPaymentReport').submit();
                //serverObject.ShowReport(data, function (result) {
                //    if (respons.Status == 200) { alert("Show Report") }
                //});
            }
        };
        var GetDataFromView = function () {
            GetCounterData();
            var DateFrom = $("#DateFrom").val();
            var DateTo = $("#DateTo").val();
            var WardID = $("#WardID").val();
            var LocationID = $("#LocationID").val();
            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }
            var data = { DateFrom: DateFrom, DateTo: DateTo, WardID: WardID, LocationID: LocationID, Counter: $("#hdnCounter").val() };
            return data;
        };
        var GetCounterData = function () {
            var counterID = "";
            $.each($(".counter-list ").find("table tbody").find(".select-counter:checked"), function (idx, counter) {
                counterID = counterID+$(counter).val() + ",";
            });
            counterID = counterID.substring(1-counterID.length, counterID.length-1);
            $("#hdnCounter").val(counterID);
        };
        var ValidateData = function (data) {
            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            if ($("#hdnCounter").val().length == 0) {
                alert("Select atleast one Counter");
                return false;
            }
            return true;
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        ResetValues.ResetLocation();
                        //Resetors.ResetAssessee();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWard(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.Status == 200 && (serverResponse.Data).length > 0) {
                                $.each(serverResponse.Data, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnWard.val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            ResetValues.ResetLocation();
                            //Resetors.ResetAssessee();
                        }
                    }
                };
            },
            Locations: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocation(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.Status == 200 && (serverResponse.Data).length > 0) {
                                $.each(serverResponse.Data, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    }
                };
            }
        };
        var DomControls = {
            ctrl_hdnWard: $("#WardID"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#LocationID"),
            ctrl_txtLocation: $("#txtLocation")
        };
        var ResetValues = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            }
        };
        var PopulateCounter = function () {
            $(".counter-list").find("table tbody").empty();
            serverObject.GetCounter(null, function (response) {
                if (response.Status == 200 && (response.Data).length > 0) {
                    $.each(response.Data, function (inx, item) {
                        var str = "<tr>";
                        str += "<td><input type='checkbox' class='select-counter' value='" + item.CounterID + "'/></td><td><label>&nbsp;&nbsp;" + item.CounterCode + "</label></td>";
                        str += "</tr>";
                        $(".counter-list").find("table tbody").append(str);
                    });
                }
            });
        };
        this.app_initiate = function () {
            BindEvent();
        };
    };
    var INIT = function () {
        var serverObj = new SERVER_OBJECT();
        var appObj = new APP_OBJECT(serverObj);
        appObj.app_initiate();
    };
    (function () {
        INIT();
    })();
});