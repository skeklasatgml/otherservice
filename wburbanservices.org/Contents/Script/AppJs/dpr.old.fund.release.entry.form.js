﻿$(document).ready(function () {
    var server = function () {
        this.UpdateInstallmentData = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/UpdateOldFundRelease',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
    };
    var ApplyOrUpdateInstallMent = function ($rmt) {
        var boxInstallment = $(".box-installment")
        var getFormData = function () {
            return {
                MstDprId: $('.dpr-id').val().toNumber(),
                FundReleaseId: boxInstallment.find('.fundrelease-id').val().toNumber(),
                MemoNo: boxInstallment.find(".memo-no").val(),
                MemoDate: boxInstallment.find(".memo-date").val().toDate("dd/mm/yyyy"),
                InstallmentAmount: boxInstallment.find(".inst-amount").val().isNanThenZero().toNumber(),
                StartDate: boxInstallment.find(".start-date").val().toDate('dd/mm/yyyy'),
                EndDate: boxInstallment.find('.end-date').val().toDate('dd/mm/yyyy'),
                ApplyCheckList: boxInstallment.find(".inst-check-list").map(function (i, v) {
                    return { IsChecked: $(v).prop('checked'), ChkId: $(v).val() }
                }).get(),
                ApprovedAmount: boxInstallment.find('.approved-amount').val().toNumber(),
                ApprovalDate: window.toDate(boxInstallment.find(".approval-date").val()),
                UoNo: boxInstallment.find(".uo-no").val(),
                UoDate: boxInstallment.find(".uo-date").getDate(),
                SanctionMemoNo: boxInstallment.find(".sanctioned-memo-no").val(),
                VerificationDate: window.toDate(boxInstallment.find(".verification-date").val()),
                AccountHeadNo: boxInstallment.find(".account-head-no").val()
            }
        };
        var doms = function (scope) {
            return {
                controls: {
                    MstDprId: scope.find('.dpr-id'),
                    InstallmentId: scope.find('.fundrelease-id'),
                    MemoNo: scope.find(".memo-no"),
                    MemoDate: scope.find(".memo-date"),
                    InstallmentAmount: scope.find(".inst-amount"),
                    StartDate: scope.find(".start-date"),
                    EndDate: scope.find('.end-date'),
                    ApplyCheckList: scope.find(".inst-check-list"),
                    ApprovedAmount: scope.find('.approved-amount'),
                    ApprovalDate: scope.find(".ApprovalDate"),
                    UoNo: scope.find(".uo-no"),
                    UoDate: scope.find(".uo-date"),
                    SanctionMemoNo: scope.find(".sanctioned-memo-no"),
                    VerificationDate: scope.find(".verification-date"),
                    AccountHeadNo: scope.find(".account-head-no"),
                    Status: scope.find(".status")
                }
            };
        };
        var validate = function () {
            var getVal = function (param) {
                if (param) {
                    var _param;
                    if (typeof param === "function") {
                        _param = param();
                    } else {
                        _param = param;
                    }
                    return _param;
                }
            };
            $.validator.addMethod('greaterThan', function (value, el, param) {
                if (getVal(param.apply) == false)
                    return true;
                return value > param.value;
            }, function (param, control) {
                return "Please enter a value greater than " + param.value;
                });
            $.validator.addMethod('greaterThanEqual', function (value, el, param) {
                if (getVal(param.apply) == false)
                    return true;

                 return value >= param.value;
            }, function (param, control) {
                return "Please enter a value greater than or equal to " + param.value;
            });
            $.validator.addMethod('lessThanEqual', function (value, el, param) {
                if (getVal(param.apply) == false)
                    return true;
                return value <= param.value;
            }, function (param, control) {
                return "Please enter a value less than " + param.value;
            });
            $.validator.addMethod('mobile', function (value, el, param) {
                if (getVal(param.apply) === false)
                    return true;
                var r = /^(\+\d{1,3}[- ]?)?\d{10}$/;
                return r.test(value);
            });

            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    if (element.siblings(".input-group-addon").length > 0) {
                        error.insertAfter(element.parent());
                        error.css('color', 'red');
                        return;
                    }
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            };
            
            var _form = doms(boxInstallment);
            var status =  _form.controls.Status.val();
            vld.rules[_form.controls.MemoNo.name()] = { required: true };
            vld.rules[_form.controls.InstallmentAmount.name()] = {
                required: false, greaterThan: { value: 0 }, number: true
            };
            vld.rules[_form.controls.MemoDate.name()] = { required: true };
            vld.rules[_form.controls.StartDate.name()] = { required: true };
            vld.rules[_form.controls.EndDate.name()] = { required: true };
            vld.rules[_form.controls.ApprovedAmount.name()] = {
                required: function () {
                    return status === "A";
                },
                greaterThan: {
                    value: 0,
                    apply: function () { return status == "A"; }
                }, number: function () { return status == "A"; }
            };
            vld.rules[_form.controls.ApprovalDate.name()] = {
                required: function () {
                    return status === "A";
                }
            };
            vld.rules[_form.controls.VerificationDate.name()] = {
                required: function () {
                    return (status === "V" || status === "A");
                }
            };
            $('form').validate(vld);
            return $('form').valid();
        };
        boxInstallment.on('click', ".btn-save-installment", function (e) {
            e.preventDefault();
             validate();
            if (!validate())
                return;
            var formData = getFormData();
            if (formData.ApplyCheckList.filter(t => t.IsChecked === true).length == 0) {
                alertify.error("Please select check list");
                return;
            }
            alertify.confirm('Comfime us!', "Are you sure to update?", function () {
                $rmt.UpdateInstallmentData(formData, function (r) {
                    if (r.Status === 200) {
                        alertify.success(r.Message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000)
                    } else {
                        alertify.error(r.Message);
                    }
                });
            }, function () { });
        });
        
        boxInstallment.on("click", ".clear-data", function () {
            var tergate = $(this).siblings("." + $(this).attr('data-clean-ctrl'));
            if (tergate.length > 0) {
                tergate.val('');
            }
        });
        (function () {
            $(".datepicker").connectDatePicker();
        }());
    };
    (function () {
        var $rmt = new server();
        var $app = new ApplyOrUpdateInstallMent($rmt);
    }())
});