﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {

        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        this.ViewAdvanceAdjustmentData = function (EffectFromDT, EffectToDT, WardID, LocationID, AssesseeID, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/AdvanceAdjustment/GetAdvanceData",
                data: JSON.stringify({ EffectFromDT: EffectFromDT, EffectToDT: EffectToDT, WardID: WardID, LocationID: LocationID, AssesseeID:AssesseeID}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.ViewAdvAdjDtlsData = function (AdvPaymentID, MunicipalityID, AssesseeID, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/AdvanceAdjustment/GetAdvAdjDtlsData",
                data: JSON.stringify({ AdvPaymentID: AdvPaymentID, MunicipalityID: MunicipalityID, AssesseeID: AssesseeID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.AdjustUnadjustedAdvance = function (AdvPaymentID, MunicipalityID, AssesseeID, ReceiptNo, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/AdvanceAdjustment/AdjustUnadjustedAdvance",
                data: JSON.stringify({ AdvPaymentID: AdvPaymentID, MunicipalityID: MunicipalityID, AssesseeID: AssesseeID, ReceiptNo: ReceiptNo }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        
    }

    var APP_OBJECT = function (serverObject) {
        var modelData = [];

        var BindEvent = function () {    
            $('#txtWard').autocomplete(AutocompleteSource.Wards());
            $('#txtLocation').autocomplete(AutocompleteSource.Locations());
            $('#txtAssessee').autocomplete(AutocompleteSource.Assessees());

            $("#divAllAssessment").hide();
            $("#btnView").on('click', OnViewAdvanceAdjustmentAll);

            var dateFormat = "dd/mm/yy",
                from = $("#dtFrom")
                    .datepicker({
                        //defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat: "dd/mm/yy"
                    })
                    .on("change", function () {
                        to.datepicker("option", "minDate", getDate(this));
                    }),
                to = $("#dtTo").datepicker({
                    //defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: "dd/mm/yy"
                })
                    .on("change", function () {
                        from.datepicker("option", "maxDate", getDate(this));
                    });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        }

        var From_ValidateForAdjustAll = function () {
            $("#frmAdjAll").validate({
                rules: {
                    dtFrom: {
                        required: true
                    },
                    dtTo: {
                        required: true
                    },
                    ddlStatus: {
                        required: true
                    }
                },
                messages: {
                    dtFrom: {
                        required: "Please select Date From."
                    },
                    dtTo: {
                        required: "Please select Date To."
                    },
                    ddlStatus: {
                        required: "Please select a Status."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                }
            });
            return $('#frmAdjAll').valid();
        }


        var uuidv4 = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        var RegistertabAllAdvAdjEvent = function () {
            $("#tabAllAssessment tbody").off('click', '.btnAdvDtls', onEditRow);
            $("#tabAllAssessment tbody").on('click', '.btnAdvDtls', onEditRow);

            $("#tabAllAssessment tbody").off('click', '.btnAdjust', onAdjust);
            $("#tabAllAssessment tbody").on('click', '.btnAdjust', onAdjust);
        };
    
        var onEditRow = function () {
            var tabRowData = [];            

            var CurrentTR = $(this).closest('tr');
            var RowUnqID = $(CurrentTR).find("td:eq(7)").find('.btnAdvDtls').attr('data-id');
            tabRowData = alasql('SELECT * FROM ?  where UnqID="' + RowUnqID + '"', [modelData]);

            if (tabRowData.length = 1) {
               var AdvPaymentID = tabRowData[0].AdvPaymentID;
               var AssesseeID = tabRowData[0].AssesseeID;
               var MunicipalityID = tabRowData[0].MunicipalityID;

               serverObject.ViewAdvAdjDtlsData(AdvPaymentID, MunicipalityID, AssesseeID, function (response) {
                   var status = response.Status;
                   if (status == 200) {                       
                       if (response.Data.length > 0) {
                           
                           PopulateAdvAdjTable(response.Data);
                       }
                       else {
                           alert("No Record found!!!");
                       }
                   } else {
                       alert(response.Message);
                   }
               });
            } else {
                alert("No record found!!!");
            }            
        }

        var onAdjust = function () {
            var tabRowDataAdjust = [];

            var CurrentTR = $(this).closest('tr');
            var RowUnqID = $(CurrentTR).find("td:eq(8)").find('.btnAdjust').attr('data-id');
            tabRowDataAdjust = alasql('SELECT * FROM ?  where UnqID="' + RowUnqID + '"', [modelData]);

            if (tabRowDataAdjust.length = 1) {
                var AdvPaymentID = tabRowDataAdjust[0].AdvPaymentID;
                var AssesseeID = tabRowDataAdjust[0].AssesseeID;
                var MunicipalityID = tabRowDataAdjust[0].MunicipalityID;
                var ReceiptNo = tabRowDataAdjust[0].ReceiptNo;

                if (confirm("Do you want to Adjust the Unadjusted Amount") == true) {
                    if (confirm("Yes/No") == true) {
                        serverObject.AdjustUnadjustedAdvance(AdvPaymentID, MunicipalityID, AssesseeID, ReceiptNo, function (response) {
                            var status = response.Status;
                            if (status == 200) {
                                alert(response.Message);
                                OnViewAdvanceAdjustmentAll();
                            } else {
                                alert(response.Message);
                            }
                        });
                    }
                }
                
            } else {
                alert("No record found!!!");
            }
        }

        var PopulateAdvAdjTable = function (responseData) {

            var tabAdvAdjDtls = $("#tabAdvAdjDtls");
            tabAdvAdjDtls.find("tbody").empty();
            //console.log(responseData);
            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {
                    var rowstr = "<tr>";
                    rowstr += "<td align='left' class='FinYear'>" + value.FinYear + "</td>";
                    rowstr += "<td align='center' class='QtrNo'>" + value.QtrNo + "</td>";
                    rowstr += "<td align='center' class='TaxValue'>" + value.TaxValue + "</td>";
                    rowstr += "<td align='center' class='PaidAmt'>" + value.PaidAmt + "</td>";
                    rowstr += "<td align='center' class='SurchargeValue'>" + value.SurchargeValue + "</td>";
                    rowstr += "<td align='center' class='PaidSurchargeValue'>" + value.PaidSurchargeValue + "</td>";
                    rowstr += "<td align='center' class='AdjustedOn'>" + value.AdjustedOn + "</td>";
                    rowstr += "</tr>";

                    tabAdvAdjDtls.find("tbody").append(rowstr);
                });
               
            } 
        }

        var OnViewAdvanceAdjustmentAll = function () {
            if (From_ValidateForAdjustAll()) {
                var EffectFromDT = $('#dtFrom').val();
                var EffectToDT = $('#dtTo').val();
                var WardID = $('#hdnWard').val();
                var LocationID = $('#hdnLocation').val();
                var AssesseeID = $('#hdnAssessee').val();
                
                serverObject.ViewAdvanceAdjustmentData(EffectFromDT, EffectToDT, WardID, LocationID, AssesseeID, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        modelData = [];
                        if (response.Data.length > 0) {
                            $.each(response.Data, function (index, value) {
                                var obj = value;
                                obj.UnqID = uuidv4();
                                modelData.push(obj);
                            });
                            //console.log(modelData);
                            PopulateTable(modelData);
                            $("#divAllAssessment").show();
                        }
                        else {
                            alert("No Record found!!!");
                            var tabAllAssessment = $("#tabAllAssessment");
                            tabAllAssessment.find("tbody").empty();
                            $("#divAllAssessment").hide();
                        }
                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var PopulateTable = function (responseData) {
            var tabAllAssessment = $("#tabAllAssessment");
            tabAllAssessment.find("tbody").empty();
            //console.log(responseData);
            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {

                    var btnString = "";
                    var btnAdjestString = "";
                    var arrRowData = alasql('SELECT * FROM ?  where AdvPaymentID=' + value.AdvPaymentID + ' and AssesseeID=' + value.AssesseeID, [modelData]);
                    var AdvAmt = arrRowData[0].AdvAmt;
                    var UnadjustAdvAmt = arrRowData[0].UnadjustAdvAmt;
                    var AdjustAdvAmt = arrRowData[0].AdjustAdvAmt;
                    
                    if (UnadjustAdvAmt == 0 && AdvAmt == AdjustAdvAmt) {
                        btnString = "<td align='center'><button type='button' data-id='" + value.UnqID + "' class='btn-link btn-xs btnAdvDtls' data-toggle='modal' data-target='#ModalAdvDetls' >Adjusted<br>Show Details</button></td>";
                    } else if (AdjustAdvAmt == 0 && AdvAmt == UnadjustAdvAmt){
                        btnString = "<td align='center'>Not Adjusted</td>";
                    } else if (UnadjustAdvAmt != 0 && AdvAmt != UnadjustAdvAmt) {
                        btnString = "<td align='center'><button type='button' data-id='" + value.UnqID + "' class='btn-link btn-xs btnAdvDtls' data-toggle='modal' data-target='#ModalAdvDetls' >Partly Adjusted<br>Show Details</button></td>";
                    }

                    if (AdvAmt != AdjustAdvAmt) {
                        btnAdjestString = "<td align='center'><button type='button' data-id='" + value.UnqID + "' class='btn-link btn-xs btnAdjust'>Adjust Remaining Amount</button></td>";
                    } else {
                        btnAdjestString = "<td></td>";
                    }
                    
                    var rowstr = "<tr>";
                    rowstr += "<td align='left' class='AssesseeName'>" + value.AssesseeName + "</td>";
                    rowstr += "<td align='center' class='HoldingNo'>" + value.HoldingNo + "</td>";
                    rowstr += "<td align='center' class='ReceiptNo'>" + value.ReceiptNo + "</td>";
                    rowstr += "<td align='center' class='AdvanceDate'>" + value.AdvanceDate + "</td>";
                    rowstr += "<td align='center' class='AdvAmt'>" + value.AdvAmt + "</td>";
                    rowstr += "<td align='center' class='UnadjustAdvAmt'>" + value.UnadjustAdvAmt + "</td>";
                    rowstr += "<td align='center' class='AdjustAdvAmt'>" + value.AdjustAdvAmt + "</td>";
                    //rowstr += "<td align='center'><input type='checkbox' name='chkApprove' class='chkAppr' value='" + value.UnqID + "'/></td>"
                    rowstr += btnString;
                    rowstr += btnAdjestString;
                    rowstr += "</tr>";

                    tabAllAssessment.find("tbody").append(rowstr);
                });
                RegistertabAllAdvAdjEvent();

            }
        }

        var AutocompleteSource = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnWard').val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        $('#hdnWard').val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnLocation').val('');
                        Resetors.ResetAssesse();
                        //reset area
                        var wardID = $('#hdnWard').val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        $('#hdnLocation').val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        $('#hdnAssessee').val('');
                        //reset area
                        var locationID = $('#hdnLocation').val();
                        var wardID = $('#hdnWard').val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName + '<' + item.HoldingNo + '>',
                                            AssesseeID: item.AssesseeID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        $('#hdnAssessee').val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            }
        };
        var Resetors = {
            ResetWard: function () {
                $('#hdnWard').val('');
                $('#txtWard').val('');
            },
            ResetLocation: function () {
                $('#hdnLocation').val('');
                $('#txtLocation').val('');
            },
            ResetAssesse: function () {
                $('#hdnAssessee').val('');
                $('#txtAssessee').val('');
            }
        };
        this.app_Initiate = function () {
            
            BindEvent();
            if ($("#btnView").attr("data-onload-serch") === "True") {
                $("#btnView").trigger('click');
            }
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});