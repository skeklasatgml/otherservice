﻿$(document).ready(function () {
    var ReleaseOrderFile = null;
    var Server = function () {
        this.GetApprovalUI = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/GetUIInstallmentApproval',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
        this.ChangeApprovalStatusInstallment = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/ChangeApprovalStatusInstallment',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('.IsreleaseFileUploded').val(response.Data);
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
        this.GenerateReleaseOrder = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/Approval_GenerateReleaseOrder',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
        this.SvInternalMessage = function (data, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SaveInternalMessage',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        }
    };

    var ApprovalApp = function (srv) {
        var doms = function () {
            return {
                parentModal: ".approval-box",
                finalModal: ".final-approval-modal"
            }
        };
        var getFormdataFirstModal = function () {
            function SobProjNotSelectedFun() {
                var arr = [];
                $(".check-sbp").each(function () {
                    var ischecked = $(this).is(':checked');
                    if (!ischecked) {
                        var ob = {
                            sbpId: $(this).attr("data-sbpId"),
                            DprId: $(this).attr("data-dprId"),
                            instId: $(this).attr("data-instId")
                        }
                        arr.push(ob);
                    }
                })
                return arr;
            };
            var parent = $(doms().parentModal).find('form');
                return {
                    FundReleaseId: parent.find("#FundReleaseId").val(),
                    status: parent.find("#status").val(),
                    remarks: parent.find("#msg").val(),
                    targetDesignationId: parent.find("#designation").val(),
                    approvedAmount: (isNaN(parent.find("#approved-amount").val()) ? 0 : Number(parent.find("#approved-amount").val())),
                    Subprojects: $(".tbl-sub-projects tbody tr.data")
                        .map(function (i, v) { return { Cost: $(v).find(".txt-subp-SancCost").val().toNumber(), Id: $(v).attr('data-id').toNumber() }; }).get(),
                    SobProjNotSelected: SobProjNotSelectedFun()
                };
        };
        var getInternalMsgFirstModal = function () {
            var parent = $(doms().parentModal).find('form');
            return {
                mstDprId: parent.find("#mstDprId").val(),
                FundReleaseId: parent.find("#FundReleaseId").val(),
                InternalMessage: $(doms().parentModal).find("#Internalmsg").val()
            };

        };
        var getFormdataSecondModal = function () {
            return {
                AccountHead: $(doms().finalModal).find(".txt-account-head").val(),
                UoDate: window.toDate($(doms().finalModal).find(".txt-uo-date").val()),
                SanctionNo: $(doms().finalModal).find(".txt-sanction-number").val(),
                UoNo: $(doms().finalModal).find(".txt-uo-no").val(),
                //ApprovalDate: window.toDate($(doms().finalModal).find(".txt-approval-date").val()),
                ReleaseOrderFile: ReleaseOrderFile,
                ReleaseOrderNo: $(doms().finalModal).find(".release-order-no").val(),
                ReleaseOrderDt: window.toDate($(doms().finalModal).find(".release-order-dt").val()),
            };
        };

        var validateFirstForm = function () {
            registerFirstValidator();
            return $(doms().parentModal).find('form').valid();
        };
        var validateSecondModalForm = function () {
            registerSecondModalValidator();
            return $(doms().finalModal).find('form').valid();
        };

        var ChangeInstallmentApprovalStatusExceptApproval = function () {
            var parent = $(doms().parentModal).find('form');
            if (!validateFirstForm()) {
                return;
            };
            alertify.confirm('Confirm Us!', 'Are you sure to change status?', function () {
                var data = getFormdataFirstModal();
                srv.ChangeApprovalStatusInstallment(data, function (r) {
                    if (r.Status === 200) {
                        alertify.alert('Successful', r.Message, function () {
                            LoadInstallmentWithProgress($(".mst-dpr-id").val(), data.FundReleaseId);
                        });
                    } else {
                        alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                    }
                });
            }, function () { })
        };
        var SaveInternalMessage = function () {
            var data = getInternalMsgFirstModal();
            srv.SvInternalMessage(data, function (r) {
                if (r.Status === 200) {
                    alertify.alert('Successful', r.Message, function () {
                        LoadInstallmentWithProgress(data.FundReleaseId);
                    });
                } else {
                    alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                }

            });
        };
        var FinalApprove = function (successcallback) {
            var parent = $(doms().parentModal).find('form');
            if (validateFirstForm() && validateSecondModalForm()) {
                alertify.confirm('Confirm us!', 'Are you sure to Approve Finally?', function () {
                    var data = getFormdataFirstModal();
                    var data2 = getFormdataSecondModal();
                    var payload = Object.assign(data, data2);
                    srv.ChangeApprovalStatusInstallment(data, function (r) {
                        if (r.Status === 200) {
                            alertify.alert('Successful', r.Message, function () {
                                LoadInstallmentWithProgress($(".mst-dpr-id").val(), data.FundReleaseId);
                                if (successcallback)
                                    successcallback();
                            });
                           
                        } else {
                            alertify.alert("<span style='color:red'>Error</span>", r.Message, function () { });
                        }
                    });
                }, function () { });

            }
        };

        var registerFirstValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            };
            $.validator.addMethod('positiveNumber', function (value) {
                return Number(value) > 0;
            }, 'Enter a positive decimal.');

            vld.rules["msg"] = { required: true };
            vld.rules["status"] = { required: true };
            vld.rules["designation"] = { required: (($(doms().parentModal).find("#status").val() !== "N") || ($(doms().parentModal).find("#status").val() !== "A" && $("#isFinallAprovalPossible").val() == 0)) };
            vld.rules["approved-amount"] = { required: ($(doms().parentModal).find("#approved-amount").prop('disabled') === false), positiveNumber: ($(doms().parentModal).find("#approved-amount").prop('disabled') === false) }
            $(doms().parentModal).find('form').validate(vld);
        };
        var registerSecondModalValidator = function () {
            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    if (element.siblings(".input-group-addon").length > 0) {
                        error.insertAfter(element.parent());
                        error.css('color', 'red');
                        return;
                    }
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            };
            $.validator.addMethod('positiveNumber', function (value) {
                return Number(value) > 0;
            }, 'Enter a positive decimal.');

            //.rules["txt-approval-date"] = { required: true };
            vld.rules["file-release-order"] = { required: true };
            vld.rules["release-order-no"] = { required: true };
            vld.rules["release-order-dt"] = { required: true };
            $(doms().finalModal).find('form').validate(vld);
        };
        var fillAaFsOnModal = function () {
            var aafs = $(doms().parentModal).find(".hdn-aafs").val();
            if (aafs && aafs != "null") {
                aafs = JSON.parse(aafs);
                $(doms().finalModal).find(".txt-sanction-number").val(aafs.SanctionNo);
                $(doms().finalModal).find(".txt-uo-no").val(aafs.UoNo);
                $(doms().finalModal).find(".txt-uo-date").val(aafs.UoDate ? new Date(aafs.UoDate).format('dd/mm/yyyy') : '');
               // $(doms().finalModal).find(".txt-approval-date").val(aafs.ApprovalDate ? new Date(aafs.ApprovalDate).format('dd/mm/yyyy') : '');
                $(doms().finalModal).find(".release-order-no").val(aafs.ReleaseOrderNo);
                $(doms().finalModal).find(".release-order-dt").val(aafs.ReleaseOrderDate ? new Date(aafs.ReleaseOrderDate).format('dd/mm/yyyy') : '');
                $('.viewFile').css("display", "block");
                $('.viewFile').attr("href", aafs.ReleaseFilePath);
            }
        };
        var makeModalReadyOnlyOnLogic = function () {
            var CanGenerateAAFS = ($(".canGenerateAAFS").val() == 1);
            var isReadonly = !CanGenerateAAFS;
            $(doms().finalModal).find(".txt-sanction-number").prop('readonly', isReadonly);
            $(doms().finalModal).find(".txt-uo-no").prop('readonly', isReadonly);
            $(doms().finalModal).find(".txt-uo-date").datepicker("option", "disabled", isReadonly);
            //$(doms().finalModal).find(".txt-approval-date").datepicker("option", "disabled", isReadonly);
            $(doms().finalModal).find(".release-order-no").prop('readonly', isReadonly);
            $(doms().finalModal).find(".release-order-dt").datepicker("option", "disabled", isReadonly);
            $(doms().finalModal).find(".file-release-order").prop('disabled', isReadonly);
        };
        var finalModalMode = {
            activateFinalApproval: function () {
                var $cntr = $(doms().finalModal);
                $cntr.find(".btn-gen-aafs").addClass('hidden');
                $cntr.find(".btn-final-approve").addClass('hidden');
                $cntr.find('.modal-title').text('Final Approval');
                $(".btn-final-approve").removeClass('hidden');
                makeModalReadyOnlyOnLogic();
            },
            activateDefault: function () {
                var $cntr = $(doms().finalModal);
                $cntr.find(".btn-gen-aafs").addClass('hidden');
                $cntr.find(".btn-final-approve").addClass('hidden');
                makeModalReadyOnlyOnLogic();
            },
            activateGenAAFs: function () {
                var $cntr = $(doms().finalModal);
                $cntr.find(".btn-gen-aafs").addClass('hidden');
                $cntr.find(".btn-final-approve").addClass('hidden');
                $cntr.find('.modal-title').text('Generate Release Order');
                $(".btn-gen-aafs").removeClass('hidden');
                makeModalReadyOnlyOnLogic();
            }
        };
        var LoadInstallmentWithProgress = function (mstDprId, fundReleaseId) {
            if (!mstDprId || !fundReleaseId) {
                return;
            }
            srv.GetApprovalUI({ mstDprId: mstDprId, fundReleaseId: fundReleaseId }, function (r) {
                $(doms().parentModal).empty().append(r);
                //make visble scrollbar
                $(".box-dpr-messages .box-body").slimScroll({
                    height: $(".box-dpr-messages  .box-body").css('height'),
                    allowPageScroll: true,
                    alwaysVisible: true
                });
                $('.btn-search-by-date').trigger('click');
                //$(doms().parentModal).find("#approved-amount").prop('disabled', !($(doms().parentModal).find("#canApprovalAmtChangble").val() == 1 ? true : true));
                $(doms().parentModal).find(".check-sbp").prop('disabled', !($(doms().parentModal).find("#canApprovalAmtChangble").val() == 1 ? true : false));
                $(doms().parentModal).find("#status").trigger('change');
                if ($(".canGenerateAAFS").val() == 1) {
                    finalModalMode.activateGenAAFs();
                }
                if ($(doms().parentModal).find("#status").val() == "A") {
                    finalModalMode.activateFinalApproval();
                }
                fillAaFsOnModal();
                //makeModalReadyOnlyOnLogic();
            });
        };
        (function () {
            
            //$(doms().parentModal).find('.modal-dialog').css('width', '95%');
            $(document).on('change', ".select-fund-release-id", function () {
                LoadInstallmentWithProgress($(".mst-dpr-id").val(), $(this).val().toNumber());
            });
            $(document).on('change', "#status", function () {
                finalModalMode.activateDefault();
                if ($(doms().parentModal).find("#status").val() == "A") {
                    finalModalMode.activateFinalApproval();
                }
                if ($('.canGenerateAAFS').val() === "1"){
                    finalModalMode.activateGenAAFs();
                }
                //if ($(doms().parentModal).find("#status").val() !== "A" && $('.canGenerateAAFS').val() !== "1") {
                //    finalModalMode.activateDefault();
                //}
            });
            $(doms().parentModal).on('keyup', ".txt-subp-SancCost", function () {
                $(this).closest('tr').find('.check-sbp').attr('data-amtapplied', $(this).closest('tr').find(".txt-subp-SancCost").val());
                $(".check-sbp").trigger("change");
            });
           // if (($(doms().parentModal).find("#status").val() == "A" || (($(doms().parentModal).find("#status").val() == "F" || $(doms().parentModal).find("#status").val() == "R") && $('.canGenerateAAFS').val() == "0")) && !($('.IsreleaseFileUploded').val().length > 0)) {
            $(doms().parentModal).on('click', "#btn-submit-msg", function () {
                if ($(doms().parentModal).find("#status").val() === "A" ||  $('.canGenerateAAFS').val() === "1") {
                    if (validateFirstForm())
                        $(doms().finalModal).modal('show');
                    return;
                }
                ChangeInstallmentApprovalStatusExceptApproval();
            });
            $(doms().parentModal).on('click', "#btn-submit-Internalmsg", function () {
                SaveInternalMessage();
            });
             $(doms().parentModal).on('change', ".check-sbp", function () {
                var SumOfAppliedAmt = parseFloat("0.0");
                $(".check-sbp").each(function () {
                    var ischecked = $(this).is(':checked');
                    if (ischecked) {
                        SumOfAppliedAmt = parseFloat(SumOfAppliedAmt) + parseFloat($(this).attr("data-amtapplied")); 
                    }
                });
                 $('#approved-amount').val(SumOfAppliedAmt.toFixed(2));
            }); 
            $(document).on('click', ".btn-gen-aafs", function () {
                //GenerateReleaseOrder(function () {
                //    $(".final-approval-modal").modal('hide');
                //    $(".final-approval-modal").find('form').trigger("reset");
                //});
                FinalApprove(function () {
                    $(doms().finalModal).modal('hide');
                    $(doms().finalModal).find('form').trigger("reset");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                });               
            });
        $(document).on('click', '.btn-final-approve', function (e) {
                e.preventDefault();
                FinalApprove(function () {
                    $(doms().finalModal).modal('hide');
                    $(doms().finalModal).find('form').trigger("reset");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                });
            });

            $(doms().parentModal).on('change', "#status", function () {
                if ($(this).val() == "A" || $("#installmentStatus").val()=="A") {
                    $(doms().parentModal).find("#designation>option:first").prop('selected', true);
                    $(doms().parentModal).find("#designation").prop('disabled', true);
                } else {
                    $(doms().parentModal).find("#designation").prop('disabled', false);
                }
            });
            $(document).on('click', ".remove-date", function () {
                var clearTergate = $(this).attr("data-clear-tergate");
                $("." + clearTergate).val('');
                //$(this).siblings('input[type=text]').val('');
                if (clearTergate == "txt-from-date") {
                    doms().txtToDate.datepicker("option", "beforeShowDay", function (date) {
                        return [false, ''];
                    });
                }
            });
            $(document).on('change', ".file-release-order", function () {
                var xyz = $(this);
                ReleaseOrderFile = {};
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        ReleaseOrderFile.StageId = response.Data;
                        ReleaseOrderFile.Result = null;
                        ReleaseOrderFile.Name = null;
                        $('.viewFile').css("display", "block");
                        $('.viewFile').attr("href", response.FileLink);
                        alertify.success(response.Message);
                    }, function (response) {
                        alertify.error(response.Message);
                        $(this).val('');
                    });
                }
            });
        }());
    };
    (function () {
        var srv = new Server();
        var app = new ApprovalApp(srv);
    }());
});