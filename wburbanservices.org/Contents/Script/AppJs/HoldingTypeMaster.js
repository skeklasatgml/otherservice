﻿var app = angular.module("myapp", []);
app.controller("mycontroller", function ($scope, $window, $http) {
    $scope.btnSave = "Save";
    angular.element('#txtHoldingTypeDescription').focus();
    $scope.HoldingTypes = [];

    $scope.load = function () {
        Get_AllHolding();
    }

    $scope.Reset = function () {
        $scope.txtHoldingTypeDescription = "";
        $scope.hdnHoldingTypeID = "";
        $scope.btnSave = "Save";
        angular.element('#txtHoldingTypeDescription').focus();
    }

    $scope.Save_Update = function () {
        if ($scope.txtHoldingTypeDescription == "") {
            alert("Please write a holding type");
            angular.element('#txtHoldingTypeDescription').focus();
            return false;
        }
        var ID = $scope.hdnHoldingTypeID;
        var txt = $scope.txtHoldingTypeDescription;
        if (ID == "") {
            Save(txt);
        } else {
            Update(ID, txt);
        }
    }

    $scope.Edit = function (HoldingType) {
        $scope.txtHoldingTypeDescription = HoldingType.HoldingTypeDesc;
        $scope.hdnHoldingTypeID = HoldingType.HoldingTypeID;
        $scope.btnSave = "Update";
    }

    $scope.Delete = function (HoldingTypeID) {
        if ($window.confirm("Are you sure want to delete this")) {
            $http({
                method: "post",
                url: "/HoldingType/Delete_HoldingType",
                data: { HoldingTypeID: HoldingTypeID }
            }).then(function (response) {
                alert(response.data.Message);
                if (response.data.Status == 200) {
                    Get_AllHolding();
                }
            }, function () { alert("Error occur"); })
        }
    }

    function Save(HoldingTypeDesc) {
        $http({
            method: "post",
            url: "/HoldingType/Save_HoldingType",
            data: { HoldingTypeDesc: HoldingTypeDesc }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200) {
                Get_AllHolding();
            }
        }, function () { alert("Error occur"); })
    }

    function Update(HoldingTypeID, HoldingTypeDesc) {
        $http({
            method: "post",
            url: "/HoldingType/Update_HoldingType",
            data: { HoldingTypeID: HoldingTypeID, HoldingTypeDesc: HoldingTypeDesc }
        }).then(function (response) {
            alert(response.data.Message);
            if (response.data.Status == 200) {
                Get_AllHolding();
            }
        }, function () { alert("Error occur"); })
    }

    function Get_AllHolding() {
        $scope.txtHoldingTypeDescription = "";
        $scope.hdnHoldingTypeID = "";
        $scope.btnSave = "Save";
        angular.element('#txtHoldingTypeDescription').focus();
        $http({
            method: "post",
            url: "/HoldingType/Get_AllHoldingType"
        }).then(function (response) {
            $scope.HoldingTypes = response.data.Data;
        }, function () { alert("Error occur"); })
    }
})