﻿//*Note: Some of the code block copied from dashboard.js
$(document).ready(function () {
    'use strict';
    var currencyFormatter = OSREC.CurrencyFormatter;
    var formatCurrency=function(amount){
        return currencyFormatter.format(amount, { currency: 'INR' });
    };
    var ajax = function (method, url, data, successCallbak) {
        var _method = 'post';
        var _url = url;
        var _data = null;
        if (method) {
            _method = method;
        }
        if (data) {
            _data = data;
        }
        $.ajax({
            type: _method,
            url: _url,
            data: JSON.stringify(_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                successCallbak(response);
            },
            failure: function (response) {
                alert(response);
            }
        });
    };
    var preaparedControl = function (selector, container) {
        var obj = {
            _selector: selector,
            _obj: null
        };
        if (container) {
            var container = $(container);
            obj._obj = container.find(selector);
        }
        return obj;
    };
    var _scopeContainer = preaparedControl('.dash-board', 'body');
    var municipalityFilter = new function () {
        var _self = this;
        var scopeContainer = $("#FB26FE2E-4474-4FB8-8835-6606CE06E060");
        this.Modal = scopeContainer;
        this.show = function () {
            scopeContainer.modal('show');
        };
        this.hide = function () {
            scopeContainer.modal('hide');
        }
        var uncheckAll = function () {
            var checkBoxes = scopeContainer.find('table tbody tr').find('input[type="checkbox"]:checked');
            checkBoxes.prop('checked', false);
        };
        this.selectedMunicipalities = function (ids) {
            uncheckAll();
            var trs = scopeContainer.find('table tbody tr');
            $.each(ids, function (i, v) {
                var check = trs.find('input[data-MunicipalityId="' + v + '"]');
                if (check) {
                    check.prop('checked', true)
                }
            });
        };
        this.applyFilter = function (callback) {

        };
        (function () {
            scopeContainer.find(".btn-apply-filter").on('click', function () {
                if (_self.applyFilter) {
                    var ids = [];
                    var checkBoxes = scopeContainer.find('table tbody tr').find('input[type="checkbox"]:checked');
                    $.each(checkBoxes, function (i, v) {
                        ids.push(Number($(v).attr('data-MunicipalityId')));
                    });
                    _self.applyFilter({ MunicipalityIds: ids }, _self);
                }
            });
        }())
    };



    var dillMunicipalityCollectionComponent = new function () {
        var _self = this;
        var _curRoute = null;
        var _domControl = {
            _tabDrilldownMunicipality: preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj),
            _tblCollectionByCategories: preaparedControl(".tbl-collection-in-categories", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _btnDoorGetData: preaparedControl(".btn-door-Municipality-Collection", _scopeContainer._obj),
            _ddlTimeSpan: preaparedControl(".ddl-time-span", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrCustomDateRange: preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _txtFromDate: preaparedControl(".from-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj)._obj),
            _txtToDate: preaparedControl(".to-date", preaparedControl(".custom-date-range", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj)._obj),
            _btnGetDetails: preaparedControl(".btn-get-details", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrPieCollectionByOrigine: preaparedControl(".pie-origine", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrPieCollectionPayHeardWise: preaparedControl(".pie-payhead-wise", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _filterBtnMuncipality: preaparedControl(".municipality-filter", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrBarChrtMunicipalityWiseCollection: preaparedControl(".bar-chrt-municipality-wise-collection", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),
            _cntrPaymentFlowChart: preaparedControl(".flow-chart-payment-flow", preaparedControl("#drilldown-Municipality-Collection", _scopeContainer._obj)._obj),


            _btnDemandVsCollection: preaparedControl(".btn-door-demand-vs-collection", _scopeContainer._obj),
            _tabDemandVsCollection: preaparedControl("#drilldown-demand-vs-collection", _scopeContainer._obj),
        };
        var pollyfill = new function () {
            _domControl._tblCollectionByCategories.setCaption = function (title) {
                _domControl._tabDrilldownMunicipality._obj.find('.box-title-right').html(title);
            };
        };
        var getRouteNPayload = function (key) {
            var getDateRangeModel = function () {
                var timeSpanVal = _domControl._ddlTimeSpan._obj.val();
                switch (timeSpanVal) {

                    case 'last-1-year':
                        {
                            var fromDate1 = new Date();
                            var toDate1 = new Date();
                            fromDate1 = new Date(fromDate1.setMonth(toDate1.getMonth() - 12))
                            return {
                                FromDate: fromDate1,
                                ToDate: toDate1
                            };
                            break;
                        }
                    case 'date-range':
                        {
                            var txtfromDate = _domControl._txtFromDate._obj.val();
                            var txttoDate = _domControl._txtToDate._obj.val();
                            if (txtfromDate.split("/").length !== 3) {
                                throw "Invalid From Date: " + txtfromDate;
                            }
                            if (txttoDate.split("/").length !== 3) {
                                throw "Invalid To Date: " + txttoDate;
                            }
                            var fromDate = CONVERTER.Date.convertToDate(
                                txtfromDate.split("/")[2],
                                txtfromDate.split("/")[1],
                                txtfromDate.split("/")[0]
                            );
                            var toDate = CONVERTER.Date.convertToDate(
                                txttoDate.split("/")[2],
                                txttoDate.split("/")[1],
                                txttoDate.split("/")[0]
                            );
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                        }
                    case 'last-6-months':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setMonth(toDate.getMonth() - 6));
                            if (timeSpanVal != 'last-6-months') {
                                _domControl._ddlTimeSpan._obj.find('option:eq(1)').prop('selected', true);
                            }
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-month':
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            fromDate = new Date(fromDate.setDate(1));
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    case 'last-7-days':
                            {
                                var fromDate = new Date();
                                var toDate = new Date();
                                fromDate = new Date(fromDate.setDate(toDate.getDate() - 7));
                                return {
                                    FromDate: fromDate,
                                    ToDate: toDate
                                };
                                break;
                            }
                    case 'to-day':
                    default:
                        {
                            var fromDate = new Date();
                            var toDate = new Date();
                            return {
                                FromDate: fromDate,
                                ToDate: toDate
                            };
                            break;
                        }
                    }
                
            };
            var dateRange = getDateRangeModel();
            var obj = {};
            var getRouteModel = function (key, url, payload) {
                return {
                    key: key,
                    url: url,
                    payload: payload
                };
            };
            obj["AppOrigine-wise-Summary"] = function () {
                return getRouteModel("AppOrigine-wise-Summary", '/Municipality/DashBoard/AppOrigineWiseCollection', dateRange);
            };
            obj["Ward-Wise-Collection"] = function () {
                return getRouteModel("Ward-Wise-Collection", '/Municipality/DashBoard/WardWiseCollection', dateRange);
            };
            obj["Collector-Wise-Collection"] = function () {
                return getRouteModel("Collector-Wise-Collection", '/Municipality/DashBoard/CollectorWiseCollection', dateRange);
            };
            obj["Payment-Flow-All-Collector"] = function () {
                var partialLoad = {
                    DataSheetType: _domControl._ddlTimeSpan._obj.val()
                };
                return getRouteModel("Payment-Flow-All-Collector", '/Municipality/DashBoard/GetCollectionFlowTimeSpanAllCollector', Object.assign( dateRange,partialLoad));
            };
            
            obj["DemandVsCollection-Summary"] = function () {
                return getRouteModel("DemandVsCollection-Summary", '/Municipality/DashBoard/DemandVsCollectionSummary', false);
            };

            obj["DemandVsCollection-WardWiseSummary"] = function () {
                return getRouteModel("DemandVsCollection-WardWiseSummary", '/Municipality/DashBoard/DemandVsCollectionWardWiseSummary', false);
            };

            return obj[key];
        };
        var setTitle = function (title) {
            _domControl._tabDrilldownMunicipality._obj.find(".box-title").html(title);
        };
        var getDefaultDateRange = function () {
            var timeSpanVal = _domControl._ddlTimeSpan._obj.val();
            switch (timeSpanVal) {

                case 'last-1-year':
                    {
                        var fromDate1 = new Date();
                        var toDate1 = new Date();
                        fromDate1 = new Date(fromDate1.setMonth(toDate1.getMonth() - 12))
                        return {
                            FromDate: fromDate1,
                            ToDate: toDate1
                        };
                        break;
                    }
                case 'date-range':
                    {
                        var txtfromDate = _domControl._txtFromDate._obj.val();
                        var txttoDate = _domControl._txtToDate._obj.val();
                        if (txtfromDate.split("/").length !== 3) {
                            throw "Invalid From Date: " + txtfromDate;
                        }
                        if (txttoDate.split("/").length !== 3) {
                            throw "Invalid To Date: " + txttoDate;
                        }
                        var fromDate = CONVERTER.Date.convertToDate(
                            txtfromDate.split("/")[2],
                            txtfromDate.split("/")[1],
                            txtfromDate.split("/")[0]
                        );
                        var toDate = CONVERTER.Date.convertToDate(
                            txttoDate.split("/")[2],
                            txttoDate.split("/")[1],
                            txttoDate.split("/")[0]
                        );
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                    }
                case 'last-6-months':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setMonth(toDate.getMonth() - 6));
                        if (timeSpanVal != 'last-6-months') {
                            _domControl._ddlTimeSpan._obj.find('option:eq(1)').prop('selected', true);
                        }
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'last-month':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setDate(1));
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'last-7-days':
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        fromDate = new Date(fromDate.setDate(toDate.getDate() - 7));
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
                case 'to-day':
                default:
                    {
                        var fromDate = new Date();
                        var toDate = new Date();
                        return {
                            FromDate: fromDate,
                            ToDate: toDate
                        };
                        break;
                    }
            }
        };

        var post = function (payload, callback) {
            ajax('post', payload.url, payload.payload, function (r) {
                
                if (r.Status === 200) {
                    callback(r)
                } else {
                    alertify.error(r.Message);
                }
            });
        };

        var _loadAppOrigineCollectionSummary = function () {
            try {
                _domControl._btnGetDetails._obj.hide();
                //summary load
                var payload = getRouteNPayload("AppOrigine-wise-Summary")();
                post(payload, function (r) {
                    _populateAppOrigineCollectionSummary(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };
        var _loadWiseCollectionData = function () {
            try {
                _domControl._btnGetDetails._obj.hide();
                //summary load
                var payload = getRouteNPayload("Ward-Wise-Collection")();
                var titelBox = _domControl._tabDrilldownMunicipality._obj.find(".title-wise-bar-chart");
                titelBox.text("Ward Wise Collection");
                if ($(".ddl-collection-by").val() == 'collector') {
                    payload = getRouteNPayload("Collector-Wise-Collection")();
                    titelBox.text("Collector Wise Collection");
                }
                post(payload, function (r) {
                    _populateWiseCollection(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };
        var _loadPaymentFlowByWithTime = function () {
            try {
                _domControl._btnGetDetails._obj.hide();
                //summary load
                var payload = getRouteNPayload("Payment-Flow-All-Collector")();
                post(payload, function (r) {
                    _populatePaymentFlowByTime(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };
        var _populatePaymentFlowByTime = function (datasrc,title) {
            var ctrl = document.getElementById(_domControl._cntrPaymentFlowChart._obj.attr("id"));
            _domControl._cntrPaymentFlowChart._obj.empty();
            if (_domControl._ddlTimeSpan._obj.val() == 'to-day' || datasrc.length==0) {
                _domControl._cntrPaymentFlowChart._obj.append("<h3><small>Details Not Available</small></h3>");
                return;
            }
            google.charts.load('current', {'packages':['line', 'corechart']});
            google.charts.setOnLoadCallback(drawChart);
            
            function drawChart() {
                var rows = [];
                rows.push(['Year', 'Periods']);
                $.each(datasrc, function (index, value) {
                    rows.push([value.DtSpanBase, value.NetPaidAmount]);
                        });
                var data = google.visualization.arrayToDataTable(rows);
                var options = {
                    title: 'Period Wise Collection Details',
                    //curveType: 'function',
                    legend: { position: 'bottom' },
                    height: '400',
                    hAxis: {
                        direction: 1,
                        slantedText: true,
                        slantedTextAngle: 45 // here you can even use 180
                    }
                };

                var chart = new google.visualization.AreaChart(ctrl);

                chart.draw(data, options);
            }
        };
        var _populateAppOrigineCollectionSummary = function (data, title) {

            var paidOnApp = 0;
            var paidOnWebPortal = 0;
            var paidOnCounter = 0;

            var getTotalAmountByOrigine = function (appOrigineOrKey, dtSource) {
                try {
                    if (appOrigineOrKey.toLowerCase()=='total') {
                        var rows = alasql("select sum(NetPaidAmount) as totAmount from ?", [dtSource]);
                        if (rows.length > 0) {
                            return Number(rows[0].totAmount).toFixed(2);
                        } else {
                            throw 'no row found';
                        }
                    } else {
                        var rows = alasql("select sum(NetPaidAmount) as totAmount from ? where AppOrigine='" + appOrigineOrKey + "'", [dtSource]);
                        if (rows.length > 0) {
                            return Number(rows[0].totAmount).toFixed(2);
                        } else {
                            throw 'no row found';
                        }
                    }
                } catch (e) {
                    return 0;
                }
            };

            var totalCollection = currencyFormatter.format(getTotalAmountByOrigine("total", data), { currency: 'INR' });
            $(".collection-summary .amount-head-total").text(totalCollection);
            $(".collection-summary .amount-head-counter").text(currencyFormatter.format(getTotalAmountByOrigine("COUNTER", data), { currency: 'INR' }));
            $(".collection-summary .amount-head-App").text(currencyFormatter.format(getTotalAmountByOrigine("APP", data), { currency: 'INR' }));
            $(".collection-summary .amount-head-web-portal").text(currencyFormatter.format(getTotalAmountByOrigine("WEB", data), { currency: 'INR' }));

            var chartHeightRecomended = 400;
            var charPopulate1 = function (data1) {
                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var chartData = [];
                    chartData.push(['Collection Type', 'Collection Amount']);
                    $.each(
                        data1,
                        function (index, value) {
                            chartData.push([
                                 value.AppOrigine + ': ' + formatCurrency(value.NetPaidAmount),
                                 value.NetPaidAmount//.toFixed(2)
                            ]);
                        }
                    );
                    var data2 = google.visualization.arrayToDataTable(chartData);

                    var options = {
                        title: 'Collection Graph',
                        height: chartHeightRecomended,
                        legend: { position: 'right' },
                        pieSliceText: 'value',
                        sliceVisibilityThreshold: 0,
                        pieHole: 0.4
                    };

                    var ctrl = document.getElementById(_domControl._cntrPieCollectionByOrigine._obj.attr("id"));
                    var chart = new google.visualization.PieChart(ctrl);

                    chart.draw(data2, options);
                };
            }
            var charPopulate2 = function (data1) {
                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    data1 = alasql("select sum(CurPropTax) as CurPropTax, sum(ArrPropTax) as ArrPropTax,sum(CurSurcharge) as CurSurcharge, sum(ArrSurcharge) as ArrSurcharge, sum(OtherAdjustment) as OtherAdjustment, sum(Rebate) as Rebate, sum(Interest) as Interest from ?", [data1]);
                    var chartData = [];
                    chartData.push(['Text', 'Value']);
                    $.each(
                        data1,
                        function (index, value) {
                            chartData.push([
                                 "Curr. Prop. Tax: " + formatCurrency(value.CurPropTax), value.CurPropTax
                            ]);
                            chartData.push([
                                "Arr. Prop. Tax: " + formatCurrency(value.ArrPropTax), value.ArrPropTax
                            ]);
                            chartData.push([
                                "Curr. Sur.: " + formatCurrency(value.CurSurcharge), value.CurSurcharge
                            ]);

                            chartData.push([
                                 "Arr. Sur.: " + formatCurrency(value.ArrSurcharge), value.ArrSurcharge
                            ]);
                            chartData.push([
                                 "Interest: " + formatCurrency(value.Interest), value.Interest
                            ]);
                            chartData.push([
                                 "Rebate: " + formatCurrency(value.Rebate * -1), (value.Rebate * -1)
                            ]);
                            chartData.push([
                                 "Other Adjst.: " + formatCurrency(value.OtherAdjustment * -1), (value.OtherAdjustment * -1)
                            ]);
                        }
                    );
                    var data2 = google.visualization.arrayToDataTable(chartData);

                    var options = {
                        title: 'Collection Payhead Wise',
                        height: chartHeightRecomended,
                        pieSliceText: 'value',
                        legend: { position: 'right' },
                        sliceVisibilityThreshold: 0,
                        pieHole: 0.4,
                    };

                    var ctrl = document.getElementById(_domControl._cntrPieCollectionPayHeardWise._obj.attr("id"));
                    var chart = new google.visualization.PieChart(ctrl);

                    chart.draw(data2, options);
                };
            }

            charPopulate1(data);
            charPopulate2(data);
        };
        var _populateWiseCollection = function (data, title) {
            var chartDivCtrl = document.getElementById(_domControl._cntrBarChrtMunicipalityWiseCollection._obj.attr("id"));

            google.charts.load("current", {
                packages: ["bar"]
            });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var chartData = [];

                if ($(".ddl-collection-by").val() == "ward") {
                    chartData.push([
                        "WardName",
                        "Net Collection",
                    ]);

                    data = alasql("select * from ?", [data])
                    $.each(
                        data,
                        function (index, value) {
                            chartData.push([
                                value.WardName,
                                value.NetPaidAmount,
                            ]);
                        }
                    );
                } else {
                    chartData.push([
                        "ColectorName",
                        "Net Collection",
                    ]);

                    data = alasql("select * from ? order by NetPaidAmount asc", [data])
                    $.each(
                        data,
                        function (index, value) {
                            chartData.push([
                                value.CollectorName,
                                value.NetPaidAmount,
                            ]);
                        }
                    );
                }
                var data1 = google.visualization.arrayToDataTable(chartData);

                var options = {
                    chart: {
                        //title: "Municipality Wise Collection",
                        //subtitle: "Ward Wise Assessee Bar Chart"
                    },
                    bars: "horizontal",
                    vAxis: {
                        format: "decimal"
                    },
                    colors: ['green'],
                    height: 600,
                    width: '100%',
                    //series: {
                    //    0: { color: 'yellow' },
                    //    1: { color: 'green' },
                    //    2: { color:  'red'},
                    //    3: { color: 'blue'},
                    //},
                };

                var chart = new google.visualization.ColumnChart(chartDivCtrl);
                chart.draw(data1, google.charts.Bar.convertOptions(options));
            }
            //_domControl._tblLast5MunicipalityGrid.setCaption(title);
        };

        var _populateTotalDemandVsCollectionSummary = function (data) {

            $("#totalGauge").empty();
            $("#totalGaugeTable1").empty();

            $('#totalGaugeTable1').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:30%;">Total Demand</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].TotalDemand).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            $('#totalGaugeTable1').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:30%;">Total Collection</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].TotalCollection).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            var percentage = (Number(data[0].TotalCollection) * 100 / Number(data[0].TotalDemand)).toFixed(2);
            var TotDemand25Percentage = ((Number(data[0].TotalDemand) * 25) / 100).toFixed(2);
            var TotDemand50Percentage = ((Number(data[0].TotalDemand) * 50) / 100).toFixed(2);
            var TotDemand75Percentage = ((Number(data[0].TotalDemand) * 75) / 100).toFixed(2);
            var TotDemand100Percentage = ((Number(data[0].TotalDemand) * 100) / 100).toFixed(2);
            
            var g1 = "";
                g1 = new JustGage({
                    id: "totalGauge",                   
                    value: data[0].TotalCollection,
                    min: 0,
                    max: data[0].TotalDemand,
                    decimals: 2,
                    title: "Demand Vs Collection",
                    label: "Total Collection  " + percentage + " %",                    
                    gaugeWidthScale: 0.8,
                    pointer: true,                    
                    counter: true,
                    relativeGaugeSize: true,
                    formatNumber: true,
                    humanFriendly: true,
                    customSectors: [{
                        color: '#ff0000',
                        lo: 0,
                        hi: TotDemand25Percentage
                    }, {
                        color: '#ff8300',
                        lo: TotDemand25Percentage,
                        hi: TotDemand50Percentage
                    }, {
                        color: '#d0ff00',
                        lo: TotDemand50Percentage,
                        hi: TotDemand75Percentage
                    }, {
                        color: '#00ff00',
                        lo: TotDemand75Percentage,
                        hi: TotDemand100Percentage
                    }],
                });
        };

        var _populateArrearDemandVsCollectionSummary = function (data) {

            $("#arrearGauge").empty();
            $("#arrearGaugeTable").empty();

            $('#arrearGaugeTable').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:40%;">Arrear Demand</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].ArrearDemand).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            $('#arrearGaugeTable').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:40%;">Arrear Collection</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].ArrearCollection).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            var percentage = (Number(data[0].ArrearCollection) * 100 / Number(data[0].ArrearDemand)).toFixed(2);
            var TotDemand25Percentage = ((Number(data[0].ArrearDemand) * 25) / 100).toFixed(2);
            var TotDemand50Percentage = ((Number(data[0].ArrearDemand) * 50) / 100).toFixed(2);
            var TotDemand75Percentage = ((Number(data[0].ArrearDemand) * 75) / 100).toFixed(2);
            var TotDemand100Percentage = ((Number(data[0].ArrearDemand) * 100) / 100).toFixed(2);

            var g1 = "";
            g1 = new JustGage({
                id: "arrearGauge",
                value: data[0].ArrearCollection,
                min: 0,
                max: data[0].ArrearDemand,
                decimals: 2,
                title: "Demand Vs Collection (Arrear)",
                label: "Total Collection  " + percentage + " %",
                gaugeWidthScale: 0.8,
                pointer: true,
                counter: true,
                relativeGaugeSize: true,
                formatNumber: true,
                humanFriendly: true,
                customSectors: [{
                    color: '#ff0000',
                    lo: 0,
                    hi: TotDemand25Percentage
                }, {
                    color: '#ff8300',
                    lo: TotDemand25Percentage,
                    hi: TotDemand50Percentage
                }, {
                    color: '#d0ff00',
                    lo: TotDemand50Percentage,
                    hi: TotDemand75Percentage
                }, {
                    color: '#00ff00',
                    lo: TotDemand75Percentage,
                    hi: TotDemand100Percentage
                }],
            });
        };

        var _populateCurrentDemandVsCollectionSummary = function (data) {

            $("#currentGauge").empty();
            $("#currentGaugeTable").empty();

            $('#currentGaugeTable').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:40%;">Current Demand</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].CurrentDemand).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            $('#currentGaugeTable').append('<tr><td align="right"><span class="glyphicon glyphicon-chevron-right"></span></td><td align="left" class="text-info" style="width:40%;">Current Collection</td><td align="left" class="text-info">' + currencyFormatter.format(Number(data[0].CurrentCollection).toFixed(2), { currency: 'INR' }) + '</td></tr>');
            var percentage = (Number(data[0].CurrentCollection) * 100 / Number(data[0].CurrentDemand)).toFixed(2);
            var TotDemand25Percentage = ((Number(data[0].CurrentDemand) * 25) / 100).toFixed(2);
            var TotDemand50Percentage = ((Number(data[0].CurrentDemand) * 50) / 100).toFixed(2);
            var TotDemand75Percentage = ((Number(data[0].CurrentDemand) * 75) / 100).toFixed(2);
            var TotDemand100Percentage = ((Number(data[0].CurrentDemand) * 100) / 100).toFixed(2);

            var g1 = "";
            g1 = new JustGage({
                id: "currentGauge",
                value: data[0].CurrentCollection,
                min: 0,
                max: data[0].CurrentDemand,
                decimals: 2,
                title: "Demand Vs Collection (Current)",
                label: "Total Collection  " + percentage + " %",
                gaugeWidthScale: 0.8,
                pointer: true,
                counter: true,
                relativeGaugeSize: true,
                formatNumber: true,
                humanFriendly: true,
                customSectors: [{
                    color: '#ff0000',
                    lo: 0,
                    hi: TotDemand25Percentage
                }, {
                    color: '#ff8300',
                    lo: TotDemand25Percentage,
                    hi: TotDemand50Percentage
                }, {
                    color: '#d0ff00',
                    lo: TotDemand50Percentage,
                    hi: TotDemand75Percentage
                }, {
                    color: '#00ff00',
                    lo: TotDemand75Percentage,
                    hi: TotDemand100Percentage
                }],
            });
        };

        var _populateWardWiseDemandVsCollectionWardWiseSummary = function (data, title) {
            var chartDivCtrl = document.getElementById('chrtDemandVSCollectionWardWise');
            
            google.charts.load('current', { packages: ['corechart'] }); 
           
            var chartData = [];            
            function drawChart() {              

                    chartData.push([
                        "WardID",
                        "Total Demand",
                        "Total Collection",
                        "Total Arrear Demand",
                        "Total Arrear Collection",
                        "Total Current Demand",
                        "Total Current Collection",
                    ]);
                   // data = alasql("select * from ?", [data])
                    $.each(
                        data,
                        function (index, value) {
                            chartData.push([
                                value.WardID,
                                value.TotalDemand,
                                value.TotalCollection,
                                value.ArrearDemand,
                                value.ArrearCollection,
                                value.CurrentDemand,
                                value.CurrentCollection,
                            ]);
                        }
                    );    
                
                    var finalChartData = google.visualization.arrayToDataTable(chartData);
                    var options = {
                        title: 'Demand Vs Colection Ward Wise',
                        //isStacked : true,
                        //vAxis: {
                        //    //title: 'Collection',
                        //    format: "decimal"
                        //},
                        //hAxis: { title: 'Ward' },
                        //seriesType: 'bars',
                        //series: { 2: { type: 'line' } },
                        height: 600,
                        width: '100%',
                        chartArea: { width: 900 },
                    };

                    var chart = new google.visualization.ColumnChart(chartDivCtrl);
                    chart.draw(finalChartData, options);               
            }
            google.charts.setOnLoadCallback(drawChart);
        };

        var _loadDemandVsCollectionTotalSummary = function () {
            try {                
                var payload = getRouteNPayload("DemandVsCollection-Summary")();
                post(payload, function (r) {
                    _populateTotalDemandVsCollectionSummary(r.Data);
                    _populateArrearDemandVsCollectionSummary(r.Data);
                    _populateCurrentDemandVsCollectionSummary(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };

        var _loadDemandVsCollectionWardWiseSummary = function () {
            try {
                var payload = getRouteNPayload("DemandVsCollection-WardWiseSummary")();
                post(payload, function (r) {
                    _populateWardWiseDemandVsCollectionWardWiseSummary(r.Data);
                });
            } catch (e) {
                alertify.error(e);
            }
        };

        var _eventHandlers = new function () {
            var _self = this;

            this.onClickBtnGetDetails = function () {
                _loadAppOrigineCollectionSummary();
                _loadWiseCollectionData();
                _loadPaymentFlowByWithTime();
            };
            this.onClick_btnDoorGetData = function () {
                _loadAppOrigineCollectionSummary();
                _loadWiseCollectionData();
                _loadPaymentFlowByWithTime();
            }
            this.onChangeTodate = function (dateText, ctrl) {
                _domControl._txtFromDate._obj.datepicker('option', {
                    maxDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeFromdate = function (dateText, ctrl) {
                _domControl._txtToDate._obj.datepicker('option', {
                    minDate: dateText
                });
                _self.onDateRangeModelChanges();
            };
            this.onChangeDdlTimeSpan = function (e, r) {
                var dateRange = getDefaultDateRange();
                _domControl._txtFromDate._obj.datepicker("setDate", dateRange.FromDate);
                _domControl._txtToDate._obj.datepicker("setDate", dateRange.ToDate);
                $('.ui-datepicker-current-day').click(); //trigger onSelect on date picker, autometically track their won event handler

                //control custom date range visibility
                if (_domControl._ddlTimeSpan._obj.val() == 'date-range') {
                    _domControl._cntrCustomDateRange._obj.show();
                } else {
                    _domControl._cntrCustomDateRange._obj.hide();
                }
                _self.onDateRangeModelChanges(e, r);
            };
            this.onDateRangeModelChanges = function (control, args) {
                var key = null;
                if (args && args.hasOwnProperty("key")) {
                    key = args.key;
                }
                if (key !== "page_load") {
                    _domControl._btnGetDetails._obj.show();
                } else {
                    _domControl._btnGetDetails._obj.hide();
                }
            };

            this.onClick_btnDemandVsCollection = function () {                
                _loadDemandVsCollectionTotalSummary();
                _loadDemandVsCollectionWardWiseSummary();
            }
        };
        var bindEvent = function () {
            _domControl._btnDoorGetData._obj.on('click', _eventHandlers.onClick_btnDoorGetData);
            _domControl._ddlTimeSpan._obj.on('change', _eventHandlers.onChangeDdlTimeSpan);
            _domControl._btnGetDetails._obj.on('click', _eventHandlers.onClickBtnGetDetails);

            _domControl._btnDemandVsCollection._obj.on('click', _eventHandlers.onClick_btnDemandVsCollection);
             

            _domControl._txtFromDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeFromdate,
                maxDate: new Date()
            });
            _domControl._txtToDate._obj.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: _eventHandlers.onChangeTodate,
                maxDate: new Date()
            });
            _domControl._filterBtnMuncipality._obj.on('click', function () {
                municipalityFilter.selectedMunicipalities(MuniciaplityFilter);
                municipalityFilter.applyFilter = function (r) {
                    MuniciaplityFilter = r.MunicipalityIds;
                    municipalityFilter.hide();
                    if (MuniciaplityFilter.length > 0) {
                        $(".municipality-wise-collection-filter").hide();
                    } else {
                        $(".municipality-wise-collection-filter").show();
                    }
                    _loadMuncipalityCollectionSummary();
                    _loadMunicipalityWiseCollectionData();
                };
                municipalityFilter.show();
            })
            $(".ddl-collection-by").on('change', function () {
                _loadWiseCollectionData();
            });
        };
        (function () {
            bindEvent();
            _domControl._ddlTimeSpan._obj.find("option:eq(1)").prop('selected', true);
            _domControl._btnDoorGetData._obj.trigger('click');
        }());
    };
    //additional intiation
    var additionalInit = new function () {
        //remove additional space on left and right side caused by .container class
        $("#DocumentBody > .container").removeClass('container').addClass('container-fluid');
        $('body').css('background-color', 'rgb(245, 245, 245);');
    };
    (function () {

    }())
});