﻿$(document).ready(function () {
    var $Remote = function () {
        return {
            getInstallmentUi: function (data, callback) {
                $.ajax({
                    type: 'post',
                    url: '/Modules/dpr/GetUIApplyFundRelease',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    success: function (response) {
                        callback(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                    }
                });
            },
            applyNewInstallment: function (data, callback) {
                $.ajax({
                    type: 'post',
                    url: '/Modules/dpr/applyNewInstallment',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        callback(response);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                    }
                });
            }
        }
    };
    var chklistFiles = [];
    var SubpProjFiles = [];
    var LetterOrUCdoc = null;
    var NewsPaprAbs = null;
    var $App = function ($rmt) {        
        var params = JSON.parse($(".hidden-settings > .params").val() || null);
        var projctParentSummary = $(".project-summary");
        var approvedProjectAmount = $(".approved-project-amount").val().toNumber();
        var cumulativeAmount = $(".cumulative-amount").val().toNumber();
        var newInstParentSummary = $(".relase-installment");
        var mstDprId = $(".mstdprid").val();
        function getTotalInstallmentAmt() {
            $(".relase-installment").find(".inst-amount").val($(".relase-installment").find(".tbl-sub-projects tbody tr .check-sbp:checked").map(function (i, v) {
                //return $(v).attr('data-cost').toNumber()
                return $(v).closest('tr').find('.inst-due').val().toNumber()
            }).get().reduce(function (a, b) { return a + b; }, 0));
        };
        var initiation = new function () {
            function getBase64(file, name, callback) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    callback({ Result: reader.result, Name: name });
                };
                reader.onerror = function (error) {
                    alertify.error('Unable to read file');
                };
            };            
            $(document).on('change', '.file-letterOrUc', function () {
                LetterOrUCdoc = {};
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        LetterOrUCdoc.StageId = response.Data;
                        LetterOrUCdoc.Result = null;
                        LetterOrUCdoc.Name = null;
                        alertify.success(response.Message);
                    }, function (response) {
                        alertify.error(response.Message);
                        $(this).val('');
                    });
                } 
                //if (this.files.length > 0) {
                //    var file = getBase64(this.files[0], this.files[0].name, function (data) {
                //        LetterOrUCdoc = data;
                //        $.ajax({
                //            type: 'post',
                //            url: '/Modules/dpr/UploadImage',
                //            data: JSON.stringify(LetterOrUCdoc),
                //            contentType: "application/json; charset=utf-8",
                //            dataType: "json",
                //            success: function (response) {
                //                if (response.Status == 200) {
                //                    LetterOrUCdoc.StageId = response.Data;
                //                    LetterOrUCdoc.Result = null;
                //                    LetterOrUCdoc.Name = null;
                //                }
                //                else {
                //                    alertify.alert(response.Message);
                //                }
                //            },
                //            error: function (xhr, ajaxOptions, thrownError) {

                //            }
                //        });
                //    });
                //}
            });
            //$(document).on('change', '.file-newsPaprAbsPg', function () {
            //    NewsPaprAbs = null;
            //    if (this.files.length > 0) {
            //        var file = getBase64(this.files[0], this.files[0].name, function (data) {
            //            NewsPaprAbs = data;
            //        });
            //    }
            //});
            $(document).on('keyup', '.inst-due', function () {
                if ($(this).val().toNumber() > $(this).closest('tr').attr('data-due').toNumber()) {
                    alertify.alert("Installment Amount can not be greater than Project Amount");
                    $(this).val($(this).closest('tr').attr('data-cost'));
                }
                else
                    getTotalInstallmentAmt();
            });
            newInstParentSummary.on('change', ".box-installment .file-checklist-file", function () {
                var id = $(this).attr('check-id');
                var checkFile = chklistFiles.filter(function (v) { return v.id == id; });
                if (this.files.length > 0) {
                    window.StageFileByFormData(this, function (response) {
                        checkFile[0].StageId = response.Data;
                        checkFile[0].Result = null;
                        checkFile[0].Name = null;
                        alertify.success(response.Message);
                    }, function (response) {
                        alertify.error(response.Message);
                        $(this).val('');
                    });
                } 
                //if (this.files.length > 0 && checkFile.length > 0) {
                //   // checkFile[0].isReadComplete = false;
                //    getBase64(this.files[0], this.files[0].name, function (d) {
                //        checkFile[0].file = d;
                //        $.ajax({
                //            type: 'post',
                //            url: '/Modules/dpr/UploadImage',
                //            data: JSON.stringify(checkFile[0]),
                //            contentType: "application/json; charset=utf-8",
                //            dataType: "json",
                //            success: function (response) {
                //                checkFile[0].StageId = response.Data;
                //                checkFile[0].file = null;
                //            },
                //            error: function (xhr, ajaxOptions, thrownError) {

                //            }
                //        });
                //       // checkFile[0].isReadComplete = true;
                //    });
                //}
                //if (this.files.length <= 0 && checkFile.length > 0) {
                //    checkFile[0].file = null;
                //    checkFile[0].isReadComplete = true;
                //}
            });
            newInstParentSummary.on('change', ".box-installment .subproj-file", function () {
                var id = $(this).attr('subproj-id');
                var checkFile = SubpProjFiles.filter(function (v) { return v.id == id; });
                if (this.files.length > 0 && checkFile.length > 0) {
                    checkFile[0].isReadComplete = false;
                    getBase64(this.files[0], this.files[0].name, function (d) {
                        checkFile[0].file = d;
                        checkFile[0].isReadComplete = true;
                    });
                }
                if (this.files.length <= 0 && checkFile.length > 0) {
                    checkFile[0].file = null;
                    checkFile[0].isReadComplete = true;
                }
            });
            projctParentSummary.find(".btn-installment-ui").on('click', function () {
                if (!$(this).closest('form').valid())
                    return;
                var getRequestedInstallmentID = projctParentSummary.find(".select-new-installment").val();
                $rmt.getInstallmentUi({ mstDprId: mstDprId, cmd: 'inst-ui', params: { installmentId: getRequestedInstallmentID } }, function (r) {
                    newInstParentSummary.empty();
                    newInstParentSummary.append(r);
                    if (getRequestedInstallmentID > 1) {
                        $('.hideshowAbsPg').hide();
                        //$('.file-newsPaprAbsPg').removeAttr("required"); 
                        //$('.file-newsPaprAbsPg').change();
                    }
                    else {
                        $('.hideshowAbsPg').show();
                    }
                    chklistFiles = newInstParentSummary.find(".box-installment .file-checklist-file")
                        .map(function (i, v) {
                            return { file: null, isReadComplete: true, id: $(v).attr("check-id") }
                        }).get();
                    SubpProjFiles = newInstParentSummary.find(".box-installment .subproj-file")
                        .map(function (i, v) {
                            return { file: null, isReadComplete: true, id: $(v).attr("subproj-id") }
                        }).get();
                });
            });
        };
        
        var applyInstallMent = new function () {
            var boxInstallment = newInstParentSummary.find(".box-installment");
            var getFormData = function () {
                return {
                    MstDprId: $('.mstdprid').val().toNumber(),
                    InstallmentId: newInstParentSummary.find('.installment-id').val().toNumber(),
                    MemoNo: newInstParentSummary.find(".memo-no").val(),
                    MemoDate: newInstParentSummary.find(".memo-date").val().toDate("dd/mm/yyyy"),
                    InstallmentAmount: newInstParentSummary.find(".inst-amount").val().isNanThenZero().toNumber(),
                    StartDate: null,
                    EndDate: null,
                    ApplyCheckList: newInstParentSummary.find(".inst-check-list").map(function (i, v) {
                        return { IsChecked: $(v).prop('checked'), ChkId: $(v).val() }
                    }).get(),
                    ApplyCheckListFiles: chklistFiles,
                    LetterOrUc: LetterOrUCdoc,
                    SubProjects: $(".tbl-sub-projects tbody .check-sbp:checked")
                        .map(function (i, v) { return { Id: $(v).closest('tr').attr('data-id').toNumber(), InstAmt: $(v).closest('tr').find('.inst-due').val().toNumber()  } }).get(),
                    SubpProjFiles: SubpProjFiles,
                   // NewsPaprAbs: NewsPaprAbs,
                }
            }
            var validateRequestedAmount = function (formData) {
                //<input type="hidden" class="approved-project-amount" value="@Model.ApprovedProjectCost" />
                //    <input type="hidden" class="cumulative-amount" value="@Model.TotalCumulativeAmount" />
                var installmentAmount = formData.InstallmentAmount;
                if (installmentAmount.isPositive() == false) {
                    throw "Installment amount is not a positive number";
                }
                if (cumulativeAmount + installmentAmount > approvedProjectAmount) {
                    throw "Installment Amount can not be greater than Project Amount";
                }
            }
            $(document).on('change', ".check-sbp:not(:disabled)", function () {
                getTotalInstallmentAmt();
                if ($(this).closest('tr').find('.check-sbp').prop('checked') == true)
                {
                    $(this).closest('tr').find('.subproj-file').attr("disabled", false);
                    $(this).closest('tr').find('.inst-due').attr("disabled", false);
                }
                if ($(this).closest('tr').find('.check-sbp').prop('checked') == false)
                {
                    $(this).closest('tr').find('.subproj-file').attr("disabled", true);
                    $(this).closest('tr').find('.subproj-file').val('').change();
                    $(this).closest('tr').find('.inst-due').attr("disabled", true);
                    $(this).closest('tr').find('.inst-due').val($(this).closest('tr').attr('data-due'));
                }
            });
            newInstParentSummary.on('click', ".btn-apply-installment", function (e) {
                e.preventDefault();
                if (!$(this).closest('form').valid())
                    return;
                var formData = getFormData();
                if (formData.ApplyCheckList.filter(t => t.IsChecked === true).length == 0) {
                    showError("Please select check list");
                    return;
                }
                try {
                    validateRequestedAmount(formData)
                } catch (ex) {
                    showError(ex);
                    return;
                }
                $rmt.applyNewInstallment(getFormData(), function (r) {
                    if (r.Status === 200) {
                        showSuccess(r.Message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000)
                    } else {
                        showError(r.Message);
                    }
                });
            });
            newInstParentSummary.on('focus', ".memo-date", function () {
                $(this).datepicker({
                    "dateFormat": 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                });
            });
            newInstParentSummary.on("click", ".clear-data", function () {
                var tergate = $(this).siblings("." + $(this).attr('data-clean-ctrl'));
                if (tergate.length > 0) {
                    tergate.val('');
                }
            })
            var showError = function (msg) {
                newInstParentSummary.find('.show-msg').children('.alert').hide();
                newInstParentSummary.find('.show-msg').children('.alert-danger').empty().append('<strong>Error!</strong> ' + msg + '.').show();
            }
            var showSuccess = function (msg) {

                newInstParentSummary.find('.show-msg').children('.alert').hide();
                newInstParentSummary.find('.show-msg').children('.alert-success').empty().append('<strong>Success!</strong> ' + msg + '.').show();
            }
        };
    };
    var $Init = new function () {
        var _rmt = new $Remote();
        var _app = new $App(_rmt);
    };
});