﻿
$(document).ready(function () {

    Bind_Date();
    AutoComplete_HoldingNo();

    $("#ddlMunicipality").change(function (evt) {
        Bind_Ward();
    });
    $("#ddlWard").change(function (evt) {
        Bind_Location('');
    });
    $("#btnAdd").click(function (evt) {
        Add();
    });
    $("#btnSave").click(function (evt) {
        Save();
    });
    $("#btnRefresh").click(function (evt) {
        Refresh();
    });

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    var ApplicationId = getParameterByName("ApplicationId", window.location.href); //alert(ApplicationId);
    var mode = getParameterByName("mode", window.location.href); //alert(mode);
    if (ApplicationId != null)
        Load(ApplicationId, mode);

    if (mode != null) {
        if (mode == 'E') {
            $("#btnSave").css('display', '');
            $("#btnAdd").css('display', '');
        }
        else if (mode == 'V') {
            $("#btnSave").css('display', 'none');
            $("#btnAdd").css('display', '');
        }
    }
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function Bind_Date() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();

    $('#txtStartofWork, #txtEndofWork').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            //$("#txtEndofWork").datepicker("option", "maxDate", $("#txtStartofWork").val());
            $("#txtEndofWork").datepicker("option", "minDate", $("#txtStartofWork").val());
        }
    });


    $("#txtStartofWork").datepicker("option", "minDate", output);
}
function Bind_Ward() {
    if ($("#ddlMunicipality").val() != "") {
        var E = "{MunicipalityID: '" + $("#ddlMunicipality").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/MobileTower/Bind_Ward',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlWard').empty();
                $('#ddlWard').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlWard').append('<option value=' + item.WardID + '>' + item.WardName + '</option>');
                        });
                    }
                }
            }
        });
    }
}
function Bind_Location(LocationId) {
    if ($("#ddlWard").val() != "") {
        var E = "{WardID: '" + $("#ddlWard").val() + "', MunicipalityID: '" + $("#ddlMunicipality").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/MobileTower/Bind_LocationByWard',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlLocation').empty();
                $('#ddlLocation').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlLocation').append('<option value=' + item.LocationID + '>' + item.LocationName + '</option>');
                        });
                    }
                    if (LocationId != "") $('#ddlLocation').val(LocationId);
                }
            }
        });
    }
}
function AutoComplete_HoldingNo() {
    $('#txtHoldingNo').autocomplete({
        source: function (request, response) {

            var GLS = "S";

            var S = "{MunicipalityID:'" + $('#ddlMunicipality').val() + "', Desc:'" + $('#txtHoldingNo').val() + "', WardID:'" + $('#ddlWard').val() + "',  LocationID:'" + $('#ddlLocation').val() + "'}"; //alert(S);
            $.ajax({
                url: '/OtherServices/MobileTower/HoldingNo',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = []; //alert(JSON.stringify(serverResponse.Data));

                    if (serverResponse.Data == null) {
                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.HoldingNo,
                                AssesseeID: item.AssesseeID,
                                holding: item.holding
                            });
                        });

                        response(AutoComplete);
                    }
                    else {
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.HoldingNo,
                                    AssesseeID: item.AssesseeID,
                                    holding: item.holding
                                });
                            });

                            response(AutoComplete);
                        }
                    }

                }
            });
        },
        select: function (e, i) {
            $('#hdnAssesseeNo').val(i.item.AssesseeID);
            $('#hdnHoldingNo').val(i.item.holding);
        },
        minLength: 0
    }).click(function () {
        if ($('#ddlMunicipality').val() == "") {
            $('#ddlMunicipality').focus();
            return false;
        }
        if ($('#ddlWard').val() == "") {
            $('#ddlWard').focus();
            return false;
        }
        if ($('#ddlLocation').val() == "") {
            $('#ddlLocation').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#txtHoldingNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtHoldingNo").val('');
            $("#hdnHoldingNo").val('');
            $('#hdnHoldingNo').val('');
        }
        if (iKeyCode == 46) {
            $("#txtHoldingNo").val('');
            $("#hdnHoldingNo").val('');
            $('#hdnHoldingNo').val('');
        }
    });
}

function Add() {

    if ($("#txtLatitudeStart").val() == "") { $("#txtLatitudeStart").focus(); return false; }
    if ($("#txtLatitudeEnd").val() == "") { $("#txtLatitudeEnd").focus(); return false; }
    if ($("#txtLongitudeStart").val() == "") { $("#txtLongitudeStart").focus(); return false; }
    if ($("#txtLongitudeEnd").val() == "") { $("#txtLongitudeEnd").focus(); return false; }


    if ($("#ddlWard").val() == "") { $("#ddlWard").focus(); return false; }
    if ($("#ddlLocation").val() == "") { $("#ddlLocation").focus(); return false; }
    if ($("#hdnHoldingNo").val() == "") { $("#txtHoldingNo").focus(); return false; }
    if ($("#txtPlotArea").val() == "") { $("#txtPlotArea").focus(); return false; }
    if ($("#txtStartofWork").val() == "") { $("#txtStartofWork").focus(); return false; }
    if ($("#txtEndofWork").val() == "") { $("#txtEndofWork").focus(); return false; }
    if ($("#txtTowerArea").val() == "") { $("#txtTowerArea").focus(); return false; }
    if ($("#txtTowerHeight").val() == "") { $("#txtTowerHeight").focus(); return false; }
    if ($("#txtTowerVolumn").val() == "") { $("#txtTowerVolumn").focus(); return false; }

    var html = ""
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsDetailID' >" + $("#hdnDetailID").val() + "</td>"
        + "<td style='display:none;' class='clsStatus' chk-dtl='" + $("#hdnDetailID").val() + "'>" + 1 + "</td>"

        + "<td>LS: <span class='clsLatStart'>" + $("#txtLatitudeStart").val() + "</span><br> LE: <span class='clsLatEnd'>" + $("#txtLatitudeEnd").val() + "</span></td>"
        + "<td>LS: <span class='clsLongStart'>" + $("#txtLongitudeStart").val() + "</span><br> LE: <span class='clsLongEnd'>" + $("#txtLongitudeEnd").val() + "</span></td>"


        + "<td class='clsBorough'>" + $("#txtBorough").val() + "</td>"
        + "<td style='display:none;' class='clsWard'  >" + $("#ddlWard").val() + "</td>"
        + "<td>" + ($("#ddlWard").val() == "" ? "" : $("#ddlWard option:selected").text()) + "</td>"

        + "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation").val() + "' >" + $("#ddlLocation").val() + "</td>"
        + "<td>" + ($("#ddlLocation").val() == "" ? "" : $("#ddlLocation option:selected").text()) + "</td>"

        + "<td class='clsLandMark'>" + $("#txtLandMark").val() + "</td>"
        + "<td class='clsHoldingNo'>" + $("#txtHoldingNo").val() + "</td>"
        + "<td class='clshdnHoldingNo' style='display:none;'>" + $("#hdnHoldingNo").val() + "</td>"
        + "<td class='clshdnAssesseeNo' style='display:none;'>" + $("#hdnAssesseeNo").val() + "</td>"
        + "<td class='clsPlotArea'>" + $("#txtPlotArea").val() + "</td>"

        + "<td class='clsStartofWork'>" + $("#txtStartofWork").val() + "</td>"
        + "<td class='clsEndofWork'>" + $("#txtEndofWork").val() + "</td>"

        + "<td class='clsTowerArea'>" + $("#txtTowerArea").val() + "</td>"
        + "<td class='clsTowerHeight'>" + $("#txtTowerHeight").val() + "</td>"
        + "<td class='clsTowerVolumn'>" + $("#txtTowerVolumn").val() + "</td>"

        + "<td style='text-align:center'>"
        + "<img src= '/Contents/image/edit.png' title= 'Edit' style= 'width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'EditDetail(this);' />"
        + "</br><img src= '/Contents/image/delete.png' class='clsDelButton' title= 'Delete' style= 'width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'DeleteDetail(this);' /></td >"
    html + "</tr>";


    var dtlID = $("#hdnDetailID").val();
    if (dtlID != "") {
        $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").text(0);
        $('#tbl tr.myData').find("td[chk-dtl='" + dtlID + "']").closest('tr').css('display', 'none');
    }
    $parentTR.after(html);

    $("#txtLongitudeStart").val(''); $("#txtLongitudeEnd").val(''); $("#txtLatitudeStart").val(''); $("#txtLatitudeEnd").val('');
    $("#ddlWard").val(''); $("#ddlLocation").val(''); $("#txtStartofWork").val('');
    $("#txtLandMark").val(''); $("#txtBorough").val(''); $("#txtPlotArea").val('');
    $("#txtEndofWork").val(''); $("#txtHoldingNo").val(''); $("#hdnHoldingNo").val(''); $("#hdnAssesseeNo").val('');
    $("#txtTowerArea").val(''); $("#txtTowerHeight").val(''); $("#txtTowerVolumn").val('');

    $("#hdnDetailID").val(''); $("#hdnStatus").val('');

}
function EditDetail(ID) {
    $("#hdnDetailID").val($(ID).closest('tr').find('.clsDetailID').text());
    $("#hdnStatus").val($(ID).closest('tr').find('.clsStatus').text());

    $("#txtLongitudeStart").val($(ID).closest('tr').find('.clsLongStart').text());
    $("#txtLongitudeEnd").val($(ID).closest('tr').find('.clsLongEnd').text());
    $("#txtLatitudeStart").val($(ID).closest('tr').find('.clsLatStart').text());
    $("#txtLatitudeEnd").val($(ID).closest('tr').find('.clsLatEnd').text());

    $("#ddlWard").val($(ID).closest('tr').find('.clsWard').text()); Bind_Location($(ID).closest('tr').find('.clsLocation').text());
    $("#ddlLocation").val($(ID).closest('tr').find('.clsLocation').text());
    $("#txtLandMark").val($(ID).closest('tr').find('.clsLandMark').text());
    $("#txtBorough").val($(ID).closest('tr').find('.clsBorough').text());

    $("#txtStartofWork").val($(ID).closest('tr').find('.clsStartofWork').text());
    $("#txtEndofWork").val($(ID).closest('tr').find('.clsEndofWork').text());
    $("#txtPlotArea").val($(ID).closest('tr').find('.clsPlotArea').text());
    $("#txtHoldingNo").val($(ID).closest('tr').find('.clsHoldingNo').text());
    $("#hdnHoldingNo").val($(ID).closest('tr').find('.clshdnHoldingNo').text());
    $("#hdnAssesseeNo").val($(ID).closest('tr').find('.clshdnAssesseeNo').text());

    $("#txtTowerArea").val($(ID).closest('tr').find('.clsTowerArea').text());
    $("#txtTowerHeight").val($(ID).closest('tr').find('.clsTowerHeight').text());
    $("#txtTowerVolumn").val($(ID).closest('tr').find('.clsTowerVolumn').text());

    var detailID = $(ID).closest('tr').find('.clsDetailID').text();
    if (detailID == "")
        $(ID).closest('tr').remove();
}
function DeleteDetail(ID) {
    var res = confirm('Are you sure want to delete ?');
    if (res == true) {
        //$(ID).closest('tr').remove();
        var dtlID = $(ID).closest('tr').find(".clsDetailID").text();
        if (dtlID == "") { $(ID).closest('tr').remove(); return false; }
        $(ID).closest('tr').find(".clsStatus").text(0);
        $(ID).closest('tr').css('display', 'none');
    }


}

function Save() {

    var ArrList = [];
    var municipalityID = $("#ddlMunicipality").val();
    var purposeID = $("#ddlPurpose").val();
    var projectName = $("#txtProjectName").val();

    if (municipalityID == "") { alert('Please Select Project Municipality/Corporation.'); $("#ddlMunicipality").focus(); return false; }
    if (purposeID == "") { alert('Please Select Purpose.'); $("#ddlPurpose").focus(); return false; }
    if (projectName == "") { alert('Please Enter Name of project.'); $("#txtProjectName").focus(); return false; }

    //var l = $('#tbl tr.myData').length; 
    //if (l == 0) {
    //    alert('Please Add Project details.'); return false;
    //}

    //var imgCnt = 0, ismaxCnt = 0, qty = 0, docName = "";
    //$('#tblimg tbody tr.myData').each(function () {

    //    var Id = $(this).find('.flimg').attr('id');
    //    var isMand = $(this).find('.flimg').attr('mandatory');
    //    var maxqty = $(this).find('.flimg').attr('maxQuantity'); var restmaxqty = $(this).find('.flimg').attr('maxQuantity');
    //    var doctName = $(this).find('.flimg').attr('docName');
    //    var doctid = $(this).find('.flimg').attr('doctypeid');
    //    var l = $('#tblimg tr.img_' + doctid).find('tr').length; //alert(l); //return false;

    //    if (isMand == 1) {
    //        if (maxqty > 0) {
    //            var totalFiles = document.getElementById(Id).files.length + l;
    //            if (totalFiles == 0) {
    //                imgCnt = 1;
    //                docName = doctName;
    //            }
    //            if ((totalFiles > (maxqty)) || (totalFiles < (maxqty))) {
    //                ismaxCnt = 1;
    //                docName = doctName;
    //                qty = maxqty;
    //            }
    //        }
    //    }
    //});
    //if (imgCnt == 1) {
    //    alert('Sorry ! Please Select File/Image for ' + docName); return false;
    //}
    //if (ismaxCnt == 1) {
    //    alert('Sorry ! Please Select ' + qty + ' File/Image ' + 'for ' + docName); return false;
    //}

    $('#tbl tbody tr.myData').each(function () {

        var clsDetailID = $(this).find('.clsDetailID').text();
        var clsStatus = $(this).find('.clsStatus').text();

        var clsLongStart = $(this).find('.clsLongStart').text();
        var clsLongEnd = $(this).find('.clsLongEnd').text();
        var clsLatStart = $(this).find('.clsLatStart').text();
        var clsLatEnd = $(this).find('.clsLatEnd').text();

        var clsWard = $(this).find('.clsWard').text();
        var clsLocation = $(this).find('.clsLocation').text();
        var clsLandMark = $(this).find('.clsLandMark').text();
        var clsBorough = $(this).find('.clsBorough').text();
        var clsStartofWork = $(this).find('.clsStartofWork').text();
        var clsEndofWork = $(this).find('.clsEndofWork').text();

        var clsPlotArea = $(this).find('.clsPlotArea').text();
        var clsHoldingNo = $(this).find('.clshdnHoldingNo').text();
        var clshdnAssesseeNo = $(this).find('.clshdnAssesseeNo').text();

        var clsTowerArea = $(this).find('.clsTowerArea').text();
        var clsTowerHeight = $(this).find('.clsTowerHeight').text();
        var clsTowerVolumn = $(this).find('.clsTowerVolumn').text();

        ArrList.push({
            'DetailID': clsDetailID, 'Status': clsStatus,
            'LongStart': clsLongStart, 'LongEnd': clsLongEnd, 'LatStart': clsLatStart, 'LatEnd': clsLatEnd,

            'Ward': clsWard, 'Location': clsLocation, 'LandMark': clsLandMark, 'BoroughNo': clsBorough, 'StartofWork': clsStartofWork, 'EndofWork': clsEndofWork,

            'PlotArea': clsPlotArea, 'HoldingNo': clsHoldingNo, 'AssesseeNo': clshdnAssesseeNo, 'TowerArea': clsTowerArea, 'TowerHeight': clsTowerHeight, 'TowerVolumn_CM': clsTowerVolumn,
        });
    });

    //alert(JSON.stringify(ArrList));
    //return false;
    var E = "{ApplicationID: '" + $('#hdnApplicationID').val() + "' , municipalityID: '" + municipalityID + "', purposeID: '" + purposeID + "', projectName: '" + projectName + "', obj: " + JSON.stringify(ArrList) + "}";

    $.ajax({
        url: '/OtherServices/MobileTower/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");

            var MSGID = $.trim($(Table).find("MSGID").text());
            var MSG = $.trim($(Table).find("MSG").text());

            var ApplicationID = $.trim($(Table1).find("ApplicationID").text());
            var SubModuleID = $.trim($(Table1).find("SubModuleID").text());

            if (MSGID == 1) {
                Upload_Doc(ApplicationID, SubModuleID);
                alert(MSG);
                location.reload();
            }
            else {
                alert('Sorry ! There is some Problem.');
                return false;
            }
        }
    });
}
function Upload_Doc(ApplicationID, SubModuleID) {

    $('#tblimg tbody tr.myData').each(function () {


        var Id = $(this).find('.flimg').attr('id');
        var isMand = $(this).find('.flimg').attr('mandatory');
        var maxqty = $(this).find('.flimg').attr('maxQuantity');
        var doctName = $(this).find('.flimg').attr('docName');
        var docTypeId = $(this).find('.flimg').attr('docTypeId');

        var totalFiles = document.getElementById(Id).files.length; //alert(totalFiles);
        if (totalFiles > 0) {
            for (var i = 0; i < totalFiles; i++) {
                var formData = new FormData();
                var file = document.getElementById(Id).files[i];
                fileName = $('#' + Id).val().substring(12);
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
                formData.append(Id, file, docTypeId + "#" + ApplicationID + "#" + SubModuleID + "#" + (parseInt(i) + 1) + fileNameExt);

                $.ajax({
                    type: "POST",
                    url: '/OtherServices/MobileTower/Upload',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {
                    }
                });
            }
        }
    });
}
function Refresh() {
    location.reload();
}

function Load(ApplicationId, mode) {
    var E = "{ApplicationID: '" + ApplicationId + "'}";
    $.ajax({
        url: '/OtherServices/MobileTower/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");
            var Table2 = xml.find("Table2");

            var ApplicationID = $.trim($(Table).find("ServMstId").text());
            var ApplicationNo = $.trim($(Table).find("ApplicationNo").text());
            var MunicipalityID = $.trim($(Table).find("MunicipalityDevAuthoId").text());
            var PurposeID = $.trim($(Table).find("PurposeOfService").text());
            var ProjectName = $.trim($(Table).find("NameOfProject").text());

            //MASTER 
            $("#hdnApplicationID").val(ApplicationID); $("#txtApplicationNo").val(ApplicationNo);
            $("#ddlMunicipality").val(MunicipalityID); Bind_Ward();
            $("#ddlPurpose").val(PurposeID);
            $("#txtProjectName").val(ProjectName);
            //DETAIL
            Detail(xml, mode);
            //DOCUMENT
            Document(xml, mode);
        }
    });
}
function Detail(xml, mode) {
   
    $("#tbl tr.myData").remove();
    var html = "";
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');
    $(xml).find("Table1").each(function () {

        var isActive = $.trim($(this).find("isActive").text());

        $parentTR.after("<tr class='myData' style='display:" + (isActive == 1 ? "" : "none") + "'>" +
            "<td style='display:none;' class='clsDetailID' >" + $.trim($(this).find("ServDatId").text()) + "</td>" +
            "<td style='display:none;' class='clsStatus' chk-dtl='" + $.trim($(this).find("ServDatId").text()) + "'>" + $.trim($(this).find("isActive").text()) + "</td>" +

            "<td>LS: <span class='clsLatStart'>" + $.trim($(this).find("LatStart").text()) + "</span><br> LE: <span class='clsLatEnd'>" + $.trim($(this).find("LatEnd").text()) + "</span></td>" +
            "<td>LS: <span class='clsLongStart'>" + $.trim($(this).find("LongStart").text()) + "</span><br> LE: <span class='clsLongEnd'>" + $.trim($(this).find("LongEnd").text()) + "</span></td>" +


            "<td class='clsBorough'>" + $.trim($(this).find("BoroughNo").text()) + "</td>" +
            "<td style='display:none;' class='clsWard' >" + $.trim($(this).find("WardNo").text()) + "</td>" +
            "<td>" + $.trim($(this).find("WardName").text()) + "</td>" +

            "<td style='display:none;' class='clsLocation' chk-data='" + $("#ddlLocation").val() + "'>" + $.trim($(this).find("LocationId").text()) + "</td>" +
            "<td>" + $.trim($(this).find("LocationName").text()) + "</td>" +

            "<td class='clsLandMark'>" + $.trim($(this).find("LandMark").text()) + "</td>" +
            "<td class='clsHoldingNo'>" + $.trim($(this).find("HoldingNo").text()) + "</td>" +
            "<td class='clshdnHoldingNo' style='display:none;'>" + $.trim($(this).find("HoldingID").text()) + "</td>" +
            "<td class='clshdnAssesseeNo' style='display:none;'>" + $.trim($(this).find("AssesseeNo").text()) + "</td>"+
            "<td class='clsPlotArea'>" + $.trim($(this).find("PlotArea").text()) + "</td>" +

            "<td class='clsStartofWork'>" + $.trim($(this).find("WorkStratDate").text()) + "</td>" +
            "<td class='clsEndofWork'>" + $.trim($(this).find("WorkEndDate").text()) + "</td>" +

            "<td class='clsTowerArea'>" + $.trim($(this).find("TowerArea").text()) + "</td>" +
            "<td class='clsTowerHeight'>" + $.trim($(this).find("TowerHight").text()) + "</td>" +
            "<td class='clsTowerVolumn'>" + $.trim($(this).find("TowerVolumn_CM").text()) + "</td>" +

            "<td style='text-align:center'>" +
            "<img src= '/Contents/image/edit.png' title= 'Edit' style= 'width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'EditDetail(this);' />" +
            "</br><img src= '/Contents/image/delete.png' style='display:" + (mode == "V" ? "none" : "") + "' title= 'Delete' style= 'width:15px;height:15px;cursor:pointer; text-align:center;' onClick= 'DeleteDetail(this);' /></td >" +
            "</tr > ");
    });
}
function Document(xml, mode) {

    var html = "";
    $("#tblimg tr.docImg td").remove();

    $(xml).find("Table2").each(function () {

        var doctypeid = $.trim($(this).find("DocTypeID").text());
        var docid = $.trim($(this).find("DocId").text());


        html += "<tr><td style='width:80%; padding:5px 0px 0px 5px'>" + $.trim($(this).find("DocPath").text()) + "</td>";
        html += "<td style='display:none;' class='cslUrl'>" + $.trim($(this).find("MainUrl").text()) + "</td>";
        html += "<td style='display:none;' class='clsdocid'>" + $.trim($(this).find("ServDocId").text()) + "</td>";
        html += "<td style='width:30%; padding:5px 0px 0px 5px'><img src= '/Contents/image/edit.png' title= 'View' style= 'width:15px;height:15px;cursor:pointer; margin-left:10px; text-align:center;' onClick= 'ViewImg(this);' />";
        html += "&nbsp; &nbsp;<img src= '/Contents/image/delete.png' style='display:" + (mode == "V" ? "none" : "") + "' title= 'Delete' style= 'width:15px;height:15px;cursor:pointer; text-align:center; margin-left:10px' onClick= 'DeleteImg(this);' /></td ></tr>";

        //alert(html);
        $("#tblimg tr.img_" + doctypeid).append(html);
        html = "";
    });

}
function DeleteImg(ID) {
    var res = confirm('Are you sure want to delete image ?');
    if (res == true) {
        var docId = $(ID).closest('tr').find('.clsdocid').text();

        var E = "{DocID: '" + docId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/MobileTower/DeleteImg',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (responce) {
                var xmlDoc = $.parseXML(responce.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");

                var Result = $.trim($(Table).find("Result").text());
                if (Result == 1) $(ID).closest('tr').remove();
            }
        });
    }
}
function ViewImg(ID) {
    var url = $(ID).closest('tr').find('.cslUrl').text(); //alert(url);
    $('#imgview').attr('src', url);
    $('#dialog').modal('show');
}
//PHOTO
function Photo(ID) {
    var Id = $(ID).attr('id');
    var maxqty = $(ID).attr('maxQuantity'); var restmaxqty = $(ID).attr('maxQuantity');
    var doctid = $(ID).attr('doctypeid');
    var l = $('#tblimg tr.img_' + doctid).find('tr').length;
    maxqty = maxqty - l;

    if (maxqty > 0) {
        var totalFiles = document.getElementById(Id).files.length;
        if (totalFiles > maxqty) {
            alert('Sorry ! You can select only ' + restmaxqty + " files.");
            $('#' + Id).val("");
            return false;
        }
    }
    else {
        alert('Sorry ! You can not select more than ' + restmaxqty + " files.");
        $('#' + Id).val("");
        return false;
    }

}

