﻿$(document).ready(function () {
    var server = {
        loadDistrict: function (clbak) {
            $.ajax({
                url: "/shared/knowurprop/GetDistrictsNic",
                method: "post",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    clbak(r);
                },
                error: function (r) {
                    alertify.error(r);
                }
            });
        },
        loadBlock: function (data, clbak) {
            $.ajax({
                url: "/shared/knowurprop/GetBlockNic",
                method: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    clbak(r);
                },
                error: function (r) {
                    alertify.error(r);
                }
            });
        },
        loadMouza: function (data, clbak) {
            $.ajax({
                url: "/shared/knowurprop/GetMouzaNic",
                method: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    clbak(r);
                },
                error: function (r) {
                    alertify.error(r);
                },
            });
        },
        loadLandDetails: function (data, clback) {
            $.ajax({
                url: "/shared/knowurprop/FetchInfo",
                method: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    clback(r);
                },
                error: function (r) {
                    alertify.error(r);
                },
            });
        },
        loadSearchNameProperty: function (data, clback) {
            $.ajax({
                url: "/shared/knowurprop/SearchService_ForName",
                method: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    clback(r);
                },
                error: function (r) {
                    alertify.error(r);
                }
            });
        },
        loadMstDistrict : function (callback) {
            $.ajax({
                type: "POST",
                url: "/Shared/District/GetDistricts",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        },
        loadMunicipality : function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Shared/Municipality/GetMunicipalitesByDistrictID",
                data: "{DistrictID:" + data + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        },
        loadSearchAssesseesByHolding : function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Assesse/GetAssesseesByHoldingNoForKnowYoueProps",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        loadGetAssesseeByID : function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Assesse/GetAssesseeByID",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        loadGetPaymentDetails : function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Assesse/Get_PropertyTaxPaymentDetails",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
    };
    var ctrls = {
        lstdist: ".lst-ditrict",
        lstblock: ".lst-block",
        lstmouza: ".lst-mouza",
        rdioSrchBy: ".rdio-searchby",
        srchText: ".txt-seachby",
        btnView: ".btn-view"
    };
    var getCourtCaseLinkTemplate = function ( khatian, plot) {
        var idn = $(".lst-mouza").val();
        var strCourtCase = "<td class='text-center'><a href='javascript:void(0);' class='show-court-case' data-idn='" + idn + "' data-khatian='" + khatian + "' data-plot='" + plot + "'>Detail</a></td>";
        return strCourtCase;
    };
    var showOutStanding = function (data) {
        var cntr = $(".plc-2nd");
        cntr.empty();
        if (data && (data.length === 0 || (data.length > 0 && data[0]["ERROR"] == 1))) {
            cntr.append('<h3 class="text-center"></h3>');
            return;
        }
        var table = "";
        table += "<div class='table-responsive'>";
        table += "<table class='table table-bordered table-striped table-hover table-condensed'>";
        table += "<thead style='background-color:#2F5B93;color:white;text-align: center;'>";
        table += "<tr><th colspan='8' class='text-center'>Property Details</th></tr>";
        table += "<tr>";
        table += "<th><center><b>Municipality Name</center></th><th><center>Ward</center></th><th><center>Holding No</center></th><th><center>Asseesse Name</center><th><center>Plot</center></th><th><center>Khatian</center></th><th><center>Tax Outstanding (INR)</center></th><th><center>Deed No</center></th>";
        table += "</tr>";
        table += "</thead>";
        table += "<tbody><tr class='text-center'><td colspan='8'></td></tr></tbody>";
        table += "<tfoot></tfoot>";
        table += "</table>";
        table += "</div>";
        table = $(table);
        if (data && data.length >= 0) {
            table.find('tbody').empty();
        }
        var getDeedText = function (deedNo, deedYr, distCode, roCode) {
            var str = "";
            var isClickable = true;
            if (deedNo) {
                str += "Deed no: " + deedNo;
            } else {
                isClickable = false;
            }
            if (deedYr) {
                str += "<br/>";
                str += "Deed Year: " + deedYr;
            } else {
                isClickable = false;
            }

            if (distCode) {
                str += "<br/>";
                str += "Dist Code: " + distCode;
            } else {
                isClickable = false;
            }

            if (roCode) {
                str += "<br/>";
                str += "Ro Code: " + roCode;
            } else {
                isClickable = false;
            }
            return {
                str: str, isClickable: isClickable
            };
        };
        $.each(data, function (i, v) {
            var deedtxt = getDeedText(v.deed_no, v.deed_year, v.dist_code, v.ro_code);
            var searchPropNameAnchor = deedtxt.isClickable ? '<a href="javascript:void(0);" class="search-name-property">' + deedtxt.str + '</a>' : deedtxt.str;
            var tr = '<tr  class="text-center" data-deed_no="' + v.deed_no + '" data-deed_year="' + v.deed_year + '" data-dist_code="' + v.dist_code + '" data-ro_code="' + v.ro_code + '">';
            tr += '<td>' + v.municipalityName + '</td><td>' + v.Ward + '</td><td>' + v.HoldingNo + '</td><td>' + v.AsseName + '</td><td>' + v.plot + '</td><td>' + v.khatian + '</td><td>' + v.TotalOutStanding + '</td>';
            tr += '<td><center>' + searchPropNameAnchor+'</center></td>';
            tr += '</tr>';
            table.find('tbody').append(tr);
        });
        var footer = "<tr ><th colspan='5'><b>Summary (Total Outstanding)</b></th><th><b>Summary</b></th><th class='text-center' colspan='2'>" + Number(data.reduce(function (a, b) { return a + b.TotalOutStanding; }, 0)).toFixed(2) + "</th></tr>";
        table.find('tfoot').append(footer);
        cntr.append(table);
    };
    var showElectricityData = function (data) {
        var cntr = $(".plc-3nd");
        cntr.empty();
        if (data && (data.length === 0 || (data.length > 0 && data[0]["ERROR"] == 1))) {
            cntr.append('<h3 class="text-center"></h3>');
            return;
        }
        ///
        var table = "";
        table += "<div class='table-responsive'>";
        table += "<table class='table table-bordered table-striped table-hover table-condensed'>";
        table += "<thead style='background-color:#2F5B93;color:white;text-align: center;'>";
        // table += "<tr><th colspan='8' class='text-center'>Property Details</th></tr>";
        table += "<tr>";
        table += "<th><b>Municipality Name</th><th>Ward No</th><th>Holding No/Assessee No</th><th>Electricity Consumer No<th>Electricity Consumer Name</th><th>Outstanding (INR)</th><th>Water Connection Consumer No(if any)</th><th>Outstanding (INR)</th>";
        table += "</tr>";
        table += "</thead>";
        table += "<tbody><tr class='text-center'><td colspan='8'></td></tr></tbody>";
        table += "<tfoot></tfoot>";
        table += "</table>";
        table += "</div>";
        table = $(table);

        var footer = "<tr ><th colspan='5'><b></b></th><th><b></b></th><th class='text-center' colspan='2'></th></tr>";
        table.find('tfoot').append(footer);
        cntr.append(table);

    };
    var showPlotData = function (data) {
        var cntr = $(".plc-1st");
        cntr.empty();
        if (data && (data.length === 0 || (data.length > 0 && data[0]["ERROR"] != 1))) {
            cntr.append('<h3 class="text-center"></h3>')
            return;
        }
        var table = "";
        table += "<div class='table-responsive'>";
        table += "<table class='table table-bordered table-striped table-hover table-condensed'>";
        table += "<thead style='background-color:#2F5B93;color:white;text-align: center;'>";
        table += "<tr><th colspan='3' class='text-center'>Plot Informations</th></tr>";
        table += "<tr>";
        table += "<th><center><b>Plot No.<br>দাগ নং </b></center></th><th><center>Classification<br>শ্রেণী</center></th><th><center>Total Area of the Plot(Acre)<br>জমির মোট পরিমাণ(একর)</center></th>";
        table += "</tr>";
        table += "</thead>";
        table += "<tbody><tr class='text-center'><td colspan='3'></td></tr></tbody>";
        table += "</table>";
        table += "</div>";
        table = $(table);

        if (data && data.length > 0) {
            table.find('tbody').empty();
        }
        $.each(data, function (i, v) {
            table.find('tbody').append('<tr class="text-center"><td>' + v.PLOT_NO + '</td><td>' + v.PLOT_CLASSIFICATION + '</td><td>' + v.PLOT_SHARE_AREA + '</td></tr>');
        });

        var table2 = "<table class='table table-bordered table-striped table-hover table-condensed'>";
        table2 += "<thead style='background-color:#2F5B93;color:white;text-align: center;'>";
        table2 += "<tr><th colspan='6' class='text-center'>Owners</th></tr>";
        table2 += "<tr class='text-center'>";
        table2 += "<th> খতিয়ান নং </th>";
        table2 += "<th> রায়তের নাম </th>";
        table2 += "<th>পিতা/স্বামী </th>";
        table2 += "<th>অংশ</th>";
        table2 += "<th>অংশ পরিমাণ(একর) </th>"; 
        table2 += "<th class='text-center'>Court Case </th>";
        table2 += "</tr>";
        table2 += "</thead>";
        table2 += "<tbody></tbody>";
        table2 += "</table>";
        table2 = $(table2);
        $.each(data, function (ii, vv) {
            $.each(vv.OWNER_DETAIL, function (i, v) {
                var courtCastTd = getCourtCaseLinkTemplate(v.KHATIAN_NO, $(".txt-seachby").val());
                table2.find('tbody').append('<tr class="text-center"><td>' + v.KHATIAN_NO + '</td><td>' + v.OWNER_NAME + '</td><td>' + v.GUARDIAN_NAME + '</td><td>' + v.KHATIAN_SHARE + '</td><td>' + v.KHATIAN_SHARE_AREA + '</td>' + courtCastTd+'</tr>');
            });
        });
        table.append(table2);
        cntr.empty().append(table);
    };
    var showKhatiandata = function (data) {
        
        var cntr = $(".plc-1st");
        cntr.empty();
        if (data && (data.length === 0 || (data.length > 0 && data[0]["Error"] != 1))) {
            cntr.append('<h3 class="text-center">no data found against search criteria</h3>')
            return;
        }
        var table = "";
        table += "<div class='table-responsive'>";
        table += "<table class='table table-bordered table-striped table-hover table-condensed'>";
        table += "<thead style='background-color:#2F5B93;color:white;text-align: center;'>";
        table += "<tr><th colspan='7' class='text-center'>Khatian Informations</th></tr>";
        table += "<tr>";
        table += "<th> খতিয়ান নং </th>";
        table += "<th> রায়তের নাম </th>";
        table += "<th>পিতা/স্বামী </th>";
        table += "<th>ঠিকানা</th>";
        table += "<th>জমির পরিমাণ </th>";
        table += "<th>Mutation Date</th>";
        table += "<th>দাগের সংখ্যা</th>";
        table += "</tr>";
        table += "</thead>";
        table += "<tbody><tr><td colspan='6'></td></tr></tbody>";
        table += "</table>";
        table += "</div>";
        table = $(table);
        if (data && data.length > 0) {
            table.find('tbody').empty();
        }
        $.each(data, function (i, v) {
            table.find('tbody').append('<tr class="text-center"><td>' + $(ctrls.srchText).val() + '</td><td>' + v.OwnerName + '</td><td>' + v.GurdianName + '</td><td>' + v.Address + '</td><td>' + v.TotalArea + '(একর/Acre)</td><td>' + (v.MutationData ? v.MutationData.KhatianCreationDate : "") + '</td><td>' + v.NoOfPlots + '</td></tr>');
        });
        cntr.empty().append(table);
    };
    var showSearchNameProperty = function (data) {
        var strDeedDetails = function (d) {
            var deedhyperLinkIfExist = function (deedNo) {
                var existFilesByDeed = [
                    { deedNo: '00062', file: 'DEED00062.pdf' },
                    { deedNo: '03538', file: 'DEED03538.pdf' }
                ];
                var DeedFile = existFilesByDeed.find(function (v) { return v.deedNo === deedNo; });
                if (DeedFile) {
                    return "<a title='Click here to download' style='text-decoration: underline;' href='/UploadedFiles/DeedCopies/" + DeedFile.file + "' target='_blank'>" + deedNo + "</a>";
                }
                return deedNo;
            };
            var str = "<div class='table-responsive'>";
            str += "<table class='table table-striped table-bordered table-hover'>";
            str += "<thead>";
            str += "<tr><th colspan='13' class='text-center'>Deed Details</th></tr>";
            str += "<tr>";
            str += "<th align='left' >Book</th>";
            //str += "<th align='left' >Registered At</th>";
            str += "<th align='left' >Deed No</th>";
            str += "<th align='left' >Deed Year</th>";
            str += "<th align='left' >District Code</th>";
            str += "<th align='left' >Date of Completion</th>";
            str += "<th align='left' >Date of Registration</th>";
            str += "<th align='left'>Query No</th>";
            str += "<th align='left'>Query Year</th>";
            str += "<th align='left'>Ro Code</th>";
            str += "<th align='left'>Serial No</th>";
            str += "<th align='left'>Serial Year</th>";
            str += "<th align='left'>Tran_maj_code</th>";
            str += "<th align='left'>Tran_min_code</th>";
            str += "</tr>";
            str += "</thead>";
            str += "<tbody>";
            str += "<tr class='firstline' align='left'>";
            str += "<td align='left' valign='top'>" + d.Book + "</td>";
            //str += "<td align='left' valign='top'>" + d.Book + "</td>";
            str += "<td align='left' valign='top'>" +deedhyperLinkIfExist(d.Deed_no)+"</td>";
            str += "<td align='left' valign='top'>"+d.Deed_year+"</td>";
            str += "<td align='left' valign='top'>" + d.District_code+"</td>";
            str += "<td align='left' valign='top'>" + d.Dt_of_Completion+"</td>";
            str += "<td align='left' valign='top'>" + d.Dt_of_Registration+"</td>";
            str += "<td align='left' valign='top'>" + d.Query_No+"</td>";
            str += "<td align='left' valign='top'>" + d.Query_year+"</td>";
            str += "<td align='left' valign='top'>" + d.Ro_code+"</td>";
            str += "<td align='left' valign='top'>" + d.Serial_No+"</td>";
            str += "<td align='left' valign='top'>" + d.Serial_year+"</td>";
            str += "<td align='left' valign='top'>" + d.Tran_maj_code+"</td>";
            str += "<td align='left' valign='top'>" + d.Tran_min_code+"</td>";
            str += "</tr>";
            str += "</tbody></table>";
            str += "</div>";
            return str;
        };
        var strPartyDetails = function (d1) {
            var str = "<div class='table-responsive'>";
            str += "<table class='table table-striped table-bordered table-hover'>";
            str += "<thead>";
            str += "<tr><th colspan='12' class='text-center'>Party Details</th></tr>";
            str += "<tr>";
            str += "<th align='left' >Address</th>";
            str += "<th align='left' >Country Name</th>";
            str += "<th align='left' >District Name</th>";
            str += "<th align='left' >Ps Name</th>";
            str += "<th align='left' >State Name</th>";
            str += "<th align='left' >City Village Name</th>";
            str += "<th align='left' >Father Mother</th>";
            str += "<th align='left'>First Name</th>";
            str += "<th align='left'>Last Name</th>";
            str += "<th align='left'>Party Serial No</th>";
            str += "<th align='left'>Pin Code</th>";
            str += "<th align='left'>Relative Name</th>";
            str += "<th align='left'>Status</th>";
            str += "</tr>";
            str += "</thead>";
            str += "<tbody>";
            
            $.each(d1, function (i, d) {
                str += "<tr class='firstline' align='left'>";
                str += "<td align='left' valign='top'>" + d.Address + "</td>";
                str += "<td align='left' valign='top'>" + d.Address_Country_Name + "</td>";
                str += "<td align='left' valign='top'>" + d.Address_District_Name + "</td>";
                str += "<td align='left' valign='top'>" + d.Address_PS_Name + "</td>";
                str += "<td align='left' valign='top'>" + d.Address_State_Name + "</td>";
                str += "<td align='left' valign='top'>" + d.CityVillage_Name + "</td>";
                str += "<td align='left' valign='top'>" + d.Father_Mother + "</td>";
                str += "<td align='left' valign='top'>" + d.First_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Last_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Party_serialno + "</td>";
                str += "<td align='left' valign='top'>" + d.Pincode + "</td>";
                str += "<td align='left' valign='top'>" + d.Relative_Name + "</td>"; 
                str += "<td align='left' valign='top'>" + d.Status_Desc + "</td>";
                str += "</tr>";
            });
            str += "</tbody></table>";
            str += "</div>";
            return str;
        };
        var strPropertyDetails = function (d1) {
            var str = "<div class='table-responsive'>";
            str += "<table class='table table-striped table-bordered table-hover'>";
            str += "<thead>";
            str += "<tr><th colspan='12' class='text-center'>Property Details</th></tr>";
            str += "<tr>";
            str += "<th align='left' >Plot No</th>";
            str += "<th align='left' >District Name</th>";
            str += "<th align='left' >Gp muni Corp name</th>";
            str += "<th align='left' >Khatian No</th>";
            str += "<th align='left' >Mouza</th>";
            str += "<th align='left' >Nature Of Land</th>";
            str += "<th align='left' >Plot no</th>";
            str += "<th align='left'>Area</th>";
            str += "<th align='left'>Serial No</th>";
            str += "<th align='left'>Ro Name</th>";
            str += "<th align='left'>Property Type</th>";
            str += "<th align='left'>Ps Name</th>";
            str += "</tr>";
            str += "</thead>";
            str += "<tbody>";

            $.each(d1, function (i, d) {
                str += "<tr class='firstline' align='left'>";
                str += "<td align='left' valign='top'>" + d.Bata_plot_no + "</td>";
                str += "<td align='left' valign='top'>" + d.District_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Gp_muni_Corp_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Khatian_no + "</td>";
                str += "<td align='left' valign='top'>" + d.Mouza + "</td>";
                str += "<td align='left' valign='top'>" + d.Nature_of_Land_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Plot_no + "</td>";
                str += "<td align='left' valign='top'>" + d.Property_Area + "</td>";
                str += "<td align='left' valign='top'>" + d.Property_Serialno + "</td>";
                str += "<td align='left' valign='top'>" + d.Property_ro_name + "</td>";
                str += "<td align='left' valign='top'>" + d.Property_type + "</td>";
                str += "<td align='left' valign='top'>" + d.Ps_name + "</td>";
                str += "</tr>";
            });
            str += "</tbody></table>";
            str += "</div>";
            return str;
        };

        var str = "";
        str += strDeedDetails(data.Deed_details);
        str += strPartyDetails(data.Party_details);
        str += strPropertyDetails(data.Property_details);

        var popupCtr = $(".mod-search-name-property");
        var body = popupCtr.find('.modal-body');
        body.empty();
        body.append(str);
        popupCtr.modal('show');
    };
    var showLandData = function (stype, Data) {
        if (stype == "pl") {
            showPlotData(Data.RemoteData);
        } else {
            showKhatiandata(Data.RemoteData);
        }
        showOutStanding(Data.OutStandings);
        showElectricityData();
    };
    var getFormData = function () {
        return {
            NidCode: $(ctrls.lstmouza).val(),
            SearchBy: $(ctrls.rdioSrchBy + ":checked").val(),
            PlotOrKhatNo: $(ctrls.srchText).val()
        };
    };
    var bindEvents = function () {
        $(document).on('click', '.search-name-property', function () {
            var row = $(this).closest('tr');
            var deedno = row.attr('data-deed_no');
            var deedYear = row.attr('data-deed_year');
            var ro_code = row.attr('data-ro_code');
            var district = row.attr('data-dist_code');

            var x = { dist_code: district, ro_code: ro_code, deed_no: deedno, deed_year: deedYear };
            server.loadSearchNameProperty(x, function (r) {
                if (r.Status === 200) {
                    showSearchNameProperty(r.Data);
                } else {
                    alertify.error(r.Message);
                }
            });
        });
        $(ctrls.lstdist).on('change', function () {
            $(ctrls.lstblock).find('option:not(:first)').remove();
            $(ctrls.lstmouza).find('option:not(:first)').remove();
            server.loadBlock({ distCode: $(this).val() }, function (r) {
                if (r.Status === 200) {
                    $.each(r.Data, function (i, v) {
                        $(ctrls.lstblock).append("<option value='" + v.Code + "'>[" + v.Code + "] " + v.Value + "</option>")
                    })
                } else {
                    alertify.alert(r.Message);
                }
            });
        });
        $(ctrls.lstblock).on('change', function () {
            $(ctrls.lstmouza).find('option:not(:first)').remove();
            server.loadMouza({ distCode: $(ctrls.lstdist).val(), blockCode: $(this).val() }, function (r) {
                if (r.Status === 200) {
                    $.each(r.Data, function (i, v) {
                        $(ctrls.lstmouza).append("<option value='" + v.Idn + "'>[" + v.Code + "] " + v.Value + "</option>")
                    })
                } else {
                    alertify.alert(r.Message);
                }
            })
        });
        $(ctrls.lstmouza).on('change', function () {

        });
        $(ctrls.rdioSrchBy).on('change', function () {
            var searchTxt = $(ctrls.rdioSrchBy + ":checked");
            $(".lbl-srch-by").text(searchTxt.attr('data-srch-txt'));
        });
        $(ctrls.btnView).on('click', function (e) {
            e.preventDefault();
            if ($('form').valid()) {
                var payload = getFormData();
                server.loadLandDetails(payload, function (r) {
                    if (r.Status == 200) {
                        showLandData(payload.SearchBy, r.Data);
                    } else {
                        alertify.error(r.Message);
                    }
                })
            } else {
                alert('not valid');
            }
        });

        // Search by Assessee part district population 16/10/2020 by subhajit sil
        $('#divCitizenProfile').hide();
        $('a[data-toggle="tab"]').on('click', function (e) {
            $(".searched-holding-result > .list-group").empty();
            $('#divCitizenProfile').hide();
            Grid.Reset();
        });

        server.loadMstDistrict(function (response) {
            DistrictDataSource = response.Data;
            PopulateDistrictData();
        });
        $("#ddlMnDistrict").on('change', DistrictChange);
        $("#ddlMnMunicipalityID").on('change', MunicipalityChange);
        $("#btn-search-by-holding-no").on('click', searchAssesseeByHoldingNo);
        $("#btn-search-by-assessee-name").on('click', searchAssesseeByAssesseeName);
        $(document).on('keyup', ".search-field-holding-search-panel>input", function () {
            var lstItems = $(".searched-holding-result > .list-group").find('.data-field');
            var value = $(this).val().toUpperCase();
            if (value === '') {
                lstItems.show();
                return false;
            }
            lstItems.each(function (index, item) {                
                $item = $(this);
                var field1 = $item.attr('data-ward').toUpperCase();
                var field2 = $item.attr('data-assessee-name').toUpperCase();
                var field3 = $item.attr('data-location').toUpperCase();

                if ((field1.indexOf(value) > -1) || (field2.indexOf(value) > -1) || (field3.indexOf(value) > -1)) {
                    $item.show();
                    console.log(field2 + '=>shown');
                }
                else {
                    $item.hide();
                    //console.log(field2 + '=>hidden');
                }               
            });
        });

        $(document).on('click', ".searched-holding-result > .list-group > .data-field .btn-searchitem-holding", function (e) {
            e.preventDefault();

            var assesseeID = $(this).attr('data-assessee-id');
            OnSearchOutstandingByAssesseeID(assesseeID);
            //alert(assesseeID);
            //$(".txtSearchByAssesseeID").val(assesseeID);
            //$('.search-by-assesseeID').trigger('click');
        });
    };
    var DistrictDataSource = [];
    var MunicipalityDataSource = [];
    var PopulateDistrictData = function () {
        $('#ddlMnDistrict').empty();
        $('#ddlMnDistrict').append('<option value="">--select--</option>');
        $.each(DistrictDataSource, function (index, value) {
            var label = value.DistrictName;
            var id = value.DistrictID;
            $('#ddlMnDistrict').append("<option value='" + id + "'>" + label + "</option>");
        });
    };

    var MunicipalityChange = function () {
        $(".searched-holding-result > .list-group").empty();
        $('#divCitizenProfile').hide();
        Grid.Reset();
    };

    var DistrictChange = function () {
        if ($('#ddlMnDistrict').val() != "") {
            server.loadMunicipality($('#ddlMnDistrict').val(), function (response) {
                MunicipalityDataSource = response.Data;
                PopulateMunicipalityData();

                $(".searched-holding-result > .list-group").empty();
                $('#divCitizenProfile').hide();
                Grid.Reset();
            });
        } else {
            $('#ddlMnMunicipalityID').empty();
        }
    };

    var PopulateMunicipalityData = function () {
        $('#ddlMnMunicipalityID').empty();
        $('#ddlMnMunicipalityID').append('<option value="">--select--</option>');
        $.each(MunicipalityDataSource, function (index, value) {
            var label = value.MunicipalityName;
            var id = value.MunicipalityID;
            $('#ddlMnMunicipalityID').append("<option value='" + id + "'>" + label + "</option>");
        });
    };

    var searchAssesseeByHoldingNo = function () {        
        if (validateAssesseeSearch()) {
            OnSearchAssessees('H');  // Search by Holding No (H)
        }
    };

    var searchAssesseeByAssesseeName = function () {
        if (validateAssesseeSearch()) {
            OnSearchAssessees('N');  // Search by Assessee Name (N)
        }
    };

    var OnSearchAssessees = function (searchTypeVal) {
        var txtHoldingNo = $("#txtHoldingNo").val();
        var txtAssesseeName = $("#txtAssesseeNameSearch").val();
        var SearchedList = new function () {
            var listContainer = $(".searched-holding-result > .list-group");
            var dataFieldTemplate = function (isSelected, assesseeName, assesseeID, Ward, location, HoldingNo) {
                strListItem = "<a class='list-group-item data-field " + (isSelected === true ? "active" : "") + "' data-assessee-id='" + assesseeID + "' data-assessee-name='" + assesseeName + "' data-ward='" + Ward + "' data-location='" + location + "' title='Assessee ID: " + assesseeID + "'>";
                strListItem += "<div class='container-fluid'>";
                strListItem += "<div class='row'>";
                strListItem += "<div class='col-md-11 col-sm-11 col-xs-10'>";
                strListItem += "<p class='list-group-item-text' ><span class='glyphicon glyphicon-user'></span>&nbsp; <b>" + assesseeName + "</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>Ward:</b> " + Ward + "&nbsp;&nbsp;&nbsp;&nbsp; <b>Location:</b> " + location + "&nbsp;&nbsp;&nbsp;&nbsp; <b>Holding No:</b> " + HoldingNo + "</p >";
                strListItem += "</div>";
                strListItem += "<div class='col-md-1 col-sm-1 col-xs-2'><button class='pull-right glyphicon glyphicon-arrow-right btn-searchitem-holding' data-assessee-id='" + assesseeID + "'> </div>";
                strListItem += "</div>";
                strListItem += "</div>";
                strListItem += "</a >";
                return strListItem;
            };
            var noAssesseeFoundTamplate = function () {
                strListItem = "<a class='list-group-item no-assessee-found-field'>";
                strListItem += "<p class='list-group-item-text' style='text-align:center'>No Assessee Found</p >";
                strListItem += "</a >";
                return strListItem;
            };
            var searchFieldTemplate = function (recordNumber) {
                strField = "<a class='input-group search-field-holding-search-panel' style='width:100%;'>";
                strField += "<input id= 'txtSearchOnHolding' type= 'text' class='form-control' name= 'email' placeholder= 'Seach with Name, Ward, Location " + (recordNumber != null ? " amoung " + recordNumber + " records" : "") + "' >";
                strField += "</a >";
                return strField;
            };
            this.emptyDataFields = function () {
                $(".searched-holding-result > .list-group > .data-field").remove();
            };
            this.removeNoAssesseeFoundFiled = function () {
                $(".searched-holding-result > .list-group > .no-assessee-found-field").remove();
            };
            this.removeSearhField = function () {
                $(".searched-holding-result > .list-group > .search-field-holding-search-panel").remove();
            };
            this.emptySearchedList = function () {
                $(".searched-holding-result > .list-group").empty();
            };
            this.addSearchField = function (recordNumber) {
                listContainer.append(searchFieldTemplate(recordNumber));
            };
            this.addDataField = function (isActive, data) {
                listContainer.append(dataFieldTemplate(isActive, data.AssesseeName, data.AssesseeID, data.WardName, data.LocationName, data.HoldingNo));
            };
            this.addNoAssesseeFoundField = function () {
                listContainer.append(noAssesseeFoundTamplate());
            };

        };

        SearchedList.emptySearchedList();
        server.loadSearchAssesseesByHolding({ MunicipalityID: $('#ddlMnMunicipalityID').val(), HoldingNo: txtHoldingNo, AssesseeName: txtAssesseeName, SearchTypeVal: searchTypeVal }, function (response) {       
            if (response.Status === 200) {
                if (response.Data.length > 0) {
                    SearchedList.addSearchField(response.Data.length);
                    response.Data.forEach(function (value, index) {
                        SearchedList.addDataField((index == 0 ? true : false), value);
                    });
                } else {
                    SearchedList.addNoAssesseeFoundField();
                }
            }                       
        });
    };

    var validateAssesseeSearch = function () {
        $("#frmAssesseeSearch").validate({
            rules: {
                ddlMnDistrict: {
                    required: true
                },
                ddlMnMunicipalityID: {
                    required: true
                },
                txtHoldingNo: {
                    required: true
                },
                txtAssesseeNameSearch: {
                    required: true
                }
            },
            messages: {
                ddlMnDistrict: {
                    required: "Please select District."
                },
                ddlMnMunicipalityID: {
                    required: "Please select an ULB."
                },
                txtHoldingNo: {
                    required: "Please enter a Holding No."
                },
                txtAssesseeNameSearch: {
                    required: "Please enter Assessee Name."
                }
            },
            highlight: function (element) {
                $(element).css('border', '1px solid #a94442');
                $(element).addClass('error');
            },
            unhighlight: function (element) {
                $(element).css('background', 'white');
                $(element).css('border', '1px solid #18bc9c');
                $(element).removeClass('error');
            }
        });
        return $('#frmAssesseeSearch').valid();
    };

    var GlobalVariables = {
        IsSearchEnabled: true,
        PaymentObject: {},
        PaymentModes: [],
        PaymentCounter: {},
        NetAmount: 0,
        //gridLast20PaymentsByAssessee: new GridLast20PaymentForAssessee(),
        Assessee: null
    };
    var Rest_GlobalVariable = function () {
        GlobalVariables.PaymentObject = {};
        GlobalVariables.NetAmount = 0;
        GlobalVariables.Assessee = null;
        //dataPopulationAlert.hide();
        GlobalVariables.IsSearchEnabled = true;
        GlobalVariables.SelectedGridRows = [];
    };

    var OnSuccessfullSeachAssesse = function (assessee) {
        GlobalVariables.Assessee = assessee;
        //DomControls.ctrl_holdingType.text(assessee.HoldingTypeDesc);
    };

    var OnSearchOutstandingByAssesseeID = function (getAssesseeid) {
        //var serchBtn = $(this);
        var payload = { AssesseeID: getAssesseeid };
        if (payload.AssesseeID.trim() == '' || isNaN(payload.AssesseeID)) {
            alert("Please enter valid AssesseeID");
            //DomControls.ctrl_txtSearchByAssesseeID.focus();
            return;
        }
        server.loadGetAssesseeByID(payload, function (response) {
            if (response.Status === 200) {
                var data = response.Data;
                if (data != null) {
                    var dt = new Date();

                    $('#lblAssessee').text(data.AssesseeName);
                    $('#lblAssesseeNo').text(data.AssesseeNo);
                    $('#lblAddress').text(data.AssesseeAddress);
                    $('#lblPhone').text(data.PrimaryPhNo);
                    $('#lblEmail').text(data.PrimaryEmail);

                    $('#lblMunicipality').text($("#ddlMnMunicipalityID option:selected").text());
                    $('#lblWard').text(data.Ward.WardName);
                    $('#lblLocation').text(data.Location.LocationName);
                    $('#lblHoldingNo').text(data.HoldingNo);
                    $('#lblHoldingType').text(data.HoldingTypeDesc);

                    $('#lblBlock').text(data.Block);
                    $('#lblMouja').text(data.Mouja);
                    $('#lblPlot').text(data.Plot);
                    $('#lblKhatian').text(data.Khatian);
                    $('#lblJlNo').text(data.JLNo);
                    $('#lblDeedNo').text(data.DeedNo);

                    OnSuccessfullSeachAssesse(data);
                    $('#hdnWard').val(data.Ward.WardID);
                    $('#hdnLocation').val(data.Location.LocationID);
                    $('#hdnAssessee').val(payload.AssesseeID);
                    
                    var dtt = $('#txtCollectionDate').val();
                    if ($('#txtCollectionDate').val() == undefined || $('#txtCollectionDate').val() == '' || $('#txtCollectionDate').val() == null) {
                        $('#txtCollectionDate').val(dt.format('yyyy-mm-dd'));
                    }
                    //DomControls.ctrl_btnSearch.trigger('click');
                    $('#divCitizenProfile').show();
                    OnSearchClick();
                } else {
                    alert("Assessee not found! Try again");
                }
            } else {
                alert(response.Message);
            }
        });
    };

    var OnSearchClick = function () {
        Grid.Reset();
        //dataPopulationAlert.reset();
        //if ($("#frmPropertyTaxPaymentSearch").valid()) {
            //dataPopulationAlert.show_ProcessingPaymentInfo();
            var data = searchableObject(true);
            PopulateData(data, function (response) {
                Grid.CheckAllRow(true);
                //GlobalVariables.gridLast20PaymentsByAssessee.Loadtable(isNaN(DomControls.ctrl_hdnAssessee.val()) ? 0 : DomControls.ctrl_hdnAssessee.val(), function (table) {
                //    $(".last-20-payment-by-assessee-panel .panel-body").append(table);
                //});
            });
        //}
    };

    var searchableObject = function (checkParamRequired) {
        if (checkParamRequired !== undefined) {
            return { MunicipalityID: $('#ddlMnMunicipalityID').val(), WardID: $('#hdnWard').val(), LocationID: $('#hdnLocation').val(), AssesseeID: $('#hdnAssessee').val(), PaymentDate: $('#txtCollectionDate').val(), TaxPaymentCheckParameters: [], NoticeServedOn: '', EffectQtrFrom: '' };
        } else {
            return { MunicipalityID: $('#ddlMnMunicipalityID').val(), WardID: $('#hdnWard').val(), LocationID: $('#hdnLocation').val(), AssesseeID: $('#hdnAssessee').val(), PaymentDate: $('#txtCollectionDate').val(), TaxPaymentCheckParameters: Grid.GetSelectedRowObjs(), NoticeServedOn: '', EffectQtrFrom: ''};
        }
    };

    var PopulateData = function (payload, callback) {
        //if (Validator.ValidateSearchFields()) {
            var selectedAssess = GlobalVariables.Assessee;
            Rest_GlobalVariable();
            GlobalVariables.Assessee = selectedAssess;
            //LockSearchField();
            server.loadGetPaymentDetails(payload, function (response) {
                if (response.Status === 200) {
                    var Data = response.Data;
                    if (Data == null) {
                        //dataPopulationAlert.show_PaymentInfoNotFound();
                    } else {
                        //serverObject.GetManualAdvanceAdjAvailability(payload, function (r) {
                        //    if (r.Status === 200) {
                        //        if (r.Data.ManualAdjAvailabity !== null && r.Data.ManualAdjAvailabity.IsAvailable === true) {
                        //            var redirectTo = r.Data.RedirectTo;
                        //            var advAmount = r.Data.ManualAdjAvailabity.AdvanceAmount;
                        //            alertify.alert("Adjustable advance available, Rs. " + advAmount + "/-", "<a href=" + redirectTo + " target='_blank'>Click here</a> to manually adjust advance");
                        //        }
                        //    } else {
                        //        alertify.error("Could not get Manuall advance info. Detail: " + r.Message);
                        //    }
                        //});
                        GlobalVariables.PaymentObject = Data;
                        Grid.SetDataSource(GlobalVariables.PaymentObject);
                        Grid.DataBind();
                        //dataPopulationAlert.hide();
                        //dataPopulationAlert.show_AdvancePaymentLink();
                    }
                    callback(Data);
                } else {
                    alert('Alert: ' + response.Message);
                }
            });

        //}
    };

    var Grid = new function () {
        var ExpandexGroupList = [];
        var totalColNumber = 11;
        var DataSet = [];
        var GiantObject = {};
        var tablContainer = $("#gridContainer");
        var TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
        if (TblCtrl.length === 0) {
            tablContainer.append("<table id='B98E8A35-C901-45A4-8AFF-B486BC58E199' style='width: 100%;margin-bottom: 0px' class='table table-condensed table-bordered table-responsive'><thead></thead><tbody></tbody><tfoot></tfoot></table>");
            TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
        }
        var tableHeader = function () {
            var hr = "<tr >" +
                "<th rowspan='2' style=' text-align: center;'>Fin Year</th>" +
                "<th rowspan='2' style=' text-align: center;'>Qtr</th>" +
                "<th rowspan='2' style=' text-align: center;'>Property Tax</th>" +
                "<th rowspan='2' style=' text-align: center;'>Surcharge</th>" +
                "<th  colspan='2' style=' text-align: center;'>Outstanding</th>" +
                "<th  rowspan='2' style=' text-align: center;'>Rebate/ Interest</th>" +
                "<th  colspan='2' style=' text-align: center;'>Paying</th>" +
                "<th  rowspan='2' style=' text-align: center;'>Action<br/><input type='checkbox' class='checkall'/></th>" +
                "<th  rowspan='2' style=' text-align: center;'>Line Total</th>" +
                "<th  rowspan='2' style=' text-align: center;'>Payment Flow</th>" +
                "</tr>" +
                "<tr >" +
                "<th style=' text-align: center;'>P. Tax</th>" +
                "<th style=' text-align: center;'>Surcharge</th>" +
                "<th style=' text-align: center;'>P. Tax</th>" +
                "<th style=' text-align: center;'>Surcharge</th>" +
                "</tr>";

            return hr;
        };
        var tableBody = function () {
            var rowFormattedObjects = function () {
                if (GiantObject === null || typeof (GiantObject) == 'undefined' || GiantObject.hasOwnProperty("propertyTaxOutstadings") == false || GiantObject.hasOwnProperty("propertyTaxPaymentMaster") == false) {
                    throw "Invalid DataSource";
                }
                var lst = [];
                $.each(GiantObject.propertyTaxOutstadings, function (index, value) {
                    var _calculatedDetail = GiantObject.propertyTaxPaymentMaster.PropertyTaxPaymentDetails[index];
                    var viewRow = {
                        FinYear: value.FinYear,
                        PropertyTaxValue: value.PropertyTaxValue,
                        SurchargeValue: value.SurchargeValue,
                        QtrNo: value.QtrNo,
                        Rank: value.Rank,
                        OSPropertyTaxAmt: value.OSPropertyTaxAmt,
                        OSSurchargeAmt: value.OSSurchargeAmt,
                        OSCalculatedValue: _calculatedDetail.CalculatedValue,
                        PaidSurchargeAmt: _calculatedDetail.PaidSurchargeAmt,
                        PaidPropertyTaxAmt: _calculatedDetail.PaidTaxAmt,
                        PaidCalculatedAmt: _calculatedDetail.PaidCalculatedAmt,
                        PayheadDetailLvl: _calculatedDetail.payHead,
                        IsChecked: _calculatedDetail.taxPaymentCheckParameter.IsChecked,
                        Rank: _calculatedDetail.taxPaymentCheckParameter.Rank
                    };
                    lst.push(viewRow);
                });
                return lst;
            };
            var lstRowStrings = "";

            var lstFormattedDatasource = rowFormattedObjects();
            DataSet = lstFormattedDatasource;

            if (lstFormattedDatasource.length > 0) {
                window.groupbyTestSource = lstFormattedDatasource;
                //group by finyear
                alasql('SELECT FinYear FROM ? GROUP BY FinYear order by CAST(SUBSTRING(FinYear, 1, 4) AS NUMBER) asc', [groupbyTestSource]).forEach(function (valuep, index) {
                    var getColorCode = randomColor({
                        luminosity: 'light',
                        hue: 'blue'
                    });;
                    //add header row
                    var headerDiv = "<div data-isExpanded='false'><span class='state-symbol'>+</span>&nbsp;&nbsp;<span class='headerTitle'>" + valuep.FinYear + "</span></div>";
                    var summaryParameters = { osPtax: 0, osSurcharge: 0, calvalue: 0, paidPtax: 0, paidSurcharge: 0, groupCheck: "<input type='checkbox' class='groupCheck' data-finyear='" + valuep.FinYear + "'/>", tillTotal: 0 };
                    lstRowStrings += "<tr  class='data-header' data-FinYear='" + valuep.FinYear + "' style='text-align: center;background-color:" + getColorCode + ";cursor:pointer;'>" +
                        "<td colspan='4' style='text-align: left;' class='Summary'>" + headerDiv + "</td>" +
                        "<td class='OSPropertyTaxAmt'><span>" + summaryParameters.osPtax + "</span></td>" +
                        "<td  class='OSSurchargeAmt'><span>" + summaryParameters.osSurcharge + "</span></td>" +
                        "<td class='PaidCalculatedAmt'><span>" + summaryParameters.paidPtax + "</span></td>" +
                        "<td class='PaidPropertyTaxAmt'><span>" + summaryParameters.paidSurcharge + "</span></td>" +
                        "<td class='PaidSurchargeAmt'><span>" + summaryParameters.calvalue + "</span></td>" +
                        "<td><span>" + summaryParameters.groupCheck + "</span></td>" +
                        "<td class='lineTotal'><span>0</span></td>" +
                        "<td class='lineTotalFlow'><span></span></td></tr>";

                    var elementsByGroup = alasql("SELECT * FROM ? where FinYear='" + valuep.FinYear + "' order by Rank", [window.groupbyTestSource]);
                    $.each(elementsByGroup, function (index, value) {
                        var tr = "<tr class='data-row' data-rank='" + value.Rank + "' data-parentGroup='" + value.FinYear + "' style=' text-align: center;display:none;'>";
                        tr += "<td class='FinYear'><span>" + value.FinYear + "</span></td>";
                        tr += "<td class='QtrNo'>" + value.QtrNo + "</td>";
                        tr += "<td class='PropertyTaxValue'>" + value.PropertyTaxValue + "</td>";
                        tr += "<td class='SurchargeValue'>" + value.SurchargeValue + "</td>";
                        tr += "<td class='OSPropertyTaxAmt'>" + value.OSPropertyTaxAmt + "</td>";
                        tr += "<td class='OSSurchargeAmt'>" + value.OSSurchargeAmt + "</td>";
                        tr += "<td class='PaidCalculatedAmt'>" + value.PaidCalculatedAmt + "</td>";
                        tr += "<td class='PaidPropertyTaxAmt'>" + value.PaidPropertyTaxAmt + "</td>";
                        tr += "<td class='PaidSurchargeAmt'>" + value.PaidSurchargeAmt + "</td>";
                        tr += "<td class='IsChecked'><input class='chkPay' type='checkbox' " + (value.IsChecked === true ? " checked " : "") + " data-FinYear='" + value.FinYear + "' data-QtrNo='" + value.QtrNo + "' data-rank='" + value.Rank + "'/></td>";
                        tr += "<td class='lineTotal'><span>" + (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt).toFixed(2) + "</span></td>";
                        tr += "<td class='lineTotalFlow'><span >0</span></td>";
                        tr += "</tr>";
                        lstRowStrings += tr;
                    });
                });


            } else {
                lstRowStrings = "<tr><td coldspan='11'>No Record found</td></tr>";
            }
            return lstRowStrings;
        };
        var tableFooter = function () {

            var getTotalPtaxSurch = function (totalPtaxSurchg) {
                var x = "<tr>";
                x += "<td style='text-align:left'><span style='font-weight: bold;'>Total PTax./SChrg.</span></td>";
                x += "<td>:</td>";
                x += "<td style='text-align:right'><span class='TotalPtaxSurch'>" + totalPtaxSurchg + "</span><span>&nbsp;Rs/-</span></td>";
                x += "</tr>";
                return x;
            };
            var getTotalRebateInt = function (totalRbtInt) {
                var x = "<tr>";
                x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Rbt./Int.</span></td>";
                x += "<td>:</td>";
                x += "<td style='text-align:right'><span class='TotalRebateInt'>" + totalRbtInt + "</span><span>&nbsp;Rs/-</span></td>";
                x += "</tr>";
                return x;
            };
            var getTotalOtherAdjAmount = function (totalOthrAdjAmnt) {
                var x = "<tr>";
                x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Adjst. Amnt.</span></td>";
                x += "<td>:</td>";
                x += "<td style='text-align:right'><span class='TotalOtherAdjAmount'>" + totalOthrAdjAmnt + "</span><span>&nbsp;Rs/-</span></td>";
                x += "</tr>";
                return x;
            };
            var getRoundOff = function (roundOff) {
                var x = "<tr>";
                x += "<td style='text-align:left'><span style='font-weight: bold;'>Rounded Off</span></td>";
                x += "<td>:</td>";
                x += "<td style='text-align:right'><span class='RoundOff'>" + roundOff + "</span><span>&nbsp;Rs/-</span></td>";
                x += "</tr>";
                return x;
            };

            var getNetPayable = function (netPayable) {
                var x = "<tr style='font-size: 18px;'>";
                x += "<td style='text-align:left'><span style='font-weight: bold;'>Net Payable</span></td>";
                x += "<td>:</td>";
                x += "<td style='text-align:right'><span class='NetPayable' >" + netPayable + "</span><span>&nbsp;Rs/-</span></td>";
                x += "</tr>";
                return x;
            };

            var getMainFooterSummary = function () {
                var x = "<div class='container-fluid'>" +
                    "<div class='row'>" +
                    "<div class='col-md-6'><table><tbody>" + getTotalPtaxSurch(0.00) + getTotalRebateInt(0.00) + getTotalOtherAdjAmount(0.00) + getRoundOff(0.00) + "</tbody></table></div>" +
                    "<div class='col-md-6'><table><tbody>" + getNetPayable(0.00) + "</tbody></table></div>" +
                    "</div>" +
                    "</div>";
                return x;
            };
            var tf = "<tr class='data-summary' style=' text-align: center;'>";
            tf += "<td class='Summary' colspan='4'>Summary</td>";
            tf += "<td class='OSPropertyTaxAmt'><span>0</span></td>";
            tf += "<td class='OSSurchargeAmt'><span>0</span></td>";
            tf += "<td class='PaidCalculatedAmt'><span>0</span></td>";
            tf += "<td class='PaidPropertyTaxAmt'><span>0</span></td>";
            tf += "<td class='PaidSurchargeAmt'><span>0</span></td>";
            tf += "<td class='check'><span>0</span></td>";
            tf += "<td class='lineTotal'><span>0</span></td>";
            tf += "<td class='lineTotalFlow'><span></span></td>";
            tf += "</tr>";


            tf += "<tr class='data-summary-payment' style=' text-align: center;'>";
            tf += "<td class='Payment-Summary' colspan='4'></td>";
            tf += "<td class='' colspan='6'>" + getMainFooterSummary() + "</td>";
            //tf += "<td class='roundOf' colspan='3'><span style='font-weight: bolder;font-size: 17px'>Round Off: 0 Rs/-</span></td>";
            //tf += "<td class='payButton' colspan='2' style='vertical-align: middle;'><input type='button' data-payment-action='N' class='btn btn-primary btn-block pull-right proceed-payment ' value='Proceed' id='btnProceedPayment' ></td>";
            tf += "</tr>";
            return tf;
        };
        this.DataBind = function () {
            TblCtrl.find('thead').empty();
            TblCtrl.find('thead').append(tableHeader());

            TblCtrl.find('tbody').empty();
            TblCtrl.find('tbody').append(tableBody());

            TblCtrl.find('tfoot').empty();
            TblCtrl.find('tfoot').append(tableFooter());
            SetLineTotalFlow();
            SetTableFooter();
            ManageHeaderCheckBoxes();
        };
        var SetLineTotalFlow = function () {
            var lineTotal = 0;
            $.each(DataSet, function (index, value) {
                if (value.IsChecked == true) {
                    lineTotal += (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt);
                    lineTotal = Number(lineTotal.toFixed(2));
                    var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentgroup='" + value.FinYear + "'][data-rank='" + value.Rank + "'] .lineTotalFlow span");
                    tergateCtrl.html(lineTotal);
                }
            });

            var groupSums = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true GROUP BY FinYear ', [DataSet]);
            $.each(groupSums, function (index, value) {
                var tergateRow = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + value.FinYear + "']");
                var lineTotalFlow = tergateRow.find(" .lineTotal span");
                var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");

                lineTotalFlow.html(value.lineTotal.toFixed(2));
                PaidCalculatedAmt.html(value.PaidCalculatedAmt.toFixed(2));
                PaidPropertyTaxAmt.html(value.PaidPropertyTaxAmt.toFixed(2));
                PaidSurchargeAmt.html(value.PaidSurchargeAmt.toFixed(2));
                OSPropertyTaxAmt.html(value.OSPropertyTaxAmt.toFixed(2));
                OSSurchargeAmt.html(value.OSSurchargeAmt.toFixed(2));
            });
        };
        var SetTableFooter = function () {

            var summary = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true', [DataSet]);

            if (GiantObject.hasOwnProperty("propertyTaxPaymentMaster") && GiantObject.propertyTaxPaymentMaster) {
                var PTaxmasterObj = GiantObject.propertyTaxPaymentMaster;

                var tergateRow = $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary");
                var lineTotalFlow = tergateRow.find(" .lineTotal span");
                var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");
                var totalChecked = tergateRow.find(" .check");

                var groupByFinYearChecked = alasql('SELECT Count(*)as checkCounts FROM ?  where IsChecked=true', [DataSet]);

                if (summary.length > 0) {
                    summary = summary[0];
                    lineTotalFlow.html(summary.lineTotal.toFixed(2));
                    PaidCalculatedAmt.html(summary.PaidCalculatedAmt.toFixed(2));
                    PaidPropertyTaxAmt.html(summary.PaidPropertyTaxAmt.toFixed(2));
                    PaidSurchargeAmt.html(summary.PaidSurchargeAmt.toFixed(2));
                    OSPropertyTaxAmt.html(summary.OSPropertyTaxAmt.toFixed(2));
                    OSSurchargeAmt.html(summary.OSSurchargeAmt.toFixed(2));
                    totalChecked.html(groupByFinYearChecked.length > 0 ? groupByFinYearChecked[0].checkCounts : 0);
                }

                //summary value gather data

                var TotalPtaxSurch = PTaxmasterObj.GrossAmount;
                var TotalRebateInt = summary.PaidCalculatedAmt.toFixed(2);
                var TotalOtherAdjAmount = PTaxmasterObj.AdjustedAmount;
                var RoundOff = PTaxmasterObj.RoundOffAdjust;
                var NetPayable = PTaxmasterObj.NetAmount;
                ///end gathering data

                //summary description
                var containerTable = ("<table><tbody>");
                var paymentSummay = "";
                $.each(PTaxmasterObj.AdjustmentSummaries, function (index, value) {
                    paymentSummay += "<tr><td style='text-align:left;'>" + value.WellKnownName + "</td><td>:</td><td> &nbsp;" + value.AdjustedAmount + " Rs/-</td></tr>";
                });
                containerTable += paymentSummay;
                containerTable += "</tbody></table>";



                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalPtaxSurch').text(TotalPtaxSurch);
                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalRebateInt').text(TotalRebateInt);
                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalOtherAdjAmount').text(TotalOtherAdjAmount);
                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.RoundOff').text(RoundOff);
                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.NetPayable').text(NetPayable);
                $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.Payment-Summary').html(containerTable);

                GlobalVariables.NetAmount = NetPayable;

                //write amount bydefault if cash mode available

            }
        };
        var OnClickProceedButton = function () {
            if ($(this).attr("data-payment-action")) {
                var obj = { paymentAction: $(this).attr("data-payment-action") };
                PaymentCompletionModal.ResetModal();
                PaymentCompletionModal.ShowModal(obj);
            }
        };
        //manage check uncheck state group header check boxes and Select All Check box
        var ManageHeaderCheckBoxes = function () {
            var groupByFinYearAll = alasql('SELECT count(*) as rowCounts,FinYear FROM ?   group by FinYear', [DataSet]);
            var groupByFinYearChecked = alasql('SELECT sum(case when IsChecked=true then 1 else 0 end)as rowCounts ,FinYear FROM ?  group by FinYear', [DataSet]);

            var joinedList = alasql("select A.rowCounts as TotalRowCount,B.rowCounts as TotalCheckedRowCount,A.FinYear  from  ? as A  join  ? as B on A.FinYear=B.FinYear", [groupByFinYearAll, groupByFinYearChecked]);
            //manage check box from group header
            $.each(joinedList, function (index, value) {
                var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header .groupCheck[data-finyear='" + value.FinYear + "']");
                if (tergateCtrl.length > 0) {
                    tergateCtrl.prop('checked', (value.TotalCheckedRowCount === value.TotalRowCount))

                }
            });

            //manage all select check box
            var totalListSummary = alasql("select SUM(TotalRowCount) as TotalRowCount,SUM(TotalCheckedRowCount) as TotalCheckedRowCount  from  ? ", [joinedList]);
            if (totalListSummary.length > 0) {
                var tergateCtrl = $("#" + TblCtrl.attr('id') + " > thead >tr .checkall");
                if (tergateCtrl.length > 0) {
                    tergateCtrl.prop('checked', (totalListSummary[0].TotalCheckedRowCount === totalListSummary[0].TotalRowCount))

                }
            }



        };
        this.GetDataSource = function () {
            return DataSet;
        };

        var OnCheckBoxClick = function () {

            var ctrl = $(this);
            var classes = ctrl.attr('class').split(' ');

            //identify data row checkboxes
            if (classes.indexOf("chkPay") > -1) {
                var curRank = ctrl.attr('data-Rank');

                //check all previous check box
                var totalRank = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                for (var i = 1; i <= totalRank; i++) {
                    var chkContrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row .chkPay[data-rank='" + i + "']");
                    var nearByGroup = $(".groupCheck[data-finyear='" + chkContrl.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                    if (ctrl.prop('checked') == true && i <= curRank) {
                        chkContrl.prop('checked', true);
                        if (nearByGroup.prop("checked") == false) {
                            nearByGroup.prop('checked', true);
                        }
                    } else if (i > curRank) {
                        if (nearByGroup.prop("checked") == true) {
                            nearByGroup.prop('checked', false);
                        }
                        chkContrl.prop('checked', false);
                    }
                }
            }
            //identify group check boxes
            else if (classes.indexOf("groupCheck") > -1) {
                var curFinYear = ctrl.attr("data-finyear");
                var lowerCheckbxes = $(".chkPay[data-finyear='" + curFinYear + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                var totalRowsCount = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                var lastRankOfCheckboxList = Number($(lowerCheckbxes[lowerCheckbxes.length - 1]).attr("data-rank"));
                var firstRankOfCheckboxList = Number($(lowerCheckbxes[0]).attr("data-rank"));
                //var uncheckArrayforGroupCheck = [];
                for (var i = 1; i <= totalRowsCount; i++) {
                    var trgateCheckBox = $(".chkPay[data-rank='" + i + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                    var nearByGroup = $(".groupCheck[data-finyear='" + trgateCheckBox.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                    if (ctrl.prop('checked') == true && i <= lastRankOfCheckboxList) {
                        trgateCheckBox.prop('checked', true);
                        if (nearByGroup.prop("checked") == false) {
                            nearByGroup.prop('checked', true);
                        }
                    } else if (ctrl.prop('checked') == false && i >= firstRankOfCheckboxList) {
                        if (nearByGroup.prop("checked") == true) {
                            nearByGroup.prop('checked', false);
                            $(".checkall", "#" + TblCtrl.attr('id')).prop('checked', false);
                        }
                        trgateCheckBox.prop('checked', false);
                    }
                };

            }
            //identify header check boxes
            else if (classes.indexOf("checkall") > -1) {
                var trgateCheckBoxes = $(".chkPay, .groupCheck", "#" + TblCtrl.attr('id') + " > tbody >tr");
                trgateCheckBoxes.prop('checked', ctrl.prop('checked'));
            }
            var data = searchableObject();

            PopulateData(data, function (resp) {
                ManageGroupVisibility();
            });
        };
        var ManageGroupVisibility = function () {
            $.each(ExpandexGroupList, function (index, value) {
                ExpandGroup(value);
            });
        };
        var BindEvent = function () {
            $(document).off('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
            $(document).on('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
            $(document).off('click', "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div');
            $(document).on("click", "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div', function () {
                var curDiv = $(this);
                var currGroup = curDiv.closest('tr').attr('data-finyear');
                var isExpanded = curDiv.attr('data-isExpanded');
                if (isExpanded == 'false') {
                    ExpandGroup(currGroup);
                } else {
                    CollapseGroup(currGroup);
                }

            });

            //remove scope
            //$(document).off('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);
            //$(document).on('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);

            $(document).off('click', ".proceed-payment", OnClickProceedButton);
            $(document).on('click', ".proceed-payment", OnClickProceedButton);
        };
        var ExpandGroup = function (finYear) {
            var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
            var headerDiv = headerTr.find('div');
            var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

            headerDiv.find('.state-symbol').html('-');
            headerDiv.attr('data-isExpanded', 'true');
            relatedDataRows.each(function (indx, elemnt) {
                $(elemnt).show('fast');
            });
            if ((ExpandexGroupList.indexOf(finYear) > -1) == false)//if not exist
            {
                ExpandexGroupList.push(finYear);
            }
        };
        var CollapseGroup = function (finYear) {
            var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
            var headerDiv = headerTr.find('div');
            var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

            headerDiv.find('.state-symbol').html('+');
            headerDiv.attr('data-isExpanded', 'false');
            relatedDataRows.each(function (indx, elemnt) {
                $(elemnt).hide('fast');
            });
            if ((ExpandexGroupList.indexOf(finYear) > -1))//if not exist
            {
                ExpandexGroupList.splice(ExpandexGroupList.indexOf(finYear), 1);
            }
        };
        //paramter type PTaxCollectionGiantObject which is downloaded from ~/Municipality/Collection/Get_PropertyTaxPaymentDetails
        this.SetDataSource = function (PTaxCollectionGiantObject) {
            GiantObject = PTaxCollectionGiantObject;
        };
        this.Reset = function () {
            DataSet = [];
            GiantObject = {};

            TblCtrl.find('thead').empty();
            TblCtrl.find('tbody').empty();
            TblCtrl.find('tfoot').empty();
        };
        this.GetSelectedRowObjs = function () {
            var inputs = $(".chkPay:checked", "#B98E8A35-C901-45A4-8AFF-B486BC58E199 > tbody >tr.data-row");
            var objs = [];// data-finyear, 2: data-qtrno, 3: data-rank,
            $.each(inputs, function (index, value) {
                objs.push({
                    FinYear: $(value).attr("data-finyear"),
                    QtrNo: $(value).attr("data-qtrno"),
                    IsChecked: true,
                    Rank: $(value).attr("data-rank")
                });
            });
            return objs;
        };
        this.CheckAllRow = function (checkValue) {
            TblCtrl.find('thead').find('.checkall').prop('checked', checkValue);
            TblCtrl.find('tbody').find('.groupCheck').prop('checked', checkValue);
            TblCtrl.find('tbody').find('.chkPay').prop('checked', checkValue);
        };
        (function () {
            BindEvent();
        }());
    };

    (function () {
        server.loadDistrict(function (r) {
            if (r.Status === 200) {
                $.each(r.Data, function (i, v) {
                    $(ctrls.lstdist).append("<option value='" + v.Code + "'>[" + v.Code + "]" + v.Value + "</option>");
                });
            } else {
                alertify.alert(r.Message);
            }
        });
        bindEvents();
        $(ctrls.rdioSrchBy).trigger('change');
    }());
});