﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetAllMappingByMunicipalityID = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserCounterAssociate/Get_UserCouterAssos",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.GetCountersByMunicipalityID = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnCounter/GetActiveCounters",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.Insert_Update = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserCounterAssociate/Insert_Update",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetUsersByMunicipalityID = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserCounterAssociate/GetUserByMunicipalityID",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.Delete = function (RowID, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserCounterAssociate/Delete",
                data: "{RowID:'" + RowID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                    
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var ddlCounter_data = [];
        var ddlCounter = $('#ddlCounter');
        var ddlUser_data = [];
        var ddlUser = $('#ddlUser');

        var Bind_GridOnPageLoad = function () {
            var ctr_btnRowDel = $(".btnRowDel");           
            $("#tblDetail").find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td class='RowID' style='text-align:center;display:none;'>" + rowData.RowID + "</td>";
                rowstr += "<td class='MunicipalityID' style='display:none;'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td class='UserID' style='display:none;'>" + rowData.UserID + "</td>";
                rowstr += "<td class='UserName' >" + rowData.UserName + "</td>";
                rowstr += "<td class='CounterID' style='display:none;' >" + rowData.CounterID + "</td>";
                rowstr += "<td class='CounterCode'>" + rowData.CounterCode + "</td>";
                rowstr += "<td class='DateFrom' >" + rowData.DateFrom + "</td>";
                rowstr += "<td class='DateTo' >" + rowData.DateTo + "</td>";
                rowstr += "<td style='text-align:center;cursor:pointer;'><div class='btnRowEdit'><img src='/Contents/image/edit.png'  /></div></td>";
                rowstr += "<td style='text-align:center;cursor:pointer;'><div class='btnRowDel'><img src='/Contents/image/delete.png'  /></div></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onDeleteRow = function () {               
                var RowID = $(this).closest('tr').find('.RowID').text();
                if(confirm('Are you sure want to delete  this record ??'))
                {
                    serverObject.Delete(RowID, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            alert('Delete Succesfull !!');
                            $("#tblDetail").find("tbody").empty();
                            serverObject.GetAllMappingByMunicipalityID(function (response) {
                                if (response.Status == 200) {
                                    var rowDatas = response.Data;
                                    $.each(rowDatas, function (index, value) {
                                        var rowString = getRowString(value);
                                        appendToBody(rowString);
                                    });
                                }
                            });
                        } else {
                            alert(response.Message);
                        }
                    })
                }
            };
            var onRowEdit = function () {
              //  alert($(this).closest('tr').find('.CounterID').text());
                $('#hdnRowID').val($(this).closest('tr').find('.RowID').text());
                $('#ddlUser').val($(this).closest('tr').find('.UserID').text());
                $('#ddlCounter').val($(this).closest('tr').find('.CounterID').text());
                $('#txtFromDate').val($(this).closest('tr').find('.DateFrom').text());
                $('#txtToDate').val($(this).closest('tr').find('.DateTo').text());
            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowEdit', onRowEdit);
                $(document).on('click', '.btnRowEdit', onRowEdit);

                $(document).off('click', '.btnRowDel', onDeleteRow);
                $(document).on('click', '.btnRowDel', onDeleteRow);             
            };
            var appendToBody = function (rowString) {
                $("#tblDetail").find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.GetAllMappingByMunicipalityID(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        }
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $('#btnCancel').on('click', Reset_Field);
            $('#txtFromDate').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                minDate: 'today',
                onSelect: function (date, b) {
                    var dts = date.split('/');
                    $('#txtToDate').datepicker('option', 'minDate', new Date(dts[2], dts[1],dts[0]));
                    
                }
            });
            $('#txtToDate').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                minDate: 'today',
                
            });
            $('#txtToDate,#txtFromDate').blur(function () {
                if ($(this).val().trim() == '')
                {
                    return false;
                }
                var todate = new Date();
                var dt=$(this).val().trim();
                var inputDate = dt.split('/')[1] + '-' + dt.split('/')[0] + '-' + dt.split('/')[2];// 'mm-dd-yy'               
                inputDate = new Date(inputDate);
                if(inputDate<todate)               
                {
                    $(this).val('');
                    return false;
                }
            });
        };
        var Reset_Field = function () {
            //window.location.reload(true);
            $("#hdnWardID").val('');
            $("#txtWard").val('');
            $("#ddlMunicipal").val(0);
            $("#btnSave").attr('value', 'Save');
        }
        var getDataFromView = function () {
            var RowID = $('#hdnRowID').val();
            var UserID = $('#ddlUser').val();
            var CounterID = $('#ddlCounter').val();
            var DateFrom = $('#txtFromDate').val().trim();
            var DateTo = $('#txtToDate').val().trim();

            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }
          
            var data = { RowID: RowID, UserID: UserID, MunicipalityID: 0, CounterID: CounterID, UserName: "", CounterCode: "", DateFrom: DateFrom, DateTo: DateTo };
            return data;
        };
        var feedToForm = function (data) {
            $("#txtWard").val(data.Ward);//
            $("#hdnWardID").val(data.WardID);
            $("#ddlMunicipal").val(data.MunicipalityID);
        };
        var PopulateCounters = function () {
            ddlCounter.append("<option value='0'>select Counter No.</option>");
            $.each(ddlCounter_data, function (index, value) {
                var label = value.CounterCode;
                var val = value.CounterID;
                ddlCounter.append("<option value=" + val + ">" + label + "</option>");
            });
        }
        var PopulateUsers = function () {
            ddlUser.append("<option value='0'> Select Users</option>");
            $.each(ddlUser_data, function (index, value) {
                var label = value.UserName;
                var val = value.UserID;
                ddlUser.append("<option value=" + val + ">" + label + "</option>");
            });
        }
        var Validate = function (data) {
            if (data.UserID == 0) {
                alert("Select a User Name");
                $('#ddlUser').focus();
                return false;
            }
            if (data.CounterID == 0) {
                alert("Select Counter Number");
                $('#ddlCounter').focus();
                return false;
            }
            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#txtFromDate').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#txtToDate').focus();
                return false;
            }

            var to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            var from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            var curDate = new Date();
            curDate.setHours(0, 0, 0, 0);//remove time from date
            if (curDate > new Date(to))
            {
                alert('To Date should not less then ToDay !!');
                $('#txtToDate').focus();
                return false;
            }
            if (curDate > new Date(from)) {
                alert('From Date should not less then ToDay !!');
                $('#txtFromDate').focus();
                return false;
            }
            if (new Date(from) > new Date(to))
            {
                alert('From Date should not greater then To Date !!');
                $('#txtFromDate').focus();
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.Insert_Update(formData, function (response) {
                    var status = response.Status;
                    var msg = response.Message;
                    if (status == 200) {
                        alert(msg);
                        if (msg == 'Save Succesfull')
                        {
                             window.location.reload(true);
                        }                       
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            Bind_GridOnPageLoad();
            serverObject.GetCountersByMunicipalityID(function (response) {
                ddlCounter_data = response.Data;
                PopulateCounters();
            });
            serverObject.GetUsersByMunicipalityID(function (response) {
                ddlUser_data = response.Data;
                PopulateUsers();
            });
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});