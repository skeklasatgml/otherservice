﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {

        this.ViewReviewData = function (EffectFromDT, EffectToDT, ApproveStatus, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/ReviewApproval/GetReviewData",
                data: JSON.stringify({ EffectFromDT: EffectFromDT, EffectToDT: EffectToDT, ApproveStatus: ApproveStatus }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.UpdateReviewApproval = function (data, callBack) {
            // alert(JSON.stringify({ AllCheckedData: data }));
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/ReviewApproval/ReviewApprovalUpdateData",
                data: JSON.stringify({ AllCheckedData: data }),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error-12");
                }
            });
        };
    }

    var APP_OBJECT = function (serverObject) {
        var modelData = [];

        var BindEvent = function () {

            $("#divAllAssessment").hide();
            $("#btnView").on('click', OnViewAssessmentApprovalAll);
            $("#btnApprove").on('click', OnUpdateAssesmentApproval);

            var dateFormat = "dd/mm/yy",
                from = $("#dtFrom")
                    .datepicker({
                        //defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat: "dd/mm/yy"
                    })
                    .on("change", function () {
                        to.datepicker("option", "minDate", getDate(this));
                    }),
                to = $("#dtTo").datepicker({
                    //defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: "dd/mm/yy"
                })
                    .on("change", function () {
                        from.datepicker("option", "maxDate", getDate(this));
                    });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        }

        $('#chkAll').click(function () {
            $("#tabAllAssessment tbody tr").find(".chkAppr").prop('checked', $("#chkAll").prop("checked"));
        });


        var From_ValidateForAdjustAll = function () {
            $("#frmAdjAll").validate({
                rules: {
                    dtFrom: {
                        required: true
                    },
                    dtTo: {
                        required: true
                    },
                    ddlStatus: {
                        required: true
                    }
                },
                messages: {
                    dtFrom: {
                        required: "Please select Date From."
                    },
                    dtTo: {
                        required: "Please select Date To."
                    },
                    ddlStatus: {
                        required: "Please select a Status."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                }
            });
            return $('#frmAdjAll').valid();
        }


        var uuidv4 = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        var OnViewAssessmentApprovalAll = function () {
            if (From_ValidateForAdjustAll()) {
                var EffectFromDT = $('#dtFrom').val();
                var EffectToDT = $('#dtTo').val();
                var ApproveStatus = $('#ddlStatus').val();
                //var AdjAllData = { EffectFromDT: $('#dtFrom').val(), EffectToDT: $('#dtTo').val(), ApproveStatus: $('#ddlStatus').val()};
                serverObject.ViewReviewData(EffectFromDT, EffectToDT, ApproveStatus, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        modelData = [];
                        if (response.Data.length > 0) {
                            $.each(response.Data, function (index, value) {
                                var obj = value;
                                obj.UnqID = uuidv4();
                                modelData.push(obj);
                            });
                            //console.log(modelData);
                            PopulateTable(modelData);

                            $("#chkAll").prop('checked', false);
                            if (ApproveStatus == 'Y') {
                                $("#btnApprove").prop('disabled', true);
                                $("#chkAll").prop('disabled', true);
                                $("#tabAllAssessment tbody tr").find(".chkAppr").prop('disabled', true);
                            }
                            else if (ApproveStatus == 'N') {
                                $("#btnApprove").prop('disabled', false);
                                $("#chkAll").prop('disabled', false);
                                $("#tabAllAssessment tbody tr").find(".chkAppr").prop('disabled', false);
                            }
                            $("#divAllAssessment").show();
                        }
                        else {
                            alert("No Record found!!!");

                            var tabAllAssessment = $("#tabAllAssessment");
                            tabAllAssessment.DataTable().clear().draw();
                            //tabAllAssessment.find("tbody").empty();
                            $("#divAllAssessment").hide();
                        }
                    } else {
                        alert(response.Message);
                    }
                });
            }
        }

        var PopulateTable = function (responseData) {
            var tabAllAssessment = $("#tabAllAssessment");
            tabAllAssessment.find("tbody").empty();

            tabAllAssessment.DataTable({
                destroy: true,
                data: responseData,
                columns: [
                    { 'data': 'WardName' },
                    { 'data': 'LocationName' },
                    { 'data': 'OldAssesseeNo' },
                    { 'data': 'HoldingNo' },
                    { 'data': 'AssesseeName' },

                    { 'data': 'PtaxRValue' },
                    { 'data': 'SurchargeRValue' },
                    { 'data': 'FromFinYear' },
                    { 'data': 'FromQtrNo' },
                    { 'data': 'ToFinYear' },
                    { 'data': 'ToQtrNo' },
                    { 'data': 'InsertedOn' },
                    {
                        'render': function (data, type, full, meta) {
                            //console.log(full);
                            return "<input type='checkbox' name='chkApprove' class='chkAppr' value='" + full.UnqID + "'/>";
                        },
                        "orderable": false
                    },
                    { 'data': 'ApprovedStatus' }
                ],
                order: []
            });

            //if (responseData.length > 0) {
            //    $.each(responseData, function (index, value) {
                    
            //        var rowstr = "<tr>";
            //        rowstr += "<td align='center' class='WardName'>" + value.WardName + "</td>";
            //        rowstr += "<td align='center' class='LocationName'>" + value.LocationName + "</td>";
            //        rowstr += "<td align='center' class='OldAssesseeNo'>" + value.OldAssesseeNo + "</td>";
            //        rowstr += "<td align='center' class='HoldingNo'>" + value.HoldingNo + "</td>";
            //        rowstr += "<td align='center' class='AssesseeName'>" + value.AssesseeName + "</td>";

            //        //rowstr += "<td align='center' class='v_address'>" + value.v_address + "</td>";
            //        rowstr += "<td align='center' class='PtaxRValue'>" + value.PtaxRValue + "</td>";
            //        rowstr += "<td align='center' class='SurchargeRValue'>" + value.SurchargeRValue + "</td>";
            //        rowstr += "<td align='center' class='FromFinYear'>" + value.FromFinYear + "</td>";
            //        rowstr += "<td align='center' class='FromQtrNo'>" + value.FromQtrNo + "</td>";
            //        rowstr += "<td align='center' class='ToFinYear'>" + value.ToFinYear + "</td>";
            //        rowstr += "<td align='center' class='ToQtrNo'>" + value.ToQtrNo + "</td>";
            //        rowstr += "<td align='center' class='InsertedOn'>" + value.InsertedOn + "</td>";
            //        rowstr += "<td align='center'><input type='checkbox' name='chkApprove' class='chkAppr' value='" + value.UnqID + "'/></td>"
            //        rowstr += "<td align='center' class='ApprovedStatus'>" + value.ApprovedStatus + "</td>";
            //        rowstr += "</tr>";

            //        tabAllAssessment.find("tbody").append(rowstr);
            //    });
            //    //RegistertabAllAdjEvent();

            //}
        }

        var OnUpdateAssesmentApproval = function () {
            if (confirm("Do you want to Approved the selected row / rows") == true) {
                if (confirm("Do you confirm to Approved Yes / No") == true) {
                    var AllCheckedData = GetRowData();
                    if (AllCheckedData.length > 0) {
                        if (confirm("If you Approved oence then never be back previous position of selected row / rows status.")) {
                            serverObject.UpdateReviewApproval(AllCheckedData, function (respons) {
                                if (respons.Status == 200) {
                                    OnViewAssessmentApprovalAll();
                                }
                                alert(respons.Message);
                            });
                        }
                    }
                }
            }
        }

        var GetRowData = function () {
            var ObjArr = [];
            var v_obj = {};
            var tblRows = $("#tabAllAssessment tbody tr");
            var checkedItems = tblRows.find('.chkAppr:checked');
            if (checkedItems.length > 0) {
                $.each(checkedItems, function (index, item) {
                    var RowUnqID = $(item).val();
                    var arrRowData = alasql('SELECT * FROM ?  where UnqID="' + RowUnqID + '"', [modelData]);
                    v_obj = { ReviewID: arrRowData[0].ReviewID, AssesseeID: arrRowData[0].AssesseeID, MunicipalityID: arrRowData[0].MunicipalityID, ApprovedFlag: arrRowData[0].ApprovedFlag};
                    ObjArr.push(v_obj);
                });
                console.log(ObjArr);
            } else {
                alert("Select Atleast One Recored .......!");
            }
            return ObjArr;
        };



        this.app_Initiate = function () {

            BindEvent();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});