﻿$(document).ready(function () {
    var server = function () {
        return {
            loadNames: function (data, callback) {
                var payload = { HoldingNo: doms().searchContainer.hdnHoldingNo.val(), WardId: doms().searchContainer.hdnWard.val(), LocationID: doms().searchContainer.hdnLocation.val() };
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/SearchHolding",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        callback(Response);
                        $.ajax({
                            type: 'post',
                            url: "/Municipality/Assessment/GetOtherTabDetails",
                            data: JSON.stringify(payload),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.Data._OtherTabDetails.ApplictnDt != null) {
                                    $('.txt-application-date').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.ApplictnDt).format('dd/mm/yyyy'));
                                }

                                if (response.Data._OtherTabDetails.DtOfOrder != null) {
                                    $('.txt-date-of-order').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.DtOfOrder).format('dd/mm/yyyy'));
                                }
                                $('.txt-application-no').val(response.Data._OtherTabDetails.ApplictnCaseNo);
                                $('.txt-primary-phone-no').val(response.Data._OtherTabDetails.PrimaryPhNo);
                                $('.txt-primary-email-id').val(response.Data._OtherTabDetails.PrimaryEmail);
                                $('.txt-pro-cosing-fees').val(response.Data._OtherTabDetails.ProCosingFees);
                                $('.txt-other-fees').val(response.Data._OtherTabDetails.OtherFees);
                            }
                        });
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            saveChangeName: function (data, callback) {
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/SaveNameChanges",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        callback(Response);
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            autoWard: function (data, callback) {
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/GetWards",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        callback(Response);
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            },
            autoLoation: function (data, callback) {
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/GetLocations",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        callback(Response);
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            }
        }
    }
    var constants = function () {
        return {
            effectiveFinYearQtr: function () {
                return {
                    ddlEffectiveFinYear: $(".assmt-finyear-qtr .ddl-finyear-picker"),//
                    ddlEffectiveQtr: $(".assmt-finyear-qtr .ddl-qtr")
                };
            },

            getValTag: function () {
                return $(".setting-parameters").find('.val-tag').val();
            },
            getYearId: function () {
                //year-id
                return $(".setting-parameters").find('.year-id').val();
            },
            convertTodate: function (format, date) {
                switch (format) {
                    case 'dd/mm/yyyy':
                    case 'dd/mm/yy':
                        {
                            var array = date.split('/');
                            return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                            break;
                        }
                }
            },
        };
    };
    var formData = function () {
        return {
            selectedHoldingAssmtData: function () { 
                var data = doms().btnSave.attr("data-assmt-holding");
                if (data) {
                    data = JSON.parse(data).ActualAssmtData;

                    var maxYearId = constants().getYearId();
                    var finYear = constants().effectiveFinYearQtr().ddlEffectiveFinYear.val();
                    var qtr = constants().effectiveFinYearQtr().ddlEffectiveQtr.val();
                    var ValTag = constants().getValTag();

                    data.ValYear = finYear;
                    data.ValQtr = qtr;
                    data.ValTag = ValTag;
                    data.YearId = maxYearId;
                    data.InsertedOn = null;
                    data.UpdatedOn = null;
                    data.ApprovedOn = null;
                    return data;
                }
                return null;
            },
            newlyAddedNames: function () {
                return doms().tblNameList.getRowDatas();
            }
        };
    };
    var helpers = function () {
        return {
            SetTitleModal: function (modal, title) {
                modal.find('.modal-title').text(title);
            },
            getModalDesign: function (searchedHodling) {
                if (searchedHodling) {
                    var data = searchedHodling.ActualAssmtData;
                    var str = "";
                    str += "<input type='hidden' class='hdn-modal-raw-data' value='" + JSON.stringify(searchedHodling) + "'>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Ward</label>";
                    str += "<input class='form-control' type='text' value='" + data.WardId + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Location</label>";
                    str += "<input class='form-control' type='text' value='" + doms().searchContainer.txtLocation.val() + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Holding No</label>";
                    str += "<input class='form-control' type='text' value='" + doms().searchContainer.hdnHoldingNo.val() + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-8'>";
                    str += "<label>Name</label>";
                    str += "<input class='form-control' type='text' value='" + data.AssesseeName + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-2'>";
                    str += "<label>Holding Type</label>";
                    var holdingTypeGroup = alasql("select * from ? where HoldingTypeGrpId=?", [searchedHodling._HoldingTypeGroupes, data.HoldingTypeGrp]);
                    var holdingTypeGroupName = "";
                    if (holdingTypeGroup.length) {
                        holdingTypeGroupName = holdingTypeGroup[0].HoldingTypeGrpDesc;
                    }
                    str += "<input class='form-control' type='text' value='" + holdingTypeGroupName + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-2'>";
                    str += "<label>Land Area</label>";
                    var holdingArea = "";
                    if (data.HoldArea == "S") {
                        holdingArea = "Slum";
                    } else if (data.HoldArea == "N") {
                        holdingArea = "Non-Slum";
                    } else if (data.HoldArea == "G") {
                        holdingArea = "General";
                    } else {
                        holdingArea = "None";
                    }
                    str += "<input class='form-control' type='text' value='" + holdingArea + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Annual Valuation</label>";
                    str += "<input class='form-control' type='text' value='" + data.AnnualVal + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Qtrly Prop Tax</label>";
                    str += "<input class='form-control' type='text' value='" + data.QtrProptax + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    return str;
                }
            },
        };
    };
    var validators = function () {
        return {
            validateSelectedHoldings: function () { 
                var data = doms().btnSave.attr("data-assmt-holding");
                if ((data && data.length > 0)==false) {
                    alertify.alert("Error validation", "Holding Not selected");
                    return false;
                }
                return true;
            },
            validateNewlyAddedNames: function () {
                if (doms().tblNameList.getRowDatas().length == 0) {
                    alertify.alert("Error validation", "Please add holding name in list");
                    return false;
                }
                return true;
            }
        };
    };
    var autocompletes = function () {
        return {
            autoCompleteWard: {
                source: function (request, response) {
                    var data = { Ward: request.term };
                    doms().searchContainer.hdnWard.val('');
                    if (!request.term) {
                        return;
                    }
                    server().autoWard(data, function (Response) {
                        if (Response.Status === 200) {
                            var serverResponse = Response.Data;
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        value: item.WardID,
                                        label: item.WardName
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        } else {
                            alertify.error("Error: " + Response.Message);
                        }
                    });
                },
                select: function (e, i) {
                    doms().searchContainer.hdnWard.val(i.item.value);
                    doms().searchContainer.txtWard.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            },
            autoCompleteLocation: {
                source: function (request, response) {
                    var data = { WardId: doms().searchContainer.hdnWard.val(), Location: request.term };
                    doms().searchContainer.hdnLocation.val('');
                    if (!request.term || !doms().searchContainer.hdnWard.val()) {
                        return;
                    }
                    server().autoLoation(data, function (Response) {
                        if (Response.Status === 200) {
                            var serverResponse = Response.Data;
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        value: item.LocationID,
                                        label: item.LocationName
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        } else {
                            alertify.error("Error: " + Response.Message);
                        }
                    });
                },
                select: function (e, i) {
                    doms().searchContainer.txtLocation.val(i.item.label);
                    doms().searchContainer.hdnLocation.val(i.item.value);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            }
        };
    };

    var doms = function () {
        return {
            appContainer: $(".app-container"),
            modalSearchHolding: $("#searched-holding-modal"),
            searchContainer: {
                frm: $("#frm-holding-details"),
                txtWard: $(".txtWard"),
                txtLocation: $(".txtLocation"),
                txtHolding: $(".txt-holding-no"),

                hdnWard: $(".hddn-Ward"),
                hdnLocation: $(".hddn-Location"),
                hdnHoldingNo: $(".hddn-holding-no"),
                btnSearch: $(".btn-load-review-form"),
                validate: function () {
                    var vldObj = {
                        rules: {},
                        messages: {},
                        errorPlacement: {}
                    };
                    //enable hidden field vallidation
                    vldObj.ignore = [];

                    //set rules
                    vldObj.rules[doms().searchContainer.hdnWard.prop('name')] = { required: true };
                    vldObj.rules[doms().searchContainer.hdnLocation.prop('name')] = { required: true };
                    vldObj.rules[doms().searchContainer.hdnHoldingNo.prop('name')] = { required: true };

                    //set error messages
                    vldObj.messages[doms().searchContainer.hdnWard.prop('name')] = { required: "Please enter Ward." };
                    vldObj.messages[doms().searchContainer.hdnLocation.prop('name')] = { required: "Please enter Location." };
                    vldObj.messages[doms().searchContainer.hdnHoldingNo.prop('name')] = { required: "Please enter Holding No" };

                    //place errors in position
                    vldObj.errorPlacement = function (error, element) {
                        if ($(element).parent(".input-group").length > 0) {
                            error.insertAfter($(element).parent(".input-group"));
                            error.css('color', 'red');
                        } else {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    }

                    //push validator object to tergate form
                    doms().searchContainer.frm.validate(vldObj);
                    //validate form and return true and false
                    return doms().searchContainer.frm.valid();
                }
            },
            tblNameList: {
                _obj: $(".tbl-name-list>table"),
                getRows: function () {
                    return doms().tblNameList._obj.find('tbody').find('.data')
                },
                emptyBody: function () {
                    doms().tblNameList._obj.find('tbody').empty();
                    //doms().tblNameList._obj.find('tbody').append(doms().tblNameList.getEmptyRowTemplate())
                },
                addRow: function (tr) {
                    doms().tblNameList._obj.find('tbody').append(tr);
                },
                deleteRow: function (tr) {
                    //doms().tblNameList._obj.find('tbody')
                    tr.remove();
                },
                getEmptyRowTemplate: function () {
                    return "<tr class='empty-row text-center'><td colspan='2'>No Name In List</td></tr>";
                },
                getRowTemplate: function (rowObj) {
                    var str = "";
                    var arr = [];
                    if (!(rowObj.constructor == Array)) {
                        arr.push(rowObj);
                    } else {
                        arr = rowObj;
                    }
                    $.each(arr, function (index, value) {
                        str += "<tr class='data'>";
                        str += "<td></td>";
                        str += "<td><input type='text' class='form-control txt-name' value='" + value.AssesseeName + "'/></td>";
                        str += "<td style='width:100px' class='text-center'><a href='javacript:void(0);' class='btn-remove-name'><i class='fa fa-trash-o'></i></a></td>"
                        str += "</tr>";
                    });
                    return str;
                },
                getRowDatas: function (tr) {
                    var arr = [];
                    if (tr) {
                        //single data
                        arr.push({ AssesseeName: tr.find('.txt-name').val() })
                    } else {
                        //all data
                        arr= doms().tblNameList.getRows().map(function (indx, val) {
                            return { AssesseeName: $(val).find('.txt-name').val() };
                        }).get();
                    }
                    return arr;
                },
                validateRow: function (tr) {
                    doms().tblNameList._obj.find('tfoot .error').hide().text('');
                    var name = tr.find('.txt-name').val();
                    if (!name.trim()) {
                        tr.find('.txt-name').siblings('.error').show().text('Enter Name');
                        return false;
                    }
                    return true;
                },
                reNumberingTableRows: function () {
                    var table = doms().tblNameList._obj;
                    $.each(table.find('tbody tr.data'), function (index, value) {
                        $(value).find('td:eq(0)').text((++index));
                    })
                }
            },
            btnSave: $(".btn-complete-name-change")
        };
    };

    var app = function () {
        var eventHandlers = function () {
            return {
                onSearchButtonClicked: function (e) {
                    e.preventDefault();
                    doms().tblNameList.emptyBody();
                    $(this).attr('data-assmt-holding', '');
                    //empty modal content
                    doms().modalSearchHolding.find('.modal-body').empty();
                    //reset title
                    helpers().SetTitleModal(doms().modalSearchHolding, "");
                    if (!doms().searchContainer.validate()) {
                        return;
                    }
                    var payload = { Holding: doms().searchContainer.hdnHoldingNo.val(), WardId: doms().searchContainer.hdnWard.val(), LocationID: doms().searchContainer.hdnLocation.val() };
                    server().loadNames(payload, function (Response) {
                        if (Response.Status === 200 && Response.Data) {
                            doms().modalSearchHolding.find('.modal-body').append(helpers().getModalDesign(Response.Data));
                            helpers().SetTitleModal(doms().modalSearchHolding, "Details for holding- " + doms().searchContainer.hdnHoldingNo.val());
                            doms().modalSearchHolding.modal('show');

                        } else {
                            alertify.alert("Error!", "Error: " + Response.Message);
                        }
                    })
                },
                onLoadNamesClicked: function (e) {
                    e.preventDefault();
                    doms().btnSave.attr('data-assmt-holding', '');
                    if (!doms().searchContainer.validate()) {
                        return;
                    }
                    var holdingData = $(".hdn-modal-raw-data");

                    if (holdingData.length > 0 && holdingData.val()) {
                        var data = JSON.parse(holdingData.val());

                        if (data.hasOwnProperty("ActualAssmtData") && data.ActualAssmtData)
                            var list = data._HoldingDetails.Owners;
                        list.map(function (val, indx) {
                            var row = doms().tblNameList.getRowTemplate(val);
                            doms().tblNameList.addRow(row);
                            eventHandlers().onRowAdded(row);
                            doms().modalSearchHolding.modal('hide');
                        })
                        doms().btnSave.attr('data-assmt-holding', JSON.stringify(data));
                    } else {
                        doms().tblNameList.addRow(doms().tblNameList.getEmptyRowTemplate());
                        alertify.alert("Error!", r.Message);
                    }

                },
                onAddNewRowClicked: function (e) {

                    if ($(this).hasClass('txt-name') && $(this).attr('type') == 'text') {
                        if (e.which != 13) {
                            if (e.which === 188) {
                                e.preventDefault();
                            }
                            return;
                        }
                    }
                    e.preventDefault();
                    var currTr = $(this).closest('tr');
                    if (doms().tblNameList.validateRow(currTr)) {
                        var rowData = doms().tblNameList.getRowDatas(currTr);
                        var rowTemp = doms().tblNameList.getRowTemplate(rowData);
                        doms().tblNameList.addRow(rowTemp);
                        eventHandlers().onRowAdded(rowTemp);
                        currTr.find('.txt-name').val('');
                    }
                },
                onRowAdded: function (tr) {
                    var tr = doms().tblNameList._obj.find('tbody tr.empty-row');
                    if (tr.length > 0) {
                        tr.remove();
                    }
                    doms().btnSave.show();
                    doms().tblNameList.reNumberingTableRows();
                },
                onRowRemoved: function (tr) {
                    var tr = doms().tblNameList._obj.find('tbody tr.empty-row');
                    
                    if (!doms().tblNameList.getRows().length > 0) {
                        doms().btnSave.hide();
                        if (!tr.length > 0) {
                            doms().tblNameList.addRow(doms().tblNameList.getEmptyRowTemplate());
                        }
                    }
                    doms().tblNameList.reNumberingTableRows();
                }
            };
        };
        var bindEvent = function () {
            $(doms().searchContainer.btnSearch).on('click', eventHandlers().onSearchButtonClicked);
            $(".btn-select-holding").on('click', eventHandlers().onLoadNamesClicked);
            $(".btn-add-new").on('click', eventHandlers().onAddNewRowClicked);
            $(".txt-name").on('keydown', eventHandlers().onAddNewRowClicked);
            $(doms().tblNameList._obj).on('click', '.btn-remove-name', function (e) {
                var curRow = $(this).closest('tr');
                doms().tblNameList.deleteRow(curRow);
                eventHandlers().onRowRemoved(curRow);
            });
            $(doms().searchContainer.txtWard).autocomplete(autocompletes().autoCompleteWard);
            $(doms().searchContainer.txtLocation).autocomplete(autocompletes().autoCompleteLocation);
            $(doms().searchContainer.txtHolding).on('change', function () {
                doms().searchContainer.hdnHoldingNo.val($(this).val());
            });
            $(doms().btnSave).on('click', function (e) {
                e.preventDefault();
                if (validators().validateNewlyAddedNames() && validators().validateSelectedHoldings()) {
                    alertify.confirm("Confirm us!", "Are you sure to save data?", function () {
                        var payload = { ExistingHolding: formData().selectedHoldingAssmtData(), Names: formData().newlyAddedNames() };
                        payload.ExistingHolding.BuildingDtl = $(".txt-building-dtl").val();
                        payload.ExistingHolding.ApplNo = $(".txt-application-no").val();
                        payload.ExistingHolding.ApplDt = $(".txt-application-date").val();
                        payload.ExistingHolding.DtOfOrder = $(".txt-date-of-order").val();
                        payload.ExistingHolding.ProcessingFees = $(".txt-pro-cosing-fees").val();
                        payload.ExistingHolding.OtherFees = $(".txt-other-fees").val();
                        payload.ExistingHolding.PrimaryEmail = $(".txt-primary-email-id").val();
                        payload.ExistingHolding.PrimaryPhNo = $(".txt-primary-phone-no").val();
                        console.log(payload);
                        server().saveChangeName(payload, function (r) {
                            if(r.Status===200)
                            {
                                alertify.alert("Successful!", r.Message, function () {
                                    location.reload();
                                });
                            } else {
                                alertify.alert("Error!", r.Message, function () { });
                            }
                        });
                    }, function () { });
                }
            });
            $(document).on('change', ".chk-inheritance", function () {
                $(".txt-date-of-death,.txt-date-cert-no").prop("disabled",!$(this).is(":checked"));
            });
            $(".chk-inheritance").trigger('change');
        };
        this.init = function () {
            bindEvent();
        };
    };
    (function () {
        new app().init();
    }())
});



//var a = function () {
//    return {
//        b: function () {
//            return {
//                e: function () {
//                    return {
//                        d: function () {
//                            return {
//                                f: function () {
//                                    return {
//                                        g: function () {
//                                            return {
//                                                h: function () {
//                                                    return {
//                                                        i: function () {
//                                                            return {
//                                                                j: function () { console.log('end of the function')}
//                                                            }
//                                                        }
//                                                    }
//                                                }

//                                            }
//                                        }
//                                    }
                                
//                                }
//                            }
//                        }

//                    }
//                }
//            }
//        },
//        c: function () {
//            //another function
//        }
//    }
//}