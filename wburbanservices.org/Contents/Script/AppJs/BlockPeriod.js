﻿

//<reference path="../jquery.min.js" />



$(function () {
    var frm = $('#blcokPeriod');
    app.validation();
    app.getAllBlockPeriod();
    $('#btnsubmit').click(function () {
        if (!frm.valid()) {
            return false;
        }
        app.save(function (res) {
            app.getAllBlockPeriod();
        });
        //app.getAllBlockPeriod();
        app.clear();
    });

    $(document).on('blur', '#txtfinyear', function () {
        app.checkFinYear(this);
    });

    $(document).on('blur', '#txtperiod', function () {

        if (!($('#txtperiod').val().indexOf('.') > -1)) {
            $('#txtperiod').val($('#txtperiod').val() + '.0');
        } 
    });

    $(document).on('click', '.btnRowEdit', function () {
        app.rowEdit(this);
    });

    $('#btncancel').click(function () {
        app.clear();
    });
});

var app = {
    MunicipalityID: null,
    YearId: null,
    checkFinYear: function (ref) {
        //var finyear=$(ref).closest('tr').find('finyear').val();
        var finyear = $(ref).val();
        var obj = { "finyear": finyear}
            $.ajax({
                url: '/Municipality/BlockPeriod/checkFinYear',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                dataType: 'json',
                success: function (response) {
                    console.log(response.Data);
                    if (response.Data.length > 0) {
                        alert('Already Exist');
                        $('#txtfinyear').val('');
                        $('#txtfinyear').focus();
                    }
                },
                error: function () { }
            });
    },
    getFormdata: function () {
        var ctrl_Finyear = $("#txtfinyear");
        var ctrl_Qtr = $('#qtr');
        var ctrl_txtperiod = $('#txtperiod');
        var ctrl_txtdiscount = $('#txtdiscount');
        var ctrl_txtfactor = $('#txtfactor');
        var ctrl_txtminimum = $('#txtminimum');
        var ctrl_txtmaxrate = $('#txtmaxrate');
        var ctrl_txtless1000 = $('#txtless1000');
        var ctrl_txtgreater1000 = $('#txtgreater1000');

        return {
            MunicipalityID: app.MunicipalityID,
            YearId:app.YearId,
            FinYear: ctrl_Finyear.val(),
            Qtr: ctrl_Qtr.val(),
            Period: ctrl_txtperiod.val(),
            DiscPer: ctrl_txtdiscount.val(),
            Factor: ctrl_txtfactor.val(),
            MinVal: ctrl_txtminimum.val(),
            MaxPtaxPer: ctrl_txtmaxrate.val(),
            AddPtaxPer1: ctrl_txtless1000.val(),
            AddPtaxPer2: ctrl_txtgreater1000.val()
        }
    },
    save: function (callback) {
        //var finyear = $('#txtfinyear')
        //var obj = { "parent": app.ParentId, "acDesc": acDesc, "GLS": GLS, "IELA": IELA, "branchId": branchId, "acno": acno, "ISBank": ISBank, "achead": achead }
        $.ajax({
            url: '/Municipality/BlockPeriod/SaveBlock',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(app.getFormdata()),
            dataType: "json",
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
                   


            },
            error: function () { }
        });
    },
   
    getAllBlockPeriod: function () {
        $.ajax({
            url: '/Municipality/BlockPeriod/GetAllBlockPeriod',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: '',
            dataType: 'json',
            success: function (response) {
                //alert(response.Message);
                app.popAllBlockPeriod(response.Data);
            },
            error: function (response) {
                alert(response.Message);
            }

        });
    },
    popAllBlockPeriod: function (data) {
        var tbl_Block = $('#tbl_BlockPeriod');
        tbl_Block.empty();
        tbl_Block.append('<thead><tr>'
            + '<th>Fin Year</th>'
            + '<th>Qtr</th>'
            + '<th>Period</th>'
            + '<th>Discount</th>'
            + '<th>Factor</th>'
            + '<th>Minimum</th>'
            + '<th>Maximum</th>'
            + '<th>Rate<1000</th>'
            + '<th>Rate>=1000</th>'
            +'<th>Edit</th>'
            + '</tr ></thead >');
        $.each(data, function (i, element) {
            tbl_Block.append('<tbody><tr>'
                + '<td class=MunicipalityID style=display:none;>' + element.MunicipalityID + '</td>'
                + '<td class=YearId style=display:none;>' + element.YearId+'</td>'
                + '<td class=YearDesc>' + element.YearDesc + '</td>'
                + '<td class=QtrDesc>' + element.QtrDesc + '</td>'
                + '<td class=BlockPeriod>' + element.BlockPeriod + '</td>'
                + '<td class=DiscPer>' + element.DiscPer + '</td>'
                + '<td class=Factor>' + element.Factor + '</td>'
                + '<td class=MinVal>' + element.MinVal + '</td>'
                + '<td class=MaxPtaxPer>' + element.MaxPtaxPer + '</td>'
                + '<td class=AddPtaxPer1>' + element.AddPtaxPer1 + '</td>'
                + '<td class=AddPtaxPer2>' + element.AddPtaxPer2 + '</td>'
                + "<td style='text-align:center;cursor:pointer;'><div class='btnRowEdit'><img src='/Contents/image/edit.png'  /></div></td>"
                + '</tr ></tbody');
        });
    },
    rowEdit: function (ref) {
        var ctrl_Finyear = $("#txtfinyear");
        var ctrl_Qtr = $('#qtr');
        var ctrl_txtperiod = $('#txtperiod');
        var ctrl_txtdiscount = $('#txtdiscount');
        var ctrl_txtfactor = $('#txtfactor');
        var ctrl_txtminimum = $('#txtminimum');
        var ctrl_txtmaxrate = $('#txtmaxrate');
        var ctrl_txtless1000 = $('#txtless1000');
        var ctrl_txtgreater1000 = $('#txtgreater1000');

         app.MunicipalityID = $(ref).closest('tr').find('.MunicipalityID').html();
         app.YearId = $(ref).closest('tr').find('.YearId').html();

        ctrl_Finyear.val($(ref).closest('tr').find('.YearDesc').html());
        ctrl_Qtr.val($(ref).closest('tr').find('.QtrDesc').html());
        ctrl_txtperiod.val($(ref).closest('tr').find('.BlockPeriod').html());
        ctrl_txtdiscount.val($(ref).closest('tr').find('.DiscPer').html());
        ctrl_txtfactor.val($(ref).closest('tr').find('.Factor').html());
        ctrl_txtminimum.val($(ref).closest('tr').find('.MinVal').html());
        ctrl_txtmaxrate.val($(ref).closest('tr').find('.MaxPtaxPer').html());
        ctrl_txtless1000.val($(ref).closest('tr').find('.AddPtaxPer1').html());
        ctrl_txtgreater1000.val($(ref).closest('tr').find('.AddPtaxPer2').html());

        $('#btnsubmit').html('Update');

        //var obj = { "MunicipalityID": MunicipalityID, "YearId": YearId }

        //$.ajax({
        //    url: '/Municipality/BlockPeriod/GetAllBlockPeriodById',
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    data: JSON.stringify(obj),
        //    dataType: 'json',
        //    success: function (response) {

        //    },
        //    error: function () { }
        //});
       
    },
    validation: function () {
        $.validator.addMethod('qtr', function (value, element) {
            return value != "0";
        });

        

        $('#blcokPeriod').validate({
            rules: {
                txtfinyear: { required: true },
                qtr: { qtr:true},
                txtperiod: { required: true },
                txtdiscount: { required: true },
                txtfactor: {required:true},
                txtminimum: {required:true},
                txtmaxrate: {required:true},
                txtless1000: {required:true},
                txtgreater1000: {required:true}
            },
            messages: {
                txtfinyear: { required: "Please Enter Year." },
                qtr: { qtr:"Please Select Quarter"},
                txtperiod: { required: "Please Enter Period" },
                txtdiscount: { required: "Please Enter Discount" },
                txtfactor: { required:"Please Enter Multiplying Factor"},
                txtminimum: { required:"Please Enter Minimum Valuation"},
                txtmaxrate: { required:"Please Enter Max Rate of PTax"},
                txtless1000: { required:"Please Enter If valuation < 1000 then divide 100 and Add"},
                txtgreater1000: { required:"Please Enter If valuation >=1000 then divide 1000 and Add"}
            }
        });

    },
    clear: function () {
        var ctrl_Finyear = $("#txtfinyear");
        var ctrl_Qtr = $('#qtr');
        var ctrl_txtperiod = $('#txtperiod');
        var ctrl_txtdiscount = $('#txtdiscount');
        var ctrl_txtfactor = $('#txtfactor');
        var ctrl_txtminimum = $('#txtminimum');
        var ctrl_txtmaxrate = $('#txtmaxrate');
        var ctrl_txtless1000 = $('#txtless1000');
        var ctrl_txtgreater1000 = $('#txtgreater1000');

        app.MunicipalityID = null,
        app.YearId=null,
        ctrl_Finyear.val('');
        ctrl_Qtr.val('0');
        ctrl_txtperiod.val('');
        ctrl_txtdiscount.val('');
        ctrl_txtfactor.val('');
        ctrl_txtminimum.val('');
        ctrl_txtmaxrate.val('');
        ctrl_txtless1000.val('');
        ctrl_txtgreater1000.val('');
        $('#btnsubmit').text('Save');
    }
}