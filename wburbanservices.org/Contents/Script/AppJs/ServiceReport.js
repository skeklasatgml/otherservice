﻿var mode = '', ApplicationId = '', UserType = '';
var modalControl = '', ServiceID = "", MuncipalityID="";
$(document).ready(function () {

    modalControl = $('#modal_MakePaymentComplete');

    $("#btnPrint").click(function (evt) {
        Print();
    });
    $("#btnRefresh").click(function (evt) {
        Refresh();
    });
    $("#btnMakePayment").click(function (evt) {
        MakePayment();
    });

    ApplicationId = getParameterByName("ApplicationId", window.location.href);
    mode = getParameterByName("mode", window.location.href);
    //ServiceID = getParameterByName("ServiceID", window.location.href); 
    //MuncipalityID = getParameterByName("MuncipalityID", window.location.href); //alert(ss); return false;

    if (ApplicationId != null) {
        Load(ApplicationId, mode);
        $(".clsBackButton").css('display', '');
        UserType = $('.clsVariables').attr('usertype'); //alert(UserType);
    }
    else {
        $(".clsBackButton").css('display', 'none');
    }

});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return ''; 
    return decodeURIComponent(results[2]);
    //return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function AllowDecimal() {
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}
function Bind_Date() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();

    $('#txtStartofWork, #txtEndofWork,#txtVerificationDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            //$("#txtEndofWork").datepicker("option", "maxDate", $("#txtStartofWork").val());
            $("#txtEndofWork").datepicker("option", "minDate", $("#txtStartofWork").val());
        }
    });


    $("#txtVerificationDate").val(output);
    $("#txtStartofWork").datepicker("option", "minDate", output);
}
function AutoComplete_Municipality() {
    $('#ddlMunicipality').autocomplete({
        source: function (request, response) {

            var GLS = "S";

            var S = "{Desc:'" + $('#ddlMunicipality').val() + "'}"; //alert(S);
            $.ajax({
                url: '/OtherServices/ApplicationForm/AutoComplete_Municipality',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = []; //alert(JSON.stringify(serverResponse.Data));

                    if (serverResponse.Data == null) {
                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.MunicipalityName,
                                MunicipalityID: item.MunicipalityID
                            });
                        });

                        response(AutoComplete);
                    }
                    else {
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });

                            response(AutoComplete);
                        }
                    }

                }
            });
        },
        select: function (e, i) {
            $('.clsVariables').attr('automunicipalityid', i.item.MunicipalityID);
            Bind_Ward();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#ddlMunicipality').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#ddlMunicipality").val('');
            $('.clsVariables').attr('automunicipalityid', '');
        }
        if (iKeyCode == 46) {
            $("#ddlMunicipality").val('');
            $('.clsVariables').attr('automunicipalityid', '');
        }
    });
}
function Bind_TypeofRoad(RoadTypeId) {
    if ($("#ddlTypeofSurface").val() != "") {
        var E = "{SurfaceID: '" + $("#ddlTypeofSurface").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_TypeofRoad',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                $('#ddlTypeofRoad').empty();
                $('#ddlTypeofRoad').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlTypeofRoad').append('<option value=' + item.RoadTypeID + '>' + item.RoadTypeName + '</option>');
                        });
                        if (RoadTypeId != "") $('#ddlTypeofRoad').val(RoadTypeId);
                    }
                }
            }
        });
    }
}
function Bind_Ward() {
    var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
    if (MunicipalityID != "") {
        var E = "{MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_Ward',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlWard').empty();
                $('#ddlWard').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlWard').append('<option value=' + item.WardID + '>' + item.WardName + '</option>');
                        });
                    }
                }
            }
        });
    }
}
function Bind_Location(LocationId) {
    if ($("#ddlWard").val() != "") {
        var MunicipalityID = $('.clsVariables').attr('automunicipalityid');
        var E = "{WardID: '" + $("#ddlWard").val() + "', MunicipalityID: '" + MunicipalityID + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_LocationByWard',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlLocation').empty();
                $('#ddlLocation').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlLocation').append('<option value=' + item.LocationID + '>' + item.LocationName + '</option>');
                        });
                    }
                    if (LocationId != "") $('#ddlLocation').val(LocationId);
                }
            }
        });
    }
}


function Show_ddlSpecification() {
    if ($("#ddlSpecification").val() != "") {
        var E = "{SpecificationID: '" + $("#ddlSpecification").val() + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Show_Specification',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;

                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        var SpecificationRWidth = item.SpecificationRWidth;
                        var SpecificationRDepth = item.SpecificationRDepth;
                        var SpecificationPNo = item.SpecificationPNo;
                        var SpecificationPLength = item.SpecificationPLength;
                        var SpecificationPWidth = item.SpecificationPWidth;
                        var SpecificationPDepth = item.SpecificationPDepth;
                        var SpecificationTDiameter = item.SpecificationTDiameter;

                        if (SpecificationRWidth == true) $("#RWidth").css('display', ''); else $("#RWidth").css('display', 'none');
                        if (SpecificationRDepth == true) $("#RDepth").css('display', ''); else $("#RDepth").css('display', 'none');
                        if (SpecificationPNo == true) $("#PNo").css('display', ''); else $("#PNo").css('display', 'none');
                        if (SpecificationPLength == true) $("#PLength").css('display', ''); else $("#PLength").css('display', 'none');
                        if (SpecificationPWidth == true) $("#PWidth").css('display', ''); else $("#PWidth").css('display', 'none');
                        if (SpecificationPDepth == true) $("#PDepth").css('display', ''); else $("#PDepth").css('display', 'none');
                        if (SpecificationTDiameter == true) $("#TDiameter").css('display', ''); else $("#TDiameter").css('display', 'none');
                    });
                }
            }
        });
    }
}
function EditOtherChargeDetail(ID) {
    $("#ddlOtherCharge").val($(ID).closest('tr').find('.clsChargeID').text());
    $("#txtOtherAmount").val($(ID).closest('tr').find('.clsOtherAmt').text());
    $(".clsVariables").attr('otherchargeMainID', $(ID).closest('tr').find('.clsID').text())
    $(ID).closest('tr').remove();
}
function DeleteOtherChargeDetail(ID, detlID) {
    var res = confirm('Are you sure want to delete ?');
    if (res == true) {

        if (detlID > 0) {
            var E = "{OtherChargeID: '" + detlID + "'} ";
            $.ajax({
                type: "POST",
                url: '/OtherServices/ApplicationForm/Delete_OtherCharge',
                contentType: "application/json; charset=utf-8",
                data: E,
                dataType: "json",
                success: function (responce) {
                    var xmlDoc = $.parseXML(responce.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    var Result = $.trim($(Table).find("Result").text());
                    if (Result == 1)
                        $(ID).closest('tr').remove();
                    else
                        alert('Sorry ! There is some problem.');
                }
            });
        }
        else {
            $(ID).closest('tr').remove();
        }
    }
}

function SpecificationString() {
    var retString = "";
    var spectiontxt = $("#ddlSpecification option:selected").text();

    var SpecificationRWidth = $.trim($("#RWidth").val() == "" ? "0" : $("#RWidth").val());
    var SpecificationRDepth = $.trim($("#RDepth").val() == "" ? "0" : $("#RDepth").val());
    var SpecificationPNo = $.trim($("#PNo").val() == "" ? "0" : $("#PNo").val());
    var SpecificationPLength = $.trim($("#PLength").val() == "" ? "0" : $("#PLength").val());
    var SpecificationPWidth = $.trim($("#PWidth").val() == "" ? "0" : $("#PWidth").val());
    var SpecificationPDepth = $.trim($("#PDepth").val() == "" ? "0" : $("#PDepth").val());
    var SpecificationTDiameter = $.trim($("#TDiameter").val() == "" ? "0" : $("#TDiameter").val());

    if (SpecificationRWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RWidth: " + SpecificationRWidth
    }
    if (SpecificationRDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RDepth: " + SpecificationRDepth
    }
    if (SpecificationPNo != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PNo: " + SpecificationPNo
    }
    if (SpecificationPLength != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PLength: " + SpecificationPLength
    }
    if (SpecificationPWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PWidth: " + SpecificationPWidth
    }
    if (SpecificationPDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PDepth: " + SpecificationPDepth
    }
    if (SpecificationTDiameter != "") {
        retString = (retString == "" ? "" : retString + ", ") + "TDiameter: " + SpecificationTDiameter
    }

    return spectiontxt + ", " + retString;
}

function Save() {

    var ArrList = [], ArrOtherCrgList = [];
    var municipalityID = $('.clsVariables').attr('automunicipalityid'); //$("#ddlMunicipality").val();
    var purposeID = $("#ddlPurpose").val();
    var projectName = $("#txtProjectName").val();

    if (municipalityID == "") { alert('Please Select Project Municipality/Corporation.'); $("#ddlMunicipality").focus(); return false; }
    if (purposeID == "") { alert('Please Select Purpose of Road Cutting.'); $("#ddlPurpose").focus(); return false; }
    if (projectName == "") { alert('Please Enter Name of project.'); $("#txtProjectName").focus(); return false; }

    //var l = $('#tbl tr.myData').length; 
    //if (l == 0) {
    //    alert('Please Add Project details.'); return false;
    //}

    //var imgCnt = 0, ismaxCnt = 0, qty = 0, docName = "";
    //$('#tblimg tbody tr.myData').each(function () {

    //    var Id = $(this).find('.flimg').attr('id');
    //    var isMand = $(this).find('.flimg').attr('mandatory'); 
    //    var maxqty = $(this).find('.flimg').attr('maxQuantity'); var restmaxqty = $(this).find('.flimg').attr('maxQuantity');
    //    var doctName = $(this).find('.flimg').attr('docName');
    //    var doctid = $(this).find('.flimg').attr('doctypeid'); 
    //    var l = $('#tblimg tr.img_' + doctid).find('tr').length; //alert(l); //return false;
    //    //maxqty = maxqty - l;

    //    if (isMand == 1)
    //    {
    //        //alert(maxqty);
    //        if (maxqty > 0) {
    //            var totalFiles = document.getElementById(Id).files.length + l;
    //            if (totalFiles == 0) {
    //                imgCnt = 1;
    //                docName = doctName;
    //            }
    //            if ((totalFiles > (maxqty)) || (totalFiles < (maxqty))) {
    //                ismaxCnt = 1;
    //                docName = doctName;
    //                qty = maxqty;
    //            }
    //        }
    //    }
    //});
    //if (imgCnt == 1)
    //{
    //    alert('Sorry ! Please Select File/Image for ' + docName); return false;
    //}
    //if (ismaxCnt == 1) {
    //    alert('Sorry ! Please Select ' + qty + ' File/Image ' +  'for ' + docName); return false;
    //}
    //DETAIL DATA
    $('#tbl tbody tr.myData').each(function () {

        var clsDetailID = $(this).find('.clsDetailID').text();
        var clsStatus = $(this).find('.clsStatus').text();

        var clsTypService = $(this).find('.clsTypService').text();
        var clsTypRoad = $(this).find('.clsTypRoad').text();

        var clsLongStart = $(this).find('.clsLongStart').text();
        var clsLongEnd = $(this).find('.clsLongEnd').text();
        var clsLatStart = $(this).find('.clsLatStart').text();
        var clsLatEnd = $(this).find('.clsLatEnd').text();

        var clsWard = $(this).find('.clsWard').text();
        var clsLocation = $(this).find('.clsLocation').text();
        var clsLandMark = $(this).find('.clsLandMark').text();
        var clsBorough = $(this).find('.clsBorough').text();
        var clsStartofWork = $(this).find('.clsStartofWork').text();
        var clsEndofWork = $(this).find('.clsEndofWork').text();

        var clsEscavation = $(this).find('.clsEscavation').text();
        var clsSpecification = $(this).find('.clsSpecification').text();
        var clsSpecificationText = $(this).find('.clsSpecificationText').text(); //alert(clsSpecificationText);
        var RWidth = '', RDepth = '', PNo = '', PLength = '', PWidth = '', PDepth = '', TDiameter = '';
        if (clsSpecificationText != "") {
            var ls = clsSpecificationText.split(',');
            if (ls.length > 0) {
                //alert(ls);
                for (var i = 0; i < ls.length; i++) {
                    var keyValue = ls[i].split(":");
                    if (keyValue[0].trim() == 'RWidth')
                        RWidth = keyValue[1];
                    if (keyValue[0].trim() == 'RDepth')
                        RDepth = keyValue[1];
                    if (keyValue[0].trim() == 'PNo')
                        PNo = keyValue[1];
                    if (keyValue[0].trim() == 'PLength')
                        PLength = keyValue[1];
                    if (keyValue[0].trim() == 'PWidth')
                        PWidth = keyValue[1];
                    if (keyValue[0].trim() == 'PDepth')
                        PDepth = keyValue[1];
                    if (keyValue[0].trim() == 'TDiameter')
                        TDiameter = keyValue[1];
                }
            }
        }

        var clsEstimatedCost = $(this).find('.clsEstimatedCost').val();
        var ischk = $(this).find('.chk').prop('checked');
        var clsApprove = (ischk == true ? 1 : 0); //$(this).find('.clsApprove').text();
        var clsRate = $(this).find('.clsRate').val();
        var clsCalAmount = $(this).find('.clsAmount').val();

        ArrList.push({
            'DetailID': clsDetailID, 'Status': clsStatus,
            'TypSurface': clsTypService, 'TypRoad': clsTypRoad,
            'LongStart': clsLongStart, 'LongEnd': clsLongEnd, 'LatStart': clsLatStart, 'LatEnd': clsLatEnd,

            'Ward': clsWard, 'Location': clsLocation, 'LandMark': clsLandMark, 'BoroughNo': clsBorough, 'StartofWork': clsStartofWork, 'EndofWork': clsEndofWork,

            'Escavation': clsEscavation, 'Specification': clsSpecification,
            'RWidth': RWidth, 'RDepth': RDepth, 'PNo': PNo,
            'PLength': PLength, 'PWidth': PWidth, 'PDepth': PDepth, 'TDiameter': TDiameter,
            'EstimatedAmount': clsEstimatedCost, 'VerifiedStatus': clsApprove, 'ApprovedRate': clsRate, 'ApprovedAmount': clsCalAmount
        });
    });
    //OTHER CHARGES DATA
    $('#tblOtherCharges tbody tr.myData').each(function () {

        var clsChargeID = $(this).find('.clsChargeID').text();
        var clsOtherAmt = $(this).find('.clsOtherAmt').text();

        ArrOtherCrgList.push({
            'OtherChargeID': clsChargeID, 'OtherAmt': clsOtherAmt,
        });
    });

    //alert(JSON.stringify(ArrList));
    //return false;
    var E = "{ApplicationID: '" + $('#hdnApplicationID').val() + "' , municipalityID: '" + municipalityID + "', purposeID: '" + purposeID + "', " +
        "projectName: '" + projectName + "', Action: '" + $('#ddlAction').val() + "', DesignationID: '" + $('#ddlDesignation').val() + "'," +
        "VerificationDate: '" + $('#txtVerificationDate').val() + "', Remarks: '" + $('#txtRemarks').val() + "', obj: " + JSON.stringify(ArrList) + ", objOtherCharge: " + JSON.stringify(ArrOtherCrgList) + "}";

    $.ajax({
        url: '/OtherServices/ApplicationForm/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");

            var MSGID = $.trim($(Table).find("MSGID").text());
            var MSG = $.trim($(Table).find("MSG").text());

            var ApplicationID = $.trim($(Table1).find("ApplicationID").text());
            var SubModuleID = $.trim($(Table1).find("SubModuleID").text());

            if (MSGID == 1) {
                Upload_Doc(ApplicationID, SubModuleID);
                alert(MSG);

                var modcode = $('#btnBack').attr('modcode');
                var href = $('#btnBack').attr('href');
                if (modcode == "" || modcode == null)
                    location.reload();
                else
                    window.location.href = href;
            }
            else {
                alert('Sorry ! There is some Problem.');
                return false;
            }
        }
    });
}
function Upload_Doc(ApplicationID, SubModuleID) {

    $('#tblimg tbody tr.myData').each(function () {


        var Id = $(this).find('.flimg').attr('id');
        var isMand = $(this).find('.flimg').attr('mandatory');
        var maxqty = $(this).find('.flimg').attr('maxQuantity');
        var doctName = $(this).find('.flimg').attr('docName');
        var docTypeId = $(this).find('.flimg').attr('docTypeId');

        var totalFiles = document.getElementById(Id).files.length; //alert(totalFiles);
        if (totalFiles > 0) {
            for (var i = 0; i < totalFiles; i++) {
                var formData = new FormData();
                var file = document.getElementById(Id).files[i];
                fileName = $('#' + Id).val().substring(12);
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
                formData.append(Id, file, docTypeId + "#" + ApplicationID + "#" + SubModuleID + "#" + (parseInt(i) + 1) + fileNameExt);

                $.ajax({
                    type: "POST",
                    url: '/OtherServices/ApplicationForm/Upload',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {
                    }
                });
            }
        }
    });
}
function Refresh() {
    location.reload();
}

function Load(ApplicationId, mode) {
    var E = "{ApplicationID: '" + ApplicationId + "'}";
    $.ajax({
        url: '/OtherServices/ApplicationForm/LoadReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");
            var Table2 = xml.find("Table2");

            var ApplicationID = $.trim($(Table).find("ServMstId").text());
            var ApplicationNo = $.trim($(Table).find("ApplicationNo").text());
            var MunicipalityID = $.trim($(Table).find("MunicipalityDevAuthoId").text());
            var MunicipalityName = $.trim($(Table).find("MunicipalityName").text());
            var Purpose = $.trim($(Table).find("Purpose").text());
            var ProjectName = $.trim($(Table).find("NameOfProject").text());


            //MASTER 
            $("#hdnApplicationID").val(ApplicationID); $("#txtApplicationNo").text(ApplicationNo);
            $("#ddlMunicipality").text(MunicipalityName); $('.clsVariables').attr('automunicipalityid', MunicipalityID); 
            $("#ddlPurpose").text(Purpose); $("#txtProjectName").text(ProjectName);
            //DETAIL
            Detail(xml, mode);
            //DOCUMENT
            //Document(xml, mode);
            //OTHER CHARGE
            OtherChargeDetail(xml, mode);
        }
    });
}
function Detail(xml, mode) {
    $("#tbl tr.myData").remove();
    var html = "", Amt = 0;
    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');
    $(xml).find("Table1").each(function () {
       
        var isActive = $.trim($(this).find("isActive").text());
        var Amts = $.trim($(this).find("ApprovedAmount").text()) == "" ? 0 : $.trim($(this).find("ApprovedAmount").text());
        Amt = parseFloat(Amt) + parseFloat(Amts);

        $parentTR.after("<tr class='myData' style='display:" + (isActive == 1 ? "" : "none") + "'>" +

            "<td>" + $.trim($(this).find("SurfaceName").text()) + "</td>" +
            "<td>" + $.trim($(this).find("RoadTypeName").text()) + "</td>" +

            "<td>LS: <span class='clsLongStart'>" + $.trim($(this).find("LongStart").text()) + "</span><br> LE: <span class='clsLongEnd'>" + $.trim($(this).find("LongEnd").text()) + "</span></td>" +
            "<td>LS: <span class='clsLatStart'>" + $.trim($(this).find("LatStart").text()) + "</span><br> LE: <span class='clsLatEnd'>" + $.trim($(this).find("LatEnd").text()) + "</span></td>" +


            "<td>" + $.trim($(this).find("WardName").text()) + "</td>" +

            "<td>" + $.trim($(this).find("LocationName").text()) + "</td>" +

            "<td class='clsLandMark'>" + $.trim($(this).find("LandMark").text()) + "</td>" +
            "<td class='clsBorough'>" + $.trim($(this).find("BoroughNo").text()) + "</td>" +
            "<td class='clsStartofWork'>" + $.trim($(this).find("WorkStratDate").text()) + "</td>" +
            "<td class='clsEndofWork'>" + $.trim($(this).find("WorkEndDate").text()) + "</td>" +
            "<td class='clsEscavation'>" + $.trim($(this).find("LengthOfEsca").text()) + "</td>" +

            "<td class='clsSpecificationText'>" + BindSpecificationString($.trim($(this).find("SpecificationRWidth").text()), $.trim($(this).find("SpecificationRDepth").text()),
                $.trim($(this).find("SpecificationPNo").text()), $.trim($(this).find("SpecificationPLength").text()), $.trim($(this).find("SpecificationPWidth").text()),
                $.trim($(this).find("SpecificationPDepth").text()), $.trim($(this).find("SpecificationTDiameter").text()), $.trim($(this).find("Specification").text())
            ) + "</td>" +


            "<td style='text-align:center;' >" + $.trim($(this).find("EstimatedAmount").text()) + "</td>" +
            "<td style='text-align:right;' class='clsAmount'>" + $.trim($(this).find("ApprovedAmount").text()) + "</td>" +

            "</tr > ");
    });

    var fhtml = "";
    fhtml = "<tr>" +
        "<td colspan='13' style='text-align:right; font-weight:bold;'>Total (&#x20B9;)</td>" +
        "<td style='text-align:right; font-weight:bold;' class='clsTotal'>" + Amt + "</td>" +
       // "<td></td>" +
        "</tr > ";
    $('#tbl tfoot').append(fhtml);
   

}
function Document(xml, mode) {

    var html = "";
    $("#tblimg tr.docImg td").remove();

    $(xml).find("Table2").each(function () {

        var doctypeid = $.trim($(this).find("DocTypeID").text()); //alert(doctypeid);
        var docid = $.trim($(this).find("DocId").text());


        html += "<tr><td style='width:80%; padding:5px 0px 0px 5px'>" + $.trim($(this).find("DocPath").text()) + "</td>";
        html += "<td style='display:none;' class='cslUrl'>" + $.trim($(this).find("MainUrl").text()) + "</td>";
        html += "<td style='display:none;' class='clsdocid'>" + $.trim($(this).find("ServDocId").text()) + "</td>";
        html += "<td style='width:30%; padding:5px 0px 5px 5px'>&nbsp;<img src='/Contents/image/paperclip.png' style='width:17px;height:15px; cursor:pointer; font-size:15px; top:5px !important;' onClick= 'ViewImg(this);' />"; //<img class='imgView' style= 'width:15px;height:15px;cursor:pointer; margin-left:10px; text-align:center;'  />
        html += "<span class='glyphicon glyphicon-trash imgDel' style='color:red; margin-left:10px; font-size:15px; cursor:pointer; text-align:center; top:5px !important;'  onClick='DeleteImg(this);'></span></td ></tr>";
        $("#tblimg tr.img_" + doctypeid).append(html);
        html = "";
    });
    ApplyOption(mode);
}
function OtherChargeDetail(xml, mode) {
    $("#tblOtherCharges tr.myData").remove();
    var html = "", Amt = 0;
    var $this = $('#tblOtherCharges .test_0');
    $parentTR = $this.closest('tr');

    var l = $(xml).find("Table3").length;
    if (l > 0) {
        $(xml).find("Table3").each(function () {

            $parentTR.after("<tr class='myData'>" +

                "<td style='display:none;' class='clsID' >" + $.trim($(this).find("ID").text()) + "</td>" +

                "<td style='display:none;' class='clsChargeID' >" + $.trim($(this).find("OtherChargeID").text()) + "</td>" +
                "<td>" + $.trim($(this).find("ChargeName").text()) + "</td>" +

                "<td class='clsOtherAmt'  style='text-align:right;'>" + $.trim($(this).find("Amount").text()) + "</td>" +

                "</tr > ");
        });
    }
    else
        $('#tblOtherCharges').css('display', 'none');

    CalculateFinal();
}
function BIND() {
    if (UserType == 'MunicipalityUser') {

    }
}
function BindSpecificationString(RWidth, RDepth, PNo, PLength, PWidth, PDepth, TDiameter, Specification) {
    var retString = "";
    var spectiontxt = Specification;

    var SpecificationRWidth = RWidth;
    var SpecificationRDepth = RDepth;
    var SpecificationPNo = PNo;
    var SpecificationPLength = PLength;
    var SpecificationPWidth = PWidth;
    var SpecificationPDepth = PDepth;
    var SpecificationTDiameter = TDiameter;


    if (SpecificationRWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RWidth: " + SpecificationRWidth
    }
    if (SpecificationRDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "RDepth: " + SpecificationRDepth
    }
    if (SpecificationPNo != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PNo: " + SpecificationPNo
    }
    if (SpecificationPLength != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PLength: " + SpecificationPLength
    }
    if (SpecificationPWidth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PWidth: " + SpecificationPWidth
    }
    if (SpecificationPDepth != "") {
        retString = (retString == "" ? "" : retString + ", ") + "PDepth: " + SpecificationPDepth
    }
    if (SpecificationTDiameter != "") {
        retString = (retString == "" ? "" : retString + ", ") + "TDiameter: " + SpecificationTDiameter
    }

    return spectiontxt + ", " + retString;
}
function DeleteImg(ID) {
    var res = confirm('Are you sure want to delete image ?');
    if (res == true) {
        var docId = $(ID).closest('tr').find('.clsdocid').text();

        var E = "{DocID: '" + docId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/DeleteImg',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (responce) {
                var xmlDoc = $.parseXML(responce.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");

                var Result = $.trim($(Table).find("Result").text());
                if (Result == 1) $(ID).closest('tr').remove();
            }
        });
    }
}
function ViewImg(ID) {
    var url = $(ID).closest('tr').find('.cslUrl').text(); //alert(url);
    $('#imgview').attr('src', url);
    $('#dialog').modal('show');
}

function ApplyOption(mode) {
    if (mode != null) {

        if (mode == 'R') {

            $(".mode_G").addClass('hide');
            $(".mode_E").css('display', '');

            //ButtonAction();
        }
        else if (mode == 'V') {
            $("#btnSave").addClass('hide');
            $("#btnAdd").addClass('hide'); $("#btnRefresh").addClass('hide');
            $(".imgDel").addClass('hide'); $(".imgEdit").addClass('hide'); $(".imgView").css('display', '');

            $(".mode_G").addClass('hide'); $(".mode_E").css('display', 'none');

            $("#btnVerify").addClass('hide');
            $("#btnReject").addClass('hide');

            $(".clsOption").addClass('hide');

        }
        else if (mode == 'G') {
            $(".mode_G").css('display', '');
            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');
            $(".mode_E").css('display', '');

            $("#btnReject").css('display', '');
            $(".chk").prop('disabled', true);
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").addClass('hide');
        }
        else if (mode == 'A') {
            $(".mode_G").css('display', 'none');
            $(".mode_E").css('display', 'none');

            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');

            $(".chk").addClass('hide');
            $(".imgDel").addClass('hide'); $(".imgEdit").addClass('hide'); $(".imgView").addClass('hide');
        }
        else if (mode == 'C') {
            $(".mode_G").css('display', 'none');

            //$(".mode_R").css('display', 'none');
            //$(".mode_E").css('display', 'none');
            $("#btnSave").css('display', '');
            $("#btnAdd").addClass('hide');

            // $(".chk").addClass('hide');
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").css('display', '');
            ButtonAction();
        }
        else if (mode == 'E') {
            $("#btnSave").css('display', '');
            $(".mode_E").css('display', '');

            $(".mode_G").css('display', 'none');

            $("#btnAdd").addClass('hide');
            //$(".chk").addClass('hide');
            $(".imgDel").addClass('hide'); //$(".imgEdit").addClass('hide');
            $(".clsEditOption").addClass('hide');
            $(".imgView").css('display', '');
        }
        else if (mode == 'I') {
            // $(".clsInspectordtl").css('display', ''); $(".clsOtherdtl").css('display', 'none');
        }
        else if (mode == 'T') {
            $(".mode_G").addClass('hide'); $(".mode_E").css('display', 'none');
            $("#btnSave").css('display', ''); $("#btnRefresh").css('display', '');
            $(".clsOption").addClass('hide');
        }
        else $(".cls").addClass('hide');
    }
    else {
        $(".cls").addClass('hide');
    }
}
function Estimation(ServiceCurrStatus) {
    if (ServiceCurrStatus == 1 || ServiceCurrStatus == 2) {
        $(".mode_E").css('display', 'none');
    }
}
function Calculate(ID) {
    //var length = $(ID).closest('tr').find('.clsEscavation').text();
    //var rate = $(ID).val() == "" ? 0 : $(ID).val();
    //var cal = parseFloat(rate) * parseFloat(length == "" ? 0 : length);

    //$(ID).closest('tr').find('.clsAmount').val(cal);

    var Total = 0;
    $(".clsAmount").each(function () {
        var amt = $(this).val() == "" ? 0 : $(this).val();
        Total = parseFloat(Total) + parseFloat(amt);
    });
    $('.clsTotal').text(Total);
}
function Bind_Designation() {
    if ($("#ddlAction").val() != "") {
        var E = "{ActionID: '" + $("#ddlAction").val() + "', ApplicationID: '" + ApplicationId + "'} ";

        $.ajax({
            type: "POST",
            url: '/OtherServices/ApplicationForm/Bind_Designation',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('#ddlDesignation').empty();
                $('#ddlDesignation').append('<option value="">Select</option');
                if (t != null) {
                    if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#ddlDesignation').append('<option value=' + item.DesignationID + '>' + item.DesignationName + '</option>');
                        });
                    }
                }
            }
        });
    }
    else {
        $('#ddlDesignation').empty();
        $('#ddlDesignation').append('<option value="">Select</option');
    }
}
function CalculateFinal() {
    var Total = 0;
    $('#tbl tr.myData').find('.clsAmount').each(function () {
        var amt = $(this).text() == "" ? 0 : $(this).text();
        Total = parseFloat(Total) + parseFloat(amt);
    });

    $('#tblOtherCharges').find('.clsOtherAmt').each(function () {
        var amt = $(this).text() == "" ? 0 : $(this).text();
        Total = parseFloat(Total) + parseFloat(amt);
    });

    var fhtml = "";
    fhtml = "<tr>" +
        "<td colspan='1' style='text-align:right; font-weight:bold;'>Gross Total (&#x20B9;)</td>" +
        "<td style='text-align:right; font-weight:bold;' class='clsTotal'>" + Total.toFixed(2); + "</td>" +
            "</tr > ";

    $('#tblOtherCharges tfoot').empty();
    $('#tblOtherCharges tfoot').append(fhtml);
}

function MakePayment()
{
    var MuncipalityID = $('.clsVariables').attr('municipalityid');
    var PayLoadPG = "{MunicipalityID:'" + MuncipalityID + "'}";
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/OtherServices/MakePayment/GetPGList",
        data: PayLoadPG,
        dataType: "json",
        success: function (data) {
            var ResponseData = data.Data;
            if (ResponseData != null) {
                $('.ddl-payment-gateway').empty();
                $('.ddl-payment-gateway').append('<option value="">--select--</option>');
                $.each(ResponseData, function (index, value) {
                    var label = value.PG_Name;
                    var id = value.PG_Code;
                    $('.ddl-payment-gateway').append("<option value='" + id + "'>" + label + "</option>");
                });
                modalControl.modal('show');
                $('.complete-payment').off('click', clickOnPayment);
                $('.complete-payment').on('click', clickOnPayment);
            } else {
                alert(data.Message);
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
}
function validate() {
    modalControl.find(".modal-footer .field-error").hide();//hide all error fields
    var selectedPGVal = modalControl.find('.ddl-payment-gateway').val();
    if (selectedPGVal == "" || selectedPGVal == null || selectedPGVal == undefined) {
        modalControl.find(".modal-footer .field-error").html('Please select payment gateway');
        modalControl.find(".modal-footer .field-error").show();
        return false;
    }
    return true;
};
function clickOnPayment() {
    if (validate() && confirm("Are you sure to confirm payment?")) {
        var ServiceID = $('.clsVariables').attr('serviceid');
        var MuncipalityID = $('.clsVariables').attr('municipalityid');
        if (ServiceID != '' && MuncipalityID != '') {
            MakeFinalPayment(ServiceID, MuncipalityID, $('.ddl-payment-gateway').val());
            //alert(ServiceID+' '+ MuncipalityID+'  ' + $('.ddl-payment-gateway').val());
        }
    }
    else {
        alert('ERROR on id mapping.');
    }
};
function MakeFinalPayment(ServiceID, MuncipalityID, PGCode) {
    var PayLoadMP = "{MunicipalityID:'" + MuncipalityID + "',ServiceID:'" + ServiceID + "',PGCode:'" + PGCode + "'}";
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/OtherServices/MakePayment/MakePaymentTemp",
        data: PayLoadMP,
        dataType: "json",
        success: function (data) {
            var ResponseData = data;
            if (ResponseData != null) {
                var rData = ResponseData.Data;
                if (rData.hasOwnProperty('RedirectTo')) {
                    location.href = rData.RedirectTo;
                }
            } else {
                alert('Error: ' + response.Message);
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function Print() {

        var res = confirm('Are You Sure Want to Print Report ??');
        if (res == true) {
            $("#btnBack").hide(); $("#btnSave").hide(); $("#btnRefresh").hide();
           
            var contents = $("#myDiv").html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
           
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write("<html><head><title></title>");
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="~/Contents/Css/Gridstyle.css" rel="stylesheet" type="text/css" />');
            
            var str = "<table border='0' width='100%'> <tr>";
            //str += "<td><img src='/Contents/image/biswabangla.png' style='width: 70px; height: 70px;' /></td>";
            str += "<td><h3 style='font-family:verdana; font-size:18px; text-align:center;'>Urban Development and Municipal Affairs Department, GoWB.</h3></td>";
            str += "</tr></table>";
            frameDoc.document.write(str);


            frameDoc.document.write(contents);

            frameDoc.document.write('</body></html>');
            frameDoc.document.close();

            $("#btnBack").show(); $("#btnSave").show(); $("#btnRefresh").show();

            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);

        }

}


