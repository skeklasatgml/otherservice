﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetMappedWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetPaymentDetails = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/Get_PropertyTaxPaymentDetails",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetCounter = function (callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/CurSession/GetCurrentPaymentCounter",
                data: null,
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetPaymentModes = function (callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/CurSession/GetPaymentModes",
                data: null,
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.MakePaymentOffLine = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/MakePayment",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetLast20Payments = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/GetLast20PaymentsSummary",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetAssesseeByID = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetAssesseeByID",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.SearchAssesseesByHolding = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetAssesseesByHoldings",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetAssesseeByHodling = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssesseesByHoldingSuggestion",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.GetManualAdvanceAdjAvailability = function (data, callBack) {
                $.ajax({
                    type: "POST",
                    url: "/Municipality/AdvanceAdjustment/GetManualAdvAdjAvailabilityInfo",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        callBack(response);
                    },
                    failure: function (response) {
                        alertify.error("http Response Error");
                    }
                });
            };
        this.GetLast20PaymentsByAssessee = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetLast20Payments",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
        };
        this.MakePaymentAdvance = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/PayAdvance",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
        };
        this.GetCollectorAdjustmentConfiguration = function () {
            var response = null;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/Collection/GetCollectorAdjustmentConfiguration",
                //data: JSON.stringify(data),
                dataType: "json",
                async:false,
                success: function (data) {
                    response= data;
                },
                error: function (result) {
                    alertify.error("Error");
                }
            });
            return response;
        };

    };
    var APP_OBJECT = function (serverObject) {
        var configs = new function () {
            this.CollectorAdjustmentConfig = function () {
                    var r = serverObject.GetCollectorAdjustmentConfiguration();
                    if (!r) {
                        throw "Collector adjustment configuration not found";
                    }
                    if (r.Status === 200) {
                        if (r.Data != null) {
                            return r.Data;
                        } else {
                            throw "Collector adjustment configuration not found"
                        }
                    } else {
                        throw r.Message + ". Collector adjustment configuration not found"
                    }
                
            };
        };
        var appObj = this;
        var getFinYearFromDate = function (dt) {
            var formatFinYear = function (startYear, endYear) {
                return startYear.toString() + "-" + endYear.toString();
            }
            var curMonth = (dt.getMonth() + 1);
            var curYear = dt.getFullYear();
            if (curMonth > 3 && curMonth <= 12) {
                return formatFinYear(curYear, (curYear + 1));
            }
            else {
                return formatFinYear((curYear - 1), curYear)
            }
        }
        var getFirstDateFromFinyear = function (finYear) {
            var dt = finYear.split('-')[0] + '-04-01';
            return dt;
        };
        var dataPopulationAlert = new function () {
            var container = $(".data-population-alert");
            var writableObject = container.find(".alert");
            writableObject.WriteText = function (title, body) {
                if (title) {
                    $(this).find("strong").html(title);
                }
                if (body) {
                    $(this).find(".msg-body").html(body);
                }
            };
            writableObject.reset = function () {
                $(this).find("strong").html("");
                $(this).find(".msg-body").html("");
            };
            this.reset = function () {
                writableObject.reset();
            };
            var show = function () {
                container.show();
            };
            this.hide = function () {
                container.hide();
            };

            this.show_PaymentInfoFound = function () {

            };
            this.show_ProcessingPaymentInfo = function () {
                show();
                writableObject.removeClass("alert-warning");
                writableObject.addClass("alert-info");
                writableObject.WriteText("Processing!","Please wait a while....")
            };
            this.show_PaymentInfoNotFound = function () {
                show();
                writableObject.addClass("alert-warning");
                writableObject.removeClass("alert-info"); 
                writableObject.WriteText("No Pendings Found!"," For advance payment, <a href='javascript:void(0);' class='alert-link proceed-payment' data-payment-action='A'>Click here</a>")
            };
            this.show_AdvancePaymentLink = function () {
                show();
                writableObject.addClass("alert-warning");
                writableObject.removeClass("alert-info");
                writableObject.WriteText("Alternative Option!", " For advance payment, <a href='javascript:void(0);' class='alert-link proceed-payment' data-payment-action='A'>Click here</a>")
            };
        };
        var GridLast20PaymentForAssessee = function () {
            var table = $("<table class='last-20-payment-by-assessee table table-condensed table-bordered table-responsive'><thead></thead><tbody></tbody></table>")
            this.Loadtable = function (assesseeID, callback) {
                var loadHeader = function () {
                    table.find('thead').append(("<tr><th> Sl</th><th>Assessee Name</th><th>Holding No</th><th>Receive Date<br>[dd/MM/yyyy]</th><th>NetAmount</th><th>Action</th></tr>"));
                };
                var emptyHeader = function () {
                    table.find('thead').empty();
                };

                var getRow = function (sl, rowObject) {
                    var row = ("<tr>");
                    row += ("<td>" + sl + "</td>");
                    row += ("<td>" + rowObject.AssesseeName + "</td>");
                    row += ("<td>" + rowObject.HoldingNo + "</td>");
                    row += ("<td>" + CONVERTER.Date.convertCsToJsDate(rowObject.PaymentReceiveDate).format('dd/mm/yyyy') + "</td>");
                    row += ("<td>" + rowObject.NetAmount + "</td>");
                    row += ("<td>" + "<a target='_blank' href='/Municipality/MnReports/PaymentReciept?PaymentId=" + rowObject.PaymentID + "'>Print Reciept</a>" + "</td>");
                    row += "</tr>";
                    return row;
                };
                var emptyBody = function () {
                    table.find("tbody").empty();
                };
                var loadBody = function (data) {
                    $.each(data, function (index, value) {
                        var rowStr = getRow(++index, value);
                        table.find('tbody').append(rowStr);
                    });
                };

                var appendEmptyMsg = function (colSpanCount, msg) {
                    table.find('tbody').append("<tr><td colspan='" + colSpanCount + "' style='text-align:center'>" + msg + "</td></tr>");
                }
                var load = function () {
                    serverObject.GetLast20PaymentsByAssessee({ AssesseeID: assesseeID }, function (response) {
                        if (response.Status === 200) {
                            var data = response.Data;
                            if (data.length == 0) {
                                emptyBody();
                                emptyHeader();
                                loadHeader();
                                appendEmptyMsg(6, "No Records Found");
                            } else {
                                emptyHeader();
                                emptyBody();
                                loadHeader();
                                loadBody(data);
                            }

                        } else {
                            alert(response.Message);
                        }
                        callback(table)
                    });
                };
                load();
            };

        };
        var GlobalVariables = {
            IsSearchEnabled: true,
            PaymentObject: {},
            PaymentModes: [],
            PaymentCounter: {},
            NetAmount: 0,
            gridLast20PaymentsByAssessee: new GridLast20PaymentForAssessee(),
            Assessee: null
        };
        var Rest_GlobalVariable = function () {
            GlobalVariables.PaymentObject = {};
            GlobalVariables.NetAmount = 0;
            GlobalVariables.Assessee = null;
            dataPopulationAlert.hide();
            GlobalVariables.IsSearchEnabled = true;
            GlobalVariables.SelectedGridRows = [];
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnWard.val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        Resetors.ResetAssesse();
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnAssessee.val('');
                        //reset area
                        var locationID = DomControls.ctrl_hdnLocation.val();
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName + '<' + item.HoldingNo + '>',
                                            AssesseeID: item.AssesseeID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Holdings: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnAssessee.val('');
                        //reset area
                        var locationID = DomControls.ctrl_hdnLocation.val();
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, LocationID: locationID, HoldingNo: request.term, TakeRowCount: 15 };
                        serverObject.GetAssesseeByHodling(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: '<' + item.HoldingNo.trim() + '>' + item.AssesseeName,
                                            AssesseeID: item.AssesseeID,
                                            HoldingNo: item.HoldingNo.trim(),
                                            Obj: item
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                        OnSuccessfullSeachAssesse(i.item.Obj);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            }
        };
        var DomControls = {
            ctrl_btnPay: $("#btnPay"),
            ctrl_btnSearch: $("#btnSearch"),

            ctrl_hdnMunicipality: $("#hdnMunicipality"),
            ctrl_txtMunicipality: $("#txtMunicipality"),

            ctrl_hdnWard: $("#hdnWard"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#hdnLocation"),
            ctrl_txtLocation: $("#txtLocation"),


            ctrl_txtAssessee: $("#txtAssessee"),
            ctrl_hdnAssessee: $("#hdnAssessee"),
            ctrl_holdingType:$(".holding-type"),
            ctnr_gridContainer: $("#gridContainer"),

            ctrl_CollectionDate: $("#txtCollectionDate"),
            ctrl_NoticeServedDate: $("#txtNoticeServedDate"),
            ctrl_ddlEffectiveQtrFrom: $("#ddlEffectiveQtrFrom"),
            ctrl_PaidAmnt: $("#txtPaidAmnt"),
            ctrl_txtCollectionDate: $("#txtCollectionDate"),
            ctrl_btnSearchByAssesseeID: $(".search-by-assesseeID"),
            ctrl_txtSearchByAssesseeID: $(".txtSearchByAssesseeID"),
            ctrl_btnReSearchByAssesseeID: $(".re-search-by-assesseeID")
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            },
            ResetAssesse: function () {
                DomControls.ctrl_hdnAssessee.val('');
                DomControls.ctrl_txtAssessee.val('');
            },
            ResetCollectionDt: function () {
                var dt = new Date();
                DomControls.ctrl_txtCollectionDate.val($(".current-date").val());
                
            },
            ResetNoticeServedDate:function () {
                DomControls.ctrl_NoticeServedDate.val('');
            },
            ResetEffectiveQtrFrom: function () {
                DomControls.ctrl_ddlEffectiveQtrFrom.find("option:eq(0)").attr('selected', true);
                DomControls.ctrl_ddlEffectiveQtrFrom.removeAttr('disabled');
            },
            ReseteWholeForm: function () {
                DomControls.ctrl_txtAssessee.removeAttr('disabled');
                DomControls.ctrl_txtLocation.removeAttr('disabled');
                DomControls.ctrl_txtWard.removeAttr('disabled');
                DomControls.ctrl_txtCollectionDate.removeAttr('disabled');
                DomControls.ctrl_NoticeServedDate.removeAttr('disabled');
                //DomControls.ctrl_ddlEffectiveQtrFrom.removeAttr('disabled');
                DomControls.ctrl_holdingType.text('');
                Resetors.ResetEffectiveQtrFrom();
                DomControls.ctrl_btnSearch.prop('value', 'Search');
                DomControls.ctrl_btnSearch.removeAttr('disabled')

                Resetors.ResetWard();
                Resetors.ResetLocation();
                Resetors.ResetAssesse();
                Resetors.ResetCollectionDt();
                Resetors.ResetNoticeServedDate();
                Rest_GlobalVariable();
                Grid.Reset();
                $(".last-20-payment-by-assessee-panel .panel-body").empty();
            }
        };
        var LockSearchField = function () {
            DomControls.ctrl_txtAssessee.attr('disabled', 'disabled');
            DomControls.ctrl_txtLocation.attr('disabled', 'disabled');
            DomControls.ctrl_txtWard.attr('disabled', 'disabled');
            //DomControls.ctrl_btnSearch.prop('value', 'Re-Search');
            DomControls.ctrl_btnSearch.attr('disabled', 'disabled');
            DomControls.ctrl_txtCollectionDate.attr('disabled', 'disabled');
            DomControls.ctrl_NoticeServedDate.attr('disabled', 'disabled');
            DomControls.ctrl_ddlEffectiveQtrFrom.attr('disabled', 'disabled');

            GlobalVariables.IsSearchEnabled = false;

            //$('.search-by-assesseeID').attr('value', 'Reset')
            //$('.txtSearchByAssesseeID ').prop('disabled', true);
        };
       
        var searchableObject = function (checkParamRequired) {
            if (checkParamRequired !== undefined)
            {
                return { WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val(), AssesseeID: DomControls.ctrl_hdnAssessee.val(), PaymentDate: DomControls.ctrl_txtCollectionDate.val(), TaxPaymentCheckParameters: [], NoticeServedOn: DomControls.ctrl_NoticeServedDate.val(), EffectQtrFrom: DomControls.ctrl_ddlEffectiveQtrFrom.val() };
            } else {
                return { WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val(), AssesseeID: DomControls.ctrl_hdnAssessee.val(), PaymentDate: DomControls.ctrl_txtCollectionDate.val(), TaxPaymentCheckParameters: Grid.GetSelectedRowObjs(), NoticeServedOn: DomControls.ctrl_NoticeServedDate.val(), EffectQtrFrom: DomControls.ctrl_ddlEffectiveQtrFrom.val() };
            }
            
        };
        var registerValidator = new function () {
            this.resisterSearchForm = function () {
                $("#frmPropertyTaxPaymentSearch").validate({
                    ignore: [],
                    rules: {
                        hdnWard: { required: true },
                        hdnLocation: { required: true },
                        hdnAssessee: { required: true },
                        txtCollectionDate: { required: true },
                        ddlEffectiveQtrFrom: {
                            required: function () {
                                if (DomControls.ctrl_NoticeServedDate.val().trim().length > 0)
                                {
                                    return true;
                                }
                                return false;
                            }
                        }
                    },
                    messages: {
                        hdnWard: {
                            required: "Please select valid ward."
                        },
                        hdnLocation: {
                            required: "Please select valid location."
                        },
                        hdnAssessee: {
                            required: "Please select valid assessee."
                        },
                        txtCollectionDate: {
                            required: "Please select collection."
                        },
                        ddlEffectiveQtrFrom: {
                            required: "Please select valid Quarter "
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }
                });
            }
        };
       
        var PaymentCompletionModal = new function () {

            var paymentAction = null;
            var modalParent = this;
            var modalControl = $('#modal_MakePaymentComplete');
            this.GetPaymentInstrumentInfo = function () {
                var paymentInfo = [];
                GlobalVariables.PaymentModes.forEach(function (value, index) {
                    var paymntTemplate = $("#modal_MakePaymentComplete .modal-body .tabContainer .tab-content").find('#' + value.PaymentModeCode);
                    var modeType = Number(paymntTemplate.attr('data-mode-type'));
                    if (value.HasInstrument == true) {
                        
                        var GetInstruments = function () {
                            var rows = paymntTemplate.find('table').find('tbody').find('tr');
                            
                            var values = [];
                            rows.each(function (indexRow, valueRow) {
                                if (modeType === 2) {
                                    values.push({
                                        ModeType: modeType,
                                        InstrumentNo: $(valueRow).find('.instrument-no').val(),
                                        InstrumentDate: $(valueRow).find('.instrument-date').val(),
                                        InstrumentAmount: Number(isNaN($(valueRow).find('.diposit-amount').val()) ? 0 : $(valueRow).find('.diposit-amount').val()),
                                        BankName: $(valueRow).find('.instrument-BankName').val()
                                    });
                                } else if (modeType===4) {
                                    values.push({
                                        ModeType: modeType,
                                        InstrumentNo: $(valueRow).find('.instrument-no').val(),
                                        InstrumentDate: $(valueRow).find('.instrument-date').val(),
                                        InstrumentAmount: Number(isNaN($(valueRow).find('.diposit-amount').val()) ? 0 : $(valueRow).find('.diposit-amount').val()),
                                        BankName: $(valueRow).find('.instrument-BankName').val(),
                                        FromAccountNo: $(valueRow).find('.instrument-account-no').val()
                                    });
                                } else if (modeType === 5) {
                                    values.push({
                                        ModeType: modeType,
                                        InstrumentNo: $(valueRow).find('.instrument-no').val(),
                                        InstrumentDate: $(valueRow).find('.instrument-date').val(),
                                        InstrumentAmount: Number(isNaN($(valueRow).find('.diposit-amount').val()) ? 0 : $(valueRow).find('.diposit-amount').val()),
                                        BankName: $(valueRow).find('.instrument-BankName').val(),
                                        CardNo: $(valueRow).find('.instrument-card-no').val()
                                    });
                                }
                            });
                            return values;
                        };
                        var instruments = GetInstruments();
                        var totalAmount = 0;
                        $.each(instruments, function (index, value) {
                            totalAmount += value.InstrumentAmount;
                        });
                        totalAmount = (typeof (totalAmount) === 'undefined' ? 0 : totalAmount);
                        paymentInfo.push({
                            ModeType: modeType,
                            PaymentModeCode: value.PaymentModeCode,
                            PaymentModeDescription: value.PaymentModeDescription,
                            TotalAmount: totalAmount,
                            HasInstrument: value.HasInstrument,
                            PaymentInstruments: instruments,
                            
                        });
                    } else {
                        var totAmount = Number(isNaN($(paymntTemplate).find('.diposit-amount').val()) ? 0 : $(paymntTemplate).find('.diposit-amount').val());
                        paymentInfo.push({
                            ModeType: modeType,
                            PaymentModeCode: value.PaymentModeCode,
                            PaymentModeDescription: value.PaymentModeDescription,
                            TotalAmount: totAmount,
                            HasInstrument: value.HasInstrument,
                            PaymentInstruments: []
                        });
                    }
                });
                return paymentInfo;
            };
            this.ResetModal = function () {
                $("#modal_MakePaymentComplete .modal-footer .total-diposited-amount").html("0.00");
                $("#modal_MakePaymentComplete .total-Payable-amount").html("0.00");
                paymentAction = null;
            };
            this.ShowModal = function (obj) {
                paymentAction = obj;
                window.paymentAction = paymentAction;
                var netAmount = 0;
                var isAllconfigurationReady = false;
                if (obj.hasOwnProperty("paymentAction")) {
                    if (obj.paymentAction == "A") {//Only Advance Payment
                        isAllconfigurationReady = true;
                        netAmount = 0;
                    }
                    else if (obj.paymentAction == "N") {//Normar Porperty Tax Paymebnt
                        isAllconfigurationReady = true;
                        netAmount = GlobalVariables.NetAmount;
                    } else {
                        alertify.error("Invalid Payment Action");
                    }
                } else {
                    alertify.error("Invalid Payment Action");
                }
                Arrangetabs();
                $("#modal_MakePaymentComplete .modal-header .total-Payable-amount").html(netAmount);
                $("#modal_MakePaymentComplete .modal-header .hdn-total-Payable-amount").val(netAmount);

                $("#modal_MakePaymentComplete  .modal-body  .tabContainer .tab-pane#C .diposit-amount").val(netAmount)
                var cashModeTab = $("#modal_MakePaymentComplete  .modal-body  .tabContainer .tab-content>div#C");
                if (cashModeTab.length > 0)
                {
                    $("#modal_MakePaymentComplete  .modal-body  .tabContainer .nav,nav-tabs").find('li a[href="#C"]').tab("show")
                    var ctr_txtCash = cashModeTab.find(".diposit-amount");
                    ctr_txtCash.val(netAmount);
                    $('#modal_MakePaymentComplete .modal-body .diposit-amount').trigger('keyup');
                }
                modalControl.find(".email-phone").find("#txtEmail").val('');
                modalControl.find(".email-phone").find("#txtPhone").val('');
                if ( GlobalVariables.PaymentObject.hasOwnProperty("AdditionalData") && GlobalVariables.PaymentObject.AdditionalData && GlobalVariables.PaymentObject.AdditionalData.hasOwnProperty("Assessee") && GlobalVariables.PaymentObject.AdditionalData.Assessee) {
                    var asseessee = GlobalVariables.PaymentObject.AdditionalData.Assessee;
                    modalControl.find(".email-phone").find("#txtEmail").val(asseessee.PrimaryEmail);
                    modalControl.find(".email-phone").find("#txtPhone").val(asseessee.PrimaryPhNo);
                }
                if (obj.paymentAction == "A") {
                    modalControl.find(".email-phone").find("#txtEmail").val(GlobalVariables.Assessee.PrimaryEmail);
                    modalControl.find(".email-phone").find("#txtPhone").val(GlobalVariables.Assessee.PrimaryPhNo);
                }
                modalControl.modal('show');
            };
            var getPayableInterestAmount = function () {
                var onlyInterestAmount = 0;
                $("#gridContainer table tbody .data-row").has(".chkPay:checked").find(".PaidCalculatedAmt").map(function (index, value) {
                    var val = Number($(value).text());
                    if (val > 0) {
                        onlyInterestAmount += val;
                    }
                });
                return onlyInterestAmount;
            };
            var validate = function () {
                //field validator
                var _paymentAction = paymentAction;
                console.log(_paymentAction);
                modalControl.find(".modal-body .tabContainer .tab-content > div .field-error").hide();//hide all error fields
                modalControl.find(".modal-body .tabContainer .nav-tabs >li a ").removeClass('error-block');//reset tab head colors
                var instrumentalErrorCount = 0;
                var nonInstrumentalErrorCount = 0;
                var instrumentInfo = modalParent.GetPaymentInstrumentInfo();
                var addErrorField = function (rootElement, msg) {
                    var hasSibling = function (rootElement, siblingClass) {
                        if (rootElement.siblings('.' + siblingClass).length > 0) {
                            return true;
                        }
                        return false;
                    };
                    if (hasSibling(rootElement, 'field-error') == false) {
                        $("<span class='field-error' style='color:red'>" + msg + "</span>").insertAfter(rootElement);
                    } else {
                        rootElement.siblings('.field-error').show().html(msg);
                    }
                };
                var ValidatePayableAmount = function () {
                    var totalAmount = 0;
                    var vals = instrumentInfo;
                    vals.forEach(function (value, index) {
                        totalAmount += value.TotalAmount;
                    });
                    if (paymentAction.paymentAction == "N") {
                        if (totalAmount >= GlobalVariables.NetAmount) {
                            //skip to normal or advance payment route
                            return true;
                        }
                        var municipalityConfig = configs.CollectorAdjustmentConfig();
                        //check if collector adjustment :true then 
                        if (municipalityConfig.IsActiveForMunicipality == true && municipalityConfig.IsEnabledCollectorAdjustmentOnUser == true) {
                            var collectorAdjustedAmount = (GlobalVariables.NetAmount - totalAmount);
                            var maxAdjustableAmount = 0;

                            //if adjustment type field value(FV), (Max adjustable amount specified in configuration field, named=>MaxAdjustableAmount)
                            if (municipalityConfig.CollctrAdjustmentType === "FV") {
                                maxAdjustableAmount = municipalityConfig.MaxAdjustableAmount;
                            }
                                //if adjustment type Interest(IN), (Max adjustable amount will be payable interest amount)
                            else if (municipalityConfig.CollctrAdjustmentType === "IN") {
                                var totalInterestAmount = getPayableInterestAmount();
                                maxAdjustableAmount = Math.floor(totalInterestAmount);
                            }
                            else {
                                throw ("Bad Collector Adjustment configuration, No adjustment configuration found");
                            }

                            //adjustment 
                            if (totalAmount < GlobalVariables.NetAmount) {//adjustable route 
                                if (collectorAdjustedAmount <= maxAdjustableAmount) {
                                    return true;
                                } else {
                                    alertify.alert("<span style='color:red'>Error: Unable to Perform Collector Adjustment</span>", "Adjustable Amount Exceeded Range. Max Adjustable Amount <b>Rs." + maxAdjustableAmount + "</b>.");
                                    return false;
                                }
                            } else {//normal route
                                return true;
                            }
                        } else {//check if collector adjustment :false then check if greater  
                            if (totalAmount < GlobalVariables.NetAmount) {
                                return false;
                            }
                        }
                    } else if (paymentAction.paymentAction == "A") {
                        if (totalAmount <= 0) {
                            return false;
                        }
                    }
                    return true;
                };
                var InstrumentedAmount = 0;
                instrumentInfo.forEach(function (value, index) {
                    var curTab = modalControl.find(".modal-body .tabContainer .tab-content > div#" + value.PaymentModeCode);
                    if (value.HasInstrument == true) {


                        var tableBodyRows = curTab.find('table > tbody > tr');
                        $.each(tableBodyRows, function (index1, value1) {
                            var instrumentNoCtrl = $(value1).find(".instrument-no");//instrument-date
                            var instrumentDateCtrl = $(value1).find(".instrument-date");
                            var instrumentAmountCtrl = $(value1).find(".diposit-amount");
                            var instrumentBankNameCtrl = $(value1).find(".instrument-BankName");
                            InstrumentedAmount += Number(instrumentAmountCtrl.val());

                            //required field validate
                            if (instrumentNoCtrl.val().trim() == '') {
                                instrumentalErrorCount += 1;
                                addErrorField(instrumentNoCtrl, "Required");
                            }
                            if (instrumentDateCtrl.val().trim() == '') {
                                instrumentalErrorCount += 1;
                                addErrorField(instrumentDateCtrl, "Required");
                            }
                            if (instrumentAmountCtrl.val().trim() == '') {
                                instrumentalErrorCount += 1;
                                addErrorField(instrumentAmountCtrl, "Required");
                            }
                            if (instrumentBankNameCtrl.val().trim() == '') {
                                instrumentalErrorCount += 1;
                                addErrorField(instrumentBankNameCtrl, "Required");
                            }
                            //end required field
                            if (isNaN(instrumentAmountCtrl.val())) {
                                instrumentalErrorCount += 1;
                                addErrorField(instrumentAmountCtrl, "Invalid amount");
                            }
                            if (instrumentalErrorCount > 0) {
                                modalControl.find(".modal-body .tabContainer .nav-tabs >li:has(a[href='#" + value.PaymentModeCode + "']) a").addClass('error-block');
                            }
                        });

                    } else {
                        if (instrumentalErrorCount === 0) {
                            if (ValidatePayableAmount() == false) {
                                var amountDipositCtrl = curTab.find(".form-group .diposit-amount");
                                addErrorField(amountDipositCtrl, 'Enter proper amount');
                                modalControl.find(".modal-body .tabContainer .nav-tabs >li:has(a[href='#" + value.PaymentModeCode + "']) a").addClass('error-block').trigger('click');//add class and redirect to current tab
                                nonInstrumentalErrorCount += 1;
                            }
                        }

                    }
                });
                if (instrumentalErrorCount > 0 || nonInstrumentalErrorCount > 0) {
                    return false;
                } else {
                    return true;
                }

            };
            var emailPhoneValidator = function () {
                var ResisterValidatorMethods = function () {
                    $.validator.addMethod(
                        "ValidEmail",
                        function (value, element, params) {
                            if (this.optional(element)) {  // "required" not in force and field is empty
                                return true;
                            } else {  // otherwise, use your rule
                                return window.RegxValidators.emailValidate(value);
                            }
                        },
                        "Enter Valid email id"
                    );
                    $.validator.addMethod(
                        "ValidPhone",
                        function (value, element, params) {
                            if (this.optional(element)) {  // "required" not in force and field is empty
                                return true;
                            } else {  // otherwise, use your rule
                                return window.RegxValidators.MobileNo10CharValidate(value);
                            }
                        },
                        "Enter Valid 10 digit mobile no"
                    );
                }
                ResisterValidatorMethods();

                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                //vldObj.ignore = [];

                //set rules
                vldObj.rules[modalControl.find("#txtEmail").prop('name')] = { ValidEmail: true };
                vldObj.rules[modalControl.find("#txtPhone").prop('name')] = { ValidPhone: true };
               
                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                modalControl.find("#form-email-phone").validate(vldObj);
                //validate form and return true and false
                return modalControl.find('#form-email-phone').valid();

            };
            var getTotalPayingAmount = function (paymentInstrumentsIfo) {
                var totalAmounts = alasql("select sum(TotalAmount) as TotalAmount from ? where TotalAmount>0", [paymentInstrumentsIfo]);
                return totalAmounts[0].TotalAmount;
            };
            var getNetamount = function () {
                var netAmount = Number( modalControl.find(".hdn-total-Payable-amount").val());
                return netAmount;
            };
            var submitPayment = function (payLoad)
            {
                serverObject.MakePaymentOffLine(payLoad, function (response) {
                    if (response.Status === 200) {
                        alertify.alert("Successs!", response.Message, function () {
                            var data = response.Data;
                            var win = window.open(data.RedirectTo, '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                            } else {
                                //Browser has blocked it
                                alertify.error('We are unable to open <b>Reciept window</b>! Please allow popups for this website! ');
                            }
                            location.reload(false);
                        });
                    } else {
                        alertify.error('Error: ' + response.Message);
                    }
                });
            }
            var submitAdvanePayment = function (payLoad) {
                serverObject.MakePaymentAdvance(payLoad, function (r) {
                    if (r.Status === 200) {
                        alertify.alert("Successs!", r.Message, function () {
                            var data = r.Data;
                            var win = window.open(data.RedirectTo, '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                            } else {
                                //Browser has blocked it
                                alertify.error('We are unable to open <b>Reciept window</b>! Please allow popups for this website! ');
                            }
                            location.reload(false);
                        });

                    } else {
                        alertify.error(r.Message);
                    }
                });
            };
            var OnClickCompeletePayment = function () {
                try {
                    if (emailPhoneValidator() === true && validate()) {
                        var payLoad = searchableObject();
                        if (typeof (payLoad) != 'undefined') {
                            var payActionConfig = paymentAction;
                            var paymentInfo = modalParent.GetPaymentInstrumentInfo();
                            payLoad.Email = modalControl.find('.email-phone  #txtEmail').val();
                            payLoad.Mobile = modalControl.find('.email-phone  #txtPhone').val();
                            payLoad.BillReciptNo = modalControl.find('.email-phone  #txt-bill-reciept-no').val().trim();
                            payLoad.PaymentModes = paymentInfo;
                            var totalPayingAmount = getTotalPayingAmount(paymentInfo);
                            var totalNetAmount = getNetamount(paymentInfo);
                            if (totalPayingAmount <= 0) {
                                alertify.error("Invalid amount paying Rs. " + totalPayingAmount);
                                return;
                            }

                            //advance payment when excess amount paid at payment time.(normal payment)
                            if (payActionConfig.paymentAction == "N") {
                                if (totalPayingAmount > totalNetAmount) {
                                    //show excess advance confirmation
                                    alertify.confirm("Confirm Advance Payment!", "Paid amount is greater than payable amount. <b>Excess amount of Rupees " + (totalPayingAmount - getNetamount()) + "</b> will be adjusted as <b>Advance</b>.<br/><b>Do you want to continue payment?</b>",
                                        function () {
                                            submitPayment(payLoad);
                                        }, function () { });
                                }
                                else if (totalNetAmount > totalPayingAmount) {
                                    var msg = "<span >You have entered a value that is less than the selected payable outstanding of <b style='color:#a94442'>₹ " + totalPayingAmount+ "</b>. The diffrence of <b style='color:#a94442'>₹ " + (totalNetAmount - totalPayingAmount)+"</b> will be treated as a <b style='color:#a94442'>remission</b> provided by collector.<br/><br/> Click <b>Ok</b> to continue.</span>";
                                    alertify.confirm("Warning!", msg,
                                        function () {
                                            submitPayment(payLoad);
                                        }, function () { });
                                }
                                else {
                                    alertify.confirm("Confirm Payment!", "Do you want to continue payment?</b>",
                                        function () {
                                            submitPayment(payLoad);
                                        }, function () { });
                                }
                            }
                            //pay only advance. when outstanding not available.(only advance payment)
                            else if (payActionConfig.paymentAction == "A") {
                                alertify.confirm("Confirm Advance Payment!", "Are you sure on paying advance <strong>Rs. " + totalPayingAmount + "</strong>?", function () {
                                    submitAdvanePayment(payLoad);
                                }, function () { });
                            }
                        } else {
                            alertify.error('Invalid Payload');
                        }
                    }
                } catch (e) {
                    alertify.error(e);
                }
            };
            var Arrangetabs = function () {
                var tabHeader = function () {
                    var stringHeader = "<ul class='nav nav-tabs'>";
                    //if cash available 
                    $.each(GlobalVariables.PaymentModes, function (index, value) {
                        stringHeader += '<li class="' + (index === 0 ? ' active' : '') + '"><a data-toggle="tab" href="#' + value.PaymentModeCode + '">' + value.PaymentModeDescription + '</a></li>'
                    });
                    stringHeader += "</ul >";
                    return stringHeader;
                };
                var tabBody = function () {
                    var stringBody = "<div class='tab-content'>";
                    $.each(GlobalVariables.PaymentModes, function (index, value) {
                        stringBody += " <div  data-mode-type='" + value.ModeType +"' id='" + value.PaymentModeCode + "' class='tab-pane fade in " + (index === 0 ? ' active' : '') + "'>";
                        stringBody += " <h4>" + value.PaymentModeDescription + "</h4>";
                        if (value.HasInstrument == false)//specially for cash mode
                        {
                            stringBody += "<div class='form-group'>";
                            stringBody += "<label > Enter Amount:</label >";
                            stringBody += "<input type='text' placeholder='Amount (0.00)' class='form-control diposit-amount'>";
                            stringBody += "</div>";
                        } else {//other modes those have instruments
                            if (value.ModeType === 2) {//(DD,N,Q,T)
                                stringBody += "<table width='100%' class='table-hover table-bordered table-responsive table-condensed'><thead><tr><th>Instrument No</th><th>Instrument Date</th><th>Instrument Amount</th><th>Bank Name</th><th> <a href='#' class='action-row-add' title='Add Row' data-mode-type='" + value.ModeType +"'> <span class='glyphicon glyphicon-plus'></span></a></th></tr></thead>";
                                stringBody += "<tbody></tbody></table>";
                            } else if (value.ModeType === 4){//Fund transfer
                                stringBody += "<table width='100%' class='table-hover table-bordered table-responsive table-condensed'><thead><tr><th>Transaction No</th><th>Transaction Date</th><th>Amount</th><th>Bank Name</th><th>Account no</th><th> <a href='#' class='action-row-add' title='Add Row' data-mode-type='" + value.ModeType +"'> <span class='glyphicon glyphicon-plus'></span></a></th></tr></thead>";
                                stringBody += "<tbody></tbody></table>";
                            } else if (value.ModeType === 5)//credit and debit card
                            {
                                stringBody += "<table width='100%' class='table-hover table-bordered table-responsive table-condensed'><thead><tr><th>Transaction No</th><th>Transaction Date</th><th>Amount</th><th>Bank Name</th><th>Card no</th><th> <a href='#' class='action-row-add' title='Add Row' data-mode-type='" + value.ModeType+"'> <span class='glyphicon glyphicon-plus'></span></a></th></tr></thead>";
                                stringBody += "<tbody></tbody></table>";
                            }
                        }
                        stringBody += "</div>";
                    });
                    stringBody += "</div >";
                    return stringBody;
                };
                //validate modal amounts


                var BindEvents = function () {
                    //.complete - payment
                    $(document).off('click', "#modal_MakePaymentComplete .modal-footer .complete-payment");
                    $(document).on('click', "#modal_MakePaymentComplete .modal-footer .complete-payment", OnClickCompeletePayment);

                    $(document).off('click', "#modal_MakePaymentComplete .modal-body .action-row-add");
                    $(document).on('click', "#modal_MakePaymentComplete .modal-body .action-row-add", function () {
                        var modeType = Number($(this).attr('data-mode-type'));
                        var rowString = "";

                        if (modeType === 2) {//(DD,N,Q,T)
                            rowString = "<tr><td><input type='text' placeholder='instrument-no' class='form-control instrument-no'></td>";
                            rowString += "<td><input type='text' placeholder='instrument-date' class='form-control instrument-date datepick' readonly></td>";
                            rowString += "<td><input type='text' placeholder='Amount (0.00)' class='form-control diposit-amount'></td>";
                            rowString += "<td><input type='text' placeholder='Bank Name' class='form-control instrument-BankName'></td>";
                            rowString += "<td><a href='#' class='action-row-remove' title='remove Row'> <span class='glyphicon glyphicon-minus'></span></a></td>";
                            rowString += "</tr>";
                        } else if (modeType=== 4) {//Fund transfer
                            rowString = "<tr><td><input type='text' placeholder='Transaction-no' class='form-control instrument-no'></td>";
                            rowString += "<td><input type='text' placeholder='Transaction-date' class='form-control instrument-date datepick' readonly></td>";
                            rowString += "<td><input type='text' placeholder='Amount (0.00)' class='form-control diposit-amount'></td>";
                            rowString += "<td><input type='text' placeholder='Bank Name' class='form-control instrument-BankName'></td>";
                            rowString += "<td><input type='text' placeholder='Account no' class='form-control instrument-account-no'></td>";
                            rowString += "<td><a href='#' class='action-row-remove' title='remove Row'> <span class='glyphicon glyphicon-minus'></span></a></td>";
                            rowString += "</tr>";
                        } else if (modeType === 5)//credit and debit card
                        {
                            rowString = "<tr><td><input type='text' placeholder='Transaction-no' class='form-control instrument-no'></td>";
                            rowString += "<td><input type='text' placeholder='Transaction-date' class='form-control instrument-date datepick' readonly></td>";
                            rowString += "<td><input type='text' placeholder='Amount (0.00)' class='form-control diposit-amount'></td>";
                            rowString += "<td><input type='text' placeholder='Bank Name' class='form-control instrument-BankName'></td>";
                            rowString += "<td><input type='text' placeholder='Card no' class='form-control instrument-card-no'></td>";
                            rowString += "<td><a href='#' class='action-row-remove' title='remove Row'> <span class='glyphicon glyphicon-minus'></span></a></td>";
                            rowString += "</tr>";
                        }
                        
                        $(this).closest('table').find('tbody').append(rowString);
                    });
                    $(document).off('click', "#modal_MakePaymentComplete .modal-body .action-row-remove");
                    $(document).on('click', "#modal_MakePaymentComplete .modal-body .action-row-remove", function () {
                        $(this).closest('tr').remove();
                    });

                    $(document).off('keyup', '#modal_MakePaymentComplete .modal-body .diposit-amount');
                    $(document).on('keyup', '#modal_MakePaymentComplete .modal-body .diposit-amount', function () {
                        var totalAmount = 0;
                        $('#modal_MakePaymentComplete .modal-body .diposit-amount').each(function (index, value) {
                            totalAmount += (isNaN($(value).val()) ? 0 : Number($(value).val()));
                        });
                        $("#modal_MakePaymentComplete .modal-footer .total-diposited-amount").html(totalAmount.toFixed(2));

                    });
                    $("body").off('focus', '.datepick');
                    $("body").on('focus', '.datepick', function () {
                        $(this).datepicker({
                            dateFormat: 'yy-mm-dd',
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            closeText: 'X',
                            showAnim: 'drop',
                            changeYear: true,
                            changeMonth: true,
                            duration: 'medium'
                        });
                    });

                };
                (function () {
                    BindEvents();
                    var tabString = tabHeader() + tabBody();
                    var tabContainer = modalControl.find('.modal-body').find('.tabContainer');
                    tabContainer.empty();
                    tabContainer.append(tabString);
                }());
            };
        };
        var Last20PaymentControl = function () {
            var cntr_table = $(".table-last20payments")
            this.Load = function () {
                serverObject.GetLast20Payments(null, function (response) {
                    if (response.Status === 200) {
                        loadTableBody(response.Data);
                    } else {
                        alert("Error: " + response.Message);
                    }
                });
            };
            var emptyBody = function () {
                cntr_table.find('tbody').empty();
            };
            var loadTableBody = function (Datasource) {
                emptyBody();
                $.each(Datasource, function (index, value) {
                    cntr_table.find('tbody').append(getRowstring(value, ++index));;
                });
            };
            var getRowstring = function (rowObject, rowNumber) {
                var tr = "<tr>";
                tr += "<td>" + rowNumber + "</td>";
                tr += "<td>" + rowObject.AssesseeName + "</td>";
                tr += "<td>" + rowObject.HoldingNo + "</td>";
                tr += "<td>" + CONVERTER.Date.convertCsToJsDate(rowObject.PaymentReceiveDate).format('dd/mm/yyyy') + "</td>";
                tr += "<td>" + rowObject.NetAmount + "</td>";
                tr += "<td><a target='_blank' href='/Municipality/MnReports/PaymentReciept?PaymentId=" + rowObject.PaymentID + "'>Print Reciept</a></td>";
                tr += "</tr>";
                return tr;
            };
        };
        var Grid = new function () {
            var ExpandexGroupList = [];
            var totalColNumber = 11;
            var DataSet = [];
            var GiantObject = {};
            var tablContainer = $("#gridContainer");
            var TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
            if (TblCtrl.length === 0) {
                tablContainer.append("<table id='B98E8A35-C901-45A4-8AFF-B486BC58E199' style='width: 100%;margin-bottom: 0px' class='table table-condensed table-bordered table-responsive'><thead></thead><tbody></tbody><tfoot></tfoot></table>");
                TblCtrl = tablContainer.find("#B98E8A35-C901-45A4-8AFF-B486BC58E199");
            }
            var tableHeader = function () {
                var hr = "<tr >" +
                    "<th rowspan='2' style=' text-align: center;'>Fin Year</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Qtr</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Property Tax</th>" +
                    "<th rowspan='2' style=' text-align: center;'>Surcharge</th>" +
                    "<th  colspan='2' style=' text-align: center;'>Outstanding</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Rebate/ Interest</th>" +
                    "<th  colspan='2' style=' text-align: center;'>Paying</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Action<br/><input type='checkbox' class='checkall'/></th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Line Total</th>" +
                    "<th  rowspan='2' style=' text-align: center;'>Payment Flow</th>" +
                    "</tr>" +
                    "<tr >" +
                    "<th style=' text-align: center;'>P. Tax</th>" +
                    "<th style=' text-align: center;'>Surcharge</th>" +
                    "<th style=' text-align: center;'>P. Tax</th>" +
                    "<th style=' text-align: center;'>Surcharge</th>" +
                    "</tr>";

                return hr;
            };
            var tableBody = function () {
                var rowFormattedObjects = function () {
                    if (GiantObject === null || typeof (GiantObject) == 'undefined' || GiantObject.hasOwnProperty("propertyTaxOutstadings") == false || GiantObject.hasOwnProperty("propertyTaxPaymentMaster") == false) {
                        throw "Invalid DataSource";
                    }
                    var lst = [];
                    $.each(GiantObject.propertyTaxOutstadings, function (index, value) {
                        var _calculatedDetail = GiantObject.propertyTaxPaymentMaster.PropertyTaxPaymentDetails[index];
                        var viewRow = {
                            FinYear: value.FinYear,
                            PropertyTaxValue: value.PropertyTaxValue,
                            SurchargeValue: value.SurchargeValue,
                            QtrNo: value.QtrNo,
                            Rank: value.Rank,
                            OSPropertyTaxAmt: value.OSPropertyTaxAmt,
                            OSSurchargeAmt: value.OSSurchargeAmt,
                            OSCalculatedValue: _calculatedDetail.CalculatedValue,
                            PaidSurchargeAmt: _calculatedDetail.PaidSurchargeAmt,
                            PaidPropertyTaxAmt: _calculatedDetail.PaidTaxAmt,
                            PaidCalculatedAmt: _calculatedDetail.PaidCalculatedAmt,
                            PayheadDetailLvl: _calculatedDetail.payHead,
                            IsChecked: _calculatedDetail.taxPaymentCheckParameter.IsChecked,
                            Rank: _calculatedDetail.taxPaymentCheckParameter.Rank
                        };
                        lst.push(viewRow);
                    });
                    return lst;
                };
                var lstRowStrings = "";

                var lstFormattedDatasource = rowFormattedObjects();
                DataSet = lstFormattedDatasource;

                if (lstFormattedDatasource.length > 0) {
                    window.groupbyTestSource = lstFormattedDatasource;
                    //group by finyear
                    alasql('SELECT FinYear FROM ? GROUP BY FinYear order by CAST(SUBSTRING(FinYear, 1, 4) AS NUMBER) asc', [groupbyTestSource]).forEach(function (valuep, index) {
                        var getColorCode = randomColor({
                            luminosity: 'light',
                            hue: 'blue'
                        });;
                        //add header row
                        var headerDiv = "<div data-isExpanded='false'><span class='state-symbol'>+</span>&nbsp;&nbsp;<span class='headerTitle'>" + valuep.FinYear + "</span></div>";
                        var summaryParameters = { osPtax: 0, osSurcharge: 0, calvalue: 0, paidPtax: 0, paidSurcharge: 0, groupCheck: "<input type='checkbox' class='groupCheck' data-finyear='" + valuep.FinYear + "'/>", tillTotal: 0 };
                        lstRowStrings += "<tr  class='data-header' data-FinYear='" + valuep.FinYear + "' style='text-align: center;background-color:" + getColorCode + ";cursor:pointer;'>" +
                            "<td colspan='4' style='text-align: left;' class='Summary'>" + headerDiv + "</td>" +
                            "<td class='OSPropertyTaxAmt'><span>" + summaryParameters.osPtax + "</span></td>" +
                            "<td  class='OSSurchargeAmt'><span>" + summaryParameters.osSurcharge + "</span></td>" +
                            "<td class='PaidCalculatedAmt'><span>" + summaryParameters.paidPtax + "</span></td>" +
                            "<td class='PaidPropertyTaxAmt'><span>" + summaryParameters.paidSurcharge + "</span></td>" +
                            "<td class='PaidSurchargeAmt'><span>" + summaryParameters.calvalue + "</span></td>" +
                            "<td><span>" + summaryParameters.groupCheck + "</span></td>" +
                            "<td class='lineTotal'><span>0</span></td>" +
                            "<td class='lineTotalFlow'><span></span></td></tr>";

                        var elementsByGroup = alasql("SELECT * FROM ? where FinYear='" + valuep.FinYear + "' order by Rank", [window.groupbyTestSource]);
                        $.each(elementsByGroup, function (index, value) {
                            var tr = "<tr class='data-row' data-rank='" + value.Rank + "' data-parentGroup='" + value.FinYear + "' style=' text-align: center;display:none;'>";
                            tr += "<td class='FinYear'><span>" + value.FinYear + "</span></td>";
                            tr += "<td class='QtrNo'>" + value.QtrNo + "</td>";
                            tr += "<td class='PropertyTaxValue'>" + value.PropertyTaxValue + "</td>";
                            tr += "<td class='SurchargeValue'>" + value.SurchargeValue + "</td>";
                            tr += "<td class='OSPropertyTaxAmt'>" + value.OSPropertyTaxAmt + "</td>";
                            tr += "<td class='OSSurchargeAmt'>" + value.OSSurchargeAmt + "</td>";
                            tr += "<td class='PaidCalculatedAmt'>" + value.PaidCalculatedAmt + "</td>";
                            tr += "<td class='PaidPropertyTaxAmt'>" + value.PaidPropertyTaxAmt + "</td>";
                            tr += "<td class='PaidSurchargeAmt'>" + value.PaidSurchargeAmt + "</td>";
                            tr += "<td class='IsChecked'><input class='chkPay' type='checkbox' " + (value.IsChecked === true ? " checked " : "") + " data-FinYear='" + value.FinYear + "' data-QtrNo='" + value.QtrNo + "' data-rank='" + value.Rank + "'/></td>";
                            tr += "<td class='lineTotal'><span>" + (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt).toFixed(2) + "</span></td>";
                            tr += "<td class='lineTotalFlow'><span >0</span></td>";
                            tr += "</tr>";
                            lstRowStrings += tr;
                        });
                    });


                } else {
                    lstRowStrings = "<tr><td coldspan='11'>No Record found</td></tr>";
                }
                return lstRowStrings;
            };
            var tableFooter = function () {

                var getTotalPtaxSurch = function (totalPtaxSurchg) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total PTax./SChrg.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalPtaxSurch'>" + totalPtaxSurchg + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getTotalRebateInt = function (totalRbtInt) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Rbt./Int.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalRebateInt'>" + totalRbtInt + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getTotalOtherAdjAmount = function (totalOthrAdjAmnt) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Total Adjst. Amnt.</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='TotalOtherAdjAmount'>" + totalOthrAdjAmnt + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };
                var getRoundOff = function (roundOff) {
                    var x = "<tr>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Rounded Off</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='RoundOff'>" + roundOff + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };

                var getNetPayable = function (netPayable) {
                    var x = "<tr style='font-size: 18px;'>";
                    x += "<td style='text-align:left'><span style='font-weight: bold;'>Net Payable</span></td>";
                    x += "<td>:</td>";
                    x += "<td style='text-align:right'><span class='NetPayable' >" + netPayable + "</span><span>&nbsp;Rs/-</span></td>";
                    x += "</tr>";
                    return x;
                };

                var getMainFooterSummary = function () {
                    var x = "<div class='container-fluid'>"+
                            "<div class='row'>"+
                                "<div class='col-md-6'><table><tbody>" + getTotalPtaxSurch(0.00) + getTotalRebateInt(0.00) + getTotalOtherAdjAmount(0.00) + getRoundOff(0.00)  + "</tbody></table></div>"+
                                "<div class='col-md-6'><table><tbody>"  + getNetPayable(0.00) + "</tbody></table></div>"+
                            "</div>"+
                        "</div>";
                    return x;
                };
                var tf = "<tr class='data-summary' style=' text-align: center;'>";
                tf += "<td class='Summary' colspan='4'>Summary</td>";
                tf += "<td class='OSPropertyTaxAmt'><span>0</span></td>";
                tf += "<td class='OSSurchargeAmt'><span>0</span></td>";
                tf += "<td class='PaidCalculatedAmt'><span>0</span></td>";
                tf += "<td class='PaidPropertyTaxAmt'><span>0</span></td>";
                tf += "<td class='PaidSurchargeAmt'><span>0</span></td>";
                tf += "<td class='check'><span>0</span></td>";
                tf += "<td class='lineTotal'><span>0</span></td>";
                tf += "<td class='lineTotalFlow'><span></span></td>";
                tf += "</tr>";


                tf += "<tr class='data-summary-payment' style=' text-align: center;'>";
                tf += "<td class='Payment-Summary' colspan='4'></td>";
                tf += "<td class='' colspan='6'>" + getMainFooterSummary()+"</td>";
                //tf += "<td class='roundOf' colspan='3'><span style='font-weight: bolder;font-size: 17px'>Round Off: 0 Rs/-</span></td>";
                tf += "<td class='payButton' colspan='2' style='vertical-align: middle;'><input type='button' data-payment-action='N' class='btn btn-primary btn-block pull-right proceed-payment ' value='Proceed' id='btnProceedPayment' ></td>";
                tf += "</tr>";
                return tf;
            };
            this.DataBind = function () {
                TblCtrl.find('thead').empty();
                TblCtrl.find('thead').append(tableHeader());

                TblCtrl.find('tbody').empty();
                TblCtrl.find('tbody').append(tableBody());

                TblCtrl.find('tfoot').empty();
                TblCtrl.find('tfoot').append(tableFooter());
                SetLineTotalFlow();
                SetTableFooter();
                ManageHeaderCheckBoxes();
            };
            var SetLineTotalFlow = function () {
                var lineTotal = 0;
                $.each(DataSet, function (index, value) {
                    if (value.IsChecked == true) {
                        lineTotal += (value.PaidCalculatedAmt + value.PaidPropertyTaxAmt + value.PaidSurchargeAmt);
                        lineTotal = Number(lineTotal.toFixed(2));
                        var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentgroup='" + value.FinYear + "'][data-rank='" + value.Rank + "'] .lineTotalFlow span");
                        tergateCtrl.html(lineTotal);
                    }
                });

                var groupSums = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                    'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true GROUP BY FinYear ', [DataSet]);
                $.each(groupSums, function (index, value) {
                    var tergateRow = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + value.FinYear + "']");
                    var lineTotalFlow = tergateRow.find(" .lineTotal span");
                    var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                    var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                    var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                    var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                    var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");

                    lineTotalFlow.html(value.lineTotal.toFixed(2));
                    PaidCalculatedAmt.html(value.PaidCalculatedAmt.toFixed(2));
                    PaidPropertyTaxAmt.html(value.PaidPropertyTaxAmt.toFixed(2));
                    PaidSurchargeAmt.html(value.PaidSurchargeAmt.toFixed(2));
                    OSPropertyTaxAmt.html(value.OSPropertyTaxAmt.toFixed(2));
                    OSSurchargeAmt.html(value.OSSurchargeAmt.toFixed(2));
                });
            };
            var SetTableFooter = function () {

                var summary = alasql('SELECT SUM(PaidCalculatedAmt+PaidPropertyTaxAmt+PaidSurchargeAmt) as lineTotal,SUM(PaidCalculatedAmt)as PaidCalculatedAmt,SUM(PaidPropertyTaxAmt) as PaidPropertyTaxAmt,SUM(PaidSurchargeAmt) as PaidSurchargeAmt,' +
                    'SUM(OSPropertyTaxAmt) as OSPropertyTaxAmt, SUM(OSSurchargeAmt) as OSSurchargeAmt, FinYear FROM ? where IsChecked= true', [DataSet]);

                if (GiantObject.hasOwnProperty("propertyTaxPaymentMaster") && GiantObject.propertyTaxPaymentMaster) {
                    var PTaxmasterObj = GiantObject.propertyTaxPaymentMaster;

                    var tergateRow = $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary");
                    var lineTotalFlow = tergateRow.find(" .lineTotal span");
                    var PaidCalculatedAmt = tergateRow.find(" .PaidCalculatedAmt span");
                    var PaidPropertyTaxAmt = tergateRow.find(" .PaidPropertyTaxAmt span");
                    var PaidSurchargeAmt = tergateRow.find(" .PaidSurchargeAmt span");
                    var OSPropertyTaxAmt = tergateRow.find(" .OSPropertyTaxAmt span");
                    var OSSurchargeAmt = tergateRow.find(" .OSSurchargeAmt span");
                    var totalChecked = tergateRow.find(" .check");

                    var groupByFinYearChecked = alasql('SELECT Count(*)as checkCounts FROM ?  where IsChecked=true', [DataSet]);

                    if (summary.length > 0) {
                        summary = summary[0];
                        lineTotalFlow.html(summary.lineTotal.toFixed(2));
                        PaidCalculatedAmt.html(summary.PaidCalculatedAmt.toFixed(2));
                        PaidPropertyTaxAmt.html(summary.PaidPropertyTaxAmt.toFixed(2));
                        PaidSurchargeAmt.html(summary.PaidSurchargeAmt.toFixed(2));
                        OSPropertyTaxAmt.html(summary.OSPropertyTaxAmt.toFixed(2));
                        OSSurchargeAmt.html(summary.OSSurchargeAmt.toFixed(2));
                        totalChecked.html(groupByFinYearChecked.length > 0 ? groupByFinYearChecked[0].checkCounts : 0);
                    }
                   


                    //summary value gather data
                   
                    var TotalPtaxSurch = PTaxmasterObj.GrossAmount;
                    var TotalRebateInt = summary.PaidCalculatedAmt.toFixed(2);
                    var TotalOtherAdjAmount = PTaxmasterObj.AdjustedAmount;
                    var RoundOff = PTaxmasterObj.RoundOffAdjust;
                    var NetPayable = PTaxmasterObj.NetAmount ;
                    ///end gathering data
                   
                    //summary description
                    var containerTable = ("<table><tbody>");
                    var paymentSummay = "";
                    $.each(PTaxmasterObj.AdjustmentSummaries, function (index, value) {
                        paymentSummay += "<tr><td style='text-align:left;'>" + value.WellKnownName + "</td><td>:</td><td> &nbsp;" + value.AdjustedAmount + " Rs/-</td></tr>";
                    });
                    containerTable += paymentSummay;
                    containerTable += "</tbody></table>";

                    

                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalPtaxSurch').text(TotalPtaxSurch);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalRebateInt').text(TotalRebateInt);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.TotalOtherAdjAmount').text(TotalOtherAdjAmount);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.RoundOff').text(RoundOff);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.NetPayable').text(NetPayable);
                    $("#" + TblCtrl.attr('id') + " > tfoot >tr.data-summary-payment").find('.Payment-Summary').html(containerTable);
                                       
                    GlobalVariables.NetAmount = NetPayable;
                    
                    //write amount bydefault if cash mode available
                    
                }
            };
            var OnClickProceedButton = function () {
                if ($(this).attr("data-payment-action")) {
                    var obj = { paymentAction: $(this).attr("data-payment-action")};
                    PaymentCompletionModal.ResetModal();
                    PaymentCompletionModal.ShowModal(obj);
                }
            };
            //manage check uncheck state group header check boxes and Select All Check box
            var ManageHeaderCheckBoxes = function () {
                var groupByFinYearAll = alasql('SELECT count(*) as rowCounts,FinYear FROM ?   group by FinYear', [DataSet]);
                var groupByFinYearChecked = alasql('SELECT sum(case when IsChecked=true then 1 else 0 end)as rowCounts ,FinYear FROM ?  group by FinYear', [DataSet]);

                var joinedList = alasql("select A.rowCounts as TotalRowCount,B.rowCounts as TotalCheckedRowCount,A.FinYear  from  ? as A  join  ? as B on A.FinYear=B.FinYear", [groupByFinYearAll, groupByFinYearChecked]);
                //manage check box from group header
                $.each(joinedList, function (index, value) {
                    var tergateCtrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header .groupCheck[data-finyear='" + value.FinYear + "']");
                    if (tergateCtrl.length > 0) {
                        tergateCtrl.prop('checked', (value.TotalCheckedRowCount === value.TotalRowCount))

                    }
                });

                //manage all select check box
                var totalListSummary = alasql("select SUM(TotalRowCount) as TotalRowCount,SUM(TotalCheckedRowCount) as TotalCheckedRowCount  from  ? ", [joinedList]);
                if (totalListSummary.length > 0) {
                    var tergateCtrl = $("#" + TblCtrl.attr('id') + " > thead >tr .checkall");
                    if (tergateCtrl.length > 0) {
                        tergateCtrl.prop('checked', (totalListSummary[0].TotalCheckedRowCount === totalListSummary[0].TotalRowCount))

                    }
                }



            };
            this.GetDataSource = function () {
                return DataSet;
            };

            var OnCheckBoxClick = function () {

                var ctrl = $(this);
                var classes = ctrl.attr('class').split(' ');

                //identify data row checkboxes
                if (classes.indexOf("chkPay") > -1) {
                    var curRank = ctrl.attr('data-Rank');

                    //check all previous check box
                    var totalRank = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                    for (var i = 1; i <= totalRank; i++) {
                        var chkContrl = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row .chkPay[data-rank='" + i + "']");
                        var nearByGroup = $(".groupCheck[data-finyear='" + chkContrl.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                        if (ctrl.prop('checked') == true && i <= curRank) {
                            chkContrl.prop('checked', true);
                            if (nearByGroup.prop("checked") == false) {
                                nearByGroup.prop('checked', true);
                            }
                        } else if (i > curRank) {
                            if (nearByGroup.prop("checked") == true) {
                                nearByGroup.prop('checked', false);
                            }
                            chkContrl.prop('checked', false);
                        }
                    }
                }
                //identify group check boxes
                else if (classes.indexOf("groupCheck") > -1) {
                    var curFinYear = ctrl.attr("data-finyear");
                    var lowerCheckbxes = $(".chkPay[data-finyear='" + curFinYear + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                    var totalRowsCount = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row").length;
                    var lastRankOfCheckboxList = Number($(lowerCheckbxes[lowerCheckbxes.length - 1]).attr("data-rank"));
                    var firstRankOfCheckboxList = Number($(lowerCheckbxes[0]).attr("data-rank"));
                    //var uncheckArrayforGroupCheck = [];
                    for (var i = 1; i <= totalRowsCount; i++) {
                        var trgateCheckBox = $(".chkPay[data-rank='" + i + "']", "#" + TblCtrl.attr('id') + " > tbody >tr.data-row");
                        var nearByGroup = $(".groupCheck[data-finyear='" + trgateCheckBox.attr("data-finyear") + "']", "#" + TblCtrl.attr('id'));
                        if (ctrl.prop('checked') == true && i <= lastRankOfCheckboxList) {
                            trgateCheckBox.prop('checked', true);
                            if (nearByGroup.prop("checked") == false) {
                                nearByGroup.prop('checked', true);
                            }
                        } else if (ctrl.prop('checked') == false && i >= firstRankOfCheckboxList) {
                            if (nearByGroup.prop("checked") == true) {
                                nearByGroup.prop('checked', false);
                                $(".checkall", "#" + TblCtrl.attr('id')).prop('checked', false);
                            }
                            trgateCheckBox.prop('checked', false);
                        }
                    };

                }
                //identify header check boxes
                else if (classes.indexOf("checkall") > -1) {
                    var trgateCheckBoxes = $(".chkPay, .groupCheck", "#" + TblCtrl.attr('id') + " > tbody >tr");
                    trgateCheckBoxes.prop('checked', ctrl.prop('checked'));
                }
                var data = searchableObject();

                PopulateData(data, function (resp) {
                    ManageGroupVisibility();
                });
            };
            var ManageGroupVisibility = function () {
                $.each(ExpandexGroupList, function (index, value) {
                    ExpandGroup(value);
                });
            };
            var BindEvent = function () {
                $(document).off('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
                $(document).on('change', " .groupCheck,.checkall, .chkPay", "#" + TblCtrl.attr('id'), OnCheckBoxClick);
                $(document).off('click', "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div');
                $(document).on("click", "#" + TblCtrl.attr('id') + ' > tbody >tr.data-header div', function () {
                    var curDiv = $(this);
                    var currGroup = curDiv.closest('tr').attr('data-finyear');
                    var isExpanded = curDiv.attr('data-isExpanded');
                    if (isExpanded == 'false') {
                        ExpandGroup(currGroup);
                    } else {
                        CollapseGroup(currGroup);
                    }

                });

                //remove scope
                //$(document).off('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);
                //$(document).on('click', ".proceed-payment", "#" + TblCtrl.attr('id') + ' > tfoot > tr.data-summary-payment .payButton', OnClickProceedButton);

                $(document).off('click', ".proceed-payment",  OnClickProceedButton);
                $(document).on('click', ".proceed-payment",  OnClickProceedButton);
            };
            var ExpandGroup = function (finYear) {
                var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
                var headerDiv = headerTr.find('div');
                var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

                headerDiv.find('.state-symbol').html('-');
                headerDiv.attr('data-isExpanded', 'true');
                relatedDataRows.each(function (indx, elemnt) {
                    $(elemnt).show('fast');
                });
                if ((ExpandexGroupList.indexOf(finYear) > -1) == false)//if not exist
                {
                    ExpandexGroupList.push(finYear);
                }
            };
            var CollapseGroup = function (finYear) {
                var headerTr = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-header[data-finyear='" + finYear + "']");
                var headerDiv = headerTr.find('div');
                var relatedDataRows = $("#" + TblCtrl.attr('id') + " > tbody >tr.data-row[data-parentGroup='" + finYear + "']");

                headerDiv.find('.state-symbol').html('+');
                headerDiv.attr('data-isExpanded', 'false');
                relatedDataRows.each(function (indx, elemnt) {
                    $(elemnt).hide('fast');
                });
                if ((ExpandexGroupList.indexOf(finYear) > -1))//if not exist
                {
                    ExpandexGroupList.splice(ExpandexGroupList.indexOf(finYear), 1);
                }
            };
            //paramter type PTaxCollectionGiantObject which is downloaded from ~/Municipality/Collection/Get_PropertyTaxPaymentDetails
            this.SetDataSource = function (PTaxCollectionGiantObject) {
                GiantObject = PTaxCollectionGiantObject;
            };
            this.Reset = function () {
                DataSet = [];
                GiantObject = {};
                
                TblCtrl.find('thead').empty();
                TblCtrl.find('tbody').empty();
                TblCtrl.find('tfoot').empty();
            };
            this.GetSelectedRowObjs = function () {
                var inputs = $(".chkPay:checked", "#B98E8A35-C901-45A4-8AFF-B486BC58E199 > tbody >tr.data-row");
                var objs = [];// data-finyear, 2: data-qtrno, 3: data-rank,
                $.each(inputs, function (index, value) {
                    objs.push({
                        FinYear: $(value).attr("data-finyear"),
                        QtrNo: $(value).attr("data-qtrno"),
                        IsChecked: true,
                        Rank: $(value).attr("data-rank")
                    });
                });
                return objs;
            };
            this.CheckAllRow = function (checkValue) {
                TblCtrl.find('thead').find('.checkall').prop('checked', checkValue);
                TblCtrl.find('tbody').find('.groupCheck').prop('checked', checkValue);
                TblCtrl.find('tbody').find('.chkPay').prop('checked', checkValue);
            };
            (function () {
                BindEvent();
            }());
        };
        var Validator = {
            ValidateSearchFields: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControls.ctrl_hdnWard.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_hdnLocation.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_hdnAssessee.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: true, validDate:true};
                vldObj.rules[DomControls.ctrl_NoticeServedDate.prop('name')] = { validDate: true };
                vldObj.rules[DomControls.ctrl_ddlEffectiveQtrFrom.prop('name')] = { validQtr: true }; 

                //set error messages
                vldObj.messages[DomControls.ctrl_hdnWard.prop('name')] = { required: "Please select Ward." };
                vldObj.messages[DomControls.ctrl_hdnLocation.prop('name')] = { required: "Please select Location." };
                vldObj.messages[DomControls.ctrl_hdnAssessee.prop('name')] = { required: "Please select Assessee" };
                vldObj.messages[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: "Please select Collection Date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                $("#frmPropertyTaxPaymentSearch").validate(vldObj);
                //validate form and return true and false
                return $('#frmPropertyTaxPaymentSearch').valid();
            },
            ResisterValidatorMethods: function () {
                $.validator.addMethod(
                    "validDate",
                    function (value, element, params) {
                        if (this.optional(element)) {  // "required" not in force and field is empty
                            return true;
                        } else {  // otherwise, use your rule
                            var regEx = /^\d{4}-\d{2}-\d{2}$/;
                            if (!value.match(regEx)) return false;  // Invalid format
                            var d = new Date(value);
                            if (!d.getTime()) return false; // Invalid date (or this could be epoch)
                            return d.toISOString().slice(0, 10) === value;
                        }
                    },
                    "Enter Valid Date"
                );
                $.validator.addMethod(
                    "validQtr",
                    function (value, element, params) {
                            if (DomControls.ctrl_NoticeServedDate.val() !== '')
                            {
                                if ([1, 2, 3, 4].indexOf(Number(DomControls.ctrl_ddlEffectiveQtrFrom.val())) === -1)
                                {
                                    return false;
                                } else {
                                    return true;
                                }
                            } else {
                                return true;
                            }
                    },
                    "select Valid Quarter"
                );
            }

        };
        //obsolete
        var ToggleSearchField = function () {
            if (GlobalVariables.IsSearchEnabled === true) {
                DomControls.ctrl_txtAssessee.attr('disabled', 'disabled');
                DomControls.ctrl_txtLocation.attr('disabled', 'disabled');
                DomControls.ctrl_txtWard.attr('disabled', 'disabled');
                //DomControls.ctrl_btnSearch.prop('value', 'Re-Search');
                DomControls.ctrl_txtCollectionDate.attr('disabled', 'disabled');
                DomControls.ctrl_NoticeServedDate.attr('disabled', 'disabled');
                DomControls.ctrl_ddlEffectiveQtrFrom.attr('disabled', 'disabled');

                GlobalVariables.IsSearchEnabled = false;

                $('.search-by-assesseeID').attr('value', 'Reset')
                $('.txtSearchByAssesseeID ').prop('disabled', true);

                $('.btn-search-by-holding-no').attr('value', 'Reset')
                $('.txtHoldingNo').prop('disabled', true);
            } else {//reset search form
                DomControls.ctrl_txtAssessee.removeAttr('disabled');
                DomControls.ctrl_txtLocation.removeAttr('disabled');
                DomControls.ctrl_txtWard.removeAttr('disabled');
                DomControls.ctrl_txtCollectionDate.removeAttr('disabled');
                DomControls.ctrl_NoticeServedDate.removeAttr('disabled');
                //DomControls.ctrl_ddlEffectiveQtrFrom.removeAttr('disabled');
                Resetors.ResetEffectiveQtrFrom();
                DomControls.ctrl_btnSearch.prop('value', 'Search');

                GlobalVariables.IsSearchEnabled = true;

                Resetors.ResetWard();
                Resetors.ResetLocation();
                Resetors.ResetAssesse();
                Resetors.ResetCollectionDt();
                Resetors.ResetNoticeServedDate();
                GlobalVariables.PaymentObject = {};
                GlobalVariables.SelectedGridRows = [];
                Grid.Reset();

                $('.search-by-assesseeID').attr('value', 'Search')
                $('.txtSearchByAssesseeID ').val('');
                $('.txtSearchByAssesseeID ').prop('disabled', false);
                $(".last-20-payment-by-assessee-panel .panel-body").empty();

                $('.btn-search-by-holding-no').attr('value', 'Search Holdings')
                $('.txtHoldingNo').prop('disabled', false);
                $('.txtHoldingNo').val();
                $(".searched-holding-result > .list-group").empty();
            }
        };
        var LoadCounter = function () {
            serverObject.GetCounter(function (response) {
                if (response.Status === 200) {
                    if (response.Data !== null) {
                        GlobalVariables.PaymentCounter = response.Data;
                        $(".payment-collection-panel > .panel-heading  .counteDesc").html(response.Data.CounterCode).attr('title', "Counter code " + response.Data.CounterCode);
                    } else {
                        alert("Error: Counter Not Configured for current user");
                    }
                } else {
                    alert("Error: " + response.Message);
                }
            });
        };
        var LoadPaymentModes = function () {
            serverObject.GetPaymentModes(function (response) {
                if (response.Status === 200) {
                    if (response.Data.length > 0) {
                        GlobalVariables.PaymentModes = response.Data;
                        window.ss = response.Data;
                        var paymentModes = "";
                        alasql('select PaymentModeDescription from ?', [response.Data]).forEach(function (value, index) {
                            paymentModes += value.PaymentModeDescription + (index == response.Data.length - 1 ? "" : ", ");
                        });

                        $(".payment-collection-panel > .panel-heading  .payment-mode-desc").html(paymentModes).attr('title', "Available payment modes " + paymentModes);

                    } else {
                        alert("Error: Payment Mode yet Not Configured for Counter");
                    }
                } else {
                    alert("Error: " + response.Message);
                }
            });
        };
        var PopulateData = function (payload, callback) {
            if (Validator.ValidateSearchFields()) {
                var selectedAssess = GlobalVariables.Assessee;
                Rest_GlobalVariable();
                GlobalVariables.Assessee = selectedAssess;
                LockSearchField();
                serverObject.GetPaymentDetails(payload, function (response) {
                    if (response.Status === 200) {
                        var Data = response.Data;
                        if(Data==null){
                            dataPopulationAlert.show_PaymentInfoNotFound();
                        }else{
                            serverObject.GetManualAdvanceAdjAvailability(payload, function (r) {
                                if (r.Status === 200) {
                                    if (r.Data.ManualAdjAvailabity !== null && r.Data.ManualAdjAvailabity.IsAvailable === true) {
                                        var redirectTo = r.Data.RedirectTo;
                                        var advAmount = r.Data.ManualAdjAvailabity.AdvanceAmount;
                                        alertify.alert("Adjustable advance available, Rs. " + advAmount + "/-", "<a href=" + redirectTo + " target='_blank'>Click here</a> to manually adjust advance");
                                    }
                                } else {
                                    alertify.error("Could not get Manuall advance info. Detail: " + r.Message);
                                }
                            });
                            GlobalVariables.PaymentObject = Data;
                            Grid.SetDataSource(GlobalVariables.PaymentObject);
                            Grid.DataBind();
                            dataPopulationAlert.hide();
                            dataPopulationAlert.show_AdvancePaymentLink();
                        }
                        callback(Data);
                    } else {
                        alert('Alert: ' + response.Message);
                    }
                });
                
            }
        };
        var OnSuccessfullSeachAssesse = function (assessee) {
            GlobalVariables.Assessee = assessee;
            DomControls.ctrl_holdingType.text(assessee.HoldingTypeDesc);
        };
        var OnSearchClick = function () {
            Grid.Reset();
            dataPopulationAlert.reset();
            if ($("#frmPropertyTaxPaymentSearch").valid()) {
                dataPopulationAlert.show_ProcessingPaymentInfo();
                var data = searchableObject(true);
                PopulateData(data, function (response) {
                    Grid.CheckAllRow(true);
                    GlobalVariables.gridLast20PaymentsByAssessee.Loadtable(isNaN(DomControls.ctrl_hdnAssessee.val()) ? 0 : DomControls.ctrl_hdnAssessee.val(), function (table) {
                        $(".last-20-payment-by-assessee-panel .panel-body").append(table);
                    });
                });
            }
        };
        var OnSearchByAssesseeIDClicked = function () {
           
            var serchBtn = $(this);
            var payload = { AssesseeID: DomControls.ctrl_txtSearchByAssesseeID.val() };
            if (payload.AssesseeID.trim() == '' || isNaN(payload.AssesseeID))
            {
                alert("Please enter valid AssesseeID");
                DomControls.ctrl_txtSearchByAssesseeID.focus();
                return;
            }
            serverObject.GetAssesseeByID(payload, function (response) {
                if (response.Status === 200)
                {
                    var data = response.Data;
                    if (data != null) {
                        var dt = new Date();
                        OnSuccessfullSeachAssesse(data);
                        DomControls.ctrl_hdnWard.val(data.Ward.WardID);
                        DomControls.ctrl_txtWard.val(data.Ward.WardName);

                        DomControls.ctrl_hdnLocation.val(data.Location.LocationID);
                        DomControls.ctrl_txtLocation.val(data.Location.LocationName);

                        DomControls.ctrl_hdnAssessee.val(payload.AssesseeID);
                        DomControls.ctrl_txtAssessee.val("<" + data.HoldingNo + "> " + data.AssesseeName);
                        var dtt = DomControls.ctrl_txtCollectionDate.val();
                        if (DomControls.ctrl_txtCollectionDate.val() == undefined || DomControls.ctrl_txtCollectionDate.val() == '' || DomControls.ctrl_txtCollectionDate.val() == null) {
                            DomControls.ctrl_txtCollectionDate.val(dt.format('yyyy-mm-dd'));
                        } 
                        DomControls.ctrl_btnSearch.trigger('click');
                    } else {
                        alert("Assessee not found! Try again");
                    }
                } else {
                    alert(response.Message);
                }
            });
        };
        var OnSearchAssesseesByHolding = function () {
            var txtHoldingNo = $("#txtHoldingNo").val();
            var SearchedList = new function () {
                var listContainer = $(".searched-holding-result > .list-group");
                var dataFieldTemplate = function (isSelected, assesseeName, assesseeID, Ward, location) {
                    strListItem = "<a class='list-group-item data-field " + (isSelected === true ? "active" : "") + "' data-assessee-id='" + assesseeID + "' data-assessee-name='" + assesseeName + "' data-ward='" + Ward + "' data-location='" + location + "' title='Assessee ID: " + assesseeID + "'>";
                    strListItem += "<div class='container-fluid'>";
                    strListItem += "<div class='row'>";
                    strListItem += "<div class='col-md-11 col-sm-11 col-xs-10'>";
                    strListItem += "<p class='list-group-item-text' ><span class='glyphicon glyphicon-user'></span>&nbsp; <b>" + assesseeName + "</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>Ward:</b> " + Ward + "&nbsp;&nbsp;&nbsp;&nbsp; <b>Location:</b> " + location + "</p >";
                    strListItem += "</div>";
                    strListItem += "<div class='col-md-1 col-sm-1 col-xs-2'><button class='pull-right glyphicon glyphicon-arrow-right btn-searchitem-holding' data-assessee-id='" + assesseeID + "'> </div>";
                    strListItem += "</div>";
                    strListItem += "</div>";
                    strListItem += "</a >";
                    return strListItem;
                };
                var noAssesseeFoundTamplate = function () {
                    strListItem = "<a class='list-group-item no-assessee-found-field'>";
                    strListItem += "<p class='list-group-item-text' style='text-align:center'>No Assessee Found</p >";
                    strListItem += "</a >";
                    return strListItem;
                };
                var searchFieldTemplate = function (recordNumber) {
                    strField = "<a class='input-group search-field-holding-search-panel' style='width:100%;'>"
                    strField += "<input id= 'txtSearchOnHolding' type= 'text' class='form-control' name= 'email' placeholder= 'Seach with Name, Ward, Location " + (recordNumber != null ? " amoung " + recordNumber+" records":"") + "' >";
                    strField += "</a >";
                    return strField;
                };
                this.emptyDataFields = function () {
                    $(".searched-holding-result > .list-group > .data-field").remove();
                };
                this.removeNoAssesseeFoundFiled = function () {
                    $(".searched-holding-result > .list-group > .no-assessee-found-field").remove();
                };
                this.removeSearhField = function () {
                    $(".searched-holding-result > .list-group > .search-field-holding-search-panel").remove();
                };
                this.emptySearchedList = function () {
                    $(".searched-holding-result > .list-group").empty();;
                };
                this.addSearchField = function (recordNumber) {
                    listContainer.append(searchFieldTemplate(recordNumber));
                };
                this.addDataField = function (isActive,data) {
                    listContainer.append(dataFieldTemplate(isActive,data.AssesseeName, data.AssesseeID, data.WardName,  data.LocationName ));
                };
                this.addNoAssesseeFoundField = function () {
                    listContainer.append(noAssesseeFoundTamplate());
                };
                
            };
            SearchedList.emptySearchedList();
            serverObject.SearchAssesseesByHolding({ HoldingNo: txtHoldingNo }, function (response) {
                if (response.Status === 200)
                {
                    if (response.Data.length > 0) {
                        SearchedList.addSearchField(response.Data.length);
                        response.Data.forEach(function (value, index) {
                            SearchedList.addDataField((index == 0 ? true : false), value);
                        });
                    } else {
                        SearchedList.addNoAssesseeFoundField();
                    }
                }
            });
        };
       
        var BindEvents = function () {
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            DomControls.ctrl_txtAssessee.autocomplete(AutocompleteSorces.Holdings());
            DomControls.ctrl_NoticeServedDate.on('change', function () {
                if ($(this).val() !== '') {
                    DomControls.ctrl_ddlEffectiveQtrFrom.removeAttr('disabled');
                }
            });
            DomControls.ctrl_CollectionDate.datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                maxDate: new Date(),
                onSelect: function (dateText, inst) {
                    DomControls.ctrl_NoticeServedDate.datepicker('option', 'maxDate', dateText);
                    var _FinYear = getFinYearFromDate(new Date(dateText));
                    var firstDateOfFinYear = getFirstDateFromFinyear(_FinYear);
                    DomControls.ctrl_NoticeServedDate.datepicker('option', 'minDate', firstDateOfFinYear);
                    if (DomControls.ctrl_NoticeServedDate.val()) {
                        DomControls.ctrl_NoticeServedDate.val(firstDateOfFinYear);
                    }
                }
            });
            DomControls.ctrl_NoticeServedDate.datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium',
                maxDate: new Date(),
                minDate: window._UTILS.getFirstDateFinYear(window._SERVER.getCurFinYear())
            });
            DomControls.ctrl_btnPay.on('click', function () {

            });
            DomControls.ctrl_btnSearch.on('click', OnSearchClick);
            $(document).off('click', '.search-by-assesseeID', OnSearchByAssesseeIDClicked);
            $(document).on('click', '.search-by-assesseeID', OnSearchByAssesseeIDClicked);
            $(".searched-holding-result").on('click', '.list-group > a', function () {
                $(".searched-holding-result > .list-group >a").removeClass('active');
                $(this).addClass('active');
            });
            $("#btn-search-by-holding-no").on('click', OnSearchAssesseesByHolding);
            $(document).on('keyup', ".search-field-holding-search-panel>input", function () {
                var lstItems = $(".searched-holding-result > .list-group").find('.data-field');
                var value = $(this).val().toUpperCase();
                if (value === '') {
                    lstItems.show();
                    return false;
                }

                lstItems.each(function (index,item) {
                    //if (index !== 0) {

                        $item = $(this);

                        var field1 = $item.attr('data-ward').toUpperCase();
                        var field2 = $item.attr('data-assessee-name').toUpperCase();
                        var field3 = $item.attr('data-location').toUpperCase();

                        if ((field1.indexOf(value) > -1) || (field2.indexOf(value) > -1) || (field3.indexOf(value) > -1)) {
                            $item.show();
                            console.log(field2 + '=>shown');
                        }
                        else {
                            $item.hide();
                            //console.log(field2 + '=>hidden');
                        }
                    //}
                });
            });
            $(document).on('keyup', ".search-field-holding-search-panel>input", function () { });
            $(document).on('click', ".searched-holding-result > .list-group > .data-field .btn-searchitem-holding", function (e) {
                e.preventDefault();
               
                var assesseeID = $(this).attr('data-assessee-id');
                $(".txtSearchByAssesseeID").val(assesseeID);
                $('.search-by-assesseeID').trigger('click');
            });
            $("#btnReset").on('click', function () {
                Resetors.ReseteWholeForm();
            });
            $(document).on('click', "[data-reset]", function () {
                var tergate = $(this).siblings("input[type='text']");
                if (tergate.prop('disabled') == false) {
                    tergate.val('');
                }
            });
            Validator.ResisterValidatorMethods();
        };

        (function () {
            
            LoadPaymentModes();
            LoadCounter();
            BindEvents();
            registerValidator.resisterSearchForm();
            var lst20Paymnts = new Last20PaymentControl();
            lst20Paymnts.Load();
        }());
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    new INIT();
});