﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.ReportCalling = function (data, callback) {
            alert('calling..');
            $.ajax({
                type: 'post',
                contentType: 'application/json;cursor=utf-8',
                data: JSON.stringify(data),
                dataType: 'json',
                url: "/Municipality/MnReports/AssesseeListReport",
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    alert('error occur');
                }
            });
        };
        this.GetHoldingType = function (d, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/HoldingType/GetHoldingType",
                data: JSON.stringify(d),
                dataType: "json",
                success: function (respons) {
                    callback(respons.Data);
                },
                error: function (err) {
                    alert("Error");
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var appObj = this;
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        //Resetors.ResetWard();
                        //Resetors.ResetLocation();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnWard.val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetWard();
                            Resetors.ResetLocation();
                        }
                    }
                };
            },
            Locations: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        //Resetors.ResetWard();
                        //Resetors.ResetLocation();
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetWard();
                            Resetors.ResetLocation();
                        }
                    }
                };
            },
            HoldingType: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnHoldingType.val('');
                        //Resetors.ResetWard();
                        //Resetors.ResetHoldingType();
                        //reset area
                        var data = { Holding: $.trim(request.term) };
                        serverObject.GetHoldingType(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.length > 0) {
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.HoldingTypeDesc,
                                        HoldingTypeID: item.HoldingTypeID
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnHoldingType.val(i.item.HoldingTypeID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                        }
                    }
                };
            },
        };
        var DomControls = {
            ctrl_btnPreviewReport: $("#btnSave"),
            ctrl_btnAssesseePreviewReport: $("#btnAssessee"),

            ctrl_hdnWard: $("#WardID"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#LocationID"),
            ctrl_txtLocation: $("#txtLocation"),

            ctrl_hdnHoldingType: $("#hdnHoldingTypeID"),
            ctrl_txtHoldingType: $("#txtHoldingTyp"),
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            },
            ResetHoldingType: function () {
                DomControls.ctrl_hdnHoldingType.val('');
                DomControls.ctrl_txtHoldingType.val('');
            }
        };
        var OnPreviewReportClick = function () {
            var data = PreviewObject();
            var cmd = $(this).attr('data-command');
            if (cmd == 'OS') {
                $('#frmAssesseeList').attr('action', '/Municipality/MnReports/AssesseeListReport');
            } else if (cmd == 'AS') {
                $('#frmAssesseeList').attr('action', '/Municipality/MnReports/AssesseeListRpt');
            }
            PopulateData(data, function (response) {
            });
        };
        var PreviewObject = function () {
            return { WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val() };
        };
        var Validator = {
            ValidateSearchFields: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControls.ctrl_hdnWard.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_hdnLocation.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[DomControls.ctrl_hdnWard.prop('name')] = { required: "Please select Ward." };
                vldObj.messages[DomControls.ctrl_hdnLocation.prop('name')] = { required: "Please select Location." };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                $("#frmAssesseeList").validate(vldObj);
                //validate form and return true and false
                return $('#frmAssesseeList').valid();
            },
            ValidateFinYear: function () {
                var bak = true;
                var fromYr = [];
                var toYr = [];
                fromYr = $('#ddlOutstandingFrm').val().split('-');
                toYr = $('#ddlOutstandingTo').val().split('-');
                if (fromYr[0] > toYr[0] &&  fromYr[1] > toYr[1]) {
                    alert('From Financial Year not more then To Financial Year');
                    bak = false;
                }
                return bak;
            }
        };
        var PopulateData = function (data, callback) {
            if (Validator.ValidateFinYear()) {
                $("#frmAssesseeList").submit();
            }
        };
        var BindEvents = function () {
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            DomControls.ctrl_txtHoldingType.autocomplete(AutocompleteSorces.HoldingType());
            DomControls.ctrl_btnPreviewReport.on('click', OnPreviewReportClick);
            DomControls.ctrl_btnAssesseePreviewReport.on('click', OnPreviewReportClick);
        };
        var PopulateFinYear = function () {
            var currDT = new Date();
            var currYear = '';
            var currMonth = currDT.getMonth();
            if (currMonth >= 0 && currMonth < 3) {
                currYear = currDT.getFullYear() - 1;
            }
            else {
                currYear = currDT.getFullYear();
            }
            var firstYear = 1950;
            var finyear = '';
            $('<option value=""> -- Select -- </option>').appendTo('#ddlOutstandingFrm, #ddlOutstandingTo');
            for (var i = currYear; i >= firstYear; i--) {
                j = i + 1;
                finyear = i + '-' + j;
                $('<option value="' + finyear + '">' + finyear + '</option>').appendTo('#ddlOutstandingFrm, #ddlOutstandingTo');
            }
        }
        (function () {
            BindEvents();
        }());
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    new INIT();
});