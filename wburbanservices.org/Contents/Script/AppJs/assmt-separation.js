﻿$(document).ready(function () {
    var constants = function () {
        return {
            effectiveFinYearQtr: function () {
                return {
                    ddlEffectiveFinYear: $(".assmt-finyear-qtr .ddl-finyear-picker"),//
                    ddlEffectiveQtr: $(".assmt-finyear-qtr .ddl-qtr")
                };
            },
            
            getValTag: function () {
                return $(".setting-parameters").find('.val-tag').val();
            },
            getYearId: function () {
                //year-id
                return $(".setting-parameters").find('.year-id').val();
            },
            convertTodate: function (format, date) {
                switch (format) {
                    case 'dd/mm/yyyy':
                    case 'dd/mm/yy':
                        {
                            var array = date.split('/');
                            return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                            break;
                        }
                }
            },
            activeTabPane: function () {
                var tab_a = _doms().tabControl.find(".nav-tabs .tab.active a");
                if (tab_a.length > 0) {
                  return  _doms().tabControl.find(".tab-pane" + tab_a.attr('href'));
                }
                return null;
            },
            HomeTabPane: function () {
                return _doms().tabControl.find('.tab-pane#home');
            },
            allNewTabPanes: function () {
                var tab_as = _doms().tabControl.find(".nav-tabs .tab.new-tab a");
                var arr = [];
                $.each(tab_as, function (index, value) {
                    arr.push(_doms().tabControl.find(".tab-pane" + $(value).attr('href')));
                });
                return arr;
            },
            getAddTab: function () {
                var tab_as = _doms().tabControl.find(".nav-tabs .tab.add-tab a");
                return tab_as;
            },
            getAllNewTab: function () {
                var tab_as = _doms().tabControl.find(".nav-tabs .tab.new-tab a");
                
                return tab_as;
            },
            getHomeTab: function () {
                var tab_as = _doms().tabControl.find(".nav-tabs .tab.home-tab a");
                return tab_as;
            },
            isValidNewTabPane: function (tabpane) {
                if (tabpane.find("#frm-valuation-details").length > 0) {
                    return true;
                } return false;
            },
            getTabScope: function (element) {
                return element.closest('.tab-pane');
            },
        };
    }
    var formLoader = new function () {
        this.Load = function (payload, callBack) {
            $.ajax({
                type: 'post',
                url: "/Municipality/Assessment/SearchHoldingOldData",
                data: JSON.stringify(payload),
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (response) {
                    callBack(response);
                    onSuccess(payload);
                    $.ajax({
                        type: 'post',
                        url: "/Municipality/Assessment/GetOtherTabDetails",
                        data: JSON.stringify(payload),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.Data && response.Data._OtherTabDetails != null) {
                                if (response.Data._OtherTabDetails.ApplictnDt != null) {
                                    $('.txt-application-date').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.ApplictnDt).format('dd/mm/yyyy'));
                                }
                                
                                if (response.Data._OtherTabDetails.DtOfOrder != null) {
                                    $('.txt-date-of-order').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.DtOfOrder).format('dd/mm/yyyy'));
                                }
                                $('.txt-application-no').val(response.Data._OtherTabDetails.ApplictnCaseNo);
                                $('.txt-primary-phone-no').val(response.Data._OtherTabDetails.PrimaryPhNo);
                                $('.txt-primary-email-id').val(response.Data._OtherTabDetails.PrimaryEmail);
                                $('.txt-pro-cosing-fees').val(response.Data._OtherTabDetails.ProCosingFees);
                                $('.txt-other-fees').val(response.Data._OtherTabDetails.OtherFees);
                            }
                        }
                    });
                },
                failure: function (response) {
                    alert(response);
                },
                error: function (response) {
                    alert(response);
                }
            });
            function onSuccess(payload) {
                //alert(payload);
                wardId = parseInt(payload.wardId);
                LocationId = parseInt(payload.LocationId);
                $.ajax({
                    type: 'POST',
                    url: "/Municipality/Assessment/GetValuationDetailsJsonData",
                    data: JSON.stringify({ ValTag: payload.ValTag, wardId: wardId, LocationId: LocationId, HoldingNo: payload.HoldingNo, IsTemp: false }),
                    //data: JSON.stringify(payload),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.Data != null) {
                            for (i = 0; i < result.Data.length; i++) {
                                var tempJson = result.Data[i];

                                var tr = $('<tr/>');
                                //floor control
                                var td = $('<td class="annual-val" />');
                                tr.append(td.append(tempJson.AnnualValuation));

                                //add usage control
                                td = $('<td class="qtr-ptax" />');
                                tr.append(td.append(tempJson.Qtr_Ptax));

                                //add Year control
                                td = $('<td class="qtr-schrg" />');
                                tr.append(td.append(tempJson.Qtr_Schrg));

                                //add covered area control
                                td = $('<td class="land-cost" />');
                                tr.append(td.append(tempJson.LandCost));

                                //add Occupent Name control
                                td = $('<td class="qtr-Edu-cess" />');
                                tr.append(td.append(tempJson.EduCessAmt));

                                td = $('<td class="hiddenAssmtID" hidden>' + tempJson.AssmtId + '</td>');
                                tr.append(td);

                                td = $('<td class="hiddenTabID" hidden>' + tempJson.AnulValDtlTabId + '</td>');
                                tr.append(td);

                                td = $('<td class="editMode" hidden></td>');
                                tr.append(td);

                                td = $('<td/>');
                                tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));
                                tempJson = JSON.stringify(tempJson);
                                td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
                                tr.append(td);

                                $('#valuation_details_table-1').find('>tbody').append(tr);
                                totalAnnualVal($('#valuation_details_table-1'));
                                totalEducess($('#valuation_details_table-1'));
                                totalQtrPtax($('#valuation_details_table-1'));
                                totalQtrScharge($('#valuation_details_table-1'));
                                totalLandCost($('#valuation_details_table-1'));
                                $('.valdtlmodal').modal('hide');
                                $('#valuationAddBtn').show();
                                $('#valuationSaveBtn').hide();
                            }
                        }
                    },
                });
            }
        };
    };

    var _doms = function (scope) {
        if (!scope) {
            scope = $(document);
        }
        return {
            cntrDownloadedForm: scope.find(".downloaded-form"),
            tabControl: scope.find(".separation-tab-control"),
            partialSaveCancel:scope.find(".partial-save-cancel"),
            searchHolding: {
                frm: scope.find("#frm-holding-details"),
                txtWard: scope.find(".txtWard"),
                hdnWard: scope.find(".hddn-Ward"),
                txtLocation: scope.find(".txtLocation"),
                hdnLocation: scope.find(".hddn-Location"),
                txtHoldingNo: scope.find(".txt-holding-no"),
                hddnHoldingNo: scope.find(".hddn-holding-no"),
            },
            partialHoldingDtl: {
                frm: scope.find(".frm-partial-holding-details"),
                //ddlHoldingType: scope.find(".ddl-holding-type"),
                ddlHoldingTypeGroup: scope.find(".ddl-hodling-type-group"),
                listHoldingNames: scope.find(".list-holding-names"),
                txtAddress: scope.find(".txt-address"),
                txtOwnerName: scope.find(".txt-assessee-name")
            },
            holdingDtl:  {
                frm: scope.find("#frm-holding-details"),
                txtWard: scope.find(".txtWard"),
                hdnWard: scope.find(".hddn-Ward"),
                txtLocation: scope.find(".txtLocation"),
                hdnLocation: scope.find(".hddn-Location"),
                txtHoldingNo: scope.find(".txt-holding-no"),
                hddnHoldingNo: scope.find(".hddn-holding-no"),
               // ddlHoldingType: scope.find(".ddl-holding-type"),
                ddlHoldingTypeGroup: scope.find(".ddl-hodling-type-group"),
                listHoldingNames: scope.find(".list-holding-names"),
                txtAddress: scope.find(".txt-address"),
                txtOwnerName: scope.find(".txt-assessee-name")
            },
            valuationDtl: {
                frm: scope.find("#frm-valuation-details"),
                ddlHoldingType: scope.find(".ddl-holding-type"),
                ddlZone: scope.find(".ddl-zone"),
                txtLandAreaKt: scope.find(".txt-land-area-kt"),
                txtLandAreaCh: scope.find(".txt-land-area-ch"),
                txtLandAreaSft: scope.find(".txt-land-area-sft"),
                ddlNatureOfUser: scope.find(".ddl-nature-of-use"),
                txtBldgAreaSft: scope.find(".txt-bldg-area-sft"),
                txtPlinthSft: scope.find(".txt-plinth-sft"),
                ddlConstruction: scope.find(".ddl-construction"),
                txtLandCost: scope.find(".txt-land-cost"),
                txtAge: scope.find(".txt-age"),
                txtReadOnlyAnnualValuation: scope.find(".txt-readonly-annual-valuation"),
                txtReadOnlyQtrPtax: scope.find(".txt-readonly-qtr-ptax"),
                txtReadOnlyQtrSchrg: scope.find(".txt-readonly-qtr-schrg"),
                ddlHoldingTypeGroup: scope.find(".ddl-hodling-type-group"),
                SrchgTag: scope.find(".chk-SrchgTag"),
                EduCessTag: scope.find(".chk-EduCessTag"),
                txtReadonlyQtrEduCess: scope.find(".txt-readonly-qtr-Edu-cess"),
                valuationDtlJsonString: function () {
                    var rowDataJsonString = null;
                    var rowDataJson = null;
                    var tbl = scope.find(".completeJson");
                    var _ValuationDetailsArr = [];
                    $.each(tbl, function (i, v) {
                        rowDataJsonString = v.innerText;
                        rowDataJson = JSON.parse(rowDataJsonString);
                        _ValuationDetailsArr.push(rowDataJson);
                    });
                    return _ValuationDetailsArr;
                }
            },
            additionalDtl: {
                frm: scope.find("#frm-sec-holding-details"),
                txtMouajaName: scope.find(".txt-mauja-name"),
                txtKhatianNoRs: scope.find(".txt-khatian-no-rs"),
                txtKhatianNoLr: scope.find(".txt-khatian-no-lr"),
                txtDagNoRs: scope.find(".txt-dag-no-rs"),
                txtDagNoLr: scope.find(".txt-dag-no-lr"),
                ddlHoldingArea: scope.find(".ddl-holding-area"),
                txtDeedNo: scope.find(".txt-deed-no"),
                ddlDeedType: scope.find(".ddl-deed-type"),
                txtDeedDate: scope.find(".txt-deed-date"),
                chkLiftTag: scope.find(".lst-tags .lift"),
                chkDrainage: scope.find(".lst-tags .drainage"),
                chkToilets: scope.find(".lst-tags .toilets"),
                chkElecticity: scope.find(".lst-tags .electricity"),
                chkWater: scope.find(".lst-tags .Water"),
                txtBoroughNo: scope.find(".txt-borough-no")
            },
            otherDtl: {
                frm: scope.find("#frm-other-details"),
                txtBuildingDtl: $(".txt-building-dtl")
            },
            otherTabDtl: {
                frm: $("#frm-new-holding-OtherTabdetails"),
                txtPhoneNo: $(".txt-primary-phone-no"),
                txtEmailId: $(".txt-primary-email-id"),
                txtPorCosingFees: $(".txt-pro-cosing-fees"),
                txtOtherFees: $(".txt-other-fees"),
                ApplictnDt: $(".txt-application-date"),
                ApplictnCaseNo: $(".txt-application-no"),
                DtOfOrder: $(".txt-date-of-order"),
            }
        }
    };
    var formDatas = function () {
        return {
            searchedHoldingData: function (scope) {
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                }
            },
            partialHoldingData: function (scope) {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    //HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                }
            },
            holdingDetails: function (scope) {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().partialHoldingDtl.listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().partialHoldingDtl.txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    WardId: _doms().searchHolding.hdnWard.val(),
                    WardName: _doms().searchHolding.txtWard.val(),
                    LocationId: _doms().searchHolding.hdnLocation.val(),
                    LocationName: _doms().searchHolding.txtLocation.val(),
                    HoldingNo: _doms().searchHolding.hddnHoldingNo.val(),
                   // HoldingTypeId: _doms().partialHoldingDtl.ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().partialHoldingDtl.ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().partialHoldingDtl.txtAddress.val(),
                };
            },
            valuationdata: function (scope) {
                return {
                    HoldingTypeId: _doms(scope).valuationDtl.ddlHoldingType.val(),
                    ZoneId: _doms(scope).valuationDtl.ddlZone.val(),
                    NatureOfUseId: _doms(scope).valuationDtl.ddlNatureOfUser.val(),
                    ConstructionId: _doms(scope).valuationDtl.ddlConstruction.val(),
                    LandArea_KT: _doms(scope).valuationDtl.txtLandAreaKt.val(),
                    LandArea_CH: _doms(scope).valuationDtl.txtLandAreaCh.val(),
                    LandArea_SFT: _doms(scope).valuationDtl.txtLandAreaSft.val(),
                    BldgArea_SFT: _doms(scope).valuationDtl.txtBldgAreaSft.val(),
                    Plinth_SFT: _doms(scope).valuationDtl.txtPlinthSft.val(),
                    LandCost: _doms(scope).valuationDtl.txtLandCost.val(),
                    Age: _doms(scope).valuationDtl.txtAge.val(),
                    HoldingTypeGroupId: _doms(scope).valuationDtl.ddlHoldingTypeGroup.val(),
                    SrchgTag: _doms(scope).valuationDtl.SrchgTag.prop('checked'),
                    EduCessTag: _doms(scope).valuationDtl.EduCessTag.prop('checked'),
                    Qtr_EduCess: _doms(scope).valuationDtl.txtReadonlyQtrEduCess.val(),
                    Qtr_Schrg: _doms(scope).valuationDtl.txtReadOnlyQtrSchrg.val()
                }
            },
            getValuationArrayData: function (scope) {
                var rowDataJsonString = null;
                var rowDataJson = null;
                var tbl = scope.find('.completeJson');
                var _ValuationDetailsArr = [];
                $.each(tbl, function (i, v) {
                    rowDataJsonString = v.innerText;
                    rowDataJson = JSON.parse(rowDataJsonString);
                    rowDataJson.HoldingTypeGroupId = _doms(scope).valuationDtl.ddlHoldingTypeGroup.val();
                    // rowDataJson.EditMode=
                    _ValuationDetailsArr.push(rowDataJson);
                });
                return _ValuationDetailsArr;
            },
            additionalHodlingData: function (scope) {
                return {
                    MaujaName: _doms(scope).additionalDtl.txtMouajaName.val(),
                    KhatianNo_RS: _doms(scope).additionalDtl.txtKhatianNoRs.val(),
                    KhatianNo_LR: _doms(scope).additionalDtl.txtKhatianNoLr.val(),
                    DaagNo_RS: _doms(scope).additionalDtl.txtDagNoRs.val(),
                    DaagNo_LR: _doms(scope).additionalDtl.txtDagNoLr.val(),
                    DeedNo: _doms(scope).additionalDtl.txtDeedNo.val(),
                    DeedDate: constants(scope).convertTodate('dd/mm/yyyy', _doms().additionalDtl.txtDeedDate.val()),
                    DeedTypeId: _doms(scope).additionalDtl.ddlDeedType.val(),
                    HoldingAreaCode: _doms(scope).additionalDtl.ddlHoldingArea.val(),
                    Tag_Lift: _doms(scope).additionalDtl.chkLiftTag.prop('checked'),
                    Tag_Drainage: _doms(scope).additionalDtl.chkDrainage.prop('checked'),
                    Tag_Toilets: _doms(scope).additionalDtl.chkToilets.prop('checked'),
                    Tag_Electricity: _doms(scope).additionalDtl.chkElecticity.prop('checked'),
                    Tag_Water: _doms(scope).additionalDtl.chkWater.prop('checked'),
                    BoroughNo: _doms(scope).additionalDtl.txtBoroughNo.val(),
                }
            },
            otherDtl: function (scope) {
                return {
                    BuildingDtl: _doms(scope).otherDtl.txtBuildingDtl.val()
                }
            },
            otherTabDtl: function () {
                return {
                    PrimaryPhNo: _doms().otherTabDtl.txtPhoneNo.val(),
                    PrimaryEmail: _doms().otherTabDtl.txtEmailId.val(),
                    ProCosingFees: (isNaN(_doms().otherTabDtl.txtPorCosingFees.val()) ? 0 : _doms().otherTabDtl.txtPorCosingFees.val()),
                    OtherFees: (isNaN(_doms().otherTabDtl.txtOtherFees.val()) ? 0 : _doms().otherTabDtl.txtOtherFees.val()),
                    ApplictnDt: _doms().otherTabDtl.ApplictnDt.val(),
                    ApplictnCaseNo: _doms().otherTabDtl.ApplictnCaseNo.val(),
                    DtOfOrder: _doms().otherTabDtl.DtOfOrder.val()
                }
            }
        }
    };
    var digestedFormdata = function () {
        var effectiveFinYear = constants().effectiveFinYearQtr().ddlEffectiveFinYear.val();
        var effectiveQtr = constants().effectiveFinYearQtr().ddlEffectiveQtr.val();
        return {
            existingHoldingData: function () {
                var existingData = constants().HomeTabPane().find(".assmt-exisiting-holding-data");
                if (existingData.length > 0 && existingData.val()) {
                    existingData = JSON.parse(existingData.val());
                    if (existingData) {
                        existingData.ValYear = effectiveFinYear;
                        existingData.ValQtr = effectiveQtr;
                        existingData.ApprovedFlag = 'N';
                        existingData.ApprovedOn = null;
                        existingData.AssesseeName = $(".separation-tab-control .tab-pane#home .list-holding-names >li").map(function (indx, val) { return $(val).attr('data-name') }).get().join(',');
                        existingData["_ValuationDetails"] = formDatas().getValuationArrayData($(".separation-tab-control .tab-pane#home"));
                        return existingData;
                    }
                }
            },
            newSeparatedHoldings: function () {
                var datas= constants().allNewTabPanes().map(function (item,indx) {
                    return {
                        "TabId": 0,
                        "YearId": constants().getYearId(),
                        "Idno": null,
                        "Idnosrl": null,
                        "WardId": _doms(constants().HomeTabPane()).searchHolding.hdnWard.val(),
                        "LocationID": _doms(constants().HomeTabPane()).searchHolding.hdnLocation.val(),
                        "HoldingNo": _doms($(item)).searchHolding.hddnHoldingNo.val(),
                        "AssesseeNo": null,
                        "AssesseeName": _doms($(item)).holdingDtl.listHoldingNames.find('li').map(function (indx, val) { return $(val).attr('data-name') }).get().join(','),
                        "AssesseeAddress": _doms($(item)).holdingDtl.txtAddress.val(),
                        "_ValuationDetails": formDatas().getValuationArrayData($(item)),
                        //"ApplNo": null,
                        //"ApplDt": null,
                        //"ProcessingFees": isNaN( _doms($(item)).otherDtl.txtPorCosingFees.val())?0:Number(_doms($(item)).otherDtl.txtPorCosingFees.val()),
                        //"OtherFees": isNaN( _doms($(item)).otherDtl.txtOtherFees.val())?0:Number(_doms($(item)).otherDtl.txtOtherFees.val()),
                        //"PrimaryPhNo": _doms($(item)).otherDtl.txtPhoneNo.val(),
                        //"PrimaryEmail": _doms($(item)).otherDtl.txtEmailId.val(),

                        "ApplNo": _doms().otherTabDtl.ApplictnCaseNo.val(),
                        "ApplDt": _doms().otherTabDtl.ApplictnDt.val(),
                        "DtOfOrder": _doms().otherTabDtl.DtOfOrder.val(),
                        "ProcessingFees": isNaN( _doms().otherTabDtl.txtPorCosingFees.val())?0:Number(_doms().otherTabDtl.txtPorCosingFees.val()),
                        "OtherFees": isNaN(_doms().otherTabDtl.txtOtherFees.val()) ? 0 : Number(_doms().otherTabDtl.txtOtherFees.val()),
                        "PrimaryPhNo": _doms().otherTabDtl.txtPhoneNo.val(),
                        "PrimaryEmail": _doms().otherTabDtl.txtEmailId.val(),
                        "BoroughNo": _doms($(item)).additionalDtl.txtBoroughNo.val(),
                        "NameCAC": null,
                        "HoldingTypeGrp": _doms($(item)).holdingDtl.ddlHoldingTypeGroup.val(),
                        //"HoldingTypeGrpId": _doms($(item)).holdingDtl.ddlHoldingType.val(),
                        "HoldArea": _doms($(item)).additionalDtl.ddlHoldingArea.val(),
                        "ScoreZoneID": _doms($(item)).valuationDtl.ddlZone.val(),
                        "ScoreUseID": _doms($(item)).valuationDtl.ddlNatureOfUser.val(),
                        "ScoreCostID": _doms($(item)).valuationDtl.ddlConstruction.val(),
                        "TotWt": null,
                        "HoldingAge": isNaN(_doms($(item)).valuationDtl.txtAge.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtAge.val()),
                        "LandType": null,
                        "VacLandAreaDc": null,
                        "VacLandAreaSft": null,
                        "LandAreaDc": null,
                        "LandAreaKt": isNaN(_doms($(item)).valuationDtl.txtLandAreaKt.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtLandAreaKt.val()),
                        "LandAreaCh": isNaN(_doms($(item)).valuationDtl.txtLandAreaCh.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtLandAreaCh.val()),
                        "LandAreaSft": isNaN(_doms($(item)).valuationDtl.txtLandAreaSft.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtLandAreaSft.val()),
                        "CoveredArea": isNaN(_doms($(item)).valuationDtl.txtBldgAreaSft.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtBldgAreaSft.val()),
                        "PlinthArea": isNaN(_doms($(item)).valuationDtl.txtPlinthSft.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtPlinthSft.val()),
                        "LandCost": 0,
                        "AnnualVal": isNaN(_doms($(item)).valuationDtl.txtReadOnlyAnnualValuation.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtReadOnlyAnnualValuation.val()),
                        "DiscPer": 0,
                        "DiscAmt": 0,
                        "RevAnnualVal": 0,
                        "PtaxPer": 0,
                        "YrProptax": 0,
                        "QtrProptax": isNaN(_doms($(item)).valuationDtl.txtReadOnlyQtrPtax.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtReadOnlyQtrPtax.val()),
                        "PrvQtrProptax": null,
                        "SrchgTag": (_doms($(item)).valuationDtl.SrchgTag.prop('checked')==true?"Y":"N"),
                        "QtrSrchg": isNaN(_doms($(item)).valuationDtl.txtReadOnlyQtrSchrg.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtReadOnlyQtrSchrg.val()),
                        "EduTag": (_doms($(item)).valuationDtl.EduCessTag.prop('checked') == true ? "Y" : "N"),
                        "QtrEduCess": (isNaN(_doms($(item)).valuationDtl.txtReadonlyQtrEduCess.val()) ? 0 : Number(_doms($(item)).valuationDtl.txtReadonlyQtrEduCess.val())),
                        "MouzaName": _doms($(item)).additionalDtl.txtMouajaName.val(),
                        "JlNo": null,
                        "PSName": null,
                        "KhatianNoRS": _doms($(item)).additionalDtl.txtKhatianNoRs.val(),
                        "KhatianNoLR": _doms($(item)).additionalDtl.txtKhatianNoLr.val(),
                        "DagNoRS": _doms($(item)).additionalDtl.txtDagNoRs.val(),
                        "DagNoLR": _doms($(item)).additionalDtl.txtDagNoLr.val(),
                        "LandNatureROR": null,
                        "DeedNo": _doms($(item)).additionalDtl.txtDeedNo.val(),
                        "DeedDt": _doms($(item)).additionalDtl.ddlDeedType.val()?helpers().convertTodate("dd/mm/yyyy",_doms($(item)).additionalDtl.txtDeedDate.val()):null,
                        "DeedTypeId": _doms($(item)).additionalDtl.ddlDeedType.val(),
                        "WhetherAssessed": null,
                        "OldYearId": null,
                        "OldAnnualValue": null,
                        "OldPTax": null,
                        "WhetherOwnChange": null,
                        "OldAssesseeName": null,
                        "WhetherHoldingChange": null,
                        "OldHoldingType": null,
                        "AADRHolding": null,
                        "ApptTotAreaSft": null,
                        "OthAreaSft": null,
                        "BuildingDtl": _doms($(item)).otherDtl.txtBuildingDtl.val(),
                        "LiftTag": _doms($(item)).additionalDtl.chkLiftTag.prop('checked')?'Y':'N',
                        "DranageTag": _doms($(item)).additionalDtl.chkDrainage.prop('checked')?'Y':'N',
                        "ToiletsTag": _doms($(item)).additionalDtl.chkToilets.prop('checked')?'Y':'N',
                        "ElectricityTag": _doms($(item)).additionalDtl.chkElecticity.prop('checked')?'Y':'N',
                        "WaterTag": _doms($(item)).additionalDtl.chkWater.prop('checked')?'Y':'N',
                        "FerruleSize": null,
                        "ValTag": constants().getValTag(),
                        "ValYear": constants().effectiveFinYearQtr().ddlEffectiveFinYear.val(),
                        "ValQtr": constants().effectiveFinYearQtr().ddlEffectiveQtr.val(),
                        "ValSrl": null,
                        "ValSubSrl": null,
                        "ValFromTo": "T",
                        "FromWardNo": null,
                        "FromStreetCode":null,
                        "FromHoldingNo": null,
                        "OldWardNo": null,
                        "OldStreetCode": null,
                        "OldHoldingNo": null,
                        "Mfactor": null,
                        "MacId": null,
                        "ApprovedFlag": "N",
                        "ApprovedOn": null,
                        "ApprovedBy": null,
                        "ActiveFlag": "Y",
                        "RefTabId": null,
                        "MunicipalityID": null,
                        "InsertedBy":null,
                        "InsertedOn": null,
                        "UpdatedBy": null,
                        "UpdatedOn": null,
                        "LocationName": null
                    };
                });
                return datas;
            }
        };
    }
    var validators = {
        validateHoldingSearch: function (scope) {

            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms(scope).searchHolding.hdnWard.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).searchHolding.hdnLocation.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).searchHolding.hddnHoldingNo.prop('name')] = { required: true };

            //set error messages
            vldObj.messages[_doms(scope).searchHolding.hdnWard.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms(scope).searchHolding.hdnLocation.prop('name')] = { required: "Please enter Location." };
            vldObj.messages[_doms(scope).searchHolding.hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms(scope).searchHolding.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).searchHolding.frm.valid();
        },
        validatePartialHolding: function (scope) {
            $.validator.addMethod(
            "OwnerList",
            function (value, element, params) {
                // otherwise, use your rule
                if (scope.find(".txt-assessee-name").parent().siblings('ul').find('li').length > 0) {
                    return true;
                }
                return false;
            },
            "Please add owner list"
            );
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //vldObj.rules[_doms(scope).partialHoldingDtl.ddlHoldingType.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).partialHoldingDtl.txtAddress.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: true };

           // vldObj.messages[_doms(scope).partialHoldingDtl.ddlHoldingType.prop('name')] = { required: "Please select holding type" };
            vldObj.messages[_doms(scope).partialHoldingDtl.ddlHoldingTypeGroup.prop('name')] = { required: "Please holding type group" };
            vldObj.messages[_doms(scope).partialHoldingDtl.txtAddress.prop('name')] = { required: "Please enter address" };
            vldObj.messages[_doms(scope).partialHoldingDtl.txtOwnerName.prop('name')] = { OwnerList: "Please add owner list" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms(scope).partialHoldingDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).partialHoldingDtl.frm.valid();
        },
        validateHoldingDetails: function (scope) {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            $.validator.addMethod(
            "OwnerList",
            function (value, element, params) {
                // otherwise, use your rule
                if (scope.find(".txt-assessee-name").parent().siblings('ul').find('li').length > 0) {
                    return true;
                }
                return false;
            },
            "Please add owner list"
            );
            //enable hidden field vallidation
            vldObj.ignore = [];
            //set rules
            vldObj.rules[_doms(scope).holdingDtl.hdnWard.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).holdingDtl.hdnLocation.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).holdingDtl.hddnHoldingNo.prop('name')] = { required: true };

            //set error messages
            vldObj.messages[_doms(scope).holdingDtl.hdnWard.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms(scope).holdingDtl.hdnLocation.prop('name')] = { required: "Please enter Location." };
            vldObj.messages[_doms(scope).holdingDtl.hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };


           // vldObj.rules[_doms(scope).holdingDtl.ddlHoldingType.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).holdingDtl.ddlHoldingTypeGroup.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).holdingDtl.txtAddress.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).holdingDtl.txtOwnerName.prop('name')] = { OwnerList: true };

           // vldObj.messages[_doms(scope).holdingDtl.ddlHoldingType.prop('name')] = { required: "Please select holding type" };
            vldObj.messages[_doms(scope).holdingDtl.ddlHoldingTypeGroup.prop('name')] = { required: "Please holding type group" };
            vldObj.messages[_doms(scope).holdingDtl.txtAddress.prop('name')] = { required: "Please enter address" };
            vldObj.messages[_doms(scope).holdingDtl.txtOwnerName.prop('name')] = { OwnerList: "Please add owner list" };
            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }
            _doms(scope).holdingDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).holdingDtl.frm.valid();
        },
        validateValuation: function (scope) {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms(scope).valuationDtl.ddlZone.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).valuationDtl.txtLandAreaKt.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms(scope).valuationDtl.txtLandAreaCh.prop('name')] = { required: true, number: true, range: [0, 15] };
            vldObj.rules[_doms(scope).valuationDtl.txtLandAreaSft.prop('name')] = { required: true, number: true, range: [0, 44] };
            vldObj.rules[_doms(scope).valuationDtl.ddlNatureOfUser.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).valuationDtl.txtBldgAreaSft.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms(scope).valuationDtl.txtPlinthSft.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms(scope).valuationDtl.ddlConstruction.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).valuationDtl.txtLandCost.prop('name')] = { required: true, number: true };
            vldObj.rules[_doms(scope).valuationDtl.txtAge.prop('name')] = { required: true, number: true };


            //set error messages
            vldObj.messages[_doms(scope).valuationDtl.ddlZone.prop('name')] = { required: "Please enter Ward." };
            vldObj.messages[_doms(scope).valuationDtl.txtLandAreaKt.prop('name')] = { required: "Please enter land area kt.", number: "Data should be numeric" };
            vldObj.messages[_doms(scope).valuationDtl.txtLandAreaCh.prop('name')] = { required: "Please enter land area ch.", number: "Data should be numeric", range: "Value sould be between 0 to 15" };
            vldObj.messages[_doms(scope).valuationDtl.txtLandAreaSft.prop('name')] = { required: "Please enter land area sft.", number: "Data should be numeric", range: "Value sould be between 0 to 44" };
            vldObj.messages[_doms(scope).valuationDtl.ddlNatureOfUser.prop('name')] = { required: "Please select nature of use" };
            vldObj.messages[_doms(scope).valuationDtl.txtBldgAreaSft.prop('name')] = { required: "Please enter bldg. area sft", number: "Data should be numeric" };
            vldObj.messages[_doms(scope).valuationDtl.txtPlinthSft.prop('name')] = { required: "Please enter plinth sft", number: "Data should be numeric" };
            vldObj.messages[_doms(scope).valuationDtl.ddlConstruction.prop('name')] = { required: "Please select construction" };
            vldObj.messages[_doms(scope).valuationDtl.txtLandCost.prop('name')] = { required: "Please enter land cost", number: "Data should be numeric" };
            vldObj.messages[_doms(scope).valuationDtl.txtAge.prop('name')] = { required: "Please enter age", number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms(scope).valuationDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).valuationDtl.frm.valid();
        },
        validateAdditional: function (scope) {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms(scope).additionalDtl.txtMouajaName.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtKhatianNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtKhatianNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtDagNoRs.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtDagNoLr.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.ddlHoldingArea.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtDeedNo.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.ddlDeedType.prop('name')] = { required: true };
            vldObj.rules[_doms(scope).additionalDtl.txtDeedDate.prop('name')] = { required: true };


            //set error messages
            vldObj.messages[_doms(scope).additionalDtl.txtMouajaName.prop('name')] = { required: "Please enter Mouja Name." };
            vldObj.messages[_doms(scope).additionalDtl.txtKhatianNoRs.prop('name')] = { required: "Please enter Khatian No RS" };
            vldObj.messages[_doms(scope).additionalDtl.txtKhatianNoLr.prop('name')] = { required: "Please enter Khatian No LR" };
            vldObj.messages[_doms(scope).additionalDtl.txtDagNoRs.prop('name')] = { required: "Please enter Dag No RS" };
            vldObj.messages[_doms(scope).additionalDtl.txtDagNoLr.prop('name')] = { required: "Please select Dag No LR" };
            vldObj.messages[_doms(scope).additionalDtl.ddlHoldingArea.prop('name')] = { required: "Please enter Holding Area" };
            vldObj.messages[_doms(scope).additionalDtl.txtDeedNo.prop('name')] = { required: "Please enter Deed no" };
            vldObj.messages[_doms(scope).additionalDtl.ddlDeedType.prop('name')] = { required: "Please select Deed Type" };
            vldObj.messages[_doms(scope).additionalDtl.txtDeedDate.prop('name')] = { required: "Please enter Deed Date" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms(scope).additionalDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).additionalDtl.frm.valid();
        },
        validateOtherDtl: function (scope) {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            ////set rules
            //vldObj.rules[_doms(scope).otherDtl.txtPorCosingFees.prop('name')] = { number: true };
            //vldObj.rules[_doms(scope).otherDtl.txtOtherFees.prop('name')] = { number: true };

            ////set error messages
            //vldObj.messages[_doms(scope).otherDtl.txtPorCosingFees.prop('name')] = { number: "Data should be numeric" };
            //vldObj.messages[_doms(scope).otherDtl.txtOtherFees.prop('name')] = { number: "Data should be numeric" };

            ////place errors in position
            //vldObj.errorPlacement = function (error, element) {
            //    if ($(element).parent(".input-group").length > 0) {
            //        error.insertAfter($(element).parent(".input-group"));
            //        error.css('color', 'red');
            //    } else {
            //        error.insertAfter(element);
            //        error.css('color', 'red');
            //    }

            //}

            //push validator object to tergate form
            _doms(scope).otherDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms(scope).otherDtl.frm.valid();
        },
        validateOtherTabDtl: function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorPlacement: {}
            };
            //enable hidden field vallidation
            vldObj.ignore = [];

            //set rules
            vldObj.rules[_doms().otherTabDtl.txtPorCosingFees.prop('name')] = { number: true };
            vldObj.rules[_doms().otherTabDtl.txtOtherFees.prop('name')] = { number: true };

            //set error messages
            vldObj.messages[_doms().otherTabDtl.txtPorCosingFees.prop('name')] = { number: "Data should be numeric" };
            vldObj.messages[_doms().otherTabDtl.txtOtherFees.prop('name')] = { number: "Data should be numeric" };

            //place errors in position
            vldObj.errorPlacement = function (error, element) {
                if ($(element).parent(".input-group").length > 0) {
                    error.insertAfter($(element).parent(".input-group"));
                    error.css('color', 'red');
                } else {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

            }

            //push validator object to tergate form
            _doms().otherTabDtl.frm.validate(vldObj);
            //validate form and return true and false
            return _doms().otherTabDtl.frm.valid();
        }
    };
    var evtHandlers = {
        onChangeValuation: function (ctrl) {
            var tabPane = constants().getTabScope(ctrl);
            if (validators.validateHoldingDetails(tabPane) && validators.validateValuation(tabPane)) {
                var valuationData = formDatas().valuationdata(tabPane);
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/CalCulateValuationDetails",
                    data: JSON.stringify(valuationData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        if (r.Status === 200) {
                            var data = r.Data;
                            _doms(tabPane).valuationDtl.txtReadOnlyAnnualValuation.val(data.AnnualValuation);
                            _doms(tabPane).valuationDtl.txtReadOnlyQtrPtax.val(data.Qtr_Ptax);
                            _doms(tabPane).valuationDtl.txtReadOnlyQtrSchrg.val(data.Qtr_Schrg);
                            _doms(tabPane).valuationDtl.txtLandCost.val(data.LandCost);
                            _doms(tabPane).valuationDtl.txtReadonlyQtrEduCess.val(data.EduCessAmt);
                        } else {
                            alertify.error(r.Message);
                        }
                    },
                    failure: function (response) {
                        alertify.error(response);
                    }
                });
            }
        }
    };
    var helpers = function () {
        return {
            enableDisableSaveCancel: function () {
                var isValid = true;
                var items = constants().allNewTabPanes();
                if (items.length > 0) {
                    $.each(items, function (index, value) {
                        if (!constants().isValidNewTabPane($(value))) {
                            isValid = false;
                        }
                    });
                } else {
                    isValid = false;
                }
                if (isValid) {
                    _doms().partialSaveCancel.find(".btn-save").prop('disabled', false);
                    _doms().partialSaveCancel.find(".btn-cancel").prop('disabled', false);
                } else {
                    _doms().partialSaveCancel.find(".btn-save").prop('disabled', true);
                    _doms().partialSaveCancel.find(".btn-cancel").prop('disabled', true);
                }
            },
            convertTodate: function (format, date) {
                switch (format) {
                    case 'dd/mm/yyyy':
                    case 'dd/mm/yy':
                        {
                            var array = date.split('/');
                            return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                            break;
                        }
                }
            },
            validateTab: function (tab) {
                var tabPill = tab.closest(".separation-tab-control").find(".tab [href='#" + tab.attr("id") + "']").parent();
                tabPill.find('a').removeClass('bg-red');//reset tab color
                if (tab.hasClass('tab-pane')) {
                    if (tab.attr('id') == 'home') {
                        var flgHoldingSearch = validators.validateHoldingSearch(tab);
                        var flgHoldingPartial = validators.validatePartialHolding(tab);
                        var flgValuation = validators.validateValuation(tab);
                        if (flgHoldingSearch === true && flgHoldingPartial === true && flgValuation === true) {
                            return true;
                        } else {
                            tabPill.find('a').addClass('bg-red');//change tab color to red
                            tabPill.find('a').tab('show');//focus tab
                            return false;
                        }
                    }
                        //detect new tab, must be new-tab
                    else if (tab.closest(".separation-tab-control").find(".tab [href='#" + tab.attr("id") + "']").length > 0 && tab.closest(".separation-tab-control").find(".tab [href='#" + tab.attr("id") + "']").parent('li').hasClass('new-tab'))
                    {
                        if(constants().isValidNewTabPane(tab))
                        {
                            var flgHolding = validators.validateHoldingDetails(tab);
                            var flgValuation = validators.validateValuation(tab);
                            if (flgHolding === true && flgValuation === true) {
                                return true;
                            } else {
                                tabPill.find('a').addClass('bg-red');//change tab color to red
                                tabPill.find('a').tab('show');//focus tab
                                throw 'Validation failed';
                            }
                        }
                    } else {
                        tabPill.find('a').addClass('bg-red');//change tab color to red
                        tabPill.find('a').tab('show');//focus tab
                        throw 'Validation failed';
                    }
                } else {
                    throw 'Invalid Tab found';
                }
            },
            reArrangeValuationModals: function () {
                $('.separation-tab-control>.tab-content>.tab-pane').each(function (i, v) {
                    var pane = $(v);
                    var curModal = pane.find('.valdtlmodal');
                    if (curModal.length > 0) {
                        var newId = "valuation-details-" + (i + 1);
                        pane.find(".sec-holding-details .valuation_details_table>thead>tr .add-row").attr('data-target', '#' + newId).attr('href', '#' + newId);
                        curModal.attr('id', newId);
                    }
                });
                $('.valuation_details_table').each(function (i, v) {
                    var table = $(v);
                    var newId = "valuation_details_table-" + (i + 1);
                    table.attr('id', newId);
                });
            }
        }
    }
    var bindEvents = function () {
        $(document).on("click", ".valdtlmodal #valuationAddBtn", function () {
            var modal = $(this).closest('.valdtlmodal');
            var popupId = modal[0].id.substring(modal[0].id.lastIndexOf("-"));
            var tempJson = {
                "HoldingTypeId": modal.find('.ddl-holding-type').val(),
                "ZoneId": modal.find('.ddl-zone').val(),
                "ZoneVal": modal.find('.zoneText').text(),
                "LandArea_KT": modal.find('.txt-land-area-kt').val(),
                "LandArea_CH": modal.find('.txt-land-area-ch').val(),
                "LandArea_SFT": modal.find('.txt-land-area-sft').val(),
                "NatureOfUseId": modal.find('.ddl-nature-of-use').val(),
                "NatureOfUseVal": modal.find('.NOUVal').text(),
                "BldgArea_SFT": modal.find('.txt-bldg-area-sft').val(),
                "Plinth_SFT": modal.find('.txt-plinth-sft').val(),
                "Age": modal.find('.txt-age').val(),
                "ConstructionId": modal.find('.ddl-construction').val(),
                "ConstructionVal": modal.find('.constText').text(),
                "SrchgTag": modal.find('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": modal.find('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": modal.find('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": modal.find('.txt-readonly-qtr-schrg').val(),
                "LandCost": modal.find('.txt-land-cost').val(),
                "Qtr_EduCess": modal.find('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": modal.find(".ddl-hodling-type-group").val(),
                "AssmtId": null,
                "AnulValDtlTabId": null,
                "EditMode": "I"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append(modal.find('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append(modal.find('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td class="hiddenAssmtID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "I" + '</td>');
            tr.append(td);

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);
            var newtableId = '#valuation_details_table' + popupId;
            $(newtableId).find('>tbody').append(tr);
            totalAnnualVal($(newtableId));
            totalEducess($(newtableId));
            totalQtrPtax($(newtableId));
            totalQtrScharge($(newtableId));
            totalLandCost($(newtableId));
            modal.modal('hide');
            $("#valuation-details" + popupId + " #valuationAddBtn").show();
            $("#valuation-details" + popupId + " #valuationSaveBtn").hide();
        });
        $(document).on('click', ".updateRow", function () {
            var tbl = $(this).closest('.valuation_details_table');
            var popupId = tbl[0].id.substring(tbl[0].id.lastIndexOf("-"));
            var modal = $("#valuation-details" + popupId);
            var row_index = $(this).closest('tr').index() + 2;
            $('.hiddenFieldForUdtate').val(row_index);
            $("#valuation-details" + popupId + " #valuationAddBtn").hide();
            $("#valuation-details" + popupId + " #valuationSaveBtn").show();
            //totalAnnualVal($(this).closest('tr'));
            //totalEducess($(this).closest('tr'));
            //totalQtrPtax($(this).closest('tr'));
            //totalQtrScharge($(this).closest('tr'));
            //totalLandCost($(this).closest('tr'));
            var rowDataJsonString = $(this).closest('tr').find('.completeJson').text();
            var rowDataJson = JSON.parse(rowDataJsonString);
            modal.modal('show');
            modal.find('.ddl-holding-type').val(rowDataJson.HoldingTypeId),
            modal.find('.ddl-zone').val(rowDataJson.ZoneId),
                modal.find('.zoneText').text(rowDataJson.ZoneVal),
                modal.find('.txt-land-area-kt').val(rowDataJson.LandArea_KT),
                modal.find('.txt-land-area-ch').val(rowDataJson.LandArea_CH),
                modal.find('.txt-land-area-sft').val(rowDataJson.LandArea_SFT),
                modal.find('.ddl-nature-of-use').val(rowDataJson.NatureOfUseId),
                modal.find('.NOUVal').text(rowDataJson.NatureOfUseVal),
                modal.find('.txt-bldg-area-sft').val(rowDataJson.BldgArea_SFT),
                modal.find('.txt-plinth-sft').val(rowDataJson.Plinth_SFT),
                modal.find('.txt-age').val(rowDataJson.Age),
                modal.find('.ddl-construction').val(rowDataJson.ConstructionId),
                modal.find('.constText').text(rowDataJson.ConstructionVal),
                modal.find(".chk-SrchgTag").prop("checked", rowDataJson.SrchgTag);
            modal.find('.txt-readonly-annual-valuation').val(rowDataJson.AnnualValuation),
                modal.find('.txt-readonly-qtr-ptax').val(rowDataJson.Qtr_Ptax),
                modal.find('.txt-readonly-qtr-schrg').val(rowDataJson.Qtr_Schrg),
                modal.find('.txt-land-cost').val(rowDataJson.LandCost),
                modal.find('.txt-readonly-qtr-Edu-cess').val(rowDataJson.Qtr_EduCess),
                modal.find('.hidden_assmt_id').val(rowDataJson.AssmtId),
                modal.find('.hidden_tab_id').val(rowDataJson.AnulValDtlTabId)
        });
        $(document).on('click', '.rmvRow', function () {
            var tbl = $(this).closest('.valuation_details_table');
            var popupId = tbl[0].id.substring(tbl[0].id.lastIndexOf("-"));
            var modal = $("#valuation-details" + popupId);

            var row_index = $(this).closest('tr').index() + 2;

            var xy = $(this).closest('tr');
            var rowDataJsonString = xy.find('.completeJson').text();
            var rowDataJson = JSON.parse(rowDataJsonString);
            rowDataJson.EditMode = "D";
            rowDataJsonString = JSON.stringify(rowDataJson);
            xy.find('.completeJson').text(rowDataJsonString);
            $("#valuation_details_table" + popupId + " tr:eq(" + row_index + ")").remove();

            totalAnnualVal(tbl);
            totalEducess(tbl);
            totalQtrPtax(tbl);
            totalQtrScharge(tbl);
            totalLandCost(tbl);
            if (tbl.find('> tbody > tr:first').length == 0) {
                tbl.find('>tbody').append(xy);
                tbl.find('> tbody > tr:first').hide();
                tbl.find('> tbody > tr:first').find('.editMode').text("D");
            }
            else if (tbl.find('> tbody > tr').length >= 1 && row_index < 3) {
                xy.prependTo("#valuation_details_table" + popupId);
                tbl.find('> tbody > tr').eq(0).hide();
                tbl.find('> tbody > tr').eq(0).find('.editMode').text("D");
            }
            else {
                $("#valuation_details_table" + popupId + " tr:eq(" + (row_index - 1) + ")").after(xy);
                $("#valuation_details_table" + popupId + " tr:eq(" + row_index + ")").hide();
                $("#valuation_details_table" + popupId + " tr:eq(" + row_index + ")").find('.editMode').text("D");
            }
            // xy.hide();

        });
        $(document).on("click", ".valdtlmodal #valuationSaveBtn", function () {
            var modal = $(this).closest('.valdtlmodal');
            var popupId = modal[0].id.substring(modal[0].id.lastIndexOf("-"));
            var rowIndex = parseInt($('.hiddenFieldForUdtate').val());
            $("#valuation_details_table" + popupId + " tr:eq(" + rowIndex + ")").remove();
            var tempJson = {
                "HoldingTypeId": modal.find('.ddl-holding-type').val(),
                "ZoneId": modal.find('.ddl-zone').val(),
                "ZoneVal": modal.find('.zoneText').text(),
                "LandArea_KT": modal.find('.txt-land-area-kt').val(),
                "LandArea_CH": modal.find('.txt-land-area-ch').val(),
                "LandArea_SFT": modal.find('.txt-land-area-sft').val(),
                "NatureOfUseId": modal.find('.ddl-nature-of-use').val(),
                "NatureOfUseVal": modal.find('.NOUVal').text(),
                "BldgArea_SFT": modal.find('.txt-bldg-area-sft').val(),
                "Plinth_SFT": modal.find('.txt-plinth-sft').val(),
                "Age": modal.find('.txt-age').val(),
                "ConstructionId": modal.find('.ddl-construction').val(),
                "ConstructionVal": modal.find('.constText').text(),
                "SrchgTag": modal.find('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": modal.find('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": modal.find('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": modal.find('.txt-readonly-qtr-schrg').val(),
                "LandCost": modal.find('.txt-land-cost').val(),
                "Qtr_EduCess": modal.find('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": modal.find(".ddl-hodling-type-group").val(),
                "AssmtId": modal.find(".hidden_assmt_id").val(),
                "AnulValDtlTabId": modal.find(".hidden_tab_id").val(),
                "EditMode": "U"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append(modal.find('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append(modal.find('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append(modal.find('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="hiddenAssmtID" hidden>' + modal.find(".hidden_assmt_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + modal.find(".hidden_tab_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "U" + '</td>');
            tr.append(td);

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);
            if ($("#valuation_details_table" + popupId + " > tbody > tr:first").length == 0) {
                $("#valuation_details_table" + popupId).find('>tbody').append(tr);
                $("#valuation_details_table" + popupId + " > tbody > tr:first").find('.editMode').text("U");
            }
            else if ($("#valuation_details_table" + popupId + " > tbody > tr").length >= 1 && rowIndex < 3) {
                tr.prependTo("#valuation_details_table" + popupId + " > tbody");
                $("#valuation_details_table" + popupId + " > tbody > tr").eq(0).find('.editMode').text("U");
            }
            else {
                $("#valuation_details_table" + popupId + " tr:eq(" + (rowIndex - 1) + ")").after(tr);
                $("#valuation_details_table" + popupId + " tr:eq(" + rowIndex + ")").find('.editMode').text("U");
            }
            totalAnnualVal($("#valuation_details_table" + popupId));
            totalEducess($("#valuation_details_table" + popupId));
            totalQtrPtax($("#valuation_details_table" + popupId));
            totalQtrScharge($("#valuation_details_table" + popupId));
            totalLandCost($("#valuation_details_table" + popupId));
            modal.modal('hide');
            $("#valuation-details" + popupId + " #valuationAddBtn").show();
            $("#valuation-details" + popupId + " #valuationSaveBtn").hide();
        });
        $(document).on('hidden.bs.modal', '.valdtlmodal', function () {
            var modal = $(this).closest('.valdtlmodal');
            var popupId = modal[0].id.substring(modal[0].id.lastIndexOf("-"));
            modal.find('.ddl-construction,.ddl-nature-of-use,.ddl-zone').val('');
            modal.find('.ddl-holding-type').val(0);
            modal.find('.txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-age,.txt-readonly-annual-valuation,.txt-land-cost,.txt-readonly-qtr-ptax,.txt-readonly-qtr-Edu-cess,.txt-readonly-qtr-schrg').val('0');
            modal.find(".chk-SrchgTag").prop("checked", false);
            modal.find('.zoneText,.NOUVal,.constText').text('0');
            modal.find("#valuation-details" + popupId + " #valuationAddBtn").show();
            modal.find("#valuation-details" + popupId + " #valuationSaveBtn").hide();
        });

        $(document).on('click', ".btn-load-review-form", function (e) {
            e.preventDefault();

            _doms().cntrDownloadedForm.empty();
            if (!validators.validateHoldingSearch()) {
                return;
            }
            var frmData = formDatas().searchedHoldingData();
            var payload = { ValTag: constants().getValTag(), LocationId: frmData.LocationId, wardId: frmData.WardId, HoldingNo: frmData.HoldingNo, YearId: constants().getYearId() };
            formLoader.Load(payload, function (r) {
                _doms().cntrDownloadedForm.append(r);
                constants().getAddTab().parent().show();
            });
        });
        $(document).on('click', '.add-tab', function () {
            var getTabTemplate = function (tabNo) {
                var str = "<li class='tab new-tab'>"
                str += "<a href='#tab" + tabNo + "' role='tab' data-toggle='tab' aria-expanded='true'><span>new holding " + tabNo + "</span>  &nbsp;&nbsp;<button class='close' type='button' title='Remove this page'>×</button></a></li>";
                return str;
            };
            var getTabBody = function (tabNo, callback) {
                var tergateContr = constants().HomeTabPane();
                var payload = { ValTag: constants().getValTag(), wardId: _doms(tergateContr).searchHolding.hdnWard.val(), LocationId: _doms(tergateContr).searchHolding.hdnLocation.val(), HoldingNo: _doms(tergateContr).searchHolding.hddnHoldingNo.val() };
                $.ajax({
                    type: 'post',
                    url: "/Municipality/Assessment/GetSeparationNewForm",
                    data: JSON.stringify(payload),
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    success: function (response) {
                        var str = "<div class='tab-pane fade' id='tab" + tabNo + "'>" + response + "</tab>";
                        callback(str);
                    },
                    failure: function (response) {
                        alert(response);
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            };
            var maxtabCount = _doms().tabControl.attr('max-tab-count');
            if (!maxtabCount) {
                maxtabCount = 0;
            }
            maxtabCount = 1+Number(maxtabCount);
            _doms().tabControl.attr('max-tab-count', maxtabCount);

            var tab = getTabTemplate(maxtabCount);
            getTabBody(maxtabCount, function (r) {
                var rsp = $(r);
                if (rsp.find(".txtLocation").length > 0) {
                    rsp.find(".txtLocation").val(constants().HomeTabPane().find(".txtLocation").val());
                    rsp.find(".txtLocation").prop('disabled', true); 
                    rsp.find(".txtWard").prop('disabled', true);
                }
                _doms().tabControl.find(".nav-tabs")
                $(tab).insertBefore(_doms().tabControl.find('.nav-tabs').find('.add-tab'));
                _doms().tabControl.find('.tab-content').append(rsp);
                _doms().tabControl.find('.nav-tabs').find('.new-tab a[href="#tab' + maxtabCount + '"]').tab('show');
                helpers().enableDisableSaveCancel();
                helpers().reArrangeValuationModals();
            });
            
        });
        $(document).on("click", ".btn-add-assesse", function () {
            var tabPane = constants().getTabScope($(this));
            var assesseName = tabPane.find(".txt-assessee-name").val();
            if (assesseName.trim()) {
                tabPane.find(".txt-assessee-name").val('');
                tabPane.find(".list-holding-names").append("<li data-name='" + assesseName + "' class='list-group-item'>" + assesseName + "<a href='javascript:void(0);' class='pull-right remove-assesse '>X</a></li>");
            }
        });
        $(document).on('click', ".list-holding-names .remove-assesse", function () {
            $(this).closest('li').remove();
        });
        //remove tab
        _doms().tabControl.find('.nav-tabs').on('click', '.new-tab button', function () {
            var ctrlSender = $(this);
            alertify.confirm('Confirm us!', 'Sure to remove this tab?', function () {
                var parentAnchor = ctrlSender.closest('a[role="tab"]');
                var tabBodyId = parentAnchor.attr('href');
                _doms().tabControl.find('.tab-content ' + tabBodyId).remove();
                parentAnchor.remove();
                if (!constants().isValidNewTabPane(constants().HomeTabPane())) {
                    constants().getAddTab().parent().hide();
                }
                if (constants().allNewTabPanes().length == 0) {
                    constants().getHomeTab().tab('show');
                } else {
                    $(constants().getAllNewTab()[0]).tab('show');
                }
                helpers().enableDisableSaveCancel();

            }, function () {});
        });
        $(document).on('focus', '.txt-deed-date', function () {
            $(this).datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
        });
        //holding details
        $(document).on('focus', ".txtWard", function () {
            var tabPane = constants().getTabScope($(this));
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { Ward: request.term };
                    _doms(tabPane).searchHolding.hdnWard.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetWards",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WardID,
                                            label: item.WardName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms(tabPane).searchHolding.hdnWard.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txtLocation", function () {
            var tabPane = constants().getTabScope($(this));
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { WardId: _doms(tabPane).searchHolding.hdnWard.val(), Location: request.term };
                    _doms(tabPane).searchHolding.hdnLocation.val('');
                    if (!request.term || !_doms(tabPane).searchHolding.hdnWard.val()) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetLocations",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.LocationID,
                                            label: item.LocationName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms(tabPane).searchHolding.txtLocation.val(i.item.label);
                    _doms(tabPane).searchHolding.hdnLocation.val(i.item.value);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txt-holding-no", function () {
            var tabPane = constants().getTabScope($(this));
            $(this).off('keyup');
            $(this).on('keyup', function () {
                tabPane.find(".hddn-holding-no").val($(this).val());
            });
        });

        //valudation details
        $(document).on('change', ".ddl-zone", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation($(this));
        });
        $(document).on('change', ".ddl-nature-of-use", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation($(this));
        });
        $(document).on('change', ".ddl-construction", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            evtHandlers.onChangeValuation($(this));
        });
        $(document).on('change', ".ddl-ownerOrOccupier", function () {
            if ($('.ddl-ownerOrOccupier').val() == 2) {
                $('.ownerOccupier').show();
            }
            else {
                $('.ownerOccupier').hide();
            }
        });
        $(document).on('change', ".txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-land-cost,.txt-age,.chk-SrchgTag,.chk-EduCessTag", function () {
            evtHandlers.onChangeValuation($(this));
        });

        //save details
        $(document).on('click', '.btn-save', function (e) {
            e.preventDefault();
            
            var isValidated = true;
            //has new tab for seperation
            if (!constants().allNewTabPanes().length > 0) {
                isValidated = false;
                alertify.error("Please add new holdings");
            }
            //validate home tab
            //try {
            //    isValidated = helpers().validateTab(constants().HomeTabPane());
            //} catch (e) {
            //    isValidated = false;//set isValidate=false to stop loop
            //    if (typeof e == "string") {
            //        alertify.error(e);
            //    }
            //}
            //validate other new tab
            //constants().allNewTabPanes().map(function (item) {
            //    if (isValidated == true) {
            //        try{
            //            isValidated = helpers().validateTab($(item));
            //        } catch (e) {
            //            isValidated = false;//set isValidate=false to stop loop
            //            if(typeof e=="string"){
            //                alertify.error(e);
            //            }
            //        }
            //    }
            //});
            
            ///finally save data
            //if (/*isValidated==true*/5>3) {
                var existingHolding = digestedFormdata().existingHoldingData();
                var newHoldings = digestedFormdata().newSeparatedHoldings();
                var payload = { ExistingHolding: existingHolding, NewHoldings: newHoldings };
                alertify.confirm("Confirm us!", "Are you sure to save data?", function () {
                            $.ajax({
                                type: 'post',
                                url: "/Municipality/Assessment/SaveSeparationData",
                                data: JSON.stringify(payload),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {
                                    if (r.Status === 200) {
                                        alertify.alert("Successful!", r.Message, function () {
                                            //to stuff after ok buttom click
                                            location.reload();
                                        });
                                    } else {
                                        alertify.alert("Error!","<b style='color:red'>" +r.Message+"</b>", function () {
                                            //to stuff after ok buttom click
                                        });
                                    }
                                },
                                failure: function (response) {
                                    alert(response);
                                },
                                error: function (response) {
                                    alert(response);
                                }
                            });
                        }, function () { });
            //} 
        });
    };
    (function () {
        bindEvents();
    }());
});
function totalAnnualVal(table) {
    var tbl = table.find('>tbody').find('.completeJson');

    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.AnnualValuation);
        }
    });
    table.find('.lbl-total-annual-vl').text(totalVal);
}
function totalEducess(table) {
    var tbl = table.find('>tbody').find('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_EduCess);
        }
    });
    table.find('.lbl-total-end-cess').text(totalVal);
}
function totalQtrPtax(table) {
    var tbl = table.find('>tbody').find('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Ptax);
        }
    });
    table.find('.lbl-total-qtr-ptax').text(totalVal);
}
function totalQtrScharge(table) {
    var tbl = table.find('>tbody').find('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Schrg);
        }
    });
    table.find('.lbl-total-qtr-scharge').text(totalVal);
}
function totalLandCost(table) {
    var tbl = table.find('>tbody').find('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.LandCost);
        }
    });
    table.find('.lbl-total-last-cost').text(totalVal);
}
