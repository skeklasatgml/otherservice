﻿$(document).ready(function () {
    var SERVER_OBJ = function () {
        this.GetDataBind = function (data,callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AdjustmentApproval/GetApprovalAdjustmentData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callBack(response);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.DataUpdate = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AdjustmentApproval/UpdateData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
    };
    var APP_OBJ = function (serverObject) {
        var GetDataFrom = function () {
            var effectTyp = $("#ddlEffectType").val();
            return data = { effectType: effectTyp };
        };
        var BindEvent = function () {
            $("#ddlEffectType").on("change", showData);
            $("#btnAproved").on("click", Insert);

            $('#divAdj').on('change', '#chkAll', function () {
                $("#tblAdj tbody tr").find(".chkAppr").prop('checked', $("#chkAll").prop("checked"));
            });

            //$("#chkAll").on("change", function () {
            //    $("#tblAdj tbody tr").find(".chkAppr").prop('checked', $("#chkAll").prop("checked"));
            //});
        };
        var Insert = function () {
            if (confirm("Do you want to Approved the selected row / rows"))
            {
                var rowD = GetRowData();
                if (rowD.length > 0) {
                    if (confirm("If you Approved oence then never be back previous position of selected row / rows status."))
                    {
                        serverObject.DataUpdate(rowD, function (respons) {
                            if (respons.Status == 200) {
                                showData();
                            }
                            alert(respons.Message);
                        });
                    }
                }
            }
        };
        var GetRowData = function () {
            var ObjArr = [];
            var tblRows = $("#tblAdj tbody tr");
            var checkedItems = tblRows.find('.chkAppr:checked');
            if (checkedItems.length > 0) {
                $.each(checkedItems, function (ind, item) {
                    //console.log($(item).val());
                    
                    ObjArr.push($(item).val());
                });

            } else
            {
                alert("Select Atleast One Recored .......!");
            }
            return ObjArr;
        };

        var showData = function () {
            var Data = GetDataFrom();
            $("#tblAdj thead").empty();
            var varAllTab = "";
            if ($('#ddlEffectType').val() == "A") {
                varAllTab = "<tr>" +
                    //"<th>Financial Year</th>" +
                    "<th>Pay Head</th>" +
                    "<th>From Date</th>" +
                    "<th>To Date</th>" +
                    "<th>Rate</th>" +
                    //"<th>Effect on Current Demand</th>" +
                    "<th>Grace Period</th>" +
                    "<th>Select Approved&nbsp;&nbsp;All <input type='checkbox' id='chkAll' name='chkAll'/> </th>" +
                    "</tr>";
            }
            else if ($('#ddlEffectType').val() == "I") {
                varAllTab = "<tr>" +
                    //"<th>Financial Year</th>" +
                    "<th>Ward</th>" +
                    "<th>Location</th>" +
                    "<th>Assessee Name</th>" +
                    "<th>Holding No.</th>" +
                    "<th>Pay Head</th>" +
                    "<th>From Date</th>" +
                    "<th>To Date</th>" +
                    "<th>Rate</th>" +
                    //"<th>Effect on Current Demand</th>" +
                    "<th>Grace Period</th>" +
                    "<th>Select Approved&nbsp;&nbsp;All <input type='checkbox' id='chkAll' name='chkAll'/> </th>" +
                    "</tr>";
            }
             
            $("#tblAdj thead").append(varAllTab);

            $("#tblAdj tbody").empty();
            $("#chkAll").prop("checked",false);
            serverObject.GetDataBind(Data, function(response) {
                console.log(response.Data);
                var str = "";
                if (response.Data.length > 0) {
                    //alert("Ok");
                    $.each(response.Data, function (idx, head) {
                        if ($('#ddlEffectType').val() == "A") {
                            str = "<tr>" +
                                //"<td>" + head.FinYear +
                                "</td><td>" + head.PayHeadDesc +
                                "</td><td>" + head.EffectiveFromDt +
                                "</td><td>" + head.EffectiveToDt +
                                "</td><td>" + head.RateValue +
                                //"</td><td>" + head.EffectOnCurrentDemand +
                                "</td><td>" + head.gracePeriodInMonth +
                                "</td><td><input type='checkbox' name='chkApprove' class='chkAppr' value='" + head.MstOthrAdjID + "'/></td></tr>";
                          //  $("#tblAdj").append(str);
                        }
                        else if ($('#ddlEffectType').val() == "I") {
                            str ="<tr>" +
                                //"<td>" + head.FinYear +
                                "</td><td>" + head.WardName +
                                "</td><td>" + head.LocationName +
                                "</td><td>" + head.AssesseeName +
                                "</td><td>" + head.HoldingNo +
                                "</td><td>" + head.PayHeadDesc +
                                "</td><td>" + head.EffectiveFromDt +
                                "</td><td>" + head.EffectiveToDt +
                                "</td><td>" + head.RateValue +
                                //"</td><td>" + head.EffectOnCurrentDemand +
                                "</td><td>" + head.gracePeriodInMonth +
                                "</td><td><input type='checkbox' name='chkApprove' class='chkAppr' value='" + head.MstOthrAdjID + "'/></td></tr>";
                          //  $("#tblAdj").append(str);
                        }
                        $("#tblAdj").append(str);
                    });
                    
                }
                //console.log(str);
            });
        };
        this.app_Initiate = function () {
            BindEvent();
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJ();
        var appObject = new APP_OBJ(serverObject);
        appObject.app_Initiate();
    };
    (function () { INIT(); }());
});