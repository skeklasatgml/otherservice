﻿$(document).ready(function () {
    app.ddlVendors();
    app.ddlWorkType();
    //app.getAllWorkStatus();
    app.PopulateWrkOrdrNo();
    $(document).on("change", ".wrkStatusGrp", function () {
        var vendorId = $(".ddlvendor option:selected").val();
        var workCatId = $(".wType option:selected").val();
        var wrkOrdrNo = $(".wrkOrdrNo").val(); 
        if (vendorId > 0 && workCatId==1 )
            app.getAllWorkStatus(vendorId, workCatId, wrkOrdrNo);
        if (vendorId > 0 && workCatId == 2)
            app.getAllPhisicalStatus(vendorId, workCatId, wrkOrdrNo);
    });
    
    $(document).on("change", ".wType", function () {
        if ($('option:selected', this).val() == 1) {
            $('.financial').css("display", "block");
            $('.phyWork').css("display", "none");
        }
        else if ($('option:selected', this).val() == 2)
        {
            $('.financial').css("display", "none");
            $('.phyWork').css("display", "block");
        }
    });
    $(document).on("click", ".btn-sbmt", function () {
        var _formdata = new FormData;
        if ($('#image1').prop('files').length > 0) {
            _formdata.append('files1', $('#image1').prop('files')[0], $('#image1').prop('files')[0].name);
        }
        else {
            _formdata.append('files1', '');
        }
        if ($('#image2').prop('files').length > 0) {
            _formdata.append('files2', $('#image2').prop('files')[0], $('#image2').prop('files')[0].name);
        }
        else {
            _formdata.append('files2', '');
        }
        if ($('#image3').prop('files').length > 0) {
            _formdata.append('files3', $('#image3').prop('files')[0], $('#image3').prop('files')[0].name);
        }
        else {
            _formdata.append('files3', '');
        }
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData()
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirme Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData(),
                    };
                _formdata.append('WorkStatusDtls', JSON.stringify(payload.configureDetails));
                console.log(_formdata);
            $.ajax({
                    contentType: false, 
                    processData: false,
                    type: "POST",
                    url: "/Modules/DprExtension/SaveWorkStatus",
                    data: _formdata,
                    dataType: "html",
                    success: function (Response) {
                        Response = JSON.parse(Response);
                        if (Response.Status == 200)
                        {
                            alert("Data Saved Successfully.");
                            location.reload();
                        }
                        else
                        {
                            alert("Data Not Saved.");
                            location.reload();
                        }
                        
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image1Prev').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on("change", "#image1", function () {
        readURL(this);
    });
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image2Prev').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on("change", "#image2", function () {
        readURL2(this);
    });
    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image3Prev').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on("change", "#image3", function () {
        readURL3(this);
    });
    $(document).on("change", ".ddwrkcmpleted", function () {
        var Percentage = $('.ddwrkcmpleted option:selected').attr('data-percentage')+"%";
        $('.progress-bar').css({ "width": Percentage });
        $('.progress-bar').text(Percentage);
    });
    $(document).on("change", ".wStartDate", function () {
            function toDate(dateStr) {
                var fromDate = dateStr.split("/");
                var strJobDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
                return strJobDate;
        }
        if ($(".ddlvendor").val().length > 0 && $(".wrkOrdrNo").val().length > 0 && $('.wStartDate').val().length > 0) {
                $.ajax({
                    url: '/Modules/DprExtension/CheckWrkStatusDtWithWrkCompltnDt',
                    type: 'POST',
                    data: JSON.stringify({ WrkStatusDt: toDate($('.wStartDate').val()), VId: $(".ddlvendor").val(), WorkOrderNo: $(".wrkOrdrNo").val() }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.Data == true) {
                            $('.hdnIsDelayed').val('1');
                            $('.nrmlRemarks').css('display', 'none');
                            $('.dlyRemarks').css('display', 'block');
                        }
                        else {
                            $('.hdnIsDelayed').val('0');
                            $('.nrmlRemarks').css('display', 'block');
                            $('.dlyRemarks').css('display', 'none');
                        }
                    },
                    error: function () { }
                });
            }
            else {
                alertify.alert("Please select vendor and work order no first.");
                $('.wStartDate').val('');
            }
    });
});
var app = {
    ddlVendors: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllVendors',
            type: 'POST',
            data: JSON.stringify({ VendorName: null,considerMnId: true }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.PopulateVendors(response.Data);
                $('#ddlvendor').val("")
            },
            error: function () { }
        });
    },
    ddlWorkType: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllWorkTypes',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.PopulateWorkType(response.Data);
                $('#wType').val("")
            },
            error: function () { }
        });
    },
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#workStatus"),
                ddlVendor: $(".ddlvendor"),
                ddlWType: $(".wType"),
                ddlWMilestone: $(".ddwrkcmpleted"),
                txtCummiAmt: $(".cummiAmt"),
                txtRemarks: $(".remarks"),
                txtDlyRemarks: $(".delayremarks"),
                txtWorkOrderNo: $(".wrkOrdrNo"),
                txtWorkStrtDt: $(".wStartDate"),
                txtPaidOn: $(".paidOn"),
                txtImg1Dt: $(".image1Date"),
                txtImg2Dt: $(".image2Date"),
                txtImg3Dt: $(".image3Date"),
            }
        };
        return {
            validate: function () {
                $("#workStatus").validate().resetForm();
                _doms().frm.removeData('validator');
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                var wt = $(".wType").val(); 
                var isDelayed=$('.hdnIsDelayed').val();
                vldObj.rules[_doms().ddlVendor.prop('name')] = { required: true }; 
                vldObj.rules[_doms().ddlWType.prop('name')] = { required: true }; 
                vldObj.rules[_doms().ddlWMilestone.prop('name')] = { required: (wt == 2) }; 
                vldObj.rules[_doms().txtRemarks.prop('name')] = { required: (isDelayed == 0) };
                vldObj.rules[_doms().txtDlyRemarks.prop('name')] = { required: (isDelayed == 1) };
                vldObj.rules[_doms().txtWorkOrderNo.prop('name')] = { required: true };
                vldObj.rules[_doms().txtCummiAmt.prop('name')] = { required: (wt == 1), number: (wt == 1) };
                vldObj.rules[_doms().txtPaidOn.prop('name')] = { required: (wt == 1) };
                vldObj.rules[_doms().txtWorkStrtDt.prop('name')] = { required: (wt == 2) };

                //set error messages
                vldObj.messages[_doms().ddlVendor.prop('name')] = { required: "Select vendor" };
                vldObj.messages[_doms().ddlWType.prop('name')] = { required: "Select work type" };
                vldObj.messages[_doms().ddlWMilestone.prop('name')] = { required: "Select work completed till" };
                vldObj.messages[_doms().txtRemarks.prop('name')] = { required: "Please put remarks" };
                vldObj.messages[_doms().txtDlyRemarks.prop('name')] = { required: "Please put delay remarks" };
                vldObj.messages[_doms().txtWorkOrderNo.prop('name')] = { required: "Please enter work order no" };
                vldObj.messages[_doms().txtCummiAmt.prop('name')] = { required: "Please amount", number: "It should be numeric" };
                vldObj.messages[_doms().txtPaidOn.prop('name')] = { required: "Please select a date"};
                vldObj.messages[_doms().txtWorkStrtDt.prop('name')] = { required: "Please select a date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }
                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                function toDate(dateStr) {
                    var fromDate = dateStr.split("/");
                    var strJobDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
                    return strJobDate;
                }
                var from = _doms().txtWorkStrtDt.val().split("/")
                var wrkStDT = _doms().txtWorkStrtDt.val() == "" ? null : toDate(_doms().txtWorkStrtDt.val());

                var paidOn = _doms().txtPaidOn.val() == "" ? null : toDate(_doms().txtPaidOn.val());

                var img1AsOn = _doms().txtImg1Dt.val() == "" ? null : toDate(_doms().txtImg1Dt.val()); 

                var img2AsOn = _doms().txtImg2Dt.val() == "" ? null : toDate(_doms().txtImg2Dt.val()); 

                var img3AsOn = _doms().txtImg3Dt.val() == "" ? null : toDate(_doms().txtImg3Dt.val()); 
                return {
                    VId: _doms().ddlVendor.val(),
                    WTypId: _doms().ddlWType.val(),
                    WMilestone: _doms().ddlWMilestone.val(),
                    Remarks: _doms().txtRemarks.val(),
                    DelayRemarks: _doms().txtDlyRemarks.val(),
                    WorkOrderNo: _doms().txtWorkOrderNo.val(),
                    WrkStrtDt: wrkStDT,
                    CummiAmt: _doms().txtCummiAmt.val() == "" ? 0 : _doms().txtCummiAmt.val(),
                    PaidOn: paidOn,
                    Pic1AsOn: img1AsOn,
                    Pic2AsOn: img2AsOn,
                    Pic3AsOn: img3AsOn
                };
            }
        }
    },
    PopulateVendors: function (data) {
        var ctlr_ddlvendor = $('.ddlvendor');
        ctlr_ddlvendor.empty();
        ctlr_ddlvendor.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            ctlr_ddlvendor.append("<option value='" + value.VId + "' >" + value.VName + "</option>")
        });
        var option = ctlr_ddlvendor.find("option[value='" + ctlr_ddlvendor.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlvendor.find('option:eq(0)').prop('selected', true);
        }
    },
    PopulateWorkType: function (data) {
        var ctlr_ddlWType = $('.wType');
        ctlr_ddlWType.empty();
        ctlr_ddlWType.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            ctlr_ddlWType.append("<option value='" + value.WTypId + "' >" + value.WTypName + "</option>")
        });
        var option = ctlr_ddlWType.find("option[value='" + ctlr_ddlWType.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlWType.find('option:eq(0)').prop('selected', true);
        }
    },
    getAllWorkStatus: function (vendorId, workCatId, wrkorderNo) {
        $.ajax({
            url: '/Modules/DprExtension/GetAllWorkStatus',
            type: 'POST',
            data: JSON.stringify({ vendorId: vendorId, workCatId: workCatId, wrkorderNo: wrkorderNo}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    $('#myTable').find('>tbody').empty();
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');
                        //floor control
                        var td = $('<td class="tender-No" />');
                        tr.append(td.append(tempJson.TenderNo));

                        var td = $('<td class="vendor-Name" />');
                        tr.append(td.append(tempJson.VendorName));

                        //add usage control
                        td = $('<td class="workorder-no" />');
                        tr.append(td.append(tempJson.WorkOrderNo));

                        //add Year control
                        td = $('<td class="work-order-dt" />');
                        tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.WrkOrdrDt).format('dd/mm/yyyy')));

                        //add covered area control
                        //td = $('<td class="allocated-wrk-strt-dt" />');
                        //tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.AllocatedWrkStrtDt).format('dd/mm/yyyy')));

                        ////add Occupent Name control
                        //td = $('<td class="work-started" />');
                        //tr.append(td.append(tempJson.WrkStrtDt != null ? CONVERTER.Date.convertCsToJsDate(tempJson.WrkStrtDt).format('dd/mm/yyyy') : ""));

                        //td = $('<td class="allocated-wrk-cmpletn-dt" />');
                        //tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.WrkCmpltnDt).format('dd/mm/yyyy')));

                        td = $('<td class="amt" />');
                        tr.append(td.append(tempJson.CummiAmt));

                        td = $('<td class="paid-on" />');
                        tr.append(td.append(tempJson.PaidOn != null ? CONVERTER.Date.convertCsToJsDate(tempJson.PaidOn).format('dd/mm/yyyy') : ""));

                        td = $('<td class="remarks" />');
                        tr.append(td.append(tempJson.Remarks));

                        $('#myTable').find('>tbody').append(tr);
                    }
                }
            },
            error: function () { }
        });
    },
    getAllPhisicalStatus: function (vendorId, workCatId, wrkorderNo) {
        $.ajax({
            url: '/Modules/DprExtension/GetAllWorkStatus',
            type: 'POST',
            data: JSON.stringify({ vendorId: vendorId, workCatId: workCatId, wrkorderNo: wrkorderNo }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    $('#PhysicalStatusTbl').find('>tbody').empty();
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');
                        //floor control
                        var td = $('<td class="tender-No" />');
                        tr.append(td.append(tempJson.TenderNo));

                        var td = $('<td class="vendor-Name" />');
                        tr.append(td.append(tempJson.VendorName));

                        //add usage control
                        td = $('<td class="workorder-no" />');
                        tr.append(td.append(tempJson.WorkOrderNo));

                        //add Year control
                        td = $('<td class="work-order-dt" />');
                        tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.WrkOrdrDt).format('dd/mm/yyyy')));

                        //add covered area control
                        //td = $('<td class="allocated-wrk-strt-dt" />');
                        //tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.AllocatedWrkStrtDt).format('dd/mm/yyyy')));

                        //add Occupent Name control
                        td = $('<td class="work-started" />');
                        tr.append(td.append(tempJson.WrkStrtDt != null ? CONVERTER.Date.convertCsToJsDate(tempJson.WrkStrtDt).format('dd/mm/yyyy') : ""));

                        //td = $('<td class="allocated-wrk-cmpletn-dt" />');
                        //tr.append(td.append(CONVERTER.Date.convertCsToJsDate(tempJson.WrkCmpltnDt).format('dd/mm/yyyy')));

                        td = $('<td class="paid-on" />');
                        tr.append(td.append(tempJson.WMilestoneDesc));

                        td = $('<td class="paid-on" />');
                        tr.append(td.append(tempJson.Percentage));

                        td = $('<td class="remarks" />');
                        tr.append(td.append(tempJson.Remarks));

                        if (tempJson.ImagePath != null && tempJson.ImagePath.length>0)
                        {
                            td = $('<td class="remarks" />');
                            tr.append(td.append('<a href="' + tempJson.ImagePath +'" target="_blank">View File</a>'));
                        }
                        else
                        {
                            td = $('<td class="remarks" />');
                            tr.append(td.append('<a href="#">No File</a>'));
                        }

                        $('#PhysicalStatusTbl').find('>tbody').append(tr);
                    }
                }
            },
            error: function () { }
        });
    },
    getWorkMilestone: function (vendorId,wrkorderNo) {
        $.ajax({
            url: '/Modules/DprExtension/GetAllMilestone',
            type: 'POST',
            data: JSON.stringify({ vendorId:vendorId , wrkorderNo: wrkorderNo }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var data=response.Data;
                var ctlr_ddlWmileStone = $('.ddwrkcmpleted');
                ctlr_ddlWmileStone.empty();
                ctlr_ddlWmileStone.append('<option value="">--select--</option>');
                $(data).each(function (index, value) {
                    ctlr_ddlWmileStone.append("<option data-percentage='" + value.percentage +"' value='" + value.MileStoneId + "' >" + value.WmDesc + "</option>")
                });
                $('#ddwrkcmpleted').val("")
            },
            error: function () { }
        });
    },
    PopulateWrkOrdrNo: function () {
        $(document).on('focus', ".wrkOrdrNo", function () {
            $(this).autocomplete({
                source: function (request, response) {
                    var VName = $(".ddlvendor option:selected").text();
                    var data = { VendorName: VName, WrkOrdrNo: request.term == " " ? '' : request.term};
                    //$(".wrkOrdrNo").val('');
                    if (!request.term) {
                        return;
                    }
                    if (VName == "" || VName == null || VName == "--select--")
                    {
                        alertify.alert("Please select Vandor");
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/Modules/DprExtension/GetAllWorkOrder',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WrkOrdrNoVal,
                                            label: item.WrkOrdrNoTxt
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    $(".wrkOrdrNo").val(i.item.value);
                    var vendorId = $(".ddlvendor option:selected").val();
                    var workCatId = $(".wType option:selected").val();
                    if (vendorId > 0 && workCatId == 1 && i.item.value.length>0)
                        app.getAllWorkStatus(vendorId, workCatId, i.item.value);
                    if (workCatId == 2 && i.item.value.length > 0)
                    {
                        app.getWorkMilestone(vendorId, i.item.value);
                        app.getAllPhisicalStatus(vendorId, workCatId, i.item.value);
                    }
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
    }
}