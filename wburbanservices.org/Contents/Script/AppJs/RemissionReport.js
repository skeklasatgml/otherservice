﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetPayHead = function (callback) {
            console.log('Get_Payheads');
            $.ajax({
                type: 'POST',
                contentType: 'application/json;cursor=utf-8',
                data: null,
                dataType: 'json',
                url: "/Municipality/MnPayHead/GetPayheads",
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    alert('error occur');
                }
            });
        };
    };
    var APP_OBJECT = function (ServObj) {
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateTo,#DateFrom').blur(function () {
                if ($(this).val().trim() == '') {
                    return false;
                }
            });
            PopulatePayHeadOnLoad();
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                $('#frmRemissionReport').submit();
            }
        };
        var Validate = function (data) {

            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            return true;
        };
        var getDataFromView = function () {
            var DateFrom = $('#DateFrom').val().trim();
            var DateTo = $('#DateTo').val().trim();

            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }

            var data = { DateFrom: DateFrom, DateTo: DateTo };
            return data;
        };
        var PopulatePayHeadOnLoad = function () {
            console.log('start');
            ServObj.GetPayHead(function (response) {
                if (response.Status == 200) {
                    console.log(response.Message);
                    $.each(response.Data, function (idx, item) {
                        var key = item.PayHeadID;
                        var label = item.PayHeadDesc;
                        $("#ddlPayHeade").append("<Option value=" + key + "> " + label + "</Option");
                    });
                }
            });
        };
        this.app_Initiate = function () {
            BindEvent();
        }; 
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    ( function(){ INIT(); })();
});