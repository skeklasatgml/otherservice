﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {

        this.GetPropertyTax = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/PropertyTax/GetAllPropertyTax",
                data: JSON.stringify({ PropertyData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };

        this.GetMunicipality = function (data,callBack)
        {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/PropertyTax/AutoMunicipality",
                data: JSON.stringify({ municipalityParam: data }),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                 error: function (result) {
                            alert("Error");
                  }
            });
        }

        this.GetWard = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/PropertyTax/AutoWard",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        this.GetLocation = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/PropertyTax/AutoLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        this.GetAssessee = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/PropertyTax/AutoAssessee",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        this.SavePropertyTax = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/PropertyTax/SavePropertyTax",
                data: JSON.stringify({ objPropertyTax: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.GetPropertyTaxbyID = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/PropertyTax/GetPropertyTaxByID",
                data: JSON.stringify({ PropertyEditData: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {

        
       
        var BindEvent = function () {
            $("#btnSubmit").on('click', OnInsertPropertyTax);
            $("#btnCancel").on('click', OnCancelPropertyTax);
            $("#btnSearch").on('click', OnSearchPropertyTax);
            $("#txtMunicipality").autocomplete(PopulateMunicipality());
            $("#txtWard").autocomplete(PopulateWard());
            $("#txtLocation").autocomplete(PopulateLocation());
            $("#txtAssessee").autocomplete(PopulateAssessee());
            
            
        };

        var PopulateMunicipality = function () {           
            return {
                source: function (request, response) {
                    serverObject.GetMunicipality(request.term, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtMunicipality').val('');
                            $('#hdnMunicipality').val('');
                        }
                    });
                },
                select: function (e, i) {
                    $("#hdnMunicipality").val(i.item.MunicipalityID);
                    PopulateWard();                    
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnMunicipality").val('');
                        $('#txtWard').val('');
                        $('#hdnWard').val('');
                        $('#txtLocation').val('');
                        $('#hdnLocation').val('');
                        $('#txtAssessee').val('');
                        $('#hdnAssessee').val('');                       
                        //PopulateWard();
                    }
                }
            };
        };

        var PopulateWard = function () {                         
                return {
                    source: function (request, response) {
                        var municipalityID = $("#hdnMunicipality").val();
                        //var wardName = request.term;
                        var data = { municipalityID: municipalityID, wardName: request.term };
                        serverObject.GetWard(data, function (serverResponse) {
                            if ((serverResponse).length > 0) {
                                var userDataAutoComplete = [];
                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                                response(userDataAutoComplete);
                            }
                            else {
                                alert('No data found.');
                                $('#txtWard').val('');
                                $('#hdnWard').val('');
                            }
                        });
                    },
                    select: function (e, i) {
                        $("#hdnWard").val(i.item.WardID);
                        PopulateLocation();
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            $("#hdnWard").val('');                           
                            $('#txtLocation').val('');
                            $('#hdnLocation').val('');
                            $('#txtAssessee').val('');
                            $('#hdnAssessee').val('');
                           // PopulateLocation();
                        }
                    }
                };        
        }

        var PopulateLocation = function () {                        
            return {
                source: function (request, response) {
                    var municipalityID = $("#hdnMunicipality").val();
                    var wardID = $("#hdnWard").val();
                    //var wardName = request.term;
                    var data = { municipalityID: municipalityID, wardID: wardID, locationName: request.term };
                    serverObject.GetLocation(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.LocationName,
                                    LocationID: item.LocationID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtLocation').val('');
                            $('#hdnLocation').val('');
                        }

                    });
                },
                select: function (e, i) {
                    $("#hdnLocation").val(i.item.LocationID);
                    PopulateAssessee();
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnLocation").val('');
                        $('#txtAssessee').val('');
                        $('#hdnAssessee').val('');
                    }
                }
            };
        }

        var PopulateAssessee = function () {             
            return {
                source: function (request, response) {
                    var municipalityID = $("#hdnMunicipality").val();
                    var wardID = $("#hdnWard").val();
                    var locationID = $("#hdnWard").val();
                    //var wardName = request.term;
                    var data = { municipalityID: municipalityID, wardID: wardID, locationID: locationID, locationName: request.term };
                    serverObject.GetAssessee(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.AssesseeName,
                                    AssesseeID: item.AssesseeID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtAssessee').val('');
                            $('#hdnAssessee').val('');
                        }
                    });
                },
                select: function (e, i) {
                    $("#hdnAssessee").val(i.item.AssesseeID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnAssessee").val('');
                    }
                }
            };
        };
            
        var PopulatePropertyTax = function (searchData) {            
            var tabPropertyTaxDtl = $("#tabPropertyTaxDtl");
            //var ctr_btnRowDel = $(".btnRowDel");
            //var btnRowEdit = $(".btnRowEdit");
            tabPropertyTaxDtl.find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";                
                rowstr += "<td style='display:none' class='PropertyTaxID'>" + rowData.PropertyTaxID + "</td>";
                rowstr += "<td style='display:none' class='MunicipalityID'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td style='display:none' class='WardID'>" + rowData.WardID + "</td>";
                rowstr += "<td style='display:none' class='LocationID'>" + rowData.LocationID + "</td>";
                rowstr += "<td style='display:none' class='AssesseeID'>" + rowData.AssesseeID + "</td>";
                rowstr += "<td class='MunicipalityName'>" + rowData.MunicipalityName + "</td>";
                rowstr += "<td class='WardName'>" + rowData.WardName + "</td>";
                rowstr += "<td class='LocationName'>" + rowData.LocationName + "</td>";
                rowstr += "<td class='HoldingNo'>" + rowData.HoldingNo + "</td>";
                rowstr += "<td class='AssesseeName'>" + rowData.AssesseeName + "</td>";
                rowstr += "<td class='FinYear'>" + rowData.FinYear + "</td>";                
                rowstr += "<td align='right' class='PropertyTaxValue'>" + rowData.PropertyTaxValue + "</td>";
                rowstr += "<td align='right' class='SurchargeValue'>" + rowData.SurchargeValue + "</td>";
                rowstr += "<td align='right' class='SurchargePercent'>" + rowData.SurchargePercent + "</td>";
                rowstr += "<td align='center'><span id='spanEdit' style='cursor: pointer'><img src='/Contents/image/edit.png'/></span></td>"
                rowstr += "<td align='center'><span id='spanDelete' style='cursor: pointer'><img src='/Contents/image/delete.png'/></span></td>"
                //rowstr += "<td><a class='btnRowEdit'>Edit</a></td>";
                //rowstr += "<td><a class='btnRowDel'>Delete</a></td>";
                rowstr += "</tr>";
                return rowstr;
            };

            var appendToBody = function (rowString) {
                tabPropertyTaxDtl.find("tbody").append(rowString);
            };

            var onDeleteRow = function () {
                var ctrl = $(this);
                //find row

            };
            var onEditRow = function () {
                
                var CurrentTR = $(this).closest('tr');
                var PropertyTaxID = CurrentTR.find('td:eq(0)').text();
                var MunicipalityID = CurrentTR.find(".MunicipalityID").text();
                var WardID = CurrentTR.find(".WardID").text();
                var LocationID = CurrentTR.find(".LocationID").text();
                var AssesseeID = CurrentTR.find(".AssesseeID").text();

                $("#btnSubmit").val('Update');
                $("#btnSubmit").attr('PropertyTaxID', PropertyTaxID);
                var data = { PropertyTaxID: PropertyTaxID, MunicipalityID: MunicipalityID, WardID: WardID, LocationID: LocationID, AssesseeID: AssesseeID };
                serverObject.GetPropertyTaxbyID(data, function (response) {
                    if (response.Status == 200) {
                        var dataByPTaxID = response.Data;
                        PropertyTaxDataforEdit(response.Data);                             
                    }
                });

                var PropertyTaxDataforEdit = function (data) {
                    var ptaxID = data.PropertyTaxID;
                    $("#txtMunicipality").val(data.MunicipalityName);
                    $("#hdnMunicipality").val(data.MunicipalityID);

                    $("#txtPropertyTaxValue").val(data.PropertyTaxValue);
                    $("#txtSurchargeValue").val(data.SurchargeValue);
                    $("#txtSurchargePercent").val(data.SurchargePercent);

                };
            };

            var RegisterTableEvent = function () {
                $("#tabPropertyTaxDtl tbody").off('click', 'span[id="spanDelete"]', onDeleteRow);
                $("#tabPropertyTaxDtl tbody").on('click', 'span[id="spanDelete"]', onDeleteRow);

                $("#tabPropertyTaxDtl tbody").off('click', 'span[id="spanEdit"]', onEditRow);
                $("#tabPropertyTaxDtl tbody").on('click', 'span[id="spanEdit"]', onEditRow);
            };
            
            (function () {
                RegisterTableEvent();
                serverObject.GetPropertyTax(searchData, function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data
                        if (rowDatas.length == 0) {
                            alert("No data found");
                            return;
                        }
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        };

        var From_Validate = function () {
            jQuery.validator.addMethod("dollarsscents", function (value, element) {
                return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
            }, "You must include number with two decimal places.");
            $("#frmPropertyTax").validate({
                rules: {
                    txtMunicipality:{
                        required:true
                    },
                    hdnMunicipality: {
                        required: true
                    },
                    txtWard: {
                        required: true
                    },
                    txtLocation: {
                        required: true
                    },
                    txtAssessee:{
                        required: true
                    },
                    txtPropertyTaxValue:{
                        required:true,
                        dollarsscents: true
                    },
                    txtSurchargeValue: {
                        required: true,
                        number: true
                    },
                    txtSurchargePercent:{
                        required: true,
                        number: true
                    }
                },
                messages: {
                    txtMunicipality: {
                        required: "Please select Municipality."
                    },
                    hdnMunicipality: {
                        required: "Please select correct Municipality."
                    },
                    txtWard: {
                        required: "Please select Ward."
                    },
                    txtLocation: {
                        required: "Please select Location."
                    },
                    txtAssessee: {
                        required: "Please select Assessee."
                    },
                    txtPropertyTaxValue: {
                        required: "Please enter Property Tax Value.",
                        number: "Input numbers only"
                    },
                    txtSurchargeValue: {
                        required: "Please enter Surcharge Value.",
                        number: "Input numbers only"
                    },
                    txtSurchargePercent: {
                        required: "Please enter Surcharge Percent.",
                        number  : "Input numbers only"
                    }
                },
                tooltip_options: {
                    txtMunicipality: { placement: 'right', html: true }
                },
                //errorPlacement: function (error, element) {
                //    element.attr("placeholder", error[0].outerText);
                //},
                highlight: function (element) {
                    //$(element).css('background', '#ffdddd');
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
            }
            });
            return $('#frmPropertyTax').valid();
        }

        var From_ValidateForSearch = function () {         
            $("#frmPropertyTax").validate({
                rules: {
                    txtMunicipality: {
                        required: true
                    },
                    txtWard: {
                        required: true
                    },
                    txtLocation: {
                        required: true
                    },
                    txtAssessee: {
                        required: true
                    }
                },
                messages: {
                    txtMunicipality: {
                        required: "Please select Municipality."
                    },
                    txtWard: {
                        required: "Please select Ward."
                    },
                    txtLocation: {
                        required: "Please select Location."
                    },
                    txtAssessee: {
                        required: "Please select Assessee."
                    }
                },
                highlight: function (element) {
                    $(element).css('border', '1px solid #a94442');
                    $(element).addClass('error');
                },
                unhighlight: function (element) {
                    $(element).css('background', 'white');
                    $(element).css('border', '1px solid #18bc9c');
                    $(element).removeClass('error')
                }
            });
            return $('#frmPropertyTax').valid();
        }

        var getDataFromView = function () {
            // var MunicipalityID = $("#btnInsert").attr('MunicipalityId');
            var PropertyTaxID = null;
            var MunicipalityID = $("#hdnMunicipality").val();
            var MunicipalityName = $("#txtMunicipality").val();
            var WardID = $("#hdnWard").val();
            var WardName = $("#txtWard").val();
            var LocationID = $("#hdnLocation").val();
            var LocationName = $("#txtLocation").val();
            var HoldingNo = null;
            var AssesseeID = $("#hdnAssessee").val();
            var AssesseeName = $("#txtAssessee").val();
            var FinYear = null;
            var PropertyTaxValue = $("#txtPropertyTaxValue").val();
            var SurchargeValue = $("#txtSurchargeValue").val();
            var SurchargePercent = $("#txtSurchargePercent").val();
            var InsertedBy = null;
            var InsertedOn = null;
            var UpdatedBy = null;
            var UpdatedOn = null;
            var data = {
                PropertyTaxID: PropertyTaxID, MunicipalityID: MunicipalityID, MunicipalityName: MunicipalityName,
                WardID: WardID, WardName: WardName, LocationID: LocationID, LocationName: LocationName, HoldingNo: HoldingNo, AssesseeID: AssesseeID, AssesseeName: AssesseeName,
                FinYear:FinYear, PropertyTaxValue:PropertyTaxValue, SurchargeValue:SurchargeValue, SurchargePercent:SurchargePercent,
                InsertedBy: InsertedBy, InsertedOn: InsertedOn, UpdatedBy: UpdatedBy, UpdatedOn: UpdatedOn
            };
            return data;
        };

        var OnInsertPropertyTax = function () {
            var formData = getDataFromView();
            var btnSubmitVal = $("#btnSubmit").val();
            var btnSubmitAttrVal = $("#btnSubmit").attr("PropertyTaxID");

            //alert(btnSubmitVal + btnSubmitAttrVal);

            if (From_Validate())
            {
                if (btnSubmitVal == 'Submit') {
                    serverObject.SavePropertyTax(formData, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            alert(response.Message);
                        } else {
                            alert(response.Message);
                        }
                    });
                }
                else if (btnSubmitVal == 'Update') {
                    //serverObject.SavePropertyTax(formData, function (response) {
                    //    var status = response.Status;
                    //    if (status == 200) {
                    //        alert(response.Message);
                    //    } else {
                    //        alert(response.Message);
                    //    }
                    //});
                }
                    
            }                       
        };

        var OnCancelPropertyTax = function () {
            ClearView();
        };

        var OnSearchPropertyTax = function () {
            //$("#txtMunicipality").rules("remove");
            //$("#txtPropertyTaxValue").rules("remove");
            //$("#txtSurchargeValue").rules("remove");
            //$("#txtSurchargePercent").rules("remove");
            if (From_ValidateForSearch()) {
                var searchData = getDataFromView();
                PopulatePropertyTax(searchData);
            }            
        };

        var ClearView = function () {

            //var validator = $("#myform").validate();
            //validator.resetForm();
            $("#txtMunicipality").val('');
            $("#hdnMunicipality").val('');
            $("#txtWard").val('');
            $("#hdnWard").val('');
            $("#txtLocation").val('');
            $("#hdnLocation").val('');
            $("#txtAssessee").val('');
            $("#hdnAssessee").val('');
            
            $("#txtPropertyTaxValue").val('');
            $("#txtSurchargeValue").val('');
            $("#txtSurchargePercent").val('');

            $("#tabPropertyTaxDtl").find("tbody").empty();

            $("#btnSubmit").attr('PropertyTaxID', '');
        };

        this.app_Initiate = function () {
            
            //PopulatePropertyTax();
            BindEvent();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
       
              
    };

    (function () {
        INIT();
    }());
});