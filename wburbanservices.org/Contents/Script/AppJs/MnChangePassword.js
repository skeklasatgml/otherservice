﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.chkPass = function (OldPassword, NewPassword, RePassword, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserPassword/ChkOldPwd",
                data: "{Old: '" + OldPassword + "', New:'" + NewPassword + "', RePwd:'" + RePassword + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var DomControls = {
            ctrl_btnChange: $("#btnChange"),
            ctrl_txtOldPassword: $("#txtOldPassword"),
            ctrl_txtNewPassword: $("#txtNewPassword"),
            ctrl_txtRePassword: $("#txtRePassword")
        };
        
        var Validate = function () {
            var vldObj = {
                rules: {},
                messages: {},
                errorOccur: {}
            };
            vldObj.rules[DomControls.ctrl_txtOldPassword.prop('name')] = { required: true };
            vldObj.rules[DomControls.ctrl_txtNewPassword.prop('name')] = { required: true };
            vldObj.rules[DomControls.ctrl_txtRePassword.prop('name')] = { equalTo: "#txtNewPassword", required: true };

            vldObj.messages[DomControls.ctrl_txtOldPassword.prop('name')] = { required: "Please Enter Old Password" };
            vldObj.messages[DomControls.ctrl_txtNewPassword.prop('name')] = { required: "Please Enter New Password" };
            vldObj.messages[DomControls.ctrl_txtRePassword.prop('name')] = { required: "Please Re-Enter Password", equalTo: "Password and Re-Enter Password Not Equal" };
            
            vldObj.errorOccur = function (error, element) {
                error.insertAfter(element);
                error.css('color', 'red');
            }
            
            $("#frmChngPwd").validate(vldObj);
            return $('#frmChngPwd').valid();
        };

        var OnClick = function () {
            //validation
            Validate();
            //server connection
            if (Validate()) {
                if (DomControls.ctrl_txtNewPassword.val() == DomControls.ctrl_txtRePassword.val()) {
                    serverObject.chkPass(DomControls.ctrl_txtOldPassword.val(), DomControls.ctrl_txtNewPassword.val(), DomControls.ctrl_txtRePassword.val(), function (response) {
                        var status = response.Status;
                        var msg = response.Message;
                        if (status == 200) {
                            if (msg == "SUCCESS") {
                                alert('Changed Successfully !!');
                                $("#txtOldPassword").val('');
                                $("#txtNewPassword").val('');
                                $("#txtRePassword").val('');
                            }
                            if (msg == "NOT_MATCHED") {
                                alert('New Password and Re-Password should be same !!');
                            }
                            if (msg == "UNSUCCESS") {
                                alert('Not Matched Old Password !!');
                                $("#txtOldPassword").focus();
                            }
                        } else {
                            alert(response.Message);
                        }
                    });
                }
            }
            //resp
            //
        };
        var BindEvents = function () {
            DomControls.ctrl_btnChange.on('click', OnClick);
        };
        (function () {
            BindEvents();
        }());
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    (function () {
        INIT();
    }());
});