﻿$(document).ready(function () {
    app.getAllFundSource();
    $(document).on("click", ".btn-sbmt, .btn-edit", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData();
        console.log(configureDetails);
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirm Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData()
                };
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/SaveFundSource",
                    data: JSON.stringify(payload.configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    $(document).on("click", ".btn-cancel", function () {
        location.reload();
    });
    $(document).on("click", ".updateRow", function () {
        var jsnData = JSON.parse($(this).closest('tr').find('.hdnJSN').val());
        $(".fundSource").val(jsnData.FundSource);
        $(".fundSourceId").val(jsnData.FundSourceID).hide();
        $(".btn-sbmt").css("display", "none");
        $(".btn-edit").css("display", "block");
        $(".btn-cancel").css("display", "block");
    });
});
var app = {
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#configureFundSource"),
                txtFundSource: $(".fundSource"),
                txtFundSourceID: $(".fundSourceId"),
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().txtFundSource.prop('name')] = { required: true };
               
                //set error messages
                vldObj.messages[_doms().txtFundSource.prop('name')] = { required: "Please enter Fund Source" };
                //vldObj.messages[_doms().ddlMuni.prop('name')] = { required: "Select Municipality" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }
                }
                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                return {
                    FundSource: _doms().txtFundSource.val(),
                    FundSourceID: _doms().txtFundSourceID.val(),
                };
            }
        }
    },
    getAllFundSource: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllFundSource',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');
                        var td = $('<td />');
                        tr.append(td.append(i+1));

                        var td = $('<td class="fund-source" />');
                        tr.append(td.append(tempJson.FundSource));

                        td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                        tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson + '\'/>Update</a>'));

                        //td = $('<td class="completeJson"><input type="hidden" class="hdnJSN" value="' + tempJson +'"/></td>');
                        //tr.append(td);
                        $('#myTable').find('>tbody').append(tr);
                    }
                }
            },
            error: function () { }
        });
    },
}