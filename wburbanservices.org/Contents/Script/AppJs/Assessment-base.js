﻿var Assessment = new function () {
    var form = null;
    var valTag = function () { 
        return $(".setting-parameters").find('.val-tag').val();
    };
    var yearId = function () {
        return $(".setting-parameters").find('.year-id').val();
    };
    var subAction = function () {
        return $(".setting-parameters").find('.sub-action').val();
    };
    var getFinYear=function(){
        return $(".ddl-finyear-picker").val();
    }
    var getQtr=function(){
        return $(".ddl-qtr").val();
    }
    var convertTodate = function (format, date) {
        switch (format) {
            case 'dd/mm/yyyy':
            case 'dd/mm/yy':
                {
                    var array = date.split('/');
                    return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                    break;
            }
        }
    }
    
    var AssesseeForm = function () {
        this.HoldingDetails = new function () {
            var _doms = function () {
                return {
                    frm: $("#frm-holding-details"),
                    txtWard: $(".txtWard"),
                    hdnWard: $(".hddn-Ward"),
                    txtLocation: $(".txtLocation"),
                    hdnLocation: $(".hddn-Location"),
                    txtHoldingNo: $(".txt-holding-no"),
                    hddnHoldingNo: $(".hddn-holding-no"),
                    ddlHoldingType: $(".ddl-holding-type"),
                    ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                    listHoldingNames: $(".list-holding-names"),
                    txtAddress: $(".txt-address"),
                    txtOwnerName: $(".txt-assessee-name")
                }
            };
            this.validate = function () {
                $.validator.addMethod(
                    "OwnerList",
                    function (value, element, params) {
                        // otherwise, use your rule
                        if ($(".txt-assessee-name").parent().siblings('ul').find('li').length > 0) {
                            return true;
                        }
                        return false;
                    },
                    "Enter Valid Date"
                );
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().hdnWard.prop('name')] = { required: true };
                vldObj.rules[_doms().hdnLocation.prop('name')] = { required: true };
                vldObj.rules[_doms().hddnHoldingNo.prop('name')] = { required: true };
                vldObj.rules[_doms().ddlHoldingType.prop('name')] = { required: true };
                vldObj.rules[_doms().ddlHoldingTypeGroup.prop('name')] = { required: true };
                vldObj.rules[_doms().txtAddress.prop('name')] = { required: true };
                vldObj.rules[_doms().txtOwnerName.prop('name')] = { OwnerList: true };


                //set error messages
                vldObj.messages[_doms().hdnWard.prop('name')] = { required: "Please enter Ward." };
                vldObj.messages[_doms().hdnLocation.prop('name')] = { required: "Please enter Location." };
                vldObj.messages[_doms().hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };
                vldObj.messages[_doms().ddlHoldingType.prop('name')] = { required: "Please select holding type" };
                vldObj.messages[_doms().ddlHoldingTypeGroup.prop('name')] = { required: "Please holding type group" };
                vldObj.messages[_doms().txtAddress.prop('name')] = { required: "Please enter address" };
                vldObj.messages[_doms().txtOwnerName.prop('name')] = { OwnerList: "Please add owner list" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            };
            this.getFormData = function () {
                var Owners = function () {
                    var owners = [];
                    $.each(_doms().listHoldingNames.find('li'), function (index, value) {
                        owners.push({
                            AssesseeName: $(value).attr("data-name"),
                            AssesseeAddress: _doms().txtAddress.val()
                        });
                    });
                    return owners;
                };
                return {
                    WardId: _doms().hdnWard.val(),
                    WardName: _doms().txtWard.val(),
                    LocationId: _doms().hdnLocation.val(),
                    LocationName: _doms().txtLocation.val(),
                    HoldingNo: _doms().hddnHoldingNo.val(),
                    HoldingTypeId: _doms().ddlHoldingType.val(),
                    HoldingTypeGroupId: _doms().ddlHoldingTypeGroup.val(),
                    Owners: Owners(),
                    Address: _doms().txtAddress.val(),
                };
            };
        };
        this.ValuationDetails = new function () {
            var _doms = function () {
                return {
                    frm: $("#frm-valuation-details"),
                    ddlZone: $(".ddl-zone"),
                    txtLandAreaKt: $(".txt-land-area-kt"),
                    txtLandAreaCh: $(".txt-land-area-ch"),
                    txtLandAreaSft: $(".txt-land-area-sft"),
                    ddlNatureOfUser: $(".ddl-nature-of-use"),
                    txtBldgAreaSft: $(".txt-bldg-area-sft"),
                    txtPlinthSft: $(".txt-plinth-sft"),
                    ddlConstruction: $(".ddl-construction"),
                    txtLandCost: $(".txt-land-cost"),
                    txtAge: $(".txt-age"),
                    txtReadOnlyAnnualValuation: $(".txt-readonly-annual-valuation"),
                    txtReadOnlyQtrPtax: $(".txt-readonly-qtr-ptax"),
                    txtReadOnlyQtrSchrg: $(".txt-readonly-qtr-schrg"),
                    ddlHoldingTypeGroup: $(".ddl-hodling-type-group"),
                };
            }
            this.validate = function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().ddlZone.prop('name')] = { required: true };
                vldObj.rules[_doms().txtLandAreaKt.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().txtLandAreaCh.prop('name')] = { required: true, number: true, range: [0, 15] };
                vldObj.rules[_doms().txtLandAreaSft.prop('name')] = { required: true, number: true, range: [0, 44] };
                vldObj.rules[_doms().ddlNatureOfUser.prop('name')] = { required: true };
                vldObj.rules[_doms().txtBldgAreaSft.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().txtPlinthSft.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().ddlConstruction.prop('name')] = { required: true };
                vldObj.rules[_doms().txtLandCost.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().txtAge.prop('name')] = { required: true, number: true };


                //set error messages
                vldObj.messages[_doms().ddlZone.prop('name')] = { required: "Please enter Ward." };
                vldObj.messages[_doms().txtLandAreaKt.prop('name')] = { required: "Please enter land area kt.", number: "Data should be numeric" };
                vldObj.messages[_doms().txtLandAreaCh.prop('name')] = { required: "Please enter land area ch.", number: "Data should be numeric", range: "Value sould be between 0 to 15" };
                vldObj.messages[_doms().txtLandAreaSft.prop('name')] = { required: "Please enter land area sft.", number: "Data should be numeric", range: "Value sould be between 0 to 44" };
                vldObj.messages[_doms().ddlNatureOfUser.prop('name')] = { required: "Please select nature of use" };
                vldObj.messages[_doms().txtBldgAreaSft.prop('name')] = { required: "Please enter bldg. area sft", number: "Data should be numeric" };
                vldObj.messages[_doms().txtPlinthSft.prop('name')] = { required: "Please enter plinth sft", number: "Data should be numeric" };
                vldObj.messages[_doms().ddlConstruction.prop('name')] = { required: "Please select construction" };
                vldObj.messages[_doms().txtLandCost.prop('name')] = { required: "Please enter land cost", number: "Data should be numeric" };
                vldObj.messages[_doms().txtAge.prop('name')] = { required: "Please enter age", number: "Data should be numeric" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            };
            this.getFormData = function () {
                return {
                    ZoneId: _doms().ddlZone.val(),
                    NatureOfUseId: _doms().ddlNatureOfUser.val(),
                    ConstructionId: _doms().ddlConstruction.val(),
                    LandArea_KT: _doms().txtLandAreaKt.val(),
                    LandArea_CH: _doms().txtLandAreaCh.val(),
                    LandArea_SFT: _doms().txtLandAreaSft.val(),
                    BldgArea_SFT: _doms().txtBldgAreaSft.val(),
                    Plinth_SFT: _doms().txtPlinthSft.val(),
                    LandCost: _doms().txtLandCost.val(),
                    Age: _doms().txtAge.val(),
                    HoldingTypeGroupId: _doms().ddlHoldingTypeGroup.val()
                }
            };
            this.feedCalculatedValues = function (data) {
                _doms().txtReadOnlyAnnualValuation.val(data.AnnualValuation);
                _doms().txtReadOnlyQtrPtax.val(data.Qtr_Ptax);
                _doms().txtReadOnlyQtrSchrg.val(data.Qtr_Schrg);
                _doms().txtLandCost.val(data.LandCost);

            };
        };
        this.AdditionalHodlingDetails = new function () {
            var _doms = function () {
                return {
                    frm: $("#frm-sec-holding-details"),
                    txtMouajaName: $(".txt-mauja-name"),
                    txtKhatianNoRs: $(".txt-khatian-no-rs"),
                    txtKhatianNoLr: $(".txt-khatian-no-lr"),
                    txtDagNoRs: $(".txt-dag-no-rs"),
                    txtDagNoLr: $(".txt-dag-no-lr"),
                    ddlHoldingArea: $(".ddl-holding-area"),
                    txtDeedNo: $(".txt-deed-no"),
                    ddlDeedType: $(".ddl-deed-type"),
                    txtDeedDate: $(".txt-deed-date"),
                    chkLiftTag: $(".lst-tags .lift"),
                    chkDrainage: $(".lst-tags .drainage"),
                    chkToilets: $(".lst-tags .toilets"),
                    chkElecticity: $(".lst-tags .electricity"),
                    chkWater: $(".lst-tags .Water"),
                    txtBoroughNo: $(".txt-borough-no")
                }
            };
            this.validate = function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().txtMouajaName.prop('name')] = { required: true };
                vldObj.rules[_doms().txtKhatianNoRs.prop('name')] = { required: true };
                vldObj.rules[_doms().txtKhatianNoLr.prop('name')] = { required: true };
                vldObj.rules[_doms().txtDagNoRs.prop('name')] = { required: true };
                vldObj.rules[_doms().txtDagNoLr.prop('name')] = { required: true };
                vldObj.rules[_doms().ddlHoldingArea.prop('name')] = { required: true };
                vldObj.rules[_doms().txtDeedNo.prop('name')] = { required: true };
                vldObj.rules[_doms().ddlDeedType.prop('name')] = { required: true };
                vldObj.rules[_doms().txtDeedDate.prop('name')] = { required: true };


                //set error messages
                vldObj.messages[_doms().txtMouajaName.prop('name')] = { required: "Please enter Mouja Name." };
                vldObj.messages[_doms().txtKhatianNoRs.prop('name')] = { required: "Please enter Khatian No RS" };
                vldObj.messages[_doms().txtKhatianNoLr.prop('name')] = { required: "Please enter Khatian No LR" };
                vldObj.messages[_doms().txtDagNoRs.prop('name')] = { required: "Please enter Dag No RS" };
                vldObj.messages[_doms().txtDagNoLr.prop('name')] = { required: "Please select Dag No LR" };
                vldObj.messages[_doms().ddlHoldingArea.prop('name')] = { required: "Please enter Holding Area" };
                vldObj.messages[_doms().txtDeedNo.prop('name')] = { required: "Please enter Deed no" };
                vldObj.messages[_doms().ddlDeedType.prop('name')] = { required: "Please select Deed Type" };
                vldObj.messages[_doms().txtDeedDate.prop('name')] = { required: "Please enter Deed Date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            };
            this.getFormData = function () {
                var ff = _doms();
                return {
                    MaujaName: _doms().txtMouajaName.val(),
                    KhatianNo_RS: _doms().txtKhatianNoRs.val(),
                    KhatianNo_LR: _doms().txtKhatianNoLr.val(),
                    DaagNo_RS: _doms().txtDagNoRs.val(),
                    DaagNo_LR: _doms().txtDagNoLr.val(),
                    DeedNo: _doms().txtDeedNo.val(),
                    DeedDate:convertTodate('dd/mm/yyyy', _doms().txtDeedDate.val()),
                    DeedTypeId: _doms().ddlDeedType.val(),
                    HoldingAreaCode: _doms().ddlHoldingArea.val(),
                    Tag_Lift: _doms().chkLiftTag.prop('checked'),
                    Tag_Drainage: _doms().chkDrainage.prop('checked'),
                    Tag_Toilets: _doms().chkToilets.prop('checked'),
                    Tag_Electricity: _doms().chkElecticity.prop('checked'),
                    Tag_Water: _doms().chkWater.prop('checked'),
                    BoroughNo: _doms().txtBoroughNo.val(),
                }
            };
        };
        this.OtherDetails = new function () {
            var _doms = function () {
                return {
                    frm: $("#frm-other-details"),
                    txtPhoneNo: $(".txt-primary-phone-no"),
                    txtEmailId: $(".txt-primary-email-id"),
                    txtPorCosingFees: $(".txt-pro-cosing-fees"),
                    txtOtherFees: $(".txt-other-fees")
                };
            }
            this.validate = function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().txtPorCosingFees.prop('name')] = {  number: true };
                vldObj.rules[_doms().txtOtherFees.prop('name')] = {  number: true };

                //set error messages
                vldObj.messages[_doms().txtPorCosingFees.prop('name')] = { number: "Data should be numeric" };
                vldObj.messages[_doms().txtOtherFees.prop('name')] = { number: "Data should be numeric" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            };
            this.getFormData = function () {
                return {
                    PrimaryPhNo: _doms().txtPhoneNo.val(),
                    PrimaryEmail: _doms().txtEmailId.val(),
                    ProCosingFees: (isNaN(_doms().txtPorCosingFees.val()) ? 0 : _doms().txtPorCosingFees.val()),
                    OtherFees:(isNaN(_doms().txtOtherFees.val()) ? 0 : _doms().txtOtherFees.val()),
                }
            };
        };
    };
    var _extenions = {
        extendHoldingDetails: function () {
            $(document).off('click', ".btn-add-assesse");
            $(document).on("click", ".btn-add-assesse", function () {
                var assesseName = $(".txt-assessee-name").val();
                if (assesseName.trim()) {
                    $(".txt-assessee-name").val('');
                    $(".list-holding-names").append("<li data-name='" + assesseName + "' class='list-group-item'>" + assesseName + "<a href='javascript:void(0);' class='pull-right remove-assesse '>X</a></li>");
                }
            });
            $(document).on('click', ".list-holding-names .remove-assesse", function () {
                $(this).closest('li').remove();
            });
            //$(document).on('change', '.ddl-hodling-type-group', function () {
            //    $(".ddl-holding-type option").not(':eq(0)').remove();
            //    if ($(this).val() == '') {
            //        return;
            //    }
            //    $.ajax({
            //        type: "POST",
            //        url: "/Municipality/Assessment/GetHoldingTypes",
            //        data: JSON.stringify({ HoldingGroupTypeId: $(this).val() }),
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: function (r) {
            //            if (r.Status == 200) {

            //                $.each(r.Data, function (index, value) {
            //                    $(".ddl-holding-type").append('<option value="' + value.HoldingTypeId + '">' + value.HoldingTypeDesc + '</option>')
            //                });
            //            }
            //        },
            //        failure: function (response) {
            //            alert(response);
            //        }
            //    });
            //});
            $(document).on('focus', ".txtWard", function () {
                $(this).autocomplete({
                    source: function (request, response) {
                        var data = { Ward: request.term };
                        $(".hddn-Ward").val('');
                        if (!request.term) {
                            return;
                        }
                        $.ajax({
                            type: "POST",
                            url: "/Municipality/Assessment/GetWards",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (Response) {
                                if (Response.Status === 200) {
                                    var serverResponse = Response.Data;
                                    var userDataAutoComplete = [];
                                    if ((serverResponse).length > 0) {
                                        $.each(serverResponse, function (index, item) {
                                            userDataAutoComplete.push({
                                                value: item.WardID,
                                                label: item.WardName
                                            });
                                        });
                                    }
                                    response(userDataAutoComplete);
                                } else {
                                    alertify.error("Error: " + Response.Message);
                                }
                            },
                            failure: function (response) {
                                alert(response);
                            }
                        });
                    },
                    select: function (e, i) {
                        $(".hddn-Ward").val(i.item.value);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            //Resetors.ResetAssesse();
                        }
                    }
                });
            });
            $(document).on('focus', ".txtLocation", function () {
                $(this).autocomplete({
                    source: function (request, response) {
                        var data = { WardId: $(".hddn-Ward").val(), Location: request.term };
                        $(".hddn-Location").val('');
                        if (!request.term || !$(".hddn-Ward").val()) {
                            return;
                        }
                        $.ajax({
                            type: "POST",
                            url: "/Municipality/Assessment/GetLocations",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (Response) {
                                if (Response.Status === 200) {
                                    var serverResponse = Response.Data;
                                    var userDataAutoComplete = [];
                                    if ((serverResponse).length > 0) {
                                        $.each(serverResponse, function (index, item) {
                                            userDataAutoComplete.push({
                                                value: item.LocationID,
                                                label: item.LocationName
                                            });
                                        });
                                    }
                                    response(userDataAutoComplete);
                                } else {
                                    alertify.error("Error: " + Response.Message);
                                }
                            },
                            failure: function (response) {
                                alert(response);
                            }
                        });
                    },
                    select: function (e, i) {
                        $(".hddn-Location").val(i.item.value);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            //Resetors.ResetAssesse();
                        }
                    }
                });
            });
            $(document).on('focus', ".txt-holding-no", function () {
                if (valTag() === "L") {
                    $(this).off('keyup');
                    $(this).on('keyup', function () {
                        $(".hddn-holding-no").val($(this).val());
                    });
                    
                } else {
                    $(this).autocomplete({
                        source: function (request, response) {
                            var data = { WardId: $(".hddn-Ward").val(), LocationID: $(".hddn-Location").val(), Holding: request.term };
                            $(".hddn-holding-no").val('');
                            if (!request.term || !$(".hddn-Ward").val() || !$(".hddn-Location").val()) {
                                return;
                            }
                            $.ajax({
                                type: "POST",
                                url: "/Municipality/Assessment/GetHoldings",
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (Response) {
                                    if (Response.Status === 200) {
                                        var serverResponse = Response.Data;
                                        var userDataAutoComplete = [];
                                        if ((serverResponse).length > 0) {
                                            $.each(serverResponse, function (index, item) {
                                                userDataAutoComplete.push({
                                                    value: item.HoldingNo,
                                                    label: item.HoldingNo
                                                });
                                            });
                                        }
                                        response(userDataAutoComplete);
                                    } else {
                                        alertify.error("Error: " + Response.Message);
                                    }
                                },
                                failure: function (response) {
                                    alert(response);
                                }
                            });
                        },
                        select: function (e, i) {
                            //$(".hddn-Location").val(i.item.value);
                            $(".hddn-holding-no").val(i.item.value);
                        },
                        change: function (e, i) {
                            if (!i.item) {
                                //Resetors.ResetAssesse();
                            }
                        }
                    });
                }
            });

        },
        extendValuationDetails: function () {
            var evtHandlers = {
                onChangeValuation: function () {
                    if (form.HoldingDetails.validate() && form.ValuationDetails.validate()) {
                        var holdingDetails = form.HoldingDetails.getFormData();
                        var valuationData = form.ValuationDetails.getFormData();
                        $.ajax({
                            type: "POST",
                            url: "/Municipality/Assessment/CalCulateValuationDetails",
                            data: JSON.stringify(valuationData),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (r) {
                                if (r.Status === 200) {
                                    form.ValuationDetails.feedCalculatedValues(r.Data);
                                } else {
                                    alertify.error(r.Message);
                                }
                            },
                            failure: function (response) {
                                alertify.error(response);
                            }
                        });
                    }
                }
            };
            $(document).on('change', ".ddl-zone", function () {
                $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
                evtHandlers.onChangeValuation();
            });
            $(document).on('change', ".ddl-nature-of-use", function () {
                $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
                evtHandlers.onChangeValuation();
            });
            $(document).on('change', ".ddl-construction", function () {
                $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
                evtHandlers.onChangeValuation();
            });
            $(document).on('change', ".txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-land-cost,.txt-age", function () {
                evtHandlers.onChangeValuation();
            });
        },
        extendAdditionHodlingDetails: function () {
            $(document).on('focus', '.txt-deed-date', function () {
                $(this).datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    closeText: 'X',
                    showAnim: 'drop',
                    changeYear: true,
                    changeMonth: true,
                    duration: 'medium'
                });
            });
        }
    };
    var _eventHandlers = {
        OnSaveChange: function () {
            var hodlingDetails = form.HoldingDetails;
            var valuationDetails = form.ValuationDetails;
            var additionalDetails = form.AdditionalHodlingDetails;
            var otherDetails = form.OtherDetails;
            if (hodlingDetails.validate() && valuationDetails.validate()  && otherDetails.validate()) {
                alertify.confirm('Please confirme me!', 'Are your sure to save?', function () {
                    var payload = {
                        FinYear: getFinYear(),
                        Qtr:getQtr(),
                        _YearID:yearId(),
                        AssessmentValTag: valTag(),
                        SubAction: subAction(),
                        _HoldingDetails: hodlingDetails.getFormData(),
                        _ValuationDetails: valuationDetails.getFormData(),
                        _OtherDetails: otherDetails.getFormData(),
                        _AdditionalHoldingDetails:additionalDetails.getFormData()
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/SaveAssessmentData",
                        data: JSON.stringify(payload),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                alertify.alert(Response.Message);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });

                }, function () { })
            }
        }
    };
    var BindEvents = new function () {
        $(document).on("click", ".btn-save", _eventHandlers.OnSaveChange);
    };
    (function () {
        form = new AssesseeForm();
        _extenions.extendHoldingDetails();
        _extenions.extendValuationDetails();
        _extenions.extendAdditionHodlingDetails();
    }());
};
