﻿


$(function () {
    //var frm = $('#scoreBelt');
    //var score = $('#score');
    //var belt = $('#belt');
    //app.validation();



    app.ddlMunicipality();
    app.getYear();
    
    app.Scoretype();


    app.belTypeValue();
    //app.GetAssmtScoreSheet();
    $('#btnGo').click(function () {
        //if (!frm.valid()) {
        //    return false;
        //}
        //app.validation();

        $('#tbodyScoreSheet').html('');
        $('#tbodyBeltSheet').html('');

        if (!app.validation()) {
            return false;
        } else {
            app.showEffecdate();
            app.GetAssmtScoreSheet();
            app.GetAssmtBeltSheet();

            app.getAllScoreSheets();
            app.getAllBeltSheets();
        }
        
        

    });
    $('#btnSubmit').click(function () {
        app.saveAssmtScoreSheet();
        app.saveAssmtBeltSheet();
    });
    $('#ddlyear').change(function () {
        //alert('hI')
        //app.showEffecdate();
    });
    app.accordion();

    //////////////////////////////////////////
    $("#btnScoreAdd").on("click", function () {
        app.addNewRow();
    });

    $(document).on('click', '.ibtnEditUpdScore', function () {
        //app.scoreValidation();
        //app.saveScoreSheets(this);
    });

    $("table.table-bordered").on("click", ".ibtnDelCanScore", function (event) {
        //app.cancelRow(this);
        
    });

    /////////////////////////
    $('#btnBeltAdd').on('click', function () {
        app.addNewBelt();
    });

})

var app = {
    counter:0,
    options: [],
    optionResponseScoreType: [],
    optionScore:[],
    optionResponseBeltType:[],
    optionResponse: [],
    Scoretype: function () {
        $.ajax({
            url: '/Administration/Score/GetScoreType',
            type: 'POST',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //app.optionResponseScoreType = response.Data;

                //$(response.Data).each(function (i, element) {
                //    app.ScoreTypeOption.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
                //});
                app.optionResponseScoreType.push('<option value="0">-- Select --</option>');
                app.optionResponseBeltType.push('<option value="select">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    if (element.ScoreBelt === "S") {
                        app.optionResponseScoreType.push('<option value=' + element.ScoreBeltTypeId + '>' + element.ScoreBeltTypeDesc + '</option>');

                    } else {
                        app.optionResponseBeltType.push('<option value=' + element.ScoreBeltTypeId+'>'+element.ScoreBeltTypeDesc+'</option>');
                    }
                });
                console.log(app.optionResponseScoreType);
            },
            error: function () { }
        });
    },
    scoreByTypeID: function (id,callback) {
        var tid = $('#scoreType_' + id + ' option:selected').val();
        var obj = { "typeId": tid }

        $.ajax({
            url: '/Administration/Score/GetScoreByTypeId',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.optionScore.length = 0;
                $(response.Data).each(function (i, element) {
                    app.optionScore.push('<option value=' + element.ScoreDesc + '>' + element.ScoreCode + '</option>');
                });
                console.log(app.optionScore.join(''));
                if (callback) {  /* testCallback  */
                    callback(app.optionScore);  /* testCallback(data[num])  */
                }
            },
            error: function () { }
        });
    },
    belTypeValue: function () {
        $.ajax({
            url: '/Administration/Score/GetBeltTypevalue',
            type: 'POST',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.optionResponse = response.Data;
                app.options.push('<option value="select">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    app.options.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
                });
                //console.log(app.options);
            },
            error: function () { }
        });
    },
    showEffecdate: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Administration/Score/GetEffectiveDate',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(JSON.stringify(response));
                //console.log(response.Data)
                //console.log(JSON.stringify(response.Data));
                //console.log(JSON.parse(response.Data));
                //app.getScoreSheet(response.Data);
                //var fdate = response.Data[0][element.EffectiveDateTo]
                if (response.Data.length > 0) {
                    console.log(response.Data);
                    console.log(response.Data[0]["EffectiveDateForm"]);
                    console.log(response.Data[0]["EffectiveDateTo"]);
                    $('#fromdate').html(CONVERTER.Date.convertCsToJsDate(response.Data[0]["EffectiveDateForm"]).format('dd/mm/yy'));
                    $('#todate').html(new Date(parseInt((response.Data[0]["EffectiveDateTo"]).replace(/[^0-9 +]/g, ''))).format('dd/mm/yy'));
                }
                
            },
            error: function () { }
        });
    },
    GetAssmtBeltSheet: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Administration/Score/GetAssmtBeltSheet',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                app.beltData(response.Data);
                //app.BeltDataDyn(response.Data);
                //app.BeltdataDynsDirectResponse(response.Data);
            },
            error: function () { }
        });
    },
    beltData: function (data) {
        $('#AssmtBeltSheet').empty();
        $('#AssmtBeltSheet').append('<thead><tr>'
            + '<th style=display:none;>BeltSheetID </th>'
            + '<th>Score Belt Type </th>'
            + '<th>From(>) K </th>'
            + '<th>Upto(<=) K </th>'
            +' <th>% Consider </th>'
            +' </tr></thead>');
        $(data).each(function (i, element) {
                $('#AssmtBeltSheet').append('<tbody><tr>'
                + '<td style=display:none;>' + element.BeltSheetID + '</td>'
                + '<td>' + element.ScoreBeltTypeDesc + '</td>'
                    + '<td> <input type=text style="width:50%; text-align:right;" class=txtbeltfrom value=' + element.FromRange + '></td>'
                    + '<td> <input type=text style="width:50%; text-align:right;" class=txtbeltto value=' + element.ToRange + '></td>'
                + '<td><select id=per_' + element.BeltSheetID +' class=txtbeltval></td>'
                    + '</tr></tbody>');
            $('#per_' + element.BeltSheetID).html(app.options.join(''));
            $('#per_' + element.BeltSheetID).val(element.BeltValue == '' ? 0 : element.BeltValue);
        });
        
    },
    BeltDataDyn: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>BeltSheetID </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>From(>) K </th>');
        Html.push('<th>Upto(<=) K </th>');
        Html.push('<th>% Consider </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        var optHtmlObj = [];
        $(data).each(function (i, element) {

            Html.push('<tbody><tr>')
            Html.push('<td style=display:none;>' + element.BeltSheetID + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            //Html.push('<td>' + element.FromRange+' ' +element.BeltUnit + '</td>');
            //Html.push('<td>' + element.ToRange + ' ' + element.BeltUnit + '</td>');
            Html.push('<td> <input type=text class=txtbeltfrom value=' + element.FromRange + ' ></td>');
            Html.push('<td> <input type=text class=txtbeltto value=' + element.ToRange + ' ></td>');
            //Html.push('<td>' + element.BeltValue + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            //Html.push('<td> <input type=text class=txtbeltval value=' + element.BeltValue + '  ></td>');


            var optHTML = [];
            optHtmlObj = $.parseHTML(app.options.join(''));
            $(optHtmlObj).each(function (x, opt) {
             //   alert($(opt).val());
                if ($(opt).val() == element.BeltValue) {
                   // $(opt).attr('selected', 'true');
                    optHTML.push('<option value=' + element.BeltValue + ' selected>' + $(opt).html() +'</option>')
                } else {
                    optHTML.push('<option value=' + element.BeltValue + '>' + $(opt).html() + '</option>')
                }
            })

            Html.push('<td><select  id=per_' + element.BeltSheetID + ' class=txtbeltval>' + optHTML.join('') +'</select></td>');
            Html.push('</tr></tbody>');
           
            //alert(element.BeltValue == '' ? 0 : element.BeltValue);
          //  $('#per_' + element.BeltSheetID).html();

            //var ddlId = '#per_' + element.BeltSheetID;
            //ddlId = $(ddlId);
            //$.each(app.options, function (ind, val) {
            //    $(ddlId).append(val);
            //})

            //$(ddlId).html(app.options.join(''));
            //$(ddlId).val(element.BeltValue);
            //Html.clone();
        });
        //$('#AssmtBeltSheet').html(Html.join(''));
        $('#AssmtBeltSheet').html(Html.join(''));
    },
    BeltdataDynsDirectResponse: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>BeltSheetID </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>From(>) K </th>');
        Html.push('<th>Upto(<=) K </th>');
        Html.push('<th>% Consider </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        $(data).each(function (i, element) {
            Html.push('<tbody><tr>')
            Html.push('<td style=display:none;>' + element.BeltSheetID + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            //Html.push('<td>' + element.FromRange+' ' +element.BeltUnit + '</td>');
            //Html.push('<td>' + element.ToRange + ' ' + element.BeltUnit + '</td>');
            Html.push('<td> <input type=text class=txtbeltfrom value=' + element.FromRange + ' ></td>');
            Html.push('<td> <input type=text class=txtbeltto value=' + element.ToRange + ' ></td>');
            //Html.push('<td>' + element.BeltValue + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            //Html.push('<td> <input type=text class=txtbeltval value=' + element.BeltValue + '  ></td>');

            var optHTML = [];
            //app.options.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
            $(app.optionResponse).each(function (i, elem) {
                if (elem.ValTag == element.BeltValue) {
                    optHTML.push('<option selected value=' + element.BeltValue + '>' + elem.ValDesc + '</option>');
                } else {
                    optHTML.push('<option value=' + elem.ValTag + '>' + elem.ValDesc + '</option>');
                }
            })

            Html.push('<td><select  id=per_' + element.BeltSheetID + ' class=txtbeltval>' + optHTML.join('') + '</select></td>');
            Html.push('</tr></tbody>');

            //alert(element.BeltValue == '' ? 0 : element.BeltValue);
            //  $('#per_' + element.BeltSheetID).html();

            //var ddlId = '#per_' + element.BeltSheetID;
            //ddlId = $(ddlId);
            //$.each(app.options, function (ind, val) {
            //    $(ddlId).append(val);
            //})

            //$(ddlId).html(app.options.join(''));
            //$(ddlId).val(element.BeltValue);
            //Html.clone();
        });
        //$('#AssmtBeltSheet').html(Html.join(''));
        $('#AssmtBeltSheet').html(Html.join(''));
    },
    GetAssmtScoreSheet: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();
        $.ajax({
            url: '/Administration/Score/GetAssmtScoreSheet',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year+ "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(JSON.stringify(response));
                //console.log(response.Data)
                //console.log(JSON.stringify(response.Data));
                //console.log(JSON.parse(response.Data));
                app.getScoreSheet(response.Data);
            },
            error: function () { }
        });
    },
    saveAssmtScoreSheet: function () {
        var arr = [];
        var data = $('#AssmtScorewSheet tr').not(":first")
        //console.log(data);
        $(data).each(function (i, item) {
            var obj = {};

            //obj.ScoreSheetID = item.find('td').eq(0).text();
            obj.ScoreSheetID = $(item).children('td').eq(0).html();
            //obj.MunicipalityName = $(item).children('td').eq(1).html();
            //obj.YearId = $(item).children('td').eq(2).html();
            //obj.ScoreBeltTypeDesc = $(item).children('td').eq(3).html();
            //obj.ScoreCode = $(item).children('td').eq(4).html();
            //obj.ScoreDesc = $(item).children('td').eq(5).html();
            //obj.ScoreValue = $(item).children('td').eq(6).html();
            
            obj.ScoreValue = $(item).find('td').find('.txtscoreval').val();



            //obj.ScoreValue = $(item).find('td:eq(6) input[type="text"]').val();
            //alert($(item).find('td').find('.txtvai').val());

            //alert(i);
            arr.push(obj);
            //arr[i] = obj;
        });
        //console.log(arr);

        $.ajax({
            url: '/Administration/Score/SaveAssmtScoreSheet',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType:'json',
            success: function (response) {
                console.log(response);
            },
            error: function () { }
        });
    },
    saveAssmtBeltSheet: function () {
        
        var arr = [];
        var data = $('#AssmtBeltSheet tr').not(":first")
        $(data).each(function (i, element) {
            var obj = {};
            
            obj.BeltSheetID = $(element).children('td').eq(0).html();
            obj.From = $(element).find('td').find('.txtbeltfrom').val();
            obj.To = $(element).find('td').find('.txtbeltto').val();
            obj.BeltValue = $(element).find('td').find('.txtbeltval').val();
            arr.push(obj);
        });
        console.log(arr);
        $.ajax({
            url: '/Administration/Score/SaveAssmtBeltSheet',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response)
            },
            error: function () { }
        });

    },
    getScoreSheet: function (data) {
        var Html = [];
        Html.push('<thead>');
        Html.push('<tr>');
        Html.push('<th style=display:none;>ScoreSheetId </th>');
        //Html.push('<th>EffectiveDateForm</th>');
        //Html.push('<th>EffectiveDateTo</th>');
        Html.push('<th style=display:none;>MunicipalityName </th>');
        Html.push('<th style=display:none;>YearId </th>');
        Html.push('<th>Score Belt Type </th>');
        Html.push('<th>Score </th>');
        Html.push('<th>Description </th>');
        Html.push('<th>Value </th>');
        Html.push('</tr>');
        Html.push('</thead>');
        $(data).each(function (i, element) {
            //CONVERTER.Date.convertCsToJsDate("/Date(1522519200000)/").format('dd/mm/yy')
            //element.EffectiveDateTo
            Html.push('<tr>')
            Html.push('<td style=display:none;>' + element.ScoreSheetID + '</td>');
            //Html.push('<td>' + CONVERTER.Date.convertCsToJsDate(element.EffectiveDateForm).format('dd/mm/yy') + '</td>');
            //Html.push('<td>' + new Date( parseInt((element.EffectiveDateTo).replace(/[^0-9 +]/g, ''))).format('dd/mm/yy') + '</td>');
            Html.push('<td style=display:none;>' + element.MunicipalityName + '</td>');
            Html.push('<td style=display:none;>' + element.YearId + '</td>');
            Html.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            Html.push('<td>' + element.ScoreCode + '</td>');
            Html.push('<td>' + element.ScoreDesc + '</td>');
            //Html.push('<td>' + element.ScoreValue + '</td>');
            //Html.push('<td> <input type=text value=' + element.ScoreValue + '  /></td>');
            Html.push('<td> <input style="width:50%; text-align:right;" type=text class=txtscoreval value=' + element.ScoreValue + '  ></td>');
            Html.push('</tr>');
        });
        $('#AssmtScorewSheet').html(Html.join(''));
    },
    ddlMunicipality: function () {
        $.ajax({
            url: '/Administration/Score/GetAllMunicipalites',
            type: 'GET',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
            },
            error: function () { }
        });
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('#ddlMunicipality');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="0">--select--</option>');
        $(data).each(function (index, value) {
        //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
    },
    getYear: function () {
        $.ajax({
            url: '/Administration/Score/GetYear',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: '{}',
            dataType: 'json',
            success: function (response) { app.populateYear(response.Data) },
            error: function () { }
        });
    },
    populateYear: function (data) {
        var ctlr_ddlYear = $('#ddlyear');
        ctlr_ddlYear.empty();
        ctlr_ddlYear.append('<option value="0">--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlYear.append('<option value=' + item.YearDesc + '>' + item.YearDesc + '</option>');
        })
    },
    accordion: function () {
       
            $("#accordion").accordion({
                collapsible: true
            });
        
    },
    validation1: function () {
        $.validator.addMethod('municipality', function (value, element) {
            return value != "0";
        });

        $.validator.addMethod('year', function (value, element) {
            return value != "0";
        })

        $('#scoreBelt').validate({
            rules: {
                ddlMunicipality: { municipality: true },
                ddlyear: { year:true}

            },
            messages: {
                ddlMunicipality: { municipality:"Please Select Municipality"},
                ddlyear: { year:"Please Select Year"}
            }
        });


    },
    validation: function () {
        $('.error').text('');
        var valid = true;
            
        $('form#scoreBelt select').each(function () {
            var $this = $(this);
            if ($this.val() == '0') {
                var attrName = $this.prop('name');
                //var info = $this.prop('data-info');
                var id = $this.prop('id');
                $('#lbl_' + id).text(attrName);
                valid = false;
            }
        });
        return valid;
    },
    scoreValidation: function () {
        var valid = true;
        $('.error').text('');
        $('form#score input:text,form#score select').each(function () {
           
            var $this = $(this);
            if ($this.val()=='0' || $this.val()=='') {
                var attrName = $this.prop('name');
                var id = $this.prop('id');
                $('#lbl_' + id).text(attrName);
                valid = false;
            }
        });
        return valid;
    },
    beltValidation: function () {
        var valid = true;
        $('.error').text('');
        $('form#belt input:text,form#belt select').each(function () {
            var $this = $(this);
            if ($this.val() == 'select' || $this.val() == '') {
                var attrName = $this.prop('name');
                var id = $this.prop('id');
                $('#lbl_' + id).text(attrName);
                valid = false;
            }
        });
        return valid;
    },

    ////////////////////////////////

    addNewRow: function () {
        var id = new Date().getTime();
        if ($("#tbodyScoreSheet").children().length < 1) {
            var firstChildHtml = [];

            firstChildHtml.push('<tr>');
            firstChildHtml.push('<td><select id="scoreType_' + id + '" name="Please Select Type" onchange="app.ddltypechange(this,\'' + id + '\');" class="scoreType form-control">' + app.optionResponseScoreType.join('') + '</select><label for="lbl" id="lbl_scoreType_' + id + '"  generated="true" class="error"></label><input type="hidden" id="hdn_' + id + '" name="custId" value="0"></td>');
            firstChildHtml.push('<td><select id="score_' + id + '" name="Please Select Score" onchange="app.onscoreChange(this,\'' + id + '\');" class="score form-control"></select><label for="lbl" id="lbl_score_' + id +'"  generated="true" class="error"></label></td>');

            ////firstChildHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

            firstChildHtml.push('<td><input type="text" id="txtDesc_' + id +'" name="Please Enter Description" class="desc form-control"><label for="lbl" id="lbl_txtDesc_'+id+'"  generated="true" class="error"></label></td>');
            firstChildHtml.push('<td><input type="text" id="txtValue_' + id + '" name="Please Enter Value" class="value form-control"><label for="lbl" id="lbl_txtValue_' + id + '"  generated="true" class="error"></label></td>');
            firstChildHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdScoreSheet(this,\'' + id + '\')"  value="Update"></td>');
            firstChildHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelScore(this)" value="Cancel"></td>');
            firstChildHtml.push('</tr>');
            $("#tbodyScoreSheet").append(firstChildHtml.join(''));
        } else {
            var firstChild = $("#tbodyScoreSheet").children()[0];

            var siblingHtml = [];

            siblingHtml.push('<tr>');

            siblingHtml.push('<td><select id="scoreType_' + id + '" name="Please Select Type" onchange="app.ddltypechange(this,\'' + id + '\');" class="scoreType form-control">' + app.optionResponseScoreType.join('') + '</select><label for="lbl" id="lbl_scoreType_' + id + '"  generated="true" class="error"></label><input type="hidden" id="hdn_' + id + '" name="custId" value="0"></td>');
            siblingHtml.push('<td><select id="score_' + id + '"  name="Please Select Score" onchange="app.onscoreChange(this,\'' + id + '\');" class="score form-control"></select><label for="lbl" id="lbl_score_' + id +'"  generated="true" class="error"></label></td>');

            //siblingHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

            siblingHtml.push('<td><input type="text" id="txtDesc_' + id + '" name="Please Enter Description" class="desc form-control"><label for="lbl" id="lbl_txtDesc_' + id +'"  generated="true" class="error"></label></td>');
            siblingHtml.push('<td><input type="text" id="txtValue_' + id + '" name="Please Enter Value" class="value form-control" /><label for="lbl" id="lbl_txtValue_' + id +'"  generated="true" class="error"></label></td>');
            siblingHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdScoreSheet(this,\'' + id + '\')" value="Update"></td>');
            siblingHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelScore(this)"  value="Cancel"></td>');
            siblingHtml.push('</tr>');

            $(firstChild).before(siblingHtml.join(''));
        }
    },

    cancelRow: function (ref) {
        $(ref).closest("tr").remove();
        //counter -= 1
        if ($('#myTable tbody tr').length == 0) {
        }
    },

    onCancelScore: function (ref) {
        $(ref).closest('tr').remove();
    },


    saveScoreSheets: function (ref) {
        var voucherId = $(ref).closest('tr').find('.scoreType').html();

        var obj = {};
        obj.MunicipalityID = $('#ddlMunicipality option:selected').val();
        obj.YearId = $('#ddlyear option:selected').val();
        obj.ScoreSheetID = 0;
        obj.ScoreBeltTypeId = $(ref).closest('tr').find('.scoreType option:selected').val();
        obj.ScoreCode = $(ref).closest('tr').find('.score option:selected').html();
        obj.ScoreDesc = $(ref).closest('tr').find('.desc').val();
        obj.ScoreValue = $(ref).closest('tr').find('.value').val();

        $.ajax({
            url: '/Administration/Score/SaveUpdateAssmtScoreSheet',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.getAllScoreSheets();
                app.cancelRow(ref);
            },
            error: function () { }
        });


    },
    insUpdScoreSheet: function (ref, id) {
       
      
        var obj = {};
        obj.MunicipalityID = $('#ddlMunicipality option:selected').val();
        obj.YearId = $('#ddlyear option:selected').val();
        obj.ScoreSheetID = $('#hdn_' + id).val();
        //obj.ScoreBeltTypeId = $(ref).closest('tr').find('.scoreType option:selected').val();
        obj.ScoreBeltTypeId = $('#scoreType_'+id).val();
        //obj.ScoreCode = $(ref).closest('tr').find('.score option:selected').html();
        //obj.ScoreCode = $('#score_' + id).val();
        obj.ScoreCode = $('#score_' + id +' option:selected').html();
        //obj.ScoreDesc = $(ref).closest('tr').find('.desc').val();
        obj.ScoreDesc = $('#score_' + id + ' option:selected').val();
        //obj.ScoreDesc = $('#txtDesc_'+ id +' option:selected').val();
        //obj.ScoreValue = $(ref).closest('tr').find('.value').val();
        obj.ScoreValue = $('#txtValue_' + id).val();
        if (app.validation() && app.scoreValidation()) {
            $.ajax({
                url: '/Administration/Score/SaveUpdateAssmtScoreSheet',
                type: 'POST',
                data: JSON.stringify(obj),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    app.getAllScoreSheets();
                    app.cancelRow(ref);
                },
                error: function () { }
            });
        }

    },
    getAllScoreSheets: function () {
       
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();

        $.ajax({
            url: '/Administration/Score/GetAssmtScoreSheets',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                app.popScoreSheets(response.Data);
            },
            error: function () { }
        });
    },
    popScoreSheets: function (data) {
        $('#tbodyScoreSheet').html('');
        var DataHtml = [];

        var optHtmlObj = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr>');
            DataHtml.push('<td style=display:none; class="tdScoreSheetId">' + element.ScoreSheetID+'</td>');
            DataHtml.push('<td style=display:none;  class="tdScoreBeltTypeId">' + element.ScoreBeltTypeId + '</td>');

            var ScoreBeltTypeName;
            optHtmlObj = $.parseHTML(app.optionResponseScoreType.join(''));
            $(optHtmlObj).each(function (x, opt) {
                //   alert($(opt).val());
                if ($(opt).val() == element.ScoreBeltTypeId) {
                    // $(opt).attr('selected', 'true');
                    ScoreBeltTypeName = $(opt).html();
                } 
            })

            DataHtml.push('<td class="">' + ScoreBeltTypeName+'</td>');
            DataHtml.push('<td class="tdScoreCode">' + element.ScoreCode+'</td>');
            DataHtml.push('<td class="tdScoreDesc">' + element.ScoreDesc+'</td>');
            DataHtml.push('<td class="tdScoreValue">'+element.ScoreValue+'</td>');
            DataHtml.push('<td><input type="button" id="ibtnEdtScore" onclick="app.editScore(this);" class="ibtnEdtScore btn btn-md btn-danger" value="Edit"></td>');
            DataHtml.push('<td><input type="button" id="ibtnDelScore" onclick="app.deleteScore(this);" class="ibtnDelScore btn btn-md btn-danger" value="Delete"></td>');
            DataHtml.push('</tr>');

        });
        //$('#tbodyScoreSheet').append(DataHtml.join(''));
        $('#tbodyScoreSheet').html(DataHtml.join(''));
    },
    editScore: function (ref) {
       // $('#ibtnEdtScore').
        var parentTr = $(ref).parents('tr');

        var scoreSheetId = $(parentTr).find('.tdScoreSheetId').html();
        //alert(scoreSheetId);
        var beltTypeId = $(parentTr).find('td:eq(1)').html();
      
        var scorecode = $(parentTr).find('.tdScoreCode').html();
        var scoreDesc = $(parentTr).find('.tdScoreDesc').html();
        var scoreValue = $(parentTr).find('.tdScoreValue').html();

        var firstChildHtml = [];
        var id = new Date().getTime();
      
        firstChildHtml.push('<td><select id="scoreType_' + id + '" name="Please Select Type" onchange="app.ddltypechange(this,\'' + id + '\');" class="scoreType form-control">' + app.optionResponseScoreType.join('') + '</select><input type="hidden" id="hdn_' + id + '" name="custId" value="' + scoreSheetId +'" ><label for="lbl" id="lbl_scoreType_' + id + '"  generated="true" class="error"></label</td>');
        firstChildHtml.push('<td><select id="score_' + id + '" name="Please Select Score" onchange="app.onscoreChange(this,\'' + id + '\');" class="score form-control"></select><label for="lbl" id="lbl_score_' + id +'"  generated="true" class="error"></label></td>');

        ////firstChildHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

        firstChildHtml.push('<td><input type="text" id="txtDesc_' + id + '" value="' + scoreDesc+'" name="Please Enter Description" class="desc form-control"><label for="lbl" id="lbl_txtDesc_' + id + '"  generated="true" class="error"></label></td>');
        firstChildHtml.push('<td><input type="text" id="txtValue_' + id + '" value="' + scoreValue+'" name="Please Enter Value" class="value form-control"><label for="lbl" id="lbl_txtValue_' + id + '"  generated="true" class="error"></label></td>');
        firstChildHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdScoreSheet(this,\'' + id + '\')"  value="Update"></td>');
        firstChildHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelScore(this)" value="Cancel"></td>');
        // ram firstChildHtml.push('</tr>');

        $(parentTr).html(firstChildHtml.join(''));

       

        setTimeout(function () {
            if ($('#scoreType_' + id).length > 0) {
                app.restoreDropDown($('#scoreType_' + id), beltTypeId);
                $('#scoreType_' + id).trigger('onchange');
                var scoreDropDown = $('#score_' + id);
                //alert(scorecode);
                app.dropDownPopulated(scoreDropDown, scoreDesc, app.restoreDropDown);
               // alert('hi');
                //$('#score_' + id).trigger('onchange');
            }
            //$('#score_' + id).trigger('onchange');
        }, 200);
          //$('#score_' + id).trigger('onchange');
    },
    deleteScore: function (ref) {
        var parentTr = $(ref).parents('tr');

        var beltSheetId = $(parentTr).find('.tdScoreSheetId').html();
        var obj = {};
        obj.id = beltSheetId;

        $.ajax({
            url: '/Administration/Score/DeleteScoreSheet',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.getAllScoreSheets();
            },
            error: function () { }
        });
    },

    dropDownPopulated: function (dropdown, selectedVal , callback) {
       var counter = setInterval(function () {
            if ($(dropdown).children().length > 0) {
                clearInterval(counter);
                if (callback) {
                    callback(dropdown, selectedVal);
                }
           }
        }, 20);
        
    },


    restoreDropDown: function (dropDrown, selectedVal) {
        $(dropDrown).find('option').each(function (i, opt) {
            if ($(opt).val() == selectedVal) {
                $(opt).prop('selected', true);
            }
        });
    },




    ddltypechange: function (ref, id) {
        var selectedVal = $(ref).find('option:selected').val();

        var obj = { "typeId": selectedVal }
        $("#score_" + id).html('');
        $.ajax({
            url: '/Administration/Score/GetScoreByTypeId',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var html = [];
                html.push('<option value="0">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    html.push('<option value="' + element.ScoreDesc + '" >' + element.ScoreCode + '</option>');
                });

                $("#score_" + id).html(html.join(''));

                //ram $("#score_" + id).trigger('onchange');
            },
            error: function () { }
        });
    },
    onscoreChange: function (ref, id) {
        try {
            var selectedVal = $(ref).find('option:selected').val();
            //var selectedText = $(ref).find('option:selected').html();

            //$.ajax({
            //    url: '/Administration/Score/GetScoreByTypeId',
            //    type: 'POST',
            //    data: JSON.stringify(obj),
            //    contentType: 'application/json; charset=utf-8',
            //    dataType: 'json',
            //    success: function (response) {
            //        var html = [];
            //        $(response.Data).each(function (i, element) {
            //            html.push('<option value=' + element.ScoreDesc + '>' + element.ScoreCode + '</option>');
            //        });

            //        $("#score_" + id).html(html.join(''));
            //    },
            //    error: function () { }
            //});
            if (selectedVal != "0") {
                $("#txtDesc_" + id).val(selectedVal);

                //$("#score_" + id).trigger('onchange');
            }
               
            else
            {
                 $("#txtDesc_" + id).val("");
            }
               

        } catch (e) {

        }
    },
    callbackforScoreType: function (data) {
        alert(data);
    },

    ////////////////////////////////////////////
    addNewBelt: function () {
        var id = new Date().getTime();
        if ($("#tbodyBeltSheet").children().length < 1) {
            var firstChildHtml = [];

            firstChildHtml.push('<tr>');
            firstChildHtml.push('<td><select id="beltType_' + id + '" name="Please Select Type"  class="scoreType form-control">' + app.optionResponseBeltType.join('') + '</select><label for="lbl" id="lbl_beltType_' + id + '"  generated="true" class="error"></label><input type="hidden" id="hdnBelt_' + id + '" name="custId" value="0"></td>');

            ////firstChildHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

            firstChildHtml.push('<td><input type="text" id="txtFrom_' + id + '" name="Please Enter From" class="desc form-control"><label for="lbl" id="lbl_txtFrom_' + id + '"  generated="true" class="error"></label></td>');
            firstChildHtml.push('<td><input type="text" id="txtUpto_' + id + '" name="Please Enter Upto" class="value form-control"><label for="lbl" id="lbl_txtUpto_' + id + '"  generated="true" class="error"></label></td>');
            firstChildHtml.push('<td><select id="consider_' + id + '" name="Please Select Consider"  class="score form-control">' + app.options.join('')+'</select><label for="lbl" id="lbl_consider_' + id + '"  generated="true" class="error"></label></td>');

            firstChildHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdBeltSheet(this,\'' + id + '\')"  value="Update"></td>');
            firstChildHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelBelt(this)" value="Cancel"></td>');
            firstChildHtml.push('</tr>');
            $("#tbodyBeltSheet").append(firstChildHtml.join(''));
        } else {
            var firstChild = $("#tbodyBeltSheet").children()[0];

            var siblingHtml = [];

            siblingHtml.push('<tr>');

            siblingHtml.push('<td><select id="beltType_' + id + '" name="Please Select Type"  class="scoreType form-control">' + app.optionResponseBeltType.join('') + '</select><label for="lbl" id="lbl_beltType_' + id + '"  generated="true" class="error"></label><input type="hidden" id="hdnBelt_' + id + '" name="custId" value="0"></td>');

            //siblingHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

            siblingHtml.push('<td><input type="text" id="txtFrom_' + id + '" name="Please Enter From" class="desc form-control"><label for="lbl" id="lbl_txtFrom_' + id + '"  generated="true" class="error"></label></td>');
            siblingHtml.push('<td><input type="text" id="txtUpto_' + id + '" name="Please Enter Upto" class="value form-control"><label for="lbl" id="lbl_txtUpto_' + id + '"  generated="true" class="error"></label></td>');
            siblingHtml.push('<td><select id="consider_' + id + '" name="Please Select Consider"  class="score form-control">' + app.options.join('') + '</select><label for="lbl" id="lbl_consider_' + id + '"  generated="true" class="error"></label></td>');

            siblingHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdBeltSheet(this,\'' + id + '\')"  value="Update"></td>');
            siblingHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelBelt(this)" value="Cancel"></td>');
            siblingHtml.push('</tr>');

            $(firstChild).before(siblingHtml.join(''));
        }
    },
    editBelt: function (ref) {
        var parentTr = $(ref).parents('tr');

        var beltSheetId = $(parentTr).find('.tdSBeltSheetID').html();
        //alert(scoreSheetId);
        var beltTypeId = $(parentTr).find('td:eq(1)').html();

        var fromRange = $(parentTr).find('.tdFromRange').html();
        var toRange = $(parentTr).find('.tdToRange').html();
        var beltvalue = $(parentTr).find('.tdBeltValue').html();

        var firstChildHtml = [];
        var id = new Date().getTime();

        //firstChildHtml.push('<tr>');
        firstChildHtml.push('<td><select id="beltType_' + id + '" name="Please Select Type"  class="scoreType form-control">' + app.optionResponseBeltType.join('') + '</select><label for="lbl" id="lbl_beltType_' + id + '"  generated="true" class="error"></label><input type="hidden" id="hdnBelt_' + id + '" name="custId" value="' + beltSheetId+'"></td>');

        ////firstChildHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

        firstChildHtml.push('<td><input type="text" id="txtFrom_' + id + '" value="' + fromRange+'" name="Please Enter From" class="desc form-control"><label for="lbl" id="lbl_txtFrom_' + id + '"  generated="true" class="error"></label></td>');
        firstChildHtml.push('<td><input type="text" id="txtUpto_' + id + '" value="' + toRange+'" name="Please Enter Upto" class="value form-control"><label for="lbl" id="lbl_txtUpto_' + id + '"  generated="true" class="error"></label></td>');
        firstChildHtml.push('<td><select id="consider_' + id + '" name="Please Select Consider"  class="score form-control">' + app.options.join('') + '</select><label for="lbl" id="lbl_consider_' + id + '"  generated="true" class="error"></label></td>');

        firstChildHtml.push('<td><input type="button" class="ibtnUpdScore btn btn-md btn-danger" onclick="app.insUpdBeltSheet(this,\'' + id + '\')"  value="Update"></td>');
        firstChildHtml.push('<td><input type="button" class="ibtnCanScore btn btn-md btn-danger" onclick="app.onCancelBelt(this)" value="Cancel"></td>');
        firstChildHtml.push('</tr>');

        $(parentTr).html(firstChildHtml.join(''));



        setTimeout(function () {
            if ($('#beltType_' + id).length > 0) {
                app.restoreDropDown($('#beltType_' + id), beltTypeId);
                app.restoreDropDown($('#consider_' + id), beltvalue);
            }
        }, 200);

       





    },
    deleteBelt: function (ref) {
        var parentTr = $(ref).parents('tr');

        var beltSheetId = $(parentTr).find('.tdSBeltSheetID').html();
        var obj = {};
        obj.id = beltSheetId;

        $.ajax({
            url: '/Administration/Score/DeleteBeltSheet',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.getAllBeltSheets();
            },
            error: function () { }
        });

    },
    insUpdBeltSheet: function (ref, id) {
        var obj = {};
        obj.MunicipalityID = $('#ddlMunicipality option:selected').val();
        obj.YearId = $('#ddlyear option:selected').val();
        obj.BeltSheetID = $('#hdnBelt_' + id).val();
        //obj.ScoreBeltTypeId = $(ref).closest('tr').find('.scoreType option:selected').val();
        obj.ScoreBeltTypeId = $('#beltType_' + id).val();
        //obj.ScoreCode = $(ref).closest('tr').find('.score option:selected').html();
        //obj.ScoreCode = $('#score_' + id).val();
        obj.FromRange = $('#txtFrom_' + id).val();
        //obj.ScoreDesc = $(ref).closest('tr').find('.desc').val();
        obj.ToRange = $('#txtUpto_' + id).val();
        //obj.ScoreDesc = $('#txtDesc_'+ id +' option:selected').val();
        //obj.ScoreValue = $(ref).closest('tr').find('.value').val();
        obj.BeltValue = $('#consider_' + id+' option:selected').val();
        if (app.validation() && app.beltValidation()) {
            $.ajax({
                url: '/Administration/Score/SaveUpdateAssmtBeltSheet',
                type: 'POST',
                data: JSON.stringify(obj),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    app.getAllBeltSheets();
                    app.onCancelBelt(ref);
                },
                error: function () { }
            });
        }
    },
    onCancelBelt: function (ref) {
        $(ref).closest('tr').remove();
    },
    getAllBeltSheets: function () {
        var municipalityId = $('#ddlMunicipality').val();
        var year = $('#ddlyear').val();

        $.ajax({
            url: '/Administration/Score/GetAssmtBeltSheets',
            type: 'POST',
            data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                app.popBeltSheets(response.Data);
            },
            error: function () { }
        });
    },
    ddltext: function (arr,element) {
        var optHtmlObj = $.parseHTML(arr.join(''));
        //var optHtmlObj = $.parseHTML(app.optionResponseBeltType.join(''));
        var text = '';
        $(optHtmlObj).each(function (x, opt) {
            //   alert($(opt).val());
            //if ($(opt).val() == element.ScoreBeltTypeId) {
            if ($(opt).val() == element) {
              text= $(opt).html();
            }
        })

        return text;
    },
    popBeltSheets: function (data) {
        var DataHtml = [];

        var optHtmlObj = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr>');
            DataHtml.push('<td style=display:none; class="tdSBeltSheetID">' + element.BeltSheetID + '</td>');
            DataHtml.push('<td style=display:none; class="tdScoreBeltTypeId">' + element.ScoreBeltTypeId + '</td>');

            //var ScoreBeltTypeName;
            //optHtmlObj = $.parseHTML(app.optionResponseBeltType.join(''));
            //$(optHtmlObj).each(function (x, opt) {
            //    //   alert($(opt).val());
            //    if ($(opt).val() == element.ScoreBeltTypeId) {
            //        // $(opt).attr('selected', 'true');
            //        ScoreBeltTypeName = $(opt).html();
            //    }
            //})

            DataHtml.push('<td clas="">' + app.ddltext(app.optionResponseBeltType,element.ScoreBeltTypeId) + '</td>');
            DataHtml.push('<td class="tdFromRange">' + element.FromRange + '</td>');
            DataHtml.push('<td class="tdToRange">' + element.ToRange + '</td>');
            DataHtml.push('<td style=display:none; class="tdBeltValue">' + element.BeltValue + '</td>');
            DataHtml.push('<td class="tdBeltValue">' + app.ddltext(app.options,element.BeltValue) + '</td>');
            DataHtml.push('<td><input type="button" class="ibtnEdt btn btn-md btn-danger" onclick="app.editBelt(this);" value="Edit"></td>');
            DataHtml.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger" onclick="app.deleteBelt(this);" value="Delete"></td>');
            DataHtml.push('</tr>');
        });
        //$('#tbodyBeltSheet').append(DataHtml.join(''));
        $('#tbodyBeltSheet').html(DataHtml.join(''));
    }
    


}


$(document).ready(function () {



    //$(document).on('click', '.ibtnUpd', function () {
    //    app1.saveScoreSheets(this);
    //});

    var counter = 0;

    //$("#addrow").on("click", function () {
    //    var newRow = $("<tr>");
    //    var cols = "";

    //    cols += '<td><input type="text" class="form-control" name="name' + counter + '"/></td>';
    //    cols += '<td><input type="text" class="form-control" name="mail' + counter + '"/></td>';
    //    cols += '<td><input type="text" class="form-control" name="phone' + counter + '"/></td>';

    //    cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
    //    newRow.append(cols);
    //    $("table.order-list").append(newRow);
    //    counter++;
    //});
    var header = function () {
        var htmlHead = [];

        htmlHead.push('<thead>');
        htmlHead.push('<tr>');
        htmlHead.push('<td>Score Type</td>');
        htmlHead.push('<td>Score</td>');
        htmlHead.push('<td>Description</td>');
        htmlHead.push('<td>Value</td>');
        htmlHead.push('<td>Update</td>');
        htmlHead.push('<td>Cancel</td>');
        htmlHead.push('</tr>');
        htmlHead.push('</thead>');
        //$("table.order-list").append(htmlHead.join(''));
        return htmlHead;
    }

    var htmlBody = [];
    htmlBody.push(header);
    

    //$("#addrow").on("click", function () {
    //    if (counter == 0) {
    //        header();
    //    }
    //    var html = [];
    //    html.push('<tbody><tr>');


    //    var optHTML = [];
    //    optHtmlObj = $.parseHTML(app.optionResponseScoreType.join(''));
    //    $(optHtmlObj).each(function (x, opt) {
    //        //   alert($(opt).val());
            
    //        optHTML.push('<option value=' + $(opt).val() + '>' + $(opt).html() + '</option>')
           
    //    })

    //    //alert(app.options);
    //    //alert(app.options.join(''));

    //    html.push('<td><select  id="ScoreType_' + counter + '" class="ddlType form-control">' + optHTML.join('') + '</td>');
    //    html.push('<td><input  type="text" class="score form-control" id="Score' + counter + '"/></td>');
    //    html.push('<td><input  type="text" class="Description form-control" id="Description' + counter + '"/></td>');
    //    html.push('<td><input  type="text" class="Value form-control" id="Value' + counter + '"/></td>');

    //    html.push('<td><input type="button" class="ibtnUpd btn btn-md btn-danger"  value="update"></td>');
    //    html.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger"  value="Cancel"></td>');
    //    html.push('</tr></tbody>');

    //    htmlBody.push(html);

    //    //$("table.order-list").append(html.join(''));
    //    //$("table.order-list").html(html.join(''));
    //    $('#myTable').html(htmlBody.join(''));
    //    counter++;


    //})

    //$("#addrow").on("click", function () {
    //    var id = new Date().getTime();
    //    if ($("#tbodyScoreSheet").children().length < 1) {
    //        var firstChildHtml = [];

    //        firstChildHtml.push('<tr>');
    //        firstChildHtml.push('<td><select id="scoreType_' + id + '" onchange="app1.ddltypechange(\'' + id + '\');" class="ddlType form-control">' + app.optionResponseScoreType.join('') + '</select></td>');
    //        //firstChildHtml.push('<td><select id="scoreType_' + id + '" onchange="app.scoreByTypeID(\'' + id +'\',app1.callbackforScoreType(\''+data+'\'));" class="ddlType form-control">' + app.optionResponseScoreType.join('') + '</select></td>');

    //        //firstChildHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

    //        firstChildHtml.push('<td><input type="text" class="score form-control"></td>');
    //        firstChildHtml.push('<td><input type="text" class="description form-control"></td>');
    //        firstChildHtml.push('<td><input type="text" class="value form-control"></td>');
    //        firstChildHtml.push('<td><input type="button" class="ibtnUpd btn btn-md btn-danger"  value="update"></td>');
    //        firstChildHtml.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger"  value="Cancel"></td>');
    //        firstChildHtml.push('</tr>');
    //        $("#tbodyScoreSheet").append(firstChildHtml.join(''));
    //    } else {
    //        var firstChild = $("#tbodyScoreSheet").children()[0];

    //        var siblingHtml = [];

    //        siblingHtml.push('<tr>');
    //        siblingHtml.push('<td><select id="scoreType_' + id + '" onchange="app1.ddltypechange(\'' + id + '\');" class="ddlType form-control">' + app.optionResponseScoreType.join('') + '</select></td>');
    //        //siblingHtml.push('<td><select id="scoreType_' + id + '" onchange="app.scoreByTypeID(app1.callbackforScoreType(\''+id+'\'));" class="ddlType form-control">' + app.optionResponseScoreType.join('') + '</select></td>');

    //        //siblingHtml.push('<td><select id="score_' + id + '" class="score form-control">' + app.optionScore.join('') + '</select></td>');

    //        siblingHtml.push('<td><input type="text" class="score form-control"></td>');
    //        siblingHtml.push('<td><input type="text" class="description form-control" /></td>');
    //        siblingHtml.push('<td><input type="text" class="value form-control"/></td>');
    //        siblingHtml.push('<td><input type="button" class="ibtnUpd btn btn-md btn-danger"  value="update"></td>');
    //        siblingHtml.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger"  value="Cancel"></td>');
    //        siblingHtml.push('</tr>');

    //        $(firstChild).before(siblingHtml.join(''));
    //    }

    //});

   

    
    

    //$("table.order-list").on("click", ".ibtnDel", function (event) {
    //    $(this).closest("tr").remove();
    //    counter -= 1
    //    if ($('#myTable tbody tr').length == 0)
    //    {

    //    }
    //});


});

