﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {        
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });

        };
    };
    var APP_OBJECT = function (serverObject) {
        var appObj = this;       
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssesse();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.GetWards(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });

                            }
                            response(userDataAutoComplete);

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnWard.val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Locations: function () {

                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        Resetors.ResetAssesse();
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.GetLocations(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if ((serverResponse).length > 0) {

                                $.each(serverResponse, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });

                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            },
            Assessees: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnAssessee.val('');
                        //reset area
                        var locationID = DomControls.ctrl_hdnLocation.val();
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, LocationID: locationID, AssesseeName: request.term };
                        serverObject.GetAssesseebyName(data, function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.AssesseeName,
                                            AssesseeID: item.AssesseeID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            } else {
                                alert("Error: " + Response.Message);
                            }

                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                        //$("#txtAssessee").val(i.item.label);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssesse();
                        }
                    }
                };
            }
        };
        var DomControls = {           
            ctrl_btnPreviewReport: $("#btnPreviewReport"),
            ctrl_btnOutstandingReport: $("#btnOutstandingReport"),

            ctrl_hdnWard: $("#WardID"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#LocationID"),
            ctrl_txtLocation: $("#txtLocation"),

            ctrl_txtAssessee: $("#txtAssessee"),
            ctrl_hdnAssessee: $("#AssesseeID"),

            //ctrl_CollectionDate: $("#BillDate"),           
            //ctrl_txtCollectionDate: $("#BillDate")
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            },
            ResetAssesse: function () {
                DomControls.ctrl_hdnAssessee.val('');
                DomControls.ctrl_txtAssessee.val('');
            },
            ResetCollectionDt: function () {
                DomControls.ctrl_txtCollectionDate.val('');
            }
        };
        var OnPreviewReportClick = function () {           
            var data = PreviewObject();
            var cmd = $(this).attr('data-command');
            if (cmd == 'demand') {
                $('#frmDemandReport').attr('action', '/Municipality/MnReports/DemandReport');
            } else if (cmd == 'outsanding') {
                $('#frmDemandReport').attr('action', '/Municipality/MnReports/DemandOutstanding');
            }
            PopulateData(data, function (response) {               
            });
        };
        var PreviewObject = function () {
            return { WardID: DomControls.ctrl_hdnWard.val(), LocationID: DomControls.ctrl_hdnLocation.val(), AssesseeID: DomControls.ctrl_hdnAssessee.val() };
        };             
        var Validator = {
            ValidateSearchFields: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControls.ctrl_hdnWard.prop('name')] = { required: true };
                //vldObj.rules[DomControls.ctrl_hdnLocation.prop('name')] = { required: true };
                //vldObj.rules[DomControls.ctrl_hdnAssessee.prop('name')] = { required: true };
                //vldObj.rules[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[DomControls.ctrl_hdnWard.prop('name')] = { required: "Please select Ward." };
                //vldObj.messages[DomControls.ctrl_hdnLocation.prop('name')] = { required: "Please select Location." };
                //vldObj.messages[DomControls.ctrl_hdnAssessee.prop('name')] = { required: "Please select Assessee" };
                //vldObj.messages[DomControls.ctrl_txtCollectionDate.prop('name')] = { required: "Please select Bill Date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                $("#frmDemandReport").validate(vldObj);
                //validate form and return true and false
                return $('#frmDemandReport').valid();
            }
        };       
        var PopulateData = function (data, callback) {
            if (Validator.ValidateSearchFields()) {
                $("#frmDemandReport").submit();
            }
        };
        var BindEvents = function () {
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            DomControls.ctrl_txtAssessee.autocomplete(AutocompleteSorces.Assessees());         
            DomControls.ctrl_btnPreviewReport.on('click', OnPreviewReportClick);
            DomControls.ctrl_btnOutstandingReport.on('click', OnPreviewReportClick);
        };

        (function () {     
            BindEvents();
        }());
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    new INIT();
});