﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {       
        this.Insert = function (data, callback) {
            console.log(data);
            var Password = data.dataPassword.Password;
            $.ajax({
                type: "POST",
                url: "/Municipality/MnUserRegistration/Insert_User",
                data: JSON.stringify({ obj: data.dataUser, Password: Password }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetUserType = function (callback) {      
            $.ajax({
                type: "GET",
                url: "/Municipality/MnUserRegistration/Get_UserType",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var ddlUserType_data = [];
        var ddlUserType = $('#ddlUserType');
        var Bind_GridOnPageLoad = function () {     
            (function () {         
            }());
        }
        var PopulateUserType = function () {           
            ddlUserType.append("<option value='0'>--(Select User's Type)--</option>");
            $.each(ddlUserType_data, function (index, value) {
                var label = value.UserType;
                var val = value.UserTypeID;
                ddlUserType.append("<option value=" + val + ">" + label + "</option>");
            });
        }
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);
            $('#btnCancel').on('click', Reset_Field);
            $('#txtPhoneNo').on('keypress', PhoneNumberValidation);
        };
        var PhoneNumberValidation = function (e) {
            //  alert(e.keyCode);
            if (!(e.keyCode > 47 && e.keyCode < 58)) {
                return false;
            }
        }
        var Reset_Field = function () {           
            $("#divtxtContainer").find('input[type=text],input[type=password]').val("");
            $("#btnSave").attr('value', 'Save');           
        };
        var getDataFromView = function () {            
            var FirstName = $('#txtFirstName').val().trim();
            var LastName = $('#txtLastName').val().trim();
            var UserType = $('#ddlUserType').val();
            var PhoneNo = $('#txtPhoneNo').val().trim();
            var Email = $('#txtEmail').val().trim();             
            var UserName = $("#txtUserID").val().trim();
            var Password = $("#txtPassword").val().trim();
            var ReEnterPassword = $("#txtReEnterPassword").val().trim();

            var IsAccountLocked = 0;
            var IsAccountVerified = 0;

            if ($('#rdVarifiedYes').is(':checked')) {
                IsAccountVerified = 1;
            }

            if ($('#rdLockYes').is(':checked')) {
                IsAccountLocked = 1;
            }
          
            var dataUser = {
                UserID: 0, FirstName: FirstName, LastName: LastName, Email: Email, PhoneNo: PhoneNo,
                IsAccountLocked: IsAccountLocked, IsAccountVerified: 1, MunicipalityID: 0, UserName: UserName,
                UserType: UserType
            };
            var dataPassword = {
                Password: Password,
                ReEnterPassword: ReEnterPassword
            }
            var data = {
                dataUser: dataUser,
                dataPassword: dataPassword
            }
            return data;
        };   
        var Validate = function (data) {
            if (data.dataUser.FirstName == '') {
                alert("Fill First Name");
                $('#txtFirstName').focus();
                return false;
            }
            if (data.dataUser.LastName == '') {
                alert("Fill Last Name");
                $('#txtLastName').focus();
                return false;
            }
            if (data.dataUser.PhoneNo == '') {
                alert("Fill Phone Number");
                $("#txtPhoneNo").focus();
                return false;
            }
            if (data.dataUser.PhoneNo != '') {
                var expr = /^(\-)?([\d]+(?:\.\d{1,2})?)$/;
                var valid = expr.test(data.dataUser.PhoneNo);
                if (valid == false) {
                    alert("Enter a valid Phone Number");
                    $('#txtPhoneNo').focus();
                    return false;
                }
            }
            if (data.dataUser.UserType ==0) {
                alert("Select a User Type");
                $('#ddlUserType').focus();
                return false;
            }
            if (data.dataUser.Email != "") {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                var valid = expr.test(data.dataUser.Email);
                if (valid == false) {
                    alert("Enter a valid Email-ID");
                    $('#txtEmail').focus();
                    return false;
                }
            }          
            if (data.dataUser.UserName == '') {
                alert("Create a Login ID");
                $('#txtUserID').focus();
                return false;
            }
            if (data.dataPassword.Password == '') {
                alert("Create a Password");
                $('#txtPassword').focus();
                return false;
            }
            if (data.dataPassword.ReEnterPassword == '') {
                alert("Re-Enter the Password");
                $('#txtReEnterPassword').focus();
                return false;
            }
            if (data.dataPassword.ReEnterPassword != data.dataPassword.Password) {
                alert("Password does not match.Create a Password carefully.");
                $('#txtPassword').val("");
                $('#txtReEnterPassword').val("");
                $('#txtPassword').focus();
                return false;
            }
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {
                serverObject.Insert(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert('Registration Successfully !!');
                        window.location.reload(true);
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        this.app_Initiate = function () {    
            BindEvent();
            Bind_GridOnPageLoad();
            serverObject.GetUserType(function (response) {               
                ddlUserType_data = response.Data;
                PopulateUserType();
            });
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
})