﻿$(document).ready(function () {
    var frm = $('#scoreBelt');
    app.Validation();
    app.pageName();
    app.ddlMunicipality();
    app.getYear();
    app.Scoretype();
    app.belTypeValue();
    $('#ddlMunicipality,#ddlyear').change(function () {
        $('#fromdate').val('');
        $('#todate').val('');
    });

    $('#ddlScoreType').change(function (evt, arg) {
        app.ddltypechange(this, 'ddlScore',arg);
    });
    $('#ddlScore').change(function () {
        app.onscoreChange(this,'txtDesc');
    });
    $('#btnGo').click(function () {
        app.tag = $(this).attr('data-tag');
        //if (!frm.valid()) {
        //    return false;
        //}
        //app.validation();
        $('#tbodyScoreSheet').html('');
        $('#tbodyBeltSheet').html('');

        if (!frm.valid()) {
            return false;
        } else {
            app.showEffecdate();
            app.ScoreView();
            app.BeltView();
        }
    });

    $('#btnAddScore').click(function () {
        app.tag = $(this).attr('data-tag');
        if (!frm.valid()) {
            return false;
        } else {
            app.scoreInsUpd();
        }
        s
    });
    $('#btnAddBelt').click(function () {
        app.tag = $(this).attr('data-tag');
        if (!frm.valid()) {
            return false;
        } else {
            app.beltInsUpd();
        }
       

    });
});


var app = {
    tag:'',
    scoreSheetId: '',
    beltSheetId:'',
    options: [],
    optionResponseScoreType: [],
    optionResponseBeltType: [],
    pageName: function () {
        var loc = window.location;
        var relativepath = window.location.pathname;
        var splt = relativepath.split('/');
        var path = "";
        for (var x = splt.length-1;x >=0;x--){
            if (splt[x].trim() !== "") {
                path = splt[x];
                break;
            }
        }
        return path.toLowerCase();
    },
    ddlMunicipality: function () {
        $.ajax({
            url: '/Administration/ScoreBelt/GetAllMunicipalites',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
            },
            error: function () { }
        });
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('#ddlMunicipality');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
        var option = ctlr_ddlmunicipality.find("option[value='" + ctlr_ddlmunicipality.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlmunicipality.find('option:eq(0)').prop('selected', true);
        }
    },
    getYear: function () {
        $.ajax({
            url: '/Administration/ScoreBelt/GetYear',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: '{}',
            dataType: 'json',
            success: function (response) { app.populateYear(response.Data) },
            error: function () { }
        });
    },
    populateYear: function (data) {
        var ctlr_ddlYear = $('#ddlyear');
        ctlr_ddlYear.empty();
        ctlr_ddlYear.append('<option value="">--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlYear.append('<option value=' + item.YearDesc + '>' + item.YearDesc + '</option>');
        })
    },
    Scoretype: function () {
        $.ajax({
            url: '/Administration/ScoreBelt/GetScoreType',
            type: 'POST',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //app.optionResponseScoreType = response.Data;

                //$(response.Data).each(function (i, element) {
                //    app.ScoreTypeOption.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
                //});
                app.optionResponseScoreType.push('<option value="">-- Select --</option>');
                app.optionResponseBeltType.push('<option value="">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    if (element.ScoreBelt === "S") {
                        app.optionResponseScoreType.push('<option value=' + element.ScoreBeltTypeId + '>' + element.ScoreBeltTypeDesc + '</option>');
                        $('#ddlScoreType').html(app.optionResponseScoreType.join(''));

                    } else {
                        app.optionResponseBeltType.push('<option value=' + element.ScoreBeltTypeId + '>' + element.ScoreBeltTypeDesc + '</option>');
                        $('#ddlbeltType').html(app.optionResponseBeltType.join(''));
                    }
                });
                console.log(app.optionResponseScoreType);
            },
            error: function () { }
        });
    },
    ddltypechange: function (ref,id,arg) {
        var selectedVal = $(ref).find('option:selected').val();
        //var obj = { "typeId": selectedVal }
        var obj = { typeId: selectedVal }
        //$("#score_" + id).html('');
        $("#" + id).html('');
        $.ajax({
            url: '/Administration/ScoreBelt/GetScoreByTypeId',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var html = [];
                html.push('<option value="0">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    html.push('<option value="' + element.ScoreDesc + '" >' + element.ScoreCode + '</option>');
                });

                //$("#score_" + id).html(html.join(''));
                $("#" + id).html(html.join(''));
                if (arg && arg._selectedScoreId) {
                    $("#" + id).find('option[value="' + arg._selectedScoreId + '"]').prop('selected', true);
                }
                //ram $("#score_" + id).trigger('onchange');
            },
            error: function () { }
        });
    },
    onscoreChange: function (ref, id) {
        try {
            var selectedVal = $(ref).find('option:selected').val();
           
            if (selectedVal != "0") {
                //$("#txtDesc_" + id).val(selectedVal);
                $("#" + id).val(selectedVal);
                //$("#score_" + id).trigger('onchange');
            }
            else {
                //$("#txtDesc_" + id).val("");
                $("#" + id).val("");
            }
        } catch (e) {

        }
    },
    belTypeValue: function () {
        $.ajax({
            url: '/Administration/ScoreBelt/GetBeltTypevalue',
            type: 'POST',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.optionResponse = response.Data;
                app.options.push('<option value="">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    app.options.push('<option value=' + element.ValTag + '>' + element.ValDesc + '</option>');
                    $('#ddlConsider').html(app.options.join(''));
                });
                //console.log(app.options);
            },
            error: function () { }
        });
    },
  
    Validation: function () {
         //$.validator.addMethod('ddlMunicipality', function (value, element) {
        //    return value != "0";
        //});

        $('#scoreBelt').validate({
            rules: {
                ddlMunicipality: {
                    required: true
                },
                ddlyear: {
                    required: true
                },
                ddlScoreType: {
                    required: function () {
                        if (app.tag == "SA") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtscore: {
                    required: function () {
                        if (app.tag == "SA") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtDesc: {
                    required: function () {
                        if (app.tag == "SA") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtvalue: {
                    required: function () {
                        if (app.tag == "SA") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlbeltType: {
                    required: function () {
                        if (app.tag == "BA") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                },
                txtFrom: {
                    required: function () {
                        if (app.tag == "BA") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                },
                txtUpto: {
                    required: function () {
                        if (app.tag == "BA") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                },
                ddlConsider: {
                    required: function () {
                        if (app.tag == "BA") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }




            },
            messages: {
                ddlMunicipality: {
                    required: "Please Select Municipality."
                },
                ddlyear: {
                    required: "Please Select Year."
                },
                ddlScoreType: {
                    required: "Please select Score Type"
                },
                txtscore: {
                    required:"Please Enter Score"
                },
                txtDesc: {
                    required:"Please Enter Description."
                },
                txtvalue: {
                    required: "Please Enter Score value."
                },

                ddlbeltType: {
                    required: "Please select Belt Type"
                },
                txtFrom: {
                    required: "Please Enter From range."
                },
                txtUpto: {
                    required: "Please Enter To range."
                },
                ddlConsider: {
                    required: "Please select Consider Type"
                }

            }
        });
    },
    showEffecdate: function () {
        var municipalityId = ($('#ddlMunicipality').val() ?$('#ddlMunicipality').val(): 0 );
        var year = $('#ddlyear').val();
        var name = app.pageName();

        

        $.ajax({
            url: '/Administration/ScoreBelt/GetEffectiveDate',
            type: 'POST',
            data: JSON.stringify({municipalityId:municipalityId ,year:year,name:name}),
            //data: "{year:" + year + "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(JSON.stringify(response));
                //console.log(response.Data)
                //console.log(JSON.stringify(response.Data));
                //console.log(JSON.parse(response.Data));
                //app.getScoreSheet(response.Data);
                //var fdate = response.Data[0][element.EffectiveDateTo]
                //if (response.Data.length > 0) {
                if (response.Data != null && response.Data.length!=0) {
                    console.log(response.Data);
                    console.log(response.Data[0]["EffectiveDateForm"]);
                    console.log(response.Data[0]["EffectiveDateTo"]);
                    $('#fromdate').val(CONVERTER.Date.convertCsToJsDate(response.Data[0]["EffectiveDateForm"]).format('dd/mm/yy'));
                    $('#todate').val(new Date(parseInt((response.Data[0]["EffectiveDateTo"]).replace(/[^0-9 +]/g, ''))).format('dd/mm/yy'));
                }

            },
            error: function () { }
        });
    },
    ScoreView: function () {

        //var municipalityId = $('#ddlMunicipality').val();
        //var year = $('#ddlyear').val();

        var obj = {};
        obj.year = $('#ddlyear').val();
        obj.municipalityId = $('#ddlMunicipality').val();
        obj.name = app.pageName();

        $.ajax({
            url: '/Administration/ScoreBelt/ScoreView',
            type: 'POST',
            //data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            //data: "{year:" + year + "}",
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                app.popScoreView(response.Data);
            },
            error: function () { }
        });
    },
    popScoreView: function (data) {
        $('#tbodyScoreSheet').html('');
        var DataHtml = [];

        var optHtmlObj = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr>');
            DataHtml.push('<td style=display:none; class="tdScoreSheetId">' + element.ScoreSheetID + '</td>');
            DataHtml.push('<td style=display:none; class="tdScoreBeltTypeId">' + element.ScoreBeltTypeId + '</td>');
            DataHtml.push('<td>' + element.ScoreBeltTypeDesc + '</td>');
            DataHtml.push('<td class="tdScoreCode">' + element.ScoreCode + '</td>');
            DataHtml.push('<td class="tdScoreDesc">' + element.ScoreDesc + '</td>');
            DataHtml.push('<td style="text-align:right;" class="tdScoreValue">' + parseFloat(element.ScoreValue).toFixed(2) + '</td>');
            if (app.pageName() == 'index') {
                DataHtml.push('<td><input type="button" class="ibtnEdt btn btn-md btn-danger" onclick="app.scoreEdit(this);" value="Edit"></td>');
                DataHtml.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger" onclick="app.scoreDel(this);" value="Delete"></td>');
            }
            DataHtml.push('</tr>');

        });
        //$('#tbodyScoreSheet').append(DataHtml.join(''));
        $('#tbodyScoreSheet').html(DataHtml.join(''));
    },
    BeltView: function () {
        var obj = {};
        obj.year = $('#ddlyear').val();
        obj.municipalityId = $('#ddlMunicipality').val();
        obj.name = app.pageName();

        $.ajax({
            url: '/Administration/ScoreBelt/BeltView',
            type: 'POST',
            //data: "{municipalityId:" + municipalityId + ",year:" + year + "}",
            //data: "{year:" + year + "}",
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                app.popBeltView(response.Data);
            },
            error: function () { }
        });
    },
    popBeltView: function (data) {
        var DataHtml = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr>');
            DataHtml.push('<td style=display:none; class="tdBeltSheetID">' + element.BeltSheetID + '</td>');
            DataHtml.push('<td style=display:none; class="tdScoreBeltTypeId">' + element.ScoreBeltTypeId + '</td>');
            DataHtml.push('<td>' +element.ScoreBeltTypeDesc+ '</td>');
            DataHtml.push('<td style="text-align: right;" class="tdFromRange">' +parseFloat(element.FromRange).toFixed(2) + '</td>');
            DataHtml.push('<td style="text-align: right;" class="tdToRange">' +parseFloat(element.ToRange).toFixed(2) + '</td>');
            DataHtml.push('<td>' + element.BeltDesc + '</td>');
            DataHtml.push('<td style=display:none; class="tdBeltValue">' + element.BeltValue + '</td>');
            if (app.pageName() == 'index') {
                DataHtml.push('<td><input type="button" class="ibtnEdt btn btn-md btn-danger" onclick="app.beltEdit(this);" value="Edit"></td>');
                DataHtml.push('<td><input type="button" class="ibtnDel btn btn-md btn-danger" onclick="app.beltDel(this);" value="Delete"></td>');
            }
            DataHtml.push('</tr>');
        });
        //$('#tbodyBeltSheet').append(DataHtml.join(''));
        $('#tbodyBeltSheet').html(DataHtml.join(''));
    },

    scoreDel: function (ref) {
        var res = confirm('Are You Sure want to Delete this Record ??');
        if (res == true) {
        var parentTr = $(ref).parents('tr');

        var obj = {};
        obj.ScoreSheetID = $(parentTr).find('.tdScoreSheetId').html();

        $.ajax({
            url: '/Administration/ScoreBelt/ScoreDel',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                alert(response.Data[0]["msg"]);
                app.ScoreView();
                //app.getAllScoreSheets();
            },
            error: function () { }
        });
        return false;
        }
    },
    beltDel: function (ref) {
        var res = confirm('Are You Sure want to Delete this Record ??');
        if (res == true) {
            var parentTr = $(ref).parents('tr');

            var obj = {};
            obj.BeltSheetID = $(parentTr).find('.tdBeltSheetID').html();

            $.ajax({
                url: '/Administration/ScoreBelt/BeltDel',
                type: 'POST',
                data: JSON.stringify(obj),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    alert(response.Data[0]["msg"]);
                    app.BeltView();
                },
                error: function () { }
            });
            return false;
        }
    },
    scoreEdit: function (ref) {
        var parentTr = $(ref).parents('tr');

        app.scoreSheetId = $(parentTr).find('.tdScoreSheetId').html();
        //alert(app.scoreSheetId);
        $('#ddlScoreType').val($(parentTr).find('.tdScoreBeltTypeId').html());
        //$('#ddlScore').val($(parentTr).find('.tdScoreCode').html());
        //$('#ddlScore').text($(parentTr).find('.tdScoreCode').html());
        $('#ddlScoreType').trigger('change', { _selectedScoreId:$(parentTr).find('.tdScoreDesc').html()});
        //$('#ddlScore').val($(parentTr).find('.tdScoreDesc').html());
        //$('#ddlScore').text($(parentTr).find('.tdScoreCode').html());
        $('#txtscore').val($(parentTr).find('.tdScoreCode').html());
        $('#txtDesc').val($(parentTr).find('.tdScoreDesc').html());
        $('#txtvalue').val($(parentTr).find('.tdScoreValue').html());

        $('#btnAddScore').html('Update');
    },
    scoreInsUpd: function () {
        
        var obj = {};
        obj.municipalityId = $('#ddlMunicipality').val();
        obj.year = $('#ddlyear').val();
        obj.ScoreSheetID = app.scoreSheetId;
        obj.ScoreBeltTypeId = $('#ddlScoreType option:selected').val();
        obj.ScoreCode = $('#ddlScore option:selected').text();
        obj.ScoreCode = $('#txtscore').val();
        obj.ScoreDesc = $('#txtDesc').val();
        obj.ScoreValue = $('#txtvalue').val();

        $.ajax({
            url: '/Administration/ScoreBelt/ScoreInsUpd',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //app.getAllBeltSheets();
                //app.onCancelBelt(ref);
                alert(response.Data[0]["msg"]);
                app.ScoreView();
                app.scoreReset();
            },
            error: function () { }
        });

    },
    beltEdit: function (ref) {
        var parentTr = $(ref).parents('tr');

        //var beltSheetId = $(parentTr).find('.tdSBeltSheetID').html();
        //var beltTypeId = $(parentTr).find('td:eq(1)').html();
        //var fromRange = $(parentTr).find('.tdFromRange').html();
        //var toRange = $(parentTr).find('.tdToRange').html();
        //var beltvalue = $(parentTr).find('.tdBeltValue').html();

        app.beltSheetId = $(parentTr).find('.tdBeltSheetID').html();
        //alert(app.beltSheetId);
        $('#ddlbeltType').val($(parentTr).find('td:eq(1)').html());
        $('#txtFrom').val($(parentTr).find('.tdFromRange').html());
        $('#txtUpto').val($(parentTr).find('.tdToRange').html());
        $('#ddlConsider').val($(parentTr).find('.tdBeltValue').html());

        $('#btnAddBelt').html('Update');

    },
    beltInsUpd: function () {
        var obj = {};
        obj.municipalityId = $('#ddlMunicipality').val();
        obj.year = $('#ddlyear').val();
        obj.BeltSheetID = app.beltSheetId;
        obj.ScoreBeltTypeId = $('#ddlbeltType').val();
        obj.FromRange=$('#txtFrom').val();
        obj.ToRange=$('#txtUpto').val();
        obj.BeltValue = $('#ddlConsider').val();

        $.ajax({
            url: '/Administration/ScoreBelt/BeltInsUpd',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //app.getAllBeltSheets();
                //app.onCancelBelt(ref);
                alert(response.Data[0]["msg"]);
                app.BeltView();
                app.beltReset();
            },
            error: function () { }
        });
    },
    beltReset: function () {
        app.beltSheetId = '';
        $('#ddlbeltType').val('');
        $('#txtFrom').val('');
        $('#txtUpto').val('');
        $('#ddlConsider').val('');
        $('#btnAddBelt').html('Add');
    },
    scoreReset: function () {
        app.scoreSheetId = '';
        $('#ddlScoreType').val('');
        $('#ddlScore').val('');
        $('#txtscore').val('');
        $('#txtDesc').val('');
        $('#txtvalue').val('');
        $('#btnAddScore').html('Add')
    }


}