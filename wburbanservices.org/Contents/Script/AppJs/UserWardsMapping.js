﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.AddWardToUser = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnWards/AddWardToUser",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.RemoveWardToUser = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnWards/RemoveWardToUser",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.GetMappedWardsByUserID = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnWards/GetMappedWardsByUserID",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {

        var RegisterEvent = function () {
            $(document).on('change', '#UserID', onChanges);
            $(document).on('click', '.chk_ward:checked', onMapped);
            $(document).on('click', '.chk_ward:unchecked', onUnmapped);
        };
        var onChanges = function () {
            $('#tblDetail').find('tbody tr').find('.lblResultText').text('');
            $('#tblDetail').find('tbody tr').find('.chk_ward').prop('checked', false);
            if ($('#UserID').val() == 0) {
                return false;
            }

            var UserID = $('#UserID').val();
            var data = { UserID: UserID };
            serverObject.GetMappedWardsByUserID(data, function (response) {
                if (response.Status == 200) {
                    var rowDatas = response.Data;
                    if (rowDatas.length > 0) {
                        BindWard_AndChecked(rowDatas);
                    }

                }
            });
        }
        var onMapped = function () {

            if ($('#UserID').val() == 0) {
                $('#UserID').focus(); return false;
            }

            var ctrl_ID = $(this);
            var WardID = ctrl_ID.closest('tr').find('.chk_ward').val();
            var UserID = $('#UserID').val();

            var data = { UserID: UserID, WardID: WardID };
            serverObject.AddWardToUser(data, function (response) {
                if (response.Status == 200) {
                    var rowDatas = response.Data;
                    //alert(response.Message);
                    ctrl_ID.closest('tr').find('.lblResultText').text('Mapped').css('color', 'green');
                }
            });
        };
        var onUnmapped = function () {
            if ($('#UserID').val() == 0) {
                $('#UserID').focus(); return false;
            }
            var ctrl_ID = $(this);
            var WardID = ctrl_ID.closest('tr').find('.chk_ward').val();
            var UserID = $('#UserID').val();
            var data = { UserID: UserID, WardID: WardID };
            serverObject.RemoveWardToUser(data, function (response) {
                if (response.Status == 200) {
                    var rowDatas = response.Data;
                    //alert(response.Message);
                    ctrl_ID.closest('tr').find('.lblResultText').text('Unmapped').css('color', 'green');
                }
            });
        };
        function BindWard_AndChecked(rowDatas) {

            $.each(rowDatas, function (index, value) {
                var obj = value;
                var ChkValue = obj.WardID;
                $('#tblDetail').find('tbody tr').find('.chk_ward[value="' + ChkValue + '"]').prop('checked', true);
            });
        }
        this.app_Initiate = function () {
            RegisterEvent();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});


