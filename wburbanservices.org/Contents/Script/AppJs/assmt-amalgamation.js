﻿$(document).ready(function () {
    var constants = function () {
        return {
            effectiveFinYearQtr: function () {
                return {
                    ddlEffectiveFinYear: $(".assmt-finyear-qtr .ddl-finyear-picker"),//
                    ddlEffectiveQtr: $(".assmt-finyear-qtr .ddl-qtr")
                };
            },

            getValTag: function () {
                return $(".setting-parameters").find('.val-tag').val();
            },
            getYearId: function () {
                //year-id
                return $(".setting-parameters").find('.year-id').val();
            },
            convertTodate: function (format, date) {
                switch (format) {
                    case 'dd/mm/yyyy':
                    case 'dd/mm/yy':
                        {
                            var array = date.split('/');
                            return CONVERTER.Date.convertToDate(array[2], array[1], array[0]);
                            break;
                        }
                }
            },
            HomeTabPane: function () {
                return _doms().tabControl.find('.tab-pane#home');
            },
            MotherHodingTabPane: function () {
                return _doms().tabControl.find(".mother-holding");
            },

            isValidNewTabPane: function (tabpane) {
                if (tabpane.find("#frm-valuation-details").length > 0) {
                    return true;
                } return false;
            },
            getTabScope: function (element) {
                return element.closest('.tab-pane');
            },
        };
    };
    //end constants
    var helpers = function () {
        return {
            getModalDesign: function (searchedHodling, scope) {
                if (searchedHodling) {
                    var data = searchedHodling.ActualAssmtData;
                    var str = "";
                    str += "<input type='hidden' class='hdn-modal-raw-data' value='" + JSON.stringify(searchedHodling.ActualAssmtData) + "'>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Ward</label>";
                    str += "<input class='form-control' type='text' value='" + data.WardId + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Location</label>";
                    str += "<input class='form-control' type='text' value='" + _doms(scope).searchHolding.txtLocation.val() + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Holding No</label>";
                    str += "<input class='form-control' type='text' value='" + _doms(scope).searchHolding.hddnHoldingNo.val() + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-8'>";
                    str += "<label>Name</label>";
                    str += "<input class='form-control' type='text' value='" + data.AssesseeName + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-2'>";
                    str += "<label>Holding Type</label>";
                    var holdingTypeGroup = alasql("select * from ? where HoldingTypeGrpId=?", [searchedHodling._HoldingTypeGroupes, data.HoldingTypeGrp]);
                    var holdingTypeGroupName = "";
                    if (holdingTypeGroup.length) {
                        holdingTypeGroupName = holdingTypeGroup[0].HoldingTypeGrpDesc;
                    }
                    str += "<input class='form-control' type='text' value='" + holdingTypeGroupName + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-2'>";
                    str += "<label>Land Area</label>";
                    var holdingArea = "";
                    if (data.HoldArea == "S") {
                        holdingArea = "Slum";
                    } else if (data.HoldArea == "N") {
                        holdingArea = "Non-Slum";
                    } else if (data.HoldArea == "G") {
                        holdingArea = "General";
                    } else {
                        holdingArea = "None";
                    }
                    str += "<input class='form-control' type='text' value='" + holdingArea + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    str += "<div class='row'>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Annual Valuation</label>";
                    str += "<input class='form-control' type='text' value='" + data.AnnualVal + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Qtrly Prop Tax</label>";
                    str += "<input class='form-control' type='text' value='" + data.QtrProptax + "' readonly />";
                    str += "</div>";
                    str += "<div class='col-md-4'>";
                    str += "<label>Edu Cess</label>";
                    str += "<input class='form-control' type='text' value='" + data.QtrEduCess + "' readonly />";
                    str += "</div>";
                    str += "</div>";
                    
                    return str;
                }
            },
            SetTitleModal: function (modal, title) {
                modal.find('.modal-title').text(title);
            },
            getTableRowTamplete: function (searchedHodling,scope) {
                var str = "";
                if (!searchedHodling) {
                    str += "<tr>";
                    str += "<td class='text-center' colspan='7'>No Holdings added</td>";
                    
                }else
                {
                    str += "<tr class='holding-data' data-assmt-data='"+JSON.stringify(searchedHodling)+"'>";
                    var data = searchedHodling;
                    str += "<td>" + data.WardId + "</td>";
                    str += "<td>" +  _doms(scope).searchHolding.txtLocation.val() + "</td>";
                    str += "<td>" + _doms(scope).searchHolding.hddnHoldingNo.val() + "</td>";
                    str += "<td>" + data.AssesseeName + "</td>";
                    str += "<td>" + data.AnnualVal + "</td>";
                    str += "<td>" + data.QtrProptax + "</td>";
                    str += "<td class='text-center'><a href='javacript:void(0);' class='remove-holding'><i class='fa fa-trash-o'></i></a></td>";
                }
                str += "</tr>";
                return str;
            },
            getChildHoldings: function () {
                var table = _doms().tabControl.find("#home .tbl-child-holding-list");
                var childHoldings = table.find('tbody tr.holding-data').map(function (indx, item) {
                    var objs = JSON.parse($(item).attr("data-assmt-data"));
                    objs.BuildingDtl = _doms().txtBuildingDtl.val();
                    return objs;
                }).get();
                return childHoldings;
            },
            getMotherHolding: function () {
                var motherHodlingDetail = _doms().tabControl.find("#mother-holding .holding-detalis .hdn-modal-raw-data")
                var motherHolding = null;
                if (motherHodlingDetail.length > 0) {
                    motherHolding = JSON.parse(motherHodlingDetail.val());
                    motherHolding.BuildingDtl = _doms().txtBuildingDtl.val();
                    motherHolding.ApplNo = $(".txt-application-no").val();
                    motherHolding.ApplDt = $(".txt-application-date").val();
                    motherHolding.DtOfOrder = $(".txt-date-of-order").val();
                    motherHolding.ProcessingFees = $(".txt-pro-cosing-fees").val();
                    motherHolding.OtherFees = $(".txt-other-fees").val();
                    motherHolding.PrimaryEmail = $(".txt-primary-email-id").val();
                    motherHolding.PrimaryPhNo = $(".txt-primary-phone-no").val();
                }
                return motherHolding;
            },
            processFormDataBeforSave: function (assmtDetail) {
                var maxYearId = constants().getYearId();
                var finYear = constants().effectiveFinYearQtr().ddlEffectiveFinYear.val();
                var qtr = constants().effectiveFinYearQtr().ddlEffectiveQtr.val();
                var ValTag = constants().getValTag();
                assmtDetail.ValYear = finYear;
                assmtDetail.ValQtr = qtr;
                assmtDetail.ValTag = ValTag;
                assmtDetail.YearId = maxYearId;
                assmtDetail.InsertedOn = null;
                assmtDetail.UpdatedOn = null;
                assmtDetail.ApprovedOn = null;
                return assmtDetail;
            },
            getValuationArrayData: function () {
                var rowDataJsonString = null;
                var rowDataJson = null;
                var tbl = $('.completeJson');
                var _ValuationDetailsArr = [];
                $.each(tbl, function (i, v) {
                    rowDataJsonString = v.innerText;
                    rowDataJson = JSON.parse(rowDataJsonString);
                    rowDataJson.HoldingTypeGroupId = _doms().valuationDtl.ddlHoldingTypeGroup.val();
                    // rowDataJson.EditMode=
                    _ValuationDetailsArr.push(rowDataJson);
                });
                return _ValuationDetailsArr;
            },

        };

    }
    var _doms = function (scope) {
        if (!scope) {
            scope = $(document);
        }
        return {
            cntrDownloadedForm: scope.find(".downloaded-form"),
            tabControl: scope.find(".amalgamation-tab-control"),
            modalSearchHolding:scope.find("#searched-holding-modal"),
            searchHolding: {
                frm: scope.find("#frm-holding-details"),
                txtWard: scope.find(".txtWard"),
                hdnWard: scope.find(".hddn-Ward"),
                txtLocation: scope.find(".txtLocation"),
                hdnLocation: scope.find(".hddn-Location"),
                txtHoldingNo: scope.find(".txt-holding-no"),
                hddnHoldingNo: scope.find(".hddn-holding-no"),
            },
            txtBuildingDtl: $(".txt-building-dtl"),
            valuationDtl: {
                frm: scope.find("#frm-valuation-details"),
                ddlHoldingType: scope.find(".ddl-holding-type"),
                ddlZone: scope.find(".ddl-zone"),
                txtLandAreaKt: scope.find(".txt-land-area-kt"),
                txtLandAreaCh: scope.find(".txt-land-area-ch"),
                txtLandAreaSft: scope.find(".txt-land-area-sft"),
                ddlNatureOfUser: scope.find(".ddl-nature-of-use"),
                txtBldgAreaSft: scope.find(".txt-bldg-area-sft"),
                txtPlinthSft: scope.find(".txt-plinth-sft"),
                ddlConstruction: scope.find(".ddl-construction"),
                txtLandCost: scope.find(".txt-land-cost"),
                txtAge: scope.find(".txt-age"),
                txtReadOnlyAnnualValuation: scope.find(".txt-readonly-annual-valuation"),
                txtReadOnlyQtrPtax: scope.find(".txt-readonly-qtr-ptax"),
                txtReadOnlyQtrSchrg: scope.find(".txt-readonly-qtr-schrg"),
                ddlHoldingTypeGroup: $('.holdingtypegroupId'),//mother group id
                SrchgTag: scope.find(".chk-SrchgTag"),
                EduCessTag: scope.find(".chk-EduCessTag"),
                txtReadonlyQtrEduCess: scope.find(".txt-readonly-qtr-Edu-cess")
            },
        }
    };
    var validators = function (scope) {
        return {
            validateSearchHolding: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms(scope).searchHolding.hdnWard.prop('name')] = { required: true };
                vldObj.rules[_doms(scope).searchHolding.hdnLocation.prop('name')] = { required: true };
                vldObj.rules[_doms(scope).searchHolding.hddnHoldingNo.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[_doms(scope).searchHolding.hdnWard.prop('name')] = { required: "Please enter Ward." };
                vldObj.messages[_doms(scope).searchHolding.hdnLocation.prop('name')] = { required: "Please enter Location." };
                vldObj.messages[_doms(scope).searchHolding.hddnHoldingNo.prop('name')] = { required: "Please enter Holding No" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms(scope).searchHolding.frm.validate(vldObj);
                //validate form and return true and false
                return _doms(scope).searchHolding.frm.valid();
            },
            ValidatedataBeforeSave: function () {
                var childHoldings = helpers().getChildHoldings();
                var motherHolding = helpers().getMotherHolding();
                //validate child is available 
                if (!childHoldings.length > 0) {
                    throw "Child holdings required";
                }
                //validate mother is selected
                if (!motherHolding) {
                    throw "Mother holding required";
                }
                //validate all childs in same location and ward //validate all child and mothers in same location and ward
                var arr = childHoldings.map(function (val, indx) {
                    return { HoldingNo: val.HoldingNo, WardId: val.WardId, LocationID: val.LocationID }
                });
                arr.push({ HoldingNo: motherHolding.HoldingNo, WardId: motherHolding.WardId, LocationID: motherHolding.LocationID });
                
               if ($.unique(arr.map(function (val) { return  val.WardId + val.LocationID })).length > 1) {
                   throw "Location and Ward of mother and childs holdings should be same";
               }
            },
            validateValuation: function () {
                $("#frm-valuation-details").validate().resetForm();
                $("#frm-valuation-details").removeData('validator');
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];
                var sdh = $(".ddl-hodling-type-group").val();
                //set rules
                vldObj.rules[_doms().valuationDtl.ddlZone.prop('name')] = { required: true };
                vldObj.rules[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: true, number: true, range: [0, 15] };
                vldObj.rules[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: true, number: true, range: [0, 44] };
                vldObj.rules[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
                vldObj.rules[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
                vldObj.rules[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: !(sdh == 8 || sdh == 9) };
                vldObj.rules[_doms().valuationDtl.txtLandCost.prop('name')] = { required: true, number: true };
                vldObj.rules[_doms().valuationDtl.txtAge.prop('name')] = { required: true, number: true };


                //set error messages
                vldObj.messages[_doms().valuationDtl.ddlZone.prop('name')] = { required: "Please enter Ward." };
                vldObj.messages[_doms().valuationDtl.txtLandAreaKt.prop('name')] = { required: "Please enter land area kt.", number: "Data should be numeric" };
                vldObj.messages[_doms().valuationDtl.txtLandAreaCh.prop('name')] = { required: "Please enter land area ch.", number: "Data should be numeric", range: "Value sould be between 0 to 15" };
                vldObj.messages[_doms().valuationDtl.txtLandAreaSft.prop('name')] = { required: "Please enter land area sft.", number: "Data should be numeric", range: "Value sould be between 0 to 44" };
                vldObj.messages[_doms().valuationDtl.ddlNatureOfUser.prop('name')] = { required: "Please select nature of use" };
                vldObj.messages[_doms().valuationDtl.txtBldgAreaSft.prop('name')] = { required: "Please enter bldg. area sft", number: "Data should be numeric" };
                vldObj.messages[_doms().valuationDtl.txtPlinthSft.prop('name')] = { required: "Please enter plinth sft", number: "Data should be numeric" };
                vldObj.messages[_doms().valuationDtl.ddlConstruction.prop('name')] = { required: "Please select construction" };
                vldObj.messages[_doms().valuationDtl.txtLandCost.prop('name')] = { required: "Please enter land cost", number: "Data should be numeric" };
                vldObj.messages[_doms().valuationDtl.txtAge.prop('name')] = { required: "Please enter age", number: "Data should be numeric" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().valuationDtl.frm.validate(vldObj);
                //validate form and return true and false
                return _doms().valuationDtl.frm.valid();
            },
        };
    }
    var formDatas = function () {
        return {
            valuationdata: function () {
                return {
                    HoldingTypeId: _doms().valuationDtl.ddlHoldingType.val(),
                    ZoneId: _doms().valuationDtl.ddlZone.val(),
                    NatureOfUseId: _doms().valuationDtl.ddlNatureOfUser.val(),
                    ConstructionId: _doms().valuationDtl.ddlConstruction.val(),
                    LandArea_KT: _doms().valuationDtl.txtLandAreaKt.val(),
                    LandArea_CH: _doms().valuationDtl.txtLandAreaCh.val(),
                    LandArea_SFT: _doms().valuationDtl.txtLandAreaSft.val(),
                    BldgArea_SFT: _doms().valuationDtl.txtBldgAreaSft.val(),
                    Plinth_SFT: _doms().valuationDtl.txtPlinthSft.val(),
                    LandCost: _doms().valuationDtl.txtLandCost.val(),
                    Age: _doms().valuationDtl.txtAge.val(),
                    HoldingTypeGroupId: _doms().valuationDtl.ddlHoldingTypeGroup.val(),
                    SrchgTag: _doms().valuationDtl.SrchgTag.prop('checked'),
                    EduCessTag: _doms().valuationDtl.EduCessTag.prop('checked')
                }
            },
        }
    };
    var drpDwnevtHandlers = {
        onChangeValuation: function () {
            if (validators().validateValuation()) {
                var valuationData = formDatas().valuationdata();
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/CalCulateValuationDetails",
                    data: JSON.stringify(valuationData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        if (r.Status === 200) {
                            var data = r.Data;
                            _doms().valuationDtl.txtReadOnlyAnnualValuation.val(data.AnnualValuation);
                            _doms().valuationDtl.txtReadOnlyQtrPtax.val(data.Qtr_Ptax);
                            _doms().valuationDtl.txtReadOnlyQtrSchrg.val(data.Qtr_Schrg);
                            _doms().valuationDtl.txtLandCost.val(data.LandCost);
                            _doms().valuationDtl.txtReadonlyQtrEduCess.val(data.EduCessAmt);
                        } else {
                            alertify.error(r.Message);
                        }
                    },
                    failure: function (response) {
                        alertify.error(response);
                    }
                });
            }
        }
    };
    var enventHandlers = function () {
        return {
            onTableRowsAddOrRemoved: function (table) {
                var nexButtonOnHomeTab = constants().getTabScope(constants().getTabScope(table)).find(".nav-to-mother-holding");
                if (table.find('tbody tr.holding-data').length > 0) {
                    nexButtonOnHomeTab.show();
                } else {
                    nexButtonOnHomeTab.hide();
                }
            }
        }
    };
    var bindEvents = function () {
        $(document).on("click", "#valuationAddBtn", function () {

            var tempJson = {
                "HoldingTypeId": $('.ddl-holding-type').val(),
                "ZoneId": $('.ddl-zone').val(),
                "ZoneVal": $('.zoneText').text(),
                "LandArea_KT": $('.txt-land-area-kt').val(),
                "LandArea_CH": $('.txt-land-area-ch').val(),
                "LandArea_SFT": $('.txt-land-area-sft').val(),
                "NatureOfUseId": $('.ddl-nature-of-use').val(),
                "NatureOfUseVal": $('.NOUVal').text(),
                "BldgArea_SFT": $('.txt-bldg-area-sft').val(),
                "Plinth_SFT": $('.txt-plinth-sft').val(),
                "Age": $('.txt-age').val(),
                "ConstructionId": $('.ddl-construction').val(),
                "ConstructionVal": $('.constText').text(),
                "SrchgTag": $('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": $('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": $('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": $('.txt-readonly-qtr-schrg').val(),
                "LandCost": $('.txt-land-cost').val(),
                "Qtr_EduCess": $('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": $(".ddl-hodling-type-group").val(),
                "AssmtId": null,
                "AnulValDtlTabId": null,
                "EditMode": "I"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append($('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append($('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append($('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append($('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append($('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td class="hiddenAssmtID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + null + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "I" + '</td>');
            tr.append(td);

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);

            $('#valuation_details_table-1').find('>tbody').append(tr);
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            $('#valuation-details-1').modal('hide');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        });
        $(document).on('click', '.updateRow', function () {
            var row_index = $(this).closest('tr').index() + 2;
            $('.hiddenFieldForUdtate').val(row_index);
            $('#valuationAddBtn').hide();
            $('#valuationSaveBtn').show();
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            var rowDataJsonString = $(this).closest('tr').find('.completeJson').text();
            var rowDataJson = JSON.parse(rowDataJsonString);
            $('#valuation-details-1').modal('show');
            $('.ddl-holding-type').val(rowDataJson.HoldingTypeId),
                $('.ddl-zone').val(rowDataJson.ZoneId),
                $('.zoneText').text(rowDataJson.ZoneVal),
                $('.txt-land-area-kt').val(rowDataJson.LandArea_KT),
                $('.txt-land-area-ch').val(rowDataJson.LandArea_CH),
                $('.txt-land-area-sft').val(rowDataJson.LandArea_SFT),
                $('.ddl-nature-of-use').val(rowDataJson.NatureOfUseId),
                $('.NOUVal').text(rowDataJson.NatureOfUseVal),
                $('.txt-bldg-area-sft').val(rowDataJson.BldgArea_SFT),
                $('.txt-plinth-sft').val(rowDataJson.Plinth_SFT),
                $('.txt-age').val(rowDataJson.Age),
                $('.ddl-construction').val(rowDataJson.ConstructionId),
                $('.constText').text(rowDataJson.ConstructionVal),
                $(".chk-SrchgTag").prop("checked", rowDataJson.SrchgTag);
            $('.txt-readonly-annual-valuation').val(rowDataJson.AnnualValuation),
                $('.txt-readonly-qtr-ptax').val(rowDataJson.Qtr_Ptax),
                $('.txt-readonly-qtr-schrg').val(rowDataJson.Qtr_Schrg),
                $('.txt-land-cost').val(rowDataJson.LandCost),
                $('.txt-readonly-qtr-Edu-cess').val(rowDataJson.Qtr_EduCess),
                $('.hidden_assmt_id').val(rowDataJson.AssmtId),
                $('.hidden_tab_id').val(rowDataJson.AnulValDtlTabId)
        });
        $(document).on('click', '.rmvRow', function () {
            var xy = $(this).closest('tr');
            xy.remove();
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
        });
        $(document).on("click", "#valuationSaveBtn", function () {
            var rowIndex = parseInt($('.hiddenFieldForUdtate').val());
            $("#valuation_details_table-1 tr:eq(" + rowIndex + ")").remove();
            var tempJson = {
                "HoldingTypeId": $('.ddl-holding-type').val(),
                "ZoneId": $('.ddl-zone').val(),
                "ZoneVal": $('.zoneText').text(),
                "LandArea_KT": $('.txt-land-area-kt').val(),
                "LandArea_CH": $('.txt-land-area-ch').val(),
                "LandArea_SFT": $('.txt-land-area-sft').val(),
                "NatureOfUseId": $('.ddl-nature-of-use').val(),
                "NatureOfUseVal": $('.NOUVal').text(),
                "BldgArea_SFT": $('.txt-bldg-area-sft').val(),
                "Plinth_SFT": $('.txt-plinth-sft').val(),
                "Age": $('.txt-age').val(),
                "ConstructionId": $('.ddl-construction').val(),
                "ConstructionVal": $('.constText').text(),
                "SrchgTag": $('.chk-SrchgTag').is(":checked"),
                "AnnualValuation": $('.txt-readonly-annual-valuation').val(),
                "Qtr_Ptax": $('.txt-readonly-qtr-ptax').val(),
                "Qtr_Schrg": $('.txt-readonly-qtr-schrg').val(),
                "LandCost": $('.txt-land-cost').val(),
                "Qtr_EduCess": $('.txt-readonly-qtr-Edu-cess').val(),
                "HoldingTypeGroupId": $(".ddl-hodling-type-group").val(),
                "AssmtId": $(".hidden_assmt_id").val(),
                "AnulValDtlTabId": $(".hidden_tab_id").val(),
                "EditMode": "U"
            };
            tempJson = JSON.stringify(tempJson);

            var tr = $('<tr/>');
            //floor control
            var td = $('<td class="annual-val" />');
            tr.append(td.append($('.txt-readonly-annual-valuation').val()));

            //add usage control
            td = $('<td class="qtr-ptax" />');
            tr.append(td.append($('.txt-readonly-qtr-ptax').val()));

            //add Year control
            td = $('<td class="qtr-schrg" />');
            tr.append(td.append($('.txt-readonly-qtr-schrg').val()));

            //add covered area control
            td = $('<td class="land-cost" />');
            tr.append(td.append($('.txt-land-cost').val()));

            //add Occupent Name control
            td = $('<td class="qtr-Edu-cess" />');
            tr.append(td.append($('.txt-readonly-qtr-Edu-cess').val()));

            td = $('<td/>');
            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

            td = $('<td class="hiddenAssmtID" hidden>' + $(".hidden_assmt_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="hiddenTabID" hidden>' + $(".hidden_tab_id").val() + '</td>');
            tr.append(td);

            td = $('<td class="editMode" hidden>' + "U" + '</td>');
            tr.append(td);

            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
            tr.append(td);

            if ($('#valuation_details_table-1 > tbody > tr:first').length == 0) {
                $('#valuation_details_table-1').find('>tbody').append(tr);
                $('#valuation_details_table-1 > tbody > tr:first').find('.editMode').text("U");
            }
            else if ($('#valuation_details_table-1 > tbody > tr').length >= 1 && rowIndex < 3) {
                tr.prependTo('#valuation_details_table-1 > tbody');
                $('#valuation_details_table-1 > tbody > tr').eq(0).find('.editMode').text("U");
            }
            else {
                $("#valuation_details_table-1 tr:eq(" + (rowIndex - 1) + ")").after(tr);
                $("#valuation_details_table-1 tr:eq(" + rowIndex + ")").find('.editMode').text("U");
            }
            totalAnnualVal();
            totalEducess();
            totalQtrPtax();
            totalQtrScharge();
            totalLandCost();
            $('#valuation-details-1').modal('hide');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        });
        $(document).on('hidden.bs.modal', '#valuation-details-1', function () {
            $('.ddl-construction,.ddl-nature-of-use,.ddl-zone').val('');
            $('.ddl-holding-type').val(0);
            $('.txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-age,.txt-readonly-annual-valuation,.txt-land-cost,.txt-readonly-qtr-ptax,.txt-readonly-qtr-Edu-cess,.txt-readonly-qtr-schrg').val('0');
            $(".chk-SrchgTag").prop("checked", false);
            $('.zoneText,.NOUVal,.constText').text('0');
            $('#valuationAddBtn').show();
            $('#valuationSaveBtn').hide();
        })
        //holding details
        $(document).on('focus', ".txtWard", function () {
            var tabPane = $(document);
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { Ward: request.term };
                    _doms(tabPane).searchHolding.hdnWard.val('');
                    if (!request.term) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetWards",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.WardID,
                                            label: item.WardName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms(tabPane).searchHolding.hdnWard.val(i.item.value);
                    _doms(tabPane).searchHolding.txtWard.val(i.item.value);
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txtLocation", function () {
            var tabPane = $(document);
            $(this).autocomplete({
                source: function (request, response) {
                    var data = { WardId: _doms(tabPane).searchHolding.hdnWard.val(), Location: request.term };
                    _doms(tabPane).searchHolding.hdnLocation.val('');
                    if (!request.term || !_doms(tabPane).searchHolding.hdnWard.val()) {
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/Assessment/GetLocations",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Response) {
                            if (Response.Status === 200) {
                                var serverResponse = Response.Data;
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {
                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            value: item.LocationID,
                                            label: item.LocationName
                                        });
                                    });
                                }
                                response(userDataAutoComplete);
                            } else {
                                alertify.error("Error: " + Response.Message);
                            }
                        },
                        failure: function (response) {
                            alert(response);
                        }
                    });
                },
                select: function (e, i) {
                    _doms(tabPane).searchHolding.txtLocation.val(i.item.label);
                    _doms(tabPane).searchHolding.hdnLocation.val(i.item.value);
                    return false;
                },
                change: function (e, i) {
                    if (!i.item) {
                        //Resetors.ResetAssesse();
                    }
                }
            });
        });
        $(document).on('focus', ".txt-holding-no", function () {
            var tabPane = constants().getTabScope($(this));
            $(this).off('keyup');
            $(this).on('keyup', function () {
                tabPane.find(".hddn-holding-no").val($(this).val());
            });
        });
        $(document).on('click', '.btn-load-review-form', function (e) {
            e.preventDefault();
            var tabPane = constants().getTabScope($(this));
            var tabPaneId = tabPane.attr('id');
            if (tabPaneId == 'mother-holding') {
                tabPane.find(".complete-amalgamation").hide();
            }
            //empty modal content
            _doms(tabPane).modalSearchHolding.find('.modal-body').empty();
            //reset title
            helpers().SetTitleModal(_doms(tabPane).modalSearchHolding.find('.modal-body'), "");
            if (!validators().validateSearchHolding(tabPane)) {
                return;
            }
            var payload = { Holding: _doms(tabPane).searchHolding.hddnHoldingNo.val(), WardId: _doms(tabPane).searchHolding.hdnWard.val(), LocationID: _doms(tabPane).searchHolding.hdnLocation.val() };
            var payload1 = { HoldingNo: _doms(tabPane).searchHolding.hddnHoldingNo.val(), WardId: _doms(tabPane).searchHolding.hdnWard.val(), LocationID: _doms(tabPane).searchHolding.hdnLocation.val() };
            $.ajax({
                type: "POST",
                url: "/Municipality/Assessment/SearchHolding",
                data: JSON.stringify(payload),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (Response) {
                    if (Response.Status === 200 && Response.Data) {
                        var str = "";
                        str += "<div class='row'>";
                        str += "<div class='col-md-12'>";
                        str += "<label>Building /Land Details:</label>";
                        str += "<textarea class='txt-building-dtl form-control'>";
                        str += "</textarea>";
                        str += "</div>";
                        str += "</div>";
                        var descriptionField = str;
                        $('.holdingtypegroupId').val(Response.Data._HoldingDetails.HoldingTypeGroupId);
                        var Mothervaltag = Response.Data.AssessmentValTag;
                        var MotherLocId=Response.Data._HoldingDetails.LocationId;
                        var MotherwardId=Response.Data._HoldingDetails.WardId;
                        var MotherHoldingNo=Response.Data._HoldingDetails.HoldingNo;
                        if (tabPaneId == 'home') {
                            _doms(tabPane).modalSearchHolding.find('.modal-body').append(helpers().getModalDesign(Response.Data, tabPane));
                            helpers().SetTitleModal(_doms(tabPane).modalSearchHolding, "Details for holding- " + _doms(tabPane).searchHolding.hddnHoldingNo.val());
                            _doms(tabPane).modalSearchHolding.modal('show');
                        } else if (tabPaneId == 'mother-holding') {
                            //hdn-modal-raw-data
                            tabPane.find(".holding-detalis").empty().append(helpers().getModalDesign(Response.Data, tabPane) + str);
                            if (tabPane.find(".hdn-modal-raw-data").length > 0) {
                               // tabPane.find(".btn-complete-amalgamation").show();
                                $(".btn-complete-amalgamation").show()
                            }
                            $('textarea').each(function () {
                                $(this).val($(this).val().trim());
                            });
                            $.ajax({
                                type: 'POST',
                                url: "/Municipality/Assessment/GetValuationDetailsJsonData",
                                data: JSON.stringify({ ValTag: Mothervaltag, wardId: MotherwardId, LocationId: MotherLocId, HoldingNo: MotherHoldingNo }),
                                //data: JSON.stringify(payload),
                                contentType: "application/json;charset=utf-8",
                                dataType: "json",
                                success: function (result) {
                                    $('#valuation_details_table-1').find('>tbody').empty();
                                    if (result.Data != null) {
                                        for (i = 0; i < result.Data.length; i++) {
                                            var tempJson = result.Data[i];

                                            var tr = $('<tr/>');
                                            //floor control
                                            var td = $('<td class="annual-val" />');
                                            tr.append(td.append(tempJson.AnnualValuation));

                                            //add usage control
                                            td = $('<td class="qtr-ptax" />');
                                            tr.append(td.append(tempJson.Qtr_Ptax));

                                            //add Year control
                                            td = $('<td class="qtr-schrg" />');
                                            tr.append(td.append(tempJson.Qtr_Schrg));

                                            //add covered area control
                                            td = $('<td class="land-cost" />');
                                            tr.append(td.append(tempJson.LandCost));

                                            //add Occupent Name control
                                            td = $('<td class="qtr-Edu-cess" />');
                                            tr.append(td.append(tempJson.EduCessAmt));

                                            td = $('<td class="hiddenAssmtID" hidden>' + tempJson.AssmtId + '</td>');
                                            tr.append(td);

                                            td = $('<td class="hiddenTabID" hidden>' + tempJson.AnulValDtlTabId + '</td>');
                                            tr.append(td);

                                            td = $('<td class="editMode" hidden></td>');
                                            tr.append(td);

                                            td = $('<td/>');
                                            tr.append(td.append('<a class="rmvRow">Remove</a> &nbsp<a  class="updateRow";">Update</a>'));

                                            tempJson = JSON.stringify(tempJson);
                                            td = $('<td class="completeJson" hidden>' + tempJson + '</td>');
                                            tr.append(td);

                                            $('#valuation_details_table-1').find('>tbody').append(tr);
                                            totalAnnualVal();
                                            totalEducess();
                                            totalQtrPtax();
                                            totalQtrScharge();
                                            totalLandCost();
                                            $('#valuation-details-1').modal('hide');
                                            $('#valuationAddBtn').show();
                                            $('#valuationSaveBtn').hide();
                                        }
                                    }

                                },
                            });
                        }
                        $.ajax({
                            type: 'post',
                            url: "/Municipality/Assessment/GetOtherTabDetails",
                            data: JSON.stringify(payload1),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.Data != null) {
                                    if (response.Data._OtherTabDetails.ApplictnDt != null) {
                                        $('.txt-application-date').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.ApplictnDt));
                                    }
                                    $('.txt-application-no').val(response.Data._OtherTabDetails.ApplictnCaseNo);
                                    if (response.Data._OtherTabDetails.DtOfOrder != null) {
                                        $('.txt-date-of-order').val(CONVERTER.Date.convertCsToJsDate(response.Data._OtherTabDetails.DtOfOrder));
                                    }
                                    $('.txt-primary-phone-no').val(response.Data._OtherTabDetails.PrimaryPhNo);
                                    $('.txt-primary-email-id').val(response.Data._OtherTabDetails.PrimaryEmail);
                                    $('.txt-pro-cosing-fees').val(response.Data._OtherTabDetails.ProCosingFees);
                                    $('.txt-other-fees').val(response.Data._OtherTabDetails.OtherFees);
                                }
                            }
                        });
                        

                    } else {
                        alertify.error("Error: " + Response.Message);
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
            
        });

        //valudation details
        $(document).on('change', ".ddl-zone", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            drpDwnevtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-nature-of-use", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            drpDwnevtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-construction", function () {
            $(this).siblings(".input-group-addon").find('span').text((Number($(this).find('option:selected').attr('data-value'))).toFixed(2));
            drpDwnevtHandlers.onChangeValuation();
        });
        $(document).on('change', ".ddl-ownerOrOccupier", function () {
            if ($('.ddl-ownerOrOccupier').val() == 2) {
                $('.ownerOccupier').show();
            }
            else {
                $('.ownerOccupier').hide();
            }
        });
        $(document).on('change', ".txt-land-area-kt,.txt-land-area-ch,.txt-land-area-sft,.txt-bldg-area-sft,.txt-plinth-sft,.txt-land-cost,.txt-age,.chk-SrchgTag,.chk-EduCessTag", function () {
            drpDwnevtHandlers.onChangeValuation();
        });

        $(_doms().modalSearchHolding.find('.modal-footer .btn-include')).on('click', function (e) {
            e.preventDefault();
            var tabPane = constants().getTabScope($(this));
            var dataElement = _doms(tabPane).modalSearchHolding.find('.modal-body').find('.hdn-modal-raw-data');
            if (dataElement.length > 0) {
                var assmtData = JSON.parse(dataElement.val());
                var tablRowStr = helpers().getTableRowTamplete(assmtData, tabPane);
                var table = tabPane.find(".tbl-child-holding-list").find('table');
                if (table.find('tbody tr:not(.holding-data)').length > 0) {
                    table.find('tbody').empty();
                }
                table.find('tbody').append(tablRowStr);
                enventHandlers().onTableRowsAddOrRemoved(table)
                _doms(tabPane).modalSearchHolding.modal('hide');
            }
        });
        $(_doms().tabControl).on('click', '.remove-holding', function () {
            var curRow = $(this).closest('tr');
            var table = curRow.closest('table');
            curRow.remove();

            var tabPane = constants().getTabScope($(this));
            enventHandlers().onTableRowsAddOrRemoved(table);
            if (table.find('tbody tr.holding-data').length == 0) {
                table.find('tbody').append(helpers().getTableRowTamplete(null, tabPane));
            }
        });
        $(document).on('click', '.btn-complete-amalgamation', function (e) {
            e.preventDefault();
            try{
                validators().ValidatedataBeforeSave();
                var childHoldings = helpers().getChildHoldings();
                var motherHolding = helpers().getMotherHolding();
                var motherHoldingValuationDetails = helpers().getValuationArrayData();
                childHoldings = childHoldings.map(function (val) {
                    return helpers().processFormDataBeforSave(val);
                });
                motherHolding = helpers().processFormDataBeforSave(motherHolding);

                alertify.confirm('Confirm us!', "Are you sure to save data?", function () { 
                //decorate payload
                    var payload = { ChildHoldings: childHoldings, MohtherHolding: motherHolding, MotherHoldingValuationDetails: motherHoldingValuationDetails };
                //save data
                $.ajax({
                    type: "POST",
                    url: "/Municipality/Assessment/SaveAmalgamationData",
                    data: JSON.stringify(payload),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful", Response.Message, function () {
                                //location.reload();
                            });
                        } else {
                            alertify.alert("Error!","Error: " + Response.Message);
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
                }, function () { });
            } catch (e) {
                if (typeof e == "string") {
                    alertify.error(e);
                }
            }
        });
    };

    var InitiateOtherSetting = function () {
        
    };
    (function () {
        new bindEvents();
    }())
});
function totalAnnualVal() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.AnnualValuation);
        }
    });
    $('.lbl-total-annual-vl').text(totalVal);
}
function totalEducess() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_EduCess);
        }
    });
    $('.lbl-total-end-cess').text(totalVal);
}
function totalQtrPtax() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Ptax);
        }
    });
    $('.lbl-total-qtr-ptax').text(totalVal);
}
function totalQtrScharge() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.Qtr_Schrg);
        }
    });
    $('.lbl-total-qtr-scharge').text(totalVal);
}
function totalLandCost() {
    var tbl = $('.completeJson');
    var totalVal = 0.0;
    $.each(tbl, function (i, v) {
        var rowDataJsonString = v.innerHTML;
        var rowDataJson = JSON.parse(rowDataJsonString);
        if (rowDataJson.EditMode != "D") {
            totalVal = parseFloat(totalVal) + parseFloat(rowDataJson.LandCost);
        }
    });
    $('.lbl-total-last-cost').text(totalVal);
}
