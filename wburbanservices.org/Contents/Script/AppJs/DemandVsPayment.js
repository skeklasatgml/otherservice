﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
    };

    var APP_OBJECT = function (serverObject) {
        var appObj = this;
        var BindEvent = function () {
            $("#btnSave").on('click', ReportShow);
        };
        var ReportShow = function () {
            var data = GetDataFromView();
            if (ValidateData(data)) {
                $('#frmDemandVsPaymentReport').submit();
            }
        };
        var GetDataFromView = function () {
            var FinYear = $("#Finyear").val();
            var data = { FinYear: FinYear };
            return data;
        };
        var ValidateData = function (data) {
            if ($("#Finyear").val().length == 0) {
                alert("Financial Year Should not be Blank......!");
                return false;
            }
            return true;
        };
        this.app_initiate = function () {
            BindEvent();
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_initiate();
    };

    (function () {
        INIT();
    })();
});
