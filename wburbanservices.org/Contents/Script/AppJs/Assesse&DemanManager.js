﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetAssesseebyName = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    callback(response);
                },
                failure: function (response) {
                    alertify.error("http Response Error");
                }
            });

        };
        this.GetAssesseeByHodling = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnAssessee/GetAssesseesByHoldingSuggestion",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.GetWards = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetMappedWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data.Data);

                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        }
        this.GetLocations = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        }
        this.UpdateAssesseInfo = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/UpdateAssesse",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.InsertNewAssesse = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/InsertNewAssesse",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.GetAssesseeByID = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetAssesseeByID",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.SearchAssesseesByHolding = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetAssesseesByHoldings",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.GetArrearRecords = function (data, callBack) {
            
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/GetAllArrearBillRecord",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.UpdateArrear = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/UpdateArrear",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.InsertArrear = function(data, callBack){
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/InsertArrear",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.GetCurrentDemandRecords = function (data, callBack) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/GetDemandRecords",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.DemandUpdate = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/UpdateDemand",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.DemandInsert = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/InsertDemand",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };

        this.InsertReview = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/InsertReview",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alertify.error("http Response Error");
                }
            });
        };
        this.ViewReviewAllData = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/AssesseDemand/ReviewAllData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },                
                error: function (result) {
                    alertify.error(result.status);
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var appObj = this;
            
        var SearchComponent = function (_updateComponents) {
            var _parentSearchComponent = this;
            this.SearchedAssesse = null;
            var Lock = function () {
                DomControls.ctrl_btnSearch.attr('disabled', true);
                DomControls.ctrl_txtWard.attr('disabled', true);
                DomControls.ctrl_txtLocation.attr('disabled', true);
                DomControls.ctrl_txtHolding.attr('disabled', true);
            };
            var UnLock = function () {
                DomControls.ctrl_btnSearch.removeAttr('disabled');
                DomControls.ctrl_txtWard.removeAttr('disabled');
                DomControls.ctrl_txtLocation.removeAttr('disabled');
                DomControls.ctrl_txtHolding.removeAttr('disabled');
            };
            var scopeContainer = $("#assesse-updation .assesee-search-component");
            var DomControls = {
                ctrl_btnReset: scopeContainer.find(".btn-assesse-reset"),
                ctrl_btnSearch: scopeContainer.find(".btn-assesse-load"),
                ctrl_hdnWard: scopeContainer.find(".hdnWard"),
                ctrl_txtWard: scopeContainer.find(".txtWard"),
                ctrl_hdnLocation: scopeContainer.find(".hdnLocation"),
                ctrl_txtLocation: scopeContainer.find(".txtLocation"),
                ctrl_hdnHolding: scopeContainer.find(".hdnHolding"),
                ctrl_txtHolding: scopeContainer.find(".txtHolding"),
                ctrl_hdnAssessee: scopeContainer.find(".hdnAssesse"),
            };
            var Resetors = {
                ResetWard: function () {
                    DomControls.ctrl_hdnWard.val('');
                    DomControls.ctrl_txtWard.val('');
                },
                ResetLocation: function () {
                    DomControls.ctrl_hdnLocation.val('');
                    DomControls.ctrl_txtLocation.val('');
                },
                ResetAssesse: function () {
                    DomControls.ctrl_hdnAssessee.val('');
                },
                ResetHolding: function () {
                    DomControls.ctrl_hdnHolding.val('');
                    DomControls.ctrl_txtHolding.val('');
                },
                ResetForm: function () {
                    UnLock();

                    Resetors.ResetWard();
                    Resetors.ResetLocation();
                    Resetors.ResetAssesse();
                    Resetors.ResetHolding();
                    _parentSearchComponent.SearchedAssesse = null;
                }
            };
            var AutocompleteSorces = {
                Wards: function () {
                    return {
                        source: function (request, response) {
                            //reset area
                            DomControls.ctrl_hdnWard.val('');
                            Resetors.ResetLocation();
                            Resetors.ResetAssesse();
                            Resetors.ResetHolding();
                            //reset area
                            var data = { wardName: $.trim(request.term) };
                            serverObject.GetWards(data, function (serverResponse) {
                               
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.WardName,
                                            WardID: item.WardID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);

                            });
                        },
                        select: function (e, i) {
                            DomControls.ctrl_hdnWard.val(i.item.WardID);
                        },
                        change: function (e, i) {
                            if (!i.item) {
                                Resetors.ResetLocation();
                                Resetors.ResetAssesse();
                                Resetors.ResetHolding();
                            }
                        }
                    };
                },
                Locations: function () {
                    return {
                        source: function (request, response) {
                            //reset area
                            DomControls.ctrl_hdnLocation.val('');
                            Resetors.ResetAssesse();
                            Resetors.ResetHolding();
                            //reset area
                            var wardID = DomControls.ctrl_hdnWard.val();
                            var data = { wardID: wardID, locationName: $.trim(request.term) };
                            serverObject.GetLocations(data, function (serverResponse) {
                                if (serverResponse.Status === 200) {
                                    serverResponse = serverResponse.Data;
                                } else {
                                    alertify.notify('Error loding location: ' + serverResponse.Message, 'error', 5)
                                }
                                var userDataAutoComplete = [];
                                if ((serverResponse).length > 0) {

                                    $.each(serverResponse, function (index, item) {
                                        userDataAutoComplete.push({
                                            label: item.LocationName,
                                            LocationID: item.LocationID
                                        });
                                    });

                                }
                                response(userDataAutoComplete);
                            });
                        },
                        select: function (e, i) {
                            DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                        },
                        change: function (e, i) {
                            if (!i.item) {
                                Resetors.ResetAssesse();
                                Resetors.ResetHolding();
                            }
                        }
                    };
                },
                Holdings: function () {
                    return {
                        source: function (request, response) {
                            //reset area
                            Resetors.ResetAssesse();
                            //Resetors.ResetHolding();
                            _parentSearchComponent.SearchedAssesse = null;

                            //reset area
                            var locationID = DomControls.ctrl_hdnLocation.val();
                            var wardID = DomControls.ctrl_hdnWard.val();
                            var data = { wardID: wardID, LocationID: locationID, HoldingNo: request.term, TakeRowCount: 15 };
                            serverObject.GetAssesseeByHodling(data, function (Response) {
                                if (Response.Status === 200) {
                                    var serverResponse = Response.Data;
                                    var userDataAutoComplete = [];
                                    if ((serverResponse).length > 0) {

                                        $.each(serverResponse, function (index, item) {
                                            userDataAutoComplete.push({
                                                label: '<' + item.HoldingNo.trim() + '>' + item.AssesseeName,
                                                AssesseeID: item.AssesseeID,
                                                HoldingNo: item.HoldingNo.trim(),
                                                Obj: item
                                            });
                                        });

                                    }
                                    response(userDataAutoComplete);
                                } else {
                                    alert("Error: " + Response.Message);
                                }

                            });
                        },
                        select: function (e, i) {
                            DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                            DomControls.ctrl_hdnHolding.val(i.item.HoldingNo);
                            _parentSearchComponent.SearchedAssesse = i.item.Obj;
                            //$("#txtAssessee").val(i.item.label);
                        },
                        change: function (e, i) {
                            if (!i.item) {
                                Resetors.ResetAssesse();
                                Resetors.ResetHolding();
                                _parentSearchComponent.SearchedAssesse = null;
                            }
                        }
                    };
                }
            };
            var EventHandlers = {
                onSearchClick: function (e) {
                    e.preventDefault();
                    //validate before search
                    if (_parentSearchComponent.SearchedAssesse === null) {
                        alertify.notify('Assesse not found', 'error', 5);
                        return;
                    }
                    Lock();
                    _updateComponents.SetAssesse(_parentSearchComponent.SearchedAssesse);
                    _updateComponents.ShowComponent();
                    _updateComponents.PopulateDefaulComponent();
                    
                }, onResetClick: function (e) {
                    e.preventDefault();
                    UnLock();
                    Resetors.ResetForm();
                    _updateComponents.Reset();
                }
            };
            var RegisterComponent = function () {
                DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
                DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
                DomControls.ctrl_txtHolding.autocomplete(AutocompleteSorces.Holdings());
                DomControls.ctrl_btnSearch.off('click', EventHandlers.onSearchClick);
                DomControls.ctrl_btnSearch.on('click', EventHandlers.onSearchClick);
                DomControls.ctrl_btnReset.off('click', EventHandlers.onResetClick);
                DomControls.ctrl_btnReset.on('click', EventHandlers.onResetClick);
            };
            (function () {
                RegisterComponent();
            }());
        };
        var InsertComponent = function ()
        {
            var _parentInsertComponent = $(this);
            var scopContainer = $(".assesse-insert-form");
            var DomControl = {
                ctrl_txtAssesseName: scopContainer.find(".txtName"),
                ctrl_txtHoldingNo: scopContainer.find(".txtHolding"),
                ctrl_txtAddress: scopContainer.find(".txtAddress"),
                ctrl_txtEmail: scopContainer.find(".txtEmail"),
                ctrl_txtPhone: scopContainer.find(".txtPhone"),
                ctrl_ddlWard: scopContainer.find(".ddlWard"),
                ctrl_ddlLocation: scopContainer.find(".ddlLocation"),
                ctrl_ddlholdingType: scopContainer.find(".ddlHoldingType"),
                ctrl_btnSave: scopContainer.find(".btn-assesse-update"),
                ctrl_txtOldAssesseNO: scopContainer.find(".txtOldAssesseNo"),
                ctrl_txtRefNo: scopContainer.find(".txtRefNo"),
                ctrl_txtRefDate: scopContainer.find(".txtRefDate"),

                ctrl_frmAssesseUpdateForm: scopContainer

            };
            var bindLocation = function (wardID, selectedLocationID) {
                DomControl.ctrl_ddlLocation.find("option:not(:eq(0))").remove();
                if (wardID.trim() == "" || wardID == null) {
                    return;
                }
                var data = { wardID: wardID, locationName: "" };
                serverObject.GetLocations(data, function (response) {
                    if (response.Status === 200) {
                        var locations = response.Data;
                        $.each(locations, function (index, value) {
                            DomControl.ctrl_ddlLocation.append("<option value='" + value.LocationID + "'>" + value.LocationName + "</option>");
                        });
                        DomControl.ctrl_ddlLocation.find("option[value='" + selectedLocationID + "']").prop('selected', true);
                    } else {
                        alertify.notify('Error loding location: ' + response.Message, 'error', 5)
                    }
                });
            }
            this.ResetForm = function () {
                DomControl.ctrl_txtAssesseName.val('');
                DomControl.ctrl_txtHoldingNo.val('');
                DomControl.ctrl_txtAddress.val('');
                DomControl.ctrl_txtEmail.val('');
                DomControl.ctrl_txtPhone.val('');
                DomControl.ctrl_ddlLocation.find("option:not(:eq(0))").remove();
                DomControl.ctrl_ddlholdingType.find("option:eq(0)").prop('selected', true);
                DomControl.ctrl_ddlWard.find("option:eq(0)").prop('selected', true);
                DomControl.ctrl_hdnAssesseID.val('');
                DomControl.ctrl_txtOldAssesseNO.val('');
                DomControl.ctrl_txtRefDate.val('');
                DomControl.ctrl_txtRefNo.val('');
            };
            var validateForm = function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControl.ctrl_txtAssesseName.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_txtHoldingNo.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_ddlLocation.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_ddlholdingType.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_ddlWard.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_txtAddress.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_txtRefNo.prop('name')] = { required: true };
                vldObj.rules[DomControl.ctrl_txtRefDate.prop('name')] = { required: true };


                //set error messages
                vldObj.messages[DomControl.ctrl_txtAssesseName.prop('name')] = { required: "Please enter Name." };
                vldObj.messages[DomControl.ctrl_txtHoldingNo.prop('name')] = { required: "Please select Holding." };
                vldObj.messages[DomControl.ctrl_ddlLocation.prop('name')] = { required: "Please select location" };
                vldObj.messages[DomControl.ctrl_ddlholdingType.prop('name')] = { required: "Please select holding type" };
                vldObj.messages[DomControl.ctrl_ddlWard.prop('name')] = { required: "Please select ward" };
                vldObj.messages[DomControl.ctrl_txtAddress.prop('name')] = { required: "Please enter address" };
                vldObj.messages[DomControl.ctrl_txtRefNo.prop('name')] = { required: "Please Enter Reference No" };
                vldObj.messages[DomControl.ctrl_txtRefDate.prop('name')] = { required: "Please select Reference Date" };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                DomControl.ctrl_frmAssesseUpdateForm.validate(vldObj);
                //validate form and return true and false
                return DomControl.ctrl_frmAssesseUpdateForm.valid();
            }
            var SaveAssesseData = function (data) {
                //validate
                if (validateForm()) {
                    alertify.confirm("Confirm","Are you sure to insert this new assessee",
                        function () {
                            serverObject.InsertNewAssesse(data, function (response) {
                                if (response.Status === 200)
                                {
                                    alertify.success("Assesse successfully saved")
                                } else {
                                    alertify.error("Error: " + response.Message);
                                }
                            });
                        }, function () { });
                }
            };
            var GetModifiedFormData = function () {
                var oldAssesseeNo = DomControl.ctrl_txtOldAssesseNO.val();
                var assesseName = DomControl.ctrl_txtAssesseName.val();
                var HoldingNo = DomControl.ctrl_txtHoldingNo.val();
                var AssesseeAddress = DomControl.ctrl_txtAddress.val();
                var PrimaryEmail = DomControl.ctrl_txtEmail.val();
                var PrimaryPhNo = DomControl.ctrl_txtPhone.val();
                var LocationID = DomControl.ctrl_ddlLocation.val();
                var HoldingTypeID = DomControl.ctrl_ddlholdingType.val();
                var WardID = DomControl.ctrl_ddlWard.val();
                var refNo = DomControl.ctrl_txtRefNo.val();
                var refDate = DomControl.ctrl_txtRefDate.val();
                refDate = CONVERTER.Date.convertToDate(refDate.split('-')[0], refDate.split('-')[1], refDate.split('-')[2]);
                var data = {
                    OldAssesseeNo: oldAssesseeNo.trim(),
                    AssesseeName: assesseName.trim(),
                    HoldingNo: HoldingNo,
                    AssesseeAddress: AssesseeAddress,
                    PrimaryEmail: PrimaryEmail,
                    PrimaryPhNo: PrimaryPhNo,
                    LocationID: LocationID,
                    HoldingTypeID: HoldingTypeID,
                    WardID: WardID,
                    AssesseeID: null,
                    ReferenceNo: refNo,
                    ReferenceDate: refDate
                };
                return data;
            };
            var bindEvents = function () {
                DomControl.ctrl_ddlWard.off('change');
                DomControl.ctrl_ddlWard.on('change', function () {
                    bindLocation($(this).val(), null);
                });
                DomControl.ctrl_btnSave.off('click');
                DomControl.ctrl_btnSave.on('click', function (e) {
                    e.preventDefault();
                    SaveAssesseData(GetModifiedFormData());
                });
                scopContainer.on('focus', '.txtRefDate', function () {
                    $(this).datepicker({
                        dateFormat: 'yy-mm-dd',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        closeText: 'X',
                        showAnim: 'drop',
                        changeYear: true,
                        changeMonth: true,
                        duration: 'medium'
                    });
                });
            };
            (function () {
                bindEvents();
            }());
        }
        var updateComponents = function () {
            var _parentUpdateComponents = this;
            var _selectedAssesse = null;
            var scopContainer = $("#assesse-updation .assesse-updation-forms");
            var SetTitle = function (title) {
                var _titleBar = scopContainer.find(".card-header");
                var _title = title;
                if (_selectedAssesse != null) {
                    _title = title + "- [" + _selectedAssesse.AssesseeName + "<" + _selectedAssesse.HoldingNo + ">]" ;
                }
                _titleBar.text(_title);
            };
            this.AssesseUpdateForm =new function() {
                var DomControl = {
                    ctrl_txtAssesseName: scopContainer.find(".assesse-update-form").find(".txtName"),
                    ctrl_txtHoldingNo: scopContainer.find(".assesse-update-form").find(".txtHolding"),
                    ctrl_txtAddress: scopContainer.find(".assesse-update-form").find(".txtAddress"),
                    ctrl_txtEmail: scopContainer.find(".assesse-update-form").find(".txtEmail"),
                    ctrl_txtPhone: scopContainer.find(".assesse-update-form").find(".txtPhone"),
                    ctrl_ddlWard: scopContainer.find(".assesse-update-form").find(".ddlWard"),
                    ctrl_ddlLocation: scopContainer.find(".assesse-update-form").find(".ddlLocation"),
                    ctrl_ddlholdingType: scopContainer.find(".assesse-update-form").find(".ddlHoldingType"), 
                    ctrl_btnSave: scopContainer.find(".assesse-update-form").find(".btn-assesse-update"),
                    ctrl_hdnAssesseID: scopContainer.find(".assesse-update-form").find(".hdnAssesse"),
                    ctrl_txtOldAssesseNO: scopContainer.find(".assesse-update-form").find(".txtOldAssesseNo"),
                    ctrl_frmAssesseUpdateForm: scopContainer.find(".assesse-update-form"),
                    ctrl_txtRefNo: scopContainer.find(".txtRefNo"),
                    ctrl_txtRefDate: scopContainer.find(".txtRefDate"),
                    ctrl_chkIsDisputed: scopContainer.find(".chkIsDisputed"),
                    ctrl_txtDisputeReason: scopContainer.find(".txtDisputeReason")
                    
                };
                var bindLocation = function (wardID,selectedLocationID)
                {
                    DomControl.ctrl_ddlLocation.find("option:not(:eq(0))").remove();
                    if (wardID.trim() == "" || wardID == null)
                    {
                        return;
                    }
                    var data = { wardID: wardID, locationName: "" };
                    serverObject.GetLocations(data, function (response) {
                        if (response.Status === 200)
                        {
                            var locations = response.Data;
                            $.each(locations, function (index, value) {
                                DomControl.ctrl_ddlLocation.append("<option value='" + value.LocationID + "'>" + value.LocationName + "</option>");
                            });
                            DomControl.ctrl_ddlLocation.find("option[value='" + selectedLocationID + "']").prop('selected', true);
                        } else {
                            alertify.notify('Error loding location: ' + response.Message, 'error', 5)
                        }
                    });
                }
                
                this.SetAssesseOnForm = function () {
                    var assesseName = _selectedAssesse.AssesseeName == null ? "" : _selectedAssesse.AssesseeName;
                    var HoldingNo = _selectedAssesse.HoldingNo == null ? "" : _selectedAssesse.HoldingNo;
                    var AssesseeAddress = _selectedAssesse.AssesseeAddress == null ? "" : _selectedAssesse.AssesseeAddress;
                    var PrimaryEmail = _selectedAssesse.PrimaryEmail == null ? "" : _selectedAssesse.PrimaryEmail;
                    var PrimaryPhNo = _selectedAssesse.PrimaryPhNo == null ? "" : _selectedAssesse.PrimaryPhNo;
                    var LocationID = _selectedAssesse.LocationID == null ? "" : _selectedAssesse.LocationID;
                    var HoldingTypeID = _selectedAssesse.HoldingTypeID == null ? "" : _selectedAssesse.HoldingTypeID;
                    var WardID = _selectedAssesse.WardID == null ? "" : _selectedAssesse.WardID;
                    var AssesseeID = _selectedAssesse.AssesseeID == null ? "" : _selectedAssesse.AssesseeID;
                    var OldAssesseeNo = _selectedAssesse.OldAssesseeNo == null ? "" : _selectedAssesse.OldAssesseeNo;
                    var IsDisputed = _selectedAssesse.IsDisputed;
                    var DisputeReason = _selectedAssesse.DisputeReason;

                    DomControl.ctrl_txtAssesseName.val(assesseName.trim());
                    DomControl.ctrl_txtHoldingNo.val(HoldingNo.trim());
                    DomControl.ctrl_txtAddress.val(AssesseeAddress.trim());
                    DomControl.ctrl_txtEmail.val(PrimaryEmail.trim());
                    DomControl.ctrl_txtPhone.val(PrimaryPhNo.trim());
                    DomControl.ctrl_ddlholdingType.find("option[value='" + HoldingTypeID + "']").prop('selected', true);
                    DomControl.ctrl_ddlWard.find("option[value='" + WardID + "']").prop('selected', true);
                    DomControl.ctrl_ddlWard.trigger("change");
                    //location will be selected on bindLocation()
                    DomControl.ctrl_hdnAssesseID.val(AssesseeID);
                    DomControl.ctrl_txtOldAssesseNO.val(OldAssesseeNo);
                    DomControl.ctrl_chkIsDisputed.prop('checked', IsDisputed);
                    DomControl.ctrl_txtDisputeReason.val(DisputeReason);
                    DomControl.ctrl_txtDisputeReason.prop('readonly', !IsDisputed);
                }
                this.ResetForm = function () {
                    DomControl.ctrl_txtAssesseName.val('');
                    DomControl.ctrl_txtHoldingNo.val('');
                    DomControl.ctrl_txtAddress.val('');
                    DomControl.ctrl_txtEmail.val('');
                    DomControl.ctrl_txtPhone.val('');
                    DomControl.ctrl_ddlLocation.find("option:not(:eq(0))").remove();
                    DomControl.ctrl_ddlholdingType.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlWard.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_hdnAssesseID.val('');
                    DomControl.ctrl_txtOldAssesseNO.val('');
                    DomControl.ctrl_txtRefDate.val('');
                    DomControl.ctrl_txtRefNo.val('');
                    DomControl.ctrl_chkIsDisputed.prop('checked', false);
                    DomControl.ctrl_txtDisputeReason.val('');
                };
                var validateForm = function () {
                    var vldObj = {
                        rules: {},
                        messages: {},
                        errorPlacement: {}
                    };
                    //enable hidden field vallidation
                    vldObj.ignore = [];

                    //set rules
                    vldObj.rules[DomControl.ctrl_txtAssesseName.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtHoldingNo.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlLocation.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlholdingType.prop('name')] = { required: true};
                    vldObj.rules[DomControl.ctrl_ddlWard.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtAddress.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtRefNo.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtRefDate.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtDisputeReason.prop('name')] = {
                        required: 
                        function (element) {
                            return DomControl.ctrl_chkIsDisputed.is(":checked");
                        }
                    };

                    //set error messages
                    vldObj.messages[DomControl.ctrl_txtAssesseName.prop('name')] = { required: "Please enter Name." };
                    vldObj.messages[DomControl.ctrl_txtHoldingNo.prop('name')] = { required: "Please select Holding." };
                    vldObj.messages[DomControl.ctrl_ddlLocation.prop('name')] = { required: "Please select location" };
                    vldObj.messages[DomControl.ctrl_ddlholdingType.prop('name')] = { required: "Please select holding type" };
                    vldObj.messages[DomControl.ctrl_ddlWard.prop('name')] = { required: "Please select ward" };
                    vldObj.messages[DomControl.ctrl_txtAddress.prop('name')] = { required: "Please enter address" };
                    vldObj.messages[DomControl.ctrl_txtRefNo.prop('name')] = { required: "Please Enter Reference No" };
                    vldObj.messages[DomControl.ctrl_txtRefDate.prop('name')] = { required: "Please select Reference Date" };
                    vldObj.messages[DomControl.ctrl_txtDisputeReason.prop('name')] = { required: "Please enter dispute reason" };

                    //place errors in position
                    vldObj.errorPlacement = function (error, element) {
                        var parent = $(element).parent(".input-group");
                        var parent1 = element.parent(".input-group");
                        if ($(element).parent(".input-group").length > 0) {
                            error.insertAfter(parent);
                           // $(element).closest('col-md-12').append(error);
                        } else {
                            error.insertAfter(element);
                        }
                        error.css('color', 'red');
                    }

                    //push validator object to tergate form
                    DomControl.ctrl_frmAssesseUpdateForm.validate(vldObj);
                    //validate form and return true and false
                    return DomControl.ctrl_frmAssesseUpdateForm.valid();
                }
                var SaveAssesseData = function (data) {
                    //validate
                    if (validateForm()) {
                        alertify.confirm("Confirm","Are you sure to Update this assessee?",
                            function () {
                                serverObject.UpdateAssesseInfo(data, function (response) {
                                    if (response.Status === 200) {
                                        alertify.success("Assesse successfully Updated")
                                    } else {
                                        alertify.error("Error: " + response.Message);
                                    }
                                });
                            }, function () { });
                    } else {
                        alertify.error("Form validation failed")
                    }
                };
                var GetModifiedFormData = function () {
                    var assesseName = DomControl.ctrl_txtAssesseName.val();
                    var HoldingNo = DomControl.ctrl_txtHoldingNo.val();
                    var AssesseeAddress = DomControl.ctrl_txtAddress.val();
                    var PrimaryEmail = DomControl.ctrl_txtEmail.val();
                    var PrimaryPhNo = DomControl.ctrl_txtPhone.val();
                    var LocationID = DomControl.ctrl_ddlLocation.val();
                    var HoldingTypeID = DomControl.ctrl_ddlholdingType.val();
                    var WardID = DomControl.ctrl_ddlWard.val();
                    var AssesseeID = _selectedAssesse.AssesseeID;
                    var oldAssesseeNo = DomControl.ctrl_txtOldAssesseNO.val();
                    var refNo = DomControl.ctrl_txtRefNo.val();
                    var refDate = DomControl.ctrl_txtRefDate.val();
                    var IsDisputed = DomControl.ctrl_chkIsDisputed.is(":checked");
                    var disputeReason = DomControl.ctrl_txtDisputeReason.val();
                    refDate = CONVERTER.Date.convertToDate(refDate.split('-')[0], refDate.split('-')[1], refDate.split('-')[2])
                    var data = {
                        AssesseeName: assesseName.trim(),
                        HoldingNo: HoldingNo,
                        AssesseeAddress: AssesseeAddress,
                        PrimaryEmail: PrimaryEmail,
                        PrimaryPhNo: PrimaryPhNo,
                        LocationID: LocationID,
                        HoldingTypeID: HoldingTypeID,
                        WardID: WardID,
                        AssesseeID: AssesseeID,
                        OldAssesseeNo: oldAssesseeNo,
                        ReferenceNo: refNo,
                        ReferenceDate: refDate,
                        IsDisputed: IsDisputed,
                        DisputeReason: disputeReason
                    };
                    return data;
                };
                this.bindEvents = function () {
                    DomControl.ctrl_frmAssesseUpdateForm.on('focus', '.txtRefDate', function () {
                        $(this).datepicker({
                            dateFormat: 'yy-mm-dd',
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            closeText: 'X',
                            showAnim: 'drop',
                            changeYear: true,
                            changeMonth: true,
                            duration: 'medium'
                        });
                    });
                    DomControl.ctrl_ddlWard.off('change');
                    DomControl.ctrl_ddlWard.on('change', function () {
                        bindLocation($(this).val(), _selectedAssesse.LocationID);
                    });
                    DomControl.ctrl_btnSave.off('click');
                    DomControl.ctrl_btnSave.on('click', function (e) {
                        e.preventDefault();
                        SaveAssesseData(GetModifiedFormData());
                    });
                    DomControl.ctrl_chkIsDisputed.on('change', function () {
                        if ($(this).is(":checked") == false) {
                            DomControl.ctrl_txtDisputeReason.val('');
                            DomControl.ctrl_txtDisputeReason.prop('readonly', true);
                        } 
                        DomControl.ctrl_txtDisputeReason.prop('readonly', !$(this).is(":checked"));
                    });
                }
            };
            this.ArrearUpdateForm =new function () {
              var  _parentArrearUpdateForm = this;

                var DomControl = {
                    ctrl_txtPropertyTaxAmount: scopContainer.find(".arrear-update-form").find(".txt-property-tax"),
                    ctrl_txtSurchargeAmount: scopContainer.find(".arrear-update-form").find(".txt-surcharge-tax"),
                    ctrl_btnSaveSingle: scopContainer.find(".arrear-update-form").find(".btn-save-single"),
                    ctrl_ChkAllPropertytax: scopContainer.find(".arrear-update-form").find(".check-all-property-tax"),
                    ctrl_ChkAllSurcharge: scopContainer.find(".arrear-update-form").find(".check-all-surcharge"),
                    ctrl_btnSaveAll: scopContainer.find(".arrear-update-form").find(".btn-save-all"),
                    ctrl_frmArrearUpdateForm: scopContainer.find(".arrear-update-form")
                };
                this.PopulateUpdateArrearData = function () {
                    if (_selectedAssesse == null)
                    {
                        alertify.error("Please select assessee");
                        return;
                    }
                    var warid = _selectedAssesse.WardID;
                    var assesseeID = _selectedAssesse.AssesseeID;
                    var locationID = _selectedAssesse.LocationID;
                    _parentArrearUpdateForm.ResetForm();
                    var data = { WardID:warid, LocationID:locationID, AssesseeID:assesseeID };
                    serverObject.GetArrearRecords(data, function (response) {
                        if (response.Status === 200)
                        {
                            var datas = response.Data;
                            if (datas.length > 0)
                            {
                                $.each(datas, function (index, value) {
                                    AddRowToTable(GetRowStringTamplate(++index, value.FinYear, value.QtrNo, value.PropertyTaxValue, value.SurchargeValue, value.OSPropertyTaxAmt, value.OSSurchargeAmt, (value.OSPropertyTaxAmt + value.OSSurchargeAmt) <= 0));
                                });
                                
                            } else {
                                AddRowToTable(GetNoRecordFounRowString());
                            }
                        } else {
                            alertify.error("Error: " + response.Message);
                        }
                    });
                };
                var GetRowStringTamplate = function (rowID, finyear, qtr, propertyTaxAmnt, surchargAmnt, osPropertyTaxAmnt, osSurchargAmnt, isReadOnly) {
                    var rowString = "<tr data-fin-year='" + finyear + "' data-qtr='" + qtr + "' data-read-only='" + isReadOnly+"'>";
                    rowString += "<td>" + finyear + "</td>";
                    rowString += "<td>" + qtr + "</td>";
                    rowString += "<td><input type='text' " + (isReadOnly == true?'disabled':'') + " class='txt-property-tax form-control' value='" + propertyTaxAmnt + "'/></td>";
                    rowString += "<td><input type='text'  " + (isReadOnly == true ? 'disabled' : '') + "  value='" + surchargAmnt + "' class='txt-surcharge-tax form-control'/></td>";
                    rowString += "<td>" + osPropertyTaxAmnt +"</td>";
                    rowString += "<td>" + osSurchargAmnt + "</td>";
                    //rowString += "<td>" + (isReadOnly == true ? '' : "<button type='button'    class='btn btn-default btn-xs btn-save-single'>Save</button>")+"</td>"
                    rowString += "</tr>";
                    return rowString;
                };
                var AddRowToTable = function (rowstring) {
                    DomControl.ctrl_frmArrearUpdateForm.find('table tbody').append(rowstring);
                };
                var GetNoRecordFounRowString = function () {
                    return "<tr><td colspan='7'>No records found</td></tr>";
                };
                var GetRowData = function (rowObj) {
                    var finyear = rowObj.attr("data-fin-year");
                    var qtr = rowObj.attr("data-qtr");
                    var propertyTax = rowObj.find(".txt-property-tax").val();
                    var surcharge = rowObj.find(".txt-surcharge-tax").val();

                    var data = {
                        FinYear: finyear,
                        Qtr: qtr,
                        PropertyTaxAmnt: propertyTax,
                        SurchargeAmnt: surcharge
                    };
                    return data;
                };
                this.ResetForm = function () {
                    DomControl.ctrl_frmArrearUpdateForm.find('table tbody').empty();
                };
                var ValidateRow = function (rowDatas) {
                    var flag = true;
                    $.each(rowDatas, function (index, value) {
                        var row = DomControl.ctrl_frmArrearUpdateForm.find('table tbody').find("tr[data-fin-year='" + value.FinYear + "'][data-qtr='" + value.Qtr + "']");
                        row.removeClass("error-element");
                        if (isNaN(value.PropertyTaxAmnt) || value.PropertyTaxAmnt<0)
                        {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                        if (isNaN(value.SurchargeAmnt) || value.SurchargeAmnt < 0)
                        {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                    });

                    return flag;
                };
                var BindEvents = function () {
                    scopContainer.find(".arrear-update-form").on('click', '.btn-save-single', function (e) {
                        e.preventDefault();
                        var row = $(this).closest('tr');
                        if (row.length == 0) {
                            return;
                        }
                        var rowData = GetRowData(row);
                        var rowDatas = [];
                        rowDatas.push(rowData);
                        if (ValidateRow(rowDatas))
                        {
                            alertify.confirm("Confirm","Are you sure to update arrear data?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.UpdateArrear(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentArrearUpdateForm.PopulateUpdateArrearData();
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }

                    });

                    DomControl.ctrl_frmArrearUpdateForm.on('click', '.btn-save-all', function (e) {
                        e.preventDefault();

                        var rows = DomControl.ctrl_frmArrearUpdateForm.find("table tbody tr[data-read-only='false']");
                        if (rows.length == 0) {
                            return;
                        }
                        var rowDatas = [];
                        $.each(rows, function (index, value) {
                            var rowData = GetRowData($(value));
                            rowDatas.push(rowData);
                        });
                        if (ValidateRow(rowDatas)) {
                            alertify.confirm("Confirm","Are you sure to update arrear datas?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.UpdateArrear(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentArrearUpdateForm.PopulateUpdateArrearData();
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }
                    });
                    scopContainer.find(".arrear-update-form").on('keyup', '.txt-property-tax', function (e) {
                        if (isNaN($(this).val()))
                        {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllPropertytax.prop("checked") === true)
                        {
                            DomControl.ctrl_frmArrearUpdateForm.find("table tbody tr[data-read-only='false']").find(".txt-property-tax").val($(this).val());
                        }
                    });

                    scopContainer.find(".arrear-update-form").on('keyup', '.txt-surcharge-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllSurcharge.prop("checked") === true) {
                            DomControl.ctrl_frmArrearUpdateForm.find("table tbody tr[data-read-only='false']").find(".txt-surcharge-tax").val($(this).val());
                        }
                    });
                    
                };
                (function () {
                    BindEvents();
                }());
            };
            this.ArrearInsertForm = new function () {
               var _parentArrearInsertForm = this;
                
                var DomControl = {
                    ctrl_ddlFromFineYear: scopContainer.find(".arrear-insert-form").find(".ddl-from-fin-year"),
                    ctrl_ddlFromQtr: scopContainer.find(".arrear-insert-form").find(".ddl-from-qtr"),

                    ctrl_ddlToFineYear: scopContainer.find(".arrear-insert-form").find(".ddl-to-fin-year"),
                    ctrl_ddlToQtr: scopContainer.find(".arrear-insert-form").find(".ddl-to-qtr"),

                    ctrl_txtPropertyTaxAmount: scopContainer.find(".arrear-insert-form").find(".txt-property-tax-amnt-per-qtr"),
                    ctrl_txtSurchargeAmount: scopContainer.find(".arrear-insert-form").find(".txt-surcharge-amnt-per-qtr"),
                    ctrl_btnSave: scopContainer.find(".arrear-insert-form").find(".btn-save"),
                    ctrl_frmArrearInsertForm: scopContainer.find(".arrear-insert-form")
                };
                
                var getFormData = function () {
                    return {
                        FromFinYearQtr: {
                            StartYear: Number(DomControl.ctrl_ddlFromFineYear.val().split("-")[0]),
                            EndYear: Number(DomControl.ctrl_ddlFromFineYear.val().split("-")[1]),
                            Qtr: Number( DomControl.ctrl_ddlFromQtr.val())
                        },
                        ToFinYearQtr: {
                            StartYear: Number(DomControl.ctrl_ddlToFineYear.val().split("-")[0]),
                            EndYear: Number(DomControl.ctrl_ddlToFineYear.val().split("-")[1]),
                            Qtr: Number(DomControl.ctrl_ddlToQtr.val())
                        },
                        PTax: Number(DomControl.ctrl_txtPropertyTaxAmount.val()),
                        SCh: Number( DomControl.ctrl_txtSurchargeAmount.val()),
                        AssesseeId: _selectedAssesse.AssesseeID
                    };
                };
                this.ResetForm = function () {
                    DomControl.ctrl_ddlFromFineYear.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlFromQtr.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlToFineYear.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlToQtr.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_txtPropertyTaxAmount.val('');
                    DomControl.ctrl_txtSurchargeAmount.val('');
                };
                var ValidateForm = function () {
                    var vldObj = {
                        rules: {},
                        messages: {},
                        errorPlacement: {}
                    };
                    //enable hidden field vallidation
                    vldObj.ignore = [];

                    //set rules
                    vldObj.rules[DomControl.ctrl_ddlFromFineYear.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlFromQtr.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlToFineYear.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlToQtr.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtPropertyTaxAmount.prop('name')] = { required: true, number:true };
                    vldObj.rules[DomControl.ctrl_txtSurchargeAmount.prop('name')] = { required: true, number:true };

                    //set error messages
                    vldObj.messages[DomControl.ctrl_ddlFromFineYear.prop('name')] = { required: "Please select From Fin-Year." };
                    vldObj.messages[DomControl.ctrl_ddlFromQtr.prop('name')] = { required: "Please select From Qtr." };
                    vldObj.messages[DomControl.ctrl_ddlToFineYear.prop('name')] = { required: "Please select To Fin-Year" };
                    vldObj.messages[DomControl.ctrl_ddlToQtr.prop('name')] = { required: "Please select To Qtr" };
                    vldObj.messages[DomControl.ctrl_txtPropertyTaxAmount.prop('name')] = { required: "Please enter Property Tax Amount", number: "Please enter numeric value" };
                    vldObj.messages[DomControl.ctrl_txtSurchargeAmount.prop('name')] = { required: "Please enter Surcharge Amount", number: "Please enter numeric value" };

                    //place errors in position
                    vldObj.errorPlacement = function (error, element) {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                    //push validator object to tergate form
                    DomControl.ctrl_frmArrearInsertForm.validate(vldObj);
                    //validate form and return true and false
                    return DomControl.ctrl_frmArrearInsertForm.valid();
                };
                var BindEvents = function () {
                    DomControl.ctrl_frmArrearInsertForm.on('click', ".btn-save", function (e) {
                        DomControl.ctrl_frmArrearInsertForm.find(".error-list").empty();
                        e.preventDefault();
                        if (ValidateForm()) {
                            alertify.confirm("Confirm", "Are you sure to insert arrear data?",
                                function () {
                                    var formData = getFormData();
                                    serverObject.InsertArrear(formData, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentArrearInsertForm.ResetForm();
                                        } else {
                                            alertify.error(response.Message);
                                            var respData = response.Data;
                                             if (response.Data != null && respData.hasOwnProperty("ErrorCode") && respData.hasOwnProperty("ErrorData") && respData.ErrorCode == "InsertArrear.InvalidFinyearsQtr") {
                                                $.each(respData.ErrorData, function (index, value) {
                                                    DomControl.ctrl_frmArrearInsertForm.find(".error-list").append("<li>" + "Arrear Demand for " + value.StartYear + "-" + value.EndYear + "," + value.Qtr+" is already inserted" + "</li>")
                                                })
                                            }
                                        }
                                    });
                                }
                                , function () {
                                    //calcel button clicked
                                });
                        }
                    });
                };
                (function () {
                    BindEvents();
                }());
            };
            this.DemanUpdateForm = new function () {
               var _parentArrearUpdateForm = this;

                var DomControl = {
                    ctrl_txtPropertyTaxAmount: scopContainer.find(".demand-update-form").find(".txt-property-tax"),
                    ctrl_txtSurchargeAmount: scopContainer.find(".demand-update-form").find(".txt-surcharge-tax"),
                    ctrl_btnSaveSingle: scopContainer.find(".demand-update-form").find(".btn-save-single"),
                    ctrl_ChkAllPropertytax: scopContainer.find(".demand-update-form").find(".check-all-property-tax"),
                    ctrl_ChkAllSurcharge: scopContainer.find(".demand-update-form").find(".check-all-surcharge"),
                    ctrl_btnSaveAll: scopContainer.find(".demand-update-form").find(".btn-save-all"),
                    ctrl_frmDemandUpdateForm: scopContainer.find(".demand-update-form")
                };
                this.PopulateUpdateDemandData = function () {
                    if (_selectedAssesse == null) {
                        alertify.error("Please select assessee");
                        return;
                    }
                    var warid = _selectedAssesse.WardID;
                    var assesseeID = _selectedAssesse.AssesseeID;
                    var locationID = _selectedAssesse.LocationID;
                    _parentArrearUpdateForm.ResetForm();
                    var data = { WardID: warid, LocationID: locationID, AssesseeID: assesseeID };
                    serverObject.GetCurrentDemandRecords(data, function (response) {
                        if (response.Status === 200) {
                            var datas = response.Data;
                            if (datas.length > 0) {
                                $.each(datas, function (index, value) {
                                    AddRowToTable(GetRowStringTamplate(++index, value.FinYear, value.QtrNo, value.PropertyTaxValue, value.SurchargeValue, value.OSPropertyTaxAmt, value.OSSurchargeAmt, (value.OSPropertyTaxAmt + value.OSSurchargeAmt) <= 0));
                                });

                            } else {
                                AddRowToTable(GetNoRecordFounRowString());
                            }
                        } else {
                            alertify.error("Error: " + response.Message);
                        }
                    });
                };
                var GetRowStringTamplate = function (rowID, finyear, qtr, propertyTaxAmnt, surchargAmnt, osPropertyTaxAmnt, osSurchargAmnt, isReadOnly) {
                    var rowString = "<tr data-fin-year='" + finyear + "' data-qtr='" + qtr + "' data-read-only='" + isReadOnly + "'>";
                    rowString += "<td>" + finyear + "</td>";
                    rowString += "<td>" + qtr + "</td>";
                    rowString += "<td><input type='text' " + (isReadOnly == true ? 'disabled' : '') + " class='txt-property-tax form-control' value='" + propertyTaxAmnt + "'/></td>";
                    rowString += "<td><input type='text'  " + (isReadOnly == true ? 'disabled' : '') + "  value='" + surchargAmnt + "' class='txt-surcharge-tax form-control'/></td>";
                    rowString += "<td>" + osPropertyTaxAmnt + "</td>";
                    rowString += "<td>" + osSurchargAmnt + "</td>";
                    //rowString += "<td>" + (isReadOnly == true ? '' : "<button type='button'    class='btn btn-default btn-xs btn-save-single'>Save</button>") + "</td>"
                    rowString += "</tr>";
                    return rowString;
                };
                var AddRowToTable = function (rowstring) {
                    DomControl.ctrl_frmDemandUpdateForm.find('table tbody').append(rowstring);
                };
                var GetNoRecordFounRowString = function () {
                    return "<tr><td colspan='7'>No records found</td></tr>";
                };
                var GetRowData = function (rowObj) {
                    var finyear = rowObj.attr("data-fin-year");
                    var qtr = rowObj.attr("data-qtr");
                    var propertyTax = rowObj.find(".txt-property-tax").val();
                    var surcharge = rowObj.find(".txt-surcharge-tax").val();

                    var data = {
                        FinYear: finyear,
                        Qtr: qtr,
                        PropertyTaxAmnt: propertyTax,
                        SurchargeAmnt: surcharge
                    };
                    return data;
                };
                this.ResetForm = function () {
                    var tbl = DomControl.ctrl_frmDemandUpdateForm.find('table tbody');
                    tbl.empty();
                };
                var ValidateRow = function (rowDatas) {
                    var flag = true;
                    $.each(rowDatas, function (index, value) {
                        var row = DomControl.ctrl_frmDemandUpdateForm.find('table tbody').find("tr[data-fin-year='" + value.FinYear + "'][data-qtr='" + value.Qtr + "']");
                        row.removeClass("error-element");
                        if (isNaN(value.PropertyTaxAmnt) || value.PropertyTaxAmnt < 0) {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                        if (isNaN(value.SurchargeAmnt) || value.SurchargeAmnt < 0) {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                    });

                    return flag;
                };
                var BindEvents = function () {
                    scopContainer.find(".demand-update-form").on('click', '.btn-save-single', function (e) {
                        e.preventDefault();
                        var row = $(this).closest('tr');
                        if (row.length == 0) {
                            return;
                        }
                        var rowData = GetRowData(row);
                        var rowDatas = [];
                        rowDatas.push(rowData);
                        if (ValidateRow(rowDatas)) {
                            alertify.confirm("Confirm", "Are you sure to update Demand data?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.DemandUpdate(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentArrearUpdateForm.PopulateUpdateDemandData();
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }

                    });

                    DomControl.ctrl_frmDemandUpdateForm.on('click', '.btn-save-all', function (e) {
                        e.preventDefault();

                        var rows = DomControl.ctrl_frmDemandUpdateForm.find("table tbody tr[data-read-only='false']");
                        if (rows.length == 0) {
                            return;
                        }
                        var rowDatas = [];
                        $.each(rows, function (index, value) {
                            var rowData = GetRowData($(value));
                            rowDatas.push(rowData);
                        });
                        if (ValidateRow(rowDatas)) {
                            alertify.confirm("Confirm", "Are you sure to update Demand datas?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.DemandUpdate(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentArrearUpdateForm.PopulateUpdateDemandData();
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }
                    });
                    scopContainer.find(".demand-update-form").on('keyup', '.txt-property-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllPropertytax.prop("checked") === true) {
                            DomControl.ctrl_frmDemandUpdateForm.find("table tbody tr[data-read-only='false']").find(".txt-property-tax").val($(this).val());
                        }
                    });

                    scopContainer.find(".demand-update-form").on('keyup', '.txt-surcharge-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllSurcharge.prop("checked") === true) {
                            DomControl.ctrl_frmDemandUpdateForm.find("table tbody tr[data-read-only='false']").find(".txt-surcharge-tax").val($(this).val());
                        }
                    });

                };
                (function () {
                    BindEvents();
                }());
            };
            this.DemanInsertForm = new function () {
                _parentArrearUpdateForm = this;

                var DomControl = {
                    ctrl_txtPropertyTaxAmount: scopContainer.find(".demand-insert-form").find(".txt-property-tax"),
                    ctrl_txtSurchargeAmount: scopContainer.find(".demand-insert-form").find(".txt-surcharge-tax"),
                    ctrl_btnSaveSingle: scopContainer.find(".demand-insert-form").find(".btn-save-single"),
                    ctrl_ChkAllPropertytax: scopContainer.find(".demand-insert-form").find(".check-all-property-tax"),
                    ctrl_ChkAllSurcharge: scopContainer.find(".demand-insert-form").find(".check-all-surcharge"),
                    ctrl_btnSaveAll: scopContainer.find(".demand-insert-form").find(".btn-save-all"),
                    ctrl_frmDemandInsertForm: scopContainer.find(".demand-insert-form")
                };
                
                var GetRowData = function (rowObj) {
                    var finyear = rowObj.attr("data-fin-year");
                    var qtr = rowObj.attr("data-qtr");
                    var propertyTax = rowObj.find(".txt-property-tax").val();
                    var surcharge = rowObj.find(".txt-surcharge-tax").val();

                    var data = {
                        FinYear: finyear,
                        Qtr: qtr,
                        PropertyTaxAmnt: propertyTax,
                        SurchargeAmnt: surcharge
                    };
                    return data;
                };
                this.ResetForm = function () {
                    //DomControl.ctrl_frmDemandInsertForm.find('table tbody').empty();
                    $.each(DomControl.ctrl_frmDemandInsertForm.find('table tbody tr'), function (index, value) {
                        $(value).find(".txt-property-tax").val(0);
                        $(value).find(".txt-surcharge-tax").val(0);
                    });

                };
                var ValidateRow = function (rowDatas) {
                    var flag = true;
                    $.each(rowDatas, function (index, value) {
                        var row = DomControl.ctrl_frmDemandInsertForm.find('table tbody').find("tr[data-fin-year='" + value.FinYear + "'][data-qtr='" + value.Qtr + "']");
                        row.removeClass("error-element");
                        if (isNaN(value.PropertyTaxAmnt) || value.PropertyTaxAmnt < 0) {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                        if (isNaN(value.SurchargeAmnt) || value.SurchargeAmnt < 0) {
                            alertify.error("Property tax should be prositive number");
                            row.addClass("error-element");
                            flag = false;
                            return flag;
                        }
                    });

                    return flag;
                };
                var BindEvents = function () {
                    scopContainer.find(".demand-insert-form").on('click', '.btn-save-single', function (e) {
                        e.preventDefault();
                        var row = $(this).closest('tr');
                        if (row.length == 0) {
                            return;
                        }
                        var rowData = GetRowData(row);
                        var rowDatas = [];
                        rowDatas.push(rowData);
                        if (ValidateRow(rowDatas)) {
                            alertify.confirm("Confirm", "Are you sure to insert Demand data?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.DemandInsert(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }

                    });

                    DomControl.ctrl_frmDemandInsertForm.on('click', '.btn-save-all', function (e) {
                        e.preventDefault();

                        var rows = DomControl.ctrl_frmDemandInsertForm.find("table tbody tr");
                        if (rows.length == 0) {
                            return;
                        }
                        var rowDatas = [];
                        $.each(rows, function (index, value) {
                            var rowData = GetRowData($(value));
                            rowDatas.push(rowData);
                        });
                        if (ValidateRow(rowDatas)) {
                            alertify.confirm("Confirm", "Are you sure to insert Demand datas?",
                                function () {
                                    var data = { AssesseID: _selectedAssesse.AssesseeID, ArrearDemands: rowDatas };
                                    serverObject.DemandInsert(data, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                        } else {
                                            alertify.error("Error: " + response.Message);
                                        }
                                    });
                                }, function () { });
                        } else {
                            alertify.error("Form validation failed");
                        }
                    });
                    scopContainer.find(".demand-insert-form").on('keyup', '.txt-property-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllPropertytax.prop("checked") === true) {
                            DomControl.ctrl_frmDemandInsertForm.find("table tbody tr").find(".txt-property-tax").val($(this).val());
                        }
                    });
                    scopContainer.find(".demand-insert-form").on('keyup', '.txt-surcharge-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllPropertytax.prop("checked") === true) {
                            DomControl.ctrl_frmDemandInsertForm.find("table tbody tr").find(".txt-surcharge-tax").val($(this).val());
                        }
                    });

                    scopContainer.find(".demand-insert-form").on('keyup', '.txt-surcharge-tax', function (e) {
                        if (isNaN($(this).val())) {
                            e.preventDefault();
                            return;
                        }
                        if (DomControl.ctrl_ChkAllSurcharge.prop("checked") === true) {
                            DomControl.ctrl_frmDemandInsertForm.find("table tbody tr[data-read-only='false']").find(".txt-surcharge-tax").val($(this).val());
                        }
                    });

                };
                (function () {
                    BindEvents();
                }());
            };

            this.ReviewInsertForm = new function () {
                var modelData = [];
                var ReviewID = null;
                var AssesseeID = null;
                var _parentReviewInsertForm = this;

                var DomControl = {
                    ctrl_ddlFromFineYear: scopContainer.find(".review-insert-form").find(".ddl-from-fin-year"),
                    ctrl_ddlFromQtr: scopContainer.find(".review-insert-form").find(".ddl-from-qtr"),
                    ctrl_ddlToFineYear: scopContainer.find(".review-insert-form").find(".ddl-to-fin-year"),
                    ctrl_ddlToQtr: scopContainer.find(".review-insert-form").find(".ddl-to-qtr"),
                    ctrl_txtPropertyTaxAmount: scopContainer.find(".review-insert-form").find(".txt-property-tax-amnt-per-qtr"),
                    ctrl_txtSurchargeAmount: scopContainer.find(".review-insert-form").find(".txt-surcharge-amnt-per-qtr"),
                    ctrl_referanceNo: scopContainer.find(".review-insert-form").find(".txt-ref-no"),
                    ctrl_referanceText: scopContainer.find(".review-insert-form").find(".txt-ref-Text"),
                    ctrl_reviewTab: scopContainer.find(".review-insert-form").find("#tabReview"),

                    ctrl_btnCancel: scopContainer.find(".review-insert-form").find(".btn-Cancel"),
                    ctrl_btnSave: scopContainer.find(".review-insert-form").find(".btn-save"),
                    ctrl_frmReviewInsertForm: scopContainer.find(".review-insert-form")
                };

                this.getReviewData = function () {
                    var frmdata = { AssesseeId: _selectedAssesse.AssesseeID };
                    serverObject.ViewReviewAllData(frmdata, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            modelData = [];
                            ReviewID = null;
                            AssesseeID = null;
                            if (response.Data.length > 0) {
                                $.each(response.Data, function (index, value) {
                                    var obj = value;
                                    obj.UnqID = uuidv4();
                                    modelData.push(obj);
                                });
                                // console.log(modelData);
                                DomControl.ctrl_reviewTab.prop('disabled', false);
                                PopulateTabReview(modelData);
                            }
                            else {
                                //alert("No Record found!!!");  
                                DomControl.ctrl_reviewTab.DataTable().clear().draw();
                            }

                        } else {
                            alert(response.Message);
                        }
                    });
                };

                var PopulateTabReview = function (responseData) {
                    var ReviewTab = DomControl.ctrl_reviewTab;
                    ReviewTab.find("tbody").empty();
                    //ReviewTab.DataTable().clear().draw();

                    ReviewTab.DataTable({
                        destroy:true,
                        data: responseData,
                        columns : [
                            { 'data': 'FromFinYear' },
                            { 'data': 'FromQtr' },
                            { 'data': 'ToFinYear' },
                            { 'data': 'ToQtr' },
                            { 'data': 'RPtax' },
                            { 'data': 'RSch' },
                            { 'data': 'Status' },
                            {
                                'render': function (data, type, full, meta) {
                                    //console.log(full);
                                    return "<span data-ID=" + full.UnqID + " id='spanEdit' style='cursor: pointer' ><img src='/Contents/image/edit.png'/></span>";
                                }
                            }
                        ],
                        order: []
                    });

                    RegisterAllTabEvent();

                    //if (responseData.length > 0) {
                    //    $.each(responseData, function (index, value) {

                    //        //var rowstr = "<tr>";
                    //        //rowstr += "<td align='center' class='FromFinYear'>" + value.FromFinYear + "</td>";
                    //        //rowstr += "<td align='center' class='FromQtr'>" + value.FromQtr + "</td>";
                    //        //rowstr += "<td align='center' class='ToFinYear'>" + value.ToFinYear + "</td>";
                    //        //rowstr += "<td align='center' class='ToQtr'>" + value.ToQtr + "</td>";
                    //        //rowstr += "<td align='center' class='RPtax'>" + value.RPtax + "</td>";
                    //        //rowstr += "<td align='center' class='RSch'>" + value.RSch + "</td>";
                    //        //rowstr += "<td align='center' class='Status'>" + value.Status + "</td>";
                    //        //rowstr += "<td align='center'><span data-ID=" + value.UnqID + " id='spanEdit' style='cursor: pointer' data-toggle='modal' data-target='#myModal'><img src='/Contents/image/edit.png'/></span></td>"
                    //        //rowstr += "</tr>";

                    //        //ReviewTab.find("tbody").append(rowstr);

                           
                    //    });
                       // RegistertabAllAdjEvent();
                  //  }
                }

                var RegisterAllTabEvent = function () {
                    DomControl.ctrl_reviewTab.find("tbody").off('click', 'span[id="spanEdit"]', onEditRow);
                    DomControl.ctrl_reviewTab.find("tbody").on('click', 'span[id="spanEdit"]', onEditRow);
                };

                var onEditRow = function () {
                    ReviewID = null;
                    AssesseeID = null;
                    var CurrentTR = $(this).closest('tr');
                    var RowUnqID = $(CurrentTR).find("td:eq(7)").find('span[id$="spanEdit"]').attr('data-ID');
                    var arrRowData = alasql('SELECT * FROM ?  where UnqID="' + RowUnqID + '"', [modelData]);
                    if (arrRowData.length = 1) {
                        var objRowData = arrRowData[0];
                        ReviewID = objRowData.ReviewID;
                        DomControl.ctrl_ddlFromFineYear.val(objRowData.FromFinYear)
                        DomControl.ctrl_ddlFromQtr.val(objRowData.FromQtr)
                        DomControl.ctrl_ddlToFineYear.val(objRowData.ToFinYear)
                        DomControl.ctrl_ddlToQtr.val(objRowData.ToQtr)
                        Number(DomControl.ctrl_txtPropertyTaxAmount.val(objRowData.RPtax))
                        Number(DomControl.ctrl_txtSurchargeAmount.val(objRowData.RSch))
                        DomControl.ctrl_referanceNo.val(objRowData.ReferanceNo)
                        DomControl.ctrl_referanceText.val(objRowData.ReferanceText)

                        if (objRowData.ApprovedFlag == 'Y') {
                            DomControl.ctrl_btnSave.prop("disabled", true);
                        } else {
                            DomControl.ctrl_btnSave.prop("disabled", false);
                        }
                    }                    
                }

                var uuidv4 = function () {
                    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return v.toString(16);
                    });
                }

                var getFormData = function () {
                    return {
                        FromFinYear: DomControl.ctrl_ddlFromFineYear.val(),
                        FromQtr: DomControl.ctrl_ddlFromQtr.val(),
                        ToFinYear: DomControl.ctrl_ddlToFineYear.val(),
                        ToQtr: DomControl.ctrl_ddlToQtr.val(),
                        PTax: Number(DomControl.ctrl_txtPropertyTaxAmount.val()),
                        SCh: Number(DomControl.ctrl_txtSurchargeAmount.val()),
                        ReferanceNo: DomControl.ctrl_referanceNo.val(),
                        ReferanceText: DomControl.ctrl_referanceText.val(),
                        AssesseeId: _selectedAssesse.AssesseeID,
                        ReviewID: ReviewID
                    };
                };

                this.ResetForm = function () {
                    DomControl.ctrl_ddlFromFineYear.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlFromQtr.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlToFineYear.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_ddlToQtr.find("option:eq(0)").prop('selected', true);
                    DomControl.ctrl_txtPropertyTaxAmount.val('');
                    DomControl.ctrl_txtSurchargeAmount.val('');
                    DomControl.ctrl_referanceNo.val('');
                    DomControl.ctrl_referanceText.val('');
                    modelData = [];
                    ReviewID = null;
                    AssesseeID = null;
                    DomControl.ctrl_reviewTab.find("tbody").empty();
                    DomControl.ctrl_btnSave.prop("disabled", false);
                };

                var getResetReviewForm = function () {
                    _parentReviewInsertForm.ResetForm();
                    _parentReviewInsertForm.getReviewData();
                }

                var ValidateForm = function () {
                    var vldObj = {
                        rules: {},
                        messages: {},
                        errorPlacement: {}
                    };
                    //enable hidden field vallidation
                    vldObj.ignore = [];

                    //set rules
                    vldObj.rules[DomControl.ctrl_ddlFromFineYear.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlFromQtr.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlToFineYear.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_ddlToQtr.prop('name')] = { required: true };
                    vldObj.rules[DomControl.ctrl_txtPropertyTaxAmount.prop('name')] = { required: true, number: true };
                    vldObj.rules[DomControl.ctrl_txtSurchargeAmount.prop('name')] = { required: true, number: true };
                    vldObj.rules[DomControl.ctrl_referanceNo.prop('name')] = { required: true};
                    vldObj.rules[DomControl.ctrl_referanceText.prop('name')] = { required: true};

                    //set error messages
                    vldObj.messages[DomControl.ctrl_ddlFromFineYear.prop('name')] = { required: "Please select From Fin-Year." };
                    vldObj.messages[DomControl.ctrl_ddlFromQtr.prop('name')] = { required: "Please select From Qtr." };
                    vldObj.messages[DomControl.ctrl_ddlToFineYear.prop('name')] = { required: "Please select To Fin-Year" };
                    vldObj.messages[DomControl.ctrl_ddlToQtr.prop('name')] = { required: "Please select To Qtr" };
                    vldObj.messages[DomControl.ctrl_txtPropertyTaxAmount.prop('name')] = { required: "Please enter Property Tax Amount", number: "Please enter numeric value" };
                    vldObj.messages[DomControl.ctrl_txtSurchargeAmount.prop('name')] = { required: "Please enter Surcharge Amount", number: "Please enter numeric value" };
                    vldObj.messages[DomControl.ctrl_referanceNo.prop('name')] = { required: "Please enter Referance No." };
                    vldObj.messages[DomControl.ctrl_referanceText.prop('name')] = { required: "Enter Referance Text." };

                    //place errors in position
                    vldObj.errorPlacement = function (error, element) {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                    //push validator object to tergate form
                    DomControl.ctrl_frmReviewInsertForm.validate(vldObj);
                    //validate form and return true and false
                    return DomControl.ctrl_frmReviewInsertForm.valid();
                };
                var BindEvents = function () {
                    
                    DomControl.ctrl_btnCancel.on('click', getResetReviewForm);
                    DomControl.ctrl_frmReviewInsertForm.on('click', ".btn-save", function (e) {
                        DomControl.ctrl_frmReviewInsertForm.find(".error-list").empty();
                        e.preventDefault();
                        if (ValidateForm()) {                            
                            alertify.confirm("Confirm", "Are you sure to submit review data?",
                                function () {
                                    var formData = getFormData();
                                    console.log(formData);
                                    serverObject.InsertReview(formData, function (response) {
                                        if (response.Status === 200) {
                                            alertify.success(response.Message);
                                            _parentReviewInsertForm.ResetForm();
                                            _parentReviewInsertForm.getReviewData();
                                        } else {
                                            alertify.error(response.Message);
                                            //var respData = response.Data;
                                            //if (response.Data != null && respData.hasOwnProperty("ErrorCode") && respData.hasOwnProperty("ErrorData") && respData.ErrorCode == "InsertArrear.InvalidFinyearsQtr") {
                                            //    $.each(respData.ErrorData, function (index, value) {
                                            //        DomControl.ctrl_frmReviewInsertForm.find(".error-list").append("<li>" + "Arrear Demand for " + value.StartYear + "-" + value.EndYear + "," + value.Qtr + " is already inserted" + "</li>")
                                            //    })
                                            //}
                                        }
                                    });
                                }
                                ,
                                function () {
                                    //calcel button clicked
                                });
                        }
                    });
                };

                (function () {  
                    //DomControl.ctrl_reviewTab.prop('disabled', true);
                    BindEvents();                    
                }());
            };


            this.Reset = function () {
                scopContainer.hide();
                $("a.btn-assesse-update").trigger('click');
                _parentUpdateComponents.AssesseUpdateForm.ResetForm();
                _parentUpdateComponents.ArrearUpdateForm.ResetForm();
                _parentUpdateComponents.ArrearInsertForm.ResetForm();
                _parentUpdateComponents.DemanUpdateForm.ResetForm();
                _parentUpdateComponents.DemanInsertForm.ResetForm();
               _parentUpdateComponents.ReviewInsertForm.ResetForm();
            };
            this.ShowComponent = function () {
                scopContainer.show();
            };
            this.SetAssesse = function (assessee) {
                _selectedAssesse = assessee;
            };
            this.PopulateDefaulComponent = function () {
                _parentUpdateComponents.AssesseUpdateForm.SetAssesseOnForm();
                SetTitle("Update Assessee")
            };
            var BindEvents = function () {
                scopContainer.find("a.btn-arrear-update").off('click');
                scopContainer.find("a.btn-arrear-update").on('click', function () {
                    _parentUpdateComponents.ArrearUpdateForm.PopulateUpdateArrearData();
                    SetTitle("Arrear Update");
                });

                scopContainer.find("a.btn-arrear-insert").off('click');
                scopContainer.find("a.btn-arrear-insert").on('click', function () {
                    SetTitle("Arrear Insert");
                });

                scopContainer.find("a.btn-demand-update").off('click');
                scopContainer.find("a.btn-demand-update").on('click', function () {
                    SetTitle("Demand Update");
                    _parentUpdateComponents.DemanUpdateForm.PopulateUpdateDemandData();
                });

                scopContainer.find("a.btn-demand-insert").off('click');
                scopContainer.find("a.btn-demand-insert").on('click', function () {
                    SetTitle("Demand Insert");
                });
                scopContainer.find("a.btn-assesse-update").off('click');
                scopContainer.find("a.btn-assesse-update").on('click', function () {
                    SetTitle("Update Assessee");
                });
                scopContainer.find("a.btn-review-insert").off('click');
                scopContainer.find("a.btn-review-insert").on('click', function () {
                    SetTitle("Review Insert");
                    _parentUpdateComponents.ReviewInsertForm.getReviewData();
                });
            };
            (function () {
                _parentUpdateComponents.Reset();
                _parentUpdateComponents.AssesseUpdateForm.bindEvents();
                BindEvents();
            }());
        };
            
        (function () {
            // BindEvents();
            var insertAssesseComponent = new InsertComponent();
            var _updateComponents = new updateComponents();
            var _searchComponent = new SearchComponent(_updateComponents);
        }());
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
    };

    new INIT();
  
});