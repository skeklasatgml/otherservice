﻿$(document).ready(function () {
    app.ddlMunicipality();
    $(document).on("click", ".btn-sbmt, .btn-edit", function () {
        app.ConfigureForm();
        var configureDetails = app.ConfigureForm().getFormData();
        console.log(configureDetails);
        if (app.ConfigureForm().validate()) {
            alertify.confirm('Please Confirm Us!', 'Are your sure to save?', function () {
                var payload = {
                    configureDetails: app.ConfigureForm().getFormData()
                };
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/SaveUlbContact",
                    data: JSON.stringify(payload.configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                                $('.ddlMunicipality1').val(Response.Data);
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    $(document).on("click", ".btn-search", function () {
        app.getAllUlbContacts();
    });
    $(document).on("click", ".updateRow", function () {
        var jsnData = JSON.parse($(this).closest('tr').find('.hdnJSN').val()); 
        $(".tabId").val(jsnData.TabId);
        $(".ddl-desigId").val(jsnData.DesignationId);
        $(".name").val(jsnData.ContactPersonName);
        $(".email").val(jsnData.EmailId);
        $(".phone").val(jsnData.MobNo);
    });
});
var app = {
    ddlMunicipality: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetAllMunicipalites',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateMunicipality(response.Data);
                $('#ddlMunicipality').val("")
            },
            error: function () { }
        });
    },
    ConfigureForm: function () {
        var _doms = function () {
            return {
                frm: $("#configureulbcontact"),
                ddlMuni: $(".ddlMunicipality1"),
                ddlDesig: $(".ddl-desigId"),
                txtName: $(".name"),
                txtEmail: $(".email"),
                txtPhone: $(".phone"),
               
            }
        };
        return {
            validate: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[_doms().ddlMuni.prop('name')] = { required: true };
                vldObj.rules[_doms().ddlDesig.prop('name')] = { required: true };
                //vldObj.rules[_doms().txtName.prop('name')] = { required: true };
                //vldObj.rules[_doms().txtEmail.prop('name')] = { required: true };
                //vldObj.rules[_doms().txtPhone.prop('name')] = { required: true, number: true };

                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    if ($(element).parent(".input-group").length > 0) {
                        error.insertAfter($(element).parent(".input-group"));
                        error.css('color', 'red');
                    } else {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }

                }

                //push validator object to tergate form
                _doms().frm.validate(vldObj);
                //validate form and return true and false
                return _doms().frm.valid();
            },
            getFormData: function () {
                function toDate(dateStr) {
                    var fromDate = dateStr.split("/");
                    var strJobDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
                    return strJobDate;
                }
                return {
                    MunicipalityID: _doms().ddlMuni.val(),
                    DesignationId: _doms().ddlDesig.val(),
                    ContactPersonName: _doms().txtName.val(),
                    EmailId: _doms().txtEmail.val(),
                    MobNo: _doms().txtPhone.val(),
                    TabId: $(".tabId").val()
                };
            }
        }
    },
    PopulateMunicipality: function (data) {
        var ctlr_ddlmunicipality = $('.ddlMunicipalityCmmn');
        ctlr_ddlmunicipality.empty();
        ctlr_ddlmunicipality.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            //$.each(data, function (index, value) {

            ctlr_ddlmunicipality.append("<option value='" + value.MunicipalityID + "' >" + value.MunicipalityName + "</option>")
        });
        var option = ctlr_ddlmunicipality.find("option[value='" + ctlr_ddlmunicipality.attr("data-selected-municipality") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlmunicipality.find('option:eq(0)').prop('selected', true);
        }
    },
    getAllUlbContacts: function () {
        var mnId = $('.ddlMunicipality1').val();
        var payload = { "mnId": mnId };
        $.ajax({
            url: '/Modules/DprExtension/GetDesigList',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data !== null) {

                    $('#myTable').find('>tbody').empty();
                    var i;
                    for (i = 0; i < result.Data.length; ++i) {
                        //floor control  
                        var tempJson = result.Data[i];

                        var tr = $('<tr />');

                        var td = $('<td />');
                        tr.append(td.append(tempJson.ContactPersonName));

                        td = $('<td />');
                        tr.append(td.append(tempJson.EmailId));

                        td = $('<td />');
                        tr.append(td.append(tempJson.MobNo));

                       
                        td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                        tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson + '\'/>Update</a>'));

                        $('.myTable').find('>tbody').append(tr);

                    }
                }
            },
            error: function () { }
        });
    },
}