﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.SearchConfigurePayHead = function (
          fin,
          applicableType,
          payHeadBehave,
          groupId,
          callback
        ) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPayHead/SearchOnConfigurePayHead",
                data: JSON.stringify({
                    FinYear: fin,
                    ApplicableType: applicableType,
                    PayHeadBehave: payHeadBehave,
                    GroupId: groupId
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.SaveRebatePayHead = function (
          fin,
          applicableType,
          payHeadBehave,
          groupId,
          tableDataRebate,
          callback
        ) {
            $.ajax({
                type: "POST",
                url: "/Municipality/MnPayHead/SaveRebatePayHead",
                data: JSON.stringify({
                    FinYear: fin,
                    ApplicableType: applicableType,
                    PayHeadBehave: payHeadBehave,
                    GroupId: groupId,
                    tableDataRebate: tableDataRebate
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };
    };

    var APP_OBJECT = function (serverObject) {
        var BindEvent = function () {
            $("#btnSubmitRebate").on("click", OnSubmitRebate);
            $("#btnSubmitInterest").on("click", OnSubmitInterest);
            $("#btnSaveRebate").on("click", OnSaveRebate);
            $("#btnSaveInterest").on("click", OnSaveInterest);
            $("#ddlFinyearRebate").on("change", OnChangeRebateFinyear);
            $("#ddlFinyearInterest").on("change", OnChangeInterestFinyear);
            $(".select-payhead-group").on("change", function () {
                $("#ddlFinyearInterest").trigger("change");
                $("#ddlFinyearRebate").trigger("change");
            });
        };
        var OnChangeRebateFinyear = function () {
            var tabRebate = $("#tabRebate");
            tabRebate.find("tbody").empty();
            $("#divRebate").prop("hidden", true);
        };
        var OnChangeInterestFinyear = function () {
            var tabInterest = $("#tabInterest");
            tabInterest.find("tbody").empty();
            $("#divInterest").prop("hidden", true);
        };
        //[obsolete]
        var OnSubmitRebate = function () {
            var fin = $("#ddlFinyearRebate").val();
            var applicableType = "D";
            var payHeadBehave = "D";

            if (From_Validate()) {
                $("#divRebate").prop("hidden", false);
                var tabRebate = $("#tabRebate");
                tabRebate.find("tbody").empty();
                //var searchData = { FinYear: fin, ApplicableType: applicableType, PayHeadBehave: payHeadBehave };

                serverObject.SearchConfigurePayHead(
                  fin,
                  applicableType,
                  payHeadBehave,
                  $(".select-payhead-group").val(),
                  function (response) {
                      var status = response.Status;
                      if (status == 200) {
                          PopulateRateTable(response.Data);
                          //alert(response.Message);
                      } else {
                          //alert(response.Message);
                      }
                  }
                );
            }
        };
        //[obsolete]
        var OnSubmitInterest = function () {
            var fin = $("#ddlFinyearInterest").val();
            var applicableType = "D";
            var payHeadBehave = "A";

            //if (From_Validate()) {

            $("#divInterest").prop("hidden", false);
            var tabInterest = $("#tabInterest");
            tabInterest.find("tbody").empty();
            //var searchData = { FinYear: fin, ApplicableType: applicableType, PayHeadBehave: payHeadBehave };

            serverObject.SearchConfigurePayHead(
              fin,
              applicableType,
              payHeadBehave,
              $(".select-payhead-group").val(),
              function (response) {
                  var status = response.Status;
                  if (status == 200) {
                      PopulateRateTableInterest(response.Data);
                      //alert(response.Message);
                  } else {
                      //alert(response.Message);
                  }
              }
            );
            //}
        };
        var OnSaveRebate = function () {
            var fin = $("#ddlFinyearRebate").val();
            var applicableType = "D";
            var payHeadBehave = "D";
            var tabRebate = $("#tabRebate");
            var tableDataRebate = [];
            tabRebate.find("tbody tr").each(function (i, el) {
                var $tds = $(this).find("td");
                var QtrNo = $tds.eq(0).text();
                var Rate = $tds
                  .eq(3)
                  .find("input[type=text]")
                  .val();
                var GracePeriodInMonth = $tds
                  .eq(4)
                  .find("input[type=text]")
                  .val();
                tableDataRebate.push({
                    QtrNo: QtrNo,
                    PayRate: Rate,
                    GracePeriodInMonth: GracePeriodInMonth
                });
            });
            var r = confirm("Do you want to submit Rebate Rate.");
            if (r == true) {
                serverObject.SaveRebatePayHead(
                  fin,
                  applicableType,
                  payHeadBehave,
                  $(".select-payhead-group").val(),
                  tableDataRebate,
                  function (response) {
                      var status = response.Status;
                      if (status == 200) {
                          alert(response.Message);
                          PopulateRateTable(response.Data);
                      } else {
                          alert(response.Message);
                      }
                  }
                );
            } else { }
        };
        var OnSaveInterest = function () {
            var fin = $("#ddlFinyearInterest").val();
            var applicableType = "D";
            var payHeadBehave = "A";
            var tabInterest = $("#tabInterest");
            var tableDataInterest = [];
            tabInterest.find("tbody tr").each(function (i, el) {
                var $tds = $(this).find("td");
                var QtrNo = $tds.eq(0).text();
                var Rate = $tds
                  .eq(3)
                  .find("input[type=text]")
                  .val();
                var GracePeriodInMonth = $tds
                  .eq(4)
                  .find("input[type=text]")
                  .val();
                var period = $tds.find(".txtPeriodOffset").val();
                tableDataInterest.push({
                    QtrNo: QtrNo,
                    PayRate: Rate,
                    GracePeriodInMonth: GracePeriodInMonth,
                    PeriodOffset: period
                });
            });

            var r = confirm("Do you want to submit Interest Rate.");
            if (r == true) {
                serverObject.SaveRebatePayHead(
                  fin,
                  applicableType,
                  payHeadBehave,
                  $(".select-payhead-group").val(),
                  tableDataInterest,
                  function (response) {
                      var status = response.Status;
                      if (status == 200) {
                          alert(response.Message);
                          PopulateRateTable(response.Data);
                      } else {
                          alert(response.Message);
                      }
                  }
                );
            } else { }
        };
        var PopulateRateTable = function (responseData) {
            var tabRebate = $("#tabRebate");
            tabRebate.find("tbody").empty();

            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {
                    var rowstr = "<tr>";
                    rowstr += "<td align='center' class='QtrNo'>" + value.QtrNo + "</td>";
                    rowstr +=
                      "<td align='center' class='EffectiveDateForm'>" +
                      value.EffectiveDateForm +
                      "</td>";
                    rowstr +=
                      "<td align='center' class='EffectiveDateTo'>" +
                      value.EffectiveDateTo +
                      "</td>";
                    //rowstr += "<td align='center' class='PayRate'>" + value.PayRate + "</td>";
                    rowstr +=
                      "<td align='center' class='PayRate'><input type='text' class='txtPayRate' name='txtPayRate' value=" +
                      value.PayRate +
                      "></td>";
                    rowstr +=
                      "<td align='center' class='GracePeriodInMonth'><input type='text' class='txtGracePeriodInMonth' name='txtGracePeriodInMonth' value=" +
                      value.GracePeriodInMonth +
                      "></td>";

                    //rowstr += "<td align='center' class='GracePeriodInMonth'>" + value.GracePeriodInMonth + "</td>";
                    rowstr +=
                      "<td align='center' class='FinYear'>" + value.FinYear + "</td>";
                    rowstr += "</tr>";

                    tabRebate.find("tbody").append(rowstr);
                });

                $(".txtPayRate").keypress(function (event) {
                    if (
                      (event.which != 46 ||
                        $(this)
                        .val()
                        .indexOf(".") != -1) &&
                      (event.which < 48 || event.which > 57)
                    ) {
                        event.preventDefault();
                    }
                });

                $(".txtGracePeriodInMonth").keypress(function (event) {
                    if (
                      event.charCode > 31 &&
                      (event.charCode < 48 || event.charCode > 57)
                    ) {
                        event.preventDefault();
                    }
                });
            }
        };
        var PopulateRateTableInterest = function (responseData) {
            var tabInterest = $("#tabInterest");
            tabInterest.find("tbody").empty();

            if (responseData.length > 0) {
                $.each(responseData, function (index, value) {
                    var rowstr = "<tr>";
                    rowstr += "<td align='center' class='QtrNo'>" + value.QtrNo + "</td>";
                    rowstr +=
                      "<td align='center' class='EffectiveDateForm'>" +
                      value.EffectiveDateForm +
                      "</td>";
                    rowstr +=
                      "<td align='center' class='EffectiveDateTo'>" +
                      value.EffectiveDateTo +
                      "</td>";
                    //rowstr += "<td align='center' class='PayRate'>" + value.PayRate + "</td>";
                    rowstr +=
                      "<td align='center' class='PayRate'><input type='text' class='txtPayRate' name='txtPayRate' value=" +
                      value.PayRate +
                      "></td>";
                    rowstr +=
                      "<td align='center' class='GracePeriodInMonth'><input type='text' class='txtGracePeriodInMonth' name='txtGracePeriodInMonth' value=" +
                      value.GracePeriodInMonth +
                      "></td>";
                    rowstr +=
                    "<td align='center' class='PeriodOffset'><input type='text' class='txtPeriodOffset' name='txtPeriodOffset' value=" +
                    value.PeriodOffset +
                    "></td>";
                    rowstr +=
                      "<td align='center' class='FinYear'>" + value.FinYear + "</td>";
                    rowstr += "</tr>";

                    tabInterest.find("tbody").append(rowstr);
                });

                $(".txtPayRate").keypress(function (event) {
                    if (
                      (event.which != 46 ||
                        $(this)
                        .val()
                        .indexOf(".") != -1) &&
                      (event.which < 48 || event.which > 57)
                    ) {
                        event.preventDefault();
                    }
                });
                $(".txtPeriodOffset").keypress(function (event) {
                    console.log(event.charCode);
                    if ((event.charCode > 31 && (event.charCode < 48 || event.charCode > 57))) {
                        if (event.charCode !== 45 || $(this).val().indexOf("-") != -1 || $(this).val().trim().length > 0)
                            event.preventDefault();
                    }
                });
                $(".txtGracePeriodInMonth").keypress(function (event) {
                    if (
                      event.charCode > 31 &&
                      (event.charCode < 48 || event.charCode > 57)
                    ) {
                        event.preventDefault();
                    }
                });
            }
        };
        var From_Validate = function () {
            $("#frmRebate").validate({
                rules: {
                    ddlFinyearRebate: {
                        required: true
                    }
                },
                messages: {
                    ddlFinyearRebate: {
                        required: "Please select FinYear."
                    }
                }
            });
            return $("#frmRebate").valid();
        };
        var PopulateFinYear = function () {
            var currDT = new Date();
            var currYear = "";
            var currMonth = currDT.getMonth();
            if (currMonth >= 0 && currMonth < 3) {
                currYear = currDT.getFullYear() - 1;
            } else {
                currYear = currDT.getFullYear();
            }
            currYear += 1;
            var firstYear = 1950;
            var finyear = "";
            for (var i = currYear; i >= firstYear; i--) {
                j = i + 1;
                finyear = i + "-" + j;
                $('<option value="' + finyear + '">' + finyear + "</option>").appendTo(
                  "#ddlFinyearRebate"
                );
                $('<option value="' + finyear + '">' + finyear + "</option>").appendTo(
                  "#ddlFinyearInterest"
                );
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            PopulateFinYear();
        };

        var PayheadExtension = function () {
            var parentContainer = {
                _selector: "#modal-payhead-config",
                _control: $("#modal-payhead-config")
            };
            var domControls = {
                btnAddGroup: {
                    _selector: ".btn-add-payhead-config-group",
                    _control: $(".btn-add-payhead-config-group")
                },
                selectPayheadGroup: {
                    _selector: ".select-payhead-group",
                    _control: $(".select-payhead-group")
                },
                btnEditGroup: {
                    _selector: ".btn-edit-payhead-config-group",
                    _control: $(".btn-edit-payhead-config-group")
                },
                selectPayheadGroup: {
                    _selector: ".select-payhead-group",
                    _control: $(".select-payhead-group")
                },
                cntrChkAssesseeList: {
                    _selector: ".chk-assessee-list",
                    _control: parentContainer._control.find(".chk-assessee-list"),
                    _childrens: {
                        cntrChkassesseeListUnmapped: {
                            _selector: ".chk-assessee-list-unmapped",
                            _control: parentContainer._control.find(
                              ".chk-assessee-list-unmapped"
                            )
                        },
                        cntrChkassesseeListMapped: {
                            _selector: ".chk-assessee-list-mapped",
                            _control: parentContainer._control.find(
                              ".chk-assessee-list-mapped"
                            )
                        }
                    }
                },
                chkFilterAssessee: {
                    _selector: ".chk-filter-assessee",
                    _control: parentContainer._control.find(".chk-filter-assessee")
                },
                selectLocation: {
                    _selector: ".select-location",
                    _control: parentContainer._control.find(".select-location")
                },
                selectWard: {
                    _selector: ".select-ward",
                    _control: parentContainer._control.find(".select-ward")
                },
                btnUnmappedAssesse: {
                    _selector: ".unmapped-assessee",
                    _control: parentContainer._control.find(
                      ".chk-assessee-list-unmapped .unmapped-assessee"
                    )
                },
                btnMappedAssessee: {
                    _selector: ".mapped-assessee",
                    _control: parentContainer._control.find(
                      ".chk-assessee-list-mapped .mapped-assessee"
                    )
                },

                btnMapAll: {
                    _selector: ".btn-mapp-all-assessee",
                    _control: parentContainer._control.find(".btn-mapp-all-assessee")
                },
                btnUnMapAll: {
                    _selector: ". btn-unmapp-all-assessee",
                    _control: parentContainer._control.find(".btn-unmapp-all-assessee")
                }
            };

            var pollyFill = function () {
                parentContainer.addNewGroup = function (groupName, callback) {
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/AddNewPayHeadGroup",
                        data: JSON.stringify({
                            GroupName: groupName
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200 && r.Data !== null) {
                                var r = r.Data;

                                if (callback) {
                                    callback(r);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                parentContainer.setTitle = function (title) {
                    parentContainer._control.find(".modal-header h4").html(title);
                };
                domControls.cntrChkAssesseeList.unload = function () {
                    domControls.cntrChkAssesseeList._control.hide("fast");
                    domControls.cntrChkAssesseeList._childrens.cntrChkassesseeListMapped._control.empty();
                    domControls.cntrChkAssesseeList._childrens.cntrChkassesseeListUnmapped._control.empty();
                };
                domControls.cntrChkAssesseeList.appendAssessees = function (assessees) {

                    var getDisplayName = function (obj) {
                        var str = "[<b>" + obj.HoldingNo.trim() + "</b>] " + obj.AssesseeName.trim();
                        if (str.length <= 43) {
                            return str;
                        } else {
                            return str.substring(0, 41) + "..";
                        }
                    };
                    var appendUngroupedAssessees = function (_assessees) {
                        var _container =
                          domControls.cntrChkAssesseeList._childrens
                          .cntrChkassesseeListUnmapped._control;
                        _assessees.chunkArray(2).map(function (values, index) {
                            var newRow = $("<div class='row'/>");
                            values.map(function (value, ind) {
                                var ctrl = "";
                                var toolTip =
                                  "Holding: " +
                                  value.HoldingNo.trim() +
                                  ",  Name: " +
                                  value.AssesseeName.trim();
                                ctrl =
                                  "<a data-assesseeId='" +
                                  value.AssesseeID +
                                  "' title='" +
                                  toolTip +
                                  "'  class='btn btn-default btn-xs btn-block unmapped-assessee' style='text-align:left;'><span class='glyphicon glyphicon-plus pull-left'></span>" +
                                  getDisplayName(value) +
                                  "</a>";
                                newRow.append("<div class='col-md-6'>" + ctrl + "</div>");
                            });
                            _container.append(newRow);
                        });
                    };
                    var appendGroupedAssessees = function (_assessees) {
                        var getSeparator = function (title, _cssObjs) {
                            var row = $("<div class='row'>");
                            if (_cssObjs && $.isArray(_cssObjs)) {
                                _cssObjs.map(function (value, index) {
                                    row.css(value.key, value.value);
                                });
                            }
                            return row.append("<div class='col-md-5'><hr style='border-color: #ccc9c9;'/></div><div class='col-md-2 text-center' style='margin-top: 10px'><span>" + title + "</span></div><div class='col-md-5'><hr style='border-color: #ccc9c9;'/></div>");
                        };
                        var appendAssessees = function (__assessees) {
                            var _container =
                              domControls.cntrChkAssesseeList._childrens
                              .cntrChkassesseeListMapped._control;
                            __assessees.chunkArray(2).map(function (values, index) {
                                var newRow = $("<div class='row'/>").css("background-color", "rgb(201, 249, 180)");;
                                values.map(function (value, ind) {
                                    var ctrl = "";
                                    var toolTip = "Holding: " + value.HoldingNo.trim() + ",  Name: " + value.AssesseeName.trim();
                                    ctrl =
                                      "<a data-assesseeId='" +
                                      value.AssesseeID +
                                      "' title='" +
                                      toolTip +
                                      "' class='btn btn-default btn-xs btn-block  mapped-assessee' style='text-align:left;'><span class='glyphicon glyphicon-minus pull-left'></span>" +
                                      getDisplayName(value) +
                                      "</a>";

                                    newRow.append("<div class='col-md-6'>" + ctrl + "</div>");
                                });
                                _container.append(newRow);
                            });
                            if (__assessees.chunkArray(2).length > 0) {
                                _container.append("<br/>");
                            }
                        };
                        var appendOtherGroupAssessees = function (__assessees) {
                            var _container =
                              domControls.cntrChkAssesseeList._childrens
                              .cntrChkassesseeListMapped._control;
                            __assessees.chunkArray(2).map(function (values, index) {
                                var newRow = $("<div class='row'/>").css("background-color", "#f9e3e7");
                                values.map(function (value, ind) {
                                    var ctrl = "";
                                    var toolTip = "Holding: " + value.HoldingNo.trim() + ",  Name: " + value.AssesseeName.trim();
                                    ctrl = "<a data-assesseeId='" + value.AssesseeID + "' title='" + toolTip + "' class='btn btn-default btn-xs btn-block disabled' style='text-align:left;'><span class='glyphicon glyphicon-minus pull-left'></span>" + getDisplayName(value) + "</a>";

                                    newRow.append("<div class='col-md-6'>" + ctrl + "</div>");
                                });
                                _container.append(newRow);
                            });
                        };
                        var appendSeparator = function (_separator) {
                            var _container =
                              domControls.cntrChkAssesseeList._childrens
                              .cntrChkassesseeListMapped._control;
                            _container.append(_separator);
                        };


                        //start local execution
                        var selfGroupedAssesees = alasql(
                          "select * from ? where GroupID=?",
                          [
                            _assessees,
                            Number(
                              domControls.selectPayheadGroup._control.val()
                            )
                          ]
                        );
                        if (selfGroupedAssesees.length > 0) {
                            appendSeparator(getSeparator(
                                selfGroupedAssesees[0].GroupName +
                                  ":" +
                                  selfGroupedAssesees.length,
                                [
                                  {
                                      key: "background-color",
                                      value: "rgb(201, 249, 180)"
                                  }
                                ]
                              ));
                            appendAssessees(selfGroupedAssesees);
                            _assessees = alasql("select * from ? where GroupID!=?", [
                                _assessees,
                                Number(
                                  domControls.selectPayheadGroup._control.val()
                                )
                            ]);
                        }
                        alasql(
                          "select GroupID,GroupName from ? group by GroupID,GroupName",
                          [_assessees]
                        ).map(function (value, index) {
                            var OthGgroupedAssesees = alasql(
                              "select * from ? where GroupID=?",
                              [_assessees, value.GroupID]
                            );
                            //other group
                            if (OthGgroupedAssesees.length > 0) {
                                appendSeparator(getSeparator(
                                    value.GroupName +
                                      ":" +
                                      OthGgroupedAssesees.length,
                                    [
                                      {
                                          key: "background-color",
                                          value: "rgb(251, 229, 233)"
                                      }
                                    ]
                                  ));
                                appendOtherGroupAssessees(OthGgroupedAssesees);
                            }
                        });
                    };


                    //execute local execution
                    var unGroupedAssessees = alasql("select * from ? where GroupID is null", [assessees]);
                    var groupedAssessees = alasql("select * from ? where GroupID is not null", [assessees]);
                    appendUngroupedAssessees(unGroupedAssessees);
                    appendGroupedAssessees(groupedAssessees);
                };
                // domControls.cntrChkAssesseeList.appendAssessees = function (assessees) {
                //   window.assessees = assessees;
                //   var mappedAssessees = alasql(
                //     "select * from ? where IsMapped=true order by HoldingNo", [assessees]
                //   );
                //   var unMappedAssessees = alasql(
                //     "select * from ? where IsMapped=false order by HoldingNo", [assessees]
                //   );

                //   var getDisplayName = function (obj) {
                //     var str =
                //       "[<b>" +
                //       obj.HoldingNo.trim() +
                //       "</b>] " +
                //       obj.AssesseeName.trim();
                //     if (str.length <= 43) {
                //       return str;
                //     } else {
                //       return str.substring(0, 41) + "..";
                //     }
                //   };
                //   var printAssesseeUI = function (container, datasource) {
                //     datasource.chunkArray(2).map(function (values, index) {
                //       var newRow = $("<div class='row'/>");
                //       values.map(function (value, ind) {
                //         var ctrl = "";
                //         var toolTip =
                //           "Holding: " +
                //           value.HoldingNo.trim() +
                //           ",  Name: " +
                //           value.AssesseeName.trim();
                //         if (value.IsMapped == true) {
                //           ctrl =
                //             "<a data-assesseeId='" +
                //             value.AssesseeID +
                //             "' title='" +
                //             toolTip +
                //             "' class='btn btn-default btn-xs btn-block  mapped-assessee' style='text-align:left;'><span class='glyphicon glyphicon-minus pull-left'></span>" +
                //             getDisplayName(value) +
                //             "</a>";
                //         } else {
                //           ctrl =
                //             "<a data-assesseeId='" +
                //             value.AssesseeID +
                //             "' title='" +
                //             toolTip +
                //             "'  class='btn btn-default btn-xs btn-block unmapped-assessee' style='text-align:left;'><span class='glyphicon glyphicon-plus pull-left'></span>" +
                //             getDisplayName(value) +
                //             "</a>";
                //         }
                //         newRow.append("<div class='col-md-6'>" + ctrl + "</div>");
                //       });
                //       container.append(newRow);
                //     });
                //   };
                //   printAssesseeUI(
                //     domControls.cntrChkAssesseeList._childrens
                //     .cntrChkassesseeListUnmapped._control,
                //     unMappedAssessees
                //   );
                //   printAssesseeUI(
                //     domControls.cntrChkAssesseeList._childrens.cntrChkassesseeListMapped
                //     ._control,
                //     mappedAssessees
                //   );
                // };
                domControls.cntrChkAssesseeList.loadAssessees = function (
                  groupId,
                  wardId,
                  locationId,
                  nameHolding,
                  callback
                ) {
                    domControls.cntrChkAssesseeList.unload();
                    var payLoad = {
                        PayheadGroupId: groupId,
                        WardId: wardId,
                        LocationId: locationId,
                        IsMapped: null,
                        NameOrHolding: nameHolding
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/GetAssesseesPayheadGroup",
                        data: JSON.stringify(payLoad),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                domControls.cntrChkAssesseeList.appendAssessees(r.Data);
                                if (callback) {
                                    callback(r.Data);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.chkFilterAssessee.changeVisibilty = function () {
                    var locationId = Number(domControls.selectLocation._control.val());
                    var wardId = Number(domControls.selectWard._control.val());

                    if (locationId > 0 && wardId > 0) {
                        domControls.chkFilterAssessee._control.prop("disabled", false);
                    } else {
                        domControls.chkFilterAssessee._control.prop("disabled", true);
                    }
                };
                domControls.selectLocation.changeVisibilty = function () {
                    var wardId = Number(domControls.selectWard._control.val());
                    if (wardId > 0) {
                        domControls.selectLocation._control.prop("disabled", false);
                    } else {
                        domControls.selectLocation._control.prop("disabled", true);
                    }
                };
                domControls.selectLocation.Loaddata = function (wardId, callback) {
                    domControls.selectLocation._control
                      .find("option:not(:eq(0))")
                      .remove();
                    if (wardId == 0 || wardId == null) return;
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnLocations/GetLocation",
                        data: JSON.stringify({
                            wardID: wardId,
                            LocationName: ""
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                r.Data.map(function (value, index) {
                                    domControls.selectLocation._control.append(
                                      "<option value='" +
                                      value.LocationID +
                                      "'>" +
                                      value.LocationName +
                                      "</option>"
                                    );
                                });
                                if (callback) {
                                    callback(r.Data);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.selectPayheadGroup.Loaddata = function (callback) {
                    domControls.selectPayheadGroup._control
                      .find("option:not(:eq(0))")
                      .remove();
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/GetPayheadGroups",
                        data: null,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                r.Data.map(function (value, index) {
                                    domControls.selectPayheadGroup._control.append(
                                      "<option value='" +
                                      value.GroupID +
                                      "'>" +
                                      value.GroupName +
                                      "</option>"
                                    );
                                });
                                if (callback) {
                                    callback(r.Data);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.btnUnmappedAssesse.mapGroup = function (
                  groupId,
                  wardId,
                  locationId,
                  AssesseeId,
                  callback
                ) {
                    var payLoad = {
                        PayheadGroupId: groupId,
                        WardId: wardId,
                        LocationId: locationId,
                        AssesseeId: AssesseeId
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/MapGroup",
                        data: JSON.stringify(payLoad),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                if (callback) {
                                    callback(r.Data);
                                    alertify.success(r.Message);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.btnMappedAssessee.unMapGroup = function (
                  AssesseeId,
                  callback
                ) {
                    var payLoad = {
                        AssesseeId: AssesseeId
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/UnMapGroup",
                        data: JSON.stringify(payLoad),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                if (callback) {
                                    callback(r.Data);
                                    alertify.success(r.Message);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.btnMapAll.mappAll = function (
                  groupId,
                  wardId,
                  locationId,
                  callback
                ) {
                    var payLoad = {
                        PayheadGroupId: groupId,
                        WardId: wardId,
                        LocationId: locationId
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/MapGroupAll",
                        data: JSON.stringify(payLoad),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                if (callback) {
                                    callback(r.Data);
                                    alertify.success(r.Message);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
                domControls.btnUnMapAll.unMapAll = function (
                  groupId,
                  wardId,
                  locationId,
                  callback
                ) {
                    var payLoad = {
                        PayheadGroupId: groupId,
                        WardId: wardId,
                        LocationId: locationId
                    };
                    $.ajax({
                        type: "POST",
                        url: "/Municipality/MnPayHead/UnMapGroupAll",
                        data: JSON.stringify(payLoad),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.Status === 200) {
                                if (callback) {
                                    callback(r.Data);
                                    alertify.success(r.Message);
                                }
                            } else {
                                alertify.error("Error: " + r.Message);
                            }
                        },
                        failure: function (response) {
                            alertify.error("Error");
                        }
                    });
                };
            };

            var bindEvent = function () {
                domControls.btnMapAll._control.on("click", function () {
                    var wardId = Number(domControls.selectWard._control.val());
                    var locationId = Number(domControls.selectLocation._control.val());
                    var groupId = Number(domControls.selectPayheadGroup._control.val());
                    alertify.confirm(
                      "Confirm to Map!",
                      "Are you sure to map all unmapped assessees in current scope(group,ward,location)?",
                      function () {
                          domControls.btnMapAll.mappAll(
                            groupId,
                            wardId,
                            locationId,
                            function (r) {
                                if (r === true) {
                                    domControls.chkFilterAssessee._control.trigger("change");
                                }
                            }
                          );
                      },
                      function () { }
                    );
                });
                domControls.btnUnMapAll._control.on("click", function () {
                    var wardId = Number(domControls.selectWard._control.val());
                    var locationId = Number(domControls.selectLocation._control.val());
                    var groupId = Number(domControls.selectPayheadGroup._control.val());
                    alertify.confirm(
                      "Confirm to Un-Map!",
                      "Are you sure to un-map all assessees in current scope(group,ward,location)?",
                      function () {
                          domControls.btnUnMapAll.unMapAll(
                            groupId,
                            wardId,
                            locationId,
                            function (r) {
                                if (r === true) {
                                    domControls.chkFilterAssessee._control.trigger("change");
                                }
                            }
                          );
                      },
                      function () { }
                    );
                });
                parentContainer._control.on(
                  "dblclick",
                  domControls.btnUnmappedAssesse._selector,
                  function () {
                      var wardId = Number(domControls.selectWard._control.val());
                      var locationId = Number(domControls.selectLocation._control.val());
                      var assesseeId = Number($(this).attr("data-assesseeId"));
                      var groupId = Number(domControls.selectPayheadGroup._control.val());
                      domControls.btnUnmappedAssesse.mapGroup(
                        groupId,
                        wardId,
                        locationId,
                        assesseeId,
                        function (r) {
                            if (r === true) {
                                domControls.chkFilterAssessee._control.trigger("change");
                            }
                        }
                      );
                  }
                );
                parentContainer._control.on(
                  "dblclick",
                  domControls.btnMappedAssessee._selector,
                  function () {
                      var assesseeId = Number($(this).attr("data-assesseeId"));
                      domControls.btnMappedAssessee.unMapGroup(assesseeId, function (r) {
                          if (r === true) {
                              domControls.chkFilterAssessee._control.trigger("change");
                          }
                      });
                  }
                );

                domControls.btnAddGroup._control.on("click", function (e) {
                    e.preventDefault();
                    alertify.prompt(
                      "Enter New Group Name",
                      "Enter 10 Character Group Name",
                      "",
                      function (evt, r) {
                          var groupName = r.trim();
                          if (groupName.length > 10) {
                              alert("Group name can not exceed 10 char");
                              evt.cancel = true;
                          }
                          if (groupName.length < 1) {
                              alert("Enter group name");
                              evt.cancel = true;
                          }

                          parentContainer.addNewGroup(groupName, function (insertedGroup) {
                              if (insertedGroup) {
                                  var rr = insertedGroup;
                                  domControls.selectPayheadGroup._control.append(
                                    "<option value='" +
                                    rr.GroupID +
                                    "'>" +
                                    rr.GroupName +
                                    "</option>"
                                  );
                                  domControls.selectPayheadGroup._control
                                    .find("option[value='" + rr.GroupID + "']")
                                    .prop("selected", true);
                              }
                          });
                      },
                      function () { }
                    );
                });
                domControls.btnEditGroup._control.on("click", function (e) {
                    e.preventDefault();
                    if (domControls.selectPayheadGroup._control.val() == "0") {
                        alertify.error(
                          "'" +
                          domControls.selectPayheadGroup._control
                          .find("option:selected")
                          .text() +
                          "' is not editable"
                        );
                        return;
                    }
                    parentContainer.setTitle(
                      "Map Un-Map Assessees to Payhead Group- " +
                      domControls.selectPayheadGroup._control
                      .find("option:selected")
                      .text()
                    );
                    domControls.chkFilterAssessee._control.prop("checked", false);
                    domControls.chkFilterAssessee._control.trigger("change");
                    parentContainer._control.modal("show");
                });
                parentContainer._control.on("click", ".load-more-assessee", function () {
                    var objs = [{
                        assesseeId: 1,
                        Name: "bj",
                        Holding: "11/2"
                    },
                      {
                          assesseeId: 2,
                          Name: "bj2",
                          Holding: "211/2"
                      },
                      {
                          assesseeId: 3,
                          Name: "bj3",
                          Holding: "311/2"
                      },
                      {
                          assesseeId: 4,
                          Name: "4bj",
                          Holding: "411/2"
                      },
                      {
                          assesseeId: 1,
                          Name: "bj",
                          Holding: "11/2"
                      },
                      {
                          assesseeId: 2,
                          Name: "bj2",
                          Holding: "211/2"
                      },
                      {
                          assesseeId: 3,
                          Name: "bj3",
                          Holding: "311/2"
                      },
                      {
                          assesseeId: 4,
                          Name: "4bj",
                          Holding: "411/2"
                      }
                    ];
                    domControls.cntrChkAssesseeList.appendAssessees(objs);
                });
                domControls.chkFilterAssessee._control.on("change", function () {
                    if ($(this).is(":checked")) {
                        var groupId = domControls.selectPayheadGroup._control.val();
                        var wardId = Number(domControls.selectWard._control.val());
                        var locationId = Number(domControls.selectLocation._control.val());
                        var nameHolding = null;
                        domControls.cntrChkAssesseeList.loadAssessees(
                          groupId,
                          wardId === 0 ? null : wardId,
                          locationId === 0 ? null : locationId,
                          nameHolding,
                          function () {
                              domControls.cntrChkAssesseeList._control.show("fast");
                          }
                        );
                        $(".chck-list-status").show();
                    } else {
                        domControls.cntrChkAssesseeList.unload();
                        $(".chck-list-status").hide();
                    }
                });
                domControls.selectLocation._control.on("change", function () {
                    domControls.chkFilterAssessee.changeVisibilty();
                    if (Number($(this).val()) == 0) {
                        domControls.chkFilterAssessee._control.prop("checked", false);
                    }
                    domControls.chkFilterAssessee._control.trigger("change");
                });
                domControls.selectWard._control.on("change", function () {
                    domControls.chkFilterAssessee.changeVisibilty();
                    domControls.selectLocation.changeVisibilty();
                    domControls.selectLocation.Loaddata($(this).val(), function (r) {
                        domControls.selectLocation._control.trigger("change");
                    });
                });
            };

            (function () {
                bindEvent();
                pollyFill();
                domControls.selectPayheadGroup.Loaddata();
            })();
        };
        (function () {
            PayheadExtension();
        })();
    };

    var INIT = function () {
        $("#divRebate").prop("hidden", true);
        $("#divInterest").prop("hidden", true);
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    })();
});