﻿
$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.Login = function (callback) {
            $.ajax({
                type: "POST",
                url: "/LoginMenuWise/Login",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.CitizenRegistration = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/OtherServices/OtherServices/Registration",
                data: JSON.stringify(data),
                dataType: "json",
                async: true,
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };

        this.otpverification = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/OtherServices/OtherServices/otpVerify",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };

        this.Changepassword = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/OtherServices/OtherServices/ForgetPassword",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };

        this.ResendOtpVerification = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/OtherServices/OtherServices/ResendOtp",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };



        this.District = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Shared/District/GetDistricts",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.Municipality = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Shared/Municipality/GetMunicipalitesByDistrictID",
                data: "{DistrictID:" + data + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.GetWard = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Ward/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocation = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Location/GetLocations",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.CitizenLogin = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Home/CitizenLogin",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.CollectorLogin = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Home/MunicipalityLogin",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.AdministrativeLogin = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Home/AdminstrativeLogin",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetHoldingsBySuggetion = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Shared/Assesse/SearchHoldingSuggetion",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.OtherCitizenLogin = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Home/OtherCitizenLogin",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    callback(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var DistrictDataSource = [];
        var MunicipalityDataSource = [];
        var CitizenLogin = function () {
            var modal = $("#ModelCitizen");
            modal.addCommand = function (cmd) {
                modal.attr('data-cmd', cmd)
                return modal;
            };
            modal.deleteCommand = function () {
                modal.removeAttr('data-cmd');
                return modal;
            };
            var ctrl_CitizenDistrict = $("#CitizenDistrict");
            var ctrl_CitizenMunicipality = $("#CitizenMunicipality");
            var ctrl_CitizenWard = $("#CitizenWard");
            var ctrl_CitizenLocation = $("#CitizenLocation");
            var cntr_CitizenHoldingNumber = $("#CitizenHoldingNumber");
            var AutocompleteSources = {
                HoldingAutoComplete: function () {
                    return {
                        source: function (request, response) {
                            //reset area
                            //DomControls.ctrl_hdnAssessee.val('');
                            //reset area
                            var municipalityID = ctrl_CitizenMunicipality.val();
                            var locationID = ctrl_CitizenLocation.val();
                            var wardID = ctrl_CitizenWard.val();
                            var data = { MunicipalityID: municipalityID, WardID: wardID, LocationID: locationID, HoldingSuggetion: request.term };
                            if (data.MunicipalityID == undefined || data.WardID == undefined || data.LocationID == undefined) {
                                return;
                            }
                            serverObject.GetHoldingsBySuggetion(data, function (Response) {
                                if (Response.Status === 200) {
                                    var serverResponse = Response.Data;
                                    var userDataAutoComplete = [];
                                    if ((serverResponse).length > 0) {

                                        $.each(serverResponse, function (index, item) {
                                            userDataAutoComplete.push({
                                                value: item,
                                                label: item
                                            });
                                        });

                                    }
                                    response(userDataAutoComplete);
                                } else {
                                    alert("Error: " + Response.Message);
                                }

                            });
                        },
                        select: function (e, i) {
                            cntr_CitizenHoldingNumber.val(i.item);
                            //$("#txtAssessee").val(i.item.label);
                        },
                        change: function (e, i) {
                            if (!i.item) {
                                //Resetors.ResetAssesse();
                            }
                        }
                    };
                }
            }
            var BindEvent = function () {
                $("#btnCitizenLogin").on('click', OnLoginCitizen);
                $("#CitizenDistrict").on('change', DistrictChange);
                $("#CitizenMunicipality").on('change', MunicipalityChange);
                $("#CitizenWard").on('change', WardChange);
                //$("#CitizenHoldingNumber").autocomplete(AutocompleteSources.HoldingAutoComplete());
                $(".login-assmt").on('click', function () {
                    modal.addCommand('rdct_assmt');
                    modal.modal('show');
                });
                modal.on('hidden.bs.modal', function () {
                    modal.deleteCommand();
                })
            };
            var ResetWindow = function () {
                location.reload(true);
            };
            var getDataFromView = function () {
                var DistrictID = $("#CitizenDistrict").val();
                var MunicipalityID = $("#CitizenMunicipality").val();
                var WardID = $("#CitizenWard").val();

                var LocationID = $("#CitizenLocation").val();
                var HoldingNumber = $("#CitizenHoldingNumber").val();

                var data = {
                    DistrictID: DistrictID, MunicipalityID: MunicipalityID, WardID: WardID,
                    LocationID: LocationID, HoldingNumber: HoldingNumber
                };
                return data;
            };
            var feedToForm = function (data) {
            };
            var PopulateDistrictData = function () {
                ctrl_CitizenDistrict.empty();
                ctrl_CitizenDistrict.append('<option value="">--select--</option>');
                $.each(DistrictDataSource, function (index, value) {
                    var label = value.DistrictName;
                    var id = value.DistrictID;
                    ctrl_CitizenDistrict.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var PopulateMunicipalityData = function () {
                ctrl_CitizenMunicipality.empty();
                ctrl_CitizenMunicipality.append('<option value="">--select--</option>');
                $.each(MunicipalityDataSource, function (index, value) {
                    var label = value.MunicipalityName;
                    var id = value.MunicipalityID;
                    ctrl_CitizenMunicipality.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var PopulateWardData = function (data) {
                ctrl_CitizenWard.empty();
                ctrl_CitizenWard.append("<option value=''>--select--</option>");
                var shortedWards = JSLINQ(data).OrderBy(function (item) { return Number(item.WardName) }).items;
                $.each(shortedWards, function (index, value) {
                    var label = value.WardName;
                    var id = value.WardID;
                    ctrl_CitizenWard.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var PopulateLocationData = function (LocationDataSource) {
                ctrl_CitizenLocation.empty();
                ctrl_CitizenLocation.append("<option value=''>--select--</option>");
                $.each(LocationDataSource, function (index, value) {
                    var label = value.LocationName;
                    var id = value.LocationID;
                    ctrl_CitizenLocation.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var PopulateTable = function () {
                (function () {
                    //   RegisterEvent();               
                }());
            };
            var DistrictChange = function () {
                //  alert(ctrl_CitizenDistrict.val());
                if (ctrl_CitizenDistrict.val() != "") {

                    serverObject.Municipality(ctrl_CitizenDistrict.val(), function (response) {
                        MunicipalityDataSource = response.Data;
                        PopulateMunicipalityData();
                    });
                } else {
                    ctrl_CitizenMunicipality.empty();
                }
            };
            var MunicipalityChange = function () {
                if (ctrl_CitizenMunicipality.val() != "") {
                    var data = { municipalityID: ctrl_CitizenMunicipality.val(), wardName: '' };
                    serverObject.GetWard(data, function (response) {
                        WardDataSource = response;
                        PopulateWardData(WardDataSource);
                    });
                } else {
                    ctrl_CitizenWard.empty();
                }
            };
            var WardChange = function () {
                if (ctrl_CitizenWard.val() != "") {
                    var data = { municipalityID: ctrl_CitizenMunicipality.val(), wardID: ctrl_CitizenWard.val(), locationName: '' };
                    serverObject.GetLocation(data, function (response) {
                        LocationDataSource = response;
                        PopulateLocationData(LocationDataSource);
                    });
                } else {
                    ctrl_CitizenLocation.empty();
                }
            };
            var Validator = {
                CitizenLogionValidate: function () {
                    $("#frm_ModelCitizen").validate({
                        rules: {
                            CitizenDistrict: { required: true },
                            CitizenMunicipality: { required: true },
                            CitizenWard: { required: true },
                            CitizenLocation: { required: true },
                            CitizenHoldingNumber: { required: true }
                        },
                        messages: {
                            CitizenDistrict: {
                                required: "Please select correct District."
                            },
                            CitizenMunicipality: {
                                required: "Please select correct municipality."
                            },
                            CitizenWard: {
                                required: "Please select correct ward."
                            },
                            CitizenLocation: {
                                required: "Please select correct location."
                            },
                            CitizenHoldingNumber: {
                                required: "Please Enter Correct Holding Number."
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                    return $('#frm_ModelCitizen').valid();
                }
            };
            var GetFormData_Citizen = function () {
                return {
                    MunicipalityID: ctrl_CitizenMunicipality.val(),
                    WardID: ctrl_CitizenWard.val(),
                    LocationID: ctrl_CitizenLocation.val(),
                    HoldingNo: cntr_CitizenHoldingNumber.val(),
                    cmd: $("#ModelCitizen").attr('data-cmd')
                };
            };
            var OnLoginCitizen = function () {
                if (Validator.CitizenLogionValidate() == true) {
                    serverObject.CitizenLogin(GetFormData_Citizen(), function (response) {
                        if (response.Status == 200 && response.Data.IsLoggedIn == true) {
                            //redirect to authoized page
                            $("#spn_frm_ModelCitizen").css("color", "green").html("Assessee Successfully Verified");
                            setTimeout(function () {
                                location.href = response.Data.RedirectUrl;
                            }, 100);
                        } else //if (response.Status == 401) 
                        {
                            $("#spn_frm_ModelCitizen").css("color", "red").html(response.Message);
                        }

                    });
                }
            };
            (function () {
                serverObject.District(function (response) {
                    DistrictDataSource = response.Data;
                    PopulateDistrictData();
                });
                PopulateTable();
                BindEvent();
            }());
        };

        var AdministrativeLogin = function () {
            var ddlAdminType = $("#ddlAdminType");
            var txtAdmUserName = $("#txtAdmUserName");
            var txtAdmPassword = $("#txtAdmPassword");
            var BtnAdmLogin = $("#admBtnLogin");
            var GetFormData = function () {
                return {
                    ValuationBoardLogin: { AdminType: ddlAdminType.val(), UserName: txtAdmUserName.val(), Password: txtAdmPassword.val() }
                };
            };
            var Validator = {
                AddValidator: function () {
                    $("#frm_Adminstration").validate({
                        rules: {
                            ddlAdminType: { required: true },
                            txtAdmUserName: { required: true },
                            txtAdmPassword: { required: true },
                        },
                        messages: {
                            ddlAdminType: {
                                required: "Please select Admin Type."
                            },
                            txtAdmUserName: {
                                required: "Please Enter Password."
                            },
                            txtAdmPassword: {
                                required: "Please Enter Password"
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                },
                Validate: function () {
                    return $('#frm_Adminstration').valid();
                }
            };
            var BindEvent = function () {
                BtnAdmLogin.on('click', onClickLogin);
            };
            var onClickLogin = function () {
                if (Validator.Validate()) {
                    var payLoad = GetFormData();
                    serverObject.AdministrativeLogin(payLoad, function (r) {
                        if (r.Status === 200 && r.Data.IsLoggedIn == true) {
                            $("#spn_frm_msg_Adm").css("color", "green").html("Login Successful");
                            setTimeout(function () {
                                location.href = r.Data.RedirectUrl;
                            }, 100);
                        } else if (r.Status == 401) {
                            $("#spn_frm_msg_Adm").css("color", "red").html(r.Message);
                        } else {
                            alertify.error(r.Message);
                        }
                    });
                }
            };
            (function () {
                Validator.AddValidator();
                BindEvent();
            }());
        };

        var MunicipalityLogin = function () {
            var ddlMnDistrict = $("#ddlMnDistrict");
            var txtMnUserName = $("#txtMnUserName");
            var txtMnPassword = $("#txtMnPassword");
            var ddlMnMunicipalityID = $("#ddlMnMunicipalityID");
            var MunicipalitybtnLogin = $("#MunicipalitybtnLogin");


            var GetFormData = function () {
                return { MunicipalityID: ddlMnMunicipalityID.val(), UserName: txtMnUserName.val(), Password: txtMnPassword.val() };
            };
            var Validator = {
                MnLogionValidate: function () {
                    $("#frm_Mncpality").validate({
                        rules: {
                            ddlMnDistrict: { required: true },
                            ddlMnMunicipalityID: { required: true },
                            txtMnUserName: { required: true },
                            txtMnPassword: { required: true }
                        },
                        messages: {
                            ddlMnDistrict: {
                                required: "Please select correct District."
                            },
                            ddlMnMunicipalityID: {
                                required: "Please select correct municipality."
                            },
                            txtMnUserName: {
                                required: "Please Enter Username"
                            },
                            txtMnPassword: {
                                required: "Please Enter Password"
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                    return $('#frm_Mncpality').valid();
                }
            };
            var OnLoginCollector = function () {
                if (Validator.MnLogionValidate() == true) {
                    serverObject.CollectorLogin(GetFormData(), function (response) {
                        if (response.Status == 200 && response.Data.IsLoggedIn == true) {
                            //redirect to authoized page
                            $("#spn_frm_msg_Mn").css("color", "green").html("Login Successful");
                            setTimeout(function () {
                                location.href = response.Data.RedirectUrl;
                            }, 100);
                        } else if (response.Status == 401) {
                            $("#spn_frm_msg_Mn").css("color", "red").html(response.Message);
                        }

                    });
                }
            };
            var BindEvent = function () {
                $("#MunicipalitybtnLogin").on('click', OnLoginCollector);
                $("#ddlMnDistrict").on('change', DistrictChange);
                $("#ModelMunicipality").off("hidden.bs.modal", OnCloseModal);
                $("#ModelMunicipality").on("hidden.bs.modal", OnCloseModal);
            };

            var OnCloseModal = function () {
                resetModalForm();
            };
            var resetModalForm = function () {
                ddlMnDistrict.find("option:first").attr("selected", true);
                ddlMnMunicipalityID.find("option:first").attr("selected", true);
                txtMnUserName.val('');
                txtMnPassword.val('');
            };
            var PopulateDistrictData = function () {
                ddlMnDistrict.empty();
                ddlMnDistrict.append('<option value="">--select--</option>');
                $.each(DistrictDataSource, function (index, value) {
                    var label = value.DistrictName;
                    var id = value.DistrictID;
                    ddlMnDistrict.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var PopulateMunicipalityData = function (MunicipalityDataSource1) {
                ddlMnMunicipalityID.empty();
                ddlMnMunicipalityID.append('<option value="">--select--</option>');
                $.each(MunicipalityDataSource1, function (index, value) {
                    var label = value.MunicipalityName;
                    var id = value.MunicipalityID;
                    ddlMnMunicipalityID.append("<option value='" + id + "'>" + label + "</option>");
                });
            };
            var DistrictChange = function () {
                //  alert(ctrl_CitizenDistrict.val());
                if (ddlMnDistrict.val() != "") {

                    serverObject.Municipality(ddlMnDistrict.val(), function (response) {
                        MunicipalityDataSource = response.Data;
                        PopulateMunicipalityData(MunicipalityDataSource);
                    });
                } else {
                    ddlMnMunicipalityID.empty();
                }
            };
            (function () {
                serverObject.District(function (response) {
                    DistrictDataSource = response.Data;
                    PopulateDistrictData();
                });
                BindEvent();
            }());
        };

        var CitizenRegistration = function () {
            var txtusername = $("#form_username")
            var txtname = $("#form_name");
            var txtdesig = $("#form_designation");
            var txtEmail = $("#form_email");
            var txtMobile = $("#form_mobile");
            var txtpassword = $("#form_password");
            var txtconfmpass = $("#form_confirm_password");
            var txtcompanyname = $("#form_company_name");
            var txtcompanyadd = $("#form_company_address");
            var btnReg = $(".btn-Register");
            var otptext = $(".form_otp");
            var btnOtp = $(".btn-SendOTP");
            var resendotp = $(".Btn-resendotp");
            var resendmobileverify = $(".Btn-mobileresendverify");
            var mobilenover = $(".form_mobile_verify");
            var btnmobileverfy = $('.btn-verifymobile');
            var mobileforget = $(".form_mobile_forget");
            var btnotpForget = $(".btn-GetOtpdata");
            var btnForgetPass = $(".btn-proceForgetpass");
            var btnchngpass = $(".btn-chngforpass");
            var typeofreg = '1';
            var mobileGlobal = '';
            var gnewmobile = '';
            var guserId = '';
            $('.radiobtn').change(function () {
                typeofreg = $(this).val();
            });

            var GetFormData = function () {
                return {
                    CitizenReg: { TypeOfRegistration: typeofreg,  UserName: txtusername.val(), Name: txtname.val(), Designation: txtdesig.val(), Email: txtEmail.val(), MobileNo: txtMobile.val(), Password: txtpassword.val(), ConfirmPassword: txtconfmpass.val(), CompanyName: txtcompanyname.val(), CompanyAddress: txtcompanyadd.val() }
                };
            };

            var Validator = {
                AddValidator: function () {
                    $("#Citizen-Registration-form").validate({
                        rules: {
                            txtusername: { required: true },
                            txtname: { required: true },
                            txtdesig: { required: true },
                            txtEmail: { required: true },
                            txtMobile: { required: true },
                            txtpassword: { required: true },
                            txtconfmpass: {
                                required: true,
                                equalTo: '#form_password'
                            },
                            txtcompanyname: { required: true },
                            txtcompanyadd: { required: true }
                        },
                        messages: {
                            txtusername: {
                                required: "Please Enter User Name."
                            },
                            txtname: {
                                required: "Please Enter Aplicant Name."
                            },
                            txtdesig: {
                                required: "Please Enter Designation."
                            },
                            txtEmail: {
                                required: "Please Enter EmailID."
                            },
                            txtMobile: {
                                required: "Please Enter Mobile No."
                            },
                            txtpassword: {
                                required: "Please Enter Password."
                            },
                            txtconfmpass: {
                                required: "Please Enter Confirm Password.",
                                equalTo: "Password Doesn't Match!!"
                            },
                            txtcompanyname: {
                                required: "Please Enter Company Name."
                            },
                            txtcompanyadd: {
                                required: "Please Enter Company Address."
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                    return $('#Citizen-Registration-form').valid();
                }
                //Validate: function () {
                //    return $('#Citizen-Registration-form').valid();
                //}
            };

            var Mobileverifyvalid = {
                addmobvalid: function () {
                    $("#Citizen-MobileVerfication-form").validate({
                        rules: {
                            mobilenover: { required: true },

                        },
                        messages: {
                            mobilenover: {
                                required: "Please Enter Mobile No."
                            }

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                    return $('#Citizen-MobileVerfication-form').valid();
                }
                //Validate: function () {
                //    return $('#Citizen-Registration-form').valid();
                //}
            };

            var MobileForgetpassvalid = {
                addForgetmobvalid: function () {
                    $("#Citizen-ForgetPass-form").validate({
                        rules: {
                            mobileforget: { required: true },

                        },
                        messages: {
                            mobileforget: {
                                required: "Please Enter Mobile No."
                            }

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }

                    });
                    return $('#Citizen-ForgetPass-form').valid();
                }
                //Validate: function () {
                //    return $('#Citizen-Registration-form').valid();
                //}
            };


            var BindEvent = function () {
                btnReg.on('click', onClickReg);
                btnOtp.on('click', onClickOtp);
                resendotp.on('click', onclickResendotp);
                resendmobileverify.on('click', onclickmobver);
                btnmobileverfy.on('click', onclickotpmob);
                btnotpForget.on('click', OtpSendforForgetpass);
                btnForgetPass.on('click', ProcessForForgetPass);
                btnchngpass.on('click', ProcessForChngpassword);
            };


            //

            //

            var onClickReg = function () {
                
                mobileGlobal = '';
                gnewmobile = '';
                guserId = '';

                if (Validator.AddValidator() == true && validateCaptcha()) {
                    var CitizenReg = GetFormData();
                    serverObject.CitizenRegistration(CitizenReg, function (r) {
                        if (r.Status === 200) {
                            if ((r.Data.Status === 1 && r.Data.isExist === 0 && r.Data.isVerified === 0)) {
                                $('.modal-title').html('OTP Verfication');
                                $('#optmsg').html(r.Data.Msg);
                                mobileGlobal = r.Data.Mobile;
                                $('#lblMobile').text(r.Data.Mobile);
                                $('#Citizen-OTP-form').show();
                                $('#Citizen-Registration-form').hide();
                                $('#Citizen-ForgetPass-form').hide();
                                $('#Citizen-ConfirmPass-form').hide();
                            }
                            else {
                                swal("", r.Data.Msg, "error");
                            }
                        } else if (r.Status == 401) {
                            swal("", 'Unsucessful!Contact To Admin', "error");
                        } else {
                            swal("", r.Message, "error");
                        }
                    });
                }
            };
            var onclickmobver = function () {
                if (Mobileverifyvalid.addmobvalid() == true) {
                    var Resendotpdata = { Type: "R", MobileNo: mobilenover.val() }; //F->Forget Password,R->Resend Otp
                    if (mobilenover.val().trim() != '') {
                        serverObject.ResendOtpVerification(Resendotpdata, function (r) {
                            if (r.Status === 200) {
                                if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
                                    //alert('Your Otp has been Sent to your Mobile No');
                                    swal("", r.Data.Msg, "success");
                                } else {
                                    alert(r.Data.Msg);
                                }
                            } else if (r.Status == 401) {
                                swal("", 'Unsucessful!Contact To Admin', "error");
                            } else {
                                swal("", r.Message, "error");
                            }
                        });
                    }
                    else {
                        alert('Entered mobile no is not correct or Blank');
                    };
                }
            };
            //
            var OtpSendforForgetpass = function () {
                if (MobileForgetpassvalid.addForgetmobvalid() == true) {
                    var Resendotpdata = { Type: "F", MobileNo: mobileforget.val() };//F->Forget Password,R->Resend Otp
                    if (mobileforget.val().trim() != '') {
                        serverObject.ResendOtpVerification(Resendotpdata, function (r) {
                            if (r.Status === 200) {
                                if ((r.Data.Status === 1 && r.Data.isVerified === 1)) {

                                    swal("", r.Data.Msg, "success");
                                } else {

                                    swal("", r.Data.Msg, "error");
                                }
                            } else if (r.Status == 401) {

                                swal("", 'Unsucessful!Please Contact To Admin', "error");

                            } else {

                                swal("", r.Message, "error");
                            }
                        });
                    }
                    else {

                        swal("", 'Entered mobile no is not correct or Blank', "error");
                    };
                }
            };
            //

            //
            var ProcessForForgetPass = function () {
                if (MobileForgetpassvalid.addForgetmobvalid() == true) {
                    var Resendotpdata = { Otpdata: $('.form_otp_ver_forget').val(), MobileNo: mobileforget.val() };//F->Forget Password,R->Resend Otp
                    if (mobileforget.val().trim() != '' && $('.form_otp_ver_forget').val() != '') {
                        serverObject.otpverification(Resendotpdata, function (r) {
                            if (r.Status === 200) {
                                if ((r.Data.Status === 1)) {
                                    gnewmobile = r.Data.Mobile;
                                    guserId = r.Data.UserID;

                                    $('.modal-title').html('Forget Password');
                                    $('#Citizen-OTP-form').hide();
                                    $('#Citizen-Login-form').hide();
                                    $('#Citizen-Registration-form').hide();
                                    $('#Citizen-ForgetPass-form').hide();
                                    $('#Citizen-ConfirmPass-form').show();
                                    swal("", r.Data.Msg, "success");
                                } else {

                                    swal("", r.Data.Msg, "error");
                                }
                            } else if (r.Status == 401) {

                                swal("", 'Unsucessful!Please Contact To Admin', "error");

                            } else {

                                swal("", r.Message, "error");
                            }
                        });
                    }
                    else {

                        swal("", 'Entered mobile no and Otp is not correct or Blank', "error");
                    };
                }
            };

            var validPass = function () {
                if ($('.form_forget_pass').val() == $('.form_confirm_pass').val()) {
                    return true;
                }
                else {
                    return false;
                }
            };

            var ProcessForChngpassword = function () {
                if (validPass) {
                    var ChangeForgetData = { Password: $('.form_forget_pass').val(), confPassWord: $('.form_confirm_pass').val(), MobileNo: gnewmobile, UserId: guserId };//F->Forget Password,R->Resend Otp
                    if ($('.form_forget_pass').val().trim() != '' && $('.form_confirm_pass').val() != '') {
                        serverObject.Changepassword(ChangeForgetData, function (r) {
                            if (r.Status === 200) {
                                if ((r.Data.Status === 1)) {

                                    swal("", r.Data.Msg, "success");
                                    $('.modal-title').html('Login');
                                    $('#Citizen-OTP-form').hide();
                                    $('#Citizen-Login-form').show();
                                    $('#Citizen-Registration-form').hide();
                                    $('#Citizen-ForgetPass-form').hide();
                                    $('#Citizen-ConfirmPass-form').hide();

                                } else {

                                    swal("", r.Data.Msg, "error");
                                }
                            } else if (r.Status == 401) {

                                swal("", 'Unsucessful!Please Contact To Admin', "error");

                            } else {

                                swal("", r.Message, "error");
                            }
                        });
                    }
                    else {

                        swal("", 'Password and Confirm Password cannot be Blank', "error");
                    };
                } else {
                    swal("", 'Password and Confirm Password are not matched', "error");
                }
            };
            //
            var onclickResendotp = function () {
                var Resendotpdata = { Type: "R", MobileNo: mobileGlobal };//F->Forget Password,R->Resend Otp
                if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal) {
                    serverObject.ResendOtpVerification(Resendotpdata, function (r) {
                        if (r.Status === 200) {
                            if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
                                //alert('Your Otp has been Sent to your Mobile No');
                                swal("", r.Data.Msg, "success");
                            } else {
                                swal("", r.Data.Msg, "error");
                            }
                        } else if (r.Status == 401) {
                            swal("", 'Unsucessful!Please Contact To Admin', "error");
                        } else {
                            swal("", r.Message, "error");
                        }
                    });
                }
                else {
                    swal("", 'Entered Mobile No cannot be Blank or Match', "error");
                };
            };
            //
            var onClickOtp = function () {
                var otpdata = { Otpdata: otptext.val().trim(), MobileNo: mobileGlobal };
                if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal && otptext.val().trim() != '') {
                    serverObject.otpverification(otpdata, function (r) {
                        if (r.Status === 200) {
                            if ((r.Data.Status === 1)) {
                                $('.modal-title').html('Login');
                                $('#Citizen-OTP-form').hide();
                                $('#Citizen-Login-form').show();
                                $('#Citizen-Registration-form').hide();
                                $('#Citizen-ForgetPass-form').hide();
                                $('#Citizen-ConfirmPass-form').hide();
                            } else {
                                swal("", r.Data.Msg, "error");
                            }

                        } else if (r.Status == 401) {

                            swal("", 'Unsucessful!Please Contact To Admin', "error");
                        } else {
                            swal("", r.Message, "error");
                        }
                    });
                }
                else {
                    swal("", 'Entered Mobile No cannot be Blank or Match', "error");
                };
            };

            var onclickotpmob = function () {
                var otpdata = { Otpdata: $('.form_otp_ver').val().trim(), MobileNo: $('.form_mobile_verify').val().trim() };
                if ($('.form_otp_ver').val().trim() != '' && $('.form_mobile_verify').val().trim() != '') {
                    serverObject.otpverification(otpdata, function (r) {
                        if (r.Status === 200) {
                            if ((r.Data.Status === 1)) {
                                $('.modal-title').html('Login');
                                $('#Citizen-OTP-form').hide();
                                $('#Citizen-MobileVerfication-form').hide();
                                $('#Citizen-Login-form').show();
                                $('#Citizen-Registration-form').hide();
                                $('#Citizen-ForgetPass-form').hide();
                                $('#Citizen-ConfirmPass-form').hide();
                            } else {
                                swal("", r.Data.Msg, "error");
                            }

                        } else if (r.Status == 401) {

                            swal("", 'Unsucessful!Please Contact To Admin', "error");
                        } else {
                            swal("", r.Message, "error");
                        }
                    });
                }
                else {
                    swal("", 'Mobile No and otp Cannot be Blank', "error");
                };
            };


            (function () {
                Validator.AddValidator();
                BindEvent();
            }());
        };

        var OtherCitizenLogin = function () {
            var txtothUserName = $("#form_user_name");
            var txtothPassword = $("#form_password_login");
            var BtnOtherLogin = $(".btn-Login");
            var GetFormData = function () {
                return {
                    userLogin: { UserName: txtothUserName.val(), Password: txtothPassword.val() }
                };
            };
            var Validator = {
                AddValidator: function () {
                    $("#Citizen-Login-form").validate({
                        rules: {

                            txtothUserName: { required: true },
                            txtothPassword: { required: true },
                        },
                        messages: {

                            txtothUserName: {
                                required: "Please Enter Password."
                            },
                            txtothPassword: {
                                required: "Please Enter Password"
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                            error.css('color', 'red');
                        }
                    });
                },
                Validate: function () {
                    return $('#Citizen-Login-form').valid();
                }
            };
            var BindEvent = function () {
                BtnOtherLogin.on('click', onClickLogin);
            };

            var onClickLogin = function () {
                if (Validator.Validate()) {
                    var payLoad = GetFormData();
                    serverObject.OtherCitizenLogin(payLoad, function (r) {
                        if (r.Status === 200 && r.Data.IsLoggedIn == true) {
                            $("#spn_frm_msg_otherciti").css("color", "green").html("Login Successful");
                            setTimeout(function () {
                                location.href = r.Data.RedirectUrl;
                            }, 100);
                        } else if (r.Status == 401) {
                            $("#spn_frm_msg_otherciti").css("color", "red").html(r.Message);
                        } else {
                            alertify.error(r.Message);
                        }
                    });
                }
            };

            (function () {
                Validator.AddValidator();
                BindEvent();
            }());
        };

        this.app_Initiate = function () {
            CitizenLogin();
            MunicipalityLogin();
            AdministrativeLogin();
            CitizenRegistration();
            OtherCitizenLogin();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();

    }());


});





//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OLD VERSION +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//$(document).ready(function () {
//    var SERVER_OBJECT = function () {
//        this.Login = function (callback) {
//            $.ajax({
//                type: "POST",
//                url: "/LoginMenuWise/Login",
//                data: null,
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                success: function (response) {
//                    callback(response);
//                },
//                failure: function (response) {
//                    alert(response.d);
//                }
//            });
//        };
//        this.CitizenRegistration = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/OtherServices/OtherServices/Registration",
//                data: JSON.stringify(data),
//                dataType: "json",
//                async: true,
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };

//        this.otpverification = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/OtherServices/OtherServices/otpVerify",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };

//        this.ResendOtpVerification = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/OtherServices/OtherServices/ResendOtp",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };



//        this.District = function (callback) {
//            $.ajax({
//                type: "POST",
//                url: "/Shared/District/GetDistricts",
//                data: null,
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                success: function (response) {
//                    callback(response);
//                },
//                failure: function (response) {
//                    alert(response.d);
//                }
//            });
//        };
//        this.Municipality = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                url: "/Shared/Municipality/GetMunicipalitesByDistrictID",
//                data: "{DistrictID:" + data + "}",
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                success: function (response) {
//                    callback(response);
//                },
//                failure: function (response) {
//                    alert(response.d);
//                }
//            });
//        };
//        this.GetWard = function (data, callBack) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Shared/Ward/GetWards",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    if (data.Status == 200) {
//                        callBack(data.Data);
//                    }
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        }
//        this.GetLocation = function (data, callBack) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Shared/Location/GetLocations",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    if (data.Status == 200) {
//                        callBack(data.Data);
//                    }
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        }
//        this.CitizenLogin = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Home/CitizenLogin",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };
//        this.CollectorLogin = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Home/MunicipalityLogin",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };
//        this.AdministrativeLogin = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Home/AdminstrativeLogin",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };
//        this.GetHoldingsBySuggetion = function (data, callBack) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Shared/Assesse/SearchHoldingSuggetion",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callBack(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };
//        this.OtherCitizenLogin = function (data, callback) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "/Home/OtherCitizenLogin",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data) {
//                    callback(data);
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        };
//    };
//    var APP_OBJECT = function (serverObject) {
//        var DistrictDataSource = [];
//        var MunicipalityDataSource = [];
//        var CitizenLogin = function () {
//            var modal = $("#ModelCitizen");
//            modal.addCommand = function (cmd) {
//                modal.attr('data-cmd', cmd)
//                return modal;
//            };
//            modal.deleteCommand = function () {
//                modal.removeAttr('data-cmd');
//                return modal;
//            };
//            var ctrl_CitizenDistrict = $("#CitizenDistrict");
//            var ctrl_CitizenMunicipality = $("#CitizenMunicipality");
//            var ctrl_CitizenWard = $("#CitizenWard");
//            var ctrl_CitizenLocation = $("#CitizenLocation");
//            var cntr_CitizenHoldingNumber = $("#CitizenHoldingNumber");
//            var AutocompleteSources = {
//                HoldingAutoComplete: function () {
//                    return {
//                        source: function (request, response) {
//                            //reset area
//                            //DomControls.ctrl_hdnAssessee.val('');
//                            //reset area
//                            var municipalityID = ctrl_CitizenMunicipality.val();
//                            var locationID = ctrl_CitizenLocation.val();
//                            var wardID = ctrl_CitizenWard.val();
//                            var data = { MunicipalityID: municipalityID, WardID: wardID, LocationID: locationID, HoldingSuggetion: request.term };
//                            if (data.MunicipalityID == undefined || data.WardID == undefined || data.LocationID == undefined) {
//                                return;
//                            }
//                            serverObject.GetHoldingsBySuggetion(data, function (Response) {
//                                if (Response.Status === 200) {
//                                    var serverResponse = Response.Data;
//                                    var userDataAutoComplete = [];
//                                    if ((serverResponse).length > 0) {

//                                        $.each(serverResponse, function (index, item) {
//                                            userDataAutoComplete.push({
//                                                value: item,
//                                                label: item
//                                            });
//                                        });

//                                    }
//                                    response(userDataAutoComplete);
//                                } else {
//                                    alert("Error: " + Response.Message);
//                                }

//                            });
//                        },
//                        select: function (e, i) {
//                            cntr_CitizenHoldingNumber.val(i.item);
//                            //$("#txtAssessee").val(i.item.label);
//                        },
//                        change: function (e, i) {
//                            if (!i.item) {
//                                //Resetors.ResetAssesse();
//                            }
//                        }
//                    };
//                }
//            }
//            var BindEvent = function () {
//                $("#btnCitizenLogin").on('click', OnLoginCitizen);
//                $("#CitizenDistrict").on('change', DistrictChange);
//                $("#CitizenMunicipality").on('change', MunicipalityChange);
//                $("#CitizenWard").on('change', WardChange);
//                //$("#CitizenHoldingNumber").autocomplete(AutocompleteSources.HoldingAutoComplete());
//                $(".login-assmt").on('click', function () {
//                    modal.addCommand('rdct_assmt');
//                    modal.modal('show');
//                });
//                modal.on('hidden.bs.modal', function () {
//                    modal.deleteCommand();
//                })
//            };
//            var ResetWindow = function () {
//                location.reload(true);
//            };
//            var getDataFromView = function () {
//                var DistrictID = $("#CitizenDistrict").val();
//                var MunicipalityID = $("#CitizenMunicipality").val();
//                var WardID = $("#CitizenWard").val();

//                var LocationID = $("#CitizenLocation").val();
//                var HoldingNumber = $("#CitizenHoldingNumber").val();

//                var data = {
//                    DistrictID: DistrictID, MunicipalityID: MunicipalityID, WardID: WardID,
//                    LocationID: LocationID, HoldingNumber: HoldingNumber
//                };
//                return data;
//            };
//            var feedToForm = function (data) {
//            };
//            var PopulateDistrictData = function () {
//                ctrl_CitizenDistrict.empty();
//                ctrl_CitizenDistrict.append('<option value="">--select--</option>');
//                $.each(DistrictDataSource, function (index, value) {
//                    var label = value.DistrictName;
//                    var id = value.DistrictID;
//                    ctrl_CitizenDistrict.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var PopulateMunicipalityData = function () {
//                ctrl_CitizenMunicipality.empty();
//                ctrl_CitizenMunicipality.append('<option value="">--select--</option>');
//                $.each(MunicipalityDataSource, function (index, value) {
//                    var label = value.MunicipalityName;
//                    var id = value.MunicipalityID;
//                    ctrl_CitizenMunicipality.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var PopulateWardData = function (data) {
//                ctrl_CitizenWard.empty();
//                ctrl_CitizenWard.append("<option value=''>--select--</option>");
//                var shortedWards = JSLINQ(data).OrderBy(function (item) { return Number(item.WardName) }).items;
//                $.each(shortedWards, function (index, value) {
//                    var label = value.WardName;
//                    var id = value.WardID;
//                    ctrl_CitizenWard.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var PopulateLocationData = function (LocationDataSource) {
//                ctrl_CitizenLocation.empty();
//                ctrl_CitizenLocation.append("<option value=''>--select--</option>");
//                $.each(LocationDataSource, function (index, value) {
//                    var label = value.LocationName;
//                    var id = value.LocationID;
//                    ctrl_CitizenLocation.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var PopulateTable = function () {
//                (function () {
//                    //   RegisterEvent();               
//                }());
//            };
//            var DistrictChange = function () {
//                //  alert(ctrl_CitizenDistrict.val());
//                if (ctrl_CitizenDistrict.val() != "") {

//                    serverObject.Municipality(ctrl_CitizenDistrict.val(), function (response) {
//                        MunicipalityDataSource = response.Data;
//                        PopulateMunicipalityData();
//                    });
//                } else {
//                    ctrl_CitizenMunicipality.empty();
//                }
//            };
//            var MunicipalityChange = function () {
//                if (ctrl_CitizenMunicipality.val() != "") {
//                    var data = { municipalityID: ctrl_CitizenMunicipality.val(), wardName: '' };
//                    serverObject.GetWard(data, function (response) {
//                        WardDataSource = response;
//                        PopulateWardData(WardDataSource);
//                    });
//                } else {
//                    ctrl_CitizenWard.empty();
//                }
//            };
//            var WardChange = function () {
//                if (ctrl_CitizenWard.val() != "") {
//                    var data = { municipalityID: ctrl_CitizenMunicipality.val(), wardID: ctrl_CitizenWard.val(), locationName: '' };
//                    serverObject.GetLocation(data, function (response) {
//                        LocationDataSource = response;
//                        PopulateLocationData(LocationDataSource);
//                    });
//                } else {
//                    ctrl_CitizenLocation.empty();
//                }
//            };
//            var Validator = {
//                CitizenLogionValidate: function () {
//                    $("#frm_ModelCitizen").validate({
//                        rules: {
//                            CitizenDistrict: { required: true },
//                            CitizenMunicipality: { required: true },
//                            CitizenWard: { required: true },
//                            CitizenLocation: { required: true },
//                            CitizenHoldingNumber: { required: true }
//                        },
//                        messages: {
//                            CitizenDistrict: {
//                                required: "Please select correct District."
//                            },
//                            CitizenMunicipality: {
//                                required: "Please select correct municipality."
//                            },
//                            CitizenWard: {
//                                required: "Please select correct ward."
//                            },
//                            CitizenLocation: {
//                                required: "Please select correct location."
//                            },
//                            CitizenHoldingNumber: {
//                                required: "Please Enter Correct Holding Number."
//                            }
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }

//                    });
//                    return $('#frm_ModelCitizen').valid();
//                }
//            };
//            var GetFormData_Citizen = function () {
//                return {
//                    MunicipalityID: ctrl_CitizenMunicipality.val(),
//                    WardID: ctrl_CitizenWard.val(),
//                    LocationID: ctrl_CitizenLocation.val(),
//                    HoldingNo: cntr_CitizenHoldingNumber.val(),
//                    cmd: $("#ModelCitizen").attr('data-cmd')
//                };
//            };
//            var OnLoginCitizen = function () {
//                if (Validator.CitizenLogionValidate() == true) {
//                    serverObject.CitizenLogin(GetFormData_Citizen(), function (response) {
//                        if (response.Status == 200 && response.Data.IsLoggedIn == true) {
//                            //redirect to authoized page
//                            $("#spn_frm_ModelCitizen").css("color", "green").html("Assessee Successfully Verified");
//                            setTimeout(function () {
//                                location.href = response.Data.RedirectUrl;
//                            }, 100);
//                        } else //if (response.Status == 401) 
//                        {
//                            $("#spn_frm_ModelCitizen").css("color", "red").html(response.Message);
//                        }

//                    });
//                }
//            };
//            (function () {
//                serverObject.District(function (response) {
//                    DistrictDataSource = response.Data;
//                    PopulateDistrictData();
//                });
//                PopulateTable();
//                BindEvent();
//            }());
//        };

//        var AdministrativeLogin = function () {
//            var ddlAdminType = $("#ddlAdminType");
//            var txtAdmUserName = $("#txtAdmUserName");
//            var txtAdmPassword = $("#txtAdmPassword");
//            var BtnAdmLogin = $("#admBtnLogin");
//            var GetFormData = function () {
//                return {
//                    ValuationBoardLogin: { AdminType: ddlAdminType.val(), UserName: txtAdmUserName.val(), Password: txtAdmPassword.val() }
//                };
//            };
//            var Validator = {
//                AddValidator: function () {
//                    $("#frm_Adminstration").validate({
//                        rules: {
//                            ddlAdminType: { required: true },
//                            txtAdmUserName: { required: true },
//                            txtAdmPassword: { required: true },
//                        },
//                        messages: {
//                            ddlAdminType: {
//                                required: "Please select Admin Type."
//                            },
//                            txtAdmUserName: {
//                                required: "Please Enter Password."
//                            },
//                            txtAdmPassword: {
//                                required: "Please Enter Password"
//                            }
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }

//                    });
//                },
//                Validate: function () {
//                    return $('#frm_Adminstration').valid();
//                }
//            };
//            var BindEvent = function () {
//                BtnAdmLogin.on('click', onClickLogin);
//            };
//            var onClickLogin = function () {
//                if (Validator.Validate()) {
//                    var payLoad = GetFormData();
//                    serverObject.AdministrativeLogin(payLoad, function (r) {
//                        if (r.Status === 200 && r.Data.IsLoggedIn == true) {
//                            $("#spn_frm_msg_Adm").css("color", "green").html("Login Successful");
//                            setTimeout(function () {
//                                location.href = r.Data.RedirectUrl;
//                            }, 100);
//                        } else if (r.Status == 401) {
//                            $("#spn_frm_msg_Adm").css("color", "red").html(r.Message);
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//            };
//            (function () {
//                Validator.AddValidator();
//                BindEvent();
//            }());
//        };

//        var MunicipalityLogin = function () {
//            var ddlMnDistrict = $("#ddlMnDistrict");
//            var txtMnUserName = $("#txtMnUserName");
//            var txtMnPassword = $("#txtMnPassword");
//            var ddlMnMunicipalityID = $("#ddlMnMunicipalityID");
//            var MunicipalitybtnLogin = $("#MunicipalitybtnLogin");


//            var GetFormData = function () {
//                return { MunicipalityID: ddlMnMunicipalityID.val(), UserName: txtMnUserName.val(), Password: txtMnPassword.val() };
//            };
//            var Validator = {
//                MnLogionValidate: function () {
//                    $("#frm_Mncpality").validate({
//                        rules: {
//                            ddlMnDistrict: { required: true },
//                            ddlMnMunicipalityID: { required: true },
//                            txtMnUserName: { required: true },
//                            txtMnPassword: { required: true }
//                        },
//                        messages: {
//                            ddlMnDistrict: {
//                                required: "Please select correct District."
//                            },
//                            ddlMnMunicipalityID: {
//                                required: "Please select correct municipality."
//                            },
//                            txtMnUserName: {
//                                required: "Please Enter Username"
//                            },
//                            txtMnPassword: {
//                                required: "Please Enter Password"
//                            }
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }

//                    });
//                    return $('#frm_Mncpality').valid();
//                }
//            };
//            var OnLoginCollector = function () {
//                if (Validator.MnLogionValidate() == true) {
//                    serverObject.CollectorLogin(GetFormData(), function (response) {
//                        if (response.Status == 200 && response.Data.IsLoggedIn == true) {
//                            //redirect to authoized page
//                            $("#spn_frm_msg_Mn").css("color", "green").html("Login Successful");
//                            setTimeout(function () {
//                                location.href = response.Data.RedirectUrl;
//                            }, 100);
//                        } else if (response.Status == 401) {
//                            $("#spn_frm_msg_Mn").css("color", "red").html(response.Message);
//                        }

//                    });
//                }
//            };
//            var BindEvent = function () {
//                $("#MunicipalitybtnLogin").on('click', OnLoginCollector);
//                $("#ddlMnDistrict").on('change', DistrictChange);
//                $("#ModelMunicipality").off("hidden.bs.modal", OnCloseModal);
//                $("#ModelMunicipality").on("hidden.bs.modal", OnCloseModal);
//            };

//            var OnCloseModal = function () {
//                resetModalForm();
//            };
//            var resetModalForm = function () {
//                ddlMnDistrict.find("option:first").attr("selected", true);
//                ddlMnMunicipalityID.find("option:first").attr("selected", true);
//                txtMnUserName.val('');
//                txtMnPassword.val('');
//            };
//            var PopulateDistrictData = function () {
//                ddlMnDistrict.empty();
//                ddlMnDistrict.append('<option value="">--select--</option>');
//                $.each(DistrictDataSource, function (index, value) {
//                    var label = value.DistrictName;
//                    var id = value.DistrictID;
//                    ddlMnDistrict.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var PopulateMunicipalityData = function (MunicipalityDataSource1) {
//                ddlMnMunicipalityID.empty();
//                ddlMnMunicipalityID.append('<option value="">--select--</option>');
//                $.each(MunicipalityDataSource1, function (index, value) {
//                    var label = value.MunicipalityName;
//                    var id = value.MunicipalityID;
//                    ddlMnMunicipalityID.append("<option value='" + id + "'>" + label + "</option>");
//                });
//            };
//            var DistrictChange = function () {
//                //  alert(ctrl_CitizenDistrict.val());
//                if (ddlMnDistrict.val() != "") {

//                    serverObject.Municipality(ddlMnDistrict.val(), function (response) {
//                        MunicipalityDataSource = response.Data;
//                        PopulateMunicipalityData(MunicipalityDataSource);
//                    });
//                } else {
//                    ddlMnMunicipalityID.empty();
//                }
//            };
//            (function () {
//                serverObject.District(function (response) {
//                    DistrictDataSource = response.Data;
//                    PopulateDistrictData();
//                });
//                BindEvent();
//            }());
//        };

//        var CitizenRegistration = function () {
//            var txtusername = $("#form_username")
//            var txtname = $("#form_name");
//            var txtdesig = $("#form_designation");
//            var txtEmail = $("#form_email");
//            var txtMobile = $("#form_mobile");
//            var txtpassword = $("#form_password");
//            var txtconfmpass = $("#form_confirm_password");
//            var txtcompanyname = $("#form_company_name");
//            var txtcompanyadd = $("#form_company_address");
//            var btnReg = $(".btn-Register");
//            var otptext = $(".form_otp");
//            var btnOtp = $(".btn-SendOTP");
//            var resendotp = $(".Btn-resendotp");
//            var resendmobileverify = $(".Btn-mobileresendverify");
//            var mobilenover = $(".form_mobile_verify");
//            var btnmobileverfy = $('.btn-verifymobile');
//            var mobileGlobal = '';

//            var GetFormData = function () {
//                return {
//                    CitizenReg: { UserName: txtusername.val(), Name: txtname.val(), Designation: txtdesig.val(), Email: txtEmail.val(), MobileNo: txtMobile.val(), Password: txtpassword.val(), CompanyName: txtcompanyname.val(), CompanyAddress: txtcompanyadd.val() }
//                };
//            };

//            var Validator = {
//                AddValidator: function () {
//                    $("#Citizen-Registration-form").validate({
//                        rules: {
//                            txtusername: { required: true },
//                            txtname: { required: true },
//                            txtdesig: { required: true },
//                            txtEmail: { required: true },
//                            txtMobile: { required: true },
//                            txtpassword: { required: true },
//                            txtconfmpass: { required: true, equalTo: '#form_password' },
//                            txtcompanyname: { required: true },
//                            txtcompanyadd: { required: true },
//                        },
//                        messages: {
//                            txtusername: {
//                                required: "Please Enter User Name."
//                            },
//                            txtname: {
//                                required: "Please Enter Aplicant Name."
//                            },
//                            txtdesig: {
//                                required: "Please Enter Designation."
//                            },
//                            txtEmail: {
//                                required: "Please Enter EmailID."
//                            },
//                            txtMobile: {
//                                required: "Please Enter Mobile No."
//                            },
//                            txtpassword: {
//                                required: "Please Enter Password."
//                            },
//                            txtconfmpass: {
//                                required: "Please Enter Confirm Password.",
//                                equalTo: "Password Doesn't Match!!"
//                            },
//                            txtcompanyname: {
//                                required: "Please Enter Company Name."
//                            },
//                            txtcompanyadd: {
//                                required: "Please Enter Company Address."
//                            }
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }

//                    });
//                    return $('#Citizen-Registration-form').valid();
//                }
//                //Validate: function () {
//                //    return $('#Citizen-Registration-form').valid();
//                //}
//            };

//            var Mobileverifyvalid = {
//                addmobvalid: function () {
//                    $("#Citizen-MobileVerfication-form").validate({
//                        rules: {
//                            mobilenover: { required: true },

//                        },
//                        messages: {
//                            mobilenover: {
//                                required: "Please Enter Mobile No."
//                            }

//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }

//                    });
//                    return $('#Citizen-MobileVerfication-form').valid();
//                }
//                //Validate: function () {
//                //    return $('#Citizen-Registration-form').valid();
//                //}
//            };

//            var BindEvent = function () {
//                btnReg.on('click', onClickReg);
//                btnOtp.on('click', onClickOtp);
//                resendotp.on('click', onclickResendotp);
//                resendmobileverify.on('click', onclickmobver);
//                btnmobileverfy.on('click', onclickotpmob);
//            };

//            var onClickReg = function () {
//                mobileGlobal = '';
//                if (Validator.AddValidator() == true && validateCaptcha()) {
//                    var CitizenReg = GetFormData();
//                    serverObject.CitizenRegistration(CitizenReg, function (r) {
//                        if (r.Status === 200) {
//                            if ((r.Data.Status === 1 && r.Data.isExist === 0 && r.Data.isVerified === 0)) {
//                                $('.modal-title').html('OTP Verfication');
//                                $('#optmsg').html(r.Data.Msg);
//                                mobileGlobal = r.Data.Mobile;
//                                $('#lblMobile').text(r.Data.Mobile);
//                                $('#Citizen-OTP-form').show();
//                                $('#Citizen-Registration-form').hide();
//                            }
//                            else {
//                                alert(r.Data.Msg);
//                            }
//                        } else if (r.Status == 401) {
//                            alert('Unsucessful');
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//            };
//            var onclickmobver = function () {
//                if (Mobileverifyvalid.addmobvalid() == true) {
//                    var Resendotpdata = { Type: "R", MobileNo: mobilenover.val() };//F->Forget Password,R->Resend Otp
//                    if (mobilenover.val().trim() != '') {
//                        serverObject.ResendOtpVerification(Resendotpdata, function (r) {
//                            if (r.Status === 200) {
//                                if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
//                                    alert('Your Otp has been Sent to your Mobile No');
//                                } else {
//                                    alert(r.Data.Msg);
//                                }
//                            } else if (r.Status == 401) {
//                                alert('Unsucessful');
//                            } else {
//                                alertify.error(r.Message);
//                            }
//                        });
//                    }
//                    else {
//                        alert('Entered mobile no is not correct');
//                    };
//                }
//            };
//            //
//            var onclickResendotp = function () {
//                var Resendotpdata = { Type: "R", MobileNo: mobileGlobal };//F->Forget Password,R->Resend Otp
//                if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal) {
//                    serverObject.ResendOtpVerification(Resendotpdata, function (r) {
//                        if (r.Status === 200) {
//                            if ((r.Data.Status === 1 && r.Data.isVerified === 0)) {
//                                alert('Your Otp has been Sent to your Mobile No');
//                            } else {
//                                alert(r.Data.Msg);
//                            }
//                        } else if (r.Status == 401) {
//                            alert('Unsucessful');
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//                else {
//                    alert('Entered mobile no is not correct');
//                };
//            };
//            //
//            var onClickOtp = function () {
//                var otpdata = { Otpdata: otptext.val().trim(), MobileNo: mobileGlobal };
//                if ($('#lblMobile').text().trim() != '' && $('#lblMobile').text().trim() === mobileGlobal && otptext.val().trim() != '') {
//                    serverObject.otpverification(otpdata, function (r) {
//                        if (r.Status === 200) {
//                            if ((r.Data.Status === 1)) {
//                                $('.modal-title').html('Login');
//                                $('#Citizen-OTP-form').hide();
//                                $('#Citizen-Login-form').show();
//                                $('#Citizen-Registration-form').hide();
//                            } else {
//                                alert(r.Data.Msg);
//                            }

//                        } else if (r.Status == 401) {

//                            alert('Unsucessful');
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//                else {
//                    alert('Entered mobile no is not correct');
//                };
//            };

//            var onclickotpmob = function () {
//                var otpdata = { Otpdata: $('.form_otp_ver').val().trim(), MobileNo: $('.form_mobile_verify').val().trim() };
//                if ($('.form_otp_ver').val().trim() != '' && $('.form_mobile_verify').val().trim() != '') {
//                    serverObject.otpverification(otpdata, function (r) {
//                        if (r.Status === 200) {
//                            if ((r.Data.Status === 1)) {
//                                $('.modal-title').html('Login');
//                                $('#Citizen-OTP-form').hide();
//                                $('#Citizen-MobileVerfication-form').hide();
//                                $('#Citizen-Login-form').show();
//                                $('#Citizen-Registration-form').hide();
//                            } else {
//                                alert(r.Data.Msg);
//                            }

//                        } else if (r.Status == 401) {

//                            alert('Unsucessful');
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//                else {
//                    alert('Entered mobile no is not correct');
//                };
//            };


//            (function () {
//                Validator.AddValidator();
//                BindEvent();
//            }());
//        };

//        var OtherCitizenLogin = function () {
//            var txtothUserName = $("#form_user_name");
//            var txtothPassword = $("#form_password_login");
//            var BtnOtherLogin = $(".btn-Login");
//            var GetFormData = function () {
//                return {
//                    userLogin: { UserName: txtothUserName.val(), Password: txtothPassword.val() }
//                };
//            };
//            var Validator = {
//                AddValidator: function () {
//                    $("#Citizen-Login-form").validate({
//                        rules: {

//                            txtothUserName: { required: true },
//                            txtothPassword: { required: true },
//                        },
//                        messages: {

//                            txtothUserName: {
//                                required: "Please Enter Password."
//                            },
//                            txtothPassword: {
//                                required: "Please Enter Password"
//                            }
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//                            error.css('color', 'red');
//                        }
//                    });
//                },
//                Validate: function () {
//                    return $('#Citizen-Login-form').valid();
//                }
//            };
//            var BindEvent = function () {
//                BtnOtherLogin.on('click', onClickLogin);
//            };

//            var onClickLogin = function () {
//                if (Validator.Validate()) {
//                    var payLoad = GetFormData();
//                    serverObject.OtherCitizenLogin(payLoad, function (r) {
//                        if (r.Status === 200 && r.Data.IsLoggedIn == true) {
//                            $("#spn_frm_msg_otherciti").css("color", "green").html("Login Successful");
//                            setTimeout(function () {
//                                location.href = r.Data.RedirectUrl;
//                            }, 100);
//                        } else if (r.Status == 401) {
//                            $("#spn_frm_msg_otherciti").css("color", "red").html(r.Message);
//                        } else {
//                            alertify.error(r.Message);
//                        }
//                    });
//                }
//            };

//            (function () {
//                Validator.AddValidator();
//                BindEvent();
//            }());
//        };

//        this.app_Initiate = function () {
//            CitizenLogin();
//            MunicipalityLogin();
//            AdministrativeLogin();
//            CitizenRegistration();
//            OtherCitizenLogin();
//        };
//    };

//    var INIT = function () {
//        var serverObject = new SERVER_OBJECT();
//        var appObject = new APP_OBJECT(serverObject);
//        appObject.app_Initiate();
//    };

//    (function () {
//        INIT();

//    }());


//});


