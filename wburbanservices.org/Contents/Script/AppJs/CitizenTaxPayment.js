﻿$(document).ready(function () {
    var PaymentProcessor = new PAYMENT_PROCESSOR();
    var PaymentGrid = new PAYMENT_GRID();

    var SERVER_OBJECT = function () {
        this.GetPropertyTaxDetails = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/PropertyTaxPayment/Get_PropertyTaxPaymentDetails",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callback(data.Data);

                    } else {
                        alert("Error on loding Property Tax Details");
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };
        this.GetpayheadByMncpltyID = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/PayheadMaster/Get_PayheadBy_MunicipalityID",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        callback(rowDatas);
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.SavePaymentDetails = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/CitizenTaxPayment/Temp_SaveTaxPaymentDetails",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                   
                        
                    callback(response);
                   
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var TotalPayable = 0;
        var OutStandindSearchDetails = [];
       var ctrl_hdnMunicipalityID = $("#hdnMunicipalityID");
       var ctrl_hdnWardID = $("#hdnWardID");
       var ctrl_hdnLocationID = $("#hdnLocationID");
       var ctrl_hdnAssesseeID = $("#hdnAssesseeID");
       var ctrl_btnPay = $("#btnPay");
       var ctnr_gridContainer = $("#gridContainer");
       var GetPtaxSearchableFormData = function () {
            return { MunicipalityID: ctrl_hdnMunicipalityID.val(), WardID: ctrl_hdnWardID.val(), LocationID: ctrl_hdnLocationID.val(), AssesseeID: ctrl_hdnAssesseeID.val() };
       };
       var ValidateFormData = function (formData) {
           if (formData.MunicipalityID || formData.WardID || formData.LocationID || formData.AssesseeID) {
               return true;
           }
           return false;
       };
       var SearchPaymentDetails = function () {
           var data = GetPtaxSearchableFormData();
           if (ValidateFormData(data) == true) {
          
               serverObject.GetPropertyTaxDetails(data, function (response) {
                   var rowDatas = response;
                   OutStandindSearchDetails = rowDatas;
                   CalculationStart(rowDatas.length > 0 ? rowDatas[0].MunicipalityID : 0);
               });
           }
       };
       var CalculationStart = function (MunicipalityID) {
           var collectionDateString = $("#txtCollectionDate").val();
           var paidAmountString = $("#txtPaidAmnt").val();
           var collectionDate = new Date();
           if (CONVERTER.Date.isDateString(collectionDateString) == true) {
               var dtParts = (collectionDateString).split("-");
               collectionDate = CONVERTER.Date.convertToDate(dtParts[0], dtParts[1], dtParts[2]);
           }
           serverObject.GetpayheadByMncpltyID({ MunicipalityID: MunicipalityID, CollectionDate: collectionDate }, function (payheadResponse) {
               if (payheadResponse.length <= 0) {
                   alert("Error: Payheads are not found. System can not initiate");
                   return;
               }
               PaymentProcessor.Set_PaymentMode("C");
               PaymentProcessor.Set_PaymentType("F");


               PaymentProcessor.Set_PaymentCollectionDate(collectionDate);
               PaymentProcessor.Set_PaymentHead(payheadResponse);
               PaymentProcessor.DataBind(OutStandindSearchDetails);
               PaymentProcessor.CalculateByPayableCheckState();
           });
       };
       var BindEvents = function () {
           ctrl_btnPay.on('click', OnPay);
          // $(".chkIsPayable").off("change");
           $(document).on("change", ".chkIsPayable", function (event) {
               try{
                   var qtrNo = Number($(this).attr("data-qtrno"));
                   PaymentProcessor.CheckUncheckQtrPaybleDemand(qtrNo,$(this).is(":checked"));
                   PaymentProcessor.CalculateByPayableCheckState();
               }catch(e){
                       $(this).prop("checked", !$(this).is(":checked"));
               }
           });
           EventBus.subscribe(PaymentProcessor.eventPaths.OnCompleteCalculation, function (response) {
               ctnr_gridContainer.empty();
               PaymentGrid.DataBind(response, ctnr_gridContainer);
               //console.log(response);
           });
           EventBus.subscribe(PaymentProcessor.eventPaths.OnCompleteCalculation, function (response) {
               var totalPaid = 0;
               var totOutStanding = 0;
               $.each(response.PropertyTaxPaymentDetails, function (index, value) {
                   totalPaid += (value.PaidTaxAmt + value.PaidCalculatedAmt + value.PaidSurchargeAmt);
                   totOutStanding += (value.TaxValue + value.SurchargeValue + value.CalculatedValue);
               });
               if (totalPaid > 0) {
                   ctrl_btnPay.attr('disabled', false);
               } else {
                   ctrl_btnPay.attr('disabled', true);
               }
               $("#spnTotOutStanding").html(totOutStanding);
               
               
           });
       };
       var OnPay = function () {
           if (confirm("Are you sure to pay?") == true) {
               serverObject.SavePaymentDetails({ PropertyTaxPaymentMaster: PaymentProcessor.wellKnownPaymentObject }, function (response) {
                   if (response.Status == 200 && response.Data==true) {
                       location.href = "/CitizenTaxPayment/BildeskPage";
                   } else {
                       alert("Error: " + response.Messages);
                   }

               });
           }
       };
       this.app_Initiate = function () {
           BindEvents();
           SearchPaymentDetails();
       };
      
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());

});