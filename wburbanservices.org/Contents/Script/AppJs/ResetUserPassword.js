﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.Get_AllUsersByMunicipalityID = function (callback) {
            $.ajax({
                type: "GET",
                url: "/Municipality/CurSession/GetActiveUsers",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                   
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.ResetPassword = function (UserID,callback) {
            $.ajax({
                type:'post',
                contentType:'application/json;charset=utf-8',
                data: "{UserID:" + UserID + "}",
                dataType:'json',
                url: "/Municipality/MnUserPassword/ResetPassword",
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    alert('error occur');
                }
            });
        }
    };

    var APP_OBJECT = function (serverObject) {
        var Bind_GridOnPageLoad = function () {
            $("#tblDetail").find("tbody").empty();
            var getRowString = function (rowData) {
                var LockedStatus = rowData.IsAccountLocked == true ? 'Locked' : 'Un-Locked';
                var rowstr = "<tr align='center'>";
                rowstr += "<td class='UserID' style='text-align:center;display:none;'>" + rowData.UserID + "</td>";
                rowstr += "<td class='MunicipalityID' style='display:none;'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td class='UserType' style='display:none;'>" + rowData.UserType + "</td>";
                rowstr += "<td>" + rowData.UserName+"</td>";
                rowstr += "<td class='Name' >" + rowData.FirstName + ' ' + rowData.LastName +"</td>";
                rowstr += "<td class='PhoneNo'>" + rowData.PhoneNo + "</td>";
                rowstr += "<td class='Email' >" + rowData.Email + "</td>";
                rowstr += "<td class='IsAccountLocked'>" + LockedStatus + "</td>";                
                rowstr += "<td style='text-align:center;cursor:pointer;'><div class='clsResetPassword' style='cursor:pointer;font-weight:bold'>Re-set Password</div></td>";              
                rowstr += "</tr>";
                return rowstr;
            };
            var RegisterEvent = function () {
                $(document).off('click', '.clsResetPassword', onResetpassword);
                $(document).on('click', '.clsResetPassword', onResetpassword);
            };
            var onResetpassword = function () {
                var ctrl_ID=$(this);
                var UserID = ctrl_ID.closest('tr').find('.UserID').text();
                if (confirm("Aru you sure want to reset the password ??")) {
                    serverObject.ResetPassword(UserID, function (response) {
                        if (response.Status == 200) {
                            ctrl_ID.closest('tr').find('.clsResetPassword').text("");
                            ctrl_ID.closest('tr').find('.clsResetPassword').text(response.Data);
                            ctrl_ID.closest('tr').find('.clsResetPassword').prop('disabled', true);
                        }
                    });
                }
            }
            var appendToBody = function (rowString) {
                $("#tblDetail").find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.Get_AllUsersByMunicipalityID(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        };
        this.app_Initiate = function () {          
            Bind_GridOnPageLoad();           
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});


