﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.WardPopulate = function (data,callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnWards/GetWards",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        };

        this.LocationPopulate = function (data,callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnLocations/GetLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    alert("Error");
                }
            });
        };

        this.AssesseePopulate = function (data, callback) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Municipality/MnAssessee/GetAssessees",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };

        this.PopulateReason=function(data,callback){
            $.ajax({
                type:"POST",
                url:"/Municipality/PaymentCancellation/GetCancelationReasonList",
                data:JSON.stringify(data),
                contentType:"application/json; charset=utf-8",
                dataType:"json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.UpdateCancelation = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/PaymentCancellation/CancelOfflinePayment",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.BindGrid = function (data, callback) {
            $.ajax({
                type: "POST",
                url: "/Municipality/PaymentCancellation/SearchOfflinePayments",
                data:JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };

        this.PupulateInstrumentDtls=function(data,callback){
            $.ajax({
                type: "POST",
                url: "/Municipality/PaymentCancellation/SearchInstrumentDtls",
                data:JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert("Error");
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
        var BindEvent = function () {
            $("#btnSearchByAssessee").on('click', Search);
            $("#btnSubmit").on('click', Update);
            DomControls.ctrl_txtWard.autocomplete(AutocompleteSorces.Wards());
            DomControls.ctrl_txtLocation.autocomplete(AutocompleteSorces.Locations());
            DomControls.ctrl_txtAssessee.autocomplete(AutocompleteSorces.Assessee());
            //$(document).on('click', '.show-instrument-details', ShowInstrumentDetails);
            $("#tblResult tbody").on('click', 'span[id="viewID"]', ShowInstrumentDetails);
            $('#DateTo').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateFrom').datepicker({
                dateFormat: 'dd/mm/yy',
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'medium'
            });
            $('#DateTo,#DateFrom').blur(function () {
                if ($(this).val().trim() == '') {
                    return false;
                }
            });
        };
        var ShowInstrumentDetails = function () {
            $("#tblInstrument").find("tbody").empty();
            serverObject.PupulateInstrumentDtls(BuildInstrumentData($(this)), function (respons) {
                if (respons.Status==200 && (respons.Data).length > 0) {
                    var str = "";
                    $.each(respons.Data,function(idx,row){
                        str = "<tr>";
                        str += "<td>" + row.PaymentModeDescription + "</td>";
                        str += "<td>" + row.InstrumentAmount + "</td>";
                        str += "<td>" + row.InstrumentNo + "</td>";
                        str += "<td>" + row.InstrumentDate + "</td>";
                        str += "<td>" + row.BankName + "</td>";
                        str += "</tr>";
                        $("#tblInstrument").find("tbody").append(str);
                    });
                }
            });
        };
        var BuildInstrumentData = function (e) {
            var row = $(e).closest('tr');
            var paymentID = $(row).find('td:eq(6)').find(".radio-Select-PaymentID").val();
            var assesseeID = ($("#tblResult tbody").has('.radio-Select-PaymentID').find(".selected-assessee-id").val());
            var data = { paymentID: paymentID, assesseeID: assesseeID };
            return data;
        };
        var ValidatePaymentCancellation = function (data) {
            if (data.PaymentID == null) {
                alert("Select Payment");
                return false;
            }
            if (data.AssesseeID == null) {
                alert("Select Payment");
                return false;
            }
            if (data.CancellationReasons.length == 0) {
                alert("Select atleast one Reasons");
                return false;
            }
            if (data.RemarksText.trim() == "") {
                alert("Enter Remark");
                return false;
            }
            return true;
        };
        var BuildCancellationData = function () {
            var GetCancellationReasons = function () {
                var reasons = [];
                $.each($(".cancelation-reason-list").find("table tbody").find(".select-reason:checked"), function (idx, item) {
                    reasons.push(
                    { ReasonID:$(item).val(),
                      ReasonText: null
                    });
                });
                return reasons;
            };
            var SelectedPaymentID = ($("#tblResult tbody").find(".radio-Select-PaymentID:checked").length > 0 ? $("#tblResult tbody").find(".radio-Select-PaymentID:checked").val() : null);
            var AssesseeID = ($("#tblResult tbody tr").has(".radio-Select-PaymentID:checked").find(".selected-assessee-id").length > 0 ? $("#tblResult tbody tr").has(".radio-Select-PaymentID:checked").find(".selected-assessee-id").val() : null);
            var RemarksText = $("#txtRemark").val();
            var Canceldata = { PaymentID: SelectedPaymentID, AssesseeID: AssesseeID, CancellationReasons: GetCancellationReasons(), RemarksText: RemarksText };
            return Canceldata;
        };
        var Update = function (e) {
            e.preventDefault();
            var data = BuildCancellationData();
            if (ValidatePaymentCancellation(data) && confirm("Are you sure to cancel this payment...?")) {
                serverObject.UpdateCancelation(data, function (respons) {
                    alert(respons.Message);
                    if (respons.Status == 200) {
                        //$(".cancelation-reason-list").find("table tbody").find(".select-reason").prop('checked', false);
                        //$("#txtRemark").val('');
                        //if (!confirm("Do you want to cancel another payment aginst this Assessee......?")) {
                            window.location.href = "/municipality/PaymentCancellation/PaymentCancellationUI";
                        //} else {
                        //    $("#btnSearchByAssessee").trigger('click');
                        //}
                    }
                });
            }
        };
        var Search = function (e) {
            e.preventDefault();
            var formData = GetDataFrmView();
            if (Validate(formData)) {
                $("#tblResult").find("tbody").empty();
                serverObject.BindGrid(BuildData(formData), function (response) {
                    if (response.Status == 200 && (response.Data).length > 0) {

                        var str = "";
                        $.each(response.Data, function (idx, row) {
                            str = "<tr>";
                            str += "<td>" + (idx + 1) + "</td>";
                            str += "<td>" + row.AssesseeName + "</td>";
                            str += "<td>" + row.RecieptID + "</td>";
                            str += "<td>" + CONVERTER.Date.convertCsToJsDate(row.PaymentReceiveDate).format('dd/mm/yyyy') + "</td>";
                            str += "<td>" + row.NetAmount + "</td>";
                            str += "<td><a style='cursor:pointer; text-decoration:underline;'class='show-instrument-details' data-backdrop='static' data-toggle='modal' href='#ModelViewInstrument'><span id='viewID'>View</span></a></td>";
                            str += "<td><input type='radio' value='" + row.PaymentID + "' class='radio-Select-PaymentID' Name='rdbSelect'><input type='hidden' class='selected-assessee-id' value='" + row.AssesseeID + "'></td>";
                            str += "</tr>";
                            $("#tblResult").find("tbody").append(str);
                        });
                    }
                });
            }
        };
        var BuildData = function (data) {
            var dateFrom = CONVERTER.Date.convertToDate(data.DateFrom.split('-')[0], data.DateFrom.split('-')[1], data.DateFrom.split('-')[2]);
            var dateTo = CONVERTER.Date.convertToDate(data.DateTo.split('-')[0], data.DateTo.split('-')[1], data.DateTo.split('-')[2]);
            return { CollectionDtFrom: dateFrom, CollectionDtTo: dateTo, WardID: data.WardID, LocationID: data.LocationID, AssesseeID: data.AssesseeID };
        };
        var Validate = function (data) {
            if (data.DateFrom == "") {
                alert("Enter a From Date");
                $('#DateFrom').focus();
                return false;
            }
            if (data.DateTo == "") {
                alert("Enter a To Date");
                $('#DateTo').focus();
                return false;
            }
            to = data.DateTo.split('-')[1] + '-' + data.DateTo.split('-')[2] + '-' + data.DateTo.split('-')[0];
            from = data.DateFrom.split('-')[1] + '-' + data.DateFrom.split('-')[2] + '-' + data.DateFrom.split('-')[0];
            if (new Date(from) > new Date(to)) {
                alert('From Date should not greater then To Date !!');
                $('#DateFrom').focus();
                return false;
            }
            if (data.WardID == "") {
                alert("Enter Ward Name");
                $("txtWard").focus();
                return false;
            }
            if (data.LocationID == "") {
                alert("Enter Location Name");
                $("txtLocation").focus();
                return false;
            }
            if (data.AssesseeID == "") {
                alert("Enter Assessee Name");
                $("txtAssessee").focus();
                return false;
            }
            return true;
        };
        var GetDataFrmView = function () {
            var DateFrom = $("#DateFrom").val().trim();
            var DateTo = $("#DateTo").val().trim();
            var WardName = $("#txtWard").val().trim();
            var WardID = $("#hdnWard").val().trim();
            var LocationName = $("#txtLocation").val().trim();
            var LocationID = $("#hdnLocation").val().trim();
            var AssesseeName = $("#txtAssessee").val().trim();
            var AssesseeID = $("#hdnAssessee").val().trim();
            if (DateFrom != "") {
                DateFrom = DateFrom.split('/')[2] + '-' + DateFrom.split('/')[1] + '-' + DateFrom.split('/')[0];
            }
            if (DateTo != "") {
                DateTo = DateTo.split('/')[2] + '-' + DateTo.split('/')[1] + '-' + DateTo.split('/')[0];
            }
            var data = { DateFrom: DateFrom, DateTo: DateTo, WardID: WardID, LocationID: LocationID, AssesseeID: AssesseeID };
            return data;
        };
        var ChkReason = function () {
            $(".cancelation-reason-list").find("table tbody").empty();
            serverObject.PopulateReason(null, function (respons) {
                if (respons.Status == 200 && (respons.Data).length > 0) {
                    $.each(respons.Data, function (inx, item) {
                        var str = "<tr>";
                        str += "<td><input type='checkbox' class='select-reason' value='" + item.ReasonID + "'/></td><td><label>&nbsp;&nbsp;" + item.ReasonText + "</label></td>";
                        str += "</tr>";
                        $(".cancelation-reason-list").find("table tbody").append(str);
                    });
                }
            });
        };
        var Resetors = {
            ResetWard: function () {
                DomControls.ctrl_hdnWard.val('');
                DomControls.ctrl_txtWard.val('');
            },
            ResetLocation: function () {
                DomControls.ctrl_hdnLocation.val('');
                DomControls.ctrl_txtLocation.val('');
            },
            ResetAssessee: function () {
                DomControls.ctrl_hdnAssessee.val('');
                DomControls.ctrl_txtAssessee.val('');
            }
        };
        var DomControls = {
            ctrl_hdnWard: $("#hdnWard"),
            ctrl_txtWard: $("#txtWard"),

            ctrl_hdnLocation: $("#hdnLocation"),
            ctrl_txtLocation: $("#txtLocation"),

            ctrl_hdnAssessee: $("#hdnAssessee"),
            ctrl_txtAssessee: $("#txtAssessee"),
        };
        var AutocompleteSorces = {
            Wards: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnWard.val('');
                        Resetors.ResetLocation();
                        Resetors.ResetAssessee();
                        //reset area
                        var data = { wardName: $.trim(request.term) };
                        serverObject.WardPopulate(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.Status == 200 && (serverResponse.Data).length > 0) {
                                $.each(serverResponse.Data, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.WardName,
                                        WardID: item.WardID
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        $("#hdnWard").val(i.item.WardID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetLocation();
                            Resetors.ResetAssessee();
                        }
                    }
                };
            },
            Locations: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnLocation.val('');
                        Resetors.ResetAssessee();
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var data = { wardID: wardID, locationName: $.trim(request.term) };
                        serverObject.LocationPopulate(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.Status == 200 && (serverResponse.Data).length > 0) {
                                
                                $.each(serverResponse.Data, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.LocationName,
                                        LocationID: item.LocationID
                                    });
                                });
                                
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnLocation.val(i.item.LocationID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                            Resetors.ResetAssessee();
                        }
                    }
                };
            },
            Assessee: function () {
                return {
                    source: function (request, response) {
                        //reset area
                        DomControls.ctrl_hdnAssessee.val('');
                        //reset area
                        var wardID = DomControls.ctrl_hdnWard.val();
                        var locationID = DomControls.ctrl_hdnLocation.val();
                        var data = { wardID: wardID, locationID: locationID, assesseeName: $.trim(request.term) };
                        serverObject.AssesseePopulate(data, function (serverResponse) {
                            var userDataAutoComplete = [];
                            if (serverResponse.Status == 200 && (serverResponse.Data).length > 0) {
                                $.each(serverResponse.Data, function (index, item) {
                                    userDataAutoComplete.push({
                                        label: item.AssesseeName + '  < ' + item.HoldingNo + ' >',
                                        AssesseeID: item.AssesseeID
                                    });
                                });
                            }
                            response(userDataAutoComplete);
                        });
                    },
                    select: function (e, i) {
                        DomControls.ctrl_hdnAssessee.val(i.item.AssesseeID);
                    },
                    change: function (e, i) {
                        if (!i.item) {
                        }
                    }
                };
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            ChkReason();
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    })();
});