﻿/// <reference path="../angular.intellisense.js" />

var myApp = angular.module('myApp', []);
//---------------------------------factories-------------------------------
myApp.factory('ModuleFactoryService', function ($http, $q) {
    var serviceObject = {};
    serviceObject.GetDetails = function (jsonParam) {
        return $q.all([//implement promise that will notify autometicaly after service call
          $http({
              url: 'GetArrearDetails',
              method: "POST",
              data: jsonParam
          })])
    };
    serviceObject.FindHoldingNo = function (jsonParam) {
        return $q.all([//implement promise that will notify autometicaly after service call
         $http({
             url: '../AppService/GetHolding',
             method: "POST",
             data: jsonParam
         })])
    };
    serviceObject.UpdateArrearDetails = function (jsonParam) {
        return $q.all([//implement promise that will notify autometicaly after service call
        $http({
            url: 'UpdateArrearDetails',
            method: "POST",
            data: jsonParam
        })])
    }
    return serviceObject;
});
//-----------------------Controller--------------------
myApp.controller('myController', ['$scope', '$http', 'ModuleFactoryService', function ($scope, $http, ModuleFactoryService) {
    //----------------start model declaration------------------
    $scope.SearchableObject = {
        MunicipalityID: 0,
        WardID: 0,
        LocationID: 0,
        HoldingNo: '*'
    };
    $scope.ConvertToDate = function (JsonDate) {
        if (JsonDate) {
            var s = new Date(parseInt(JsonDate.substr(6)));
            return $.datepicker.formatDate('mm/dd/yy', s);
        }
        return null;
    };
    $scope.Details = [];
    //----------------end model declaration-------------------
    $scope.$watch("SearchableObject", function (newVal, oldVal) {
        var data = {
            'MunicipalityID': newVal.MunicipalityID,
            'WardID': newVal.WardID, 'LocationID': newVal.LocationID, 'HoldingNo': newVal.HoldingNo
        };
        ModuleFactoryService.GetDetails(data).then(function (response) {
            if (response.length > 0) {
                $scope.Details = response[0].data.Data;
            }
        });
    }, true)
    $scope.UpdateArrear = function (rowObject) {
                var requestedObject = {
                    'ArrearID': rowObject.ArrearID, 'ArrearInterest': rowObject.ArrearInterest, 'ArrearPropertyTax': rowObject.ArrearPropertyTax, 'ArrearSurcharge': rowObject.ArrearSurcharge
                };

                ModuleFactoryService.UpdateArrearDetails(requestedObject).then(function (response) {
                    if (response.length > 0) {
                        var data = response[0].data;
                        if (data.Status === 1) {
                            alertify.success(data.Message);
                        } else {
                            alertify.error(data.Message);
                        }
                    }
                }, function (response) {
                    alertify.error(response.statusText+'.['+response.status+']');
                });
            
       // });
        
    };
    $scope.AutocompleteMunicipality = new function () {
        var Self = this;
        this.SearchByStr = '';
        this.List = [];
        this.HideAutoComlete = true;
        this.Hide = function () {
            setTimeout(function () {
                Self.HideAutoComlete = true;
            }, 10);
        };
        this.SetValue = function (obj) {
            Self.SearchByStr = obj.MunicipalityName;
            $scope.SearchableObject.MunicipalityID = obj.MunicipalityID;
        };
        this.Query = function (Name) {
            //return $http({ url: '../AppService/GetMunicipality', method: 'Post', data: { Name: Name } })
            //     .then(function (data) {
            //         Self.MunicipalityList = data.data.Data;
            //         return Self.MunicipalityList;
            //     }, function (response) {
            //         console.log(response);
            //     });
            $.ajax({
                type: "POST",
                url: '../AppService/GetMunicipality',
                data: JSON.stringify({ Name: Name }),
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Self.HideAutoComlete = false;
                    Self.List = response.Data;
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    };
    $scope.AutocompleteWard = new function () {
        var Self = this;
        this.SearchByStr = '';
        this.List = [];
        this.HideAutoComlete = true;
        this.Hide = function () {
            setTimeout(function () {
                Self.HideAutoComlete = true;
            }, 5);
        };
        this.SetValue = function (obj) {
            Self.SearchByStr = obj.WardName;
            $scope.SearchableObject.WardID = obj.WardID;
        };
        this.Query = function (Name, MunicipalityID) {
            $.ajax({
                type: "POST",
                url: '../AppService/GetWard',
                data: JSON.stringify({ wardName: Name, MunicipalityID: MunicipalityID }),
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Self.HideAutoComlete = false;
                    Self.List = response.Data;
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    };
    $scope.AutocompleteLocation = new function () {
        var Self = this;
        this.SearchByStr = '';
        this.List = [];
        this.HideAutoComlete = true;
        this.Hide = function () {
            setTimeout(function () {
                Self.HideAutoComlete = true;
            }, 10);
        };
        this.SetValue = function (obj) {
            Self.SearchByStr = obj.LocationName;
            $scope.SearchableObject.LocationID = obj.LocationID;
        };
        this.Query = function (Name, MunicipalityID, wardId) {
            $.ajax({
                type: "POST",
                url: '../AppService/GetLocation',
                data: JSON.stringify({ locationName: Name, MunicipalityID: MunicipalityID, wardId: wardId }),
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Self.HideAutoComlete = false;
                    Self.List = response.Data;
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    };
    $scope.Holdings = new function () {
        var Self = this;
        this.SearchByStr = '';
        this.List = [];
        this.HideAutoComlete = true;
        this.Hide = function () {
            setTimeout(function () {
                Self.HideAutoComlete = true;
            }, 10);
        };
        this.SetValue = function (obj) {
            Self.SearchByStr = obj.HoldingNo;
            $scope.SearchableObject.HoldingNo = obj.HoldingNo;
        };
        this.Query = function (HoldingNo, MunicipalityID, wardId, LocationID) {
            var data = { 'HoldingNo': HoldingNo, 'MunicipalityID': MunicipalityID, 'WardID': wardId, 'LocationID': LocationID };
            ModuleFactoryService.FindHoldingNo(data).then(function (response) {
                if (response.length > 0) {
                    Self.HideAutoComlete = false;
                    Self.List = response[0].data.Data;
                }
            });
        }
    };
}]);


