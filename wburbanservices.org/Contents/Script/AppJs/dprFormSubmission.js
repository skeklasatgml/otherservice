﻿$(document).ready(function () {
    var VettedDoc = null;
    var AbsPgDoc = null;
    var Server = function () {
        this.submitForm = function (obj, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SubmitForm',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
        this.saveAsDraft = function (obj, callback) {
            $.ajax({
                type: 'post',
                url: '/Modules/dpr/SaveDprAsDraft',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    callback({ Status: xhr.status, Message: xhr.responseText, Data: null });
                }
            });
        };
    };
    var App = function (srv) {
        var doms = function () {
            return {
                hdn_municipalityId: $(".hdn-dev-auth-id"),
                hdn_IdentityStamp: $(".hdn-IdentityStamp"),
                txt_devAuth: $(".txt-dev-auth"),
                txt_workName: $(".txt-work-name"),
                txt_exeSummary: $(".txt-exe-summary"),
                txt_implAgency: $(".txt-impl-agency"),
                txt_estCost: $(".txt-estimated-cost"),
                hdn_schemeTypes: $(".hdn-SchemeTypes"),
                hdn_SchemeCategories: $(".hdn-SchemeCategories"),
                ddl_SchemeType: $(".ddl-scheme-types"),
                ddl_schemeCategory: $(".ddl-scheme-category"),
                txt_email: $('.txt-email'),
                btn_addYear: $(".btn-add-year"),
                txt_phasingFinYear: $(".txt-phasing-fin-year"),
                ddl_addYear: $(".ddl-add-year"),
                chkLstPar1: $(".tbl-part1-check-list tbody tr").map(function (ind, val) {
                    return {
                        Id: $(val).attr("data-Id"),
                        chk: $(val).find('.ddl-chk-part1'),
                        txtPageno: $(val).find('.txt-pageno'),
                        txtRemark: $(val).find('.txt-remarks'),
                        Perticular: $(val).find('.lbl-Perticular').text()
                    }
                }),
                txt_engName: $(".txt-engineer-name"),
                txt_designation: $(".txt-designation"),
                txt_ceoName: $(".txt-name-ceo"),
                txt_ceoEmail: $(".txt-email-ceo"),
                txt_ceoMob: $(".txt-mobile-ceo"),
                txt_contactNumber: $(".txt-contact-number"),
                chkLstPar2: $(".tbl-part2-check-list tbody tr").map(function (ind, val) {
                    return {
                        Id: $(val).attr("data-Id"),
                        chk: $(val).find('.ddl-chk-part2'),
                        txtPageno: $(val).find('.txt-pageno'),
                        txtRemark: $(val).find('.txt-remarks'),
                        Perticular: $(val).find('.lbl-Perticular').text()
                    }
                }),
                txt_MemoDate: $(".txt-memoDate"),
                txt_CommencementDate: $(".txt-commencementDate"),
                txt_CompletionDate: $(".txt-completionDate"),
                txt_memoNumber: $(".txt-memo-no"),
                btn_submitForm: $(".btn-form-submit"),
                //txt_SchemeDate:$(".txt-SchemeDate"),
                btn_resetForm: $(".btn-reset"),
                btn_saveAsDraft: $(".btn-save-as-draft"),
                tbl_phasingExpenditure: $(".tbl-phasing-expenditure"),
                tbl_subPorjects: $(".tbl-sub-projects")
            };
        }
        var utils = {
            validateExpenditure: function () {
                try {
                    var FrmData = getFormData();
                    var estimatedCost = FrmData.EstimatedCost;
                    var phasingExpanditures = FrmData.PhasingOfExpenditures;
                    if (estimatedCost > 0 && phasingExpanditures.length > 0) {
                        var totalPhasingExpenditure = phasingExpanditures.reduce(function (a, b) { return a + Number(isNaN(b.ExpenditureAmount) ? 0 : b.ExpenditureAmount); }, 0);
                        if (totalPhasingExpenditure != estimatedCost) {
                            throw 'Total Of Phasing expenditures and Estimated cost should be Equal'
                        }
                    } else {
                        throw 'Phasing Of Expenditure Requied';
                    }
                    return { r: true };
                } catch (e) {
                    return { r: false, msg: e };
                }
            },
        };
        var tblExpenditure = new function () {
            var reCalculateExpentureSummary = function () {
                doms().tbl_phasingExpenditure.find('tfoot').empty();
                if (doms().tbl_phasingExpenditure.find('tbody tr').length == 0) {
                    return;
                }
                var tr = $('<tr/>');
                var totalExpendature = getFormData().PhasingOfExpenditures.reduce(function (a, b) { return a + Number(isNaN(b.ExpenditureAmount) ? 0 : b.ExpenditureAmount); }, 0);
                tr.append('<td><label>Total Project Cost</label></td><td class="text-right"><lable>' + totalExpendature + '</label><td/>')
                doms().tbl_phasingExpenditure.find('tfoot').append(tr);
            };
            var _onClickAddYear= function (e) {
                e.preventDefault();
                var validateFinYear = function (finYear) {
                    var errorMsg = 'Invalid Fin-Year: ' + finYear;
                    var p = /\b\d{4}-\d{4}\b/; //dddd-dddd format
                    if (!p.test(finYear))
                        throw errorMsg;

                    var arr = finYear.split('-');
                    if (Number(arr[0]) >= Number(arr[1])) {
                        throw errorMsg;
                    }
                    if (Number(arr[0]) + 1 !== Number(arr[1])) {
                        throw errorMsg;
                    }
                }
                var addYearToFinYear = function (finyear, addBy) {
                    validateFinYear(finyear);
                    var arr = finyear.split('-');
                    return (Number(arr[0]) + Number(addBy)) + '-' + (Number(arr[1]) + Number(addBy));
                }

                var curFinYear = doms().txt_phasingFinYear.val();
                try {
                    var addBy = doms().ddl_addYear.val();
                    var afterAddFinYear = addYearToFinYear(curFinYear, addBy);
                    doms().tbl_phasingExpenditure.find('tbody').append(
                        "<tr><td>" +
                        "<label>" + afterAddFinYear + "</label><input type='hidden' class='hdn-phasing-finyear' value='" + afterAddFinYear + "'/>" +
                        "</td><td>" +
                        "<input type='text' class='form-control txt-prop-expenture-amnt' name='txt-prop-expenture-amnt' placeholder='enter amount in Rs.'/>" +
                        "</td><td><a href='javascript:void(0);' class='del-phasing-row'><span class='glyphicon glyphicon-trash'></span></a></td>" +
                        "</tr>"
                        );
                    reCalculateExpentureSummary();
                    doms().tbl_phasingExpenditure.find('tbody tr.no-row').remove();
                    doms().ddl_addYear.find('option[value="' + (Number(addBy) + 1) + '"]').prop('selected', true);
                } catch (e) {
                    alertify.error(e);
                    doms().txt_phasingFinYear.focus();
                }

            };
            var bindEvent = function () {
                function getBase64(file, name, callback) {
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        callback({ Result: reader.result, Name: name });
                    };
                    reader.onerror = function (error) {
                        alertify.error('Unable to read file');
                    };
                };  
                $(".file-abstruct-page").on('change', function () {
                    AbsPgDoc = {};
                    if (this.files.length > 0) {
                        window.StageFileByFormData(this, function (response) {
                                        AbsPgDoc.StageId = response.Data;
                                        AbsPgDoc.Result = null;
                            AbsPgDoc.Name = null;
                            alertify.success(response.Message);
                        }, function (response) {
                            alertify.error(response.Message);
                            (this).val('');
                        });
                    } 
                });
                $(document).on('change', ".file-subproject", function () {
                    var $tr = $(this).closest('tr');
                    $tr.attr('data-StageId', null);
                    if (this.files.length > 0) {
                        window.StageFileByFormData(this, function (response) {
                            $tr.attr('data-StageId', response.Data);
                            alertify.success(response.Message);
                        }, function (response) {
                            alertify.error(response.Message);
                            (this).val('');
                        });
                    } 
                });
                $(".file-vetted-project-page").on('change', function () {
                    VettedDoc = null;
                    if (this.files.length > 0) {
                        var file = getBase64(this.files[0], this.files[0].name, function (data) {
                            VettedDoc = data;
                        });
                    }
                });
                doms().tbl_phasingExpenditure.on('click', ".del-phasing-row", function () {
                    $(this).closest('tr').remove();
                    reCalculateExpentureSummary();
                });
                doms().tbl_phasingExpenditure.on('keyup', '.txt-prop-expenture-amnt', function () {
                    reCalculateExpentureSummary();
                });
                doms().btn_addYear.on('click', _onClickAddYear);
            };
            (function () {
                bindEvent();
            }());
        };
        var tblSubProjects = new function () {
            var tbl = doms().tbl_subPorjects;
            var prnt = this;
            var reCalculateExpentureSummary = function () {
                tbl.find('tfoot').empty();
                if (tbl.find('tbody tr').length == 0) {
                    return;
                }
                var tr = $('<tr/>');
                var totalExpendature = getFormData().SubProjects.reduce(function (a, b) { return a + Number(isNaN(b.Cost) ? 0 : b.Cost); }, 0);
                tr.append('<td colspan="6"><label>Total Project Cost</label></td><td class="text-right"><lable>' + totalExpendature + '</label></td>')
                tbl.find('tfoot').append(tr);
            };
            var bindEvent = function () {
                tbl.on('click', ".del-row-sub-project", function () {
                    $(this).closest('tr').remove();
                    reCalculateExpentureSummary();
                });
                tbl.on('keyup', '.txt-sub-project-cost', function () {
                    reCalculateExpentureSummary();
                });
                tbl.on('click', '.btn-add-subproject', function (e) {
                    var getUnitControl = function () {
                        var units = JSON.parse($(".hdn-units").val());
                        var select = "<select style='width:85px;' class='form-control sub-project-unit' name='sub-project-unit'>";
                         select += "<option value=''>--select--</option>";
                        units.forEach(function (v, i) {
                            select += "<option value='"+v.Id+"'>"+v.Name+"</option>";
                         });
                        select += "</select>";
                        return select;
                    };
                    tbl.find('tbody tr.no-row').remove();
                    var randId = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                    var randId1 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                    var FinalRandId = randId + randId1;
                    
                    var str = "<tr id=" + FinalRandId + " row-id=" + FinalRandId +"><td>"+(tbl.find('tbody tr').length+1)+"</td><td>";
                        str += "<input type='text' class='form-control txt-sub-project-Name' name='txt-sub-project-Name' value='' placeholder='Sub Porject Name' />";
                        str+="</td>";
                        str += "<td>" + getUnitControl() + "</td>";
                        str += "<td><input type='text' class='form-control txt-sub-project-qty' name='txt-sub-project-qty' value='0' style='text-align:right' placeholder='Quantity' /></td>";
                        str += "<td><input type='text' class='form-control txt-sub-project-cost' name='txt-sub-project-cost' value='' style='text-align:right' placeholder='Estimated Cost' />";
                        str += "<td><input type='file' accept='application/pdf' class='form-control file-subproject'/></td>"
                        //str += "<td><input type='file' class='form-control file-uc'/></td>"
                        str += "</td><td><a href='javascript:void(0);' class='del-row-sub-project'><span class='glyphicon glyphicon-trash'></span></a></td>";
                        str += "</tr>";
                        tbl.find('tbody').append(str);
                        reCalculateExpentureSummary();
                })
            };
            this.Validate = function () {
                var totalEstimate = getFormData().EstimatedCost;
                var totalSubprojectCost = getFormData().SubProjects.reduce(function (a, b) { return a + Number(isNaN(b.Cost) ? 0 : b.Cost); }, 0);
                if (totalEstimate != totalSubprojectCost) {
                    alertify.error('Total cost for subprojects and total esimated cost should be equal')
                    return false;
                }
                return true;
            };
            (function () {
                bindEvent();
            }())
        }
        var _eventHandlers = {
            _onSaveDraftClicked: function (e) {
                e.preventDefault();
                var formdata = getFormData();
                srv.saveAsDraft(formdata, function (e) {
                    if (e.Status == 200) {
                        alertify.success('Draft saved successfully');
                    } else {
                        alertify.error(e.Message);
                    }
                });
            },
            _onSubmitForm: function (e) {
                e.preventDefault();
                if (!$('form').valid()) {
                    var scrollTo = $(".error:nth(0)");
                    if (scrollTo.length > 0) {
                        $([document.documentElement, document.body]).animate({
                            scrollTop: scrollTo.offset().top
                        }, 200);
                    }
                    alertify.error('please enter data on mandetory fields');
                    return;
                }
                var validateExpenditure = utils.validateExpenditure();
                if (validateExpenditure.r === false) {
                    alertify.error(validateExpenditure.msg);
                    return;
                }
                var formdata = getFormData();
                var validationFlag = true;
                formdata.Part1.CheckList.forEach(function (ind, val) {
                    if (ind.IsNAConsiderable == "False" && ind.Val=="NO") {
                        validationFlag = false;
                    }
                });
                formdata.Part2.CheckList.forEach(function (ind, val) {
                    if (ind.IsNAConsiderable == "False" && ind.Val == "NO") {
                        validationFlag = false;
                    }
                });
                if (!validationFlag) {
                    alertify.alert("Please enter mandetory data!!");
                    return false;
                }
                var totalSubProjCost = 0;
                formdata.SubProjects.forEach(function (ind, val) {
                    totalSubProjCost = parseFloat(totalSubProjCost) + parseFloat(ind.Cost);
                });
                if (totalSubProjCost != formdata.EstimatedCost) {
                    alertify.alert("Estimated cost should be equal to total sub project's cost!!");
                    return false;
                }
                alertify.confirm("Confirm us!", "Are you sure to submit?", function () {
                   
                    srv.submitForm(formdata, function (e) {
                        if (e.Status == 200) {
                            alertify.alert('Successful', e.Message, function () {
                                location.href = "/Modules/Dpr/DprNewForm";
                            });
                        } else {
                            alert("Error on DPR submission");
                            alertify.error(e.Message);
                        }
                    });
                }, function () { });
            },
            _onResetForm: function (e) {
                location.reload();
            },
            _onChangeSchemeType: function () {
                var schemeCategories = JSON.parse(doms().hdn_SchemeCategories.val());
                doms().ddl_schemeCategory.find("option:not(:first-child)").remove();
                schemeCategories.filter(t=>t.SchemeTypeId == $(this).val()).forEach(function (val) {
                    doms().ddl_schemeCategory.append('<option value="' + val.Id + '">' + val.Name + '</option>')
                })
            }
            
        };
        var bindEvents = function () {
            doms().btn_submitForm.on('click', _eventHandlers._onSubmitForm);
            doms().btn_resetForm.on('click', _eventHandlers._onResetForm);
            doms().txt_MemoDate.datepicker({
                "dateFormat": 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
            });
            doms().txt_CommencementDate.datepicker({
                "dateFormat": 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
            });
            doms().txt_CompletionDate.datepicker({
                "dateFormat": 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
            });
            //doms().txt_SchemeDate.datepicker({ "dateFormat": 'dd/mm/yy' });
            doms().btn_saveAsDraft.on('click', _eventHandlers._onSaveDraftClicked);
            doms().ddl_SchemeType.on('change', _eventHandlers._onChangeSchemeType);
            //$('.hasSubProj').change(function () {
            //    if ($(this).is(":checked")) {
            //        $('.subProjDiv').css("display", "block");
            //    }
            //    else
            //    {
            //        $('.subProjDiv').css("display", "none");
            //    }
            //});
            $('.ddl-chk-part1').change(function () {
                if ($(this).val() == "NO" && $(this).attr('data-isNAConsiderable') == "False") {
                    $(this).css("color", "red");
                    $(this).css("border-color", "red");
                }
                else {
                    $(this).css("color", "black");
                    $(this).css("border-color", "#d2d6de");
                }
                if ($(this).val() == "NA" || $(this).val() == "NO")
                {
                    if ($(this).val() == "NA")
                    {
                        $(this).parent().parent().parent().find('.txt-pageno').prop("disabled", true);
                        $(this).parent().parent().parent().find('.txt-remarks').prop("disabled", true);
                    }
                    if ($(this).val() == "NO") {
                        $(this).parent().parent().parent().find('.txt-pageno').prop("disabled", true);
                        $(this).parent().parent().parent().find('.txt-remarks').prop("disabled", false);
                    }   
                }
                else
                {
                    $(this).parent().parent().parent().find('.txt-pageno').prop("disabled", false);
                    $(this).parent().parent().parent().find('.txt-remarks').prop("disabled", false);
                }
            });
            $('.ddl-chk-part2').change(function () {
                if ($(this).val() == "NO" && $(this).attr('data-isNAConsiderable') == "False") {
                    $(this).css("color", "red");
                    $(this).css("border-color", "red");
                }
                else {
                    $(this).css("color", "black");
                    $(this).css("border-color", "#d2d6de");
                }
                if ($(this).val() == "NA" || $(this).val() == "NO") {
                    $(this).parent().parent().parent().find('.txt-pageno').prop("disabled", true);
                    $(this).parent().parent().parent().find('.txt-remarks').prop("disabled", true);
                }
                else {
                    $(this).parent().parent().parent().find('.txt-pageno').prop("disabled", false);
                    $(this).parent().parent().parent().find('.txt-remarks').prop("disabled", false);
                }
            });
        }
        var registerValidator = function () {
            //add validate method
            $.validator.addMethod('greaterThan', function (value, el, param) {
                return value > param;
            });
            $.validator.addMethod('mobile', function (value, el, param) {
                var r = /^(\+\d{1,3}[- ]?)?\d{10}$/;
                return r.test(value);
            });

            var vld = {
                rules: {},
                messages: {},
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }
            }

            //impl rules
            vld.rules[doms().txt_contactNumber.attr('name')] = { required: true, mobile: true };
            vld.rules[doms().txt_devAuth.attr('name')] = { required: true };
            vld.rules[doms().txt_workName.attr('name')] = { required: true };
            vld.rules[doms().txt_implAgency.attr('name')] = { required: true };
            vld.rules[doms().txt_estCost.attr('name')] = { required: true, number: true, min: 0, greaterThan: 0 };
            vld.rules[$('.txt-prop-expenture-amnt').attr('name')] = { required: true, number: true, min: 0, greaterThan: 0 };
            vld.rules[doms().txt_engName.attr('name')] = { required: true };
            vld.rules[doms().txt_designation.attr('name')] = { required: true };
            vld.rules[doms().txt_ceoName.attr('name')] = { required: true };
            vld.rules[doms().txt_ceoEmail.attr('name')] = { required: true, email: true };
            vld.rules[doms().txt_ceoMob.attr('name')] = { required: true, mobile: true };
            vld.rules[doms().txt_MemoDate.attr('name')] = { required: true };
            vld.rules[doms().txt_memoNumber.attr('name')] = { required: true };
            //vld.rules[doms().txt_SchemeDate.attr('name')] = { required: true };
            vld.rules[doms().ddl_schemeCategory.attr('name')] = { required: true };
            vld.rules[doms().ddl_SchemeType.attr('name')] = { required: true };
            vld.rules[doms().txt_email.attr('name')] = { required: true, email: true };

            //impl mssgs
            vld.messages[doms().txt_contactNumber.attr('name')] = { mobile: "Invalid contact no" };
            vld.messages[doms().txt_ceoMob.attr('name')] = { mobile: "Invalid contact no" };
            //vld.messages[doms().txt_devAuth.attr('name')] = { required: true };
            //vld.messages[doms().txt_workName.attr('name')] = { required: true };
            //vld.messages[doms().txt_implAgency.attr('name')] = { required: true };
            vld.messages[doms().txt_estCost.attr('name')] = { greaterThan: "Invalid estimated cost" };
            vld.messages[$('.txt-prop-expenture-amnt').attr('name')] = { greaterThan: "Invalid amount" };
            //vld.messages[doms().txt_engName.attr('name')] = { required: true };
            //vld.messages[doms().txt_designation.attr('name')] = { required: true };
            //vld.messages[doms().txt_date.attr('name')] = { required: true };
            //vld.messages[doms().txt_memoNumber.attr('name')] = { required: true };

            $('form').validate(vld);
        }
        var getFormData = function () {  
            var getSubprojects = function () {
                var $cntr = $(".subProjDiv .tbl-sub-projects");
                return $cntr.find('tbody tr').map(function (i, v) {
                    var $tr = $(v);
                    return { RandId: $tr.attr('row-id'), StageId: $tr.attr('data-stageid') };

                }).get();
            };
            var convertToDate = function (strDate) {
                var arr = strDate.split('/');
                return new Date(arr[2], arr[1] - 1, arr[0]);
            }
            return {
                MunicipalityId: doms().hdn_municipalityId.val(),
                MunicipalityName: doms().txt_devAuth.val(),
                NameOfWork: doms().txt_workName.val(),
                ExecutiveSummary: doms().txt_exeSummary.val(),
                AgencyName: doms().txt_implAgency.val(),
                EstimatedCost: Number(isNaN(doms().txt_estCost.val()) ? 0 : doms().txt_estCost.val()),
                MemoDate: convertToDate(doms().txt_MemoDate.val()),
                CommencementDate: convertToDate(doms().txt_CommencementDate.val()),
                CompletionDate: convertToDate(doms().txt_CompletionDate.val()),
                //SchemeDate:convertToDate(doms().txt_SchemeDate.val()),
                MemoNo: doms().txt_memoNumber.val(),
                IdentityStamp: doms().hdn_IdentityStamp.val(),
                SchemeTypes: JSON.parse(doms().hdn_schemeTypes.val()),
                SchemeCategories: JSON.parse(doms().hdn_SchemeCategories.val()),
                SchemeTypeId: doms().ddl_SchemeType.val(),
                SchemeCategoryId: doms().ddl_schemeCategory.val(),
                EmailId: doms().txt_email.val(),
                PhasingOfExpenditures: doms().tbl_phasingExpenditure.find("tbody tr").map(function (i, t) { t = $(t); return { FinYear: t.find('.hdn-phasing-finyear').val(), ExpenditureAmount: t.find('.txt-prop-expenture-amnt').val() }; }).get(),
                Part1: {
                    CheckList: doms().chkLstPar1.map(function (ind, val) {
                        return {
                            Id: val.Id
                            , PageNo: val.txtPageno.val()
                            , Val: val.chk.val()
                            , IsReadOnly: val.chk.attr('data-IsReadOnly')
                            , DefaultValue: val.chk.attr('data-DefaultValue')
                            , Remarks: val.txtRemark.val()
                            , Perticular: val.Perticular
                            , IsNAConsiderable: val.chk.attr('data-isNAConsiderable')
                        };
                    }).get()
                },
                Part2: {
                    CheckList: doms().chkLstPar2.map(function (ind, val) {
                        return {
                            Id: val.Id
                            , PageNo: val.txtPageno.val()
                            , Val: val.chk.val()
                            , IsReadOnly: val.chk.attr('data-IsReadOnly')
                            , DefaultValue: val.chk.attr('data-DefaultValue')
                            , Remarks: val.txtRemark.val()
                            , Perticular: val.Perticular
                            , IsNAConsiderable: val.chk.attr('data-isNAConsiderable')
                        };
                    }).get()
                },
                NameOfEngineer: doms().txt_engName.val(),
                Designation: doms().txt_designation.val(),
                ContactNo: doms().txt_contactNumber.val(),
                CEOName: doms().txt_ceoName.val(),
                CEOEmail: doms().txt_ceoEmail.val(),
                CEOMob: doms().txt_ceoMob.val(),
                HasSubProj: true,
                SubProjects: doms()
                    .tbl_subPorjects.find("tbody tr")
                    .map(function (i, t) {
                        t = $(t);
                        return {
                            Name: t.find('.txt-sub-project-Name').val(),
                            Cost: (isNaN(t.find('.txt-sub-project-cost').val()) ? 0 : t.find('.txt-sub-project-cost').val()),
                            Qty: (isNaN(t.find('.txt-sub-project-qty').val()) ? 0 : t.find('.txt-sub-project-qty').val()),
                            UnitId: (isNaN(t.find('.sub-project-unit').val()) ? 0 : t.find('.sub-project-unit').val()),
                            RandId: t.attr('row-id')
                        };
                    }).get(),
                VettedDoc: VettedDoc,
                AbsPgDoc: AbsPgDoc,
                SubpProjFiles: getSubprojects()
            }
        }
        this.Init = function () {
            bindEvents();
            registerValidator();
        };
    };
    (function () {
        var srv = new Server();
        var app = new App(srv);
        app.Init();
    }());
});
