﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.GetCounterByMunicipalityID = function (callback) {          
            $.ajax({
                type: "POST",
                url: "/Municipality/MnCounter/CountersByMunicipalityID",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.Insert = function (data, callback) {          
            $.ajax({
                type: "POST",
                contentType:'application/json;charset=utf-8',
                url: "/Municipality/MnCounter/AddCounter",
                data: JSON.stringify({ obj: data }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
        this.ChangeStatusByCounterID = function (CounterID, callback) {
            $.ajax({
                type: "POST",
                contentType: 'application/json;charset=utf-8',
                url: "/Municipality/MnCounter/ChangeStatusByCounterID",
                data: "{CounterID:'" + CounterID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {     
        var Bind_GridOnPageLoad = function () {           
            $("#tblCounter").find("tbody").empty();
            var getRowString = function (rowData) {
                var Status = rowData.IsActive == true ? 'Active' : 'In-Active';
                var rowstr = "<tr>";
                rowstr += "<td class='CounterID' style='text-align:center;display:none'>" + rowData.CounterID + "</td>";
                rowstr += "<td class='CounterCode' style='text-align:center' >" + rowData.CounterCode + "</td>";
                rowstr += "<td class='MunicipalityID' style='text-align:center;display:none' >" + rowData.MunicipalityID + "</td>";
                rowstr += "<td class='IsActive' style='text-align:center;display:none'>" + rowData.IsActive + "</td>";
                rowstr += "<td class='Status' style='text-align:center'>" + Status + "</td>";
                rowstr += "<td style='text-align:center;cursor:pointer;'><div  class='btnRowEdit'><img title='Change Status' src='/Contents/image/edit.png'/></div></td>";
                //rowstr += "<td style='text-align:center'><a class='btnRowDel'><img src='/Contents/image/delete.png'  /></a></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onEditRow = function () {               
                var CounterID = $(this).closest('tr').find('.CounterID').text();
                var Status = $(this).closest('tr').find('.Status').text();
                var CounterCode = $(this).closest('tr').find('.CounterCode').text();
                Status = Status == 'Active' ? 'In-Active' : 'Active';
                if (confirm('Are you sure want to ' + Status + ' ' + CounterCode + ' Counter ??'))
                {
                    serverObject.ChangeStatusByCounterID(CounterID, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            alert('Status Change Succesfull !!');
                            $("#tblCounter").find("tbody").empty();
                            serverObject.GetCounterByMunicipalityID(function (response) {
                                if (response.Status == 200) {
                                    var rowDatas = response.Data;
                                    $.each(rowDatas, function (index, value) {
                                        var rowString = getRowString(value);
                                        appendToBody(rowString);
                                    });
                                }
                            });
                        } else {
                            alert(response.Message);
                        }
                    })                  
                }                
            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowEdit', onEditRow);
                $(document).on('click', '.btnRowEdit', onEditRow);
            };
            var appendToBody = function (rowString) {
                $("#tblCounter").find("tbody").append(rowString);
            };
            (function () {                
                serverObject.GetCounterByMunicipalityID(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
                RegisterEvent();
            }());
        }
        var BindEvent = function () {
            $("#btnSave").on('click', Insert);          
        };      
        var getDataFromView = function () {
            var CounterCode = $('#txtCounter').val().trim();
            var Status = $('#rdActive').is(':checked');
            var Municipality = [];
            var data = { CounterID: 0, CounterCode: CounterCode, MunicipalityID: 0, IsActive: Status, Municipality: Municipality };
            return data;
        };           
        var Validate = function (data) {
            if (data.CounterCode == '') {
                alert("Enter a Counter Number");
                $('#txtCounter').focus();
                return false;
            }            
            return true;
        };
        var Insert = function () {
            var formData = getDataFromView();
            if (Validate(formData)) {         
                serverObject.Insert(formData, function (response) {
                    var status = response.Status;
                    if (status == 200) {
                        alert('Record Save Successfully !!');
                        window.location.reload(true);
                    } else {
                        alert(response.Message);
                    }
                });
            }
        };
        this.app_Initiate = function () {
            BindEvent();
            Bind_GridOnPageLoad();           
        };
    };
    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };
    (function () {
        INIT();
    }());
});