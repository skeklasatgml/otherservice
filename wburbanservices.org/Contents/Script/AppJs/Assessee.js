﻿

$(document).ready(function () {
    var SERVER_OBJECT = function ()
    { 
        this.GetAssessee = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Assessee/Get_AllAssessee",
                data: null,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    callback(response);
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };
        this.Get_Municipality = function (data,callback) {

                    $.ajax({
                        type: "POST",
                        url: "/Assessee/AutoMunicipality",
                        data: JSON.stringify(data),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            if (response.Status == 200)
                            {
                                var rowDatas = response.Data;
                                callback(rowDatas);
                            }
                            
                        }
                    });
        };
        this.GetWard = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Assessee/AutoWard",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetLocation = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Assessee/AutoLocation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.GetHoldingType = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Assessee/AutoHoldingType",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.Status == 200) {
                        callBack(data.Data);
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        this.Insert_Assessee = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Assessee/Insert_Assessee",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callBack(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        this.Update_Assessee = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Assessee/Update_Assessee",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (response) {
                    callBack(response);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    };

    var APP_OBJECT = function (serverObject)
    {
        var BindEvent = function () {
            $("#btnCancel").on('click', Reset_Field);
            $("#btnInsert").on('click', OnSave);

            $("#txtMunicipality").autocomplete(Auto_Municipality());
            $("#txtMunicipality").on('keydown', Blank_Municipality);

            $("#txtWard").autocomplete(PopulateWard());
            $("#txtWard").on('keydown', Blank_Ward);

            $("#txtLocation").autocomplete(PopulateLocation());
            
            $("#txtHoldingType").autocomplete(PopulateHoldingType());
            $("#txtHoldingType").on('keydown', Blank_HoldingType);
        };
        var OnSave = function () {

            if (From_Validate()) {

                var EDate = ""; var TDate = "";
                if ($("#txtEffectiveFrom").val() != "") {
                    var eDate = ($("#txtEffectiveFrom").val()).split('/');
                    EDate = eDate[2] + "-" + eDate[1] + "-" + eDate[0];
                }
                if ($("#txtEffectiveTo").val() != "") {
                    var tDate = ($("#txtEffectiveTo").val()).split('/');
                    TDate = tDate[2] + "-" + tDate[1] + "-" + tDate[0];
                }

                var formData = {
                    AssesseeID: $("#hdnAssesseID").val(), MunicipalityID: $('#hdnMunicipality').val(), WardID: $('#hdnWard').val(), LocationID: $('#hdnLocation').val(),
                    HoldingTypeID: $('#hdnHoldingType').val(), HoldingNo: $.trim($('#txtHoldingNo').val()), AssesseeName: $.trim($('#txtAssesseeName').val()),
                    AssesseAddress: $.trim($('#txtAssesseAddress').val()), EffectiveFrom: $.trim(EDate), EffectiveTo: $.trim(TDate),
                };
               
                var buttonText = $("#btnInsert").val(); 
                if (buttonText == 'Submit') {
                    serverObject.Insert_Assessee(formData, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            alert('Assessee Details are Inserted Successfully.')
                            Reload_Page();
                        } else {
                            alert(response.Message);
                        }
                    });
                }
                else {
                    serverObject.Update_Assessee(formData, function (response) {
                        var status = response.Status;
                        if (status == 200) {
                            alert('Assessee Details are Updated Successfully.')
                            Reload_Page();
                        } else {
                            alert(response.Message);
                        }
                    });
                }
            }
            else {
               
            }
        };
        var PopulateWard = function () {
            
            $("#txtWard").autocomplete({

                source: function (request, response) {
                    var municipalityID = $("#hdnMunicipality").val();
                    var data = { municipalityID: municipalityID, wardName: $.trim(request.term) }; 
                    serverObject.GetWard(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.WardName,
                                    WardID: item.WardID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtWard').val('');
                            $('#hdnWard').val('');
                        }

                    });
                },
                select: function (e, i) {
                    $("#hdnWard").val(i.item.WardID);
                    PopulateLocation();
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnWard").val('');
                        PopulateLocation();
                        $('#txtLocation').val('');
                        $('#hdnLocation').val('');
                    }
                }
            });


        }
        var PopulateLocation = function () {

            $("#txtLocation").autocomplete({
                source: function (request, response) {
                    var municipalityID = $("#hdnMunicipality").val();
                    var wardID = $("#hdnWard").val();
                    var data = { municipalityID: municipalityID, wardID: wardID, locationName: $.trim(request.term) };
                    serverObject.GetLocation(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.LocationName,
                                    LocationID: item.LocationID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtLocation').val('');
                            $('#hdnLocation').val('');
                        }

                    });
                },
                select: function (e, i) {
                    $("#hdnLocation").val(i.item.LocationID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnLocation").val('');
                    }
                }
            });
        }
        var PopulateHoldingType = function () {
            $("#txtHoldingType").autocomplete({
                source: function (request, response) {
                    var data = { HoldingType: $.trim(request.term) };
                    serverObject.GetHoldingType(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.HoldingTypeDesc,
                                    HoldingTypeID: item.HoldingTypeID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtHoldingType').val('');
                            $('#hdnHoldingType').val('');
                        }

                    });
                },
                select: function (e, i) {
                    $("#hdnHoldingType").val(i.item.HoldingTypeID);
                },
                change: function (e, i) {
                    if (!i.item) {
                        $("#hdnHoldingType").val('');
                    }
                }
            });
        }
        var From_Validate = function ()
        {
            $("#AssesseeForm").validate({
                rules: {
                    txtMunicipality: "required",
                    txtWard: "required",
                    txtLocation: "required",
                    txtHoldingType: "required",
                    txtHoldingNo: "required",
                    txtAssesseeName: "required",
                },
                messages: {
                    txtMunicipality: {
                        required: "Please select municipality."
                    },
                    txtWard: {
                        required: "Please select ward."
                    },
                    txtLocation: {
                        required: "Please select location."
                    },
                    txtHoldingType: {
                        required: "Please select holding type."
                    },
                    txtHoldingNo: {
                        required: "Please select holding No."
                    },
                    txtAssesseeName: {
                        required: "Please enter Assessee Name."
                    }
                },
                errorPlacement: function (error, element) {

                    element.attr("placeholder", error[0].outerText);
                    element.css("color", 'black');
                }
            });
           return $('#AssesseeForm').valid();
        }
        var Auto_Municipality = function () {
            return {
                source: function (request, response) {
                    var data = { municipalityParam: $.trim(request.term) };
                    serverObject.Get_Municipality(data, function (serverResponse) {
                        if ((serverResponse).length > 0) {
                            var userDataAutoComplete = [];
                            $.each(serverResponse, function (index, item) {
                                userDataAutoComplete.push({
                                    label: item.MunicipalityName,
                                    MunicipalityID: item.MunicipalityID
                                });
                            });
                            response(userDataAutoComplete);
                        }
                        else {
                            alert('No data found.');
                            $('#txtMunicipality').val('');
                            $('#hdnMunicipality').val('');
                        }
                    });
                },
                select: function (e, i) {
                    $("#hdnMunicipality").val(i.item.MunicipalityID);
                    PopulateWard();
                },
                change: function (e, i) {
                   
                    if (!i.item) {
                        $("#hdnMunicipality").val('');
                        $('#txtWard').val('');
                        $('#hdnWard').val('');
                        $('#txtLocation').val('');
                        $('#hdnLocation').val('');
                        PopulateWard();
                    }
                }
            };
        }
        var Reset_Field = function ()
        {
            $("#txtAssesseeNo").val('');
        }
        var feedToForm = function (data) {
            $("#hdnAssesseID").val(data.AssesseeID);
            $("#txtMunicipality").val(data.Municipality);
            $("#hdnMunicipality").val(data.MunicipalityID);
            $("#txtWard").val(data.Ward);
            $("#hdnWard").val(data.WardID);

            $("#txtLocation").val(data.Location);
            $("#hdnLocation").val(data.LocationID);

            $("#txtHoldingType").val(data.HoldingType);
            $("#hdnHoldingType").val(data.HoldingTypeID);

            $("#txtHoldingNo").val(data.HoldingNo);
            $("#txtAssesseeName").val(data.AssesseeName);
            $("#txtAssesseAddress").val(data.Address == "null" ? '' : data.Address);

            //var myDate = new Date((data.EffectiveTo).match(/\d+/)[0] * 1);
            if (data.EffectiveTo != '' || data.EffectiveTo != null || data.EffectiveTo != "null") {

            } else {
                var ToDate = new Date(parseInt((data.EffectiveTo).substr(6)));
                var Tresult = ToDate.format("dd/mm/yyyy")
                $("#txtEffectiveTo").val(Tresult == "null" ? '' : Tresult);
            }

            if (data.EffectiveFrom != '' || data.EffectiveFrom != null || data.EffectiveFrom != "null") {

            } else {
                var FromDate = new Date(parseInt((data.EffectiveFrom).substr(6)));
                var Fresult = FromDate.format("dd/mm/yyyy")
                $("#txtEffectiveFrom").val(Fresult == "null" ? '' : Fresult);
            }

            $("#btnInsert").val("Update");
            //ctrl_DrpDistrict.find("option[value='" + DistrictID + "']").prop('selected', true);

        };

        //Grid Edit and Delete
        var Bind_GridOnPageLoad = function () { 
            var ctr_btnRowDel = $(".btnRowDel");
            var btnRowEdit = $(".btnRowEdit");
            $("#tabDtl").find("tbody").empty();
            var getRowString = function (rowData) {
                var rowstr = "<tr>";
                rowstr += "<td class='AssesseeName'>" + rowData.AssesseeName + "</td>";
                rowstr += "<td class='AssesseeID' style='display:none;'>" + rowData.AssesseeID + "</td>";
                rowstr += "<td class='Municipality'>" + rowData.MunicipalityName + "</td>";
                rowstr += "<td class='MunicipalityID' style='display:none;'>" + rowData.MunicipalityID + "</td>";
                rowstr += "<td class='Location'>" + rowData.LocationName + "</td>";
                rowstr += "<td class='LocationID' style='display:none;'>" + rowData.LocationID + "</td>";
                rowstr += "<td class='Ward'>" + rowData.WardName + "</td>";
                rowstr += "<td class='WardID' style='display:none;'>" + rowData.WardID + "</td>";
                rowstr += "<td class='HoldingType'>" + rowData.HoldingTypeDesc + "</td>";
                rowstr += "<td class='HoldingTypeID' style='display:none;'>" + rowData.HoldingTypeID + "</td>";
                rowstr += "<td class='HoldingNo'>" + rowData.HoldingNo + "</td>";
                rowstr += "<td class='Address'>" + rowData.AssesseeAddress + "</td>";
                rowstr += "<td class='EffectiveFrom' style='display:none;'>" + rowData.EffectiveFrom + "</td>";
                rowstr += "<td class='EffectiveTo' style='display:none;'>" + rowData.EffectiveTo + "</td>";
                rowstr += "<td><a class='btnRowEdit' href='javascript:void(0);'><img src='/Contents/image/edit.png'  /></a></td>";
                rowstr += "<td><a class='btnRowDel' ><img src='/Contents/image/delete.png'  /></a></td>";
                rowstr += "</tr>";
                return rowstr;
            };
            var onDeleteRow = function () {
                var ctrl = $(this);
                //find row

            };
            var onEditRow = function () {
                var ctrl = $(this);
                var currentrow = ctrl.parent().parent();

                var AssesseeName = currentrow.find(".AssesseeName").text();
                var AssesseeID = currentrow.find(".AssesseeID").text();
                var Municipality = currentrow.find(".Municipality").text();
                var MunicipalityID = currentrow.find(".MunicipalityID").text();
                var Location = currentrow.find(".Location").text();
                var LocationID = currentrow.find(".LocationID").text();
                var Ward = currentrow.find(".Ward").text();
                var WardID = currentrow.find(".WardID").text();
                var HoldingType = currentrow.find(".HoldingType").text();
                var HoldingTypeID = currentrow.find(".HoldingTypeID").text();
                var HoldingNo = currentrow.find(".HoldingNo").text();
                var Address = currentrow.find(".Address").text();
                var EffectiveFrom = currentrow.find(".EffectiveFrom").text();
                var EffectiveTo = currentrow.find(".EffectiveTo").text();

                $("#btnInsert").attr('AssesseeID', AssesseeID);

                var model = {
                    AssesseeName: AssesseeName, Municipality: Municipality, MunicipalityID: MunicipalityID, Location: Location,
                    LocationID: LocationID, Ward: Ward, WardID: WardID, HoldingType: HoldingType, HoldingTypeID: HoldingTypeID,
                    HoldingNo: HoldingNo, Address: Address, EffectiveFrom: EffectiveFrom, EffectiveTo: EffectiveTo, AssesseeID: AssesseeID
                };
                feedToForm(model);
            };
            var RegisterEvent = function () {
                $(document).off('click', '.btnRowDel', onDeleteRow);
                $(document).on('click', '.btnRowDel', onDeleteRow);

                $(document).off('click', '.btnRowEdit', onEditRow);
                $(document).on('click', '.btnRowEdit', onEditRow);
            };
            var appendToBody = function (rowString) {
                $("#tabDtl").find("tbody").append(rowString);
            };
            (function () {
                RegisterEvent();
                serverObject.GetAssessee(function (response) {
                    if (response.Status == 200) {
                        var rowDatas = response.Data;
                        $.each(rowDatas, function (index, value) {
                            var rowString = getRowString(value);
                            appendToBody(rowString);
                        });
                    }
                });
            }());
        }
        var Blank_Municipality = function (evt)
        {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 8) {
                $("#txtMunicipality").val('');
                $("#hdnMunicipality").val('');
                $('#txtWard').val('');
                $('#hdnWard').val('');
                $('#txtLocation').val('');
                $('#hdnLocation').val('');
            }
            if (iKeyCode == 46) {
                $("#txtMunicipality").val('');
                $("#hdnMunicipality").val('');
                $('#txtWard').val('');
                $('#hdnWard').val('');
                $('#txtLocation').val('');
                $('#hdnLocation').val('');
            }
        }
        var Blank_Ward = function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 8) {
                //$("#txtMunicipality").val('');
                //$("#hdnMunicipality").val('');
                $('#txtWard').val('');
                $('#hdnWard').val('');
                $('#txtLocation').val('');
                $('#hdnLocation').val('');
            }
            if (iKeyCode == 46) {
                //$("#txtMunicipality").val('');
                //$("#hdnMunicipality").val('');
                $('#txtWard').val('');
                $('#hdnWard').val('');
                $('#txtLocation').val('');
                $('#hdnLocation').val('');
            }
        }
        var Blank_HoldingType = function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 8) {
                $('#txtHoldingType').val('');
                $('#hdnHoldingType').val('');
            }
            if (iKeyCode == 46) {
                $('#txtHoldingType').val('');
                $('#hdnHoldingType').val('');
            }
        }

        var Reload_Page=function()
        {
            $('#txtMunicipality').val(''); $('#hdnMunicipality').val('');
            $('#txtWard').val(''); $('#hdnWard').val('');
            $('#hdnLocation').val(''); $('#txtLocation').val('');
            $('#txtHoldingType').val(''); $('#hdnHoldingType').val('');
            $('#txtHoldingNo').val(''); $('#txtAssesseeName').val('');
            $('#txtAssesseAddress').val(''); $('#txtEffectiveFrom').val(''); $('#txtEffectiveTo').val('');
            $("#hdnAssesseID").val('');
        }

        this.app_Initiate = function () {
            Bind_GridOnPageLoad();
            BindEvent();            
        };
    };

    var INIT = function ()
    {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());


});