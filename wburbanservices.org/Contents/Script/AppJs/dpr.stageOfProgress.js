﻿$(document).ready(function () {
    var groupAddEditComponent = function () {
        var $component = this;
        var _dom = null;
        this.onChangeGroup = function (t) { };
        this.triggerddlGroup = function () {
            _dom.ddlGroup.obj.trigger('change');
        };
        this.getSelectGroupId = function () {
            return _dom.ddlGroup.obj.val();
        };
        var cacheDom = function () {
            var _selector = ".group-master";
            return {
                container: { selector: _selector, obj: $(_selector) },
                ddlGroup: { selector: _selector.concat(" .ddl-group"), obj: $(_selector.concat(" .ddl-group")) },
                openAdd: { selector: _selector.concat(" .add-new-group"), obj: $(_selector.concat(" .add-new-group")) },
                openEdit: { selector: _selector.concat(" .edit-group"), obj: $(_selector.concat(" .edit-group")) },
                modal: { selector: _selector.concat('#group-modal'), obj: $(_selector.concat(" #group-modal")) }
            };
        };
        var bindEvents = function () {
            $(document).on('click', _dom.openAdd.selector, function () {
                _dom.modal.reset();
                _dom.modal.changeTitle("Add new group");
                _dom.modal.obj.modal('show');
            });
            $(document).on('click', _dom.openEdit.selector, function () {
                var ddlGroup = _dom.ddlGroup.obj;
                _dom.modal.reset();
                _dom.modal.changeTitle("Edit selected group");
                _dom.modal.setData(ddlGroup.val(), ddlGroup.find('option:selected').text());
                _dom.modal.obj.modal('show');
            });
            $(document).on('change', _dom.ddlGroup.selector, function () {
                if ($component.onChangeGroup && typeof $component.onChangeGroup === "function")
                    $component.onChangeGroup($(this).val());
            });
            _dom.modal.changeTitle = function (title) {
                _dom.modal.obj.find(".modal-title").text(title);
            };
            _dom.modal.reset = function () {
                _dom.modal.obj.find(".txt-add-edit-group").val("");
                _dom.modal.obj.find(".hdn-groupid").val("");
            };
            _dom.modal.getFormData = function () {
                return {
                    Id: _dom.modal.obj.find(".hdn-groupid").val(),
                    Value: _dom.modal.obj.find(".txt-add-edit-group").val()
                };
            };
            _dom.modal.isValid = function () {
                return _dom.modal.obj.find("form").valid();
            };
            _dom.modal.setData = function (groupId, groupName) {
                _dom.modal.obj.find(".hdn-groupid").val(groupId);
                _dom.modal.obj.find(".txt-add-edit-group").val(groupName);
            };
            _dom.modal.obj.on('click', ".btn-submit", function () {
                if (_dom.modal.isValid()) {
                    alertify.confirm("Confirm Us!", "Are you sure to save?", function () {
                        var formData = _dom.modal.getFormData();
                        $.ajax({
                            url: "/Modules/DprExtension/SaveStageGroup",
                            method: "Post",
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify(formData),
                            success: function (e) {
                                if (e.Status === 200) {
                                    _dom.modal.obj.modal('hide');
                                    alertify.success(e.Message);
                                    if (e.Data) {
                                        var select = _dom.ddlGroup.obj.find('option[value="' + e.Data.Id + '"]');
                                        if (select.length > 0) {
                                            select.text(e.Data.Value);

                                        } else {
                                            _dom.ddlGroup.obj.append("<option value='" + e.Data.Id + "'>" + e.Data.Value + "</option>")
                                        }
                                    }
                                } else {
                                    alertify.error(e.Message);
                                }
                            },
                            error: function (e) {
                                alertify.error(e.responseText || e.statusText);
                            }
                        });
                    }, function () { })
                }
            })
            _dom.modal.obj.on("hidden.bs.modal", function () {
                _dom.modal.reset();
            });
        };
        (function () {
            _dom = cacheDom();
            bindEvents();
        }());
    };

    var MileStonesComponent = function (groupComponent) {
        var $mileStoneComp = this;
        var _table = null;
        var _dom = null;
        var cacheDom = function () {
            var _selector = ".milestones";
            return {
                container: { selector: _selector, obj: $(_selector) },
                table: { selector: _selector.concat(" .table-mile-stone"), obj: $(_selector.concat(" .table-mile-stone")) },
                modal: { selector: _selector.concat('#mile-stone-crud'), obj: $(_selector.concat(" #mile-stone-crud")) }
            };
        };
        groupComponent.onChangeGroup = function (tt) {
            var payload = { groupId: tt };
            $.ajax({
                url: "/Modules/DprExtension/GetWorkMileStones",
                method: "Post",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(payload),
                success: function (e) {
                    if (e.Status === 200) {
                        OnMilestonesLoaded(e.Data);
                    } else {
                        alertify.error(e.Message);
                    }
                },
                error: function (e) {
                    alertify.error(e.responseText || e.statusText);
                }
            });
        };
        var OnMilestonesLoaded = function (datas) {
            _table.reset();
            datas.forEach(function (v, i) {
                var row = _table.getRowTemplate(v);
                _table.addRow(row);
            });
        };
        var tableCreate = function (obj) {
            var $table = this;
            var addEmptyRowIfPossible = function () {
                if (obj.find('tbody tr:not(.no-row)').length <= 0) {
                    var str = "<tr class='no-row'><td colspan='4' class='text-center'>no data available</td></tr>";
                    obj.find('tbody').append(str);
                } else {
                    obj.find('tbody tr.no-row').remove();
                }
            };
            this.addRow = function (row) {
                obj.find('tbody').append(row);
                addEmptyRowIfPossible();
            };
            this.getRowTemplate = function (data) {
                var jsonForm = JSON.stringify(data);
                var str = "<tr>";
                str += "<td>" + data.Dscr + "</td>";
                str += "<td>" + data.PerComplited + " %</td>";
                str += "<td><a href='javascript:void(0);' class='row-edit'>edit</a></td>"
                str += "<td><a href='javascript:void(0);' class='row-del'>delete</a></td>"
                str += "</tr>";
                var outp = $(str);
                outp.attr('data-json', jsonForm);
                outp.attr('data-id', data.Id);
                return outp;
            };
            this.delRow = function (data) {
                obj.find('tbody tr[data-id="' + data.Id + '"]').remove();
                addEmptyRowIfPossible();
            };
            this.updateRow = function (data, row) {
                obj.find('tbody tr[data-id="' + data.Id + '"]').replaceWith(row);
            }
            this.reset = function () {
                obj.find('tbody').empty();
                addEmptyRowIfPossible();
            };
            //default funstion assigne. actually not called, because subscriber overridden
            this.onAddClicked = function (successCallback) {
                if (successCallback && typeof successCallback === "function"){
                        successCallback('data');
                    }
            };
            //default funstion assigne. actually not called, because subscriber overridden
            this.onEditClicked = function (data, successCallback) {
                if (successCallback && typeof successCallback === "function") {
                    successCallback('data');
                }
            };
            //default funstion assigne. actually not called, because subscriber overridden
            this.onDeleteClicked = function (data, successCallback) {
                if (successCallback && typeof successCallback === "function") {
                    successCallback();
                }
            };
            //manipulate dom
            var bindEvent = function () {
                _dom.table.obj.find('tbody').on('click', 'tr .row-edit', function () {
                    var $row = $(this).closest('tr');
                    if ($table.onEditClicked && typeof $table.onEditClicked === "function")
                        $table.onEditClicked(JSON.parse($(this).closest('tr').attr('data-json')), function (data) {
                            //successCallback
                            var row = $table.getRowTemplate(data.data);
                            if (data.mode === "edit") {
                                $row.replaceWith(row);
                            } else if (data.mode === "add") {
                                $table.addRow(row);
                            } else {
                                alertify.error('Invalid mode selected!')
                            }
                        });
                });
                _dom.table.obj.find('tbody').on('click', 'tr .row-del', function () {
                    var $row = $(this).closest('tr');
                    if ($table.onDeleteClicked && typeof $table.onDeleteClicked === "function")
                        $table.onDeleteClicked(JSON.parse($(this).closest('tr').attr('data-json')), function (data) {
                            //successCallback
                            $table.delRow(JSON.parse($row.attr('data-json')));
                        });
                });
                _dom.table.obj.find('tfoot').on('click', 'tr .btn-add-mile-stone', function () {
                    var $row = $(this).closest('tr');
                    if ($table.onAddClicked && typeof $table.onAddClicked === "function")
                        $table.onAddClicked(function (data) {
                            //successCallback
                            var row = $table.getRowTemplate(data.data);
                            if (data.mode === "edit") {
                                $row.replaceWith(row);
                            } else if (data.mode === "add") {
                                $table.addRow(row);
                            } else {
                                alertify.error('Invalid mode selected!')
                            }
                        });
                });
            };

            (function () {
                bindEvent();
                $table.reset();
            }());
        };
        var modalFormPollyfill = function () {
            var _modalDom = {
                txtDscr: _dom.modal.obj.find(".txt-dscr"),
                txtPer: _dom.modal.obj.find(".txt-per"),
                btnSubmit: _dom.modal.obj.find(".btn-submit"),
                hdnId: _dom.modal.obj.find(".hdn-id"),
            };
            _dom.modal.changeTitle = function (title) {
                _dom.modal.obj.find(".modal-title").text(title);
            };
            _dom.modal.reset = function () {
                _modalDom.txtDscr.val('');
                _modalDom.txtPer.val('');
                _modalDom.hdnId.val('');
            };
            _dom.modal.getFormData = function () {
                return {
                    Id: _modalDom.hdnId.val(),
                    Dscr: _modalDom.txtDscr.val(),
                    PerComplited: _modalDom.txtPer.val(),
                    GroupId: groupComponent.getSelectGroupId()
                };
            };
            _dom.modal.isValid = function () {
                return _dom.modal.obj.find("form").valid();
            };
            _dom.modal.setData = function (id, percent, dscr) {
                _modalDom.txtDscr.val(dscr);
                _modalDom.txtPer.val(percent);
                _modalDom.hdnId.val(id);
            };
            _dom.modal.obj.on('click', ".btn-submit", function () {
                if (_dom.modal.isValid()) {
                    alertify.confirm("Confirm Us!", "Are you sure to save?", function () {
                        var formData = _dom.modal.getFormData();
                        formData = Object.assign({ payload: formData }, { mode: _dom.modal.callback.mode });

                        $.ajax({
                            url: "/Modules/DprExtension/SaveChangeMileStone",
                            method: "Post",
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify(formData),
                            success: function (e) {
                                if (e.Status === 200) {
                                    _dom.modal.callback.callback({ data: e.Data, mode: _dom.modal.callback.mode });
                                    alertify.success(e.Message);
                                    _dom.modal.obj.modal('hide');
                                } else {
                                    alertify.error(e.Message);
                                }
                            },
                            error: function (e) {
                                alertify.error(e.responseText || e.statusText);
                            }
                        });
                    }, function () { });
                }
            })
            _dom.modal.obj.on("hidden.bs.modal", function () {
                _dom.modal.reset();
            });
            (function () {
                var obj = {
                    rules: {},
                    messages: {},
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                        error.css('color', 'red');
                    }
                };
                obj.rules[_modalDom.txtDscr.attr('name')] = { required: true };
                obj.rules[_modalDom.txtPer.attr('name')] = { required: true };

                obj.messages[_modalDom.txtPer.attr('name')] = { required: "Data required" };
                obj.messages[_modalDom.txtDscr.attr('name')] = { required: "Data required" };

                _dom.modal.obj.find('form').validate(obj);
            }());
        };
        var bindEvent = function () {
            _table.onAddClicked = function (successCallback) {
                _dom.modal.obj.modal('show');
                _dom.modal.changeTitle("Add New Milestone");
                _dom.modal.callback = { mode: "add", callback: successCallback };
            };
            _table.onEditClicked = function (data, successCallback) {
                _dom.modal.obj.modal('show');
                _dom.modal.changeTitle("Edit Milestone");//{"Id":1,"GroupId":1,"Dscr":"Soil preparation – rototilling, grading ","Order":1,"PerComplited":10}
                _dom.modal.setData(data.Id, data.PerComplited, data.Dscr);
                _dom.modal.callback = { mode: "edit", callback: successCallback };
            };
            _table.onDeleteClicked = function (data, successCallback) {
                if (successCallback && typeof successCallback === "function") {

                    alertify.confirm("Confirm Us!", "Are you sure to delete?", function () {
                        var formData = _dom.modal.getFormData();
                        formData = Object.assign({ payload: { Id: data.Id } }, { mode: "delete" });

                        $.ajax({
                            url: "/Modules/DprExtension/SaveChangeMileStone",
                            method: "Post",
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify(formData),
                            success: function (e) {
                                if (e.Status === 200) {
                                    successCallback({ data: e.Data, mode: formData.mode });
                                    alertify.success(e.Message);
                                } else {
                                    alertify.error(e.Message);
                                }
                            },
                            error: function (e) {
                                alertify.error(e.responseText || e.statusText);
                            }
                        });
                    }, function () { });
                    
                }
            };
        };
            (function () {
                _dom = cacheDom();
                _table = new tableCreate(_dom.table.obj);
                groupComponent.triggerddlGroup();
                bindEvent();
                modalFormPollyfill();
            }());
    };
    (function () {
        var x = new groupAddEditComponent();
        var y = new MileStonesComponent(x);
    }())
});