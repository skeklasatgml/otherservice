﻿$(document).ready(function () {
    app.ddlStageGroup();
    $('#subproj-modal').on('hidden.bs.modal', function () {
        $('.txt-add-edit-spt').val("");
        $('.hdntxt-add-edit-spt').val(null);
    })
    $(document).on("click", ".btn-submit", function () {
        var configureDetails = { "StgGrpId": $('.ddlStageGrp').val(), "Id": $('.hdntxt-add-edit-spt').val(), "Desc": $('.txt-add-edit-spt').val()}
        console.log(configureDetails);
        if ($('.txt-add-edit-spt').val().length <= 0)
        {
            alertify.alert("Please enter Sub Project Type");
            return;
        }
        if ($('.ddlStageGrp').val().length <= 0) {
            alertify.alert("Please select stage group");
            return;
        }
        if ($('.txt-add-edit-spt').val().length>0) {
            alertify.confirm('Please Confirm Us!', 'Are you sure to save?', function () {
                $.ajax({
                    type: "POST",
                    url: "/Modules/DprExtension/SaveSubProjType",
                    data: JSON.stringify(configureDetails),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (Response) {
                        if (Response.Status === 200) {
                            alertify.alert("Successful!", Response.Message, function () {
                                location.reload();
                            });
                        } else {
                            alertify.alert("Error!", Response.Message, function () { });
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });

            }, function () { })
        }
    });
    $(document).on("change", ".ddlStageGrp", function () {
        app.GetSubProjectType();
    });
    $(document).on("click", ".btn-add", function () {
        $('#subproj-modal').modal("show");
    });
    $(document).on("click", ".updateRow", function () {
        var jsnData = JSON.parse($(this).closest('tr').find('.hdnJSN').val());
        $('#subproj-modal').modal("show");
        $('.txt-add-edit-spt').val(jsnData.Desc);
        $('.hdntxt-add-edit-spt').val(jsnData.Id);
    });
    $(document).on("click", ".delete", function () {
        var ch = confirm("Are you sure you want to delete?");
        if (ch)
        {
            var jsnData = JSON.parse($(this).closest('tr').find('.hdnJSN').val());
            app.DelSubProjectType(jsnData.Id);
        }
           
    });
});
var app = {
    GetSubProjectType: function () {
        var payload = { "Id": $('.ddlStageGrp').val()};
        $.ajax({
            url: '/Modules/DprExtension/GetSubProjectType',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.Data != null) {
                    $('#myTable').find('>tbody').empty();
                    for (i = 0; i < result.Data.length; i++) {
                        var tempJson = result.Data[i];

                        var tr = $('<tr/>');
                        var td = $('<td />');
                        tr.append(td.append(i + 1));

                        var td = $('<td class="subprojtype" />');
                        tr.append(td.append(tempJson.Desc));

                        td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                        tr.append(td.append('<a class="updateRow"><input type="hidden" class="hdnJSN" value=\'' + tempJson + '\'/>Edit</a>'));

                        td = $('<td/>'); tempJson = JSON.stringify(tempJson);
                        tr.append(td.append('<a class="delete"><input type="hidden" class="hdnJSN" value=\'' + tempJson + '\'/>Delete</a>'));

                        //td = $('<td class="completeJson"><input type="hidden" class="hdnJSN" value="' + tempJson +'"/></td>');
                        //tr.append(td);
                        $('#myTable').find('>tbody').append(tr);
                    }
                }
            },
            error: function () {
                $('#myTable').find('>tbody').empty();}
        });
    },
    DelSubProjectType: function(Id) {
        var payload = { "Id": Id };
        $.ajax({
            url: '/Modules/DprExtension/DelSubProjectType',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                alertify.alert("Data deleted successfully!!");
                location.reload();
            },
            error: function () { }
        });
    },
    ddlStageGroup: function () {
        $.ajax({
            url: '/Modules/DprExtension/GetStageGroup',
            type: 'POST',
            data: null,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                app.PopulateStageGroup(response.Data);
                $('#ddlStageGrp').val("")
            },
            error: function () { }
        });
    },
    PopulateStageGroup: function (data) {
        var ctlr_ddlstagegroup = $('.ddlStageGrp');
        ctlr_ddlstagegroup.empty();
        ctlr_ddlstagegroup.append('<option value="">--select--</option>');
        $(data).each(function (index, value) {
            //$.each(data, function (index, value) {

            ctlr_ddlstagegroup.append("<option value='" + value.Value + "' >" + value.Text + "</option>")
        });
        var option = ctlr_ddlstagegroup.find("option[value='" + ctlr_ddlstagegroup.attr("data-selected-stagegroup") + "']");
        if (option.length > 0) {
            option.prop('selected', true);
        } else {
            ctlr_ddlstagegroup.find('option:eq(0)').prop('selected', true);
        }
    },
}