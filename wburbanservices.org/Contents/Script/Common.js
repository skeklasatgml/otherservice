﻿$(document).ready(function () {

    //obsolete codes
    $('.DatePicker').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $(document).on('focus', '.datepicker', function () {
        $(this).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "-200:+10"
        });
    });
    var setDocElementProperly = function () {
        //---------set min hight of document body----------------------
        var footerHeight = $(".footer").height();
        //console.log(footerHeight);
        var footerTopDownPadding = parseInt($(".footer").css('padding-top')) + parseInt($(".footer").css('padding-bottom'));
        //console.log(footerTopDownPadding);
        var headerHeight = $(".navbar").height();
        //console.log(headerHeight);
        var windowHeight = $(window).height();
        //console.log(windowHeight);
        var DocumentBodyTopPadding = parseInt($("#DocumentBody").css('padding-top'));
        //console.log(DocumentBodyTopDownPadding);
        $("#DocumentBody").css('min-height', (windowHeight - (footerHeight + headerHeight)) + DocumentBodyTopPadding - footerTopDownPadding);
        //----------end set min hight of document body
    };
    setDocElementProperly();
    var width = $(window).width();
    $(window).resize(function () {
        if ($(this).width() != width) {
            width = $(this).width();
            setDocElementProperly();
        }
    });
    //end obsolete codes

    var curDateString = $("#srv-dt").val();
    var curFinYearString = $("#srv-cur-fin-year").val();
    window._SERVER = {};
    window._CLIENT = {};
    window._UTILS = {};

    window._SERVER.getCurFinYear = function () {
        return curFinYearString;
    };
    window._SERVER.getCurDateTime = function () {
        throw "Method not implemented";
    };
    window._SERVER.getCurDate = function () {
        return new Date(curDateString);
    };
    window._UTILS.getFirstDateFinYear = function (finYear) {
        var leftYear = finYear.split('-')[0];
        return CONVERTER.Date.convertToDate(Number(leftYear), 04, 01);
    };
    window.getFileBase64= function (file, name, callback) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            callback({ Result: reader.result, Name: name });
        };
        reader.onerror = function (error) {
            alertify.error('Unable to read file');
        };
    }; 

    Object.defineProperty(Array.prototype, 'chunkArray', {
        value: function (chunkSize) {
            var array = this;
            return [].concat.apply([],
                array.map(function (elem, i) {
                    return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
                })
            );
        }
    });
    Object.defineProperty(Array.prototype, 'firstOrDefault', {
        value: function (callback) {
            var array = this;
            if (callback) {
                var x = array.filter(function (t) {
                    return callback(t);
                });
                return x[0] ? x[0] : null;
            } else {
                return array[0] ? array[0] : null;
            }
        }
    });
    Object.defineProperty(Array.prototype, 'remove', {
        value: function (callback) {
            var array = this;
            if (callback) {
                var x = array.filter(function (t) {
                    return callback(t);
                }).firstOrDefault();
                if (x) {
                    var i = array.indexOf(x);
                    array.splice(i, 1);
                }
            }
            return array;
        }
    });
    $("body").tooltip({
        selector: "[data-toggle='tooltip']",
        container: "body"
    })
    .popover({
            selector: "[data-toggle='popover']",
            container: "body",
            html: true
        });
    $('body').on('click', function (e) {
        //did not click a popover toggle or popover
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    $('body').on('keypress', 'input[type="text"].money', function (evt) {
        var element = this;
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
            return false;
        else {
            var len = $(element).val().length;
            var index = $(element).val().indexOf('.');
            if (index > 0 && charCode == 46) {
                return false;
            }
            if (index > 0) {
                var CharAfterdot = (len + 1) - index;
                if (CharAfterdot > 5) {
                    return false;
                }
            }

        }
        return true;
    })
});

