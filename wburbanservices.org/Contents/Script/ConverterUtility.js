﻿var CONVERTER = {};
$(document).ready(function () {
    CONVERTER.Date = {};
    var dt = CONVERTER.Date;
    dt.convertToString = function (date, stringFormat) {
        return date.format(stringFormat);
    };
    dt.convertToDate = function (year, month, day) {
        return new Date(Number(year), Number(month) - 1, Number(day));
    };
    dt.convertCsToJsDate = function (date) {
        if (date == '' || date == null || date == "null" || (!date)) {
            return null; 
        } else {
            var ToDate = new Date(parseInt((date).substr(6)));
            return ToDate;
        }
    };
    dt.isDateObject = function (date) {
        if (date instanceof Date) {
            return true;
        }
        return false;
    }
    dt.isDateString = function (stringDate) {
        return (new Date(stringDate) !== "Invalid Date" && !isNaN(new Date(stringDate))) ? true : false;
    };

    CONVERTER.DateDiff = {};
    var dtDiff = CONVERTER.DateDiff;
    dtDiff.CountDays = function (startDate, endDate) {
        var treatAsUTC = function (date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }
        var millisecondsPerDay = 24 * 60 * 60 * 1000;
        var res = (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        return Number(res);
    };
    dtDiff.CountMonths = function (startDate, endDate) {
        var d1 = endDate, d2 = startDate;
        if (endDate < startDate) {
            d1 = startDate;
            d2 = endDate;
        }
        var m = (d1.getFullYear() - d2.getFullYear()) * 12 + (d1.getMonth() - d2.getMonth());
        if (d1.getDate() < d2.getDate())--m;
        return m;
    }

    CONVERTER.currency = {};
    CONVERTER.currency.format = function (number,scale) {
        var isInteger = function (value) {
            return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }
        var x = number;
        if (isInteger(x)) {
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            return res;
        } else {
            if (isInteger(scale)) {
                x = x.toFixed(scale).toString();
            }
            x = x.toString();
            var afterPoint = '';
            if (x.indexOf('.') > 0)
                afterPoint = x.substring(x.indexOf('.'), x.length);
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
            return res;
        }
    };
});