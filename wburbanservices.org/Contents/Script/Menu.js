﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {

        this.GetMenuDetails = function (callback) {
            $.ajax({
                type: "POST",
                url: "/Shared/Menu/GetMenu",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //console.log(response);
                    // callback(response);
                    $.each(response.Data, function () {
                        var dropdown = $('<li class="dropdown"></div>');
                        var parentID = this.ParrentMenuID;
                        if (this.ParrentMenuID == null) {
                            if (this.List.length > 0) {
                                var btn = $('<a class=" dropdown-toggle" type="button" data-toggle="dropdown">' + this.MenuName + '<span class="caret"></span></a>');
                                var dropdownMenu = $('<ul class="dropdown-menu"></ul>');
                                $.each(this.List, function () {
                                    var li = "";
                                    if (this.List.length > 0) {
                                        li = $('<li class="dropdown-submenu"><a class="test" tabindex= "-1" href="' + this.Url + '" >' + this.MenuName + '<span class="caret"></span></a></li>');
                                        var dropdownMenuSub = $('<ul class="dropdown-menu"></ul>');
                                        $.each(this.List, function () {
                                            var liSub = $('<li><a tabindex="-1" href="' + this.Url + '">' + this.MenuName + '</a></li>');
                                            dropdownMenuSub.append(liSub);
                                        });
                                        li.append(dropdownMenuSub);
                                    }
                                    else {
                                        li = $('<li><a tabindex="-1" href="' + this.Url + '">' + this.MenuName + '</a></li>');
                                    }
                                    dropdownMenu.append(li);
                                });
                                dropdown.append(btn);
                                dropdown.append(dropdownMenu);
                            }
                            else {
                                var btn = $('<a class="dropdown-toggle" type="button" data-toggle="dropdown">' + this.MenuName + '<span class="caret"></span></a>');
                                dropdown.append(btn);
                            }
                        }
                        else {
                            //var li = $('<li style="color:white;" class="dropdown-submenu"><a class="test" tabindex="-1" href="#">' + this.MenuName + '</a></li>');
                            // var li = $('<li style="color:white;" ><a class="test" tabindex="-1" href="' + this.Url + '">' + this.MenuName + '</a></li>');

                        }
                        console.log(dropdown);
                        $(".nav-menu").append(dropdown);
                    });
                    $(".nav-menu").prepend('<li style="color:white;"><a href="/Home/Index">Home</a></li>');
                },
                failure: function (response) {
                    alert(response);
                }
            });
        };

    };

    var APP_OBJECT = function (serverObject) {

        var populateMenu = function () {
            serverObject.GetMenuDetails(function (response) {
                var status = response.Status;
                if (status == 200) {
                    //alert(response.Message);
                    buildMenu($('#menu-sm'), response.Data);
                    buildMenu($('#menu-lg'), response.Data);
                    //var m = $('.nav-menu').html();
                    // $('<li style="color:white;"><a href="/Home/Index">Home</a></li>').insertBefore(".nav-menu"); prepend
                    $(".nav-menu").prepend('<li style="color:white;"><a href="/Home/Index">Home</a></li>');
                    //$('#menu').menu();
                } else {
                    alert(response.Message);
                }
            });
        };

        var buildMenu = function (parent, items) {
            var chk;
            $.each(items, function () {
                var parentID = this.ParrentMenuID;
                if (this.ParrentMenuID == null) {
                    if (this.List.length > 0) {
                        //child node
                        var li = $('<li style="color:white;" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">' + this.MenuName + '<span class="caret"></span></a></li>');
                    }
                    else {
                        var li = $('<li style="color:white;"><a href="' + this.Url + '">' + this.MenuName + '</a></li>');
                    }

                }
                else {
                    //var li = $('<li style="color:white;" class="dropdown-submenu"><a class="test" tabindex="-1" href="#">' + this.MenuName + '</a></li>');
                    var li = $('<li style="color:white;" ><a class="test" tabindex="-1" href="' + this.Url + '">' + this.MenuName + '</a></li>');

                }

                if (!this.IsActive) {
                    li.addClass("ui-state-disabled");
                }
                li.appendTo(parent);
                if (this.List && this.List.length > 0) {
                    chk = 'Y';
                    var ul = $('<ul class="dropdown-menu"></ul>');
                    ul.appendTo(li);
                    buildMenu(ul, this.List);
                }
                chk = 'N';

            });
        }

        this.app_Initiate = function () {
            if ($(".menu-availibility").length > 0 && $(".menu-availibility").val() === 'no') {
                return;
            }
            populateMenu();
        };
    };

    var INIT = function () {
        var serverObject = new SERVER_OBJECT();
        var appObject = new APP_OBJECT(serverObject);
        appObject.app_Initiate();
    };

    (function () {
        INIT();
    }());
});

//$(document).ready(function () {
//    //$("#tabPropertyTaxDtl tbody").on('click', 'span[id="spanDelete"]', onDeleteRow);
//    $('#menu').on('click', '.dropdown-submenu a.test', function (e) {
//        $(this).next('ul').toggle();
//        e.stopPropagation();
//        e.preventDefault();
//    });
//});