﻿var MM = {};//Gaint object

MM.messageDialog = {
    alert: function (title, body, type) {
        //type in [success,error,undefined,null]
        
    },
    confirm: function (title, body, callback) {
        var confObj={
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: callback
        };
        if (title !== null && title !== typeof ('undefined') && title.trim() !== '') {
            //show with title
            confObj.title = title;
        } else if (body !== null && body !== typeof ('undefined') && body.trim() !== '') {
            //show with title
            confObj.message = body;
        }

        bootbox.confirm(confObj);
    },
    customDialog: function () { }
};//all message box available here
MM.notifire = {};// all Notifire availble here


