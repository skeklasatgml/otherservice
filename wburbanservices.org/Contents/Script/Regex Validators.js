﻿window.RegxValidators = {};
var validators = window.RegxValidators;
validators.emailValidate = function (email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var patt = new RegExp(regex);
    return patt.test(email);
};
validators.MobileNo10CharValidate = function (phone) {
    var regex = /^\d{10}$/;
    var patt = new RegExp(regex);
    return patt.test(phone);
};